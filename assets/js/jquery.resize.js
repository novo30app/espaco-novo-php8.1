// RESIZE IMAGENS SLIDE
function adaptImage(targetimg, local) {
    //alert(local);
    if(local!=undefined){
        var wheight = local.height();
        var wwidth = local.width();
    }else{
        var wheight = $(window).height();
        var wwidth = $(window).width();
    }

    targetimg.removeAttr("width")
        .removeAttr("height")
        .css({ width: "", height: "" });
    /*
       var imgwidth = targetimg.width();
       var imgheight = targetimg.height();
   */

    var imgwidth = targetimg.data('w');
    var imgheight = targetimg.data('h');
    //alert(targetimgW);


    var destwidth = wwidth;
    var destheight = wheight;

    var ratio_orig = imgwidth/imgheight;
    //verifica imagem vertical ou horizontal
    if(imgheight > imgwidth) {

        destwidth = (imgwidth * wheight)/imgheight;

        $(targetimg).height(destheight);
        $(targetimg).width(destwidth);

        destheight = $(targetimg).height();

    }else{

        if (wwidth/wheight>ratio_orig) {
            destheight = (imgheight * wwidth)/imgwidth;

            $(targetimg).height(destheight);
            $(targetimg).width(destwidth);

            destwidth = $(targetimg).width();
        } else {
            destwidth = (imgwidth * wheight)/imgheight;

            $(targetimg).height(destheight);
            $(targetimg).width(destwidth);

            destheight = $(targetimg).height();
        }
    }


    var posy = (destheight/2 - wheight/2);
    var posx = (destwidth/2 - wwidth/2);


    //if(posy > 0) {
    posy *= -1;
    //}
    //if(posx > 0) {
    posx *= -1;
    //}


    $(targetimg).css({'top': posy + 'px', 'left': posx + 'px'});
}

/* JQUERY GERAL */
jQuery(document).ready(function() {

    $('.resize').each(function() {
        adaptImage($(this), $(this).parent());
    });

    $(window).resize(function() {
        setTimeout(function() {
            $('.resize').each(function() {
                adaptImage($(this), $(this).parent());
            });
        }, 100);
    });

});