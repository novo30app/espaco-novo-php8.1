var kanban = new jKanban({
    element: '#cards',
    gutter: '15px',
    click: function (el) {
        alert(el.innerHTML);
    },
    boards: [{
        'id': 'status1',
        'title': 'Pendente',
        'class': 'bg-warning',
    },
        {
            'id': 'status2',
            'title': 'Resolvido',
            'class': 'bg-success',
        },
        {
            'id': 'status3',
            'title': 'Respondido/Não Atendido',
            'class': 'bg-danger',
        }
    ]
});