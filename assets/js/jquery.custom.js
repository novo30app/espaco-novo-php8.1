/* JQUERY GERAL */
jQuery(document).ready(function() {

    $(".descer").click(function(event){
        event.preventDefault();
        $('html,body').animate({scrollTop:$(this.hash).offset().top}, 800);
    });

    // MENU MOBILE
    $(document).on("click", ".menuMob.off", function(evt){
        event.preventDefault();
        $(this).removeClass('off');
        $('nav').removeClass('off');
        $(this).addClass('active');
        $('nav').addClass('active');
    });

    $(document).on("click", ".menuMob.active", function(evt){
        event.preventDefault();
        $(this).removeClass('active');
        $('nav').removeClass('active');
        $(this).addClass('off');
        $('nav').addClass('off');
    });

});