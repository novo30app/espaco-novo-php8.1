const payoffForm = document.getElementById('payoffForm');
payoffForm.addEventListener('submit', e => {
    $('#save').addClass('disabled');
    e.preventDefault();
    showLoading();
    let formData = formDataToJson('payoffForm');
    fetch(baseUrl + 'api/financeiro/contribuicoes-abertas', {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        $('#save').removeClass('disabled');
        response.json().then(json => {
            if (response.status === 201) {
                $('#save').addClass('disabled');
                showNotify('success', json.message, 0);
                //setTimeout(function () {
                //    location.reload()
                //}, 15000);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

$('input[type=checkbox]').on('change', function () {
    var quantity = $('input[type=checkbox]:checked').length;
    var today = moment();
    var validslip = today.add(5, "d");
    var valid = validslip.format("DD/MM/YYYY");
    var totalValue = document.getElementById('totalValue');
    var total = 0;
    var valores = $('input[type="checkbox"]:checked');
    setIds();
    $.each(valores, function (i, v) {
        total = parseFloat(total) + parseFloat($(v).val().replace(",", "."));
    });
    var f = total.toLocaleString('pt-br', {
        style: 'currency',
        currency: 'BRL'
    });
    document.getElementById("slip").innerHTML = valid;
    document.getElementById("total").innerHTML = f;
    document.getElementById("sliptotal").innerHTML = quantity;
    totalValue.value = total;
});

function setIds() {
    var arr = [];
    var inputElements = document.getElementsByClassName('checkbox');
    for (var i = 0; inputElements[i]; ++i) {
        if (inputElements[i].checked)
            arr.push(inputElements[i].name);
    }
    ids.value = arr;
}

$(document).ready(function () {
    let clipboard = new ClipboardJS('.btn');
    clipboard.on('success', function (e) {
        alert('Copiado para área de transferência');
    });
    $("select[name=paymentMethod]").change(function () {
        let paymentMethod = $(this).val();
        $("#creditCards").hide(500);
        if (paymentMethod == 1) {
            $("#creditCards").show(500);
            $("#lineslip").hide(500);
        } else if (paymentMethod == 2) {
            $("#lineslip").show(500);
            $("#creditCards").hide(500);
        } else {
            $("#creditCards").hide(500);
            $("#lineslip").hide(500);
        }
    });
});