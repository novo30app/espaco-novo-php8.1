var index = partial = 0;
var total = 1;
var payDayBegin = payDayEnd = paymentValue = origin = description = directory = providerCnpj = providerName = "";

const form = document.getElementById('filter');
form.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    index = partial = 0;
    total = 1;
    payDayBegin = formData.payDayBegin;
    payDayEnd = formData.payDayEnd;
    paymentValue = formData.paymentValue;
    origin = formData.origin;
    description = formData.description;
    directory = formData.directory;
    providerCnpj = formData.providerCnpj;
    providerName = formData.providerName;
    $("#table tbody").empty();
    generateTable();
});

function resetTable() {
    index = partial = 0;
    total = 1;
    payDayBegin = payDayEnd = paymentValue = origin = description = directory = providerCnpj = providerName = "";
    $("#table tbody").empty();
    generateTable();
};

function formatAttachment(attachment) {
    attachment = attachment.split("uploads/");
    return 'https://gestao.novo.org.br/' + `uploads/${attachment[1]}`;
}

function generateList(payments) {
    let cpfCnpj = payments.providerCpfCnpj;
    var cpfCnpjFormated = cpfCnpj.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4");
    if(cpfCnpj.length > 11) var cpfCnpjFormated = cpfCnpj.replace(/^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/, "$1$2$3/$4-$5");
    let bankSlip = 'N/A';
    if(payments.attachmentBillet != '') bankSlip = `<a href="${formatAttachment(payments.attachmentBillet)}" target="_blank"><i class="fa fa-file-text-o text-info px-1" title="Boleto Bancário"></i></a>`;
    let fiscal = 'N/A';
    if(payments.attachmentNf != '') fiscal = `<a href="${formatAttachment(payments.attachmentNf)}" target="_blank"><i class="fa fa-file-text-o text-info px-1" title="Documento Fiscal"></i></a>`;
    let contract = 'N/A';
    if(payments.contract != '') {
        contract = `<a href="${baseUrl}portal-da-transparencia/contratos/${payments.contract}" target="_blank"><i class="fa fa-file-text text-info px-1" title="Contrato"></i></a>`;
        if(payments.contract.includes('/var/www/html/')) contract = `<a href="${formatAttachment(payments.contract)}" target="_blank"><i class="fa fa-file-text-o text-info px-1" title="Contrato"></i></a>`;
    } 
    return `<tr class="middle">
                <td class="text-center">${payments.payDay}</td>
                <td class="text-center">${payments.directory}</td>
                <td class="text-center">${cpfCnpjFormated}</td>
                <td class="text-center">${payments.providerName}</td>
                <td class="text-center">${payments.paymentValue}</td>
                <td class="text-center">${payments.origin}</td>
                <td class="text-center">${payments.description}</td>
                <td class="text-center">${bankSlip}</td>
                <td class="text-center">${fiscal}</td>
                <td class="text-center">${contract}</td>
            </tr>`;
}

function generateTable() {
    if (total > partial) {
        $('#loader').show();
        // fetch(`${baseUrl}portal-da-transparencia/listagem/?index=${index}&payDayBegin=${payDayBegin}&payDayEnd=${payDayEnd}&paymentValue=${paymentValue}&origin=${origin}&description=${description}&directory=${directory}&providerCnpj=${providerCnpj}&providerName=${providerName}`, {
        fetch(`${baseUrlGestao}espaco-novo/portal-da-transparencia/listagem/?index=${index}&payDayBegin=${payDayBegin}&payDayEnd=${payDayEnd}&paymentValue=${paymentValue}&origin=${origin}&description=${description}&directory=${directory}&providerCnpj=${providerCnpj}&providerName=${providerName}`, {
            method: "GET",
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-Auth': token
            }
        }).then(response => {
            response.json().then(json => {
                $('#loader').hide();
                total = json.total;
                partial = json.partial;
                let membershipList = json.message;
                let options = membershipList.map(generateList);
                $("#table tbody").append(options);
                $('#total').text(total);
                $('#partial').text(partial);
            });
        });
    }
}

$(document).ready(function () {
    generateTable();
    $(window).scroll(function() {
        if ($(window).scrollTop() + $(window).height() >= $(document).height() - 1) {
            index++;
            generateTable();
        }
    });
    $("#paymentValue").maskMoney({
        prefix: 'R$ ',
        allowNegative: true,
        thousands: '.',
        decimal: ',',
        affixesStay: false
    });
});