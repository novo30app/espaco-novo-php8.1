const instructionMessageForm = document.getElementById('instructionMessageForm');
$("#instructionForm").submit(function(e) {
    e.preventDefault();
    $('#saveInstruction').addClass('disabled');
    showLoading();
    $.ajax({
            type: 'POST',
            url: `https://cep.novo.org.br/api/instrucao/`,
            data: new FormData(this),
            processData: false,
            contentType: false
        })
        .done(function(json) {
            closeLoading();
            instructionMessageForm.classList.add("alert-success");
            instructionMessageForm.classList.remove("alert-danger");
            instructionMessageForm.textContent = json.message;
            instructionMessageForm.classList.remove("d-none");
            $('#saveInstruction').addClass('disabled');
            setTimeout(function() {
                $('#instructionModal').modal('hide');
                location.reload();
            }, 2000);
        })
        .fail(function(json) {
            closeLoading();
            $('#saveInstruction').removeClass('disabled');
            instructionMessageForm.classList.add("alert-danger");
            instructionMessageForm.classList.remove("alert-success");
            instructionMessageForm.textContent = json.responseJSON.message;
            instructionMessageForm.classList.remove("d-none");
        });
});

const defenseMessageForm = document.getElementById('defenseMessageForm');
$("#defenseForm").submit(function(e) {
    e.preventDefault();
    $('#saveDefense').addClass('disabled');
    showLoading();
    $.ajax({
            type: 'POST',
            url: `https://cep.novo.org.br/api/defesa/`,
            data: new FormData(this),
            processData: false,
            contentType: false
        })
        .done(function(json) {
            closeLoading();
            defenseMessageForm.classList.add("alert-success");
            defenseMessageForm.classList.remove("alert-danger");
            defenseMessageForm.textContent = json.message;
            defenseMessageForm.classList.remove("d-none");
            $('#saveDefense').addClass('disabled');
            setTimeout(function() {
                $('#defenseModal').modal('hide');
                location.reload();
            }, 2000);
        })
        .fail(function(json) {
            closeLoading();
            defenseMessageForm.classList.add("alert-danger");
            defenseMessageForm.classList.remove("alert-success");
            defenseMessageForm.textContent = json.responseJSON.message;
            defenseMessageForm.classList.remove("d-none");
            $('#saveDefense').removeClass('disabled');
        });
});

function changeModal(modal, action){
    $('#' + modal).modal(action);
}

function setPhaseDefense(phase) {
    $('#phaseDefense').val(phase);
}
