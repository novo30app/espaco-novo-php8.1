const divMessageForm = document.getElementById('divMessageForm');
$("#form").submit(function(e) {
    $('#save').addClass('disabled');
    e.preventDefault();
    showLoading();
    $.ajax({
            type: 'POST',
            url: `https://cep.novo.org.br/api/processos/`,
            data: new FormData(this),
            processData: false,
            contentType: false
        })
        .done(function(json) {
            closeLoading();
            divMessageForm.classList.add("alert-success");
            divMessageForm.classList.remove("alert-danger");
            divMessageForm.textContent = json.message;
            divMessageForm.classList.remove("d-none");
            document.getElementById("save").disabled = true;
            setTimeout(function () {
                location.reload()
            }, 2500);
        })
        .fail(function(json) {
            closeLoading();
            $('#save').removeClass('disabled');
            divMessageForm.classList.add("alert-danger");
            divMessageForm.classList.remove("alert-success");
            divMessageForm.textContent = json.responseJSON.message;
            divMessageForm.classList.remove("d-none");
        });
});

var elementRef =    `<div class='dep_fc'>
                        <label class="btn btn-block btn-success text-white" name="othersFilesDiv" style="margin-top: 0px;">
                            <input class="upload-file-selector" id="complaintFiles" type="file" name="complaintFiles[]" style="opacity: 1; position: relative;">
                        </label>
                        <button class='btn btn-primary remove' type='button' ><i class="fa fa-trash" aria-hidden="true"></i></button>
                    </div>`;

$("#add-campo").click(function () {
    $('#divCloned').append(elementRef);
});

$(document).on('click', 'button.remove', function () {
    $(this).closest('div.dep_fc').remove();
});

let countSqueaker = 1;
function duplicateDiv1() {
    countSqueaker++;
    let squeakerHtml = `<div class="row dep_fc">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="squeakerName" class="required">Nome (${countSqueaker}):</label>
                                    <input class="form-control" type="text" name="squeakerName[]" id="squeakerName">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="squeakerCPF" class="required">CPF (${countSqueaker}):</label>
                                    <input class="form-control" type="text" name="squeakerCPF[]" id="squeakerCPF">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="squeakerEmail" class="required">E-mail (${countSqueaker}):</label>
                                    <input class="form-control" type="email" name="squeakerEmail[]" id="squeakerEmail">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="squeakeraffiliatedID" class="required">Filiado ID (${countSqueaker}):</label>
                                    <input class="form-control" type="text" name="squeakeraffiliatedID[]" id="squeakeraffiliatedID">
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label>Remover</label>
                                    <button class='btn btn-primary remove' type='button' >
                                        <i class="fa fa-trash" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>`;
    $("#squeakerDiv").append(squeakerHtml);
}

let countDenounced = 1;
function duplicateDiv2() {
    countDenounced++;
    let denouncedHtml = `<div class="row dep_fc">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="denouncedName" class="required">Nome (${countDenounced}):</label>
                                    <input class="form-control" type="text" name="denouncedName[]" id="denouncedName">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="denouncedCPF">CPF (${countDenounced}):</label>
                                    <input class="form-control" type="text" name="denouncedCPF[]" id="denouncedCPF">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="denouncedEmail">E-mail (${countDenounced}):</label>
                                    <input class="form-control" type="email" name="denouncedEmail[]" id="denouncedEmail">
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label for="denouncedState" class="required">UF (${countDenounced}):</label>
                                    <select class="form-control" name="denouncedState[]" id="denouncedState" 
                                            onchange="getCities(this.value, 'denouncedCity${countDenounced}')">
                                        <option value="">Selecione</option>
                                        <option value="1">AC</option>
                                        <option value="2">AL</option>
                                        <option value="3">AM</option>
                                        <option value="4">AP</option>
                                        <option value="5">BA</option>
                                        <option value="6">CE</option>
                                        <option value="7">DF</option>
                                        <option value="8">ES</option>
                                        <option value="9">GO</option>
                                        <option value="10">MA</option>
                                        <option value="11">MG</option>
                                        <option value="12">MS</option>
                                        <option value="13">MT</option>      
                                        <option value="14">PA</option>      
                                        <option value="15">PB</option>      
                                        <option value="16">PE</option>      
                                        <option value="17">PI</option>      
                                        <option value="18">PR</option>      
                                        <option value="19">RJ</option>      
                                        <option value="20">RN</option>      
                                        <option value="21">RO</option>      
                                        <option value="22">RR</option>      
                                        <option value="23">RS</option>      
                                        <option value="24">SC</option>      
                                        <option value="25">SE</option>      
                                        <option value="26">SP</option>      
                                        <option value="27">TO</option>                        
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="denouncedCity" class="required">Cidade (${countDenounced}):</label>
                                    <select class="form-control" name="denouncedCity[]" id="denouncedCity${countDenounced}">
                                        <option value="">Cidade</option>
                                    </select>
                                </div>
                            </div>
                             <div class="col-md-1">
                                <div class="form-group">
                                    <label>Remover</label>
                                    <button class='btn btn-primary remove' type='button' >
                                        <i class="fa fa-trash" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>`;
    $("#denouncedDiv").append(denouncedHtml);
}

var elementRefIndicationOfEvidence =   
     `<div class='dep_fc2'>
        <label class="btn btn-block btn-success text-white" name="othersFilesDiv" style="margin-top: 0px;">
            <input class="upload-file-selector" id="indicationOfEvidenceFiles" type="file" name="indicationOfEvidenceFiles[]" style="opacity: 1; position: relative;">
        </label>
        <button class='btn btn-primary remove2' type='button' ><i class="fa fa-trash" aria-hidden="true"></i></button>
    </div>`;

$("#add-provas").click(function () {
    $('#divClonedIndicationOfEvidence').append(elementRefIndicationOfEvidence);
});

$(document).on('click', 'button.remove2', function () {
    $(this).closest('div.dep_fc2').remove();
});