function buttonActive(status, url, id, button, method) {
    if (status == 1) {
        return `
            <div class="badge badge-success label-square pointer" onclick="changeActive('${url}', '${id}', '${button}', '${method}')" id="button${button}${id}">
                <span class="f-14">Ativo</span>
            </div>
            `;
    } else if (status == 0) {
        return `
            <div class="badge badge-danger label-square pointer" onclick="changeActive('${url}', '${id}', '${button}', '${method}')" id="button${button}${id}">
               <span class="f-14">Inativo</span>
            </div>
            `;
    }
}

function changeActive(url, id, button, method) {
    fetch(`${baseurl}${url}/novo-status/${id}`, {
        method: method,
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    }).then(response => {
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                if (method == 'put') {

                    if ($('#button' + button + id).hasClass('badge-success')) {
                        $('#button' + button + id).removeClass('badge-success').addClass('badge-danger').empty().html(`<span class="f-14">Inativo</span>`);
                    } else {
                        $('#button' + button + id).removeClass('badge-danger').addClass('badge-success').empty().html(`<span class="f-14">Ativo</span>`);
                    }
                } else if (method == 'delete') {
                    $('#line' + id).hide("slow");
                    $('#modalDelete').modal('hide');
                }
            } else if (response.status === 403) {
                showNotify('danger', 'Você foi logado em outro lugar, será redirecionado para a página de login!', 0);
                setTimeout(function () {
                    window.location.href = baseurl + 'logout';
                }, 1000)
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
}