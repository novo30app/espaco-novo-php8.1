const form = document.getElementById('filter');
form.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('LibertasCoursesBuys');
})

function statusStr(status) {
    switch (status) {
        case 0:
            return "Em aberto";
        case 1:
            return "Pago";
        case 2:
            return "Cancelado";
        default:
            return "desconhecido";
    }
}

function paymentMethodStr(paymentMethod) {
    switch (paymentMethod) {
        case 1:
            return "Cartão de crédito";
        case  2:
            return "Boleto";
        case  3:
            return "PIX";
        default:
            return 'Desconhecido';
    }
}

function generateLines(requests) {
    let status = 'Encerrado';
    if (requests.active == 1) status = 'Aberto';
    return `<tr class="middle">
                    <td class="text-center">${requests.id}</td>
                    <td class="text-center"><a href="${baseUrl}cursos-libertas/curso/${requests.courseId}">${requests.title}</a></td>
                    <td class="text-center">${formatMoney(requests.price)}</td>                   
                    <td class="text-center">${paymentMethodStr(requests.paymentMethod)}</td>                    
                    <td class="text-center">${statusStr(requests.status)}</td>                    
                </tr>`;
}

function setStorageIndex() {
    setStorage('LibertasCoursesBuys', {
        'url': 'cursos-libertas/minhas-compras/json',
        'columns': 5,
        'variables': {
            'index': 0,
            'title': '',
            'limit': 25
        }
    })
}

$(document).ready(function () {
    verifySession('LibertasCoursesBuys');
});