const form = document.getElementById('filter');
form.addEventListener('submit', e => {
    e.preventDefault();
    filterTable('LibertasCourses');
})


function generateLines(requests) {
    let status = 'Encerrado';
    if (requests.active == 1) status = 'Aberto';
    return `<tr class="middle">
                    <td class="text-center">${requests.id}</td>
                    <td class="text-center"><a href="${baseUrl}cursos-libertas/curso/${requests.id}">${requests.title}</a></td>
                    <td class="text-center">${formatMoney(requests.price)}</td>
                    <td class="text-center">${requests.active ? 'Aberto' : 'Encerrado'}</td>                    
                </tr>`;
}

function setStorageIndex() {
    setStorage('LibertasCourses', {
        'url': 'cursos-libertas/json',
        'columns': 5,
        'variables': {
            'index': 0,
            'title': '',
            'limit': 25
        }
    })
}

$(document).ready(function () {
    verifySession('LibertasCourses');
});