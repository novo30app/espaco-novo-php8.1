function generatePagination(total, indexPag, limitPag, session) {
    let div = Math.ceil(total / limitPag);

    $('#pagination' + session).empty();
    if ((total / limitPag) <= 5) {
        let disabled = '';
        if (indexPag == 0) disabled = 'disabled';
        let buttons = `<li class="page-item ${disabled} pointer" onclick="addIndex('previous', ${div}, '${session}')"><a class="page-link">Anterior</a></li>`;
        for (let i = 1; i <= div; i++) {
            let classe = '';
            if ((i - 1) == indexPag) classe = 'active';
            buttons += `<li class="page-item ${classe} pointer" onclick="addIndex(${i - 1}, ${div}, '${session}')"><a class="page-link">${i}</a></li>`;
        }
        disabled = '';
        if (indexPag + 1 == div) disabled = 'disabled';
        buttons += `<li class="page-item pointer ${disabled}" onclick="addIndex('next', ${div}, '${session}')"><a class="page-link">Próximo</a></li>`;
        $('#pagination' + session).append(buttons);
    } else if ((total / limitPag) > 5) {
        let disabled = '';
        if (indexPag == 0) disabled = 'disabled';
        let buttons = `<li class="page-item ${disabled} pointer" onclick="addIndex('previous', ${div}, '${session}')"><a class="page-link">Anterior</a></li>`;

        if (indexPag >= 3) buttons += `<li class="page-item"><a class="page-link">...</a></li>`;

        if (indexPag < 3) {
            for (let i = 1; i <= 5; i++) {
                let classe = '';
                if ((i - 1) == indexPag) classe = 'active';
                buttons += `<li class="page-item ${classe} pointer" onclick="addIndex(${i - 1}, ${div}, '${session}')"><a class="page-link">${i}</a></li>`;
            }
        } else if (indexPag >= 3 && indexPag < (div - 2)) {
            for (let i = indexPag - 1; i <= indexPag + 3; i++) {
                let classe = '';
                if ((i - 1) == indexPag) classe = 'active';
                buttons += `<li class="page-item ${classe} pointer" onclick="addIndex(${i - 1}, ${div}, '${session}')"><a class="page-link">${i}</a></li>`;
            }
        } else if (indexPag >= (div - 2)) {
            for (let i = div - 5; i <= Math.ceil(total / limitPag); i++) {
                let classe = '';
                if ((i - 1) == indexPag) classe = 'active';
                buttons += `<li class="page-item ${classe} pointer" onclick="addIndex(${i - 1}, ${div}, '${session}')"><a class="page-link">${i}</a></li>`;
            }
        }

        if (indexPag < (Math.ceil(total / limitPag) - 3)) buttons += `<li class="page-item"><a class="page-link">...</a></li>`;

        if (indexPag + 1 == div) disabled = 'disabled';
        buttons += `<li class="page-item pointer ${disabled}" onclick="addIndex('next', ${div}, '${session}')"><a class="page-link">Próximo</a></li>`;
        $('#pagination' + session).append(buttons);
    }
}

function addIndex(value, div,session) {
    let obj = JSON.parse(sessionStorage.getItem(session));

    if (value == obj.variables.index) {
        return false;
    } else if (value == 'next') {
        if (obj.variables.index + 1 == div) return false;
        obj.variables.index = obj.variables.index + 1;
    } else if (value == 'previous') {
        if (obj.variables.index == 0) return false;
        obj.variables.index =  obj.variables.index - 1;
    } else {
        obj.variables.index = value;
    }
    sessionStorage.setItem(session, JSON.stringify(obj));
    $('html, body').animate({scrollTop: 0}, 'slow');
    $('#table' + session + ' tbody').empty();
    generateTable(session);
}

function getLimit(value, session) {
    let obj = JSON.parse(sessionStorage.getItem(session));
    $('#table' + session + ' tbody').empty();
    obj.variables.index =  0;
    obj.variables.limit =  value;
    sessionStorage.setItem(session, JSON.stringify(obj));
    generateTable(session);
}