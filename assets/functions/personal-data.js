$(document).ready(function() {
    maskCellPhone();
    if (verify) addressFields(false);
    if (irpf == 1) exemptionChange(1);
    $("#zipCode").blur(function() {
        loadAddress();
    });
});

function exemptionChange(value) {
    const exemption = document.getElementById("exemption")
    if (value == 1) {
        document.getElementById('exemptionDiv').classList.remove("d-none");
    } else {
        exemption.checked = false;
        document.getElementById('exemptionDiv').classList.add("d-none");
    }
}

var diaCode = -1;

var cellPhone = window.intlTelInput(document.querySelector('#cellPhone'), {
    "initialCountry": 'br'
});

function maskCellPhone() {
    if (diaCode == cellPhone.getSelectedCountryData().dialCode) {
        return;
    }
    diaCode = cellPhone.getSelectedCountryData().dialCode;
    $('#cellPhone').unmask();
    $('#cellPhone').mask('999999999999');
    if (diaCode == 55) {
        $('#cellPhone').mask('(99)99999-99999');
    }
}

var phone = window.intlTelInput(document.querySelector('#phone'), {
    "initialCountry": 'br'
});

function maskPhone() {
    if (diaCode == phone.getSelectedCountryData().dialCode) {
        return;
    }
    diaCode = phone.getSelectedCountryData().dialCode;
    $('#phone').unmask();
    $('#phone').mask('999999999999');
    if (diaCode == 55) {
        $('#phone').mask('(99)9999-9999');
    }
}

const formPersonalData = document.getElementById('personalData');
function validateForm() {
    let errors = '';
    const formData = formDataToJson('personalData');
    const dateMax = moment(new Date()).subtract(16, 'years');
    const dateMin = moment(new Date()).subtract(120, 'years');
    if (formData.name.trim().length < 3) errors += '<b>Nome Completo</b> inválido<br>';
    if (!moment(formData.birth, 'DD/MM/YYYY', true).isValid()) errors += '<b>Data de Nascimento</b> valor inválido<br>';
    if (formData.cellPhone.replace(/\D+/g, '').length < 8) errors += '<b>Telefone Celular</b> inválido<br>';
    if (moment(formData.birth, 'DD/MM/YYYY', true).isValid() &&
        moment(formData.birth, 'DD/MM/YYYY').format('YYYY/MM/DD') > dateMax.format('YYYY/MM/DD')) {
        errors += '<b>Minimo 16 anos</b><br>';
    }
    if (moment(formData.birth, 'DD/MM/YYYY', true).isValid() &&
        moment(formData.birth, 'DD/MM/YYYY').format('YYYY/MM/DD') < dateMin.format('YYYY/MM/DD')) {
        errors += '<b>Máximo 120 anos</b><br>';
    }
    if (formData.zipCode.trim().length < 3) errors += '<b>CEP</b> inválido<br>';
    if (formData.street.trim().length < 3) errors += '<b>Logradouro</b> inválido<br>';
    if (formData.number.trim() == '') errors += '<b>Número</b> é obrigatório<br>';
    if (formData.district.trim().length < 3) errors += '<b>Bairro</b> inválido<br>';
    if (formData.addressOutside == 1) {
        if (formData.addressCountry.trim() == '') errors += '<b>País</b> inválido<br>';
        if (formData.city.trim().length < 3) errors += '<b>Cidade</b> inválido<br>';
    } else {
        if (formData.ufId.trim() == '') errors += '<b>Estado</b> é obrigatório<br>';
        if (formData.cityId.trim() == '') errors += '<b>Cidade</b> é obrigatório<br>';
    }
    if (formData.mother.trim().length < 3) errors += '<b>Nome da Mãe</b> inválido<br>';
    if (formData.voterTitleZone.trim().length < 3) errors += '<b>Zona</b> deve ter no minimo 3 caracteres<br>';
    if (formData.voterTitleSection.trim().length < 3) errors += '<b>Seção</b> deve ter no minimo 3 caracteres<br>';
    if (formData.originVoterTitle == 2) { //brasil
        if (formData.voterTitleUF.trim() == '') errors += '<b>UF eleitoral</b> é obrigatório<br>';
        if (formData.voterTitleCity.trim() == '') errors += '<b>Município Eleitoral</b> é obrigatório<br>';
    } else {
        if (formData.voterTitleCountry.trim() == '') errors += '<b>País eleitoral</b> é obrigatório<br>';
        if (formData.voterTitleCityString.trim() == '') errors += '<b>Município Eleitoral</b> é obrigatório<br>';
    }
    if (!validateVoterTitle(formData.voterTitle)) errors += '<b>Título de eleitor</b> inválido<br>';
    if (errors != '') {
        showNotify('danger', errors, 1500);
        return false;
    }
    return true;

}

formPersonalData.addEventListener('submit', e => {
    e.preventDefault();
    formPersonalData.phoneDialCode.value = phone.getSelectedCountryData().dialCode;
    formPersonalData.phoneIso2.value = phone.getSelectedCountryData().iso2;
    formPersonalData.cellPhoneDialCode.value = cellPhone.getSelectedCountryData().dialCode;
    formPersonalData.cellPhoneIso2.value = cellPhone.getSelectedCountryData().iso2;
    if (!validateForm()) {
        return false;
    }
    showLoading();
    let formData = formDataToJson('personalData');
    fetch(baseUrl + "api/usuario/dados-pessoais", {
        method: "PUT",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
})

function addressFields(type) {
    $('#addressInsideDiv').removeClass('d-none');
    $('#addressOutsideDiv').removeClass('d-none');
    $('#addressCountryDiv').removeClass('d-none');
    if (type == false) {
        $('#addressOutsideDiv').addClass('d-none');
        $('#addressCountryDiv').addClass('d-none');
    } else {
        $('#addressInsideDiv').addClass('d-none');
    }
}

function voterTitleFields(type) {
    $('#titleInternational').hide();
    $('#titleNational').show();
    if (type == 1) {
        $('#titleInternational').show();
        $('#titleNational').hide();
    }
}

function getCityOptionId(city) {
    return `<option value="${city.id}">${city.name}</option>`;
}

function getCityOptionName(city) {
    return `<option value="${city.name}">${city.name}</option>`;
}

function getCities(state, divCities) {
    fetch(`${baseUrl}api/sistema/cidades/${state}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            let cities = json.message;
            let options;
            if (divCities == 'cityId') { //address em string
                options = '<option value="">Selecione a cidade</option>' + cities.map(
                    getCityOptionName);
            } else { //voter title int id
                options = '<option value="">Selecione a cidade</option>' + cities.map(getCityOptionId);
            }
            document.getElementById(divCities).innerHTML = options;
            loadAddress();
        });
    });
}

function loadAddress() {
    var cep = $('#zipCode').val().replace(/\D/g, '');
    var validacep = /^[0-9]{8}$/;
    if (validacep.test(cep)) {
        $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function(dados) {
            if (!("erro" in dados)) {
                $("#street").val(dados.logradouro);
                $("#district").val(dados.bairro);
                $(`#cityId option[value='${dados.localidade}']`).attr('selected', 'selected');
                if (dados.uf != $('#ufId').val()) {
                    $(`#ufId option[value=${dados.uf}]`).attr('selected', 'selected');
                    $('#ufId').trigger('change');
                }
            }
        });
    }
}

function validateVoterTitle(inscricao) {
    var paddedInsc = inscricao.replace(/\D+/g, '');
    var dig1 = 0;
    var dig2 = 0;
    var tam = paddedInsc.length;
    var digitos = paddedInsc.substr(tam - 2, 2);
    var estado = paddedInsc.substr(tam - 4, 2);
    var titulo = paddedInsc.substr(0, tam - 2);
    var exce = (estado == '01') || (estado == '02');
    dig1 = (titulo.charCodeAt(0) - 48) * 9 + (titulo.charCodeAt(1) - 48) * 8 +
        (titulo.charCodeAt(2) - 48) * 7 + (titulo.charCodeAt(3) - 48) * 6 +
        (titulo.charCodeAt(4) - 48) * 5 + (titulo.charCodeAt(5) - 48) * 4 +
        (titulo.charCodeAt(6) - 48) * 3 + (titulo.charCodeAt(7) - 48) * 2;
    var resto = (dig1 % 11);
    if (resto == 0) {
        if (exce) {
            dig1 = 1;
        } else {
            dig1 = 0;
        }
    } else {
        if (resto == 1) {
            dig1 = 0;
        } else {
            dig1 = 11 - resto;
        }
    }
    dig2 = (titulo.charCodeAt(8) - 48) * 4 + (titulo.charCodeAt(9) - 48) * 3 + dig1 * 2;
    resto = (dig2 % 11);
    if (resto == 0) {
        if (exce) {
            dig2 = 1;
        } else {
            dig2 = 0;
        }
    } else {
        if (resto == 1) {
            dig2 = 0;
        } else {
            dig2 = 11 - resto;
        }
    }
    if ((digitos.charCodeAt(0) - 48 == dig1) && (digitos.charCodeAt(1) - 48 == dig2)) {
        return true; // Titulo valido
    } else {
        return false;
    }
}

const formPassword = document.getElementById('formPassword');
formPassword.addEventListener('submit', e => {
    showLoading();
    e.preventDefault();
    let formData = formDataToJson('formPassword');
    fetch(baseUrl + "api/usuario/alterar-senha", {
        method: "PUT",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                formPassword.reset();
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
})

const formEmail = document.getElementById('formEmail');
formEmail.addEventListener('submit', e => {
    showLoading();
    e.preventDefault();
    let formData = formDataToJson('formEmail');
    fetch(baseUrl + "api/usuario/alterar-email", {
        method: "PUT",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
               showNotify('success', json.message, 1500);
                formEmail.reset();
            } else {
                showNotify('denger', json.message, 1500);
            }
        });
    });
})