function ValidateForm(id) {
    console.log(id)
    $('.spinner-card').show();
    var form = document.getElementById(id);
    var inputs = [];
    var ids = [];
    jQuery('[required]').each(function () {
        if (!jQuery(this).val()) {
            inputs.push(jQuery(this).attr('name'));
            ids.push(jQuery(this).attr('id'));
        }
    });
    console.log(inputs);
    console.log(ids);
    if (inputs.length > 0) {
        showNotify('danger', 'Preencha todos os campos obrigatórios!', 1000)
        form.classList.add('was-validated');
        setTimeout(function () {
            $('.spinner-card').hide();
        }, 500)
        return false;
    } else {
        form.classList.add('was-validated');
        return true;
    }
}

function getBorder(classe) {
    $('.' + classe).removeClass('is-invalid is-valid');
    let value = ($('#' + classe).val())

    if (value != '') {
        $('.' + classe).addClass('is-valid');
    } else {
        $('.' + classe).addClass('is-invalid');
    }
}

function tinymceBorder(form, input) {
    var iframe_document = document.getElementById(input + '_ifr').contentWindow.document;
    var tinymce = iframe_document.getElementById('tinymce');
    var value = tinymce.innerHTML;
    value = value.replaceAll('<p>', '');
    value = value.replaceAll('</p>', '');
    value = value.replaceAll('<br>', '');
    value = value.replaceAll('<br data-mce-bogus="1">', '');
    value = value.replaceAll('<p><br data-mce-bogus="1"></p>', '');
    if (value != '') {
        $('.' + input).removeClass('is-invalid');
        $('.' + input).addClass('is-valid');
    } else {
        $('.' + input).removeClass('is-valid');
        $('.' + input).addClass('is-invalid');
    }
}