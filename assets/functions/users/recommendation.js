function copyUrl() {
    var url = document.getElementById('url').value;
    navigator.clipboard.writeText(url);
    showNotify('success', 'Link copiado para área de transferência!', 2000);
}

var index = partial = 0;
var total = 1;
var name = status = "";

const form = document.getElementById('filter');
form.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    index = partial = 0;
    total = 1;
    name = formData.name;
    status = formData.status;
    $("#table tbody").empty();
    generateTable();
});

function resetTable() {
    index = partial = 0;
    total = 1;
    name = status = "";
    $("#table tbody").empty();
    generateTable();
};

function generateList(indications) {
    let style = '';
    switch (indications.affiliated) {
        case 7:
        case 8:
        case 2:
        case 14:
            style= 'background: #008000' // green        
            break;
        case 1:
        case 3:
            style= 'background: #f4c430'; // yellow
            break;
        default:
            style= 'background: #f40000; opacity: 50%;'; // red
            break;
    }
    return `<tr class="middle">
                <td class="text-center">${indications.name}</td>
                <td class="text-center">
                    <a rel="noopener" href="https://api.whatsapp.com/send?phone=${indications.phone.replace(/\\D+/g, '')}" target="_blank">
                        ${indications.phone} <i class="fa fa-external-link" aria-hidden="true"></i>
                    </a>
                </td>
                <td class="text-center">${indications.date}</td>
                <td class="text-center">${indications.status}</td>
                <td class="text-center">${indications.state}</td>
                <td class="text-center">${indications.city}</td>
                <td style="${style}"></td>
            </tr>`;
}

function generateTable() {
    if (total > partial) {
        $('#loader').show();
        fetch(`${baseUrl}usuario/indicacoes/?index=${index}&name=${name}&status=${status}`, {
            method: "GET",
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(response => {
            response.json().then(json => {
                $('#loader').hide();
                total = json.total;
                partial = json.partial;
                let membershipList = json.message;
                let options = membershipList.map(generateList);
                $("#table tbody").append(options);
                $('#total').text(total);
                $('#partial').text(partial);
                $('#totalBottom').text(total);
                $('#partialBottom').text(partial);
                if(total == partial) $('#scrolling').prop('disabled', true);
            });
        });
    }
}

function changeModal(action) {
    $("#exampleModal").modal(action);
}

function scrolling() {
    index++;
    generateTable();
}

const divMessageForm = document.getElementById('divMessageForm');
$("#formFile").submit(function (e) {
    e.preventDefault();
    $('#buttonFile').addClass('disabled');
    showLoading();
    $.ajax({
        type: 'POST',
        url: `${baseUrl}usuario/indicacoes-arquivo/`,
        data: new FormData(this),
        processData: false,
        contentType: false
    })
        .done(function (json) {
            closeLoading();
            showNotify('success', json.message, 0);
            setTimeout(function () {
                window.location.reload;
            }, 5000);
        })
        .fail(function (json) {
            $('#buttonFile').removeClass('disabled');
            closeLoading();
            showNotify('danger', json.responseJSON.message, 1500);
        });
});

$(document).ready(function () {
    generateTable();
});