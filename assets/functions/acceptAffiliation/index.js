    const form = document.getElementById('msform')
    const divMessageForm = document.getElementById('divMessageFormRegister');
    form.addEventListener('submit', e => {
        e.preventDefault();
        let formData = formDataToJson('msform');
        fetch(`${baseurl}api/filiacao/configura-senha/`, {
            method: "POST",
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(formData)
        }).then(response => {
            response.json().then(json => {
                if (json.exemption == 0) {
                    divMessageForm.textContent = json.message;
                } else {
                    divMessageForm.textContent = json.message;
                    //setTimeout(function() {
                    //    window.location.href = `${baseurl}`;
                    //}, 10000);
                }
                $('#save').removeClass('disabled');
                divMessageForm.classList.add("alert-danger");
                divMessageForm.classList.remove("alert-success");
                divMessageForm.classList.remove("d-none");
                if (response.status === 201) {
                    document.getElementById('save').classList.add("d-none");
                    divMessageForm.classList.add("alert-success");
                    divMessageForm.classList.remove("alert-danger");
                    if (json.exemption == 0) {
                            setTimeout(function() {
                                document.getElementById('payment').classList.remove("d-none");
                                document.getElementById('register').classList.add("d-none");
                            }, 3500);
                    }
                }
            });
        });
    });

    function validateFormPayment() {
        divMessageFormPayment.classList.add('d-none');
        let errors = '';
        const formData = formDataToJson('formPayment');
        const maxValue = MAXVALUE;
        let minValue = MINMONTHLY;
        let value = formData.value;
        if (value == 0) {
            value = formData.value2;
            value = value.replace('.', '', value);
            value = value.replace(',', '.', value);
        }
        switch (formData.plan) {
            case '1':
                if (value < minValue) errors +=
                    `Valor minimo mensal é R$ ${minValue.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'})}<br>`;
                if (value > maxValue) errors +=
                    `Valor máximo mensal é R$ ${maxValue.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'})}<br>`;
                break;
            case '12':
                minValue = MINYEARLY;
                if (value < minValue) errors +=
                    `Valor minimo anual é R$ ${minValue.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'})}<br>`;
                if (value > maxValue) errors +=
                    `Valor máximo anual é R$ ${maxValue.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'})}<br>`;
                break;
        }
        if (errors != '') {
            divMessageFormPayment.innerHTML = errors;
            divMessageFormPayment.classList.add("alert-danger");
            divMessageFormPayment.classList.remove("alert-success");
            divMessageFormPayment.classList.remove('d-none');
            return false;
        }
        return true;
    }

    const formPayment = document.getElementById('formPayment');
    const divMessageFormPayment = document.getElementById('divMessageFormPayment');
    formPayment.addEventListener('submit', e => {
        e.preventDefault();
        if (!validateFormPayment()) {
            return;
        }
        $('#savePayment').addClass('disabled');
        showLoading();
        let formData = formDataToJson('formPayment');
        fetch(`${baseurl}api/filiacao/pagamento`, {
            method: 'POST',
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(formData)
        }).then(response => {
            closeLoading();
            divMessageFormPayment.classList.add("alert-danger");
            //divMessageFormPayment.classList.remove("alert-success");
            $('#savePayment').removeClass('disabled');
            response.json().then(json => {
                divMessageFormPayment.classList.remove("d-none");
                divMessageFormPayment.innerHTML = json.message;
                if (response.status === 201) {
                    formPayment.classList.add("d-none");
                    //divMessageFormPayment.classList.add("alert-success");
                    divMessageFormPayment.classList.remove("alert-danger");
                }
            });
        });
    });

    function setPaymentMethod(id) {
        $(`#${id}`).prop('checked', true);
        $('input[name=plan]').prop('checked', false);
        $('input[name=value]').prop('checked', false);
        $('#creditCard').hide();
        $('#valueDiv').hide();
        $('#divValue').hide();
        $('#planDiv').show();
    }

    function setPlan(id) {
        $(`#${id}`).prop('checked', true);
        $('#valueDiv').show();
        $('#paymentMethodDiv').show();
        $('#contributionMonthly').show();
        $('#contributionYearly').show();
        $('#contributionSemester').show();
        $('#paymentMethod1Button').show();
        let formData = formDataToJson('formPayment');
        if (formData.plan == 12) {
            $('#contributionMonthly').hide();
            $('#contributionSemester').hide();
        } else if (formData.plan == 6) {
            $('#contributionMonthly').hide();
            $('#contributionYearly').hide();
        } else {
            $('#contributionYearly').hide();
            $('#contributionSemester').hide();
        }
    }

    function setValue(id) {
        $('#paymentMethodDiv').show();
        $(`#${id}`).prop('checked', true);
        $('#divValue').hide();
        if ($(`#${id}`).val() == 0) $('#divValue').show();
        var seg = document.querySelector('input[name=paymentMethod]:checked').value;
        if (seg == 1) $('#creditCard').show();
    }