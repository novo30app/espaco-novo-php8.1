function showNotify(type, message, delay) {
    let returnMessage = '';
    if (type == 'danger') {
        returnMessage = "<b><i class='fa fa-exclamation-triangle mr-2'></i></b> " + message;
    } else if (type == 'success') {
        returnMessage = "<b><i class='fa fa-check-circle mr-2'></i></b> " + message;
    }
    return $.notify({
        message: returnMessage,
    }, {
        type: type,
        allow_dismiss: false,
        placement: {
            from: "bottom",
            align: "center"
        },
        delay: delay,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutDown'
        },
    });
}

function formDataToJson(id) {
    var $form = $(`#${id}`);
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function (n) {
        if (n['name'].indexOf("[]") != -1) {// é um multiselect
            let field = n['name'].replace("[]", "");
            if (indexed_array[field] == undefined) {
                indexed_array[field] = [];
            }
            indexed_array[field].push(n['value']);
        } else {
            indexed_array[n['name']] = n['value'];
        }
    });
    return indexed_array;
}

function setModalMaxHeight(element) {
    this.$element = $(element);
    this.$content = this.$element.find('.modal-content');
    var borderWidth = this.$content.outerHeight() - this.$content.innerHeight();
    var dialogMargin = $(window).width() < 768 ? 20 : 60;
    var contentHeight = $(window).height() - (dialogMargin + borderWidth);
    var headerHeight = this.$element.find('.modal-header').outerHeight() || 0;
    var footerHeight = this.$element.find('.modal-footer').outerHeight() || 0;
    var maxHeight = contentHeight - (headerHeight + footerHeight);

    this.$content.css({
        'overflow': 'hidden'
    });

    this.$element
        .find('.modal-body').css({
        'max-height': maxHeight,
        'overflow-y': 'auto'
    });
}

$('.modal').on('show.bs.modal', function () {
    $(this).show();
    setModalMaxHeight(this);
});

$(window).resize(function () {
    if ($('.modal.in').length != 0) {
        setModalMaxHeight($('.modal.in'));
    }
});

function removeDiv(id) {
    var campo = $("#" + id);
    campo.hide(201);
    document.getElementById(id).remove();
}

function maskCnpj(cnpj) {
    return cnpj.replace(/^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/, "$1.$2.$3/$4-$5");
}

function maskPhone(phone, id) {

    let teste = document.getElementById(id).value;
    let caracter = teste.replace(/\D/gim, '');
    let tamanho = caracter.length;

    if (tamanho <= 10) {
        caracter = caracter.replace(/(\d{2})(\d)/, "($1) $2");
        caracter = caracter.replace(/(\d{4})(\d)/, "$1-$2");
        return document.getElementById(id).value = caracter
    } else {
        caracter = caracter.replace(/(\d{2})(\d)/, "($1) $2");
        caracter = caracter.replace(/(\d{5})(\d)/, "$1-$2");
        return document.getElementById(id).value = caracter.substring(0, 15)
    }
}

function validateCpfCnpj(val) {
    var cpf = val.trim();
    cpf = cpf.replace(/\./g, '');
    cpf = cpf.replace('-', '');
    cpf = cpf.replace('/', '');

    if (cpf.length == 11) {

        cpf = cpf.split('');

        var v1 = 0;
        var v2 = 0;
        var aux = false;

        for (var i = 1; cpf.length > i; i++) {
            if (cpf[i - 1] != cpf[i]) {
                aux = true;
            }
        }

        if (aux == false) {
            return false;
        }

        for (var i = 0, p = 10;
             (cpf.length - 2) > i; i++, p--) {
            v1 += cpf[i] * p;
        }

        v1 = ((v1 * 10) % 11);

        if (v1 == 10) {
            v1 = 0;
        }

        if (v1 != cpf[9]) {
            return false;
        }

        for (var i = 0, p = 11;
             (cpf.length - 1) > i; i++, p--) {
            v2 += cpf[i] * p;
        }

        v2 = ((v2 * 10) % 11);

        if (v2 == 10) {
            v2 = 0;
        }

        if (v2 != cpf[10]) {
            return false;
        } else {
            return true;
        }
    } else if (cpf.length == 14) {
        var cnpj = val.trim();

        cnpj = cnpj.replace(/\./g, '');
        cnpj = cnpj.replace('-', '');
        cnpj = cnpj.replace('/', '');
        cnpj = cnpj.split('');

        var v1 = 0;
        var v2 = 0;
        var aux = false;

        for (var i = 1; cnpj.length > i; i++) {
            if (cnpj[i - 1] != cnpj[i]) {
                aux = true;
            }
        }

        if (aux == false) {
            return false;
        }

        for (var i = 0, p1 = 5, p2 = 13;
             (cnpj.length - 2) > i; i++, p1--, p2--) {
            if (p1 >= 2) {
                v1 += cnpj[i] * p1;
            } else {
                v1 += cnpj[i] * p2;
            }
        }

        v1 = (v1 % 11);

        if (v1 < 2) {
            v1 = 0;
        } else {
            v1 = (11 - v1);
        }

        if (v1 != cnpj[12]) {
            return false;
        }

        for (var i = 0, p1 = 6, p2 = 14;
             (cnpj.length - 1) > i; i++, p1--, p2--) {
            if (p1 >= 2) {
                v2 += cnpj[i] * p1;
            } else {
                v2 += cnpj[i] * p2;
            }
        }

        v2 = (v2 % 11);

        if (v2 < 2) {
            v2 = 0;
        } else {
            v2 = (11 - v2);
        }

        if (v2 != cnpj[13]) {
            return false;
        } else {
            return true;
        }
    } else {
        return false;
    }
}

function addClass(value, id) {
    if (value > 0) {
        return document.getElementById(id).classList.add('password');
    } else {
        return document.getElementById(id).classList.remove('password');
    }
}

function showLoading(form) {
    $('.loaderTop, .spinner-card').show();
    if (form) {
        $('#' + form + ' .form-group').css('opacity', .2);
    } else {
        $('.form-group, .card').css('opacity', .2);
    }
    $('.btn').attr('disabled', true);
}

function closeLoading() {
    setTimeout(function () {
        $('.loaderTop, .spinner-card').hide();
        $('.form-group, .card').css('opacity', 1);
        $('.btn').attr('disabled', false);
    }, 800)
}

function getCityOption(city) {
    return `<option value="${city.id}">${city.name}</option>`;
}

function getCities(state, div) {
    let element = div ? div : 'city';
    fetch(`${baseUrl}api/sistema/cidades/${state}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            let cities = json.message;
            let options;
            document.getElementById(element).innerHTML = '<option value="">Selecione a cidade</option>';
            options = '<option value="">Selecione a cidade</option>' + cities.map(getCityOption);
            document.getElementById(element).innerHTML = options;
            if (city > 0) {
                document.getElementById(element).value = city;
                city = 0;
            }
        });
    });
}

function getCitiesWhitValue(state, div, city) {
    let element = div ? div : 'city';
    fetch(`${baseUrl}api/sistema/cidades/${state}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            let cities = json.message;
            let options;
            document.getElementById(element).innerHTML = '<option value="">Selecione a cidade</option>';
            options = '<option value="">Selecione a cidade</option>' + cities.map(getCityOption);
            document.getElementById(element).innerHTML = options;
            if (city > 0) {
                document.getElementById(element).value = city;
                city = 0;
            }
        });
    });
}

function getCitiesAdress(state, div) {
    let element = div ? div : 'city2';
    fetch(`${baseUrl}api/sistema/cidades/${state}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            let cities = json.message;
            let options;
            document.getElementById(element).innerHTML = '<option value="">Selecione a cidade</option>';
            options = '<option value="">Selecione a cidade</option>' + cities.map(getCityOption);
            document.getElementById(element).innerHTML = options;
            if (city > 0) {
                document.getElementById(element).value = city;
                city = 0;
            }
        });
    });
}

function inputFile() {
    $('input:file').change(function (e) {
        let input = e.target;
        if (input.files.length > 0) {
            input.parentElement.classList.remove('btn-primary');
            input.parentElement.classList.remove('btn-danger');
            input.parentElement.classList.add('btn-success');
            input.text = input.value;
            let string = input.value.replace('fakepath', '');

            let labelId = input.getAttribute('id') + 'Label';
            document.getElementById(labelId).textContent = string.slice(4);
        } else {
            input.parentElement.classList.remove('btn-success');
            input.parentElement.classList.add('btn-danger');
            let labelId = input.getAttribute('id') + 'Label';
            document.getElementById(labelId).textContent = document.getElementById(labelId).getAttribute('default');
        }
    });
}

function maskCpf(cpf) {
    return cpf.replace(/^(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4");
}

function formatDate(date) {
    if (date != null) {
        const day = date.date;

        let array = day.split(' ');
        let days = array[0].split('-');

        return `${days[2]}/${days[1]}/${days[0]}`;
    }
}

function formatHour(date) {
    if (date != null) {
        const day = date.date;

        let array = day.split(' ');
        array = array[1].split('.')

        return `${array[0]}`;
    }
}
