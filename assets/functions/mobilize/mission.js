const filter = document.getElementById('filter');
filter.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    setStorage('Mission', {
        'url': 'missoes/json',
        'columns': 7,
        'variables': {
            'index': 0,
            'title': formData.title,
            'completed': formData.completed,
            'total': 1,
            'limit': $('#limit').val()
        }
    })
});

const formAdd = document.getElementById('form');
formAdd.addEventListener('submit', e => {
    e.preventDefault();
    showLoading();
    if (!ValidateForm('form')) {
        closeLoading();
        return;
    }
    fetch(baseUrl + "missoes/indicar-realizacao", {
        method: 'POST',
        credentials: 'same-origin',
        body: new FormData(formAdd)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                formAdd.reset();
                resetTable();
                $('#modal').modal('hide');
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

function resetTable() {
    filter.reset();
    setStorageIndex();
}

function generateLines(item) {
    let actions = '';
    if (item.status == 3) {
        actions = `<i class="fa fa-check-circle text-success px-1" title="Missão Concluída"></i>`;
    } else if (item.status == 2) {
        actions = `<i class="fa fa-times-circle text-danger px-1" title="Missão Cancelada"></i>`;
    }  else if (item.status == 1) {
        actions = `<i class="fa fa-clock-o text-muted px-1" title="Aguardando Aprovação"></i>`;
    } else if (item.data.type === false) {
        actions = `<i class="fa fa-check-square text-info px-1" title="Indicar Realização" onclick="openModal(${item.data.id})"></i>`;
    }
    actions = `<td class="text-center">${actions} </td>`;


    return `<tr class="middle" id="line${item.data.id}">
                    <td class="text-center">${item.data.id}</td>                    
                    <td class="text-center">${item.data.title}</td>                    
                    <td class="text-center">${item.data.points}</td>                    
                    ${actions}
                </tr>`;
}

function openModal(id) {
    document.getElementById('form').reset();
    document.getElementById('mission').value = id;
    $('#modal').modal('show');
}

$(document).ready(function () {
    verifySession('Mission');
});

function setStorageIndex() {
    setStorage('Mission', {
        'url': 'missoes/json',
        'columns': 7,
        'variables': {
            'index': 0,
            'title': '',
            'completed': '',
            'total': 1,
            'limit': $('#limit').val()
        }
    });
};
