function openModal(id, type) {
    document.getElementById('form').reset();
    setType(1, 'addressUser');
    if (id > 0) {
        if (type == 3) {
            $('#rowAddress').show();
            $('#rowAlert').hide();
        } else {
            $('#rowAlert').show();
            $('#rowAddress').hide();
        }
        $('#benefit').val(id);
        $('#type_delivery').val(type);
        $('#modal').modal('show');
    }
}

function setType(value, id) {
    $('.card-address').removeClass('active');
    $('#' + id).addClass('active');

    $('#addressType').val(value);

    if (value === 1) {
        $('#form input').attr('required', false);
        $('#rowForm').hide();
    } else {
        $('#form input').attr('required', true);
        $('#rowForm').show();
    }
}

function loadAddress() {
    var cep = $('#zipCode').val().replace(/\D/g, '');
    var validacep = /^[0-9]{8}$/;
    if (validacep.test(cep)) {
        $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {
            if (!("erro" in dados)) {
                $("#address").val(dados.logradouro);
                $("#neighborhood").val(dados.bairro);
                $("#city").val(dados.localidade);
                $("#uf").val(dados.uf);
                $("#complement").val(dados.complemento);
            }
        });
    }
}

const formAdd = document.getElementById('form');
formAdd.addEventListener('submit', e => {
    e.preventDefault();
    showLoading();
    if (!ValidateForm('form')) {
        closeLoading();
        return;
    }
    fetch(baseUrl + "novo-mobiliza/beneficios/solicitar", {
        method: 'POST',
        credentials: 'same-origin',
        body: new FormData(formAdd)
    }).then(response => {
        response.json().then(json => {
            if (response.status === 201) {
                $('#modal').modal('hide');
                showNotify('success', json.message, 1500);
                setTimeout(() => {
                    closeLoading();
                    window.location.reload();
                }, 1000)
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

function submit(id) {
    if (id > 0) {
        fetch(`${baseUrl}novo-mobiliza/beneficios/solicitar/${id}`, {
            method: 'GET',
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then(response => {
            response.json().then(json => {
                if (response.status === 200) {
                    showNotify('success', json.message, 1500);
                    setTimeout(() => {
                        window.location.reload();
                    }, 1200)
                } else {
                    showNotify('danger', json.message, 1500);
                }
            });
        });
    }
}
