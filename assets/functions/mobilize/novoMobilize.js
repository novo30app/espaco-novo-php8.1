var linkAccess = '';

function showModal(id) {
    document.getElementById('form').reset();
    let input = document.getElementById('doc');
    input.parentElement.classList.remove('btn-success');
    input.parentElement.classList.add('btn-danger');
    let labelId = input.getAttribute('id') + 'Label';
    document.getElementById(labelId).textContent = document.getElementById(labelId).getAttribute('default');
    $('.modal-footer').removeClass('d-none')
    $('#btn-link, #add-doc, #check-mission').addClass('d-none');
    $('#doc').prop('required', false);

    fetch(baseUrl + "missoes/" + id, {
        method: 'GET',
        credentials: 'same-origin',
    }).then(response => {
        response.json().then(json => {
            if (response.status === 200) {
                json = json.message

                document.getElementById('mission').value = id;
                document.getElementById('title-mission').innerText = json.title;
                document.getElementById('points-mission').innerText = json.points;
                document.getElementById('description-mission').innerHTML = json.description;

                if (json.link) {
                    $('#btn-link').removeClass('d-none');
                    $('.modal-footer').addClass('d-none')
                    linkAccess = json.link;
                } else {
                    $('#doc').prop('required', true);
                    $('#add-doc').removeClass('d-none');
                }

                if (id == 4) {
                    $('#check-mission').removeClass('d-none');
                    $('#btn-link, #add-doc').addClass('d-none');
                }

                $('#modal').modal('show');
            }
        });
    });
}

function playVideo() {
    var iframe = document.getElementById('youtube-iframe');
    var url = iframe.src;

    if (url.indexOf('autoplay=1') === -1) {
        url += (url.indexOf('?') === -1 ? '?' : '&') + 'autoplay=1';
    }

    iframe.src = url;

    const icon = document.getElementById('icon-play');

    iframe.style.opacity = '1';
    iframe.style.visibility = 'visible';

    icon.style.opacity = '0';
    icon.style.visibility = 'hidden';
}

function redirect() {
    window.open(linkAccess, '_blank');
    $('#modal').modal('hide');
}

function getPoints() {
    fetch(baseUrl + "novo-mobiliza/pontos", {
        method: 'GET',
        credentials: 'same-origin',
    }).then(response => {
        response.json().then(json => {
            if (response.status === 200) {
                document.getElementById('user-points').innerText = json.points;

                let html = '';

                if (json.missions.length > 0) {
                    json.missions.forEach(mission => {
                        html += `
                        <div class="badge badge-secondary mb-1" id="badge-mission-${mission.id}"
                                     onclick="showModal('${mission.id}')">
                            <div class="text-white">
                                <div class="row">
                                    <div class="col-1 my-auto">
                                        <div class="rounded-circle circle-icon badge-circle-missions">
                                            <i class="fa fa-play text-white"></i>
                                        </div>
                                    </div>
                                    <div class="col-8 text-center my-auto">
                                        <h6 class="space my-auto">${mission.title}</h6>
                                    </div>
                                    <div class="col-3 my-auto text-end">
                                        <h6 class="my-auto m-l-10 space">${mission.points} pontos</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    `;
                    })
                }

                if (json.missionWeek) {
                    $('#missionWeekCard').prop('onclick', null).off('click').click(function () {
                        showModal(json.missionWeek.id);
                    });
                } else {
                    $('#missionWeek').empty();
                }

                if (!json.missionWeek && json.missions.length === 0) {
                    html = `<h6>No momento você não possui missões disponíveis</h6>`
                }

                document.getElementById('list-badge-missions').innerHTML = html;

            }
        });
    });
}

$(document).ready(() => {
    getPoints();
    setInterval(getPoints, 10000);
    inputFile();
})

const formAdd = document.getElementById('form');
formAdd.addEventListener('submit', e => {
    e.preventDefault();
    showLoading();
    if (!ValidateForm('form')) {
        closeLoading();
        return;
    }
    fetch(baseUrl + "missoes/indicar-realizacao", {
        method: 'POST',
        credentials: 'same-origin',
        body: new FormData(formAdd)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                const id = document.getElementById('mission').value;
                if (document.getElementById('badge-mission-' + id)) document.getElementById('badge-mission-' + id).style.display = 'none';
                if (document.getElementById('user-points')) document.getElementById('user-points').innerText = json.points;
                getPoints();
                $('#modal').modal('hide');
                formAdd.reset();
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});
