function delElement(url, element, id, local) {
    $('#confirmDelete').prop('onclick', null).off('click').click(function () {
        confirmDel(url, id, local);
    });
    $('#elementDelete').empty().text(element);
    $('#modalDelete').modal('show');
}

function confirmDel(url, id, local) {
    fetch(baseUrl + url + "/excluir/" + id, {
        method: 'delete',
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'X-Auth': token
        },
    }).then(response => {
        endLoading();
        response.json().then(json => {
            if (response.status === 201) {
                $('#modalDelete').modal('hide');
                showNotify('success', json.message, 1500);
                setTimeout(function () {
                    $('#table tbody').empty();
                    generateTable(local)
                }, 500)
            } else if (response.status === 403) {
                window.location.href = `${baseUrl}logout`;
                return;
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
}