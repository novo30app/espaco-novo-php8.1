var index = 0;
var partial = 0;
var total = 1;
var agent = 0;
var state = 0;
var city = 0;
var year = '';

const filter = document.getElementById('filter');
filter.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    partial = 0;
    index = 0;
    total = 1;
    agent = formData.agent;
    state = formData.state;
    city = formData.city;
    year = formData.year;
    $("#spendingTable tbody").empty();
    generateTable();
    graphicSpending();
});

function resetGraphicsData() {
    var index = 0;
    var partial = 0;
    var total = 1;
    agent = 0;
    state = 0;
    city = 0;
    year = '';
    $("#spendingTable tbody").empty();
    generateTable();
    graphicSpending();
}

var spendingChart = new Chart(document.getElementById("spendingChart"), {
    type: 'bar', // or doughnut
    options: {
        locale: 'en-US',
        responsive: true,
        maintainAspectRatio: false,
        barValueSpacing: 20,
        // scales: {
        //     yAxes: [{
        //         ticks: {
        //             "min": 0,
        //         }
        //     }]
        // }
    }
});

const setIntl = (intl) => {
    spendingChart.options.locale = intl;
    spendingChart.update();
}

function graphicSpending() {
    fetch(`${baseUrl}api/dam/gastos/?agent=${agent}&state=${state}&city=${city}&year=${year}`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            $('#totalValue').text('R$ ' + json.valueSaved);
            $('#yearSpan').text(json.year);
            spendingChart.data = {
                    "labels": json.month,
                    "datasets": [{
                        "label": "Material de papelaria",
                        "data": json.value1,
                        "backgroundColor": '#ef5a23',
                    }, {
                        "label": "Passagens / Locomoção / Combustível / Aluguel de veículos",
                        "data": json.value2,
                        "backgroundColor": '#1a2948',
                    }, {
                        "label": "Assinatura de notícias e periódicos",
                        "data": json.value3,
                        "backgroundColor": '#fdc908',
                    }, {
                        "label": "Gasto com pessoal / Estagiário/ Consultoria",
                        "data": json.value4,
                        "backgroundColor": '#00BFFF',
                    }, {
                        "label": "Aluguel software / Material de comunicação",
                        "data": json.value5,
                        "backgroundColor": '#00FF00',
                    }, {
                        "label": "Outros",
                        "data": json.value6,
                        "backgroundColor": '#FF0000',
                    }]
                },
                spendingChart.update();
        });
    });
}

function generateSpendingshipList(json) {
    return `
        <tr class="middle">
            <td>${json.type}</td>
            <td class="text-center">${json.cabinet}</td>
            <td class="text-center">${json.category}</td>
            <td class="text-center">R$ ${json.value}</td>
            <td class="text-center">R$ ${json.valueSave}</td>
            <td class="text-center">${json.justify}</td>
            <td class="text-center">${json.dueDate}</td>
        </tr>
    `;
}

function generateTable() {
    $('#loader').show();
    fetch(`${baseUrl}api/dam/tabela-gastos/?index=${index}&agent=${agent}&state=${state}&city=${city}&year=${year}`, {
        method: 'GET',
        credentials: "same-origin",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            $('#loader').hide();
            total = json.total;
            partial = json.partial;
            let spendingshipList = json.message;
            let options = spendingshipList.map(generateSpendingshipList);
            $("#spendingTable tbody").append(options);
            $('#total').text(total);
            $('#partial').text(partial);
        });
    });
}

$(document).ready(function() {
    setIntl('en-US')
    graphicSpending();
    generateTable();
    $(window).scroll(function() {
        if ($(window).scrollTop() + $(window).height() >= $(document).height() - 1) {
            index++;
            generateTable();
        }
    });
});

function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

function getCityOptionByUf(city) {
    return `<option value="${city.city}">${city.city}</option>`;
}

function getCitiesByState(state, div) {
    $("#agent").val('');
    let element = div ? div : 'city';
    fetch(`https://dam.novo.org.br/api/adminNovo/gastos-por-estado/${state}/`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            let cities = json.message;
            let options;
            document.getElementById(element).innerHTML = '<option value="">Selecione a cidade</option>';
            options = '<option value="">Selecione a cidade</option>' + cities.map(getCityOptionByUf);
            document.getElementById(element).innerHTML = options;
        });
    });
}

function getCabinetByCityOptionBy(cabinet) {
    return `<option value="${cabinet.id}">${cabinet.cabinet}</option>`;
}

function getCabinetByCity(city) {
    let element = 'agent';
    var state = document.getElementById('state').value;
    fetch(`https://dam.novo.org.br/api/adminNovo/gastos-por-cidade/${state}/${city}/`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            let cabinets = json.message;
            let options;
            document.getElementById(element).innerHTML = '<option value="">Selecione o gabinete</option>';
            options = '<option value="">Selecione o gabinete</option>' + cabinets.map(getCabinetByCityOptionBy);
            document.getElementById(element).innerHTML = options;
        });
    });
}

function getYearSByCabinetOption(year) {
    return `<option value="${year.year}">${year.year}</option>`;
}

function getYearsByCabinet(cabinet) {
    let element = 'year';
    fetch(`https://dam.novo.org.br/api/adminNovo/gastos-por-ano/${cabinet}/`, {
        method: "GET",
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(response => {
        response.json().then(json => {
            let years = json.message;
            let options;
            document.getElementById(element).innerHTML = '<option value="">Selecione o ano</option>';
            options = '<option value="">Selecione o ano</option>' + years.map(getYearSByCabinetOption);
            document.getElementById(element).innerHTML = options;
        });
    });
}