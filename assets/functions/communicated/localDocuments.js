var index = partial = 0;
var total = 1;
var keyword = '';

const form = document.getElementById('filter');
form.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    index = partial = 0;
    total = 1;
    keyword = formData.keyword;
    $("#table tbody").empty();
    generateTable();
});

function resetTable() {
    index = partial = 0;
    total = 1;
    keyword = "";
    $("#table tbody").empty();
    generateTable();
};

function formatAttachment(attachment) {
    attachment = attachment.split("uploads/");
    return 'https://gestao.novo.org.br/' + `uploads/${attachment[1]}`;
}

function generateList(documents) {
    var file = `<a href="${formatAttachment(documents.file)}" target="_blank"><i class="fa fa-download" aria-hidden="true" style="font-size: x-large;"></i> Download</a>`;
    return `<tr class="middle">
				<td class="text-center">${documents.created}</td>
				<td class="text-center">${documents.title}</td>
                <td class="text-center">${documents.description}</td>
                <td class="text-center">${file}</td>
    		</tr>`;
}

function generateTable() {
    if (total > partial) {
        $('#loader').show();
        fetch(`${baseUrl}comunicados/comunicados-listagem/?keyword=${keyword}`, {
            method: "GET",
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(response => {
            response.json().then(json => {
                $('#loader').hide();
                total = json.total;
                partial = json.partial;
                let membershipList = json.message;
                let options = membershipList.map(generateList);
                $("#table tbody").append(options);
                $('#total').text(total);
                $('#partial').text(partial);
            });
        });
    }
}

$(document).ready(function () {
    generateTable();
    $(window).scroll(function() {
        if ($(window).scrollTop() + $(window).height() >= $(document).height() - 1) {
            index++;
            generateTable();
        }
    });
});