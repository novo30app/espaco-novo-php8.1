<?php

namespace App\Controllers;
use App\Helpers\Validator;
use App\Helpers\Messages;
use App\Helpers\Utils;
use App\Exceptions\FoundException;
use App\Models\Entities\Events;
use App\Models\Entities\PersonIugu;
use App\Models\Entities\User;
use App\Models\Entities\State;
use App\Models\Entities\City;
use App\Models\Entities\Address;
use App\Models\Entities\EventsParticipants;
use App\Models\Entities\EventsTransactions;
use App\Models\Entities\EventsPrices;
use App\Models\Entities\EventsCard;
use App\Models\Entities\EventsCoupons;
use App\Models\Entities\Transaction;
use App\Models\Entities\PersonRede;
use App\Models\Entities\PersonCreditCard;
use App\Models\Entities\DirectoryGatway;
use App\Models\Entities\CivilState;
use App\Models\Entities\Gender;
use App\Models\Entities\Schooling;
use App\Models\Entities\Phone;
use App\Models\Entities\vaccineVoucher;
use App\Models\Entities\CommunicationConsent;
use App\Models\Entities\EventsPress;
use App\Services\Email;
use App\Services\IuguService;
use App\Services\MaxiPago;
use App\Services\Hubspot;
use App\Services\Pdf;
use App\Services\GoogleService;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

class EventsController extends Controller
{    
    public function myEvents(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $tickets = $this->em->getRepository(EventsParticipants::class)->tickets($user->getId());
        return $this->renderer->render($response, 'default.phtml', ['page' => 'events/my-events.phtml', 'section' => 'myEvents', 'user' => $user, 'tickets' => $tickets, 
            'title' => 'Meus Eventos', 'subMenu' => 'myEvents']);
    }

    public function myTickets(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $id = $request->getAttribute('route')->getArgument('id');
        $ticket = $this->em->getRepository(EventsParticipants::class)->tickets($user->getId(), $id)[0];
        if($ticket['user'] != $user->getId()) throw new \Exception('Atenção, você não tem permissão para acessar este conteúdo!');
        Pdf::ticket($ticket);
    }

    public function subscriptionOnline(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            GoogleService::recapchaNovo($data['g-recaptcha-response']);
            $fields = [
                'eventId' => 'Evento',
                'name' => 'Nome',
                'email' => 'E-mail',
                'phone' => 'Telefone',
                'state' => 'Estado',
                'city' => 'Cidade'
            ];
            Validator::requireValidator($fields, $data);
            $event = $this->em->getRepository(Events::class)->findOneBy(['id' =>  $data['eventId']]);
            $participatsEvent = count($this->em->getRepository(EventsParticipants::class)->findBy(['tbEventosId' => $data['eventId']]));
            if($participatsEvent >= $event->getLimitSubscribers()) throw new \Exception('Atenção, o evento alcançou o limite de participantes permitido!');
            $user = $this->em->getRepository(User::class)->findOneBy(['email' => $data['email']]);
            if($event->getAffiliatedOnly() == 1) if(!$user || !$user->isAffiliated()) throw new \Exception('Evento exclusivo para filiados!');
            $participant = $this->em->getRepository(EventsParticipants::class)->findBy(['tbEventosId' => $data['eventId'], 'email' => $data['email']]);
            if($participant) throw new \Exception('Você já se inscreveu para este evento!');
            $state = $this->em->getRepository(State::class)->findOneBy(['id' => $data['state']]);
            $city = $this->em->getRepository(City::class)->findOneBy(['cidade' => $data['city']]);
            $participantsData = new EventsParticipants();
            $participantsData->setTbEventsId($data['eventId'])
                                ->setTbEventsPricesId(0)
                                ->setTbEventsTransactionId(0)
                                ->setName($data['name'])
                                ->setEmail($data['email'])
                                ->setPhone($data['phone'])
                                ->setStateId($data['state'])
                                ->setState($state->getEstado())
                                ->setCityId($city->getId())
                                ->setCity($city->getCidade())
                                ->setCode(strtoupper(substr(bin2hex(random_bytes(4)), 1)))
                                ->setValue('0.00')
                                ->setStatus('confirmado')
                                ->setDateCreated(new \Datetime())
                                ->setRecommendation($data['recommendation'] ?: null);
            $this->em->getRepository(EventsParticipants::class)->save($participantsData);
            $firstName = explode(' ', $data['name'])[0];
            $msg = Messages::subscriptionOnline($data, $firstName, $event);
            $subject = "Inscrição Realizada com sucesso";
            if($event->getId() == 6311) {
                $msg = Messages::subscriptionOnlineEvent6311($firstName);
                $subject = "Parabéns! Você se inscreveu no Encontro Online das Protagonistas pelo Brasil!";
            } 
            Email::send($data['email'], $data['name'], $subject, $msg);
            $valid = false;
            if(isset($data['term'])) {
                $valid = true;
            } 
            //register consent
            $consent = new CommunicationConsent();
            $consent->setUser($participantsData->getId())->setType(4)->setValid($valid)->setCreated_at(new \Datetime());
            $this->em->getRepository(CommunicationConsent::class)->save($consent);
            // registra quem indicou
            if($data['codigo'] && $user) $this->setWhoIndicate($user, $data['codigo'], 4, $event->getId());
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => "O NOVO agradece sua participação,<br> 
                                sua inscrição foi realizada com sucesso e um email foi enviado para sua caixa de entrada!"
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function eventsLogin(Request $request, Response $response) 
    {
        try{
            $data = (array)$request->getParams();
            if(!$data['dev']) GoogleService::recapchaNovo($data['g-recaptcha-response']);
            $fields = [
                'email2' => 'E-mail',            
                'password' => 'Senha',
            ];
            Validator::requireValidator($fields, $data);
            $usersRepository = $this->em->getRepository(User::class);
            $user = $usersRepository->login($data['email2'], $data['password']);
            if(!$user) throw new \Exception('Login ou senha inválidos!');
            return $response->withJson([
                'status' => 'ok',
                'message' => "Login realizado com sucesso, aguarde uns instantes para ser redirecionado."
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function subscriptionLogin(Request $request, Response $response) 
    {
        try{
            $data = (array)$request->getParams();
            GoogleService::recapchaNovo($data['g-recaptcha-response']);
            $fields = [
                'email' => 'E-mail',            
                'password' => 'Senha',
            ];
            Validator::requireValidator($fields, $data);
            if(array_sum($data['qtde']) == 0) throw new \Exception('Atenção, selecione pelo menos um ingresso!');
            $event = $this->em->getRepository(Events::class)->findOneBy(['id' =>  $data['eventId']]);
            $participatsEvent = count($this->em->getRepository(EventsParticipants::class)->findBy(['tbEventosId' => $data['eventId'], 'status' => 'confirmado']));
            if($participatsEvent >= $event->getLimitSubscribers()) throw new \Exception('Atenção, o evento alcançou o limite de participantes permitido!');
            $email = $data['email'];
            $usersRepository = $this->em->getRepository(User::class);
            $user = $usersRepository->login($data['email'], $data['password']);
            if(!$user) throw new \Exception('Login ou senha inválidos!');
            $total = $qtde = null;
            $tickets = array_map(null, $data["qtde"], $data["idTicket"]);
            foreach($tickets as $ticket) {
                $ticketValue = $this->em->getRepository(EventsPrices::class)->findOneBy(['id' =>  $ticket[1]]);
                $qtdeTicketConfirmed = count($this->em->getRepository(EventsParticipants::class)->findBy(['tbEventosId' => $data['eventId'], 'status' => 'confirmado', 'tbEventosPrecosId' => $ticketValue->getId()]));
                $value = $ticket[0] * $ticketValue->getValue();
                $qtde += $ticket[0];
                $total += $value;
                if(intVal($ticket[0]) >= 1){
                    if($ticketValue->getOnlyAffiliated() == 1 && !$user->isAffiliated()) throw new \Exception('Você selecionou um ingresso restrito para filiados,<br> retorne e escolha outro ingresso!');
                    if($qtdeTicketConfirmed >= $ticketValue->getLimitTotal()) throw new \Exception('Você selecionou um ingresso esgotado, volte e selecione outro ingresso!');
                    $userTickets = count($this->em->getRepository(EventsParticipants::class)->findBy(['tbPessoaId' => $user->getId(), 'tbEventosPrecosId' => $ticket[1]]));
                    if($userTickets >= $ticketValue->getLimitParticipants()) throw new \Exception('Você já adquiriu o máximo de ingressos nesta faixa de preço, selecione outro ingresso!');
                    $sumTicketsByUser = $ticket[0] + $userTickets;
                    if($sumTicketsByUser > $ticketValue->getLimitParticipants()) throw new \Exception('Você já adquiriu o máximo de ingressos nesta faixa de preço!');
                }
            }
            if($total > MAXVALUE) throw new \Exception("Valor máximo para a compra de ingressos é R$ " . number_format(MAXVALUE, 2, ',', '.') . "!"); 
            if($qtde == 0) throw new \Exception('Atenção, selecione pelo menos um ingresso!');
            if($participatsEvent >= $event->getLimitSubscribers()) throw new \Exception('Atenção, o evento alcançou o limite de participantes permitido!');
            if($event->getAffiliatedOnly() == 1) {
                if(!$user->isAffiliated()) throw new \Exception('Evento exclusivo para filiados, se for filiado utilize as mesmas credenciais do Espaço NOVO!');
            }
            $message = "Login realizado com sucesso!";
            if($total == 0) {
                $this->insertTableParticipantes($data, $user, $tickets, $total, 0, 'confirmado');
                $msg = Messages::subscriptionLogin($data, $user, $event);
                if($event->getSpecie() == 3) $msg = Messages::subscriptionOnline($data, $user->getFirstName(), $event);
                Email::send($email, $user->getFirstName(), 'Inscrição Realizada com sucesso', $msg);
                $message = "O NOVO agradece sua participação,<br> sua inscrição foi realizada com sucesso e um email foi enviado para sua caixa de entrada!";
            }
            // registra quem indicou
            if($data['codigo']) $this->setWhoIndicate($user, $data['codigo'], 4, $event->getId());
            return $response->withJson([
                'status' => 'ok',
                'pessoaId' => $user->getId(),
                'total' => $total,
                'message' => $message,
                'qtde2' => $data["qtde"],
                'idTicket2' => $data["idTicket"],
                'eventId' => $data["eventId"],
                'qtde' => $qtde
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function insertCard($user, $data, $eventTransaction = null, $status = null, $tickets) 
    {
        foreach ($tickets as $ticket) {
            $price = $this->em->getRepository(EventsPrices::class)->findOneBy(['id' => $ticket[1]]);
            for($i = 1; $i <= $ticket[0]; $i++) {
                $participantsData = new EventsCard();
                $participantsData->setTbPessoaId($user->getId())
                                ->setTbEventsTransactionId($eventTransaction)
                                ->setTbEventsId($data['eventId'])
                                ->setTbEventsPricesId($ticket[1])
                                ->setQuantity($ticket[0])
                                ->setValue($price->getValue())
                                ->setStatus($status)
                                ->setDateCreated(new \Datetime(date('Y-m-d H:i:s')))
                                ->setRefresh();
                $this->em->getRepository(EventsCard::class)->save($participantsData);   
            }
        }  
    }

    private function useCoupon(array $data, float $total, User $user, int $qtde, Events $event, Response $response)
    {
        $coupon = $this->em->getRepository(EventsCoupons::class)->findOneBy(['code' => $data['couponHidden'], 'event' => $data['eventId']]);
        if(!$coupon) throw new \Exception("Cupom inválido!");
        $countCuponsUseds = $this->em->getRepository(EventsTransactions::class)->findBy(['coupon' => $coupon]);
        if(count($countCuponsUseds) >= $coupon->getMax()) throw new \Exception("Este cupom não pode mais ser usado!");
        $checkUsePerPerson = $this->em->getRepository(EventsTransactions::class)->findBy(['coupon' => $coupon, 'tbPessoaId' => $user->getId()]);
        if(count($checkUsePerPerson) >= $coupon->getMaxPerPerson()) throw new \Exception("Você já utilizou este cupom o máximo de vezes permitido");
        if($qtde < $coupon->getMinPerPerson()) throw new \Exception("Para utilizar este cupom você precisa escolher no mínimo {$coupon->getMinPerPerson()} ingressos");
        if($qtde > $coupon->getMaxPerPerson()) throw new \Exception("Para utilizar este cupom você precisa escolher no máximo {$coupon->getMaxPerPerson()} ingressos");
        if($coupon->getType() == 1) {
            $total = $total - (floor($total * $coupon->getPorcent()) / 100);
        } else {
            $total = $total - $coupon->getValue();
        }
        return $total;
    }

    public function eventsPayment(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $date = new \DateTime();
            $data = (array)$request->getParams();
            $qtde = explode(",", $data["qtde2"]);
            $idTicket = explode(",", $data["idTicket2"]);
            $tickets = array_map(null, $qtde, $idTicket);
            $event = $this->em->getRepository(Events::class)->findOneBy(['id' =>  $data['eventId']]);
            $type = in_array($event->getSpecie(), array(1,6)) ? 'doação' : 'inscrição';
            $user = $this->em->getRepository(User::class)->findOneBy(['id' => $data['pessoaId']]);
            $address = $this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()]);
            $address = $address ? $this->iuguAddress($address) : $this->iuguAddressDonation();
            $total = $qtde = 0;
            foreach($tickets as $ticket) {
                $ticketValue = $this->em->getRepository(EventsPrices::class)->findOneBy(['id' =>  $ticket[1]]);
                $value = $ticket[0] * $ticketValue->getValue();
                $total += $value;
                $qtde += $ticket[0];
            }
            if($data['couponHidden']) $total = $this->useCoupon($data, $total, $user, $qtde, $event, $response);
            if($total == 0.00) {
                $eventTransaction = $this->insertTableTransactions($total, $data, $user, 0, 'confirmado');
                $this->insertTableParticipantes($data, $user, $tickets, $total, $eventTransaction, 'confirmado');  
                $firstName = explode(' ', $user->getName())[0];
                $msg = Messages::subscriptionOnline($data, $firstName, $event);
                Email::send($user->getEmail(), $user->getName(), 'Inscrição Realizada com sucesso', $msg);
                $this->em->commit();
                return $response->withJson([
                    'status' => 'ok',
                    'message' => 'O NOVO agradece sua participação,<br> 
                                sua inscrição foi realizada com sucesso e um email foi enviado para sua caixa de entrada!',
                    'user' => $user->getId(),
                    'event' => $event->getId(),
                ], 201)
                    ->withHeader('Content-type', 'application/json');
            }
            $fields = ['paymentMethod' => 'Forma de pagamento'];
            Validator::requireValidator($fields, $data);
            $ticket = $type == 'inscrição' ? "Após a confirmação do pagamento seus ingressos serão disponibilizados em seu Espaço NOVO.<br><br>" : "";
            $keyIUGU = $this->getIuguTokenDirectory($event->getDiretorioId());
            if($data['paymentMethod'] == 1) { //cartão.
                if(!$data['cardId']){
                    $fields = [
                        'cardNumber'    => 'Número do cartão',
                        'cvv'           => 'Código de verificação',
                        'expiringDate'  => 'Validade do cartão',
                        'cardName' => 'Nome como está no cartão',
                        'international' => 'Cartão emitido no Brasil',
                    ];
                    Validator::requireValidator($fields, $data);
                    $data['flag'] ??= '';
                    /** Save card IUGU */
                    $card = $this->createCardApiIugu($user, $data['cardNumber'], $data['cardName'], $data['cvv'], $data['expiringDate'], $data['flag'], $data['international']);
                    $cardToken = $card['paymentToken'];
                    $gateway = 1;
                } else {
                    $card = $this->em->getRepository(PersonCreditCard::class)->findOneBy(['id' => $data['cardId']]);
                    $cardToken = $card->getTokenCartaoCredito();
                    $gateway = $card->getGatewayPagamento();
                }
                /** Cobrança */
                if ($gateway == 1) {
                    $personIugu = $this->getCustomerIdIUGU($user);
                    $charge = IuguService::charge($personIugu->getCustomerId(), $cardToken, $user->getEmail(), $user->getCpf(), $user->getName(), $address, $total, $event->getCategoryString() . ' - ' . $event->getName(), 0, $keyIUGU['key']);
                    $invoiceId = $charge['invoice_id'];
                    $directory = $keyIUGU['deposit']; //DN
                    $customerId = $personIugu->getCustomerId();
                    $owner = $keyIUGU['owner'];
                } else {
                    $personRede = $this->em->getRepository(PersonRede::class)->findOneBy(['user' => $user->getId()], ['id' => 'DESC']);
                    if (!$personRede) {
                        $redeId = MaxiPago::createCustomer($user);
                        $personRede = new PersonRede();
                        $personRede->setUser($user)->setCustomerId($redeId);
                        $this->em->getRepository(PersonRede::class)->save($personRede);
                    }
                    $directoryGatway = $this->em->getRepository(DirectoryGatway::class)->findOneBy(['tbDiretorioId' => $event->getDiretorioId(), 'formaPagamento' => 1, 'gatwayPagamento' => 2]);
                    $transactionRede = MaxiPago::transaction($personRede, $cardToken, $total, $event->getCategoryString() . ' - ' . $event->getName(), $directoryGatway, $user);
                    $directory = $directoryGatway ? $event->getDiretorioId() : 1; //DN
                    $invoiceId = $transactionRede['transactionID'];
                    $customerId = $personRede->getCustomerId();
                    $owner = $event->getDiretorioId();
                }
                $transactionData = $this->populateTransaction($user, $total, $date, $date, $date, 1, 'paid',
                    $customerId, 0, $gateway, $event->getCategory(), 'Não ratear', $cardToken, $invoiceId, '', $directory, $owner, '', '', '');
                $this->em->getRepository(Transaction::class)->save($transactionData);
                $msg = Messages::eventsPaymentCard($user, $type, $event);
                Email::send($user->getEmail(), $user->getName(), 'Inscrição Realizada com sucesso!', $msg);
                $message = Messages::eventsPaymentMessage($data['paymentMethod'], $type);
                $eventTransaction = $this->insertTableTransactions($total, $data, $user, $transactionData->getId(), 'confirmado');
                $this->insertTableParticipantes($data, $user, $tickets, $total, $eventTransaction, 'confirmado');
            } else if ($data['paymentMethod'] == 2) { //boleto
                $slip = IuguService::slip($user->getEmail(), $user->getCpf(), $user->getName(), $address, $total, $event->getCategoryString() . ' - ' . $event->getName(), 5, $keyIUGU['key']);
                $transactionData = $this->populateTransaction($user, $total, $date, $date, null, 2, 'pending', '', 0, 1, $event->getCategory(), 'Não ratear', '', $slip['id'], '', 
                                                                $keyIUGU['deposit'], $keyIUGU['owner'], $slip['bank_slip']['bank_slip_pdf_url'], $slip['bank_slip']['digitable_line'], '');
                $this->em->getRepository(Transaction::class)->save($transactionData);
                $msg = Messages::eventsPaymentSlip($user, $type, $event, $slip, $ticket);
                Email::send($user->getEmail(), $user->getName(), 'Inscrição evento solicitada - Boleto', $msg);
                $message = Messages::eventsPaymentMessage($data['paymentMethod'], $type, $slip);
                $eventTransaction = $this->insertTableTransactions($total, $data, $user, $transactionData->getId(), 'pendente');
                $this->insertTableParticipantes($data, $user, $tickets, $total, $eventTransaction, 'pendente');
            } else if ($data['paymentMethod'] == 5) { //pix
                $pix = IuguService::slip($user->getEmail(), $user->getCpf(), $user->getName(), $address, $total, $event->getCategoryString() . ' - ' . $event->getName(), 5, $keyIUGU['key']);
                if (!$pix['pix']['qrcode_text']) throw new \Exception("Erro ao gerar QR Code Pix");
                $transactionData = $this->populateTransaction($user, $total, $date, $date, null, 11, 'pending', '', 0, 1, $event->getCategory(), 'Não ratear', '', $pix['id'], '',
                                                                $keyIUGU['deposit'], $keyIUGU['owner'], $pix['pix']['qrcode'], '', '');
                $this->em->getRepository(Transaction::class)->save($transactionData);
                $msg = Messages::eventsPaymentPix($user, $type, $event, $ticket, $pix);
                Email::send($user->getEmail(), $user->getName(), 'Participação evento solicitada - PIX', $msg);
                $message = Messages::eventsPaymentMessage($data['paymentMethod'], $type, $pix);
                $eventTransaction = $this->insertTableTransactions($total, $data, $user, $transactionData->getId(), 'pendente');
                $this->insertTableParticipantes($data, $user, $tickets, $total, $eventTransaction, 'pendente'); 
            }
            // registra quem indicou
            if($data['codigo']) $this->setWhoIndicate($user, $data['codigo'], 4, $event->getId());
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => $message,
                'user' => $user->getId(),
                'event' => $event->getId(),
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    private function insertTableTransactions($total, array $data, User $user, $transaction, $status)
    {
        $code = strtoupper(substr(bin2hex(random_bytes(5)), 1));
        $eventsTransactionsData = new EventsTransactions();
        $eventsTransactionsData->setTbEventosId($data['eventId'])
                                ->setTbPessoaId($user->getId())
                                ->setTbTransacaoId($transaction)
                                ->setValor($total)
                                ->setName($user->getName())
                                ->setDataNascimento($user->getDataNascimento())
                                ->setCpf($user->getCpf())
                                ->setEmail($user->getEmail())
                                ->setCode($code)
                                ->setStatus($status)
                                ->setDataCriacao(new \Datetime(date('Y-m-d H:i:s')));
        if($data['couponHidden']) {
            $coupon = $this->em->getRepository(EventsCoupons::class)->findOneBy(['code' => $data['couponHidden'], 'event' => $data['eventId']]);
            $eventsTransactionsData->setCoupon($this->em->getReference(EventsCoupons::class, $coupon->getId()));
        } 
        return $this->em->getRepository(EventsTransactions::class)->save($eventsTransactionsData)->getId();
    }

    private function insertTableParticipantes(array $data, User $user, array $tickets, $total, $eventTransaction, $status)
    {
        $phone = $this->em->getRepository(Phone::class)->findOneBy(['tbPessoaId' => $user->getId(), 'tbTipoTelefoneId' => 3]);
        foreach ($tickets as $ticket) {
            $price = $this->em->getRepository(EventsPrices::class)->findOneBy(['id' => $ticket[1]]);
            for($i = 1; $i <= $ticket[0]; $i++) {
                $code = strtoupper(substr(bin2hex(random_bytes(4)), 1));
                $participantsData = new EventsParticipants();
                $participantsData->setTbEventsId($data['eventId'])
                                    ->setTbEventsPricesId($ticket[1])
                                    ->setTbEventsTransactionId($eventTransaction)
                                    ->setTbPessoaId($user->getId())
                                    ->setName($user->getName())
                                    ->setCpf($user->getCpf())
                                    ->setEmail($user->getEmail())
                                    ->setDateBirth($user->getDataNascimento())
                                    ->setCode($code)
                                    ->setValue($price->getValue())
                                    ->setStatus($status)
                                    ->setDateCreated(new \Datetime(date('Y-m-d H:i:s')));
                if($phone) $participantsData->setPhone($phone->getTelefone());
                $this->em->getRepository(EventsParticipants::class)->save($participantsData);   
            }
        }  
    }

    public function vaccineVoucher(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $file = $request->getUploadedFiles(); 
            if($data['cpf']){
                $user = $this->em->getRepository(User::class)->findOneBy(['cpf' => $data['cpf']]);
            } else {
                $user = $this->em->getRepository(User::class)->findOneBy(['id' => $data['user']]);
            }
            if(!$user) throw new \Exception('Participante não encontrado!');
            $date = new \DateTime();
            $file = $file['file'];
            $event = $this->em->getRepository(Events::class)->findOneBy(['id' => $data['event']]);
            $folder = UPLOAD_FOLDER . 'vaccine-voucher/';
            $file->moveTo($folder . $data['user'] . $file->getClientFilename());
            $vaccineVoucher = new vaccineVoucher();
            $vaccineVoucher->setUser($user)
                            ->setEvent($event)
                            ->setFile($data['user'] . $file->getClientFilename())
                            ->setDate($date);
            $this->em->getRepository(vaccineVoucher::class)->save($vaccineVoucher);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Comprovante anexado com sucesso, o NOVO agradece sua participação!',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function editTicket(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $ticket = $this->em->getRepository(EventsParticipants::class)->findOneBy(['tbEventosId' => $data['eventId'], 'code' => $data['code']]);
            $ticket->setName($data['name'])
                   ->setCpf($data['cpf']);
            $this->em->getRepository(EventsParticipants::class)->save($ticket);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Ingresso alterado com sucesso!',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function events(Request $request, Response $response)
    {
        $data = (array)$request->getParams();
        $events = $this->em->getRepository(Events::class)->eventsList($data);
        $eventsArray = [];
        foreach ($events as $e) {
            $eventsArray[] = ['id' => $e['id'], 'event' => $e['event'], 'date' => $e['date'], 'directory' => $e['directory'], 'specie' => $e['especie']];
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $eventsArray,
        ])->withHeader('Content-Type', 'application/json');
    }

    public function eventsByUf(Request $request, Response $response)
    {
        $data = (array)$request->getParams();
        $uf = $this->em->getRepository(Events::class)->eventsListUf($data);
        $ufArray = [];
        foreach ($uf as $u) {
            $ufArray[] = ['id' => $u['id'], 'uf' => $u['uf']];
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $ufArray,
        ])->withHeader('Content-Type', 'application/json');
    }

    public function eventsByCity(Request $request, Response $response)
    {
        $data = (array)$request->getParams();
        $cities = $this->em->getRepository(Events::class)->eventsListCity($data);
        $citiesArray = [];
        foreach ($cities as $c) {
            $citiesArray[] = ['id' => $c['id'], 'city' => $c['city']];
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $citiesArray,
        ])->withHeader('Content-Type', 'application/json');
    }

    public function eventsBySpecie(Request $request, Response $response)
    {
        $data = (array)$request->getParams();
        $specie = $this->em->getRepository(Events::class)->eventsListSpecie($data);
        $specieArray = [];
        foreach ($specie as $s) {
            $specieArray[] = ['id' => $s['id'], 'specie' => $s['specie']];
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $specieArray,
        ])->withHeader('Content-Type', 'application/json');
    }

    public function recommendation(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $register = $this->em->getRepository(EventsParticipants::class)->findBy(['tbEventosId' => $data['event'], 'tbPessoaId' => $data['idParticipant']]);
            foreach($register as $r){
                $r->setRecommendation($data['recommendation']);
                $this->em->getRepository(EventsParticipants::class)->save($r);
            }
            return $response->withJson(['status' => 'ok'], 201)->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }
    
    public function cards(Request $request, Response $response)
    {
        $data = (array)$request->getParams();
        $cards = $this->em->getRepository(PersonCreditCard::class)->findBy(['user' => $data['user'], 'visivel' => 1, 'gatewayPagamento' => 1]);
        $cardsArray = [];
        foreach ($cards as $c) {
            $cardsArray[] = ['id' => $c->getId(), 'legenda' => $c->getLegend()];
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $cardsArray,
        ])->withHeader('Content-Type', 'application/json');
    }

    public function eventsDetails(Request $request, Response $response)
    {
        $data = (array)$request->getParams();
        $e = $this->em->getRepository(Events::class)->findOneBy(['id' => $data['event']]);
        $billsDate = date('Y-m-d', strtotime('-'. $e->getBillsDay() .' days', strtotime($e->getDateBegin()->format('d-m-Y'))));
        $billsDayValid = date('Y-m-d') > $billsDate ? false : true;
        $city = $this->em->getRepository(City::class)->findOneBy(['id' => $e->getCityId()]);
        $state = $this->em->getRepository(State::class)->findOneBy(['id' => $e->getStateId()]);
        $eventArray[] = ['id' => $e->getId(), 'specie' => $e->getSpecie(), 'pathLogo' => $e->getPathLogo(), 
                            'pathBanner' => $e->getPathBanner(), 'name' => $e->getName(), 'affiliatedOnly' => $e->getAffiliatedOnly(),
                            'dateBegin' => $e->getDateBegin()->format('d/m/Y H:i:s'), 'description' => $e->getDescription(), 
                            'billsDay' => $e->getBillsDay(), 'address' => $e->getAddress(), 'complement' => $e->getComplement(), 
                            'local' => $e->getLocal(), 'district' => $e->getDistrict(), 'city' => $city->getCidade(), 'state' => $state->getSigla(),
                            'cep' => $e->getCep(), 'type' => $e->getType(), 'status' => $e->getStatus(), 'billsDayValid' => $billsDayValid
                        ];
        return $response->withJson([
            'status' => 'ok',
            'message' => $eventArray,
        ])->withHeader('Content-Type', 'application/json');
    }

    public function eventsTickets(Request $request, Response $response)
    {
        $data = (array)$request->getParams();
        $tickets = $this->em->getRepository(EventsPrices::class)->findBy(['tbEventosId' => $data['event'], 'status' => 'ativo'],['valor' => 'ASC']);
        $ticketsArray = [];
        foreach ($tickets as $t) {
            $ticketsArray[] = ['id' => $t->getId(), 'descricao' => $t->getDescription(), 'apenasFiliados' => $t->getOnlyAffiliated(), 'valor' => number_format($t->getValue(),2, ',', '.'),
                'dataTermino' => $t->getDateFinal()->format('d/m/Y'), 'limiteInscritos' => $t->getLimitParticipants()];
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $ticketsArray,
        ])->withHeader('Content-Type', 'application/json');
    }

    public function subscriptionPress(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            GoogleService::recapchaNovo($data['g-recaptcha-response']);
            $fields = [
                'reporter' => 'Nome do Repórter',
                'vehicle' => 'Veículo',
                'phone' => 'Telefone',
            ];
            Validator::requireValidator($fields, $data);
            $camera = $data['camera'];
            $photographer = $data['photographer'];
            if(!$camera && !$photographer) throw new \Exception("Selecione pelo menos um dos profissionais!");
            $event = $this->em->getRepository(Events::class)->find($data['eventsId']);
            $participant = $this->em->getRepository(EventsPress::class)->findBy(['eventsId' => $data['eventsId'], 'phone' => $data['phone']]);
            if($participant) throw new \Exception('Você já realizou sua inscrição!');
            $participantsData = new EventsPress();
            $participantsData->setEventsId($event)
                                ->setReporter($data['reporter'])
                                ->setVehicle($data['vehicle'])
                                ->setPhone($data['phone'])
                                ->setStatus(1)
                                ->setCamera($camera ?: 0)
                                ->setPhotographer($photographer ?: 0);
            $this->em->getRepository(EventsPress::class)->save($participantsData);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => "O NOVO agradece sua participação!<br>Sua inscrição foi realizada com sucesso e em breve daremos um retorno!"
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function validateCpf(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            //GoogleService::recapchaNovo($data['g-recaptcha-response']);
            Validator::validateCPF($data['cpf']);
            $cpf = Utils::formatCpf($data['cpf']);
            $user = $this->em->getRepository(User::class)->findOneBy(['cpf' => $cpf]);
            if(!$user) {
                if(!$data['nameRegister']) throw new FoundException("Não encontramos seu cadastro, preencha os campos abaixo para seguirmos com sua inscrição!");
                $fields = [
                    'nameRegister' => 'Nome',            
                    'emailRegister' => 'E-mail',
                ];
                Validator::requireValidator($fields, $data);
                $date = new \Datetime();
                $user = new User();
                $user->setFiliado(0)->setStatus(0)->setName($data['nameRegister'])->setCpf(Utils::formatCpf($cpf))->setEmail($data['emailRegister'])->setTermoAceite0(1)
                    ->setTermoAceite1(1)->setTermoAceite2(1)->setTermoAceite3(1)->setDataCriacao($date)->setEstadoCivil($this->em->getReference(CivilState::class, 6))
                    ->setGenero($this->em->getReference(Gender::class, 3))->setEscolaridade($this->em->getReference(Schooling::class, 7))->setOpcaoReligiosa(11)
                    ->setIp(Utils::getAcessData()['ip'])->setUltimaAtualizacao($date)->setDataCriacao($date);
                $user = $this->em->getRepository(User::class)->save($user);
                $cellPhone = new Phone();
                $cellPhone->setPais('br')
                    ->setDdi(55)
                    ->setTelefone($data['phone'])
                    ->setTbTipoTelefoneId(3)
                    ->setTbPessoaId($user->getId());
                $this->em->getRepository(Phone::class)->save($cellPhone);
                $this->confirmEmail($user, 'Evento - Ativação da Conta');
                if(isset($data['term'])){
                    
                    /*-----Hubspot/Mailchimp-----*/
                    Hubspot::createContact($user);
                    $this->mailchimpCreateUser($user, null, $data['phone']);
                }
            }
            if(array_sum($data['qtde']) == 0) throw new \Exception('Atenção, selecione pelo menos um ingresso!');
            $event = $this->em->getRepository(Events::class)->findOneBy(['id' =>  $data['eventId']]);
            if(!$event) throw new \Exception("Evento não encontrado, Solicitação inválida!");
            $participatsEvent = count($this->em->getRepository(EventsParticipants::class)->findBy(['tbEventosId' => $data['eventId'], 'status' => 'confirmado']));
            if($participatsEvent >= $event->getLimitSubscribers()) throw new \Exception('Atenção, o evento alcançou o limite de participantes permitido!');
            $email = $user->getEmail();
            $total = $qtde = null;
            $tickets = array_map(null, $data["qtde"], $data["idTicket"]);
            foreach($tickets as $ticket) {
                $ticketValue = $this->em->getRepository(EventsPrices::class)->findOneBy(['id' =>  $ticket[1]]);
                $qtdeTicketConfirmed = count($this->em->getRepository(EventsParticipants::class)->findBy(['tbEventosId' => $data['eventId'], 'status' => 'confirmado', 'tbEventosPrecosId' => $ticketValue->getId()]));
                $value = $ticket[0] * $ticketValue->getValue();
                $qtde += $ticket[0];
                $total += $value;
                if(intVal($ticket[0]) >= 1){
                    if($ticketValue->getOnlyAffiliated() == 1 && !$user->isAffiliated()) throw new \Exception('Você selecionou um ingresso restrito para filiados,<br> retorne e escolha outro ingresso!');
                    if($qtdeTicketConfirmed >= $ticketValue->getLimitTotal()) throw new \Exception('Você selecionou um ingresso esgotado, volte e selecione outro ingresso!');
                    $userTickets = count($this->em->getRepository(EventsParticipants::class)->findBy(['tbPessoaId' => $user->getId(), 'tbEventosPrecosId' => $ticket[1]]));
                    if($userTickets >= $ticketValue->getLimitParticipants()) throw new \Exception('Você já adquiriu o máximo de ingressos nesta faixa de preço, selecione outro ingresso!');
                    $sumTicketsByUser = $ticket[0] + $userTickets;
                    if($sumTicketsByUser > $ticketValue->getLimitParticipants()) throw new \Exception('Você já adquiriu o máximo de ingressos nesta faixa de preço!');
                }
            }
            if($total > MAXVALUE) throw new \Exception("Valor máximo para a compra de ingressos é R$ " . number_format(MAXVALUE, 2, ',', '.') . "!"); 
            if($qtde == 0) throw new \Exception('Atenção, selecione pelo menos um ingresso!');
            if($participatsEvent >= $event->getLimitSubscribers()) throw new \Exception('Atenção, o evento alcançou o limite de participantes permitido!');
            if($event->getAffiliatedOnly() == 1) {
                if(!$user->isAffiliated()) throw new \Exception('Evento exclusivo para filiados, se for filiado utilize as mesmas credenciais do Espaço NOVO!');
            }
            $message = "Obrigado pelas informações, aguarde uns instantes para ser redirecionado!";
            if($total == 0) {
                $this->insertTableParticipantes($data, $user, $tickets, $total, 0, 'confirmado');
                $msg = Messages::subscriptionLogin($data, $user, $event);
                if($event->getSpecie() == 3) $msg = Messages::subscriptionOnline($data, $user->getFirstName(), $event);
                Email::send($email, $user->getFirstName(), 'Inscrição Realizada com sucesso', $msg);
                $message = "O NOVO agradece sua participação,<br> sua inscrição foi realizada com sucesso e um email foi enviado para sua caixa de entrada!";
            }
            if($data['codigo']) $this->setWhoIndicate($user, $data['codigo'], 4, $event->getId());
            return $response->withJson([
                'status' => 'ok',
                'pessoaId' => $user->getId(),
                'email' => $user->getEmail(),
                'total' => $total,
                'message' => $message,
                'qtde2' => $data["qtde"],
                'idTicket2' => $data["idTicket"],
                'eventId' => $data["eventId"],
                'qtde' => $qtde
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (FoundException $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(302);
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }
}