<?php

namespace App\Controllers;

use App\Helpers\Session;
use App\Models\Commons\Entities\LibertasMoodleRedirect;
use App\Models\Entities\Address;
use App\Models\Entities\City;
use App\Services\Libertas;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

class MoodleController extends Controller
{

    public function index(Request $request, Response $response)
    {
        $short = $request->getAttribute('route')->getArgument('short');
        $course = $request->getQueryParams()['course'];
        $wantsurl = '';
        if ($short) $wantsurl = $this->em->getRepository(LibertasMoodleRedirect::class)->findOneBy(['shortLink' => $short, 'status' => 1]);
        if ($wantsurl) $wantsurl = $wantsurl->getMoodleLink();
        if (!$short && $course) $wantsurl = "https://cursos.institutolibertas.org.br/course/view.php?id={$course}";
        $user = $this->getLogged([], true);
        $data = [
            'name' => $user->getName(),
            'email' => $user->getEmail(),
            'cpf' => $user->getCpf(),
            'filiado' => 0,
            'city' => '',
            'estado' => '',
        ];
        if ($user->isAffiliated()) $data['filiado'] = 1;

        $address = $this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()]);
        if ($address) {
            $data['city'] = $address->getCidade();
            $data['estado'] = $address->getEstado();
            if ($address->getTbCidadeId()) {
                $city = $this->em->getRepository(City::class)->find($address->getTbCidadeId());
                if ($city) {
                    $data['city'] = $city->getCidade();
                    $data['estado'] = $city->getState()->getSigla();
                }
            }
        }
        $login = Libertas::moodleLogin($data);
        if ($login['link']) $this->redirectExternal($login['link'] . "&wantsurl={$wantsurl}");
        elseif ($login['errorMessage']) $this->redirect('', $login['errorMessage']);
        $this->redirect('', 'Falha ao acessar a plataforma.');
    }


}
