<?php

namespace App\Controllers;

use App\Helpers\Messages;
use App\Helpers\Session;
use App\Models\Entities\Candidate;
use App\Helpers\Validator;
use App\Helpers\Utils;
use App\Services\Auth;
use App\Services\Hubspot;
use App\Services\Mailchimp;
use App\Models\Entities\User;
use App\Models\Entities\Phone;
use App\Models\Entities\Gender;
use App\Models\Entities\Schooling;
use App\Models\Entities\CivilState;
use App\Models\Entities\State;
use App\Models\Entities\ReasonDisaffection;
use App\Models\Entities\City;
use App\Models\Entities\Country;
use App\Models\Entities\Address;
use App\Models\Entities\PersonSignature;
use App\Models\Entities\PersonCreditCard;
use App\Models\Entities\Communicated;
use App\Models\Entities\ChangeEmail;
use App\Models\Entities\HowdidKnow;
use App\Models\Entities\Transaction;
use App\Models\Entities\ProcessCep;
use App\Models\Entities\BellowSigned;
use App\Models\Entities\Comments;
use App\Models\Entities\CommentStatus;
use App\Models\Entities\ResearchNovoUser;
use App\Models\Entities\Indicator;
use App\Models\Entities\IndicatorFiles;
use App\Services\Email;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

class UserController extends Controller
{
    public function index(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $restricted = $this->em->getRepository(Communicated::class)->findBy(['tipo' => 'restricted'], ['data' => 'desc'], 5);
        $contents = $this->em->getRepository(Communicated::class)->findBy(['tipo' => 'communicated'], ['data' => 'desc'], 5);
        $resolutions = $this->em->getRepository(Communicated::class)->findBy(['tipo' => 'resolution'], ['data' => 'desc'], 5);
        $record = $this->em->getRepository(Communicated::class)->findBy(['tipo' => 'record'], ['data' => 'desc'], 5);
        if (Session::get('seletivo2022') === null) {
            Session::set('seletivo2022', false);
            $candidate = $this->em->getRepository(Candidate::class)->findOneBy(['cpf' => Utils::onlyNumbers($user->getCpf()), 'candidateStatus' => 3]);
            if ($user->isAffiliated() && $candidate) Session::set('seletivo2022', true);
        }
        $states = $this->em->getRepository(State::class)->findAll();
        $address = $this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()]);
        $know = $this->em->getRepository(HowdidKnow::class)->findOneBy(['tbPessoaId' => $user->getId()]);
        $addressCities = $address ? $this->em->getRepository(City::class)->findBy(['state' => $address->getStateId()]) : null;
        $contribution = $this->em->getRepository(PersonSignature::class)->findOneBy(['tbPessoaId' => $user->getId(), 'origin' => [2, 3], 'status' => 'created']);
        $card = null;
        if ($contribution) $card = $this->em->getRepository(PersonCreditCard::class)->findOneBy(['user' => $user->getId(), 'principal' => 1, 'tokenCartaoCredito' => $contribution->getTokenCartaoCredito()]);
        $affiliated = $this->em->getRepository(User::class)->recommendation();
        $refreshData = $this->em->getRepository(Comments::class)->findBy(['tbPessoaId' => $user->getId(), 'status' => 18]);
        $valid = $text = null;

        // Verifica se há transações pendentes de pagamento por cartão de crédito
        $transactionPending = $this->getTransactionPending($user);

        $active = $this->em->getRepository(Transaction::class)->activeUpdatePersonalData($user);
        if ($user->getExemption() == 1) $active = true;

        $reportIncome = $this->em->getRepository(Transaction::class)->getReportIncome($user);
        $researchNovoUser = $this->em->getRepository(ResearchNovoUser::class)->verifyAnswer($user->getId());

        return $this->renderer->render($response, 'default.phtml', ['page' => 'index.phtml', 'section' => 'home', 'refreshData' => $refreshData,
            'user' => $user, 'valid' => $valid, 'know' => $know, 'title' => 'Home', 'text' => $text, 'affiliated' => $affiliated, 'addressCities' => $addressCities,
            'address' => $address, 'states' => $states, 'record' => $record, 'resolutions' => $resolutions, 'restricted' => $restricted, 'contents' => $contents,
            'contribution' => $contribution, 'active' => $active, 'card' => $card, 'reportIncome' => $reportIncome, 'researchNovoUser' => $researchNovoUser,
            'transactionPending' => $transactionPending]);
    }

    public function newAffiliation(Request $request, Response $response)
    {
        $user = $this->getLogged([0, 5, 9]);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'users/new-register.phtml', 'section' => 'filie',
            'user' => $user, 'title' => 'Filiação']);
    }

    public function affiliation(Request $request, Response $response)
    {
        $user = $this->getLogged([0, 5, 9]);
        $cellPhone = $this->em->getRepository(Phone::class)->findOneBy(['tbPessoaId' => $user->getId(), 'tbTipoTelefoneId' => 3]);
        $states = $this->em->getRepository(State::class)->findAll();
        $countries = $this->em->getRepository(Country::class)->findAll();
        $voterTitleCities = $this->em->getRepository(City::class)->findBy(['state' => $user->getTituloEleitoralUfId()]);
        $address = $this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()], ['id' => 'DESC']);
        $addressCities = $address ? $this->em->getRepository(City::class)->findBy(['state' => $address->getStateId()]) : null;
        $today = new \Datetime();
        $validProcessCep = false;
        $processCep = $this->em->getRepository(ProcessCep::class)->findOneBy(['personId' => $user->getId()]);
        if($processCep) {
            $validProcessCep = true;
            if($processCep->getDateFinal()) $validProcessCep = $processCep->getDateFinal()->format('Y-m-d') >= $today->format('Y-m-d') ? true : false;
        }
        return $this->renderer->render($response, 'default.phtml', ['page' => 'users/register.phtml', 'section' => 'filie',
            'user' => $user, 'title' => 'Filiação', 'cellPhone' => $cellPhone, 'states' => $states, 'voterTitleCities' => $voterTitleCities,
            'address' => $address, 'validProcessCep' => $validProcessCep, 'addressCities' => $addressCities, 'countries' => $countries]);
    }

    public function personalData(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $phone = $this->em->getRepository(Phone::class)->findOneBy(['tbPessoaId' => $user->getId(), 'tbTipoTelefoneId' => 1]);
        $cellPhone = $this->em->getRepository(Phone::class)->findOneBy(['tbPessoaId' => $user->getId(), 'tbTipoTelefoneId' => 3]);
        $genders = $this->em->getRepository(Gender::class)->findAll();
        $education = $this->em->getRepository(Schooling::class)->findAll();
        $civilStatus = $this->em->getRepository(CivilState::class)->findAll();
        $states = $this->em->getRepository(State::class)->findAll();
        $countries = $this->em->getRepository(Country::class)->findAll();
        $voterTitleCities = $this->em->getRepository(City::class)->findBy(['state' => $user->getTituloEleitoralUfId()]);
        $address = $this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()]);
        $addressCities = $address ? $this->em->getRepository(City::class)->findBy(['state' => $address->getStateId()]) : '';
        $voterTitleCountry = $user->getTituloEleitoralPaisId() == null ? 33 : $user->getTituloEleitoralPaisId()->getId();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'users/personal-data.phtml', 'section' => 'myAccount',
            'user' => $user, 'title' => 'Minha Conta', 'phone' => $phone, 'cellPhone' => $cellPhone, 'genders' => $genders, 'civilStatus' => $civilStatus,
            'education' => $education, 'states' => $states, 'voterTitleCities' => $voterTitleCities, 'address' => $address, 'addressCities' => $addressCities,
            'countries' => $countries, 'voterTitleCountry' => $voterTitleCountry]);
    }

    public function savePersonalData(request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $user = $this->populateUser($data);
            $this->populateAddress($data);
            $cellPhone = $this->em->getRepository(Phone::class)->findOneBy(['tbPessoaId' => $user->getId(), 'tbTipoTelefoneId' => 3]);
            $cellPhone = $cellPhone ? $cellPhone : $cellPhone = new Phone();
            $cellPhone->setTbPessoaId($user->getId())->setTbTipoTelefoneId(3)->setPais($data['cellPhoneIso2'])->setDdi($data['cellPhoneDialCode'])->setTelefone($data['cellPhone']);
            $this->em->getRepository(Phone::class)->save($cellPhone);
            $phone = $this->em->getRepository(Phone::class)->findOneBy(['tbPessoaId' => $user->getId(), 'tbTipoTelefoneId' => 1]);
            $phone = $phone ? $phone : new Phone();
            $phone->setTbPessoaId($user->getId())->setTbTipoTelefoneId(1)->setPais($data['phoneIso2'])->setDdi($data['phoneDialCode'])->setTelefone($data['phone']);
            $this->em->getRepository(Phone::class)->save($phone);
            $refreshComment = $this->em->getRepository(Comments::class)->findOneBy(['tbPessoaId' => $user->getId(), 'status' => 18]);
            if ($refreshComment) {
                $refreshComment->setStatus($this->em->getReference(CommentStatus::class, 19));
                $this->em->getRepository(Comments::class)->save($refreshComment);
            }
            if (isset($data['irpf'])) {
                $date = new \DateTime();
                $subscription = $this->em->getRepository(PersonSignature::class)->findOneBy(['tbPessoaId' => $user, 'origin' => 2]);
                if ($subscription) {
                    if (isset($data['exemption']) && $data['exemption'] == 1) {
                        $user->setIrpf(1)->setExemption(1);
                        $subscription->setPeriodicidade(1)->setValor(0.00)->setPaymentForm(0)->setGatewayPagamento(0)->setTbDiretorioOrigem(1);
                        if (in_array($user->getFiliado(), array(1, 3))) {
                            if ($user->getFiliado() == 1) {
                                $user->setFiliado(2);
                                //->setDataSolicitacaoFiliacao($date);
                            } else {
                                $user->setFiliado(14);
                                //->setDataSolicitacaoReFiliacao($date);
                            }
                        }
                        /*
                        $transactions = $this->em->getRepository(Transaction::class)->findBy(['tbPessoaId' => $user, 'formaPagamento' => [1, 2], 'status' => ['pending', 'expired']]);
                        foreach($transactions as $t) {
                            $t->setValor('0.00')
                                ->setStatus('exemption')
                                ->setFormaPagamento(0)
                                ->setGatewayPagamento(0)
                                ->setObs('Filiação Isenta');
                            $t = $this->em->getRepository(Transaction::class)->save($t);
                        }*/
                        $exemption = 'SIM';
                    } elseif ($subscription->getFormaPagamento() == 0) {
                        if(in_array($user->getFiliado(), array(2, 14))) throw new \Exception("Você só poderá retirar sua isenção de filiação após o período de impugnação de 3 dias corridos a partir da sua solicitação!");
                        $user->setExemption(0);
                        $subscription->setPeriodicidade(1)->setValor(MINMONTHLY)->setPaymentForm(2)->setGatewayPagamento(1)->setTbDiretorioOrigem(1);
                        $exemption = 'NÃO';
                    }
                    
                    /*-----Hubspot/Mailchimp-----*/
                    $this->updateStatusRegister($user);
                
                }
                $this->em->getRepository(User::class)->save($user);
                $this->em->getRepository(PersonSignature::class)->save($subscription);
            }
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Dados alterados com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    private function populateAddress(array $data): Address
    {
        $user = $this->getLogged();
        $data['addressOutside'] ??= 0;
        $address = $this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()]) ?: new Address();
        $uf = $this->em->getRepository(State::class)->findOneBy(['id' => $data['ufId']]); // vem como string
        $uf = $uf ? $uf->getId() : 1; // para funcionar o select integrado com o cep
        $city = $this->em->getRepository(City::class)->findOneBy(['id' => $data['cityId']]);
        $city = $city ? $city->getId() : 0;
        $address->setTbPessoaId($user->getId())
            ->setCountryId($data['addressCountry']) //Brasil
            ->setResideExterior($data['addressOutside'])
            ->setStateId($uf)
            ->setEstado('')
            ->setCityId($city)
            ->setCidade('')
            ->setCep($data['zipCode'])
            ->setNumero($data['number'])
            ->setEndereco($data['street'])
            ->setComplemento($data['complement'])
            ->setBairro($data['district']);
        if ($data['addressOutside']) {
            $address->setCityId(0)
                ->setCountryId($data['addressCountry'])
                ->setStateId(1)
                ->setEstado($data['uf'])
                ->setCidade($data['city']);
        }
        return $this->em->getRepository(Address::class)->save($address);
    }

    private function populateUser(array $data): User
    {
        $user = $this->getLogged();
        $usersRepository = $this->em->getRepository(User::class);
        $user->setEscolaridade($this->em->getReference(Schooling::class, $data['education']))
            ->setProfissao($data['profession'])
            ->setGenero($this->em->getReference(Gender::class, $data['gender']))
            ->setEstadoCivil($this->em->getReference(CivilState::class, $data['civilStatus']))
            ->setName($data['name'])
            ->setRg($data['rg'])
            ->setOrgaoExpedidor($data['issuingAgency'])
            ->setDataNascimento(\DateTime::createFromFormat('d/m/Y', $data['birth']))
            ->setNomeMae($data['mother'])
            ->setTituloEleitoral($data['voterTitle'])
            ->setTituloEleitoralZona($data['voterTitleZone'])
            ->setTituloEleitoralSecao($data['voterTitleSection'])
            ->setTituloEleitoralMunicipio($data['voterTitleCityString'])
            ->setTituloEleitoralPaisId($this->em->getReference(Country::class, $data['voterTitleCountry']))
            ->setTituloEleitoralMunicipioId(null)
            ->setTituloEleitoralUfId(null)
            ->setUltimaAtualizacao(new \Datetime());
        if (isset($data['irpf'])) $user->setIrpf($data['irpf']) ?: $user->setIrpf(0);        
        if ($data['originVoterTitle'] != 1) {
            $user->setTituloEleitoralPaisId($this->em->getReference(Country::class, 33)) //Brasil
            ->setTituloEleitoralMunicipioId($this->em->getReference(City::class, $data['voterTitleCity']))
                ->setTituloEleitoralUfId($this->em->getReference(State::class, $data['voterTitleUF']));
        }

        /*-----Hubspot/Mailchimp-----*/
        $hp = [];
        $hp['email'] = $user->getEmail();
        $hp['filiado'] = $user->getFiliadoString();
        $hp['status_de_filiado'] = $user->getFiliadoString();
        $hp['state'] = $user->getTituloEleitoralUfId()->getSigla();
        $hp['city'] = $user->getTituloEleitoralMunicipioId()->getCidade();
        Hubspot::updateContact($user, $hp);
        $body = [];
        $body['email_address'] = $user->getEmail();
        $body['merge_fields']['FILIADO'] = $user->getFiliadoString();
        $body['merge_fields']['MMERGE63'] = $user->getFiliadoString();
        $body['merge_fields']['ADDRESS']['state'] = $user->getTituloEleitoralUfId()->getSigla();
        $body['merge_fields']['ADDRESS']['city'] = $user->getTituloEleitoralMunicipioId()->getCidade();
        Mailchimp::updateContact($body);

        return $usersRepository->save($user);
    }

    public function disaffection(Request $request, Response $response)
    {
        $user = $this->getLogged([7, 8]);
        $reasons = $this->em->getRepository(ReasonDisaffection::class)->findBy(['visivel' => 1], ['motivo' => 'ASC']);
        $processCep = $this->em->getRepository(ProcessCep::class)->getProcessCEPCRM($user) ? true : false;

        return $this->renderer->render($response, 'default.phtml', ['page' => 'users/disaffection.phtml', 'section' => 'disaffection', 
            'subMenu' => 'myRequests', 'user' => $user, 'title' => 'Desfiliação', 'reasons' => $reasons, 'processCep' => $processCep
        ]);
    }

    public function saveDisaffection(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $user = $this->getLogged();
            $processCep = $this->em->getRepository(ProcessCep::class)->getProcessCEPCRM($user) ? true : false;

            $fields = [
                //'reason' => 'Motivo',
                'reasons' => 'Tipo do Motivo',
            ];
            Validator::requireValidator($fields, $data);

            $data['reasons'] = implode((','), $data['reasons']);
            $user->setDataDesfiliacao(new \DateTime())
                 ->setFiliado(9)
                 ->setMotivoDesfiliacao($data['reasons'])
                 ->setDescricaoDesfiliacao($data['reason'])
                 ->setMonthlyBill(0)
                 ->setIrpf(0)
                 ->setExemption(0)
                 ->setUltimaAtualizacao(new \Datetime());

            if($processCep) {
                $user->setSuspensao(true)
                    ->setObservacao("Filiação suspensa por 8 anos.");
            }

            $this->em->getRepository(User::class)->save($user);

            $signature = $this->em->getRepository(PersonSignature::class)->findOneBy(['tbPessoaId' => $user->getId()]);
            $signature->setStatus('paused');
            $this->em->getRepository(PersonSignature::class)->save($signature);
            
            /*-----Hubspot/Mailchimp-----*/
            $this->updateStatusRegister($user);

            $msg = "Olá " . $user->getName() . ",<br><br>
                    Sua desfiliação foi realizada com sucesso, a partir deste momento não haverá cobranças relativas à sua contribuição de filiado.<br><br>
                    Obs: Informamos que cabe ao ELEITOR informar ao juiz eleitoral de sua circunscrição por meio de petição de sua desfiliação,
                    e que a justiça eleitoral processa os pedidos de desfiliação apenas nos meses de abril e outubro, de acordo com o que dispõe a
                    Lei 9.096/95.<br><br>
                    Equipe NOVO!";
            Email::send($user->getEmail(), $user->getName(), 'Desfiliação - Partido NOVO', $msg);

            $msg = Messages::disaffectionEmail($user, $data['reason']);
            Email::send('refiliacao@novo.org.br', 'Filiação NOVO', "NOVO PEDIDO DE DESFILIAÇÃO - {$user->getName()}", $msg);

            $message = 'Desfiliação realizada com sucesso';

            $this->registerHistoric($user, 4);
            return $response->withJson([
                'status' => 'ok',
                'message' => $message,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }


    public function savePassword(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            Validator::validatePassword($data);
            $user = $this->getLogged();
            if (md5($data['actualPassword']) != $user->getPassword()) {
                throw new \Exception('Senha atual inválida!');
            }
            $usersRepository = $this->em->getRepository(User::class);
            $user->setPassword(md5($data['password']));
            $usersRepository->save($user);
            Auth::charge($user->getName(), $user->getEmail(), $user->getPassword());
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Senha alterada com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function saveEmail(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $user = $this->getLogged();
            $fields = [
                'email2' => 'Digite novo E-mail',
            ];
            Validator::requireValidator($fields, $data);
            $token = Utils::generateToken();
            $changeEmail = new ChangeEmail();
            $changeEmail->setUser($user)
                ->setCreated(new \DateTime())
                ->setUsed(0)
                ->setToken($token)
                ->setEmail($data['email2']);
            $this->em->getRepository(ChangeEmail::class)->save($changeEmail);
            $msg = "<p>Olá {$user->getName()},</p>
                    <p>Para efetivar a alteração de seu e-mail no espaço novo, clique no link abaixo.</p>
                    <a class='btn btn-primary' style='color: white' href='{$this->baseUrl}usuario/alterar-email/{$token}' target='_blank'>Alterar E-mail</a>
                    <p>Caso o botão não funcione copie e cole o link no seu navegador: {$this->baseUrl}usuario/alterar-email/{$token}</p>";
            Email::send($data['email2'], $user->getName(), "Espaço NOVO - Alteração de Email", $msg);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Um link para confirmação de e-mail foi enviado para {$data['email2']}, assim que você confirmar a alteração será efetivada",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function changeEmail(Request $request, Response $response)
    {
        $token = $request->getAttribute('route')->getArgument('token');
        $change = $this->em->getRepository(ChangeEmail::class)->findOneBy(['token' => $token]);
        $confirm = $msg = false;
        if ($change && $change->isUsed() == false) {
            $change->setUsed(1);
            $this->em->getRepository(ChangeEmail::class)->save($change);
            $user = $change->getUser();

            /*-----Hubspot/Mailchimp-----*/
            $this->updateStatusRegister($user);

            $confirm = !$user->getEmailConfirmado();
            $user->setEmail($change->getEmail());
            $user->setEmailConfirmado(1);
            $this->em->getRepository(User::class)->save($user);
            Auth::charge($user->getName(), $user->getEmail(), $user->getPassword() ?? '');
        } else {
            $msg = 'O Link é inválido!';
            if ($change && $change->isUsed() == true) {
                $change = false;
                $msg = 'O e-mail já foi ativado, tente realizar o login!';
            }
        }
        return $this->renderer->render($response, 'public/change-email.phtml', ['change' => $change, 'confirm' => $confirm, 'msg' => $msg]);
    }

    public function saveKnowledge(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $user = $this->getLogged();

            $howdidKnow = new HowdidKnow();
            $howdidKnow->setTbPessoaId($user->getId())
                ->setType($data['type'])
                ->setDataCriacao(new \DateTime());
            if ($data['type'] == 1) {
                $howdidKnow->setContent($data['name_affiliated']);
            } else if ($data['type'] == 2) {
                $howdidKnow->setSubtype($data['media'])
                    ->setContent($data['media']);
            } else if ($data['type'] == 3) {
                $howdidKnow->setUf($data['ufId'])
                    ->setCity($data['cityId'])
                    ->setEvent($data['event'])
                    ->setContent($data['event']);
            } else if ($data['type'] == 4) {
                $howdidKnow->setContent("Conheci pelo site do novo.org.br");
            } else if ($data['type'] == 5) {
                $howdidKnow->setContent($data['news']);
            } else if ($data['type'] == 6) {
                $howdidKnow->setContent($data['others']);
            }
            $this->em->getRepository(HowdidKnow::class)->save($howdidKnow);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Agradecemos sua participação",
            ], 201)->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function affiliationCancel(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $status = $user->getFiliado() == 2 ? 'filiação' : 'refiliação';
        return $this->renderer->render($response, 'default.phtml', ['page' => 'users/affiliationCancel.phtml', 'section' => 'cancelSolicitation',
            'user' => $user, 'status' => $status, 'title' => 'Cancelamento de pedido de ' . $status]);
    }

    public function SaveAffiliationCancel(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            $user->setDataCancelamentoPedido(new \DateTime())
                ->setFiliado(5)
                ->setDescricaoDesfiliacao($data['reason'])
                ->setObservacao(null)
                ->setIrpf(0)
                ->setExemption(0);
            $this->em->getRepository(User::class)->save($user);

            /*-----Conversões Hubspot/Mailchimp-----*/
            $this->updateStatusRegister($user);

            $signature = $this->em->getRepository(PersonSignature::class)->findOneBy(['tbPessoaId' => $user->getId()]);
            $signature->setStatus('paused');
            $this->em->getRepository(PersonSignature::class)->save($signature);
            $this->registerHistoric($user, 9);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Pedido de cancelado com sucesso",
            ], 201)->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function undersigned(Request $request, Response $response)
    {
        $total = $this->em->getRepository(BellowSigned::class)->listTotalByTheme('abuse_of_authority');
        return $this->renderer->render($response, 'public/default.phtml', ['page' => 'undersigned/undersigned.phtml', 'total' => $total]);
    }

    public function updatePersonalData(Request $request, Response $response): void
    {
        $this->em->beginTransaction();
        $user = $this->getLogged();
        $data = (array)$request->getParams();
        /** Save personal data */
        $user = $this->em->getRepository(User::class)->findOneBy(['id' => $data['user']]);
        $user->setName($data['name'])->setEmail($data['email']);
        //valida
        $active = $this->em->getRepository(Transaction::class)->activeUpdatePersonalData($user);
        $contribution = $this->em->getRepository(PersonSignature::class)->findOneBy(['tbPessoaId' => $user->getId(), 'origin' => 2, 'status' => 'created']);
        if ($active || $contribution->getFormaPagamento() == 2 || $user->getExemption() == 1) $user->setUltimaAtualizacao(new \Datetime());
        //save data
        $this->em->getRepository(User::class)->save($user);
        /** Save cellphone */
        $cellPhone = $this->em->getRepository(Phone::class)->findOneBy(['tbPessoaId' => $user->getId(), 'tbTipoTelefoneId' => 3]);
        $cellPhone = $cellPhone ? $cellPhone : $cellPhone = new Phone();
        $cellPhone->setTbPessoaId($user->getId())->setTbTipoTelefoneId(3)->setPais($data['cellPhoneIso2'])->setDdi($data['cellPhoneDialCode'])->setTelefone($data['cellPhone']);
        $this->em->getRepository(Phone::class)->save($cellPhone);
        /** Save address */
        $address = $this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()]);
        if ($address) {
            $address->setCityId($data['city'])->setStateId($data['uf']);
            $this->em->getRepository(Address::class)->save($address);
        }
        $this->em->commit();
    }

    public function updateFinish(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $user->setUltimaAtualizacao(new \Datetime());
            $this->em->getRepository(User::class)->save($user);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Dados registrados com sucesso, o NOVO agradece sua participação!",
            ], 201)->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function recommendation(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $this->redirectByPermissions();
        $confirmed = $this->em->getRepository(Indicator::class)->check($user, 1);
        $pending = $this->em->getRepository(Indicator::class)->check($user, 2);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'users/recommendation.phtml', 'section' => 'recommendation',
            'user' => $user, 'confirmed' => count($confirmed), 'pending' => count($pending)]);
    }

    public function listRecommendation(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $index = $request->getQueryParam('index');
        $name = $request->getQueryParam('name');
        $status = $request->getQueryParam('status');
        $indications = $this->em->getRepository(User::class)->listIndications($user, $name, $status, 20, $index * 20);
        $total = count($this->em->getRepository(User::class)->listIndicationsTotal($user, $name, $status));
        $partial = ($index * 20) + sizeof($indications);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status'  => 'ok',
            'message' => $indications,
            'total'   => $total,
            'partial' => $partial,
        ], 200)->withHeader('Content-type', 'application/json');
    }

    public function updateStatus(Request $request, Response $response)
    {
        $this->em->beginTransaction();
        $data = (array)$request->getParams();
        $user = $this->em->getRepository(User::class)->findOneBy(['cpf' => Utils::formatCpf($data['cpf'])]);
        if(!$user) die();
        $user->setStatus($data['status']);
        $this->em->getRepository(User::class)->save($user);
        
        /*-----Hubspot/Mailchimp-----*/
        $this->updateStatusRegister($user);

        $this->em->commit();
    }

    public function fileRecommendation(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $file = $request->getUploadedFiles() ? $request->getUploadedFiles(): null;
            if($file['file']->getClientFilename() != "") {
                $file = $file['file'];
                if($file->getSize() > 5057154) throw new \Exception('Limite máximo do tamanho do arquivo deve ser de 5MB!');
                $way = $file->getClientFilename();
                $name = explode(".", $file->getClientFilename());
                $ext = array_pop($name);
                $ext = strtolower($ext);
                $folder = 'uploads/indicacoes/';
                $file->moveTo($folder . $file->getClientFilename());
            } else {
                throw new \Exception("Arquivo não encontrado!");
            }
            $indicatorFiles = $this->em->getRepository(IndicatorFiles::class)->findOneBy(['user' => $user]);
            if(!$indicatorFiles) $indicatorFiles = new IndicatorFiles();
            $indicatorFiles->setCreated_at(new \Datetime())
                            ->setUser($user)
                            ->setFile($folder . $way);
            $this->em->getRepository(IndicatorFiles::class)->save($indicatorFiles);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Arquivo salvo com sucesso",
        ], 201)
            ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function disaffiliationLetter(Request $request, Response $response)
    {
        $user = $this->getLogged();
        if($user->getFiliado() != 9) $this->redirectByPermissions();
        setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');
        $date = strftime('%A, %d de %B de %Y', strtotime('today'));
        return $this->renderer->render($response, 'users/disaffiliationLetter.phtml', ['section' => 'home',
            'user' => $user, 'date' => $date]);
    }

    public function getDataIA(Request $request, Response $response)
    {
        try {
            //Pega parâmetro da requisição
            $value = $request->getAttribute('route')->getArgument('value');
            if (empty($value)) throw new \InvalidArgumentException("Parâmetro inválido!");

            //Pega usuário pelo email
            $user = $this->em->getRepository(User::class)->getDataIA($value);
            if (!$user) throw new \Exception("Cadastro não encontrado!");

            //Pega transação do usuário
            $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['id' => $user['transaction']]);
            if (!$transaction) throw new \Exception("Transação não encontrada!");

            //Pega total de débitos do usuário
            $total = $this->em->getRepository(Transaction::class)->getTotalDebit($user['id'])['total'] ?? 0;

            //Retorna dados do usuário
            $arr = [
                    // Dados de Identificação
                    'nome' => $user['nome'] ?? '',
                    'email' => $user['email'] ?? '',
                    'cpf' => $user['cpf'] ?? '',
                    'telefone' => $user['phone'] ?? '',
                    'data_nascimento' => $user['birth_date'] ?? '',

                    // Dados da Filiação
                    'situação_de_filiado' => $user['filiado'] ?? '',
                    'data_de_filiacao' => $user['data_filiacao'] ?? '',
                    'data_de_desfiliacao' => $user['data_desfiliacao'] ?? '',
                    'estado_de_votacao' => $user['state'] ?? '',
                    'cidade_de_votacao' => $user['city'] ?? '',
                    'forma_pagamento' => $user['forma_pagamento'] ?? '',
                    'valor_assinatura' => number_format($user['valor_assinatura'] ?? 0, 2, ',', '.'),
                    'proxima_cobranca' => $user['proxima_cobranca'] ?? '',
                    'cartao_credito' => !empty($user['card_brand']) ? "Final " . $user['card_brand'] : '',
                    'expiração_cartao' => $user['card_expiration'] ?? '',
                    'valor_total_pendente' => "R$ " . number_format($total, 2, ',', '.'),
                    'periodo_total_inadimplente' => $user['periodo_total_inadimplente'] ?? '',
            ];

            //Pega dados da transação atual
            if(in_array($transaction->getStatus(), ['expired', 'pending'])) {
                $arr['valor_atual_pendente'] = "R$ " . number_format($transaction->getValor() ?? 0, 2, ',', '.');
                $arr['codigo_de_barra_boleto_atual'] = $transaction->getCodigoBarras() ?? '';
                $arr['link_boleto_atual'] = $transaction->getUrl() ?? '';
                $arr['pagamento_com_cartao'] = "https://novo.org.br/faturas/" . base64_encode($transaction->getId());
                $arr['qrcode_pix'] = $transaction->getQrcode() ?? '';
                //$arr['link_cadastra_cartão'] = "https://novo.org.br/atualizacao-de-cartao-de-credito/?transaction=" . base64_encode($transaction->getId());
            }

            //Retorna dados do usuário
            return $response->withJson([
                'status' => 'ok',
                'message' => $arr,
                ], 200)
            ->withHeader('Content-type', 'application/json');

        } catch (\InvalidArgumentException $e) {
            return $response
                ->withJson([
                    'status' => 'error',
                    'message' => $e->getMessage(),
                ], 400)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response
                ->withJson([
                    'status' => 'error',
                    'message' => $e->getMessage(),
                ], 500)
                ->withHeader('Content-type', 'application/json');
        }
    }
}