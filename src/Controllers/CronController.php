<?php

namespace App\Controllers;

use App\Models\Entities\AuthCharge;
use App\Models\Entities\PersonSignature;
use App\Models\Entities\Transaction;
use App\Models\Entities\User;
use App\Models\Entities\Directory;
use App\Models\Entities\Address;
use App\Models\Entities\PersonIugu;
use App\Models\Entities\EventsTransactions;
use App\Models\Entities\EventsParticipants;
use App\Models\Entities\Cron;
use App\Models\Entities\Events;
use App\Models\Entities\ProcessCep;
use App\Models\Entities\IuguTokens;
use App\Models\Entities\PersonSignatureInstallments;
use App\Models\Entities\Plan;
use App\Models\Entities\CogmoPhones;
use App\Models\Entities\CogmoSchedule;
use App\Models\Entities\PersonCreditCard;
use App\Services\Auth;
use App\Services\IuguService;
use App\Services\Mailchimp;
use App\Services\MaxiPago;
use App\Services\Email;
use App\Services\Hubspot;
use App\Services\WhatsService;
use App\Services\BotConversa;
use App\Services\CogmoBot;
use App\Helpers\Messages;
use App\Helpers\Validator;
use App\Helpers\Utils;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class CronController extends Controller
{
    private string $token = 'qj2n50OhznQwze9uLKiG';

    private function validToken($task, $request)
    {
        $header = $request->getHeaders();
        if (!isset($header['HTTP_TOKEN'][0]) || $header['HTTP_TOKEN'][0] != $this->token) die('token invalido');
        $assignment = $this->em->getRepository(Cron::class)->check($task, date('Y-m-d'));
        if ($assignment) die('já executou');
        $assignment = new Cron();
        $assignment->setTarefa($task)->setData(new \DateTime());
        $this->em->getRepository(Cron::class)->save($assignment);
    }

    public function membershipNotice(Request $request, Response $response)
    {
        $this->validToken(6, $request);
        $records = $this->em->getRepository(User::class)->challengePeriod();
        $group = array();
        foreach ($records as $value) {
            $group[$value["email"]][] = $value;
        }
        foreach ($group as $key => $value2) {
            $subject = "Novos pedidos de filiação!";
            $msg = Messages::membershipNotice($value2);
            Email::send($key, '', $subject, $msg);
        }
        echo 'Rotina concluída!';
    }

    public function refiliation(Request $request, Response $response)
    {
        $this->validToken(9, $request);
        $records = $this->em->getRepository(User::class)->refiliation();
        foreach ($records as $value) {
            $group[$value["email"]][] = $value;
        }
        foreach ($group as $key => $value2) {
            $subject = "Refiliação(ões) em andamento aguardando aprovação!";
            $msg = Messages::refiliation($value2);
            Email::send($key, '', $subject, $msg);
        }
        foreach ($records as $record) {
            $request = $this->em->getRepository(User::class)->findOneBy(['id' => $record['id']]);
            $request->setObservacao('aguardando aprovacao refiliacao');
            $this->em->getRepository(User::class)->save($request);
        }
        echo 'Rotina concluída!';
    }

    public function endsEvent(Request $request, Response $response)
    {
        $this->validToken(10, $request);
        $records = $this->em->getRepository(Events::class)->endsEvent();
        foreach ($records as $record) {
            $event = $this->em->getRepository(Events::class)->findOneBy(['id' => $record['id']]);
            $status = $event->getDateFinal() < new \Datetime() ? 'finalizado' : 'pendente';
            $event->setStatus($status);
            $this->em->getRepository(Events::class)->save($event);
        }
        echo 'Rotina concluída!';
    }

    public function affiliation(Request $request, Response $response)
    {
        $this->validToken(11, $request);
        $records = $this->em->getRepository(User::class)->affiliation();

        foreach ($records as $record) {
            $lastTransaction = $this->em->getRepository(Transaction::class)->getDateLastTransaction($record['id'])[0];
            $dateSolicitation = new \Datetime($record['data_solicitacao_filiacao']);
            $days = $dateSolicitation->diff(new \Datetime())->days;
            $affiliationDate = $days > 9 ? $lastTransaction['data_pago'] : $record['data_solicitacao_filiacao'];
            $lastAffiliatedIdFree = $this->em->getRepository(User::class)->getLastAffiliatedIdFree()[0]['id'];

            /*-----Aprova filiação-----*/
            $affiliation = $this->em->getRepository(User::class)->findOneby(['id' => $record['id']]);
            $affiliation->SetFiliadoId($lastAffiliatedIdFree)
                ->setFiliado(7)
                ->setDataFiliacao(new \Datetime($affiliationDate))
                ->setUltimaAtualizacao(new \Datetime())
                ->setStatus(9)
                ->setObservacao(null);
            if (!$affiliation->getTokenAd()) $affiliation->setTokenAd(Utils::generateToken(22) . md5(date('Y-m-d H:i:s')));
            $this->em->getRepository(User::class)->save($affiliation);

            /*-----Torna a proxima data de geração de fatura com base na aprovação-----*/
            $signature = $this->em->getRepository(PersonSignature::class)->findOneby(['tbPessoaId' => $record['id'], 'origin' => 2]);
            $array = ['dia_ciclo' => date('d'), 'periodicidade' => $signature->getPeriodicidade(), 'data_gera_fatura' => date('Y-m-d')];
            $nextCharge = $this->nextCharge($array);
            $signature->setStatus('created')->setDataGeraFatura(new \Datetime($nextCharge))->setDiaCiclo(date('d'))->setMesCiclo(date('m'));
            if ($signature->getPeriodicidade() == 1 && $signature->getValor() < MINMONTHLY) {
                $signature->setValor(MINMONTHLY);
            } else if ($signature->getPeriodicidade() == 6 && $signature->getValor() < MINSEMESTER) {
                $signature->setValor(MINSEMESTER);
            } else if ($signature->getPeriodicidade() == 12 && $signature->getValor() < MINYEARLY) {
                $signature->setValor(MINYEARLY);
            }
            $this->em->getRepository(PersonSignature::class)->save($signature);

            /*-----Torna a data de referencia do primeiro pagamento com base na aprovação-----*/
            $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['id' => $record['transactionId']]);
            $transaction->setDataCriacao(new \Datetime);
            $this->em->getRepository(Transaction::class)->save($transaction);
            
            /*-----Hubspot/Mailchimp-----*/
            $hp = [];
            $hp['email'] = $affiliation->getEmail();
            $hp['filiado'] = $affiliation->getFiliadoString();
            $hp['status_de_filiado'] = $affiliation->getFiliadoString();
            $hp['status_cadastro'] = $affiliation->getStatusString();
            $hp['filiado_id'] = $lastAffiliatedIdFree;
            $hp['data_filiado'] = date('Y-m-d', strtotime($affiliationDate));
            $hp["state"] = $affiliation->getTituloEleitoralUfId()->getSigla();
            $hp["city"] = $affiliation->getTituloEleitoralMunicipioId()->getCidade();
            $hp['sexo_'] = $affiliation->getGenero()->getGenero();
            $hp['tokenpesquisa'] = $affiliation->getTokenAd();
            $hp['isento'] = 'NÃO';
            Hubspot::updateContact($affiliation, $hp);
            //Hubspot::insertIntoTheFlow($affiliation, 57289643);
            $body = [];
            $body['email_address'] = $affiliation->getEmail();
            $body['merge_fields']['FILIADO'] = $affiliation->getFiliadoString();
            $body['merge_fields']['MMERGE63'] = $affiliation->getFiliadoString();
            $body['merge_fields']['MMERGE58'] = $affiliation->getStatusString();
            $body['merge_fields']['FILIADO_ID'] = $lastAffiliatedIdFree;
            $body['merge_fields']['FILIACAO'] = date('d/m/Y', strtotime($affiliationDate));
            $body['merge_fields']['UF_TITULO'] = $affiliation->getTituloEleitoralUfId()->getSigla();
            $body['merge_fields']['MUN_TITULO'] = $affiliation->getTituloEleitoralMunicipioId()->getCidade();
            $body['merge_fields']['GENERO'] = $affiliation->getGenero()->getGenero();
            $body['merge_fields']['PESQUISA'] = $affiliation->getTokenAd();
            $body['merge_fields']['ISENTO'] = 'NÃO';
            $body['merge_fields']['ADDRESS'] = '';
            Mailchimp::updateContact($body);

            /*-----E-mail novo filiado-----*/
            $name = explode(' ', $record['name'])[0];
            $subject = "Filiação ao NOVO confirmada";
            $msg = Messages::affiliation($name, $lastAffiliatedIdFree, $affiliation->getTermoAceite0());
            Email::send($record['email'], $record['name'], $subject, $msg);

            /*-----E-mail Filiação-----*/
            $subject = "Nova Filiação – EMAIL: " . $record['email'];
            $msg = Messages::affiliationFiliation($record, date('d/m/Y', strtotime($affiliationDate)));
            Email::send('refiliacao@novo.org.br', $record['name'], $subject, $msg, null, null, null, null);
            
            /*-----Verifica se possui processo na CEP-----*/
            $cep = $this->em->getRepository(ProcessCep::class)->findOneBy(['personId' => $record['id'], 'reason' => array('flagged', 'disbanded')]);
            if ($cep) {
                $subject = "Nova filiação com processo na CEP - " . $record['name'];
                $msg = Messages::affiliationCep($record, $cep);
                Email::send('etica@novo.org.br', $record['name'], $subject, $msg);
            }
            $this->registerHistoric($affiliation, 3);
            $this->checkIndicationOfAffiliation($affiliation, 5);
        }
        echo 'Rotina concluída!';
    }

    /**
     * Processa cobranças automáticas de cartão
     * 
     * @param Request $request Request HTTP
     * @param Response $response Response HTTP
     * @return Response
     * @throws \Exception Em caso de erro no processamento
     */
    public function chargeCard(Request $request, Response $response)
    {
        try {
            $this->validToken(1, $request);
            $date = new \Datetime(); 
            $records = $this->em->getRepository(Transaction::class)->chargeCard();

            foreach ($records as $record) {

                $user = $this->em->getRepository(User::class)->findOneBy(['id' => $record['tb_pessoa_id']]);
                if (!$user) {
                    continue;
                }

                $record['nome'] = trim($record['nome']);
                $origin = $record['origem_transacao'];
                if($record['origem_transacao'] == 3) $origin = 8;

                $plan = $record['periodicidade'];
                $obs = $invoiceId = '';
                $result = 'failure';
                $status = "pending";
                $dataCharge = null;

                // Verifica situacao do titulo para registrar como doação caso esteja irregular.
                if (in_array($record['origem_transacao'], [2, 3]) && $record['pendencia_titulo'] == 1) {
                    $origin = 1;
                    $obs = 'F';
                    $plan = 0;
                }
                // Ajusta label da origem.
                $label1Iugu = 'Filiacao';
                if($record['origem_transacao'] == 3) $label1Iugu = 'Plano de contribuição';
                if ($record['origem_transacao'] == 1 || $record['pendencia_titulo'] == 1) $label1Iugu = 'Doacao';

                // Verifica periodicidade.
                if ($record['periodicidade'] == 1 && $record['pendencia_titulo'] == 0) {
                    $label2 = 'Mensal';
                } else if ($record['periodicidade'] == 12 && $record['pendencia_titulo'] == 0) {
                    $label2 = 'Anual';
                } else if (in_array($record['periodicidade'], array(1, 12)) && $record['pendencia_titulo'] == 1) {
                    $label2 = 'Avulsa';
                }

                // Define Label da transação
                $labelIugu = $label1Iugu . " " . $label2;

                // Verifica se já possui alguma transação da origem em questão no mês.
                $duplicity = $this->em->getRepository(Transaction::class)
                    ->avoidDuplication($record['tb_pessoa_id'], 
                                     $record['periodicidade'], 
                         $record['data_gera_fatura'])['total'];

                if ($duplicity == 0) {
                    // Verifica se pagou alguma contribuição nos últimos 8 meses.
                    $active = $this->em->getRepository(Transaction::class)->active($record['tb_pessoa_id']);

                    // Verifica se não está suspenso com alguma paga nos últimos 6 meses ou é anual.
                    if ($record['suspensao'] == 0 && $active || $record['periodicidade'] == 12) {
                        $personIugu = $this->getCustomerIdIUGU($user);
                        $address = $this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $record['tb_pessoa_id']]);

                        // Verifica se Endereço é válido e se não for seta um padrão para processar.
                        $address = $address ? $this->iuguAddress($address) : $this->iuguAddressDonation();
                        $keyIUGU = null;
                        $depositDirectory = 1;

                        // Verifica se é Doação para processar no diretório correto.
                        if($record['origem_transacao'] == 1) {
                            $iuguTokens = $this->getIuguTokenDirectory($record['tb_diretorio_origem']);
                            if($iuguTokens) {
                                $keyIUGU = $iuguTokens['key'];
                                $depositDirectory = $iuguTokens['deposit'];
                            }
                        }

                        $validNewCard = $this->em->getRepository(PersonCreditCard::class)->validNewCard($record['token_cartao_credito']);
                        // Aciona o service do IUGU para cobrança.
                        if ($validNewCard) {
                            $charge = IuguService::charge($personIugu->getCustomerId(), $record['token_cartao_credito'], $user->getEmail(), $user->getCpf(), 
                                $user->getName(), $address, $record['valor'], $labelIugu, 1, $keyIUGU);
                            $customerId = $personIugu->getCustomerId();
                            $result = $charge['status'];
                            $invoiceId = $charge['invoice_id'];
                            if (!$invoiceId) $invoiceId = '';
                        } else {
                            $customerId = $personIugu->getCustomerId();
                            $result = 'failure';
                            $invoiceId = '';
                        }

                        // Verifica se foi cobrada com sucesso.
                        if ($result != 'captured') {
                            if (in_array($record['origem_transacao'], [2, 3])) {
                                $this->updatePaymentStatus($user, 5);
                                Hubspot::insertIntoTheFlow($user, 57110097);
                            }
                        } else {
                            $dataCharge = $date;
                            $status = "paid";
                            if(in_array($record['origem_transacao'], [2, 3])) {
                                $this->updatePaymentStatus($user, 4);
                                Hubspot::insertIntoTheFlow($user, 58676825);
                            } else {
                                Hubspot::insertIntoTheFlow($user, 57075892);
                            }
                        }
                    } else {
                        // Se a pessoa não pagou nada nos últimos 6 meses ou está suspensa é registrada uma transação sem tentativa de cobrança.
                        $status = $record['suspensao'] == 1 ? "canceled" : "pending";
                        if ($record['suspensao'] == 1) $obs = "Filiação suspensa";
                        $invoiceId = '';
                        $customerId = ($record['customer_id']) ?: '';
                        if ($record['suspensao'] == 0) Hubspot::insertIntoTheFlow($user, 62089908);
                    }

                    // Registra nova transação;
                    $transactionData = $this->populateTransaction($user, $record['valor'], $date, $date, $dataCharge, 1, $status, $customerId, $plan, 
                        $record['gateway_pagamento'], $origin, $obs, $record['token_cartao_credito'], $invoiceId, '', $depositDirectory ?? 1, 
                        $record['tb_diretorio_origem'], '', '', $record['id'], null, '');
                    $this->em->getRepository(Transaction::class)->save($transactionData);

                    // Valida se está paga e se é a última para alterar o status de pagamento.
                    if ($result == 'captured') $this->validLastTransaction($user, $transactionData, 4);
                }

                /*-----Atualiza Assinatura-----*/
                $nextCharge = $this->nextCharge($record);
                $subscription = $this->em->getRepository(PersonSignature::class)->findOneBy(['id' => $record['id']]);
                $subscription->setDataGeraFatura(new \DateTime($nextCharge));
                $this->em->getRepository(PersonSignature::class)->save($subscription);

                 // Atualiza dados de integração com Hubspot e Mailchimp                
                 $hp = [];
                 $hp['email'] = $user->getEmail();
                 $hp['firstname'] = $user->getFirstName();
                 $hp['lastname'] = $user->getSurname();
                 $hp['filiado_id'] = $user->getFiliadoId();
                 $hp['filiado'] = $user->getFiliadoString();
                 $hp['status_de_filiado'] = $user->getFiliadoString();   
                 $hp['status_cadastro'] = $user->getStatusString();     
                 $hp['cpf_format'] = strrev(preg_replace('/\d/', '*', strrev($user->getCpf()), 6));
                 $hp['valor'] = number_format($record['valor'], 2, ',', '.');
                 $hp['vencimento'] = date('Y-m-d');
                 $hp["recadastra_cartao_iugu"] = "https://novo.org.br/faturas/" . base64_encode($transactionData->getId());
                 $hp['isento'] = 'NÃO';
                 Hubspot::updateContact($user, $hp);
                 $body = [];
                 $body['email_address'] = $user->getEmail();
                 $body['merge_fields']['FNAME'] = $user->getFirstName();
                 $body['merge_fields']['LNAME'] = $user->getSurname();
                 $body['merge_fields']['FILIADO_ID'] = $user->getFiliadoId();
                 $body['merge_fields']['FILIADO'] = $user->getFiliadoString();
                 $body['merge_fields']['MMERGE63'] = $user->getFiliadoString();  
                 $body['merge_fields']['MMERGE58'] = $user->getStatusString();      
                 $body['merge_fields']['CPF_FORMAT'] = strrev(preg_replace('/\d/', '*', strrev($user->getCpf()), 6));
                 $body['merge_fields']['VALOR'] = number_format($record['valor'], 2, ',', '.');
                 $body['merge_fields']['VENCIMENTO'] = date('d/m/Y');
                 $body['merge_fields']['MMERGE65'] = "https://novo.org.br/faturas/" . base64_encode($transactionData->getId());
                 $body['merge_fields']['ISENTO'] = 'NÃO';
                 $body['merge_fields']['ADDRESS'] = '';
                 Mailchimp::updateContact($body);
            }

            return $response->withJson([
                'status' => 'success',
                'processed' => count($records),
                'date' => $date->format('Y-m-d H:i:s')
            ]);
        
        } catch (\Exception $e) {
            throw new \Exception("Erro ao processar cobrança: " . $e->getMessage());
        }
    }

    public function chargeBillet(Request $request, Response $response)
    {
        try {
            $this->validToken(2, $request);
            $date = new \Datetime(); 
            $records = $this->em->getRepository(Transaction::class)->chargeBillet();

            foreach ($records as $record) {

                $plan = $record['periodicidade'];
                $origin = $record['origem_transacao'];
                if($record['origem_transacao'] == 3) $origin = 8;

                $obs = null;
                $user = $this->em->getRepository(User::class)->findOneBy(['id' => $record['tb_pessoa_id']]);
                $address = $this->iuguAddress($this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $record['tb_pessoa_id']], ['id' => 'DESC']));
                $validadeCep = Validator::validateCep($address);
                if ($validadeCep == false) $address = $this->iuguAddressDonation();
                $dateGenerate = new \Datetime($record['data_gera_fatura']);

                /*-----Verifica situacao do titulo para registrar como doação caso esteja irregular-----*/
                if (in_array($record['origem_transacao'], [2, 3]) && $record['pendencia_titulo'] == 1) {
                    $origin = 1;
                    $obs = 'F';
                    $plan = 0;
                }

                /*-----Ajusta label da origem-----*/
                $label1Iugu = match ($record['origem_transacao']) {
                    1 => 'Doação',
                    2 => 'Filiação',
                    3 => 'Plano de contribuição',
                    default => 'Filiação'
                };

                // Verifica periodicidade
                if ($record['pendencia_titulo'] == 0) {
                    if ($record['periodicidade'] == 1) {
                        $label2 = 'Mensal';
                    } else if ($record['periodicidade'] == 12) {
                        $label2 = 'Anual';
                    } else if($record['periodicidade'] == 24) {
                        $label2 = 'Bienal';
                    }
                } else {
                    $label2 = 'Avulsa';
                }
                $labelIugu = $label1Iugu . " " . $label2;

                /*-----Verifica se já possui alguma transação da origem em questão no mês-----*/
                $duplicity = $this->em->getRepository(Transaction::class)
                    ->avoidDuplication($record['tb_pessoa_id'], 
                                     $record['periodicidade'], 
                         $record['data_gera_fatura'])['total'];
                
                if ($duplicity == 0) {

                    /*-----Verifica se pagou alguma coisa nos últimos 6 meses-----*/
                    $active = $this->em->getRepository(Transaction::class)->active($record['tb_pessoa_id']);
                    if ($active && $record['suspensao'] == 0 || in_array($record['periodicidade'], [6, 12])) {
                        $keyIUGU = null;
                        $depositDirectory = 1;
                        //if($record['origem_transacao'] == 1) {
                        //    $iuguTokens = $this->getIuguTokenDirectory($record['tb_diretorio_origem']);
                        //    if($iuguTokens) {
                        //        $keyIUGU = $iuguTokens['key'];
                        //        $depositDirectory = $iuguTokens['deposit'];
                        //    }
                        //}
                        $slip = IuguService::slip($record['email'], $record['cpf'], $record['nome'], $address, $record['valor'], $labelIugu, 5, $keyIUGU);
                        $url = $slip['bank_slip']['bank_slip_pdf_url'];
                        $barCode = $slip['bank_slip']['digitable_line'];
                        $invoicedId = $slip['id'];
                        $status = 'pending';
                        $pix = $slip['pix']['qrcode_text'];
                        $dueDate = new \Datetime($slip['due_date']);
                    } else {
                        $slip = null;
                        if ($record['suspensao'] == 1) $obs = "Filiação suspensa";
                        $url = $barCode = $invoicedId = $pix = $dueDate = '';
                        $status = $record['suspensao'] == 1 ? "canceled" : "pending";
                        $dueDate = date('Y-m-d H:i:s', strtotime('+5 days'));
                        $dueDate = new \Datetime($dueDate);
                    }

                    /*-----Cria transacao-----*/
                    $transactionData = $this->populateTransaction($user, $record['valor'], $dateGenerate, $dueDate, null, 2, $status, '', $plan, 1, $origin, $obs, '', $invoicedId, '',
                        $depositDirectory ?? 1, $record['tb_diretorio_origem'], $url, $barCode, $record['id'], $pix);
                    $this->em->getRepository(Transaction::class)->save($transactionData);

                    if ($record['suspensao'] == 0) {
                        $urlGenerateBillet = 'https://espaco-novo.novo.org.br/api/financeiro/gera-boleto/?transaction=' . base64_encode($transactionData->getId());
                        $url = $slip ? $url : $urlGenerateBillet;
                        
                        /*-----Hubspot/Mailchimp-----*/
                        $hp = [];
                        $hp['email'] = $user->getEmail();
                        $hp['firstname'] = $user->getFirstName();
                        $hp['lastname'] = $user->getSurname();
                        $hp['filiado_id'] = $user->getFiliadoId();
                        $hp['filiado'] = $user->getFiliadoString();
                        $hp['status_de_filiado'] = $user->getFiliadoString();
                        $hp['status_cadastro'] = $user->getStatusString();     
                        $hp['cpf_format'] = strrev(preg_replace('/\d/', '*', strrev($user->getCpf()), 6));
                        $hp['valor'] = $transactionData->getValue();
                        $hp['vencimento'] = $dueDate->format('Y-m-d');
                        $hp['link'] = $url;
                        $hp["recadastra_cartao_iugu"] = "https://novo.org.br/atualiza/" . base64_encode($user->getId());
                        $hp['status_pagamento'] = 'Boleto - Gerado';
                        $hp['isento'] = 'NÃO';
                        $hp['pix_boleto'] = $pix;
                        Hubspot::updateContact($user, $hp);
                        Hubspot::insertIntoTheFlow($user, 57164220); // Insere no Fluxo
                        $body = [];
                        $body['email_address'] = $user->getEmail();
                        $body['merge_fields']['FNAME'] = $user->getFirstName();
                        $body['merge_fields']['LNAME'] = $user->getSurname();
                        $body['merge_fields']['FILIADO_ID'] = $user->getFiliadoId();
                        $body['merge_fields']['FILIADO'] = $user->getFiliadoString();
                        $body['merge_fields']['MMERGE63'] = $user->getFiliadoString();  
                        $body['merge_fields']['MMERGE58'] = $user->getStatusString();      
                        $body['merge_fields']['CPF_FORMAT'] = strrev(preg_replace('/\d/', '*', strrev($user->getCpf()), 6));
                        $body['merge_fields']['VALOR'] = $transactionData->getValue();
                        $body['merge_fields']['VENCIMENTO'] = $dueDate->format('d/m/Y');
                        $body['merge_fields']['LINKBOLETO'] = $url;
                        $body['merge_fields']['MMERGE65'] = "https://novo.org.br/atualiza/" . base64_encode($user->getId());
                        $body['merge_fields']['MMERGE59'] = 'Boleto - Gerado';
                        $body['merge_fields']['ISENTO'] = 'NÃO';
                        $body['merge_fields']['ADDRESS'] = '';
                        Mailchimp::updateContact($body);
                    }
                }

                /*-----Assinatura-----*/
                $nextCharge = $this->nextCharge($record);
                $subscription = $this->em->getRepository(PersonSignature::class)->findOneBy(['id' => $record['id']]);
                $subscription->setDataGeraFatura(new \DateTime($nextCharge));
                $this->em->getRepository(PersonSignature::class)->save($subscription);

            }

            return $response->withJson([
                'status' => 'success',
                'processed' => count($records),
                'date' => $date->format('Y-m-d H:i:s')
            ]);

        } catch (\Exception $e) {
            throw new \Exception("Erro ao processar cobrança: " . $e->getMessage());
        }
    }

    public function reprocessChargeCard(Request $request, Response $response)
    {
        $this->validToken(4, $request);
        $date = new \Datetime();
        $records = $this->em->getRepository(Transaction::class)->reprocessChargeCard();
        foreach ($records as $record) {
            $user = $this->em->getRepository(User::class)->findOneBy(['id' => $record['tb_pessoa_id']]);

            /*-----Hubspot/Mailchimp-----*/
            $hp = [];
            $hp['email'] = $user->getEmail();
            $hp['firstname'] = $user->getFirstName();
            $hp['lastname'] = $user->getSurname();
            $hp['filiado_id'] = $user->getFiliadoId();
            $hp['filiado'] = $user->getFiliadoString();
            $hp['status_de_filiado'] = $user->getFiliadoString();    
            $hp['status_cadastro'] = $user->getStatusString();    
            $hp['cpf_format'] = strrev(preg_replace('/\d/', '*', strrev($user->getCpf()), 6));
            $hp['valor'] = number_format($record['valor'], 2, ',', '.');
            $hp['vencimento'] = date('Y-m-d');
            $hp["recadastra_cartao_iugu"] = "https://novo.org.br/atualiza/" . base64_encode($user->getId());
            $hp['isento'] = 'NÃO';
            Hubspot::updateContact($user, $hp);
            $body = [];
            $body['email_address'] = $user->getEmail();
            $body['merge_fields']['FNAME'] = $user->getFirstName();
            $body['merge_fields']['LNAME'] = $user->getSurname();
            $body['merge_fields']['FILIADO_ID'] = $user->getFiliadoId();
            $body['merge_fields']['FILIADO'] = $user->getFiliadoString();
            $body['merge_fields']['MMERGE63'] = $user->getFiliadoString();   
            $body['merge_fields']['MMERGE58'] = $user->getStatusString();     
            $body['merge_fields']['CPF_FORMAT'] = strrev(preg_replace('/\d/', '*', strrev($user->getCpf()), 6));
            $body['merge_fields']['VALOR'] = number_format($record['valor'], 2, ',', '.');
            $body['merge_fields']['VENCIMENTO'] = date('d/m/Y');
            $body['merge_fields']['MMERGE65'] = "https://novo.org.br/atualiza/" . base64_encode($user->getId());
            $body['merge_fields']['ISENTO'] = 'NÃO';
            $body['merge_fields']['ADDRESS'] = '';
            Mailchimp::updateContact($body);

            $invoiceId = '';
            $result = 'failure';
            $active = $this->em->getRepository(Transaction::class)->active($user->getId());             
            if ($active && $record['token_cartao_credito'] != null) {
                $personIugu = $this->getCustomerIdIUGU($user);
                $address = $this->iuguAddress($this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()]));
                $keyIUGU = null;
                if($record['origem_transacao'] == 1) {
                    $iuguTokens = $this->getIuguTokenDirectory($record['tb_diretorio_origem']);
                    if($iuguTokens) $keyIUGU = $iuguTokens['key'];
                }

                $charge = IuguService::charge($personIugu->getCustomerId(), 
                                              $record['token_cartao_credito'], 
                                                  $user->getEmail(), 
                                                    $user->getCpf(), 
                                                   $user->getName(), 
                                                $address, 
                                                  $record['valor'], 
                                            '', 
                                                   1, 
                                                    $keyIUGU);
                $result = $charge['status'];
                $invoiceId = $charge['invoice_id'];
            }

            if ($result == 'captured') {
                $status = 'paid';
                $dateCharge = $date;                    
                if (in_array($record['origem_transacao'], [2,3])) {
                    $this->updatePaymentStatus($user, 4);
                    Hubspot::insertIntoTheFlow($user, 58676825);
                } else {
                    Hubspot::insertIntoTheFlow($user, 57075892);
                }
            } else {
                $status = 'pending';
                $dateCharge = null;
                if (in_array($record['origem_transacao'], [2,3])) {
                    $this->updatePaymentStatus($user, 5);
                    Hubspot::insertIntoTheFlow($user, 57110097);
                }
            }

            $phase = match ((int)$record['days']) {
                2 => 2,
                4 => 3,
                10 => 4,
                default => 0
            };

            $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['id' => $record['id']]);
            $transaction->setUltimaAtualizacao($date)
                ->setStatus($status)
                ->setDataPago($dateCharge)
                ->setFaseCobranca($phase)
                ->setGatewayPagamento($record['gateway_pagamento'])
                ->setInvoiceId($invoiceId);
            if($status == 'paid') {
                $transaction->setNumeroRecibo(
                    $this->getReceiptDirectory(
                        $this->em->getReference(Directory::class, $transaction->getTbDiretorioId()), $transaction));
            }
            $this->em->getRepository(Transaction::class)->save($transaction);

            if ($result == 'captured') $this->validLastTransaction($user, $transaction, 4);
        }
        echo 'Rotina concluída!';
    }

    public function reprocessChargeBillet(Request $request, Response $response)
    {
        $this->validToken(3, $request);
        $records = $this->em->getRepository(Transaction::class)->reprocessChargeBillet();

        foreach ($records as $record) {
            $user = $this->em->getRepository(User::class)->find($record['tb_pessoa_id']);
            $url = $record['url'] ?: 'https://espaco-novo.novo.org.br/api/financeiro/gera-boleto/?transaction=' . base64_encode($record['id']);
            
            /*-----Atualiza transacao-----*/
            $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['id' => $record['id']]);
            $transaction->setUltimaAtualizacao(new \Datetime());
            $this->em->getRepository(Transaction::class)->save($transaction);

            /*-----Hubspot/Mailchimp-----*/
            $hp = [];
            $hp['email'] = $user->getEmail();
            $hp['link'] = $url;
            $hp["recadastra_cartao_iugu"] = "https://novo.org.br/atualiza/" . base64_encode($user->getId());
            $hp['status_pagamento'] = 'Boleto - Vencido';
            $hp['isento'] = 'NÃO';
            Hubspot::updateContact($user, $hp);
            Hubspot::insertIntoTheFlow($user, 57164703); // Insere no fluxo do Hubspot

            $body = [];
            $body['email_address'] = $user->getEmail();
            $body['merge_fields']['LINKBOLETO'] = $url;
            $body['merge_fields']['MMERGE65'] = "https://novo.org.br/atualiza/" . base64_encode($user->getId());
            $body['merge_fields']['MMERGE59'] = 'Boleto - Vencido';
            $body['merge_fields']['ISENTO'] = 'NÃO';
            $body['merge_fields']['ADDRESS'] = '';
            Mailchimp::updateContact($body);
        }

        echo 'Rotina concluída!';
    }

    public function refreshInvoice(Request $request, Response $response)
    {
        /*-----Autenticação-----*/
        $header = $request->getHeaders();
        if (!isset($header['HTTP_AUTHORIZATION'][0]) || $header['HTTP_AUTHORIZATION'][0] != $this->token) die('token invalido');
        /*-----Registra execução-----*/
        $date = new \DateTime();
        $exeCron = $this->em->getRepository(Cron::class)->check(12, date('Y-m-d'));
        if (!$exeCron) {
            $assignment = new Cron();
            $assignment->setTarefa(12)->setData($date);
            $this->em->getRepository(Cron::class)->save($assignment);
        }
        /*-----Inicia processamento-----*/
        $data = (array)$request->getParams();
        $status = $data['data']['status'];
        $paymentMethod = match ($data['data']['payment_method']) {
            'iugu_pix' => 11,
            'iugu_bank_slip' => 2,
            'iugu_credit_card' => 1,
            default => ''
        };

        $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['invoiceId' => $data['data']['id']]);
        $keyIUGU = null;
        $directory = $this->em->getRepository(IuguTokens::class)->findOneBy(['account' => $data['data']['account_id']]);
        $iuguTokens = $this->getIuguTokenDirectory($directory->getDirectory());
        if($iuguTokens) $keyIUGU = $iuguTokens['key'];
        $invoice = IuguService::find($data['data']['id'], $keyIUGU);
        /*-----Verifica se fatura foi paga diretamente pela tela do IUGU através do cartão de crédito-----*/
        if ($paymentMethod == 1) {
            $i = 0;
            foreach ($invoice['logs'] as $logs) {
                if (in_array($logs['description'], ['Fatura visualizada!', 'Invoice viewed!'])) $i++;
            }
            if ($i == 0) die();
        }
        /*-----Se não encontra, uma nova transação é criada------*/
        if (!$transaction) {
            $total = floatVal(str_replace('R$ ', '', str_replace(',', '.', $invoice['total'])));
            $created = new \Datetime(date('Y-m-d', strtotime($invoice['created_at_iso'])));
            $payment = $invoice['paid_at'] ? new \Datetime(date('Y-m-d', strtotime($invoice['paid_at']))) : '';
            $user = $this->em->getRepository(User::class)->findOneBy(['email' => $invoice['email']]);
            $transaction = $this->populateTransaction($user, $total, $created, new \Datetime($invoice['due_date']), $payment, $paymentMethod, $invoice['status'], '', 0, 1, 1, 'Inclusão IUGU',
                '', $data['data']['id'], '', 1, 1, $invoice['bank_slip']['bank_slip_pdf_url'], '', '', '');
            $this->em->getRepository(Transaction::class)->save($transaction);
        } else {
            $user = $this->em->getRepository(User::class)->findOneBy(['id' => $transaction->getTbPessoaId()]);
        }
        $filiadoId = $user->getFiliadoId() ?: '';
        $firstName = explode(' ', $user->getName())[0];
        /*-----Gatilho com alteração de invoice, fatura-----*/
        if ($data["event"] == "invoice.status_changed") {
            if ($transaction->getStatus() != $status) {
                $transaction->setStatus($status)
                    ->setUltimaAtualizacao($date)
                    ->setFormaPagamento($paymentMethod);
                if ($status == 'paid') {
                    $transaction->setDataPago($date)
                        ->setNumeroRecibo($this->getReceiptDirectory($this->em->getReference(Directory::class, $transaction->getTbDiretorioId()), $transaction));
                    $type = $paymentMethod == 1 ? 4 : 1;
                    $this->validLastTransaction($user, $transaction, $type);
                }
                $this->em->getRepository(Transaction::class)->save($transaction);
            }
            /*-----Atualiza tabela de transações de eventos-----*/
            $transaction_event = $this->em->getRepository(EventsTransactions::class)->findOneBy(['tbTransacaoId' => $transaction->getId()]);
            if ($transaction_event && $status == 'paid') {
                if ($transaction_event) {
                    $subscriptions = $this->em->getRepository(EventsParticipants::class)->findBy(['tbEventosTransacaoId' => $transaction_event->getId(), 'tbPessoaId' => $transaction->getTbPessoaId()]);
                    foreach ($subscriptions as $subscription) {
                        $subscription->setStatus('confirmado');
                        $this->em->getRepository(EventsParticipants::class)->save($subscription);
                    }
                    $transaction_event->setStatus('confirmado');
                    $this->em->getRepository(EventsTransactions::class)->save($transaction_event);
                    $event = $this->em->getRepository(Events::class)->findOneBy(['id' => $transaction_event->getTbEventosId()]);
                    $msg = Messages::refreshInvoice($firstName, $event, 'contribuição');
                    if ($event->getSpecie() == 'tradicional') $msg = Messages::refreshInvoice($firstName, $event, 'inscrição');
                    Email::send($user->getEmail(), $firstName, "Compra " . $event->getName() . " Efetivada", $msg);
                }
            }
        }
        /*-----Verifica se status da fatura está paga-----*/
        if ($data['data']['status'] == 'paid') {
            /*-----Verifica se fatura paga e de filiação e se o cadastro está aguardando pagamento-----*/
            if ($transaction->getOrigemTransacao() == 2 && in_array($user->getFiliado(), array(1, 3))) {
                /*-----Atualiza o cadastro para período de impugnação e envia e-mail informativo-----*/
                $statusAffiliated = $user->getFiliado() == 1 ? 2 : 14;
                $user->setFiliado($statusAffiliated);
                if ($statusAffiliated == 2) {
                    //$user->setDataSolicitacaoFiliacao($date);
                    $type = 2;
                } else {
                    //$user->setDataSolicitacaoRefiliacao($date);
                    $type = 5;
                }
                $this->em->getRepository(User::class)->save($user);
                
                /*-----Hubspot/Mailchimp-----*/
                $hp = [];
                $hp['email'] = $user->getEmail();
                $hp['filiado'] = $user->getFiliadoString();
                $hp['status_de_filiado'] = $user->getFiliadoString();
                $hp['status_cadastro'] = $user->getStatusString();
                $hp['isento'] = $user->getExemption() == 1 ? 'SIM' : 'NÃO';
                $hp['plano_atual'] = $user->getPaymentPlanStr();
                $hp['data_da_ultima_mensalidade_paga'] = $date->format('Y-m-d');
                Hubspot::updateContact($user, $hp);
                
                $body = [];
                $body['email_address'] = $user->getEmail();
                $body['merge_fields']['FILIADO'] = $user->getFiliadoString();
                $body['merge_fields']['MMERGE63'] = $user->getFiliadoString();
                $body['merge_fields']['MMERGE58'] = $user->getStatusString();
                $body['merge_fields']['ISENTO'] = $user->getExemption() == 1 ? 'SIM' : 'NÃO';
                $body['merge_fields']['ADDRESS'] = '';
                $body['merge_fields']['PLANO'] = $user->getPaymentPlanStr();
                $body['merge_fields']['ULTIMAPAGA'] = $date->format('d/m/Y');
                Mailchimp::updateContact($body);

                $this->registerHistoric($user, $type);
                $subject = "Pagamento de filiação confirmado!";
                $msg = Messages::affiliationPaymentConfirmed($firstName);
                Email::send($user->getEmail(), $firstName, $subject, $msg);
                $this->checkIndicationOfAffiliation($user, 4);
            }
        }
    }

    public function exemptionAffiliation(Request $request, Response $response)
    {
        $this->validToken(14, $request);
        $records = $this->em->getRepository(User::class)->exemptionAffiliation();
        foreach ($records as $record) {
            $days = date_diff(new \Datetime($record['data_solicitacao_filiacao']), new \Datetime())->days;
            $affiliationDate = $days > 9 ? new \Datetime() : new \Datetime($record['data_solicitacao_filiacao']);
            if ($record['filiado'] == 2) {
                $lastAffiliatedIdFree = $this->em->getRepository(User::class)->getLastAffiliatedIdFree()[0]['id'];
                /*-----Aprova filiação-----*/
                $affiliation = $this->em->getRepository(User::class)->findOneby(['id' => $record['id']]);
                $affiliation->SetFiliadoId($lastAffiliatedIdFree)
                    ->setFiliado(7)
                    ->setDataFiliacao($affiliationDate)
                    ->setUltimaAtualizacao(new \Datetime())
                    ->setStatus(12)
                    ->setObservacao(null);
                if (!$affiliation->getTokenAd()) $affiliation->setTokenAd(Utils::generateToken(22) . md5(date('Y-m-d H:i:s')));
                $this->em->getRepository(User::class)->save($affiliation);
                /*-----Torna a proxima data de geração de fatura com base na aprovação-----*/
                $signature = $this->em->getRepository(PersonSignature::class)->findOneby(['tbPessoaId' => $record['id'], 'origin' => 2]);
                $array = ['dia_ciclo' => date('d'), 'periodicidade' => $signature->getPeriodicidade(), 'data_gera_fatura' => date('Y-m-d')];
                $nextCharge = $this->nextCharge($array);
                $signature->setStatus('created')->setDataGeraFatura(new \Datetime($nextCharge))->setDiaCiclo(date('d'))->setMesCiclo(date('m'));
                $this->em->getRepository(PersonSignature::class)->save($signature);
                
                /*-----Hubspot/Mailchimp-----*/
                $hp = [];
                $hp['email'] = $affiliation->getEmail();
                $hp['filiado'] = $affiliation->getFiliadoString();
                $hp['status_de_filiado'] = $affiliation->getFiliadoString();
                $hp['status_cadastro'] = $affiliation->getStatusString();
                $hp['filiado_id'] = $lastAffiliatedIdFree;
                $hp['data_filiado'] = $affiliationDate->format('Y-m-d');
                $hp["state"] = $affiliation->getTituloEleitoralUfId()->getSigla();
                $hp["city"] = $affiliation->getTituloEleitoralMunicipioId()->getCidade();
                $hp['tokenpesquisa'] = $affiliation->getTokenAd();
                $hp['isento'] = 'SIM';
                Hubspot::updateContact($affiliation, $hp);
                //Hubspot::insertIntoTheFlow($affiliation, 57289643);
                $body = [];
                $body['email_address'] = $affiliation->getEmail();
                $body['merge_fields']['FILIADO'] = $affiliation->getFiliadoString();
                $body['merge_fields']['MMERGE63'] = $affiliation->getFiliadoString();
                $body['merge_fields']['MMERGE58'] = $affiliation->getStatusString();
                $body['merge_fields']['FILIADO_ID'] = $lastAffiliatedIdFree;
                $body['merge_fields']['FILIACAO'] = $affiliationDate->format('d/m/Y');
                $body['merge_fields']['UF_TITULO'] = $affiliation->getTituloEleitoralUfId()->getSigla();
                $body['merge_fields']['MUN_TITULO'] = $affiliation->getTituloEleitoralMunicipioId()->getCidade();
                $body['merge_fields']['PESQUISA'] = $affiliation->getTokenAd();
                $body['merge_fields']['ISENTO'] = 'SIM';
                $body['merge_fields']['ADDRESS'] = '';
                Mailchimp::updateContact($body);

                /*-----E-mail novo filiado-----*/
                $name = explode(' ', $record['name'])[0];
                $subject = "Filiação ao NOVO confirmada";
                $msg = Messages::affiliation($name, $lastAffiliatedIdFree, $affiliation->getTermoAceite0());
                Email::send($record['email'], $record['name'], $subject, $msg);
                /*-----E-mail Filiação-----*/
                $subject = "Nova Filiação – EMAIL: " . $record['email'];
                $msg = Messages::affiliationFiliation($record, $affiliationDate->format('d/m/Y'));
                Email::send('refiliacao@novo.org.br', $record['name'], $subject, $msg);
                /*-----Verifica se possui processo na CEP-----*/
                $cep = $this->em->getRepository(ProcessCep::class)->findOneBy(['personId' => $record['id'], 'reason' => array('flagged', 'disbanded')]);
                if ($cep) {
                    $subject = "Nova filiação com processo na CEP - " . $record['name'];
                    $msg = Messages::affiliationCep($record, $cep);
                    Email::send('etica@novo.org.br', $record['name'], $subject, $msg);
                }
                $this->registerHistoric($affiliation, 3);
                $this->checkIndicationOfAffiliation($affiliation, 5);
            } else {
                $request = $this->em->getRepository(User::class)->findOneBy(['id' => $record['id']]);
                $request->setObservacao('aguardando aprovacao refiliacao');
                $this->em->getRepository(User::class)->save($request);
                $msg = Messages::affiliationAlert($request);
                Email::send('refiliacao@novo.org.br', 'Departamento de Filiação', 'Departamento de Filiação', 'Conferência de Indicação de Filiação', $msg);
            }
        }
        echo 'Rotina concluída!';
    }

    public function chargeExemption(Request $request, Response $response)
    {
        $this->validToken(15, $request);
        $records = $this->em->getRepository(Transaction::class)->generateAffiliationExemption();
        foreach ($records as $record) {
            $user = $this->em->getRepository(User::class)->findOneBy(['id' => $record['id']]);
            $dateGenerate = new \Datetime($record['data_gera_fatura']);
            $transactionData = $this->populateTransaction($user, '0.00', $dateGenerate, $dateGenerate, null, 0, 'exemption', '', 1, 0, 2, 'Filiação Isenta', '', '', '', 1, 1, '', '', $record['signature'], '');
            $this->em->getRepository(Transaction::class)->save($transactionData);
            /*-----Atualiza Pagamento RD-----*/
            $this->validLastTransaction($user, $transactionData, 6);
            /*-----Assinatura-----*/
            $nextCharge = $this->nextCharge($record);
            $subscription = $this->em->getRepository(PersonSignature::class)->findOneBy(['id' => $record['signature']]);
            $subscription->setDataGeraFatura(new \DateTime($nextCharge));
            $this->em->getRepository(PersonSignature::class)->save($subscription);
        }
        echo 'Rotina concluída!';
    }

    public function verifyValueWithoutFeesTransactions(Request $request, Response $response) // TO DO CONFERE VALOR SEM TAXAS
    {
        $this->validToken(16, $request);
        $transactions = $this->em->getRepository(Transaction::class)->monthlyPaymentsIugu();
        if ($transactions) {
            $group = array();
            foreach ($transactions as $transaction) {
                $group[$transaction["tb_diretorio_id"]][] = $transaction;
            }
            foreach ($group as $key => $value2) {
                $key = $this->em->getRepository(IuguTokens::class)->findOneBy(['directory' => $key]);
                if ($key) {
                    $account = $key->getAccount();
                    $key = $key->getToken();
                }
                $i = 0;
                foreach ($value2 as $transaction) {
                    $i++;
                    $t = $this->em->getRepository(Transaction::class)->findOneBy(['id' => $transaction['id']]);
                    $details = IuguService::find($transaction['invoice_id'], $key);
                    $valueWithoutFees = 0;
                    if ($details['payment_method'] == 'iugu_credit_card') {
                        if ($t->getFormaPagamento() != 1) $t->setFormaPagamento(1);
                        $t->setDataLiberacao(new \Datetime($details['financial_return_date']));
                        $taxes_paid = (floor($t->getValor() * 2.59) / 100) + 0.30;
                    } elseif ($details['payment_method'] == 'iugu_pix') {
                        if ($t->getFormaPagamento() != 11) $t->setFormaPagamento(11);
                        $t->setDataLiberacao(null);
                        $taxes_paid = round($t->getValor() * 0.99) / 100;
                    } elseif ($details['payment_method'] == 'iugu_bank_slip') {
                        if ($t->getFormaPagamento() != 2) $t->setFormaPagamento(2);
                        $t->setDataLiberacao(null);
                        $taxes_paid = 1.91;
                    }
                    $valueWithoutFees = $t->getValor() - $taxes_paid;
                    $t->setValorSemTaxas($valueWithoutFees);
                    $this->em->getRepository(Transaction::class)->save($t);
                    if ($i == 90) {
                        sleep(70);
                        $i = 0;
                    }
                }
            }
        }
        echo 'Rotina concluída!';
        //$this->verifyValuesDirectoriesRede();
    }

    public function verifyValuesDirectoriesRede()
    {
        $records = $this->em->getRepository(Transaction::class)->getTransactionsDirectories();
        foreach ($records as $record) {
            $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['id' => $record['id']]);
            $dateDeposit = $this->getWorkingDay($transaction->getDataPago()->format('Y-m-d'));
            $value = $transaction->getValue() - ($transaction->getValue() * (2.72 / 100));
            $transaction->setDataDeposito($dateDeposit)->setValorSemTaxas($value);
            $this->em->getRepository(Transaction::class)->save($transaction);
        }
        echo 'Rotina concluída!';
    }

    public function IuguRescue(Request $request, Response $response)
    {
        $this->validToken(17, $request);
        /** Recolhe transações não carimbadas do mês */
        $transactions = $this->em->getRepository(Transaction::class)->iuguCardDailyWithdraw();
        $diasemana = intVal(date('w', strtotime(date('Y-m-d'))));
        if ($diasemana === 3) {
            $transactionsBillet = $this->em->getRepository(Transaction::class)->iuguBilletWeeklyWithdraw();
            $transactions = array_merge($transactions, $transactionsBillet);
        }
        if ($transactions) {
            $group = array();
            foreach ($transactions as $transaction) {
                $group[$transaction["tb_diretorio_id"]][] = $transaction;
            }
            foreach ($group as $key => $value2) {
                /** Identifica Diretório */
                $directory = $this->em->getRepository(Directory::class)->find($key);
                $key = $this->em->getRepository(IuguTokens::class)->findOneBy(['directory' => $key]);
                if ($key) {
                    $account = $key->getAccount();
                    $key = $key->getToken();
                }
                //$copyTo = 'deliane@novo.org.br';
                /** consulta valor disponível */
                $queryAccount = IuguService::queryAccount($key, $account);
                $availableOnAccount = str_replace('R$ ', '', $queryAccount['balance_available_for_withdraw']);
                $availableOnAccount = str_replace('.', '', $availableOnAccount);
                $availableOnAccount = str_replace(',', '.', $availableOnAccount);
                $availableOnAccount = number_format((int)$availableOnAccount, 2, '.', '');
                /** Soma o total a ser resgatado */
                $amount = number_format(array_sum(array_column($value2, 'valor_s_taxas')), 2, '.', '');
                /** Verifica se o valor a ser sacado é menos do que o disponível */
                if ($amount <= $availableOnAccount) {
                    /** Solicita o resgate */
                    $rescue = IuguService::rescue($amount, $key, $account);
                    /** Recolhe informações do saque */
                    $rescueInformations = IuguService::withdraw_conciliations($rescue['id'], $key);
                    $paying_at = new \Datetime($rescueInformations['paying_at']);
                    /** Valida resgate */
                    if ($rescue['id']) {
                        /** Carimba transações e a data de pagamento */
                        foreach ($value2 as $t) {
                            $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['id' => $t['id']]);
                            $transaction->setRescue($rescue['id'])->setDataDeposito($paying_at);
                            $this->em->getRepository(Transaction::class)->save($transaction);
                        }
                        /** Envia e-mail de sucesso */
                        $subject = "Solicitação Resgate IUGU com sucesso - {$directory->getName()}";
                        $msg = Messages::rescue($rescue);
                        Email::send('renan@novo.org.br', " ", $subject, $msg, null, null, null, null);
                    } else {
                        /** Envia e-mail de erro no saque */
                        //array_push($copyTo, ['ti@novo.org.br']);
                        $subject = "Erro: Solicitação Resgate IUGU - {$directory->getName()}";
                        $msg = Messages::rescueError($availableOnAccount);
                        Email::send('renan@novo.org.br', " ", $subject, $msg, null, null, null, null);
                    }
                } else {
                    /** Envia e-mail de erro no sque por valor excedente */
                    //$copyTo = 'ti@novo.org.br';
                    $subject = "Erro: Solicitação Resgate IUGU - {$directory->getName()}";
                    $msg = Messages::rescueErrorQueryAccount($queryAccount, $availableOnAccount);
                    Email::send('renan@novo.org.br', " ", $subject, $msg, null, null, null, null);
                }
            }
        }
        /** Finaliza rotina */
        echo 'Rotina concluída!';
    }

    public function contributionIncrease(Request $request, Response $response)
    {
        $this->validToken(19, $request);
        /** Pega quem vai vencer a contribuição em 10 dias e nunca recebeu o comunicado de aumento de contribuição */
        $records = $this->em->getRepository(PersonSignature::class)->contributionIncrease();
        /** Percorre registros */
        foreach ($records as $record) {
            $linkEncode = "https://novo.org.br/atualiza-contribuicao/?email=" . base64_encode($record['email']);
            /** Registra Fase 1 */
            $this->setHistoricalIncrease((int)$record['personId'], '1');
        }
        echo 'Rotina concluída!';
    }

    public function refreshCard(Request $request, Response $response)
    {
        $this->validToken(20, $request);
        $records = $this->em->getRepository(PersonSignature::class)->getRefreshCard();
        foreach ($records as $record) {
            $user = $this->em->getRepository(User::class)->findOneBy(['id' => $record['id']]);
            $user->setObservacao('AtualizaçãoIugu');
            $this->em->getRepository(User::class)->save($user);
        }
    }

    public function reimbursementIUGU(Request $request, Response $response)
    {
        $transactions = $this->em->getRepository(Transaction::class)->findBy(['obs' => 'cancelar']);
        foreach ($transactions as $t) {
            $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['id' => $t->getId()]);
            if ($transaction->getGatewayPagamento() == 1) {
                IuguService::reimbursement($t->getInvoiceId());
            } else {
                Maxipago::chargeback($transaction);
            }
            $transaction->setObs('cancelado via api')->setStatus('canceled')->setDataCancelamento(new \Datetime());
            $this->em->getRepository(Transaction::class)->save($transaction);
        }
        echo 'Rotina Concluída!';
    }

    protected function getIdTagBotconversa(string $tag)
    {
        $idTag = match ($tag) {
            'Desfiliado' => 3847106,
            'Filiado' => 3847017,
            'Cadastrado' => 3849421,
            'Adimplente' => 3847185,
            'Inadimplente' => 3847187,
            'Isento' => 3871192,
            'Jornada 2024 Completo' => 3847193,
            'Jornada 2024 Incompleto' => 3847235,
            'Masculino' => 3849149,
            'Feminino' => 3849151,
            'Não quero informar' => 3849152,
            'Inscrito Jornada Completo' => 3853355,
            'Inscrito Jornada Incompleto' => 3853357,
            default => null
        };
        return $idTag;
    }

    protected function getIdCustomFieldBotconversa(string $customField, string $value)
    {
        $idCustomField = match ($customField) {
            'Genero' => [1193744, $value],
            default => null
        };
        return $idCustomField;
    }

    public function botconversa(Request $request, Response $response)
    {
        $records = $this->em->getRepository(User::class)->getRecordsBotConversa(26);
        foreach ($records as $record) {
            $userBotconversaArray = BotConversa::getRegisterID($record['phone']);
            if (!$userBotconversaArray) $userBotconversaArray = BotConversa::registerSubscriber($record);
            $customFields = ['Genero'];
            foreach ($customFields as $customField) {
                $customField = $this->getIdCustomFieldBotconversa($customField, $record['gender']);
                BotConversa::setCustomField($userBotconversaArray['id'], $customField);
            }
        }
        echo 'Rotina Concluída!';
    }

    public function sendWhatsZap(Request $request, Response $response)
    {
        $today = date('H');
        if ($today < "09" || $today > "18") die();
        //$this->validToken(21, $request);
        $records = $this->em->getRepository(Transaction::class)->sendZap();
        foreach ($records as $record) {
            $user = $this->em->getRepository(User::class)->find($record['tb_pessoa_id']);
            $url = $record['url'] ?: 'https://espaco-novo.novo.org.br/api/financeiro/gera-boleto/?transaction=' . base64_encode($record['id']);
            match ($record['days']) {
                -1 => WhatsService::billetGenerated($user->getFirstName(), $record['phone'], $url),
                0 => WhatsService::billetValidityToday($user->getFirstName(), $record['phone'], $url)
            };
            /*-----Atualiza transacao-----*/
            $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['id' => $record['id']]);
            $transaction->setUltimaAtualizacao(new \Datetime());
            $this->em->getRepository(Transaction::class)->save($transaction);
        }
        echo 'Rotina concluída!';
    }

    public function chargeAuth()
    {
        $lastAuth = $this->em->getRepository(AuthCharge::class)->findOneBy([], ['id' => 'desc']);
        $users = $this->em->getRepository(User::class)->getByMinId($lastAuth->getLastId());
        $lastId = $lastAuth->getLastId();
        foreach ($users as $user) {
            try {
                Auth::charge($user->getName() ?? '', $user->getEmail() ?? '', $user->getPassword() ?? '');
            } catch (\Exception $exception) {

            }
            $lastId = $user->getId();
        }
        $auth = new AuthCharge();
        $auth->setLastId($lastId);
        $this->em->persist($auth);
        $this->em->flush();
        echo 'Rotina concluída!';
    }

    public function chargeInstallmentsPlans(Request $request, Response $response)
    {
        try {
            $this->validToken(22, $request);
            $date = new \Datetime();
            $installments = $this->em->getRepository(PersonSignatureInstallments::class)->charge();
            foreach ($installments as $i) {
                $user = $this->em->getRepository(User::class)->findOneBy(['id' => $i['user']]);
                $paymentPlan = $this->em->getRepository(Plan::class)->find($i['paymentPlan']);
                $subscription = $this->em->getRepository(PersonSignature::class)
                    ->findOneBy(['tbPessoaId' => $user->getId(), 'tbDiretorioOrigem' => 1, 'origin' => 3]);
                $address = $this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()], ['id' => 'DESC']);
                if($address) $validadeCep = Validator::validateCep($this->iuguAddress($address));
                if($validadeCep == false) $address = $this->iuguAddressDonation();
                $keyIUGU = $this->getIuguTokenDirectory(1);
                $dueDate = new \Datetime(date('Y-m-d', strtotime($i['dueDate'])));

                if($i['paymentMethod'] == 2) {

                    $charge = IuguService::slip($user->getEmail(), $user->getCpf(), $user->getName(), $address, $i['value'], 
                        'Parcela plano de contribuição', 5, $keyIUGU['key']);
                    $transaction = $this->populateTransaction($user, $i['value'], $date, $dueDate, null, 2, 'pending', '', 
                        $i['plan'], 1, 8, '', '', $charge['id'], '', 1, 1, $charge['bank_slip']['bank_slip_pdf_url'], 
                        $charge['bank_slip']['digitable_line'], $i['signature']);

                } else if($i['paymentMethod'] == 1) {
                    $personIugu = $this->getCustomerIdIUGU($user);
                    $charge = IuguService::charge($personIugu->getCustomerId(), $i['cardToken'], $user->getEmail(), $user->getCpf(), 
                        $user->getName(), $address, $i['value'], 'Parcela plano de contribuição', 1, $keyIUGU['key']);
                    $status = 'paid';
                    if($charge['status'] != 'captured') {
                        $status = 'pending';
                        $charge['invoice_id'] = "";
                    }   
                    $transaction = $this->populateTransaction($user, $i['value'], $date, $date, $date, 1, $status, $personIugu->getCustomerId(),
                        $i['plan'], 1, 8, '', $i['cardToken'], $charge['invoice_id'], '', 1, 1, '', '', $i['signature']);

                }
                $this->em->getRepository(Transaction::class)->save($transaction);

                $hp = [];
                $hp['email'] = $user->getEmail();
                $hp['filiado'] = $user->getFiliadoString();
                $hp['status_de_filiado'] = $user->getFiliadoString();
                $hp['status_cadastro'] = $user->getStatusString();   
                $hp['plano_atual'] = $paymentPlan->getNameStr();
                $hp['plano_cancelado'] = 'Não';
                $hp['inadimplente'] = $charge['status'] == 'captured' ? 'Não' : 'Sim';
                //$hp['data_ultimo_pagamento'] = '';
                $hp['periodo_de_assinatura'] = $subscription->getFrequency();
                $hp['valor_total_do_plano'] = $i['value'];
                Hubspot::updateContact($user, $hp);

                $body = [];
                $body['email_address'] = $user->getEmail();
                $body['merge_fields']['FILIADO'] = $user->getFiliadoString();
                $body['merge_fields']['MMERGE63'] = $user->getFiliadoString();
                $body['merge_fields']['MMERGE58'] = $user->getStatusString();
                $body['merge_fields']['PLANO'] = $paymentPlan->getNameStr();
                $body['merge_fields']['PLANOCANCE'] = 'Não';
                $body['merge_fields']['INADIMPLEN'] = $charge['status'] == 'captured' ? 'Não' : 'Sim';
                //$body['merge_fields']['DATAULTIMOPAGAMENTO'] = '';
                $body['merge_fields']['PERIODO'] = $subscription->getFrequency();;
                $body['merge_fields']['VALORPLANO'] = Utils::formatMoney($i['value']);

                /*-----Atualiza a próxima cobrança-----*/
                $dueDate = new \Datetime(date('Y-m-d', strtotime($i['dueDate'])));
                $dueDate->add(new \DateInterval("P{$i['plan']}M"));

                if($i['id']) {
                    $installment = $this->em->getRepository(PersonSignatureInstallments::class)->find($i['id']);
                    $installment->setDueDate($dueDate);
                    $this->em->getRepository(PersonSignatureInstallments::class)->save($installment);
                } else {
                    $subscription = $this->em->getRepository(PersonSignature::class)->find($i['signature']);
                    $subscription->setDataGeraFatura($dueDate);
                    $this->em->getRepository(PersonSignature::class)->save($subscription);    
                }

            }
            
            return $response->withJson([
                'status' => 'success', 
                'message' => 'Rotina concluída'
            ]);

        } catch (\Exception $e) {
            $this->logError('Erro ao processar cobranças:', $e);
            return $response->withJson([
                'status' => 'error', 
                'message' => 'Erro ao processar cobranças'
            ], 500);
        }
    }

    /**
     * Levanta agendamento para envio de mensagens
     */
    public function scheduleMessageCogmo(Request $request, Response $response): void
    {
        $this->validToken(23, $request);

        // Busca contatos com limite
        $contacts = $this->em->getRepository(Transaction::class)->groupMessageCogmo();

        // Busca números ativos
        $cogmoPhoneNumbers = $this->em->getRepository(CogmoPhones::class)->findBy(['status' => 'active']);
        if (empty($cogmoPhoneNumbers)) throw new \Exception("Nenhum número ativo disponível");

        // Calcula intervalos
        $startHour = 8;
        $endHour = 18;

        foreach ($contacts as $index => $contact) {
            $user = $this->em->getRepository(User::class)->find($contact['tb_pessoa_id']);

            // Busca histórico de envios para o contato
            $previousMessage = $this->em->getRepository(CogmoSchedule::class)
                ->findOneBy(
                    ['contactId' => $user->getId()],
                    ['createdAt' => 'DESC']
                );

            // Define o número do telefone para o contato
            if ($previousMessage && $previousMessage->getPhoneNumber()) {
                // Usa o mesmo número anterior se existir e ainda estiver ativo
                $phoneNumber = array_filter($cogmoPhoneNumbers, function($phone) use ($previousMessage) {
                    return $phone->getId() === $previousMessage->getPhoneNumber()->getId() 
                        && $phone->getStatus() === 'active';
                });
                
                // Se o número anterior não estiver mais ativo, escolhe um novo
                if (empty($phoneNumber)) {
                    $phoneNumber = $cogmoPhoneNumbers[$user->getId() % count($cogmoPhoneNumbers)];
                } else {
                    $phoneNumber = reset($phoneNumber); // Pega o primeiro item do array filtrado
                }
            } else {
                // Se não houver mensagem anterior, distribui baseado no ID do contato
                $phoneNumber = $cogmoPhoneNumbers[$user->getId() % count($cogmoPhoneNumbers)];
            }

            $this->scheduleGroup(
                [$contact],
                $phoneNumber,
                $startHour,
                $endHour,
                $index,
                count($contacts)
            );
        }

        echo "Rotina concluída!";
    }

    /**
     * Agenda horários para um grupo de contatos
     */
    private function scheduleGroup(array $contact, CogmoPhones $phoneNumber, int $startHour, int $endHour, 
        int $index, int $totalContacts): void
    {
        // Define a mensagem
        if($contact[0]['forma_pagamento'] == 2) {
            $message = $contact[0]['days'] <= 0 
                ? 'Cobrar Filiado a respeito de boleto vencido'
                : 'Lembrar filiado do boleto próximo ao vencimento';
        } else {
            $message = 'Cobrar Filiado a respeito de cartão com erro no processamento, sugerir trocar o cartão neste link: https://novo.org.br/faturas/' . base64_encode($contact[0]['transaction_id']);
        }

        // Calcula o período total em minutos
        $totalMinutes = ($endHour - $startHour) * 60;
            
        // Calcula o intervalo fixo baseado no total de contatos
        $fixedInterval = $totalMinutes / ($totalContacts + 1);
            
        // Calcula o horário base para este contato
        $baseTime = strtotime(date('Y-m-d') . " {$startHour}:00:00");
        $contactTime = $baseTime + ($index * $fixedInterval * 60);
            
        // Adiciona uma variação aleatória pequena (±10% do intervalo)
        $variation = rand(-($fixedInterval * 0.1 * 60), ($fixedInterval * 0.1 * 60));
        $sendTime = date('Y-m-d H:i:s', $contactTime + $variation);

        // Atualiza o registro com o horário agendado
        $cogmoSchedule = new CogmoSchedule();
        $cogmoSchedule->setContact($this->em->getRepository(User::class)->find($contact[0]['tb_pessoa_id']))
            ->setContactNumber($contact[0]['phone'])
            ->setTransaction($this->em->getRepository(Transaction::class)->find($contact[0]['id']))
            ->setPhoneNumber($phoneNumber)
            ->setMessage($message)
            ->setScheduledTime(new \DateTime($sendTime))
            ->setStatus('scheduled')
            ->setCreatedAt(new \DateTime());
        $this->em->getRepository(CogmoSchedule::class)->save($cogmoSchedule);
    }

    /**
     * Envia mensagens para os contatos agendados
     */
    public function sendMessageCogmo(Request $request, Response $response) 
    {
        // Busca agendamento para o horário atual
        $schedules = $this->em->getRepository(CogmoSchedule::class)->getPendingSchedule();
        if (!$schedules) return "Nenhum agendamento encontrado";
        foreach ($schedules as $schedule) {
            $schedule = $this->em->getRepository(CogmoSchedule::class)->findOneBy(['id' => $schedule['id']]);
            $user = $this->em->getRepository(User::class)->find($schedule->getContactId()->getId());

            $totalDebit = $this->em->getRepository(Transaction::class)->getTotalDebit($user->getId())['total'];
            $totalDebit = Utils::formatMoney($totalDebit);

            // Processa envio
            $result = CogmoBot::index($user, $schedule, $totalDebit);
            $status = $result['error'] ? 'error' : 'sent';

            // Atualiza status
            $schedule->setStatus($status);
            $this->em->getRepository(CogmoSchedule::class)->save($schedule);
        }
        echo "Rotina concluída!";
    }
}