<?php

namespace App\Controllers;
use App\Helpers\Messages;
use App\Helpers\Utils;
use App\Helpers\Validator;
use App\Exceptions\FoundException;
use App\Models\Entities\Address;
use App\Models\Entities\AffiliationTmp;
use App\Models\Entities\City;
use App\Models\Entities\CivilState;
use App\Models\Entities\Country;
use App\Models\Entities\CpfTmp;
use App\Models\Entities\Directory;
use App\Models\Entities\DirectoryGatway;
use App\Models\Entities\Gender;
use App\Models\Entities\PersonCreditCard;
use App\Models\Entities\PersonIugu;
use App\Models\Entities\PersonRede;
use App\Models\Entities\Phone;
use App\Models\Entities\Schooling;
use App\Models\Entities\State;
use App\Models\Entities\Transaction;
use App\Models\Entities\User;
use App\Models\Entities\CommunicationConsent;
use App\Models\Entities\IndicationOfAffiliation;
use App\Models\Entities\Logs;
use App\Services\Email;
use App\Services\IuguService;
use App\Services\Mailchimp;
use App\Services\MaxiPago;
use App\Services\Hubspot;
use App\Services\GoogleService;
use App\Services\Auth;
use App\Services\Payload;
use App\Services\Pix;
use Mpdf\QrCode\QrCode;
use Mpdf\QrCode\Output;
use phpDocumentor\Reflection\DocBlock\Tags\Throws;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class AffiliationController extends Controller
{

    public function index(Request $request, Response $response)
    {
        return $this->renderer->render($response, 'public/affiliation/index.phtml');
    }

    public function payment(Request $request, Response $response)
    {
        $cpf = $request->getAttribute('route')->getArgument('cpf');
        return $this->renderer->render($response, 'public/affiliation/payment.phtml', ['cpf' => $cpf]);
    }

    public function validateCpf(Request $request, Response $response)
    {
        try {
            $cpf = $request->getAttribute('route')->getArgument('cpf');
            Validator::validateCPF($cpf);
            $user = $this->em->getRepository(User::class)->findOneBy(['cpf' => Utils::formatCpf($cpf)]);
            if ($user) {
                throw new FoundException('CPF já cadastrado');
            }
            $cpfTmp = new CpfTmp();
            $cpfTmp->setCpf(Utils::formatCpf($cpf));
            $cpfTmp = $this->em->getRepository(CpfTmp::class)->save($cpfTmp);
            return $response->withJson([
                'status' => 'ok',
                'cpfTmp' => $cpfTmp->getId(),
            ], 200)
                ->withHeader('Content-type', 'application/json');
        } catch (FoundException $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(302);
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function beginFiliation(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            $fields = [
                'name' => 'Nome',
                'email' => 'E-mail',
                'cellPhone' => 'WhatsApp'
            ];
            Validator::requireValidator($fields, $data);
            GoogleService::recapchaNovo($data['g-recaptcha-response']);      
            /*-----Confere e salva pessoa-----*/
            $user = $this->em->getRepository(User::class)->findOneBy(['email' => $data['email']]);
            if($user) {
                if($user->isAffiliated()) throw new FoundException('Você já está filiado ao NOVO,<br> acesse seu Espaço NOVO para mais detalhes.');
                if(in_array($user->getFiliado(), [1, 2, 3, 4, 14])) throw new FoundException("Você já possui um pedido de filiação em aberto,<br> acesse seu Espaço NOVO para mais detalhes.");
                if(in_array($user->getFiliado(), [6, 11, 10, 13])) throw new FoundException("Não será possível prosseguir com o pedido de filiação,<br>
                    para mais detalhes entre em contato conosco pelo e-mail <a href='mailto:filiacao@novo.org.br'>filiacao@novo.org.br</a>.");
            } else {
                $user = new User();
                $user->setFiliado(0)
                    ->setName($data['name'])
                    ->setEmail($data['email'])
                    ->setTermoAceite0(1)
                    ->setTermoAceite1(1)
                    ->setTermoAceite2(1)
                    ->setTermoAceite3(1)
                    ->setDataCriacao(new \Datetime)
                    ->setEstadoCivil($this->em->getReference(CivilState::class, 6))
                    ->setGenero($this->em->getReference(Gender::class, 3))
                    ->setEscolaridade($this->em->getReference(Schooling::class, 7))
                    ->setOpcaoReligiosa(11)
                    ->setIp(Utils::getAcessData()['ip'])
                    ->setUltimaAtualizacao(new \Datetime);
            }
            $user->setStatus(1);
            $this->em->getRepository(User::class)->save($user);
            /*-----Registra Telefone-----*/
            $cellPhone = new Phone();
            $cellPhone->setPais('br')
                ->setDdi(55)
                ->setTelefone($data['cellPhone'])
                ->setTbTipoTelefoneId(3)
                ->setTbPessoaId($user->getId());
            $this->em->getRepository(Phone::class)->save($cellPhone);
            
            /*-----Hubspot/Mailchimp-----*/
            Hubspot::createContact($user, $data['cellPhone']);
            $this->mailchimpCreateUser($user, null, $data['cellPhone']);
            Hubspot::insertIntoTheFlow($user, 57589575);

            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => ''
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (FoundException $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(302);
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function firstRegister(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            $fields = [
                'name' => 'Nome',
                'email' => 'E-mail',
                'cellPhone' => 'WhatsApp',
                'cpf' => 'CPF',
                'rg' => 'RG',
                'birth' => 'Nascimento',
                'sex' => 'Sexo'
            ];
            Validator::requireValidator($fields, $data);

            /*-----Identifica e atualiza cadastro-----*/

            // Busca usuários
            $userByEmail = $this->em->getRepository(User::class)->findOneBy(['email' => $data['email']]);
            $userByCPF = $this->em->getRepository(User::class)->findOneBy(['cpf' => $data['cpf']]);

            // Verifica as possibilidades
            if ($userByEmail && $userByCPF) {
                // Ambos existem - devem ser o mesmo usuário
                if ($userByEmail->getId() !== $userByCPF->getId()) {
                    throw new FoundException('CPF já cadastrado para outro usuário!');
                }
                $user = $userByEmail;  // ou userByCPF, são o mesmo
            } elseif ($userByEmail) {
                // Só email existe
                $user = $userByEmail;
            } elseif ($userByCPF) {
                // Só CPF existe
                throw new FoundException('CPF já cadastrado!');
            } else {
                // Nenhum existe - usuário novo
                $user = null;
            }            
            
            $user->setStatus(2)
                 ->setCpf(Utils::formatCpf($data['cpf']))
                 ->setRg($data['rg'])
                 ->setDataNascimento(\DateTime::createFromFormat('d/m/Y', $data['birth']))
                 ->setGenero($this->em->getReference(Gender::class, $data['sex']));
            $this->em->getRepository(User::class)->save($user);
            
            /*-----Hubspot/Mailchimp-----*/
            $hp = [];
            $hp['email'] = $user->getEmail();
            $hp['filiado'] = $user->getFiliadoString();
            $hp['status_de_filiado'] = $user->getFiliadoString();
            $hp['status_cadastro'] = $user->getStatusString();
            $hp['cpf'] = Utils::onlyNumbers($user->getCpf());
            $hp['status_cadastro'] = 'Etapa 2 - CPF';
            Hubspot::updateContact($user, $hp);
            $body = [];
            $body['email_address'] = $user->getEmail();
            $body['merge_fields']['FILIADO'] = $user->getFiliadoString();
            $body['merge_fields']['MMERGE63'] = $user->getFiliadoString();
            $body['merge_fields']['MMERGE58'] = $user->getStatusString();
            $body['merge_fields']['CPF'] = $user->getCpf();
            $body['merge_fields']['ADDRESS'] = '';
            Mailchimp::updateContact($body);

            //Hubspot::insertIntoTheFlow($user, 57589575); // insere no fluxo de recuperação de carrinho.
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => ''
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (FoundException $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(302);
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function filication(Request $request, Response $response)
    {
        $cpf = $request->getAttribute('route')->getArgument('cpf');
        $cpfTmp = $this->em->getRepository(CpfTmp::class)->findOneBy(['cpf' => Utils::formatCpf($cpf)], ['id' => 'DESC'])->getId();
        $states = $this->em->getRepository(State::class)->findAll();
        $countries = $this->em->getRepository(Country::class)->findAll();
        return $this->renderer->render($response, 'public/affiliation/register-wizard.phtml',
            ['states' => $states, 'countries' => $countries, 'cpf' => $cpf, 'cpfTmp' => $cpfTmp]);
    }

    public function registerPassword(request $request, Response $response) 
    {
        try {
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            $date = new \Datetime();
            Validator::requireValidator(['email' => 'E-mail',
                                         'password' => 'Senha',
                                         'password2' => 'Confirmação de Senha'
                                        ], $data);
            $user = $this->em->getRepository(User::class)->findOneBy(['cpf' => Utils::formatCpf($data['cpf'] ?? '')]);
            if(!$user) throw new \Exception("Solicitação Inválida");      

            if($user->getEmail() != $data['email']) {
                $checkMail = $this->em->getRepository(User::class)->findOneBy(['email' => $data['email']]);
                if ($checkMail) throw new FoundException('Email já cadastrado!');
            }
            if($data['password'] != $data['password2']) throw new \Exception("As senhas devem ser iguais!");
            $user->setPassword(md5($data['password']))
                 ->setFiliado(0)
                 ->setEmail($data['email']);
            $msg = 'Cadastro realizado com sucesso, você será redirecionado em alguns segundos...';
            if($user->getExemption()) {
                $this->checkIndicationOfAffiliation($user, 4);
                match ($user->getFiliado()) {
                    9 => $user->setDataSolicitacaoReFiliacao($date)->setFiliado(14),
                    default => $user->setDataSolicitacaoFiliacao($date)->setFiliado(2),
                };
                $this->em->getRepository(User::class)->save($user); // atualiza cadastro
                $array = ['plan' => 1, 'value' => '0.00', 'paymentMethod' => 0, 'directory' => 1];
                $subscription = $this->createSubscription($user, $array, 0); // cria ou edita assinatura como isenta
                $transaction = $this->populateTransaction($user, '0.00', $date, $date, null, 0, 'exemption', '', 1, 0, 2, 'Filiação Isenta', '', '', '', 1, 1, '', '', $subscription->getId(), '');
                $this->em->getRepository(Transaction::class)->save($transaction); // cria transação isenta
                $msg = "Falta pouco para você se juntar ao NOVO!, sua solicitação será analisada em até 5 dias úteis. 
                        Enviaremos um email quando você estiver efetivamente filiado ao NOVO.
                        Você pode acompanhar o processo pelo Espaço NOVO!";
            } 
            $this->em->getRepository(User::class)->save($user);
            
            /*-----Hubspot/Mailchimp-----*/
            $this->updateStatusRegister($user);
            
            $this->em->commit();
            return $response->withJson([
                'exemption' => intVal($user->getExemption()),
                'status' => 'ok',
                'message' => $msg,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage()
            ])->withStatus(500);
        }
    }

    public function register(request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            $data['addressOutside'] ??= 0;

            $user = $this->em->getRepository(User::class)->findOneBy(['cpf' => Utils::formatCpf($data['cpf'] ?? '')]);
            if ($user && in_array($user->getFiliado(), [2,14,7,8])) 
                throw new FoundException('Você já está filiado ao NOVO ou possui algum pedido de filiação em aberto,<br> 
                    acesse seu Espaço NOVO para mais detalhes!');

            if($user && $user->getSuspensao() && $user->getObservacao() == "Filiação suspensa por 8 anos.") throw new FoundException("Não será possível prosseguir com o pedido de filiação, 
                entre em contato conosco pelo e-mail <a href='mailto:etica@novo.org.br'>etica@novo.org.br</a>.");
    
            $msg = '';
            if (!$user) {
                $checkMail = $this->em->getRepository(User::class)->findOneBy(['email' => $data['email']]);
                if ($checkMail) throw new FoundException('Email já cadastrado!');
                $msg = 'Cadastro realizado com sucesso';
            }

            $this->validaRegisterData($data, $user);
            if(!$data['password']) throw new \Exception("Solicitação Inválida");
            
            $user = $this->populateUser($data, $user);
            Auth::charge($user->getName(), $user->getEmail(), $user->getPassword());
            $this->populateAddress($data, $user);
            $this->populatePhone($data, $user);
            
            /*-----Hubspot/Mailchimp-----*/
            Hubspot::createContact($user);
            $this->mailchimpCreateUser($user, null, $data['cellPhone']);

            if($data['exemption']) $msg = $this->insertExemptionAffliation($user, $data);
            if(isset($data['campaign'])) $this->setCampaign($user, $data['campaign']);

            $this->em->commit();
            return $response->withJson([
                'exemption' => $user->getExemption(),
                'status' => 'ok',
                'message' => $msg,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    private function insertExemptionAffliation(User $user, array $data): string
    {
        $date = new \DateTime();
        $user->setPaymentPlan(0)
            ->setDatePlan($date)
            ->setExemption(1);

        match ($user->getFiliado()) {
            9 => $user->setDataSolicitacaoReFiliacao($date)->setFiliado(14),
            default => $user->setDataSolicitacaoFiliacao($date)->setFiliado(2),
        };
        $this->em->getRepository(User::class)->save($user); // atualiza cadastro
        
        /*-----Hubspot/Mailchimp-----*/
        $this->updateStatusRegister($user);
        
        $this->checkIndicationOfAffiliation($user, 4);

        $array = ['plan' => 1, 'value' => '0.00', 'paymentMethod' => 0, 'directory' => 1];
        $subscription = $this->createSubscription($user, $array, 0); // cria ou edita assinatura como isenta

        $transaction = $this->populateTransaction($user, '0.00', $date, $date, null, 0, 'exemption', '', 1, 0, 2, 'Filiação Isenta', '', '', '', 1, 1, '', '', $subscription->getId(), '');
        $this->em->getRepository(Transaction::class)->save($transaction); // cria transação isenta

        if($data['confirmDonation']) {
            if($data['confirmDonation'] == 1) {
                $data['value'] = str_replace('.', '', $data['donation']);
                $data['value'] = str_replace(',', '.', $data['value']);
                $data['value'] = $data['value'];
                $array = ['plan' => 1, 'value' => $data['value'], 'paymentMethod' => 2, 'directory' => 1];
                $this->createSubscription($user, $array, 1, '', '', 1, 1);
            }
        }

        $msg = Messages::affiliationPaymentConfirmed($user);
        Email::send($user->getEmail(), $user->getName(), 'Solicitação de filiação confirmada', $msg, 'refiliacao@novo.org.br');
        
        $msg = "Recebemos seu pedido de filiação!<br>
                Agora, escolha sua forma de contribuição (você será redirecionado automaticamente).";
        return $msg;
    }

    private function validaRegisterData(array $data, $user = false)
    {
        $fields = [
            'name' => 'Nome Completo',
            'birth' => 'Data de Nascimento',
            'rg' => 'RG',
            'cellPhone' => 'Telefone Celular',
            'zipCode' => 'CEP',
            'street' => 'Logradouro',
            'number' => 'Número',
            'district' => 'Bairro',
            'originVoterTitle' => 'Origem título de eleitor',
            'mother' => 'Nome da Mãe',
            'voterTitle' => 'Título de eleitor',
            'voterTitleZone' => 'Zona',
            'voterTitleSection' => 'Seção',
            'term1' => 'Termo 1',
            'term2' => 'Termo 2',
            'term3' => 'Termo 3',
            'term4' => 'Termo 4',
        ];
        if (!($data['addressOutside'])) {
            $fields['ufId'] = 'Estado';
            $fields['cityId'] = 'Cidade';
        } else {
            $fields['addressCountry'] = 'País';
            $fields['city'] = 'Cidade';
        }
        if ($data['originVoterTitle'] == 2) { //brasil
            $fields['voterTitleUF'] = 'UF eleitoral';
            $fields['voterTitleCity'] = 'Município Eleitoral';
        } else {
            $fields['voterTitleCountry'] = 'País eleitoral';
            $fields['voterTitleCityString'] = 'Município Eleitoral';
        }
        Validator::requireValidator($fields, $data);
        if (!$user) Validator::validatePassword($data);
        if (!$user) Validator::validateEmail($data);
        Validator::validDate($data['birth']);
        Validator::validateCPF($data['cpf'] ?? '');
    }

    private function populateUser(array $data, $user): User
    {
        $new = false;
        if (!$user) {
            $new = true;
            $user = new User();
        }
        if ($new) {
            $user->setCpf(Utils::formatCpf($data['cpf']))
                    ->setDataCriacao(new \DateTime())
                    ->setDataSolicitacaoFiliacao(new \DateTime())
                    ->setFiliado(0)
                    ->setUltimaAtualizacao(new \Datetime());
        }
        $user->setName($data['name'])
                ->setPassword(md5($data['password']))
                ->setEmail($data['email3'])
                ->setDataNascimento(\DateTime::createFromFormat('d/m/Y', $data['birth']))
                ->setRg($data['rg'])
                ->setNomeMae($data['mother'])
                ->setTituloEleitoral($data['voterTitle'])
                ->setTituloEleitoralZona($data['voterTitleZone'])
                ->setTituloEleitoralSecao($data['voterTitleSection'])
                ->setTituloEleitoralMunicipio($data['voterTitleCityString'])
                ->setTituloEleitoralPaisId($this->em->getReference(Country::class, $data['voterTitleCountry']))
                ->setTermoAceite0($data['term1'])
                ->setTermoAceite1(1)
                ->setTermoAceite2(1)
                ->setTermoAceite3(1)
                ->setEstadoCivil($this->em->getReference(CivilState::class, 6))
                ->setGenero($this->em->getReference(Gender::class, $data['sex']))
                ->setEscolaridade($this->em->getReference(Schooling::class, 7))
                ->setOpcaoReligiosa(11)
                ->setIp(Utils::getAcessData()['ip'])
                ->setIrpf(0)
                ->setExemption(0)
                ->setStatus(11);
        if ($data['originVoterTitle'] != 1) {
            $user->setTituloEleitoralPaisId($this->em->getReference(Country::class, 33)) //Brasil
                    ->setTituloEleitoralMunicipioId($this->em->getReference(City::class, $data['voterTitleCity']))
                    ->setTituloEleitoralUfId($this->em->getReference(State::class, $data['voterTitleUF']));
        }
        $this->em->getRepository(User::class)->save($user);
        if($data['codigo']) $this->setWhoIndicate($user, $data['codigo'], 2);
        if($new) {
            $this->confirmEmail($user, 'Espaço NOVO - Ativação da Conta');
            $this->registerHistoric($user, 1);
        }

        /*-----Hubspot/Mailchimp-----*/
        $this->updateStatusRegister($user);
        
        return $user;
    }

    private function populateAddress(array $data, User $user): Address
    {
        $data['addressOutside'] ??= 0;
        $address = new Address();
        $uf = $this->em->getRepository(State::class)->findOneBy(['sigla' => $data['ufId']]); // vem como string
        $uf = $uf ? $uf->getId() : null; // para funcionar o select integrado com o cep
        $city = $this->em->getRepository(City::class)->findOneBy(['cidade' => $data['cityId'], 'state' => $uf]);
        $city = $city ? $city->getId() : null;

        $address->setTbPessoaId($user->getId())
            ->setCountryId($data['addressCountry']) //Brasil
            ->setResideExterior($data['addressOutside'])
            ->setStateId($uf)
            ->setEstado('')
            ->setCityId($city)
            ->setCidade('')
            ->setCep($data['zipCode'])
            ->setNumero($data['number'])
            ->setEndereco($data['street'])
            ->setComplemento($data['complement'])
            ->setBairro($data['district']);
            
        if ($data['addressOutside']) {
            $address->setCityId(0)
                ->setCountryId($data['addressCountry'])
                ->setEstado($data['uf'])
                ->setCidade($data['city']);
        }
        return $this->em->getRepository(Address::class)->save($address);
    }

    private function populatePhone(array $data, User $user): Phone
    {
        $cellPhone = new Phone();
        $cellPhone->setPais($data['cellPhoneIso2'])
            ->setDdi($data['cellPhoneDialCode'])
            ->setTelefone($data['cellPhone'])
            ->setTbTipoTelefoneId(3)
            ->setTbPessoaId($user->getId());
        return $this->em->getRepository(Phone::class)->save($cellPhone);
    }

    public function charge(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $fields = [
                'paymentMethod' => 'Forma de Pagamento',
                'plan' => 'Plano de Pagamento',
                'value' => 'Valor',
                'cpf' => 'Cpf',
            ];
            if ($data['value'] == 0) {
                $data['value2'] = str_replace('.', '', $data['value2']);
                $data['value2'] = str_replace(',', '.', $data['value2']);
                $data['value'] = $data['value2'];
            }
            if ($data['paymentMethod'] == 1) {
                $fields['cardNumber'] = 'Número do cartão';
                $fields['cvv'] = 'Código de verificação';
                $fields['expiringDate'] = 'Validade do cartão';
                $fields['cardName'] = 'Nome como está no cartão';
                $fields['international'] = 'Cartão emitido no Brasil';
            }
            Validator::requireValidator($fields, $data);
            $data['flag'] ??= '';
            $data['site'] ??= 'novo';
            $maxValue = MAXVALUE;
            switch ($data['plan']) {
                case 1:
                    $minValue = MINMONTHLY;
                    break;
                case 6:
                    $minValue = MINSEMESTER;
                    break;
                case 12:
                    $minValue = MINYEARLY;
                    break;
                default:
                    $minValue = MINMONTHLY;
                    break;
            }
            Validator::validateRangeNumber($data['value'], 'Valor da Contribuição', $minValue, $maxValue);
            //match ($data['site']) {
            //    'espaconovo' => GoogleService::recapchaEspacoNovo($data['g-recaptcha-response']),
            //    default => GoogleService::recapchaNovo($data['g-recaptcha-response'])
            //};
            $user = $this->em->getRepository(User::class)->findOneBy(['cpf' => Utils::formatCpf($data['cpf'])]);
            if (!$user) throw new FoundException('CPF inválido');
            if (!in_array($user->getFiliado(), [0, 1, 3, 5, 9])) throw new FoundException('Acesse o espaço NOVO');
            $date = new \DateTime();
            if ($data['paymentMethod'] == 1) { // cartao de credito
                /** Save card IUGU */
                $card = $this->createCardApiIugu($user, $data['cardNumber'], $data['cardName'], $data['cvv'], $data['expiringDate'], $data['flag'], $data['international']);
                /** Save card IUGU */
                $subscription = $this->createSubscription($user, $data, 1, $card['customer'], $card['paymentToken']);
                /** Cobrança */
                $address = $this->iuguAddress($this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()]));
                $personIugu = $this->getCustomerIdIUGU($user);
                $charge = IuguService::charge($personIugu->getCustomerId(), $card['paymentToken'], $user->getEmail(), $user->getCpf(), $user->getName(), $address, $data['value'], 'Filiação', 0);
                if(!$charge['invoice_id']) {
                    $user->setStatus(8);
                    $this->em->getRepository(User::class)->save($user);
                    
                    /*-----Hubspot/Mailchimp-----*/
                    $this->updateStatusRegister($user);

                    throw new \Exception("Houve um problema para realizar o pagamento!");
                }
                $transaction = $this->populateTransaction($user, $data['value'], $date, $date, $date, 1, 'paid',
                    $personIugu->getCustomerId(), $data['plan'], 1, 2, '', $card['paymentToken'], $charge['invoice_id'], '', 1, 1, '', '', $subscription->getId());
                $this->em->getRepository(Transaction::class)->save($transaction);
                if ($user->getFiliado() == 9) {
                    $user->setFiliado(14)->setDataSolicitacaoRefiliacao(new \DateTime());
                    $type = 5;
                } else {
                    $user->setFiliado(2)->setDataSolicitacaoFiliacao(new \DateTime());
                    $type = 2;
                }
                $user->setStatus(9);
                $this->em->getRepository(User::class)->save($user);
                
                /*-----Hubspot/Mailchimp-----*/
                $this->updateStatusRegister($user);

                $this->registerHistoric($user, $type);
                $this->checkIndicationOfAffiliation($user, 4);
                $msg = Messages::affiliationPaymentConfirmed($user);
                Email::send($user->getEmail(), $user->getName(), 'Pagamento de filiação confirmado', $msg, 'refiliacao@novo.org.br');
                switch ($user->getTermoAceite0()) {
                    case 2:
                        $msg = Messages::notificationTerm0Filiation($user);
                        Email::send('refiliacao@novo.org.br', '', 'Aviso Termo 1 - Nova Filiação', $msg);
                        break;
                    case 3:
                        $msg = Messages::notificationTerm0Dam($user);
                        Email::send('dam@novo.org.br', '', 'Nova Filiação - Indicação Termo 1', $msg);
                        $msg = Messages::notificationTerm0Affiliation();
                        Email::send($user->getEmail(), $data['name'], 'Aviso Filiação - Partido NOVO', $msg);
                        break;               
                }
                $this->notificationTerm0($user);
                return $response->withJson([
                    'status' => 'ok',
                    'message' => 'Parabéns!<br>
                                  Você concluiu a primeira etapa para sua Filiação ao NOVO!<br>
                                  Enviamos para seu e-mail a confirmação e os detalhes mais importantes, para concluir o processo.<br>
                                  E agora, você já pode acessar seu ambiente exclusivo, no Espaço NOVO. Aperte no botão abaixo:',
                ], 201)
                    ->withHeader('Content-type', 'application/json');
            }
            // boleto
            $address = $this->iuguAddress($this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()], ['id' => 'DESC']));
            $slip = IuguService::slip($user->getEmail(), $user->getCpf(), $user->getName(), $address, $data['value'], 'Filiação');
            if(!$slip['id']) throw new \Exception("Houve um problema para gerar sua contribuição!");
            $subscription = $this->createSubscription($user, $data, 1);
            $dueDate = new \Datetime(date('Y-m-d', strtotime("+5 days",strtotime(date('Y-m-d')))));
            $transaction = $this->populateTransaction($user, $data['value'], $date, $dueDate, null, 2, 'pending',
                '', $data['plan'], 1, 2, '', '', $slip['id'], '',
                1, 1, $slip['bank_slip']['bank_slip_pdf_url'], $slip['bank_slip']['digitable_line'], $subscription->getId());
            $this->em->getRepository(Transaction::class)->save($transaction);
            if($user->getFiliado() == 9){
                $user->setFiliado(3)->setDataSolicitacaoRefiliacao(new \DateTime());
                $type = 5;
            } else {
                $user->setFiliado(1)->setDataSolicitacaoFiliacao(new \DateTime());
                $type = 2;
            }
            $user->setStatus(9);
            $this->em->getRepository(User::class)->save($user);
            
            /*-----Hubspot/Mailchimp-----*/
            $this->updateStatusRegister($user);

            $this->registerHistoric($user, $type);
            $msg = Messages::emailMembershipAwaitingPayment($user->getName(), $slip);
            Email::send($user->getEmail(), $user->getName(), 'Lembrete de pagamento - Contribuição do filiado', $msg, 'refiliacao@novo.org.br');
            if($user->getFiliado() == 3) {
                $page = 'https://novo.org.br/participe/pagamento-pendente-refiliacao/';
            } else {
                $page = 'https://novo.org.br/participe/pagamento-pendente/';
            }
            $this->notificationTerm0($user);
            $this->checkIndicationOfAffiliation($user, 6);
            return $response->withJson([
                'status' => 'ok',
                'page' => $page,
                'message' => "Boleto gerado com sucesso. <br> 
                                Código de Barras: {$slip['bank_slip']['digitable_line']} <br> 
                                <span class='btn btn-primary' data-clipboard-text='{$slip['bank_slip']['digitable_line']}'>Copiar Código de Barras <i class='fa fa-barcode'></i></span>
                                <a class='btn btn-success' href='{$slip['bank_slip']['bank_slip_pdf_url']}' target='_blank'>Baixar o boleto <i class='fa fa-file-pdf-o'></i></a><br>
                                Atenção, seu boleto pode demorar até 2 horas para ser registrado, se não conseguir realizar o pagamento aguarde alguns instantes ou tente fazer via PIX.",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            if($e->getCode() == 422){
                throw new FoundException('Atenção, o CEP inserido não é válido para emissão de boletos. Realize o pagamento por Cartão de Crédito!');
            } else {
                return $response->withJson([
                    'status' => 'error',
                    'message' => $e->getMessage(),
                ])->withStatus(500);
            }
        }
    }

    public function partial(request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $data['addressOutside'] ??= 0;
            $user = $this->em->getRepository(AffiliationTmp::class)->findOneBy(['cpfTmp' => $data['cpfTmp']]);
            if (!$user) $user = new AffiliationTmp();
            $user->setCpfTmp($this->em->getReference(CpfTmp::class, $data['cpfTmp']))
                ->setName($data['name'] ?? '')
                ->setCellPhone($data['cellPhone'] ?? '')
                ->setBirth(\DateTime::createFromFormat('d/m/Y', $data['birth']) ?? null)
                ->setZipCode($data['zipCode'] ?? '')
                ->setVoterTitle($data['voterTitle'] ?? '')
                ->setMother($data['mother'] ?? '')
                ->setVoterTitleZone($data['voterTitleZone'] ?? '')
                ->setVoterTitleSection($data['voterTitleSection'] ?? '')
                ->setVoterTitleCity($data['voterTitleCityString'] ?? '')
                ->setVoterTitleCountry($data['voterTitleCountry'] ?? 33)
                ->setAddressCountry($data['addressCountry'] ?? 0)
                ->setUf($data['ufId'] ?? '')
                ->setCity($data['cityId'] ?? '')
                ->setNumber($data['number'] ?? '')
                ->setDistrict($data['district'] ?? '')
                ->setStreet($data['street'] ?? '');
            if ($data['originVoterTitle'] != 1) {
                $user->setVoterTitleCountry(33) //Brasil
                ->setVoterTitleCity($data['voterTitleCity'] ?? '')
                    ->setVoterTitleUF($data['voterTitleUF'] ?? '');
            }
            if ($data['addressOutside']) {
                $user->setUf($data['uf'] ?? '')
                    ->setCity($data['city'] ?? '');
            }
            $this->em->getRepository(AffiliationTmp::class)->save($user);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Cadastro realizado com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function publicDonation(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $date = new \DateTime();
            $fields = [
                'plan' => 'Plano de Pagamento',
                'value' => 'Valor',
                'name' => 'Nome',
                'email' => 'E-mail',
                'directory' => 'Diretório',
                'cpf' => 'Cpf',
                'paymentMethod' => 'Forma de Pagamento',
                'g-recaptcha-response' => 'Recaptcha'
            ];
            if ($data['paymentMethod'] == 1) {
                $fields['cardNumber'] = 'Número do cartão';
                $fields['cvv'] = 'Código de verificação';
                $fields['expiringDate'] = 'Validade do cartão';
                $fields['cardName'] = 'Nome como está no cartão';
                $fields['international'] = 'Cartão emitido no Brasil';
            }
            if ($data['value'] == 0) {
                $data['value2'] = str_replace('.', '', $data['value2']);
                $data['value2'] = str_replace(',', '.', $data['value2']);
                $data['value'] = $data['value2'];
            }
            Validator::requireValidator($fields, $data);
            if ((int)$data['value'] <= 15) throw new \Exception("Valor minimo para doação é de R$15,00.");
            if(!$data['dev']) GoogleService::recapchaNovoDoacao($data['g-recaptcha-response']);    
            $directory = $this->em->getRepository(Directory::class)->findOneBy(['nome' => $data['directory']]);
            $data['directory'] = $directory->getId();
            $data['flag'] ??= '';
            if ($data['value'] > MAXVALUE) {
                $directoryAccount = $this->em->getRepository(Directory::class)->getAccount($directory);
                throw new \Exception("Valor máximo para a doação via sistema é R$ " . number_format(MAXVALUE, 2, ',', '.') . ".<br> 
                                        Para doar um valor superior faça uma transfêrencia bancaria:<br> 
                                        <b>Banco:</b> {$directoryAccount['bank']} <br>
                                        <b>Agência:</b> {$directoryAccount['agency']} <br>
                                        <b>Conta:</b> {$directoryAccount['account']} <br>
                                        <b>CNPJ:</b> {$directoryAccount['cnpj']} <br>");
            }
            $user = $this->em->getRepository(User::class)->findOneBy(['cpf' => Utils::formatCpf($data['cpf'])]);
            if (!$user) {
                $user = new User();
                $user->setFiliado(0)
                    ->setStatus(0)
                    ->setName($data['name'])
                    ->setCpf(Utils::formatCpf($data['cpf']))
                    ->setEmail($data['email'])
                    ->setTermoAceite0(1)
                    ->setTermoAceite1(1)
                    ->setTermoAceite2(1)
                    ->setTermoAceite3(1)
                    ->setDataCriacao($date)
                    ->setEstadoCivil($this->em->getReference(CivilState::class, 6))
                    ->setGenero($this->em->getReference(Gender::class, 3))
                    ->setEscolaridade($this->em->getReference(Schooling::class, 7))
                    ->setOpcaoReligiosa(11)
                    ->setIp(Utils::getAcessData()['ip'])
                    ->setUltimaAtualizacao($date);
                $user = $this->em->getRepository(User::class)->save($user);
                $this->registerHistoric($user, 1);
                $this->confirmEmail($user, 'Doação - Ativação da Conta');
                $valid = false;
                if(isset($data['term'])){
                    
                    /*-----Hubspot/Mailchimp-----*/
                    Hubspot::createContact($user);
                    $this->mailchimpCreateUser($user, null, null);

                    $valid = true;
                }
                $consent = new CommunicationConsent();
                $consent->setUser($user->getId())->setType(3)->setValid($valid)->setCreated_at($date);
                $this->em->getRepository(CommunicationConsent::class)->save($consent);
            }
            $this->em->getRepository(Transaction::class)->validateDonation($user, $data['value'], $directory);
            /* ------- Pega chave IUGU -------- */
            $keyIUGU = null;
            $depositDirectory = 1;
            $iuguTokens = $this->getIuguTokenDirectory($directory->getId());
            if($iuguTokens) {
                $keyIUGU = $iuguTokens['key'];
                $depositDirectory = $iuguTokens['deposit'];
            } 
            /* --------------- */
            if ($data['paymentMethod'] == 1) {


                $logs = new Logs();
                $logs->setMethod('donation')
                    ->setContent(json_encode($data))
                    ->setInserted($date);
                $this->em->getRepository(Logs::class)->save($logs);
                $validateAttempts = $this->em->getRepository(Logs::class)->publicDonationCheckValue()['total'];
                if($validateAttempts >= 7) throw new \Exception("Tente novamente mais tarde ou escolha outra forma de pagamento.");
                
                /** Save card IUGU */
                $card = $this->createCardApiIugu($user, $data['cardNumber'], $data['name'], $data['cvv'], $data['expiringDate'], $data['flag'], $data['international']);
                /** Cobrança */
                $address = $this->iuguAddressDonation();
                $personIugu = $this->getCustomerIdIUGU($user);
                $charge = IuguService::charge($personIugu->getCustomerId(), $card['paymentToken'], $user->getEmail(), $user->getCpf(), $user->getName(), $address, $data['value'], 'Doação - ' . $directory->getName(), 0, $keyIUGU);
                if ($data['plan'] == 1) $subscription = $this->createSubscription($user, $data, 1, $card['customer'], $card['paymentToken'], 1)->getId();
                $transaction = $this->populateTransaction($user, $data['value'], $date, $date, $date, 1, 'paid', $personIugu->getCustomerId(), $data['plan'], 1, 1, 
                    '', $card['paymentToken'], $charge['invoice_id'], '', $depositDirectory, intVal($directory->getId()), '', '', $subscription ?? '');
                $this->em->getRepository(Transaction::class)->save($transaction);
                $msg = 'Doação realizada com sucesso.';
            } else if ($data['paymentMethod'] == 2) {
                $address = $this->iuguAddressDonation();
                $slip = IuguService::slip($user->getEmail(), $user->getCpf(), $user->getName(), $address, $data['value'], 'Doação - ' . $directory->getName(), 5, $keyIUGU);
                if ($data['plan'] == 1) $subscription = $this->createSubscription($user, $data, 1, null, null, 1)->getId();
                $transaction = $this->populateTransaction($user, $data['value'], $date, $date, null, 2, 'pending', '', $data['plan'], 1, 1, '', '', $slip['id'], '', 
                    $depositDirectory, intVal($directory->getId()), $slip['bank_slip']['bank_slip_pdf_url'], $slip['bank_slip']['digitable_line'], $subscription ?? '');
                $this->em->getRepository(Transaction::class)->save($transaction);
                $msg = "Boleto gerado com sucesso. <br> 
                        Código de Barras: {$slip['bank_slip']['digitable_line']} <br> 
                        <b>O boleto pode levar até 20 minutos para ser registrado.</b><br> 
                        <span class='btn btn-primary' data-clipboard-text='{$slip['bank_slip']['digitable_line']}'>Copiar Código de Barras <i class='fa fa-barcode'></i></span>
                        <a class='btn btn-success' href='{$slip['bank_slip']['bank_slip_pdf_url']}' target='_blank'>Baixar o boleto <i class='fa fa-file-pdf-o'></i></a><br>
                        Atenção, seu boleto pode demorar até 2 horas para ser registrado, se não conseguir realizar o pagamento aguarde alguns instantes ou tente fazer via PIX.";
            } else if ($data['paymentMethod'] == 5) {
                $address = $this->iuguAddressDonation();
                $pix = IuguService::slip($user->getEmail(), $user->getCpf(), $user->getName(), $address, $data['value'], 'Doação - ' . $directory->getName(), 5, $keyIUGU);
                if (!$pix['pix']['qrcode_text']) throw new \Exception("Erro ao gerar QR Code Pix");
                $data['paymentMethod'] = 2;
                if ($data['plan'] == 1) $subscription = $this->createSubscription($user, $data, 1, null, null, 1)->getId();
                $transaction = $this->populateTransaction($user, $data['value'], $date, $date, null, 11, 'pending', '', $data['plan'], 1, 1, '', '', $pix['id'], '',
                    $depositDirectory, intVal($directory->getId()), $pix['pix']['qrcode'], '', $subscription ?? '');
                $transaction = $this->em->getRepository(Transaction::class)->save($transaction);
                $msg = "QR Code para doação gerado com sucesso. <br> 
                        <span class='btn btn-primary' data-clipboard-text='{$pix['pix']['qrcode_text']}'>PIX Copia e Cola <i class='fa fa-qrcode'></i></span> <br>
                        <img src=\"{$pix['pix']['qrcode']}\" style='width: 70%;'> <br>
                        <b>O sistema pode levar até 24h para dar a baixa na doação.</b>";
            }
            return $response->withJson([
                'status' => 'ok',
                'message' => $msg,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function publicEventSubscription(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $fields = [
                'directory' => 'Diretório',
                'paymentMethod' => 'Forma de Pagamento',
                'plan' => 'Plano de Pagamento',
                'value' => 'Valor',
            ];
            if ($data['value'] == 0) {
                $data['value2'] = str_replace('.', '', $data['value2']);
                $data['value2'] = str_replace(',', '.', $data['value2']);
                $data['value'] = $data['value2'];
            }
            if ($data['paymentMethod'] == 1) {
                $fields['card'] = 'Cartão de crédito';
            }
            if ($data['plan'] == 1) {
                $data['directory'] = 1; //DN
            }
            Validator::requireValidator($fields, $data);
            if ($data['value'] < 10) {
                throw new \Exception("Valor minimo para a doação é R$ 10,00");
            }
            GoogleService::recapchaEspacoNovo($data['g-recaptcha-response']);
            $directory = $this->em->getRepository(Directory::class)->find($data['directory']);
            if ($data['value'] > MAXVALUE) {
                $directoryAccount = $this->em->getRepository(Directory::class)->getAccount($directory);
                throw new \Exception("Valor máximo para a doação via sistema é R$ " . number_format(MAXVALUE, 2, ',', '.') . ".<br> 
                                                Para doar um valor superior faça uma transfêrencia bancaria:<br> 
                                                <b>Banco:</b> {$directoryAccount['bank']} <br>
                                                <b>Agência:</b> {$directoryAccount['agency']} <br>
                                                <b>Conta:</b> {$directoryAccount['account']} <br>
                                                <b>CNPJ:</b> {$directoryAccount['cnpj']} <br>");
            }
            $user = $this->getLogged();
            $this->em->getRepository(Transaction::class)->validateDonation($user, $data['value'], $directory);
            $date = new \DateTime();
            $subscription = null;
            if ($data['paymentMethod'] == 1) { // cartao de credito
                $card = $this->em->getRepository(PersonCreditCard::class)->find($data['card']);
                if ($data['plan'] == 1) {
                    $subscription = $this->createSubscription($user, $data, $card->getToken(), null)->getId();
                }
                if ($card->getGatewayPagamento() == 2) {//rede
                    $personRede = $this->em->getRepository(PersonRede::class)->findOneBy(['user' => $user->getId()], ['id' => 'DESC']);
                    $directoryGatway = $this->em->getRepository(DirectoryGatway::class)->findOneBy(['tbDiretorioId' => $data['directory'],
                        'formaPagamento' => 1, 'gatwayPagamento' => 2]);
                    $gatway = 2;

                    $transactionRede = MaxiPago::transaction($personRede, $card->getToken(), $data['value'], '', $directoryGatway, $user);
                    $directory = $directoryGatway ? $data['directory'] : 1; //DN
                    $customerId = $personRede->getCustomerId();
                    $invoiceId = $transactionRede['transactionID'];
                    $rederId = $transactionRede['orderId'];
                } else { // iugu
                    $personIugu = $this->getCustomerIdIUGU($user);
                    $address = $this->iuguAddress($this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()]));
                    $charge = IuguService::charge($personIugu->getCustomerId(), $card->getToken(), $user->getEmail(), $user->getCpf(), $user->getName(), $address, $data['value']);
                    $gatway = 1;
                    $invoiceId = $charge['invoice_id'];
                    $customerId = $personIugu->getCustomerId();
                    $rederId = '';
                }
                $transaction = $this->populateTransaction($user, $data['value'], $date, $date, $date, 1, 'paid',
                    $customerId, $data['plan'], $gatway, 1, '',$card->getToken(), $invoiceId, $rederId, $directory, $data['directory'], $subscription ?? '');
                $this->em->getRepository(Transaction::class)->save($transaction);
                return $response->withJson([
                    'status' => 'ok',
                    'message' => 'Doação realizada com sucesso',
                ], 201)
                    ->withHeader('Content-type', 'application/json');
            }
            // boleto
            if ($data['plan'] == 1) {
                $subscription = $this->createSubscription($user, $data, 1, null)->getId();
            }
            $address = $this->iuguAddress($this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()]));
            $slip = IuguService::slip($user->getEmail(), $user->getCpf(), $user->getName(), $address, $data['value'], 'Evento');
            $date = new \DateTime();
            $transaction = $this->populateTransaction($user, $data['value'], $date, $date, null, 2, 'pending',
                '', $data['plan'], 1, 1,'','', $slip['id'], '',
                1, $data['directory'], $slip['bank_slip']['bank_slip_pdf_url'], $slip['bank_slip']['digitable_line'], $subscription ?? '');
            $this->em->getRepository(Transaction::class)->save($transaction);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Boleto gerado com sucesso. <br> 
                                Código de Barras: {$slip['bank_slip']['digitable_line']} <br> 
                                <span class='btn btn-primary' data-clipboard-text='{$slip['bank_slip']['digitable_line']}'>Copiar Código de Barras <i class='fa fa-barcode'></i></span>
                                <a class='btn btn-success' href='{$slip['bank_slip']['bank_slip_pdf_url']}' target='_blank'>Baixar o boleto <i class='fa fa-file-pdf-o'></i></a><br>
                                Atenção, seu boleto pode demorar até 2 horas para ser registrado, se não conseguir realizar o pagamento aguarde alguns instantes ou tente fazer via PIX.",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function publicRegister(request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            Validator::requireValidator([
                'cpf' => 'CPF',
                'password' => 'Senha',
                'email' => 'E-mail',
                'birth' => 'Data Nascimento'
           ], $data);
            $user = $this->em->getRepository(User::class)->findOneBy(['cpf' => Utils::formatCpf($data['cpf'] ?? '')]);
            if ($user) throw new FoundException('CPF já foi cadastrado');
            if (!$user) {
                $checkMail = $this->em->getRepository(User::class)->findOneBy(['email' => $data['email']]);
                if ($checkMail && ($user && $user->getId() != $checkMail->getId())) throw new FoundException('Email já foi cadastrado');
            }
            $user = new User();
            $user->setCpf(Utils::formatCpf($data['cpf']))
                    ->setDataCriacao(new \DateTime())
                    ->setPassword(md5($data['password']))
                    ->setFiliado(0)
                    ->setStatus(0)
                    ->setEmail($data['email'])
                    ->setName($data['name'])
                    ->setDataNascimento(\DateTime::createFromFormat('d/m/Y', $data['birth']))   
                    ->setTituloEleitoralPaisId($this->em->getReference(Country::class, 33))         
                    ->setTermoAceite0(1)
                    ->setTermoAceite1(1)
                    ->setTermoAceite2(1)
                    ->setTermoAceite3(1)
                    ->setEstadoCivil($this->em->getReference(CivilState::class, 6))
                    ->setGenero($this->em->getReference(Gender::class, 3))
                    ->setEscolaridade($this->em->getReference(Schooling::class, 7))
                    ->setOpcaoReligiosa(11)
                    ->setIp(Utils::getAcessData()['ip'])
                    ->setUltimaAtualizacao(new \Datetime());
            $this->em->getRepository(User::class)->save($user);
            $this->confirmEmail($user, 'Cadastro - Ativação da Conta');
            $this->populateAddress($data, $user);
            $this->populatePhone($data, $user);
            $this->registerHistoric($user, 1);
            $valid = false;
            if(isset($data['term'])){
                
                /*-----Hubspot/Mailchimp-----*/
                Hubspot::createContact($user);
                $this->mailchimpCreateUser($user, null, $data['cellPhone']);

                $valid = true;
            }
            //register consent
            $consent = new CommunicationConsent();
            $consent->setUser($user->getId())->setType(1)->setValid($valid)->setCreated_at(new \Datetime());
            $this->em->getRepository(CommunicationConsent::class)->save($consent);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => "Cadastro realizado com sucesso!",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function confirmAffiliation(request $request, Response $response)
    {
        $hash = $request->getAttribute('route')->getArgument('hash');
        $indication = $this->em->getRepository(IndicationOfAffiliation::class)->findOneBy(['hash' => $hash, 'used' => 0]);
        if(!$indication) {
            echo 'Solicitação Inválida!';
            die();
        }
        $user = $this->em->getRepository(User::class)->findOneBy(['id' => $indication->getUser()->getId()]);  
        $cellPhone = $this->em->getRepository(Phone::class)->findOneBy(['tbPessoaId' => $user->getId(), 'tbTipoTelefoneId' => 3]);      
        $address = $this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()],['id' => 'DESC']);
        $addressState = $this->em->getReference(State::class, $address->getStateId());
        $addressCity = $this->em->getReference(City::class, $address->getTbCidadeId());
        $states = $this->em->getRepository(State::class)->findAll();
        $countries = $this->em->getRepository(Country::class)->findAll();
        return $this->renderer->render($response, 'public/acceptAffiliation/index.phtml', ['states' => $states, 'countries' => $countries, 'user' => $user,
            'cellPhone' => $cellPhone, 'address' => $address, 'addressCity' => $addressCity, 'addressState' => $addressState]);
    }
}