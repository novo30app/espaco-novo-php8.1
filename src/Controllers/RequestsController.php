<?php

namespace App\Controllers;

use App\Helpers\Validator;
use App\Models\Entities\Impeachment;
use App\Models\Entities\State;
use App\Models\Entities\User;
use App\Models\Entities\AccessLog;
use App\Services\Email;
use App\Services\Hubspot;
use App\Services\Mailchimp;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class RequestsController extends Controller
{
    public function membership(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $states = $this->em->getRepository(State::class)->findAll(); 
        return $this->renderer->render($response, 'default.phtml', ['page' => 'requests/membership.phtml', 'states' => $states,
            'section' => 'membershipRequests', 'user' => $user, 'subMenu' => 'myRequests', 'title' => 'Solicitações de Filiação']);
    }

    public function membershipList(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $user->isAffiliated(true);
        $name = $request->getQueryParam('name');
        $uf = $request->getQueryParam('uf');
        $city = $request->getQueryParam('city');
        $index = $request->getQueryParam('index');
        $total = $this->em->getRepository(User::class)->getListToImpeachment($user, $city, $uf, $name);
        $requestsArray = $this->em->getRepository(User::class)->getListToImpeachment($user, $city, $uf, $name, 20, $index * 20);
        $total = sizeof($total);
        $partial = ($index * 20) + sizeof($requestsArray);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $requestsArray,
            'total' => $total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function impeachment(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $fields = [
                'user' => 'user',
                'target' => 'target',
                'accessLog' => 'accessLog',
                'msg' => 'Motivo',
            ];
            Validator::requireValidator($fields, $data);
            $target = $this->em->getRepository(User::class)->find($data['target']);
            $user = $this->em->getRepository(User::class)->find($data['user']);
            $target->setFiliado(4);
            $this->em->getRepository(User::class)->save($target);
            $impeachment = new Impeachment();
            $impeachment->setMsg($data['msg'])
                ->setUser($user)
                ->setTarget($target)
                ->setAccessLog($this->em->getReference(AccessLog::class, $data['accessLog']));
            $this->em->getRepository(Impeachment::class)->save($impeachment);
            $data['msg'] = nl2br($data['msg']);
            $msg = "<p><b>Dados do filiado que solicitou a impugnação:</b><br>";
            $msg .= "<b>Nome:</b> {$user->getName()}<br>";
            //$msg .= "<b>Email:</b> {$user->getEmail()}<br>";
            //$msg .= "<b>CPF:</b> {$user->getCpf()}<br>";
            $msg .= "<b>Motivo:</b> {$data['msg']}</p>";
            $msg .= "<p><b>Dados da pessoa a ser impugnada:</b><br>";
            $msg .= "<b>Nome:</b> {$target->getName()}<br>";
            //$msg .= "<b>Email:</b> {$target->getEmail()}<br>";
            //$msg .= "<b>CPF:</b> {$target->getCpf()}<br>";
            $msg .= "<b>Atenção: este é um e-mail automático; por favor não responda.</p>";
            Email::send('refiliacao@novo.org.br', 'Filiação', 'Novo Pedido de impugnação', $msg);
            
            /*-----Conversões Hubspot/Mailchimp-----*/
            $this->updateStatusRegister($user);

            return $response->withJson([
                'status' => 'ok',
                'message' => 'Solicitação de impugnação cadastrada com sucesso.',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }

    }


}