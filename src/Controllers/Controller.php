<?php

namespace App\Controllers;

use App\Models\Commons\Entities\LibertasCourse;
use App\Services\EmailLibertas;
use App\Services\Libertas;
use Doctrine\ORM\EntityManager;
use App\Helpers\Utils;
use App\Helpers\Messages;
use App\Helpers\Validator;
use App\Helpers\Session;
use App\Models\Entities\Address;
use App\Models\Entities\ChangeEmail;
use App\Models\Entities\City;
use App\Models\Entities\State;
use App\Models\Entities\Transaction;
use App\Models\Entities\User;
use App\Models\Entities\PersonCreditCard;
use App\Models\Entities\PersonSignature;
use App\Models\Entities\Directory;
use App\Models\Entities\smsSend;
use App\Models\Entities\Historic;
use App\Models\Entities\PersonIugu;
use App\Models\Entities\Indicator;
use App\Models\Entities\Events;
use App\Models\Entities\HistoricalIncrease;
use App\Models\Entities\IuguTokens;
use App\Models\Entities\IndicationOfAffiliation;
use App\Models\Entities\Campaigns;
use App\Models\Entities\CampaignsUsers;
use App\Models\Entities\Country;
use App\Services\IuguService;
use App\Services\SmsService;
use App\Services\Hubspot;
use App\Services\Email;
use App\Services\Mailchimp;
use Slim\Views\PhpRenderer;

abstract class Controller
{
    protected $em;
    protected $renderer;
    protected $baseUrl = BASEURL;
    protected $env = ENV;

    public function __construct(EntityManager $entityManager, PhpRenderer $renderer)
    {
        $this->em = $entityManager;
        $this->renderer = $renderer;
    }

    protected function getConfigs()
    {
        $config = parse_ini_file('configs.ini', true);
        return $config[$config['environment']];
    }

    protected function getLoggedApi(bool $excepetion = false)
    {
        $user = Session::get('user');
        if (!$user) {
            throw new \Exception("Sessão expirada");
        }
        $user = $this->em->getRepository(User::class)->find($user->getId());
        return $user;
    }

    protected function getLogged(array $affiliatedStatus = [], bool $libertasLogin = false)
    {
        $user = Session::get('user');
        if (!$user) {
            Session::set('redirect', $_SERVER["REQUEST_URI"]);
            Session::set('errorMsg', 'Você precisa se autenticar');
            $url = 'login';
            if ($libertasLogin) $url = 'instituto-libertas/login';
            $this->redirect($url);
            exit;
        }
        $user = $this->em->getRepository(User::class)->find($user->getId());
        if (!empty($affiliatedStatus) && !in_array($user->getFiliado(), $affiliatedStatus)) {
            Session::set('errorMsg', 'Você não tem permissão para acessar esse conteúdo');
            $this->redirect();
        }
        return $user;
    }

    protected function redirect(string $url = '', $message = false)
    {
        if ($message) Session::set('errorMsg', 'Você não tem permissão para acessar esse conteúdo');
        if ($message && is_string($message)) Session::set('errorMsg', $message);
        header("Location: {$this->baseUrl}{$url}");
        die();
    }

    protected function redirectExternal(string $url = '')
    {
        header("Location: {$url}");
        die();
    }

    public function responseJson($result, $response, $code = 200)
    {
        if ($result instanceof \Exception) {
            return $response->withJson(['status' => 'error',
                'message' => $result->getMessage(),])->withStatus(500);
        }

        return $response->withJson($result)
            ->withStatus($code)
            ->withHeader('Content-type', 'application/json');
    }

    protected function iuguAddressDonation($city = null, $state = null): array
    {
        return [
//            'street' => $address->getEndereco(),
            'number' => '1',
//            'district' => $address->getBairro(),
            'city' => $city ?? 'São Paulo',
            'state' => $state ?? 'SP',
            'zip_code' => '01034902',
        ];
    }

    protected function iuguAddress(Address $address): array
    {
        $city = $state = false;
        if (!$address->getResideExterior()) {
            $city = $this->em->getRepository(City::class)->find($address->getTbCidadeId())->getCidade();
            $state = $this->em->getRepository(State::class)->find($address->getStateId())->getSigla();
        }
        return [
            'street' => $address->getEndereco(),
            'number' => $address->getNumero(),
            'district' => $address->getBairro(),
            'city' => $city ?? 'São Paulo',
            'state' => $state ?? 'SP',
            'zip_code' => Utils::onlyNumbers($address->getCep()),
            'outside' => $address->getResideExterior(),
        ];
    }

    protected function confirmEmail(User $user, string $subject = 'Ativação da Conta')
    {
        $token = Utils::generateToken();
        $changeEmail = new ChangeEmail();
        $changeEmail->setUser($user)
            ->setCreated(new \DateTime())
            ->setUsed(0)
            ->setToken($token)
            ->setEmail($user->getEmail());
        $this->em->getRepository(ChangeEmail::class)->save($changeEmail);
        $msg = "<p>Olá, {$user->getName()}.<br>
                Agradecemos o seu interesse em acessar o Espaço NOVO.<br>
                Porém, você ainda não ativou seu cadastro. Para finalizar seu cadastro e confirmar seu email <a href='{$this->baseUrl}usuario/alterar-email/{$token}' target='_blank'>Clique aqui!</a><br>
                <p>Caso tenha alguma dúvida entre em contato conosco pelo e-mail: filiacao@novo.org.br<br>
                <p>Equipe NOVO</p>";
        Email::send($user->getEmail(), $user->getName(), $subject, $msg);
    }

    protected function createSubscription(User $user, array $data, int $gateway, $redeId = false, $card = false, $origin = 2, $directory = null): PersonSignature
    {
        $subscription = false;
        if ($origin == 2) {//só pode ter uma assinatura de filiação
            $subscription = $this->em->getRepository(PersonSignature::class)->findOneBy(['tbPessoaId' => $user, 'origin' => 2]);
        }
        if (!$subscription) $subscription = new PersonSignature();
        $today = new \DateTime();
        $nextCharge = new \DateTime();
        $nextCharge->add(new \DateInterval("P{$data['plan']}M"));
        $cycleDay = $data['dueDate'] ?: $today->format('d');
        $subscription->setStatus('created')
            ->setDataCriacao($today)
            ->setOrigemTransacao($origin)
            ->setGatewayPagamento($gateway)
            ->setGatewayStatus(1)
            ->setCustomerId($redeId)
            ->setTbPessoaId($user->getId())
            ->setValor($data['value'])
            ->setPaymentForm($data['paymentMethod'])
            ->setPeriodicidade($data['plan'])
            ->setDiaCiclo($cycleDay)
            ->setMesCiclo(date('m'))
            ->setDataGeraFatura($nextCharge)
            ->setTbDiretorioOrigem((int)$data['directory']);
        if ($card) {
            $subscription->setTokenCartaoCredito($card);
        }
        return $this->em->getRepository(PersonSignature::class)->save($subscription);
    }

    protected function populateTransaction(User   $user, float $value, \DateTime $creationDate, \DateTime $dueDate, ?\DateTime $paymentDate, int $paymentType,
                                           string $status, string $customerId, int $plan, int $gateway, int $origin, string $obs = null, string $tokenCreditCard, string $invoiceId,
                                           string $redeId, int $directory, int $directoryOrigin, string $url = '', string $barCode = '', $subscription = false, string $qrCode = null, string $creditCardScheme = '')
    {
        $receipt = 'Em processamento';
        //if($status == 'paid'){
        //    $directory = $this->em->getRepository(Directory::class)->findOneBy(['id' => $directory]);
        //    $maskReceipt = $directory->getMaskReceipt();
        //    $lastDirectoryReceipt = $this->em->getRepository(Transaction::class)->lastDirectoryReceipt($directory->getId());
        //    $receipt = $maskReceipt . $lastDirectoryReceipt[0]['receipt'];
        //}

        $transaction = new Transaction();
        $transaction->setTbPessoaId($user->getId())
            ->setPendenciaTitulo($user->getPendenciaTitulo() ?? 0)
            ->setValor($value)
            ->setDataCriacao($creationDate)
            ->setDataVencimento($dueDate)
            ->setDataPago($paymentDate)
            ->setStatus($status)
            ->setFormaPagamento($paymentType)
            ->setPeriodicidade($plan)
            ->setCustomerId($customerId)
            ->setGatewayPagamento($gateway)
            ->setOrigemTransacao($origin)
            ->setObs($obs)
            ->setTokenCartaoCredito($tokenCreditCard)
            ->setInvoiceId($invoiceId)
            ->setRedeId($redeId)
            ->setTbDiretorioId($directory)
            ->setTbDiretorioOrigem($directoryOrigin)
            ->setUrl($url)
            ->setCodigoBarras($barCode)
            ->setNumeroRecibo($receipt)
            ->setQrCode($qrCode)
            ->setCreditCardFlag($creditCardScheme);
        if ($status == 'paid') {
            $transaction->setNumeroRecibo($this->getReceiptDirectory($this->em->getReference(Directory::class, $directory), $transaction));
        }
        if ($subscription) {
            $transaction->setTbAssinaturaId($subscription);
        }
        return $transaction;
    }

    protected function smsService($firstName, $record, $msg)
    {
        $date = new \Datetime();
        $response = SmsService::send_sms($record['phone'], $msg);
        $this->smsSend($record['tb_pessoa_id'], 1, 'Recobrança', $date, $msg, $record['phone'], $response);
    }

    protected function smsSend(int $id, int $task, string $type, \DateTime $date, string $msg, string $phone, string $response)
    {
        $smsSend = new smsSend();
        $smsSend->setTbPessoaId($id)
            ->setTask($task)
            ->setType($type)
            ->setDate($date)
            ->setMessage($msg)
            ->setPhone($phone)
            ->setObs($response);
        $this->em->getRepository(smsSend::class)->save($smsSend);
    }

    protected function nextCharge($record)
    {
        $day = $record['dia_ciclo'];
        $period = "+{$record['periodicidade']} month";
        $dataGeraFatura = date('Y-m', strtotime($record['data_gera_fatura']));
        $nextCharge = date("Y-m", strtotime($dataGeraFatura . $period)) . '-' . $day;
        $explode = explode('-', $nextCharge);
        $valid = checkdate($explode[1], $day, $explode[0]);
        while ($valid == false) {
            $day--;
            $nextCharge = date("Y-m", strtotime($dataGeraFatura . $period)) . '-' . $day;
            $explode = explode('-', $nextCharge);
            $valid = checkdate($explode[1], $day, $explode[0]);
        }
        return $nextCharge;
    }

    protected function notificationTerm0($user)
    {
        switch ($user->getTermoAceite0()) {
            case 2:
                $msg = Messages::notificationTerm0Filiation($user);
                Email::send('refiliacao@novo.org.br', '', 'Aviso Termo 1 - Nova Filiação', $msg);
                break;
            case 3:
                $msg = Messages::notificationTerm0Dam($user);
                Email::send('dam@novo.org.br', '', 'Nova Filiação - Indicação Termo 1', $msg);
                $msg = Messages::notificationTerm0Affiliation();
                Email::send($user->getEmail(), $user->getName(), 'Aviso Filiação - Partido NOVO', $msg);
                break;
        }
    }

    protected function registerHistoric($user, int $type)
    {
        $historic = new Historic();
        $historic->setUser($user)->setType($type)->setDate(new \Datetime);
        $this->em->getRepository(Historic::class)->save($historic);
    }

    protected function redirectByPermissions($url = '')
    {
        Session::set('errorMsg', 'Você não tem permissão para acessar esse conteúdo.');
        header("Location: {$this->baseUrl}{$url}");
        die();
    }

    protected function createCardApiIugu(User   $user, string $cardNumber, string $cardName, string $cvv,
                                         string $expiringDate, string $flag, string $international): array
    {
        // create person
        $personIugu = $this->getCustomerIdIUGU($user);
        // create paymentform
        $cardTokenIugu = IuguService::createToken($cardNumber, rtrim($cardName), $cvv, $expiringDate);
        $paymentToken = IuguService::createPaymentMethods($personIugu->getCustomerId(), $cardTokenIugu);
        // create card
        if ($cardTokenIugu) {
            $card = new PersonCreditCard();
            $card->setToken($paymentToken)
                ->setUser($user)
                ->setFlag($flag)
                ->setName($cardName)
                ->setGatewayPagamento(1)
                ->setInternational($international)
                ->setExpiration($expiringDate)
                ->setCardNumber(substr($cardNumber, -4))
                ->setVisivel(1)
                ->setPrincipal(1);
            $this->em->getRepository(PersonCreditCard::class)->save($card);
        }
        return [
            'paymentToken' => $paymentToken,
            'customer' => $personIugu->getCustomerId()
        ];
    }

    protected function setWhoIndicate(User $user, string $indicator, int $origin, string $event = null): void
    {
        $getIndicator = $this->em->getRepository(User::class)->findOneBy(['id' => $indicator]);
        if ($getIndicator) {
            if ($getIndicator->getId() != $user->getId()) {
                $indicator = new Indicator();
                $indicator->setCreated_at(new \Datetime())
                    ->setUser($user)
                    ->setIndicator($getIndicator)
                    ->setOrigin($origin);
                if ($event) {
                    $event = $this->em->getRepository(Events::class)->findOneBy(['id' => $event]);
                    $indicator->setEventId($event);
                }
                $this->em->getRepository(Indicator::class)->save($indicator);
            }
        }
    }

    protected function setCampaign(User $user, string $campaign): void
    {
        $campaign = $this->em->getRepository(Campaigns::class)->findOneBy(['hash' => $campaign, 'status' => 1]);
        if ($campaign) {
            $campaignUsers = $this->em->getRepository(CampaignsUsers::class)->findOneBy(['user' => $user, 'campaign' => $campaign->getId()]);
            if (!$campaignUsers) $campaignUsers = new CampaignsUsers();
            $campaignUsers->setUser($user)
                ->setCampaign($campaign);
            $this->em->getRepository(CampaignsUsers::class)->save($campaignUsers);
        }
    }

    protected function setHistoricalIncrease(int $user, string $phase, float $value = null, float $newValue = null, string $message = null): void
    {
        $historicalIncrease = new HistoricalIncrease();
        $historicalIncrease->setUser($user)
            ->setPhase($phase)
            ->setDateEvent(new \Datetime())
            ->setCurrentValue($value)
            ->setNewValue($newValue)
            ->setObs($message);
        $this->em->getRepository(HistoricalIncrease::class)->save($historicalIncrease);
    }

    protected function tirarAcentos($string)
    {
        return preg_replace(array("/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/", "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/", "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/", "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/", "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/", "/(ñ)/", "/(Ñ)/"), explode(" ", "a A e E i I o O u U n N"), $string);
    }

    protected function payLastOpened(User $user, string $token, array $data = null): void
    {
        $date = new \DateTime();
        $transaction = $this->em->getRepository(Transaction::class)->getLastOpenedTransaction($user);
        if ($transaction) {
            $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['id' => $transaction['id']]);
            $value = $data ? $data['value'] : $transaction->getValor();
            $personIugu = $this->getCustomerIdIUGU($user);
            $address = $this->iuguAddress($this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()]));
            $validadeCep = Validator::validateCep($address);
            if ($validadeCep == false) $address = $this->iuguAddressDonation();
            $charge = IuguService::charge($personIugu->getCustomerId(), $token, $user->getEmail(), $user->getCpf(), $user->getName(), $address, $value, '');
            if ($charge['status'] == 'captured') {
                $transaction->setUltimaAtualizacao($date)
                    ->setStatus('paid')
                    ->setValor((float)$value)
                    ->setDataPago($date)
                    ->setFormaPagamento(1)
                    ->setGatewayPagamento(1)
                    ->setInvoiceId($charge['invoice_id'])
                    ->setNumeroRecibo($this->getReceiptDirectory($this->em->getReference(Directory::class, $transaction->getTbDiretorioId()), $transaction));
                $this->em->getRepository(Transaction::class)->save($transaction);
                $this->validLastTransaction($user, $transaction, 4);
            }
            /*-----Verifica se fatura paga e de filiação e se o cadastro está aguardando pagamento-----*/
            if ($transaction->getOrigemTransacao() == 2 && in_array($user->getFiliado(), array(1, 3))) {
                /*-----Atualiza o cadastro para período de impugnação e envia e-mail informativo-----*/
                $statusAffiliated = $user->getFiliado() == 1 ? 2 : 14;
                $user->setFiliado($statusAffiliated);
                if ($statusAffiliated == 2) {
                    //$user->setDataSolicitacaoFiliacao($date);
                    $type = 2;
                } else {
                    //$user->setDataSolicitacaoRefiliacao($date);
                    $type = 5;
                }
                $this->em->getRepository(User::class)->save($user);

                /*-----Hubspot/Mailchimp-----*/
                $this->updateStatusRegister($user);

                $this->registerHistoric($user, $type);
                $subject = "Pagamento de filiação confirmado!";
                $firstName = explode(' ', $user->getName())[0];
                $msg = Messages::affiliationPaymentConfirmed($firstName);
                Email::send($user->getEmail(), $firstName, $subject, $msg);
                $this->checkIndicationOfAffiliation($user, 4);
            }
        }
    }

    protected function checkCardName(User $user, string $cardName): void
    {
        $cardName = explode(' ', $cardName);
        $cardName = $this->tirarAcentos(strtolower($cardName[0]));
        $userName = $this->tirarAcentos(strtolower($user->getfirstName()));
        $valid = true;
        if (strlen($cardName) > 1) {
            if ($userName != $cardName) {
                $valid = false;
            }
        } else {
            if (substr($userName, 0, 1) != substr($cardName, 0, 1)) {
                $valid = false;
            }
        }
        if (!$valid) throw new \Exception('Atenção, permitido apenas cartões em nome do titular do cadastro!');
    }

    protected function disabledCards(User $user): void
    {
        $cards = $this->em->getRepository(PersonCreditCard::class)->findBy(['user' => $user->getId()]);
        foreach ($cards as $card) {
            $card->setPrincipal(0)->setVisivel(0);
            $this->em->getRepository(PersonCreditCard::class)->save($card);
        }
    }

    public function getWorkingDay(string $date, string $days = null): \Datetime
    {
        if (!$days) $days = '+31 days';
        /** Recebe data, converte e adiciona 31 dias */
        $date = new \Datetime(date('Y-m-d', strtotime($date)));
        $date->modify($days);
        $dateFormat = $date->format('Y-m-d');
        //foreach($holidays as $holiday){
        //    if($dateFormat === date('Y-') . $holiday->getData()) {
        //        $date->modify('+1 days');
        //        $dateFormat = $date->format('Y-m-d');
        //    }
        //}
        /** Verifica se é final de semana, se for adiciona um dia até que não seja mais */
        $weekDay = date('w', strtotime($dateFormat));
        while (in_array($weekDay, array(0, 6))) {
            $date->modify('+1 days');
            $dateFormat = $date->format('Y-m-d');
            $weekDay = date('w', strtotime($dateFormat));
        }
        /** Retorna data útil correto */
        return $date;
    }

    public function getIuguTokenDirectory(int $directory): array
    {
        $key = '473900DCC93F1CF5329439FDA1EE86A494F5B694ADF7AD70F1A8224971FF9B59';
        //$key = 'AE54672FA6DFE513E178719FC62BABF9724A456B214155E89A71AD91E76E0C73';
        $deposit = 1;
        $veriryTokenIugu = $this->em->getRepository(IuguTokens::class)->findOneBy(['directory' => 1]);
        if ($veriryTokenIugu) {
            $key = $veriryTokenIugu->getToken();
            $deposit = $veriryTokenIugu->getDirectory();
        } else {
            $getDirectory = $this->em->getRepository(Directory::class)->findOneBy(['id' => $directory]);
            if ($getDirectory->isCity()) {
                $getDirectoryStateId = $this->em->getRepository(Directory::class)->findOneBy(['tbEstadoId' => $getDirectory->getStateId(), 'estadual' => 'S']);
                if ($getDirectoryStateId) {
                    $veriryTokenIugu = $this->em->getRepository(IuguTokens::class)->findOneBy(['directory' => $getDirectoryStateId->getId()]);
                    if ($veriryTokenIugu) {
                        $key = $veriryTokenIugu->getToken();
                        $deposit = $veriryTokenIugu->getDirectory();
                    }
                }
            }
        }
        $array = [
            'key' => $key,
            'deposit' => $deposit,
            'owner' => $directory
        ];
        return $array;
    }

    protected function checkIndicationOfAffiliation(User $user, int $status): void
    {
        $indication = $this->em->getRepository(IndicationOfAffiliation::class)->findOneBy(['user' => $user->getId()]);
        if ($indication) {
            $indication->setUsed(1)->setStatus($status);
            $this->em->getRepository(IndicationOfAffiliation::class)->save($indication);
        }
    }

    protected function updatePaymentStatus(User $user, int $type): void
    {
        $type = match ($type) {
            1 => 'Boleto - Em dia',
            2 => 'Boleto - Gerado',
            3 => 'Boleto - Vencido',
            4 => 'Cartão - Em dia',
            5 => 'Cartão - Erro',
            6 => 'Isento',
            default => 'Desconhecido'
        };

        /*-----Hubspot/Mailchimp-----*/
        $hp = [];
        $hp['email'] = $user->getEmail();
        $hp['status_pagamento'] = $type;
        Hubspot::updateContact($user, $hp);
        $body = [];
        $body['email_address'] = $user->getEmail();
        $body['merge_fields']['MMERGE59'] = $type;
        $body['merge_fields']['ADDRESS'] = '';
        Mailchimp::updateContact($body);
    }

    protected function validLastTransaction(User $user, Transaction $transaction, int $type): void
    {
        $valid = false;
        $lastTransaction = $this->em->getRepository(Transaction::class)->findOneBy(['tbPessoaId' => $user->getId(), 'origemTransacao' => 2], ['id' => 'DESC']);
        if ($lastTransaction) {
            if ($transaction->getId() == $lastTransaction->getId()) $valid = true;
        }
        if ($valid == true) $this->updatePaymentStatus($user, $type);
    }

    protected function getReceiptDirectory(Directory $directory, Transaction $transaction): string
    {
        if (!$transaction->getNumeroRecibo() || !preg_match("/{$transaction->getNumeroRecibo()}/", "P3000.")) {
            if ($directory->getReciboEmite() == 'S') {
                $mask = $this->em->getRepository(Directory::class)->find($directory)->getMaskReceipt();
                $lastReceipt = $this->em->getRepository(Transaction::class)->lastDirectoryReceipt($directory->getId())[0]['receipt'];
                $transactionsOfMonth = $this->em->getRepository(Transaction::class)->getTransactionsMonthlyByDirectory($directory);
                if (!$transactionsOfMonth) { // alterar para não existe
                    $receipts = [];
                    for ($i = 0; $i <= 6; $i++) {
                        $lastReceipt++;
                        array_push($receipts, $mask . str_pad($lastReceipt, 6, "0", STR_PAD_LEFT));
                    }
                    $msg = Messages::receiptReserved($receipts, $directory);
                    $copyTo = ['deliane@novo.org.br', 'renan.almeida@novo.org.br'];
                    Email::send('luciana.pereira@novo.org.br', 'Renan Almeida', "Recibos reservados",
                        $msg, null, null, null, $copyTo);
                }
                $lastReceipt++;
                return $mask . str_pad($lastReceipt, 6, "0", STR_PAD_LEFT);
            }
            return "Em processamento";
        }
        return $transaction->getNumeroRecibo();
    }

    protected function mailchimpCreateUser(User $user, $address = null, $phone = null): void
    {
        $body['email_address'] = $user->getEmail();
        $body['status'] = "subscribed";
        $body['merge_fields']['FNAME'] = $user->getFirstName();
        $body['merge_fields']['LNAME'] = $user->getSurname();
        if ($user->getDataNascimento()) $body['merge_fields']['BIRTHDAY'] = $user->getDataNascimento()->format('m/d');
        $body['merge_fields']['FILIADO'] = $user->getFiliadoString();
        $body['merge_fields']['MMERGE63'] = $user->getFiliadoString();
        $body['merge_fields']['MMERGE58'] = $user->getStatusString();
        if ($address) {
            $body['merge_fields']['ADDRESS']['city'] = $this->em->getRepository(City::class)->find($address->getTbCidadeId())->getCidade();
            $body['merge_fields']['ADDRESS']['state'] = $this->em->getRepository(State::class)->find($address->getStateId())->getEstado();
            $body['merge_fields']['ADDRESS']['country'] = $this->em->getRepository(Country::class)->find($address->getTbPaisId())->getNome();
        }
        if ($phone) $body['merge_fields']['PHONE'] = $phone;
        $body['merge_fields']['url'] = "novo.org.br";
        Mailchimp::createContact($body);
    }

    protected function updateStatusRegister(User $user): void
    {
        $hp = [];
        $hp['email'] = $user->getEmail();
        $hp['filiado'] = $user->getFiliadoString();
        $hp['status_de_filiado'] = $user->getFiliadoString();
        $hp['status_cadastro'] = $user->getStatusString();
        $hp['isento'] = $user->getExemption() == 1 ? 'SIM' : 'NÃO';
        $hp['plano_atual'] = $user->getPaymentPlanStr();
        Hubspot::updateContact($user, $hp);
        $body = [];
        $body['email_address'] = $user->getEmail();
        $body['merge_fields']['FILIADO'] = $user->getFiliadoString();
        $body['merge_fields']['MMERGE63'] = $user->getFiliadoString();
        $body['merge_fields']['MMERGE58'] = $user->getStatusString();
        $body['merge_fields']['ISENTO'] = $user->getExemption() == 1 ? 'SIM' : 'NÃO';
        $body['merge_fields']['ADDRESS'] = '';
        $body['merge_fields']['PLANO'] = $user->getPaymentPlanStr();
        Mailchimp::updateContact($body);
    }

    protected function matricularMoodle(User $user, LibertasCourse $course): array
    {
        $data = [
            'name' => $user->getName(),
            'email' => $user->getEmail(),
            'cpf' => $user->getCpf(),
            'filiado' => 0,
            'city' => '',
            'estado' => '',
            'courseId' => $course->getIdMoodle(),
        ];
        if ($user->isAffiliated()) $data['filiado'] = 1;

        $address = $this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()]);
        if ($address) {
            $data['city'] = $address->getCidade();
            $data['estado'] = $address->getEstado();
            if ($address->getTbCidadeId()) {
                $city = $this->em->getRepository(City::class)->find($address->getTbCidadeId());
                if ($city) {
                    $data['city'] = $city->getCidade();
                    $data['estado'] = $city->getState()->getSigla();
                }
            }
        }
        return Libertas::moodleLogin($data);
    }


    protected function confirmacaoCompraLibertas(User $user, string $subject = 'Ativação da Conta')
    {
        $token = Utils::generateToken();
        $changeEmail = new ChangeEmail();
        $changeEmail->setUser($user)
            ->setCreated(new \DateTime())
            ->setUsed(0)
            ->setToken($token)
            ->setEmail($user->getEmail());
        $this->em->getRepository(ChangeEmail::class)->save($changeEmail);
        $msg = "<p>{$user->getName()}, agradecemos o seu interesse no curso do Instituto Libertas.</p>
                <p>Porém, você ainda não ativou seu cadastro. Para finalizar seu cadastro e confirmar seu email <a href='{$this->baseUrl}usuario/alterar-email/{$token}' target='_blank'>Clique aqui!</a></p>
                <p>Caso tenha alguma dúvida entre em contato conosco pelo e-mail: contato@institutolibertas.org.br<br>
                <p>Equipe Instituto Libertas </p>";
        EmailLibertas::send($user->getEmail(), $user->getName(), $subject, $msg);
    }

    protected function getCustomerIdIUGU(User $user): PersonIugu
    {
        $personIugu = $this->em->getRepository(PersonIugu::class)->getCustomerId($user)['customer_id'];
        if($personIugu) {
            $personIugu = $this->em->getRepository(PersonIugu::class)->findOneBy(['customerId' => $personIugu]);
            return $personIugu;
        }
        $customer = IuguService::createCliente($user->getName(), $user->getEmail());
        $personIugu = new PersonIugu();
        $personIugu->setTbPessoaId($user->getId())->setCustomerId($customer);
        $this->em->getRepository(PersonIugu::class)->save($personIugu);
        return $personIugu;
    }

    protected function getTransactionPending(User $user): array
    {
        $validTransactionPending = false;
        $transactionPending = $this->em->getRepository(Transaction::class)->findOneBy(
            [
                'tbPessoaId' => $user->getId(), 
                'origemTransacao' => [2, 3],
                'periodicidade' => [1, 12], 
                'status' => ['pending', 'expired'],
            ], ['dataCriacao' => 'desc']);
        
        if($transactionPending && in_array($user->getFiliado(), [7,8])) {
            $validTransactionPending = date('Y-m') == $transactionPending->getDataCreation()->format('Y-m') ?? false;
        } 
        return [
            'validTransactionPending' => $validTransactionPending, 
            'transactionPending' => $transactionPending
        ];
    }
}
