<?php

namespace App\Controllers;

use App\Helpers\Session;
use App\Helpers\Validator;
use App\Helpers\Utils;
use App\Models\Entities\City;
use App\Models\Entities\State;
use App\Models\Entities\BellowSigned;
use App\Models\Entities\User;
use App\Models\Entities\Transaction;
use App\Models\Entities\CivilState;
use App\Models\Entities\Gender;
use App\Models\Entities\Schooling;
use App\Models\Entities\IDontWantToPay;
use App\Services\Hubspot;
use App\Services\GoogleService;
use App\Services\Pdf;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

class RegisterController extends Controller
{
    public function undersigned(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            $fields = ['name' => 'Nome', 'email' => 'E-mail'];
            Validator::requireValidator($fields, $data);
            GoogleService::recapchaEspacoNovo($data['g-recaptcha-response']);
            $records = $this->em->getRepository(BellowSigned::class)->findOneBy(['email' => $data['email'], 'theme' => $data['form']]);
            if ($records) throw new \Exception("Você já colaborou com sua assinatura!");
            $data['phone'] ?: '';
            $news = isset($data['news']) ? 1 : 0;
            $register = new BellowSigned();
            $register->setName($data['name'])
                ->setCreated(new \Datetime())
                ->setEmail($data['email'])
                ->setPhone($data['phone'])
                ->setNews($news)
                ->setCity($this->em->getReference(City::class, $data['city']))
                ->setState($this->em->getReference(State::class, $data['state']))
                ->setTheme($data['form']);
            $this->em->getRepository(BellowSigned::class)->save($register);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'O NOVO Agradece sua participação!',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    private function setNewUser($data): User
    {
        $user = $this->em->getRepository(User::class)->findOneBy(['cpf' => [Utils::formatCpf($data['cpf']), $data['cpf']]]);
        if (!$user) {
            $user = new User();
            $user->setName($data['name'])
                ->setCpf(Utils::formatCpf($data['cpf']))
                ->setEmail($data['email'])
                ->setPassword(md5('jornada2024'))
                ->setTermoAceite0(1)
                ->setTermoAceite1(1)
                ->setTermoAceite2(1)
                ->setTermoAceite3(1)
                ->setTituloEleitoralUfId($this->em->getReference(State::class, $data['state']))
                ->setTituloEleitoralMunicipioId($this->em->getReference(City::class, $data['city']))
                ->setEstadoCivil($this->em->getReference(CivilState::class, 6))
                ->setGenero($this->em->getReference(Gender::class, 3))
                ->setEscolaridade($this->em->getReference(Schooling::class, 7))
                ->setOpcaoReligiosa(11)
                ->setStatus(1)
                ->setDoador(1)
                ->setFiliado(0)
                ->setDataCriacao(new \DateTime())
                ->setObservacao('jornada2024')
                ->setEmailConfirmado(1);
            $this->em->getRepository(User::class)->save($user);
            
            /*-----Hubspot/Mailchimp-----*/
            Hubspot::createContact($user);
            $this->mailchimpCreateUser($user, null, null);
        }
        return $user;
    }

    public function setNewTransaction(User $user, array $data): void
    {
        $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['invoiceId' => $data['invoice']]);
        if (!$transaction) {
            $transaction = new Transaction();
            $transaction->setDataCriacao(new \DateTime())
                ->setDataPago(new \DateTime($data['paidAt']))
                ->setTbPessoaId($user->getId())
                ->setValor($data['value'])
                ->setStatus('paid')
                ->setFormaPagamento($data['paymentMethod'])
                ->setPeriodicidade(0)
                ->setGatewayPagamento(1)
                ->setInvoiceId($data['invoice'])
                ->setTbDiretorioId(1)
                ->setTbDiretorioOrigem(102)
                ->setOrigemTransacao(7)
                ->setNumeroRecibo($this->getReceiptDirectory($this->em->getReference(Directory::class, 1), $transaction));
            $this->em->getRepository(Transaction::class)->save($transaction);
            $this->validLastTransaction($user, $transaction, 4);
        }
    }

    public function journey2024(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $header = $request->getHeader('X-Auth')[0];
            if ($header != 'Gsahfetefvmgofgfkg') die('token invalido');
            $date = new \DateTime();
            $data = (array)$request->getParams();
            $fields = [
                'name' => 'Nome',
                'cpf' => 'Cpf',
                'email' => 'E-mail',
                'state' => 'state',
                'city' => 'city',
                'paidAt' => 'Data Pago',
                'value' => 'Valor',
                'paymentMethod' => 'Forma de Pagamento',
                'invoice' => 'InvoiceId'
            ];
            Validator::requireValidator($fields, $data);
            $user = $this->setNewUser($data);
            $this->setNewTransaction($user, $data);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Regitro salvo com sucesso!',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function IDontWantToPay(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            GoogleService::recapchaNovo($data['g-recaptcha-response']);
            $fields = [
                'form' => 'Formulário',
                'name' => 'Nome',
                'email' => 'E-mail',
                'cpf' => 'CPF'
            ];
            if($data['form'] == 1) {
                $fields = array_merge($fields, [
                    'rg' => 'rg', 
                    'companyName' => 'Nome da empresa', 
                    'companyCNPJ' => 'CNPJ da empresa',
                    'uf' => 'Estado',
                    'cityId' => 'Cidade'
                ]);
            } else {
                $fields = array_merge($fields, [
                    'cellPhone' => 'WhatsApp',
                    'syndicateName' => 'Nome do sindicato',
                    'syndicateUfId' => 'Estado do sindicato',
                    'syndicateCityId' => 'Cidade do sindicato'
                ]);
            }
            Validator::requireValidator($fields, $data);
            $check = $this->em->getRepository(IDontWantToPay::class)->findOneBy(['cpf' => $data['cpf'], 'form' => $data['form']]);
            if($check) throw new \Exception("Você já preencheu este formulário");
            $iDontWantToPay = new IDontWantToPay();
            $iDontWantToPay->setCreated(new \Datetime())
                        ->setForm($data['form'])
                        ->setName($data['name'])
                        ->setEmail($data['email'])
                        ->setCPF($data['cpf']);
            $user = $this->em->getRepository(User::class)->findOneBy(['cpf' => $data['cpf']]);
            if($user) $iDontWantToPay->setUser($user);
            $message = "";
            if($data['form'] == 1) {
                $state = $this->em->getRepository(State::class)->findOneBy(['sigla' => $data['uf']]);
                $city = $this->em->getReference(City::class, $data['cityId']);
                $iDontWantToPay->setRg($data['rg'])
                            ->setCompanyName($data['companyName'])
                            ->setCompanyCNPJ($data['companyCNPJ'])
                            ->setState($state)
                            ->setCity($city);
                $cpfEncode = base64_encode($data['cpf']);
                $message = "Informações salvas com sucesso, <a href='https://espaco-novo.novo.org.br/api/nao-quero-pagar/gera-declaracao/{$cpfEncode}/' target='_blank'>Clique aqui</a> para gerar sua declaração!";
            } else {
                $iDontWantToPay->setPhone($data['cellPhone'])
                            ->setSyndicateName($data['syndicateName'])
                            ->setSyndicateState($this->em->getReference(State::class, $data['syndicateUfId']))
                            ->setSyndicateCity($this->em->getReference(City::class, $data['syndicateCityId']));
                $message = 'Salvo com sucesso!';
            }
            $this->em->getRepository(IDontWantToPay::class)->save($iDontWantToPay);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => $message
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),]
            )->withStatus(500);
        }
    }

    public function generateDeclarationPDF(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $cpf = $request->getAttribute('route')->getArgument('cpf');
            $cpf = base64_decode($cpf);
            $check = $this->em->getRepository(IDontWantToPay::class)->findOneBy(['cpf' => $cpf, 'form' => 1]);
            if(!$check) throw new \Exception("Solicitação inválida");
            $state = $this->em->getRepository(State::class)->findOneBy(['sigla' => $check->getState()->getSigla()]);
            $city = $this->em->getReference(City::class, $check->getCity()->getId());
            Pdf::declaration($check, $state, $city);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Gerado com sucesso!'
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),]
            )->withStatus(500);
        }
    }
}