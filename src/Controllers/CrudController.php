<?php

namespace App\Controllers;

use App\Models\Entities\Cabinet;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Symfony\Component\String\Inflector\EnglishInflector;

class CrudController extends Controller
{
    public function index(Request $request, Response $response)
    {
        try {

        $model = $request->getAttribute('route')->getArgument('model');
        $url = $request->getAttribute('route')->getArgument('url');
        $folder = $request->getAttribute('route')->getArgument('folder');

        $this->model($model);
        $this->repository($model);
        $this->controller($model, $folder);
        $this->routes($model, $url);
        $this->request($model);
        $this->view($model, $folder);
        $this->js($model, $folder, $url);

        echo "Arquivos gerados com sucesso!";
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function model($model): void
    {
        $inflector = new EnglishInflector();
        $plural = $inflector->pluralize($model);
        $ucModel = ucfirst($model);

        $content = <<<EOF
<?php

namespace App\Models\Entities;

use App\Helpers\Utils;
use App\Models\Entities\Cabinet;

/**
 * @Entity @Table(name="{$plural[0]}")
 * @ORM @Entity(repositoryClass="App\Models\Repository\\{$ucModel}Repository")
 */
class {$ucModel}
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int \$id = null;

    /**
     * @Column(type="boolean")
     */
    private bool \$active = true;

    /**
     * @Column(type="string")
     */
    private string \$name = '';
    
    /**
     * @Column(type="datetime", nullable=true)
     */
    private ?\DateTime \$deleted_at = null;
    
    public function toArray(): array
    {
        return get_object_vars(\$this);
    }
    
    public function activeStr(): string
    {
        return 1 == \$this->active ? "Ativo" : "Inativo";
    }

    public function changeActive(): {$ucModel}
    {
        \$this->active = !\$this->active;
        return \$this;
    }
}
EOF;

        file_put_contents(dirname(__DIR__) . "/Models/Entities/{$ucModel}.php", $content);

    }

    public function repository($model): void
    {
        $ucModel = ucfirst($model);

        $content = <<<EOF
<?php

namespace App\Models\Repository;

use App\Models\Entities\\{$ucModel};
use App\Models\Entities\Cabinet;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Exception;

class {$ucModel}Repository extends EntityRepository
{
    public function flush({$ucModel} \$entity): {$ucModel}
    {
        \$this->getEntityManager()->persist(\$entity);
        \$this->getEntityManager()->flush();
        return \$entity;
    }
    
    public function save(array \$data): array|Exception
    {
        try {
            \$save = new {$ucModel}();
            if (\$data['id'] > 0) {
                \$save = \$this->_em->getRepository({$ucModel}::class)->find(\$data['id']);
            } 
            \$save->setName(\$data['name'])
                ->setActive(true);
            \$this->flush(\$save);
            return [
                'status' => 'ok',
                'message' => empty(\$data['id']) ? 'Cadastrado realizado com sucesso!' : 'Edição realizada com sucesso!'
            ];
        } catch (\Exception \$e) {
            \$this->_em->rollback();
            return \$e;
        }
    }
    
    public function changeActive(int \$id): array|Exception
    {
        try {
            \$element = \$this->_em->getRepository({$ucModel}::class)->find(\$id);
            \$element->changeActive();
            \$this->flush(\$element);

            return [
                'status' => 'ok',
                'message' => "Status alterado para {\$element->activeStr()}"
            ];
        } catch (\Exception \$e) {
            \$this->_em->rollback();
            return \$e;
        }
    }

    public function destroy(int \$id): array|Exception
    {
        try {
            \$element = \$this->_em->getRepository({$ucModel}::class)->find(\$id);
            \$element->setDeletedAt(new \DateTime())
                ->setActive(false);
            \$this->flush(\$element);
            return [
                'status' => 'ok',
                'message' => "Item excluído com sucesso!",
            ];
        } catch (\Exception \$e) {
            \$this->_em->rollback();
            return \$e;
        }
    }


    private function generateWhere(\$filter, &\$params): string
    {
        \$where = '';
        if (\$filter['name']) {
            \$name = \$filter['name'];
            \$params[':name'] = "%\$name%";
            \$where .= " AND m.name LIKE :name";
        }
        if (\$filter['active'] || \$filter['active'] === '0') {
            \$params[':active'] = \$filter['active'];
            \$where .= " AND m.active = :active";
        }

        return \$where;
    }

	public function list(array \$filter, int \$limit, int \$offset): array
	{
	    \$params = [];
		\$where = \$this->generateWhere(\$filter, \$params);
		return \$this->getEntityManager()->createQuery(
			"SELECT m
				FROM  App\Models\Entities\\{$ucModel} as m
                WHERE m.deleted_at IS NULL {\$where}
                ORDER BY m.id ASC")
			->setMaxResults(\$limit)
			->setFirstResult(\$offset * \$limit)
			->setParameters(\$params)
            ->getResult(Query::HYDRATE_ARRAY);
	}

    public function listTotal(\$filter): int
    {
        \$params = [];
        \$where = \$this->generateWhere(\$filter, \$params);
        return \$this->getEntityManager()->createQuery(
            "SELECT COUNT(m) AS total FROM  App\Models\Entities\\{$ucModel} as m
                WHERE m.deleted_at IS NULL {\$where}
                ")
             ->execute(\$params)[0]['total'];
    }

	public function data(int \$id)
	{
		\$params = [];
		\$params[':id'] = \$id;
		return \$this->getEntityManager()->createQuery(
			"SELECT m
				FROM  App\Models\Entities\\{$ucModel} as m
                WHERE m.id = :id")
			->setParameters(\$params)
            ->getResult(Query::HYDRATE_ARRAY);
	}
}
EOF;


        file_put_contents(dirname(__DIR__) . "/Models/Repository/{$ucModel}Repository.php", $content);
    }

    public function controller($model, $folder): void
    {
        $ucModel = ucfirst($model);

        $content = <<<EOF
<?php

namespace App\Controllers;
use App\Helpers\Session;
use App\Models\Entities\\{$ucModel};
use App\Requests\\{$ucModel}Request;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class {$ucModel}Controller extends Controller
{
    public function index(Request \$request, Response \$response)
    {
        \$user = \$this->getLogged();
        return \$this->renderer->render(\$response, 'default.phtml', ['page' => '{$folder}/{$model}.phtml', 'subMenu' => '{$model}',
            'user' => \$user, 'title' => 'Título']);
    }

    public function list(Request \$request, Response \$response)
    {
        try {
            \$user = \$this->getLogged();
            \$filter = \$request->getQueryParams();
            \$index = \$request->getQueryParam('index') ?: 0;
            \$limit = \$request->getQueryParam('limit') ?: 25;
            \$result = \$this->em->getRepository({$ucModel}::class)->list(\$filter, \$limit, \$index);
            \$total = \$this->em->getRepository({$ucModel}::class)->listTotal(\$filter);
            \$partial = (\$index * \$limit) + sizeof(\$result);
            \$partial = \$partial <= \$total ? \$partial : \$total;
            return \$response->withJson([
                'status' => 'ok',
                'message' => \$result,
                'total' => (int)\$total,
                'partial' => (int)\$partial,
            ], 200)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception \$e) {
            return \$response->withJson([
                'status' => 'error',
                'message' => \$e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function save(Request \$request, Response \$response)
    {
        try {
           \$this->em->beginTransaction();
           \$user = \$this->getLogged();
           \$data = (array)\$request->getParams();

            \$requestValidate = new {$ucModel}Request(\$data);
            \$requestValidate->validate();

            \$result = \$this->em->getRepository({$ucModel}::class)->save(\$data);

            \$this->em->commit();

            return \$this->responseJson(\$result, \$response, 201);
        } catch (\Exception \$e) {
            return \$response->withJson(['status' => 'error',
                'message' => \$e->getMessage(),])->withStatus(500);
        }
    }

    public function changeActive(Request \$request, Response \$response)
    {
        try {
            \$user = \$this->getLogged();
            \$id = \$request->getAttribute('route')->getArgument('id');

            \$result = \$this->em->getRepository({$ucModel}::class)->changeActive(\$id);

            return \$this->responseJson(\$result, \$response);

        } catch (\Exception \$e) {
            return \$response->withJson(['status' => 'error',
                'message' => \$e->getMessage(),])->withStatus(500);
        }
    }

    public function destroy(Request \$request, Response \$response)
    {
        try {
            \$user = \$this->getLogged();
            \$id = \$request->getAttribute('route')->getArgument('id');

            \$result = \$this->em->getRepository({$ucModel}::class)->destroy(\$id);

            return \$this->responseJson(\$result, \$response);
        } catch (\Exception \$e) {
            return \$response->withJson(['status' => 'error',
                'message' => \$e->getMessage(),])->withStatus(500);
        }
    }

    public function show(Request \$request, Response \$response)
    {
        try {
            \$user = \$this->getLogged();
            \$id = \$request->getAttribute('route')->getArgument('id');
            \$data = \$this->em->getRepository({$ucModel}::class)->find(\$id);
            return \$response->withJson([
                'status' => 'ok',
                'message' => \$data->toArray(),
            ], 200)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception \$e) {
            return \$response->withJson(['status' => 'error',
                'message' => \$e->getMessage(),])->withStatus(500);
        }
    }

}

EOF;
        file_put_contents(__DIR__ . "/{$ucModel}Controller.php", $content);
    }

    public function routes($model, $url): void
    {
        $ucModel = ucfirst($model);
        $content = <<<EOF
<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

\$app->group('/{$url}', function () use (\$app) {
    \$app->get('/', fn(Request \$request, Response \$response) => \$this->{$ucModel}Controller->index(\$request, \$response));
    \$app->post('/cadastro/', fn(Request \$request, Response \$response) => \$this->{$ucModel}Controller->save(\$request, \$response));
    \$app->get('/json/', fn(Request \$request, Response \$response) => \$this->{$ucModel}Controller->list(\$request, \$response));
    \$app->get('/{id}/', fn(Request \$request, Response \$response) => \$this->{$ucModel}Controller->show(\$request, \$response));
    \$app->put('/novo-status/{id}/', fn(Request \$request, Response \$response) => \$this->{$ucModel}Controller->changeActive(\$request, \$response));
    \$app->delete('/{id}/', fn(Request \$request, Response \$response) => \$this->{$ucModel}Controller->destroy(\$request, \$response));

});
EOF;

        $src = dirname(__DIR__);
        file_put_contents(dirname($src) . "/routes/system/{$url}.php", $content);

    }

    public function request($model): void
    {
        $ucModel = ucfirst($model);

        $content = <<<EOF
<?php

namespace App\Requests;

class {$ucModel}Request extends FormRequest
{
    public function __construct(\$data)
    {
        \$this->data = \$data;
        \$this->rules = \$this->rules();
        \$this->attributes = \$this->attributes();
    }

    public function rules(): array
    {
        if (empty(\$this->data['id'])) {
            \$rules = [
                'email' => ['required', 'email'],
                'name' => ['required'],
                'type' => ['required'],
                'password' => ['required', 'min' => [8], 'different'],
            ];
        } else {
            \$rules = [
                'email' => ['required', 'email'],
                'name' => ['required'],
                'type' => ['required'],
            ];
        }
        return \$rules;
    }

    public function attributes(): array
    {
        return [
            'name' => 'Nome',
            'email' => 'E-mail',
            'password' => 'Senha',
            'type' => 'Categoria'
        ];
    }
}
EOF;

        $src = dirname(__DIR__);
        file_put_contents($src . "/Requests/{$ucModel}Request.php", $content);

    }

    public function view($model, $folder): void
    {
        $ucModel = ucfirst($model);

        $content = <<<EOF
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header border-bottom">
                    <div class="row">
                        <div class="col-md-8">
                            <h5 class="title">{$ucModel}</h5>
                        </div>
                        <div class="col-md-4">
                            <button class="btn btn-primary pull-right" onclick="openModal(0)"><i
                                    class="fa fa-plus"></i> {$ucModel}
                            </button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form id="filter">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="filterName">Nome</label>
                                    <input class="form-control" id="filterName" name="name" type="text"
                                           placeholder="Nome">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="filterEmail">E-mail</label>
                                    <input class="form-control" id="filterEmail" name="email" type="email"
                                           placeholder="E-mail">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="filterActive">Status</label>
                                    <select class="form-control form-select" id="filterActive" name="active">
                                        <option value="" selected>Todos</option>
                                        <option value="1">Ativo</option>
                                        <option value="0">Inativo</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 text-end">
                                <div class="form-group">
                                    <button class="btn btn-danger" type="reset" onclick="resetTable()">Limpar</button>
                                    <button class="btn btn-primary" type="submit">Filtrar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="row g-3 align-items-center m-b-30">
                        <div class="col-auto">
                            <span>Exibir </span>
                        </div>
                        <div class="col-auto">
                            <select class="form-control form-select" id="limit"
                                    onchange="getLimit(this.value, '{$ucModel}')">
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                        </div>
                        <div class="col-auto">
                            <span> registros</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <p>Exibindo de <span id="start{$ucModel}"></span> a <span id="end{$ucModel}"></span> de <span
                                    id="total{$ucModel}"></span> registros</p>
                        </div>
                    </div>
                    <div class="row table-responsive-lg">
                        <div class="col-12">
                            <table id="table{$ucModel}" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th class="text-center">ID</th>
                                    <th class="text-center">Nome</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Ações</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col text-center">
                            <div class="spinner-border loader{$ucModel}" role="status">
                                <span class="sr-only mx-auto">Loading...</span>
                            </div>
                        </div>
                    </div>
                    <div class="row text-end">
                        <nav aria-label="...">
                            <ul class="pagination justify-content-end pagination-primary"
                                id="pagination{$ucModel}" style="top: auto">

                            </ul>
                        </nav>
                    </div>

                </div>
            </div>
        </div>
    </div>

<!-- Modal Cadastro-->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="subscriptionModal"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Cadastro</h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="form" novalidate class="was-validated">
                <div class="modal-body custom-scrollbar">
                    <div class="row mt-3">
                        <div class="col text-center">
                            <div class="spinner-border loaderTop" style="width: 5rem; height: 5rem;" role="status">
                                <span class="sr-only mx-auto">Loading...</span>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="id" id="id">
                    <div class="form-group">
                        <div class="row">
                            <div class="col mb-3">
                                <label for="name">Nome <span class="text-danger">*</span></label>
                                <input id="name" name="name" class="form-control" type="text" placeholder="Nome"
                                       required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancelar</button>
                    <button type="submit" id="save" class="btn btn-success">Confirmar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="<?= BASEURL ?>assets/functions/changeActive.js"></script>
<script src="<?= BASEURL ?>assets/functions/generateTable.js"></script>
<script src="<?= BASEURL ?>assets/functions/pagination.js"></script>
<script src="<?= BASEURL ?>assets/functions/form-validate.js"></script>
<script src="<?= BASEURL ?>assets/functions/{$folder}/{$model}.js"></script>
<script src="<?= BASEURL ?>assets/functions/changeStatus.js"></script>
<script src="<?= BASEURL ?>assets/functions/delElement.js"></script>
EOF;
        mkdir(dirname(__DIR__) . "/views/{$folder}");
        file_put_contents(dirname(__DIR__) . "/views/{$folder}/{$model}.phtml", $content);

    }

    public function js($model, $folder, $url):void
    {
        $ucModel = ucfirst($model);

        $content = <<<EOF
const filter = document.getElementById('filter');
filter.addEventListener('submit', e => {
    e.preventDefault();
    let formData = formDataToJson('filter');
    setStorage('{$ucModel}', {
        'url': '{$url}/json',
        'columns': 7,
        'variables': {
            'index': 0,
            'name': formData.name,
            'total': 1,
            'limit': $('#limit').val()
        }
    })
});

const formAdd = document.getElementById('form');
formAdd.addEventListener('submit', e => {
    e.preventDefault();
    showLoading();
    if (!ValidateForm('form')) {
        closeLoading();
        return;
    }
    let method = 'POST';
    let formData = formDataToJson('form');
    fetch(baseUrl + "{$url}/cadastro", {
        method: method,
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData)
    }).then(response => {
        closeLoading();
        response.json().then(json => {
            if (response.status === 201) {
                showNotify('success', json.message, 1500);
                formAdd.reset();
                resetTable();
                $('#modal').modal('hide');
            } else {
                showNotify('danger', json.message, 1500);
            }
        });
    });
});

function resetTable() {
    filter.reset();
    setStorageIndex();
}

function generateLines(item) {
    let actions = `<i class="fa fa-pencil text-info px-1" title="Editar" onclick="openModal(\${item.id})"></i>`;
    actions = `<td class="text-center">\${actions} </td>`;


    return `<tr class="middle" id="line\${item.id}">
                    <td class="text-center">\${item.id}</td>                    
                    <td class="text-center">\${item.name}</td>                    
                    <td class="text-center">\${buttonActive(item.active, '{$url}', item.id, '{$ucModel}', 'put')}</td>
                    \${actions}
                </tr>`;
}

function openModal(id) {
    document.getElementById('form').reset();
    document.getElementById('id').value = id;
    $('#modal').modal('show');
    if (id > 0) {
        showLoading();
        fetch(baseUrl + `{$url}/\${id}`, {
            method: "GET",
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then(response => {
            response.json().then(json => {
                json = json.message;
                closeLoading();
                setValuesModal(json);
            });
        });
    }
}

$(document).ready(function () {
    verifySession('{$ucModel}');
});

function setStorageIndex() {
    setStorage('{$ucModel}', {
        'url': '{$url}/json',
        'columns': 7,
        'variables': {
            'index': 0,
            'name': '',
            'total': 1,
            'limit': $('#limit').val()
        }
    });
};

EOF;

        $src = dirname(__DIR__);
        mkdir(dirname($src) . "/assets/functions/{$folder}");
        file_put_contents(dirname($src) . "/assets/functions/{$folder}/{$model}.js", $content);

    }
}
