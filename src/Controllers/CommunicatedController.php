<?php

namespace App\Controllers;
use App\Helpers\Session;
use App\Models\Entities\Communicated;
use App\Models\Entities\CepMembers;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

class CommunicatedController extends Controller
{
    public function index(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $type = $request->getAttribute('route')->getArgument('type');
        if ($type){
            if(in_array(($type), array('atas', 'resolucoes', 'restrito'))) $user->isAffiliated(true);
            switch ($type) {
                case "atas":
                    $type = 'record';
                    $title = 'Atas';
                    $section = 'record';
                    break;
                case "resolucoes":
                    $type = 'resolution';
                    $title = 'Resoluções';
                    $section = 'resolution';
                    break;
                case "restrito":
                    $type = 'restricted';
                    $title = 'Restrito a filiados';
                    $section = 'restricted';
                    break;
                case "diretrizes":
                    $type = 'guideline';
                    $title = 'Diretrizes';
                    $section = 'guideline';
                    break;
            }
            $contents = $this->em->getRepository(Communicated::class)->findBy(['tipo' => $type, 'ativo' => 1],['data' => 'desc']);
        }else{
            $contents = $this->em->getRepository(Communicated::class)->findBy(['tipo' => 'communicated', 'ativo' => 1],['data' => 'desc']);
            $title = 'Comunicados';
            $section = 'communicated';
        }
        return $this->renderer->render($response, 'default.phtml', ['page' => 'communicated/index.phtml', 'contents' => $contents, 'section' => $section,
            'user' => $user, 'subMenu' => 'myContent', 'title' => $title]);
    }

    public function membersCep(Request $request, Response $response)
    {
        $user = $this->getLogged([7, 8]);
        $members = $this->em->getRepository(CepMembers::class)->findBy(['active' => 1]);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'communicated/cep/members-cep.phtml',  'subMenu' => 'myCep',
            'section' => 'membersCep', 'user' => $user, 'title' => 'Comissão de Ética Partidária', 'members' => $members]);
    }

    public function consultationsCep(Request $request, Response $response)
    {
        $user = $this->getLogged([7, 8]);
        $consultations = $this->em->getRepository(Communicated::class)->findBy(['tipo' => 'consultation', 'ativo' => 1],['data' => 'desc']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'communicated/cep/consultations-cep.phtml', 'consultations' => $consultations, 'subMenu' => 'myCep',
            'section' => 'consultationsCep', 'user' => $user, 'title' => 'Comissão de Ética Partidária']);
    }

    public function decisionsCep(Request $request, Response $response)
    {
        $user = $this->getLogged([7, 8]);
        $decisions = $this->em->getRepository(Communicated::class)->findBy(['tipo' => 'decision', 'ativo' => 1],['processo' => 'desc']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'communicated/cep/decisions-cep.phtml', 'decisions' => $decisions,'subMenu' => 'myCep',
            'section' => 'decisionsCep', 'user' => $user, 'title' => 'Comissão de Ética Partidária']);
    }

    public function questionsCep(Request $request, Response $response)
    {
        $user = $this->getLogged([7, 8]);
        $decisions = $this->em->getRepository(Communicated::class)->findBy(['comite' => 1]);
        $consultations = $this->em->getRepository(Communicated::class)->findBy(['comiteConsultas' => 1]);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'communicated/cep/questions-cep.phtml', 'consultations' => $consultations, 'decisions' => $decisions,'subMenu' => 'myCep',
            'section' => 'questionsCep', 'user' => $user, 'title' => 'Comissão de Ética Partidária']);
    }

    public function flowchart(Request $request, Response $response)
    {
        $user = $this->getLogged();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'communicated/cep/flowchart.phtml', 'section' => 'flowchart', 'subMenu' => 'myCep', 'user' => $user, 'title' => 'Fluxograma da CEP']);
    }

    public function statute(Request $request, Response $response)
    {
        $user = $this->getLogged();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'communicated/statute.phtml', 'section' => 'statute', 'subMenu' => 'myContent', 'user' => $user, 'title' => 'Estatuto do NOVO']);
    }

    public function conduct(Request $request, Response $response)
    {
        $user = $this->getLogged();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'communicated/conduct.phtml', 'section' => 'conduct', 'subMenu' => 'myContent', 'user' => $user, 'title' => 'Código de Conduta do NOVO']);
    }

    public function view(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $id = $request->getAttribute('route')->getArgument('file');
        $file = $this->em->getRepository(Communicated::class)->findOneBy(['id' => $id]);
        if(in_array(($file->getType()), array('record', 'resolution', 'restricted'))) $user->isAffiliated(true);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'communicated/view.phtml', 'section' => 'consultationsCep', 'subMenu' => 'myCep', 'user' => $user, 'title' => $file->getTitle(), 'file' => $file->getArchive()]);
    }

    public function localDocuments(Request $request, Response $response)
    {
        $user = $this->getLogged([7, 8]);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'communicated/localDocuments.phtml', 'section' => 'localDocuments', 'subMenu' => 'myContent', 'user' => $user, 'title' => 'Documentos Locais']);
    }
}