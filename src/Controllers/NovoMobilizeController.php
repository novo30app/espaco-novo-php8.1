<?php

namespace App\Controllers;

use App\Helpers\Messages;
use App\Helpers\Session;
use App\Models\Commons\Entities\Benefit;
use App\Models\Commons\Entities\BenefitPlan;
use App\Models\Commons\Entities\Mission;
use App\Models\Commons\Entities\UserPoint;
use App\Models\Commons\Entities\UserPointAddress;
use App\Models\Entities\Address;
use App\Models\Entities\City;
use App\Models\Entities\NovoMobilize;
use App\Models\Entities\State;
use App\Models\Entities\User;
use App\Requests\MissionRequest;
use App\Requests\NovoMobilizeRequest;
use App\Requests\UserPointAddressRequest;
use App\Services\Email;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class NovoMobilizeController extends Controller
{
    public function index(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $user->liberarMobiliza(true);
//        $missions = $this->em->getRepository(Mission::class)->findBy(['active' => 1, 'deleted_at' => null]);
        $benefits = $this->em->getRepository(Benefit::class)->listUser($user, 3);
        $missions = $this->em->getRepository(Mission::class)->listAvailable($user);
        $missionWeek = $this->em->getRepository(Mission::class)->listAvailableWeek($user);
        $points = $this->em->getRepository(UserPoint::class)->pointsByUser($user);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'mobilize/index.phtml', 'section' => 'mobilize',
            'user' => $user, 'title' => 'Novo Mobiliza', 'missions' => $missions, 'benefits' => $benefits, 'points' => $points[0]['points'],
            'missionWeek' => $missionWeek[0]]);
    }

    public function missions(Request $request, Response $response)
    {
        $user = $this->getLogged();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'mobilize/mission.phtml', 'section' => 'mobilize',
            'user' => $user, 'title' => 'Missões']);
    }

    public function benefits(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $user->liberarMobiliza(true);
//        $benefits = $this->em->getRepository(Benefit::class)->findBy(['active' => 1, 'deleted_at' => null], ['id' => 'desc']);
        $benefits = $this->em->getRepository(Benefit::class)->listUser($user);
        $points = $this->em->getRepository(UserPoint::class)->pointsByUser($user);
        $acquireds = $this->em->getRepository(UserPoint::class)->findBy(['user' => $user, 'mission' => null]);
        $addressArr = $this->getAddressArr($user);

        return $this->renderer->render($response, 'default.phtml', ['page' => 'mobilize/benefit.phtml', 'section' => 'mobilize',
            'user' => $user, 'title' => 'Benefícios', 'benefits' => $benefits, 'points' => $points[0]['points'],
            'address' => $addressArr, 'acquireds' => $acquireds]);
    }

    public function requestBenefit(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $user = $this->getLogged();
            $data = (array)$request->getParams();

            if ($data['addressType'] == 1) {
                $arr = $this->getAddressArr($user);
                $data = array_merge($data, $arr);
            } else {
                $requestValidate = new UserPointAddressRequest($data);
                $requestValidate->validate();
            }

            $benefitPlan = $this->em->getRepository(BenefitPlan::class)->find($data['benefit']);

            if ($benefitPlan->getAvailable() <= 0) throw new \Exception('Todos os volumes destes item já foram distruibuídos!');

            $benefitPlan->setAvailable($benefitPlan->getAvailable() - 1);
            $this->em->persist($benefitPlan);
            $this->em->flush();

            $benefit = $benefitPlan->getBenefit();
            $benefit->setPoints($benefitPlan->getPoints()); //aqui alteramos para pegar os pontos pelo beneficio plano
            $result = $this->em->getRepository(UserPoint::class)->save(user: $user, benefit: $benefit);

            $data['userPoint'] = $result['id'];
            // Se for entrega externa, salva o endereço
            if ($data['type_delivery'] == 3) $this->em->getRepository(UserPointAddress::class)->save($data);

            Messages::requestBenefitParam($user, $benefit);
            $this->em->commit();

            return $this->responseJson($result, $response, 201);
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }


    private function getAddressArr(User $user): array|\Exception
    {
        $address = $this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()]);
        if (!$address) throw new \Exception('Endereço não encontrado!');

        $uf = $this->em->getRepository(State::class)->find($address->getStateId());
        $city = $this->em->getRepository(City::class)->find($address->getTbCidadeId());

        return [
            'zipCode' => $address->getCep(),
            'uf' => $uf->getSigla(),
            'city' => $city->getCidade(),
            'neighborhood' => $address->getBairro(),
            'address' => $address->getEndereco(),
            'complement' => $address->getComplemento(),
            'number' => $address->getNumero(),
        ];
    }

    public function getPoints(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $points = $this->em->getRepository(UserPoint::class)->pointsByUser($user);
            $missions = $this->em->getRepository(Mission::class)->listAvailable($user, true);
            $missionWeek = $this->em->getRepository(Mission::class)->listAvailableWeek($user, true);

            if (date('Y-m-d') <= '2024-06-15' && !$missions) {
                $countMission = $this->em->getRepository(UserPoint::class)->totalByMission(4);
                $countMission2 = $this->em->getRepository(UserPoint::class)->findOneBy(['mission' => 4, 'user' => $user]);
                if ($countMission < 15 && !$countMission2) {
                    $mission = $this->em->getRepository(Mission::class)->find(4);
                    $missions[] = ['id' => $mission->getId(), 'active' => true, 'link' => '',
                        'deleted_at' => null, 'type' => true, 'expiration' => null, 'points' => $mission->getPoints(),
                        'validate_type' => 1, 'title' => $mission->getTitle(), 'description' => $mission->getDescription(),
                    ];
                }
            }

            return $response->withJson([
                'status' => 'ok',
                'points' => $points[0]['points'],
                'missions' => $missions,
                'missionWeek' => $missionWeek[0],
            ])
                ->withStatus(200)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }
}
