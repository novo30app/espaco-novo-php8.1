<?php


namespace App\Controllers;

use App\Helpers\Utils;
use App\Helpers\Validator;
use App\Helpers\Session;
use App\Models\Entities\AccessLog;
use App\Models\Entities\ChangeEmail;
use App\Models\Entities\RecoverPassword;
use App\Models\Entities\User;
use App\Services\Auth;
use App\Services\Email;
use App\Services\Jornada2024;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class LoginController extends Controller
{

    public function login(Request $request, Response $response, bool $libertas = false)
    {
        if (Session::get('user')) {
            $this->redirect();
        }
        if ($libertas && $_COOKIE['userLoginLibertas']) {
            $tokenDecode = explode(':', $_COOKIE['userLoginLibertas']);
            $user = $this->em->getRepository(User::class)->findOneBy(['id' => base64_decode($tokenDecode[0]),
                'token2' => base64_decode($tokenDecode[1])]);
            if ($user) {
                $militar = Jornada2024::isMilitary($user->getCpf() ?? '123');
                Session::set('user-token', $user->getToken2());
                Session::set('militar', $militar);
                Session::set('user', $user);
                $acessData = Utils::getAcessData();
                $accessLog = new AccessLog();
                $accessLog->setUser($user);
                $accessLog->setIp($acessData['ip'] ?? 0);
                $accessLog->setDevice("{$acessData['name']} {$acessData['version']}");
                $accessLog->setSo($acessData['platform']);
                $accessLog = $this->em->getRepository(AccessLog::class)->save($accessLog);
                Session::set('accessLog', $accessLog);
                Session::set('libertas', 1);
                $this->redirect();
            }
        }
        $view = $libertas ? 'login-libertas.phtml' : 'login.phtml';
        return $this->renderer->render($response, "login/{$view}", ['libertas' => $libertas]);
    }

    public function recover(Request $request, Response $response, bool $libertas = false)
    {
        return $this->renderer->render($response, 'login/recover.phtml', ['libertas' => $libertas]);
    }

    public function autentication(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $fields = [
                'emailCpf' => 'Email',
                'password' => 'Senha',
            ];
            Validator::requireValidator($fields, $data);
            $usersRepository = $this->em->getRepository(User::class);

            $user = $usersRepository->login($data['emailCpf'], $data['password']);

            if (ENV != 'prod') {
                //$user = $usersRepository->loginAuth($data['emailCpf']); // aqui buscamos o usuario por email ou cpf
                //Auth::login($user->getEmail(), $data['password']); // aqui logamos ele pelo email
            }

//            Auth::charge($user->getName(), $user->getEmail(), $user->getPassword());


            if (!$user->getEmailConfirmado()) {
                $this->confirmEmail($user);
                throw new \Exception('Para acessar o espaço NOVO você precisa confirmar seu e-mail, acabamos de reenviar um novo e-mail para confirmação!');
            }
            if ($user->getBloqueado() == 1 || $user->getFiliado() == 11) {
                throw new \Exception('Atenção, não é possível acessar seu Espaço NOVO. Dúvidas, entre em contato com <a href="mailto:filiacao@novo.org.br">filiacao@novo.org.br</a>.<br>');
            }

            $militar = Jornada2024::isMilitary($user->getCpf() ?? '123');
            $user->setToken2(time() . $user->getId() . Utils::generateToken());
            Session::set('user-token', $user->getToken2());
            Session::set('militar', $militar);
            Session::set('user', $user);

            if ($data['remind_me']) {
                setcookie('userLoginLibertas', $user->getToken2(), strtotime("+1 month"), '/', '', false, true);
            }

//            dd($_COOKIE);

            $acessData = Utils::getAcessData();
            $accessLog = new AccessLog();
            $accessLog->setUser($user);
            $accessLog->setIp($acessData['ip'] ?? 0);
            $accessLog->setDevice("{$acessData['name']} {$acessData['version']}");
            $accessLog->setSo($acessData['platform']);
            $accessLog = $this->em->getRepository(AccessLog::class)->save($accessLog);
            Session::set('accessLog', $accessLog);
            Session::set('libertas', $data['libertas']);
            $redirect = Session::get('redirect');
            if ($redirect) {
                Session::forgot('redirect');
                $redirect = substr($redirect, 0, 1) == '/' ? substr($redirect, 1) : $redirect;
                $this->redirect($redirect);
                exit;
            }
            $this->redirect();
        } catch (\Exception $e) {
            Session::set('errorMsg', $e->getMessage());
            $url = 'login';
            if ($data['libertas']) $url = 'instituto-libertas/login';
            $this->redirect($url);
            exit;
        }

    }

    public function logout(Request $request, Response $response)
    {
        Session::forgot('user');
        Session::forgot('user-token');
        Session::forgot('seletivo2022');
        $libertas = Session::get('libertas');
        Session::forgot('libertas');
        setcookie('userLoginLibertas', '', -1);
        unset($_COOKIE['userLoginLibertas']);
        $url = 'login';
        if ($libertas) $url = 'instituto-libertas/login';
        $this->redirect($url);
    }

    public function saveRecover(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $fields = [
                'emailCpf' => 'E-mail ou CPF',
            ];
            Validator::requireValidator($fields, $data);
            $usersRepository = $this->em->getRepository(User::class);
            $user = $usersRepository->getByEmailOrCpf($data['emailCpf']);
            if (!$user->getEmail()) {
                if ($data['emailRegister']) {
                    $user->setEmail($data['emailRegister']);
                    $this->em->getRepository(User::class)->save($user);
                } else {
                    return $response->withJson([
                        'status' => 'error-email',
                        'message' => 'Parece que você possui um cadastro mas seu e-mail ainda não está registrado, 
                            preencha os campos abaixo para que as informações sejam registradas e você consiga receba 
                            o e-mail para redefinir sua senha!',
                    ])->withStatus(500);
                }
            }
            $recoverPassword = new RecoverPassword();
            $recoverPassword->setCreated(new \DateTime())
                ->setUser($user)
                ->setToken(Utils::generateToken())
                ->setUsed(false);
            $this->em->getRepository(RecoverPassword::class)->save($recoverPassword);
            $msg = "Olá {$user->getFirstName()},<br>
                    Você solicitou uma redefinição de senha para seu Espaço NOVO,<br>
                    Para redefinir sua senha <a href='{$this->baseUrl}recuperar-senha/{$recoverPassword->getToken()}' target='_blank'>Clique aqui!</a>.<br><br>
                    Se não foi você quem solicitou, favor entrar em contato com <a href='mailto:filiacao@novo.org.br'>filiacao@novo.org.br</a>";
            Email::send($user->getEmail(), $user->getName(), 'Recuperação de Senha - Espaço NOVO', $msg);
            return $response->withJson([
                'status' => 'ok',
                'message' => "Foi enviado um e-mail para {$user->getEmailCustom()} redefinir de senha.",
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function changePassword(Request $request, Response $response)
    {
        $id = $request->getAttribute('route')->getArgument('id');
        $msg = '';
        $valid = true;
        $recover = $this->em->getRepository(RecoverPassword::class)->findOneBy(['token' => $id]);
        if ($recover) {
            if ($recover->isUsed() == 1) {
                $msg = "Cadastro já ativado, você pode realizar o login!";
                $valid = false;
            }
        } else {
            $msg = "Link Inválido!";
            $valid = false;
        }
        return $this->renderer->render($response, 'login/change-password.phtml', ['section' => 'admin', 'id' => $id, 'valid' => $valid, 'msg' => $msg]);
    }

    public function savePassword(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $recover = $this->em->getRepository(RecoverPassword::class)->findOneBy(['token' => $data['id'], 'used' => 0]);
            if (!$recover) {
                throw new \Exception('Token Inválido');
            }
            $fields = [
                'password2' => 'Confirme a Nova senha',
                'password' => 'Nova senha',
            ];

            Validator::requireValidator($fields, $data);
            Validator::validatePassword($data);
            $recover->setUsed(1);
            $this->em->getRepository(RecoverPassword::class)->save($recover);
            $user = $recover->getUser();
            $user->setPassword(md5($data['password']));
            $this->em->getRepository(User::class)->save($user);
            Auth::charge($user->getName(), $user->getEmail(), $user->getPassword());
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Senha alterada com sucesso.',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

}
