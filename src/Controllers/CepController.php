<?php

namespace App\Controllers;

use App\Models\Entities\Phone;
use App\Models\Entities\State;
use App\Services\CEP;
use App\Helpers\Session;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

class CepController extends Controller
{
    public function complaints(Request $request, Response $response)
    {
        $user = $this->getLogged();
        if (!$user->isAffiliated()) $this->redirectByPermissions();
        $process = CEP::getProcessByUser($user->getEmail());
        return $this->renderer->render($response, 'default.phtml', ['page' => 'cep/index.phtml', 'section' => 'complaints', 
            'user' => $user, 'process' => $process]);
    }

    public function register(Request $request, Response $response)
    {
        $user = $this->getLogged();
        if (!$user->isAffiliated()) $this->redirectByPermissions();
        $states = $this->em->getRepository(State::class)->findAll();
        $phone = $this->em->getRepository(Phone::class)->findOneBy(['tbPessoaId' => $user->getId(), 'tbTipoTelefoneId' => 3]);
        if($phone) $phone = $phone->getTelefone();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'cep/register.phtml', 'section' => 'complaints', 
            'user' => $user, 'phone' => $phone, 'states' => $states]);
    }

    public function view(Request $request, Response $response)
    {
        $user = $this->getLogged();
        if (!$user->isAffiliated()) $this->redirectByPermissions();
        $id = $request->getAttribute('route')->getArgument('id');
        $details = CEP::getProcessDetails($id, $user->getEmail());
        if ($details['status'] == 'error') {
            Session::set('errorMsg', $details['message']);
            $this->redirect();
        }
        $userType = $details['userType'];
        $process = $details['process'][0];
        $denounced = $details['denounced'];
        $squeakers = $details['squeakers'];
        $files = $details['files'];
        $preliminaryAnalysis = $details['preliminaryAnalysis'];
        $defense = $details['defense'];
        $instruction = $details['instruction'];
        $judgment = $details['judgment'];
        $intimation = $details['intimation'];
        $resource = $details['resource'];
        $judgmentResource = $details['judgmentResource'];
        $intimationResource = $details['intimationResource'];
        return $this->renderer->render($response, 'default.phtml', ['page' => 'cep/view.phtml', 'section' => 'complaints', 'user' => $user, 'process' => $process, 
            'denounced' => $denounced, 'squeakers' => $squeakers, 'files' => $files, 'preliminaryAnalysis' => $preliminaryAnalysis, 'defense' => $defense,
            'instruction' => $instruction, 'judgment' => $judgment, 'intimation' => $intimation, 'resource' => $resource, 'judgmentResource' => $judgmentResource,
            'intimationResource' => $intimationResource, 'userType' => $userType
        ]);
    }

    public function document(Request $request, Response $response)
    {
        $user = $this->getLogged();
        if (!$user->isAffiliated()) $this->redirectByPermissions();
        $hash = $request->getAttribute('route')->getArgument('hash');
        $document = CEP::getDocument($hash);
        $details = $document['message']['details'];
        $signatures = $document['message']['signatures'];
        if ($details['status'] == 'error') {
            Session::set('errorMsg', $details['message']);
            $this->redirect();
        }
        $mpdf = new \Mpdf\Mpdf(['margin-header' => 10, 'margin-right' => 10, 'margin-left' => 10, 'margin-footer' => 10]);
        $mpdf->WriteHTML("<h2>Processo: {$details['process']}</h2><hr>");       
        $mpdf->WriteHTML("<ul>
                            <li>Data: {$details['created']}</li>
                            <li>Votante: {$details['user']}</li>
                            <li>Voto: {$details['situation']}</li>
                        </ul><hr>");
        $mpdf->WriteHTML("<h3>Descrição:</h3> {$details['decision']}");
        $text = "<h3>Assinaturas:</h3>";
        foreach($signatures as $signature) {
            $text .= "<ul>
                        <li>Número do Documento: {$signature['signatureId']}</li>
                        <li>Hash da assinatura: {$signature['signatureHash']}</li>                        
                        <li>Assinante: {$signature['signatureName']}</li>
                        <li>Documento assinado em: {$signature['signatureDate']}.</li>
                        <li>E-mail: {$signature['signatureEmail']}</li>
                    </ul><hr>";
        }
        $mpdf->AddPage();
        $mpdf->WriteHTML($text);
        $mpdf->Output("Voto Processo {}--{}.pdf", \Mpdf\Output\Destination::INLINE);
        return $response->withHeader('Content-Type', 'application/pdf');
    }
}