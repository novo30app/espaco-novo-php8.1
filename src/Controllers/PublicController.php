<?php

namespace App\Controllers;

use App\Helpers\Validator;
use App\Helpers\Messages;
use App\Helpers\Utils;
use App\Models\Commons\Entities\Mission;
use App\Models\Commons\Entities\UserPoint;
use App\Models\Entities\Address;
use App\Models\Entities\City;
use App\Models\Entities\Form;
use App\Models\Entities\PersonCreditCard;
use App\Models\Entities\State;
use App\Models\Entities\Candidate;
use App\Models\Entities\Transaction;
use App\Models\Entities\PersonSignature;
use App\Models\Entities\User;
use App\Models\Entities\DirectoryGatway;
use App\Models\Entities\Directory;
use App\Models\Entities\PersonRede;
use App\Models\Entities\Schooling;
use App\Models\Entities\Gender;
use App\Models\Entities\ResearchNovoUser;
use App\Models\Entities\ResearchNovoUnaffiliated;
use App\Models\Entities\ResearchNovoAffiliated;
use App\Models\Entities\ResearchNovoInformative;
use App\Services\GoogleService;
use App\Services\IuguService;
use App\Services\MaxiPago;
use App\Services\Email;
use App\Services\Movidesk;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class PublicController extends Controller
{

    public function municipalStrategicPlanning2022(Request $request, Response $response)
    {
        $states = $this->em->getRepository(State::class)->findBy([], ['estado' => 'asc']);
        return $this->renderer->render($response, 'public/default.phtml', ['page' => 'forms/01.phtml', 'states' => $states]);
    }

    public function saveMunicipalStrategicPlanning2022(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $fields = [
                'state' => 'Estado',
                'city' => 'Cidade',
                'cityParticipation' => 'Sua cidade já participou de alguma campanha do NOVO?',
                'volunteer' => 'Voluntários',
                'electedCandidate' => 'Sua cidade já possui algum mandatário eleito?',
                'strategy' => 'Estratégia de atração',
                'votes' => 'Votos em 2022',
                'numberOfAffiliates' => 'Filiados em 2022',
                'events' => 'Eventos a realizar',
                'disclosureEvents' => 'Eventos para divulgação',
                'followersNow' => 'Seguidores atualmente',
                'followers2022' => 'Seguidores em 2022',
                'socialMediaObjective' => 'Objetivos de alcance das Redes Sociais',
                'otherDisclosures' => 'Outros canais utilizados',
                'fundraisingActions' => 'Ações para arrecadas fundos',
                'fundraisingValue' => 'Valor arrecadado em 2021',
                'theme' => 'Temas para a campanha',
                'danger' => 'Riscos da campanha',
                'divisionOfTasks' => 'Divisão de tarefas',
                'references' => 'Referências utilizadas',
                'newIdea' => 'Ideias adicionais'
            ];
            Validator::requireValidator($fields, $data);
            $form = new Form();
            $form->setEmail($data['email'])
                ->setCityParticipation($data['cityParticipation'])
                ->setVolunteer($data['volunteer'])
                ->setElectedCandidate($data['electedCandidate'])
                ->setStrategy($data['strategy'])
                ->setVotes($data['votes'])
                ->setEvents($data['events'])
                ->setDisclosureEvents($data['disclosureEvents'])
                ->setFollowersNow($data['followersNow'])
                ->setSocialMedia(implode(', ', array_filter($data['socialMedia'])))
                ->setFollowers2022($data['followers2022'])
                ->setSocialMediaObjective($data['socialMediaObjective'])
                ->setOtherDisclosures($data['otherDisclosures'])
                ->setFundraisingActions($data['fundraisingActions'])
                ->setFundraisingValue($data['fundraisingValue'])
                ->setTheme($data['theme'])
                ->setDanger($data['danger'])
                ->setNumberOfAffiliates($data['numberOfAffiliates'])
                ->setDivisionOfTasks($data['divisionOfTasks'])
                ->setReference($data['references'])
                ->setState($this->em->getReference(State::class, $data['state']))
                ->setCity($this->em->getReference(City::class, $data['city']))
                ->setNewIdea($data['newIdea']);
            $this->em->getRepository(Form::class)->save($form);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Formulário preenchido com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function totvsRevenue(Request $request, Response $response)
    {
        $data = (array)$request->getParams();
        $validUser = (is_null($data['user']) || $data['user'] != 'user_totvs' ? false : true);
        $validPass = (is_null($data['password']) || $data['password'] != 'HF8nPyFm' ? false : true);
        if ($validUser == false || $validPass == false) throw new \Exception("Unauthorized");
        $revenue = $this->em->getRepository(Transaction::class)->totvsRevenue();
        $revenue = json_encode($revenue);
        return $revenue;
    }


    public function totvsTransactionError(Request $request, Response $response)
    {
        $data = (array)$request->getParams();
        $validUser = (is_null($data['user']) || $data['user'] != 'user_totvs' ? false : true);
        $validPass = (is_null($data['password']) || $data['password'] != 'HF8nPyFm' ? false : true);
        if ($validUser == false || $validPass == false) throw new \Exception("Unauthorized");
        $id_transacao = $data['id_transacao'];
        $erro = $data['erro'];
        $this->em->getRepository(Transaction::class)->registerTotvsTransactionError($id_transacao, $erro);
    }

    public function economizometer(Request $request, Response $response)
    {
        $size = $request->getAttribute('route')->getArgument('size');
        $fontSize = $size . 'px';
        $valueSaved = $this->em->getRepository(Candidate::class)->valueSaved();
        $valueSaved = str_replace(',', '.', str_replace('.', '', $valueSaved[0]['valueSaved']));
        return $this->renderer->render($response, 'public/default.phtml', ['page' => 'undersigned/economizometer.phtml', 'title' => 'Economizometro ',
            'valueSaved' => floatVal($valueSaved), 'fontSize' => $fontSize]);
    }

    private string $token = "wze9uLKiGc4fba54743bc5f8789c";

    public function increaseContribution(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $user = $this->em->getRepository(User::class)->findOneBy(['email' => base64_decode($data['email'])]);
            $subscription = $this->em->getRepository(PersonSignature::class)->findOneBy(['tbPessoaId' => $user->getId(), 'origin' => 2]);
            if (!$user) throw new \Exception("Solicitação Inválida");
            $this->setHistoricalIncrease($user->getId(), '4');
            $firstName = explode(' ', $user->getName());
            $message = [
                'name' => $firstName[0],
                'subscription' => $subscription->getId(),
                'value' => number_format($subscription->getValor(), 2, '.', '')
            ];
            return $response->withJson([
                'status' => 'ok',
                'message' => $message
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function increaseContributionSave(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $fields = ['subscription' => 'Assinatura', 'value' => 'Valor'];
            Validator::requireValidator($fields, $data);
            $subscription = $this->em->getRepository(PersonSignature::class)->findOneBy(['id' => $data['subscription'], 'origin' => 2]);
            if (!$subscription) throw new \Exception("Solicitação Inválida");
            $user = $this->em->getRepository(User::class)->findOneBy(['id' => $subscription->getTbPessoaId()]);
            if (!$user) throw new \Exception("Solicitação Inválida");
            if ($data['value'] == 0) {
                $data['value2'] = str_replace('.', '', $data['value2']);
                $data['value2'] = str_replace(',', '.', $data['value2']);
                $data['value'] = $data['value2'];
            }
            /** Valida se não está tentando diminuir a contribuição */
            if ($data['value'] < $subscription->getValor()) {
                throw new \Exception("Por este link não é possível diminuir seu valor de filiação. Qualquer dúvida, favor entrar em contato com filiacao@novo.org.br");
            }
            if ($data['value'] > MAXVALUE) throw new \Exception(("Desculpe, o valor máximo para contribuição é de R$ 1.064,00!"));
            /** Recaptcha */
            GoogleService::recapchaNovo($data['g-recaptcha-response']);
            /** Registra Fase */
            $this->setHistoricalIncrease($user->getId(), '5', (float)$subscription->getValor(), $data['value']);
            /** Salva Valor */
            $subscription->setValor($data['value']);
            $this->em->getRepository(PersonSignature::class)->save($subscription);
            /** Envia e-mail */
            $msg = Messages::increaseContributionSave($user->getFirstName(), number_format($data['value'], 2, ',', '.'), $subscription->getFrequency());
            $subject = "{$user->getFirstName()}, muito obrigado por ter aceitado o nosso desafio";
            Email::send($user->getEmail(), $user->getName(), $subject, $msg);
            /** Mensam de sucesso */
            $message = "Muito obrigado pelo aumento da sua contribuição. Ela será fundamental para fazer as mudanças que sonhamos para o Brasil!";
            /** verifica se é adimplente */
            $transactionRede['responseMessage'] = null;
            $transactionEncode = null;
            $transaction = $this->em->getRepository(Transaction::class)->lastTransaction($user)[1];
            /** Tenta cobrar última aberta (se houver) */
            if ($transaction && $subscription->getFormaPagamento() == 1 && $subscription->getGatewayPagamento() == 2) {
                $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['id' => $transaction]);
                $directoryGatway = $this->em->getRepository(DirectoryGatway::class)->findOneBy(['tbDiretorioId' => 1, 'formaPagamento' => 1, 'gatwayPagamento' => 2]);
                /** Salva personRede */
                $personRede = $this->em->getRepository(PersonRede::class)->findOneBy(['user' => $user->getId()], ['id' => 'DESC']);
                if (!$personRede || $this->env != 'prod') {
                    $redeId = MaxiPago::createCustomer($user);
                    $personRede = new PersonRede();
                    $personRede->setUser($user)->setCustomerId($redeId);
                    $this->em->getRepository(PersonRede::class)->save($personRede);
                }
                $transactionRede = MaxiPago::transaction($personRede, $subscription->getTokenCartaoCredito(), $transaction->getValor(), 'Filiação', $directoryGatway, $user, true);
                if ($transactionRede['responseMessage'] == 'CAPTURED') {
                    $date = new \Datetime();
                    $transaction->setUltimaAtualizacao($date)
                        ->setStatus('paid')
                        ->setDataPago($date)
                        ->setFormaPagamento(1)
                        ->setGatewayPagamento(2)
                        ->setInvoiceId($transactionRede['transactionID'])
                        ->setObs('Origem: Aumento Contribuição')
                        ->setNumeroRecibo($this->getReceiptDirectory($this->em->getReference(Directory::class, $transaction->getTbDiretorioId()), $transaction));
                    $this->em->getRepository(Transaction::class)->save($transaction);
                    $this->validLastTransaction($user, $transaction, 4);
                } else {
                    $transactionEncode = base64_encode($transaction->getId());
                    $message .= "<br>Não foi possível realizar a cobrança da sua última contribuição, em instantes você será direcionado para atualizar os dados do seu cartão!";
                }
            }
            return $response->withJson([
                'status' => 'ok',
                'message' => $message,
                'transactionRede' => $transactionRede['responseMessage'],
                'transaction' => $transactionEncode
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function filiationValues(Request $request, Response $response)
    {
        try {
            $values = [
                'monthy' => MINMONTHLY,
                'semester' => MINSEMESTER,
                'yearly' => MINYEARLY,
                'maxValue' => MAXVALUE
            ];
            return $response->withJson([
                'status' => 'ok',
                'message' => $values,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function getUserRefreshCard(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $user = $this->em->getRepository(User::class)->findOneBy(['id' => base64_decode($data['user'])]);
            if (!$user) throw new \Exception("Solicitação Inválida");
            $transaction = $this->em->getRepository(Transaction::class)->getLastOpenedTransaction($user);
            $digitableLine = $value = null;
            if($transaction) {
                if(strtotime($transaction['data_vencimento']) >= strtotime(date('Y-m-d')) && $transaction['codigo_barras']) {
                    if($transaction['codigo_barras']) {
                        $digitableLine = $transaction['codigo_barras'];
                        $value = number_format($transaction['valor'],2,",",".");
                    }
                }
            }
            $subscription = $this->em->getRepository(PersonSignature::class)->findOneBy(['tbPessoaId' => $user->getId()]);
            $firstName = explode(' ', $user->getName());
            $message = ['name' => $firstName[0], 
                        'plan' => $subscription->getPeriodicidade(), 
                        'paymentMethod' => $subscription->getFormaPagamento(),
                        'digitableLine' => $digitableLine,
                        'value' => $value
                    ];
            return $response->withJson([
                'status' => 'ok',
                'message' => $message
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function saveRefreshCard(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            $user = $this->em->getRepository(User::class)->findOneBy(['id' => base64_decode($data['user'])]);
            if (!$user || !in_array($user->getFiliado(), [7, 8])) throw new \Exception('Usuário inválido!');
            $fields = [
                'dueDay' => 'Dia da contribuição',
                'value' => 'Valor da contribuição',
                'paymentMethod' => 'Forma de pagamento',
            ];
            $arr1 = [];
            if($data['paymentMethod'] == 1) {
                $arr1 = [
                    'cvv' => 'Código de verificação',
                    'expiringDate' => 'Válidade do cartão',
                    'cardNumber' => 'Número do cartão',
                    'cardName' => 'Nome como está no cartão',
                    'international' => 'Cartão emitido no Brasil?'
                ];
            }
            $fields = array_merge($fields, $arr1);
            Validator::requireValidator($fields, $data);
            GoogleService::recapchaNovo($data['g-recaptcha-response']);
            $subscription = $this->em->getRepository(PersonSignature::class)->findOneBy(['tbPessoaId' => $user->getId(), 'status' => 'created', 'origin' => 2]);
            if($data['paymentMethod'] == 1) {
                $cards = $this->em->getRepository(PersonCreditCard::class)->check($user)['total'];
                if ($cards >= 2) throw new \Exception('Você só pode cadastrar no máximo 2 cartões por dia!');    
                $data['flag'] ??= '';
                /** Check cardName */
                $this->checkCardName($user, $data['cardName']);
                /** Disabled others cards */
                $cards = $this->em->getRepository(PersonCreditCard::class)->findBy(['user' => $user->getId()]);
                foreach ($cards as $card) {
                    $card->setPrincipal(0)->setVisivel(0);
                    $this->em->getRepository(PersonCreditCard::class)->save($card);
                }
                /** Save card IUGU */
                $card = $this->createCardApiIugu($user, $data['cardNumber'], $data['cardName'], $data['cvv'], $data['expiringDate'], $data['flag'], $data['international']);
                /** Save subscriptions IUGU */
                $subscription->setPaymentForm(1)
                    ->setCustomerId($card['customer'])
                    ->setTokenCartaoCredito($card['paymentToken']);
                /** Tenta realizar pagamento de última aberta */
                $this->payLastOpened($user, $card['paymentToken'], $data);
                $msg = 'Parabéns, agora está tudo certo com as suas contribuições. Sua ajuda é fundamental para sermos um time
                    cada vez mais forte na transformação do Brasil!';
                $this->updatePaymentStatus($user, 4);
            } else {
                $openTransaction = $this->em->getRepository(Transaction::class)->getLastOpenedTransaction($user);
                $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['id' => $openTransaction['id']]);
                if(!$transaction) die (var_dump("Solicitação Inválida"));
                $address = $this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()]);
                $address = $address ? $this->iuguAddress($address) : $this->iuguAddressDonation();
                $slip = IuguService::slip($user->getEmail(), $user->getCpf(), $user->getName(), $address, $data['value'], 'Filiação');
                $transaction->setFormaPagamento(2)
                            ->setValor($data['value'])
                            ->setCodigoBarras($slip['bank_slip']['digitable_line'])
                            ->setUrl($slip['bank_slip']['barcode'])
                            ->setStatus('pending')
                            ->setDataVencimento(new \Datetime($slip['due_date']))
                            ->setInvoiceId($slip['id']);
                $this->em->getRepository(Transaction::class)->save($transaction);
                $msg = "Boleto gerado com sucesso. <br> 
                        Código de Barras: {$slip['bank_slip']['digitable_line']} <br> 
                        <span class='btn btn-primary' data-clipboard-text='{$slip['bank_slip']['digitable_line']}'>Copiar Código de Barras <i class='fa fa-barcode'></i></span>
                        <a class='btn btn-success' href='{$slip['bank_slip']['bank_slip_pdf_url']}' target='_blank'>Baixar o boleto <i class='fa fa-file-pdf-o'></i></a><br>
                        Atenção, seu boleto pode demorar até 2 horas para ser registrado, se não conseguir realizar o pagamento aguarde alguns instantes ou tente fazer via PIX.";
                $this->updatePaymentStatus($user, 2);
            }
            $subscription->setPaymentForm($data['paymentMethod'])
                        ->setValor($data['value'])
                        ->setGatewayPagamento(1)
                        ->setDiaCiclo($data['dueDay'])
                        ->setObs('RD');
            $this->em->getRepository(PersonSignature::class)->save($subscription);
            
            /*-----Hubspot/Mailchimp-----*/
            $this->updateStatusRegister($user);
            
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => $msg
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function getDataMovidesk(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $ticketData = Movidesk::getTicket($data['Id']);
            $user = $this->em->getRepository(User::class)->findOneBy(['email' => $ticketData['createdBy']['email']]);
            if(!$user) throw new \Exception("Cadastro com o email {$ticketData['createdBy']['email']} nao encontrado!");
            $array = [
                'id' => $ticketData['id'],
                'customFieldValues' => [
                    [
                        "customFieldId" => 146015,
                        "customFieldRuleId" => 73775,
                        "line" => 1,
                        "value" => Utils::onlyNumbers($user->getCpf()),
                        "items" => []
                    ],
                    [
                        "customFieldId" => 146018,
                        "customFieldRuleId" => 73780,
                        "line" => 1,
                        "value" => $user->getFiliadoId() ?: '',
                        "items" => []
                    ]
                ]
            ];
            $updateTicket = Movidesk::updateTicket($array);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Atualizacao bem sucedida!'
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function getDataSearch(Request $request, Response $response) 
    {
        try {
            $data = (array)$request->getParams();
            $user = $this->em->getRepository(User::class)->getDataSearch($data['hash'])[0];
            if(!$user) throw new \Exception("Solicitação Inválida");
            $questionnaireUser = $this->em->getRepository(ResearchNovoUser::class)->verifyAnswer($user['id']);
            $array = [
                'token' => $user['token_ad'], 'name' => $user['nome'], 'cpf' => substr_replace($user['cpf'], '***.***', 4, -3), 'email' => $user['email'], 'gender' => $user['genero'],
                'state' => $user['state'], 'city' => $user['titulo_eleitoral_municipio_id'], 'schooling' => $user['escolaridade'],
                'situation' => $user['situation'], 'affiliated' => $user['filiado'], 'age' => $user['idade'], 'questionnaireUser' => $questionnaireUser
            ];
            return $response->withJson([
                'status' => 'ok',
                'message' => $array
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function saveSearch(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            $fields = [
                'sex' => 'Sexo',
                'age' => 'Idade',
                'state' => 'Estado',
                'city' => 'Cidade',
                'profession' => 'Ocupação',
                'schooling' => 'Nível de escolaridade',
                'income' => 'Faixa de renda familiar mensal bruta'
            ];
            if($data['affiliated'] == 9) {
                $questionsArray = [
                    'form2Question1' => 'Questão 1',
                    'form2Question2' => 'Questão 2',
                    'form2Question3' => 'Questão 3',
                    'form2Question4' => 'Questão 4',
                    'form2Question5' => 'Questão 5',
                    'form2Question6' => 'Questão 6',
                ];
            } else {
                $questionsArray = [
                    'question1' => 'Questão 1',
                    'question2' => 'Questão 2',
                    'question3' => 'Questão 3',
                    'question4' => 'Questão 4',
                    'question5' => 'Questão 5',
                    'question6' => 'Questão 6'
                ];
            }
            $fields = array_merge($fields, $questionsArray);
            Validator::requireValidator($fields, $data);
            GoogleService::recapchaNovoDoacao($data['g-recaptcha-response']);
            $date = new \Datetime();
            if(!$data['token']) throw new \Exception("Solicitação inválida!");
            $user = $this->em->getRepository(User::class)->findOneBy(['tokenAd' => $data['token']]);
            if(!$user) throw new \Exception("Solicitação inválida!");
            $state = $this->em->getRepository(State::class)->findOneBy(['sigla' => $data['state']]);
            $check = $this->em->getRepository(ResearchNovoUser::class)->verifyAnswer($user->getId());
            if($check) throw new \Exception("Você já preencheu este formulário");
            $user->setGenero($this->em->getReference(Gender::class, $data['sex']))
                ->setTituloEleitoralUfId($state)
                ->setTituloEleitoralMunicipioId($this->em->getReference(City::class, $data['city']))
                ->setEscolaridade($this->em->getReference(Schooling::class, $data['schooling']));
            if($user->getEmail() != $data['email']) $user->setEmail($data['email']);
            $this->em->getRepository(User::class)->save($user);
            $researchNovoUser = new ResearchNovoUser();
            $researchNovoUser->setUser($user)
                    ->setAge($data['age'])
                    ->setProfession($data['profession'])
                    ->setIncome($data['income'])
                    ->setContact($data['term'] ? true : false);
            $this->em->getRepository(ResearchNovoUser::class)->save($researchNovoUser);
            if($data['affiliated'] == 9) {
                $researchNovoUnaffiliated = new ResearchNovoUnaffiliated();
                $researchNovoUnaffiliated->setResearchNovoUserUser($researchNovoUser)
                            ->setQuestion1(implode(",", $data['form2Question1']))
                            ->setTextAreaOthersQuestion1($data['form2TextAreaOthersQuestion1'])
                            ->setQuestion2(implode(",", $data['form2Question2']))
                            ->setTextAreaOthersQuestion2($data['form2TextAreaOthersQuestion2'])
                            ->setQuestion3(implode(",", $data['form2Question3']))
                            ->setTextAreaOthersQuestion3($data['form2TextAreaOthersQuestion3'])
                            ->setQuestion4(implode(",", $data['form2Question4']))
                            ->setTextAreaOthersQuestion4($data['form2TextAreaOthersQuestion4'])
                            ->setQuestion5($data['form2Question5'])
                            ->setQuestion6($data['form2Question6']);
                $this->em->getRepository(ResearchNovoUnaffiliated::class)->save($researchNovoUnaffiliated);
            } else {
                $researchNovoAffiliated = new ResearchNovoAffiliated();
                $researchNovoAffiliated->setResearchNovoUser($researchNovoUser)
                            ->setQuestion1(implode(",", $data['question1']))
                            ->setTextAreaOthersQuestion1($data['textAreaOthersQuestion1'])
                            ->setQuestion2(implode(",", $data['question2']))
                            ->setTextAreaOthersQuestion2($data['textAreaOthersQuestion2'])
                            ->setQuestion3(implode(",", $data['question3']))
                            ->setTextAreaOthersQuestion3($data['textAreaOthersQuestion3'])
                            ->setTextAreaOthersQuestion31($data['textAreaOthersQuestion31'])
                            ->setTextAreaOthersQuestion32($data['textAreaOthersQuestion32'])
                            ->setQuestion4($data['question4'])
                            ->setQuestion5($data['question5'])
                            ->setQuestion51Option1($data['question51Option1'])
                            ->setQuestion51Option2($data['question51Option2'])
                            ->setQuestion52Option1($data['question52Option1'])
                            ->setQuestion52Option2($data['question52Option2'])
                            ->setQuestion53Option1($data['question53Option1'])
                            ->setQuestion53Option2($data['question53Option2'])
                            ->setQuestion6($data['question6']);
                if($data['question31']) {
                    $researchNovoAffiliated->setQuestion31($data['question31']);
                    if($data['question31'] == 2) $researchNovoAffiliated->setQuestion32($data['question32']);
                } 
                $this->em->getRepository(ResearchNovoAffiliated::class)->save($researchNovoAffiliated);
                if($user->isAffiliated()) {
                    $mission = $this->em->getRepository(Mission::class)->find(3);
                    $userPoint = $this->em->getRepository(UserPoint::class)->findOneBy(['user' => $user, 'mission' => 3]);
                    if (!$userPoint){
                        $userPoint = new UserPoint();
                        $userPoint->setUser($user)
                            ->setMission($mission)
                            ->setPoints($mission->getPoints())
                            ->setStatus(UserPoint::APPROVED_MISSION);
                        $this->em->persist($userPoint);
                        $this->em->flush();
                    }
                }
            }
            $msg = "Pesquisa salva com sucesso, o NOVO agradece sua participação!";
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => $msg
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),]
            )->withStatus(500);
        }
    }

    public function csvAffiliatedSearch(Request $request, Response $response) 
    {
        $token = $request->getAttribute('route')->getArgument('token');
        if($token != md5(date('Y-m-d'))) die();
        $affiliateds = $this->em->getRepository(ResearchNovoAffiliated::class)->getList();
        $filename = "Pesquisa Filiados -" . date('Y-m-d') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = [
            'Registro',
            'Nome', 
            'CPF',
            'E-mail',
            'Sexo',
            'Idade',
            'Estado',
            'Cidade',
            'Ocupação',
            'Escolaridade',
            'Faixa de renda familiar mensal bruta',
            'Newsletter',
            '1. Por que você quer ver o Partido NOVO crescer? (Selecione aqueles com os quais você mais se identifica)',
            'Descrição',
            '2. Como você gostaria de ter maior participação nas atividades do Partido NOVO?',
            'Descrição',
            '3. O que levou você a se filiar ao Partido NOVO?',
            'Descrição',
            '3.1 Percebemos que sua contribuição financeira mensal como Filiado encontra-se defasada ou atrasada. Poderia indicar o motivo que o levou a deixar de realizar essa contribuição?',
            'Descrição',
            '3.2 Entendemos a situação. E, já que você gostaria de continuar contribuindo financeiramente, qual valor ficaria bom para você, atualmente?',
            'Descrição',
            '4. Como você entende que sua contribuição financeira mensal como Filiado contribui para a construção de um NOVO Brasil?',
            '5. Em uma escala de 0 a 10, qual a probabilidade de você recomendar a filiação ao Partido NOVO a um amigo ou colega?',
            '(9,10) 5.1 Qual a principal razão para você recomendar a filiação ao Partido?',
            '(9,10) 5.2 O que o Partido NOVO poderia fazer ainda melhor?',
            '(7,8) 5.1 Qual a principal razão para você recomendar a filiação ao Partido?',
            '(7,8) 5.2 Qual é a principal ação que o Partido NOVO poderia tomar para tornar você mais propenso a recomendar a filiação ao mesmo?',
            '(0-6) 5.1 Qual é a principal ação que o Partido NOVO poderia tomar para tornar você mais propenso a recomendar a filiação ao mesmo?',
            '(0-6) 5.2 Você tem algum outro comentário?',
            '6. Você gostaria de receber acesso exclusivo a uma apresentação sobre o novo momento do Partido NOVO?'
        ];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($affiliateds as $affiliated) {
            fputcsv($out, array_values($affiliated), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function csvUnaffiliatedSearch(Request $request, Response $response) 
    {
        $token = $request->getAttribute('route')->getArgument('token');
        if($token != md5(date('Y-m-d'))) die();
        $unaffiliateds = $this->em->getRepository(ResearchNovoUnaffiliated::class)->getList();
        $filename = "Pesquisa Desfiliados -" . date('Y-m-d') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = [
            'Registro',
            'Nome', 
            'CPF',
            'E-mail',
            'Sexo',
            'Idade',
            'Estado',
            'Cidade',
            'Ocupação',
            'Escolaridade',
            'Faixa de renda familiar mensal bruta',
            'Newsletter',
            '1. Quando você se filiou ao Partido NOVO, quais foram os principais motivos que o levaram a fazer isso?',
            'Descrição',
            '2. Quando você se filiou ao Partido NOVO, o que você gostaria de ver acontecer, ou seja, quais foram os posicionamentos com os quais você mais se identificou?',
            'Descrição',
            '3. E o que levou você a se desfiliar do Partido NOVO?',
            'Descrição',
            '4. O que faria você retornar ao Partido NOVO (refiliar-se)?',
            '5. Você gostaria de deixar outras sugestões ou detalhar suas opiniões? Utilize o campo abaixo:',
            '6. Você gostaria de receber acesso exclusivo a uma apresentação sobre o novo momento do Partido NOVO?'
        ];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($unaffiliateds as $unaffiliated) {
            fputcsv($out, array_values($unaffiliated), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function saveSearchInformative(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            $fields = [
                'question1' => 'Questão 1',
                'question2' => 'Questão 2',
                'question3' => 'Questão 3'
            ];
            Validator::requireValidator($fields, $data);
            GoogleService::recapchaNovoDoacao($data['g-recaptcha-response']);
            if(!$data['token']) throw new \Exception("Solicitação inválida!");
            $user = $this->em->getRepository(User::class)->findOneBy(['tokenAd' => $data['token']]);
            if(!$user) throw new \Exception("Solicitação inválida!");
            $check = $this->em->getRepository(ResearchNovoInformative::class)->findOneBy(['user' => $user]);
            if($check) throw new \Exception("Você já preencheu este formulário");
            $ResearchNovoInformative = new ResearchNovoInformative();
            $ResearchNovoInformative->setCreated(new \Datetime())
                        ->setUser($user)
                        ->setQuestion1(implode(",", $data['question1']))
                        ->setQuestion2(implode(",", $data['question2']))
                        ->setTextAreaOthersQuestion2($data['textAreaOthersQuestion2'])
                        ->setQuestion3($data['question3']);
            $this->em->getRepository(ResearchNovoInformative::class)->save($ResearchNovoInformative);
            $msg = "Pesquisa salva com sucesso, o NOVO agradece sua participação!";
            /* Mobiliza */
            $mission = $this->em->getRepository(Mission::class)->find(13);
            $userPoint = $this->em->getRepository(UserPoint::class)->findOneBy(['user' => $user, 'mission' => 13]);
            if (!$userPoint){
                $userPoint = new UserPoint();
                $userPoint->setUser($user)
                    ->setMission($mission)
                    ->setPoints($mission->getPoints())
                    ->setStatus(UserPoint::APPROVED_MISSION);
                $this->em->persist($userPoint);
                $this->em->flush();
            }
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => $msg
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),]
            )->withStatus(500);
        }
    }

    public function csvInformativeSearch(Request $request, Response $response) 
    {
        $token = $request->getAttribute('route')->getArgument('token');
        if($token != md5(date('Y-m-d'))) die();
        $answers = $this->em->getRepository(ResearchNovoInformative::class)->getList();
        $filename = "Pesquisa Informativa -" . date('Y-m-d') . ".csv";
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: text/csv; charset=UTF-8");
        $out = fopen("php://output", 'w');
        $header = [
            'ID',
            'FiliadoID',
            'Nome', 
            'E-mail',
            'Sexo',
            'Data Resposta',
            '1. Qual destas opções representa seu maior interesse ao seu filiar ao NOVO?',
            '2. Você já participou ou participa de algum movimento de capacitação política?',
            'Descrição',
            '3. Você tem alguma habilidade gostaria de colocar à disposição do NOVO, em um trabalho de voluntariado?'
        ];
        fputs($out, "\xEF\xBB\xBF"); // UTF-8 BOM !!!!!
        fputcsv($out, array_values($header), ';', '"');
        foreach ($answers as $answer) {
            fputcsv($out, array_values($answer), ';', '"');
        }
        fclose($out);
        exit;
    }

    public function invoiceSite(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $id = base64_decode($request->getAttribute('route')->getArgument('id'));
            $transaction = $this->em->getRepository(Transaction::class)->find($id);
            if(!$transaction) throw new \Exception("Solicitação inválida, transação não encontrada.");
            if(in_array($transaction->getStatus(), ['canceled', 'reversed'])) {
                throw new \Exception("Soicitação inválida, transação não pode ser paga.");
            } 
            $user = $this->em->getRepository(User::class)->find($transaction->getTbPessoaId());
            $arr = [
                'id' => $transaction->getId(),
                'name' => $user->getName(),
                'cpf' => $user->getCpf(),
                'created' => $transaction->getDataCreation()->format('d/m/Y'),
                'invoice' => $transaction->getInvoiceId(),
                'value' => $transaction->getValue(),
                'dueDate' => $transaction->getDataVencimento()->format('d/m/Y'),
                'status' => $transaction->getStatus(),
            ];
            if($transaction->getStatus() == 'paid') {
                $arr2 = ['payDay' => $transaction->getDataPago()->format('d/m/Y')];
                $arr = array_merge($arr, $arr2);
            } 
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => $arr
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),]
            )->withStatus(500);
        }
    }

    public function saveInvoiceSite(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            $date = new \DateTime();
            $fields = [
                'paymentMethod' => 'Forma de Pagamento',
                'transaction' => 'Transação',
            ];
            if ($data['paymentMethod'] == 1) {
                $fields = [
                    'cvv' => 'CVV',
                    'expiringDate' => 'Válidade do cartão',
                    'cardNumber' => 'Número do cartão',
                    'cardName' => 'Nome como está no cartão',
                    'international' => 'Cartão emitido no Brasil?'
                ];
            }
            Validator::requireValidator($fields, $data);
            $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['id' => base64_decode($data['transaction'])]);
            if (!$transaction) throw new \Exception("Transação inválida!");
            $user = $this->em->getRepository(User::class)->find($transaction->getTbPessoaId());
            $address = $this->iuguAddress($this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()]));
            $keyIUGU = $this->getIuguTokenDirectory($transaction->getTbDiretorioId());
            $type = 0;
            if ($data['paymentMethod'] == 1) {
                GoogleService::recapchaNovo($data['g-recaptcha-response']);
                $data['flag'] ??= '';
                $this->disabledCards($user);
                $card = $this->createCardApiIugu($user, $data['cardNumber'], $data['cardName'], $data['cvv'], $data['expiringDate'], $data['flag'], $data['international']);
                $subscriptions = $this->em->getRepository(PersonSignature::class)->findBy(['tbPessoaId' => $user->getId(), 'status' => 'created']);
                foreach ($subscriptions as $subscription) {
                    $subscription->setPaymentForm(1)
                        ->setGatewayPagamento(1)
                        ->setCustomerId($card['customer'])
                        ->setTokenCartaoCredito($card['paymentToken']);
                    $this->em->getRepository(PersonSignature::class)->save($subscription);
                }
                $charge = IuguService::charge($card['customer'], $card['paymentToken'], $user->getEmail(), $user->getCpf(), $user->getName(), $address, $transaction->getValor(), $transaction->getOriginTypeString(), 0, $keyIUGU['key']);
                $transaction->setTokenCartaoCredito($card['paymentToken'])
                    ->setFormaPagamento(1)
                    ->setGatewayPagamento(1)
                    ->setDataPago($date)
                    ->setStatus('paid')
                    ->setRedeId('')
                    ->setUrl("https://novo.org.br/faturas/" . $data['transaction'])
                    ->setCodigoBarras('')
                    ->setInvoiceId($charge['invoice_id'])
                    ->setCreditCardFlag('')
                    ->setNumeroRecibo($this->getReceiptDirectory($this->em->getReference(Directory::class, $transaction->getTbDiretorioId()), $transaction));
                $this->em->getRepository(Transaction::class)->save($transaction);
                if($transaction->getOrigemTransacao() == 2) $this->validLastTransaction($user, $transaction, 4);
                $message = [
                    "paymentMethod" => $data['paymentMethod'],
                    "message" => "Pagamento realizado com sucesso, o NOVO agradece sua contribuição!"
                ];
                $type = 4;
            } elseif($data['paymentMethod'] == 2) {
                $slip = IuguService::slip($user->getEmail(), $user->getCpf(), $user->getName(), $address, $transaction->getValor(), $transaction->getOriginTypeString(), 5, $keyIUGU['key']);
                $transaction->setDataVencimento($date->add(new \DateInterval('P5D')))
                    ->setFormaPagamento(2)
                    ->setGatewayPagamento(1)
                    ->setUrl($slip['bank_slip']['bank_slip_pdf_url'])
                    ->setStatus('pending')
                    ->setCodigoBarras($slip['bank_slip']['bank_slip_pdf_url'])
                    ->setInvoiceId($slip['id']);
                $this->em->getRepository(Transaction::class)->save($transaction);
                if($transaction->getOrigemTransacao() == 2) $this->validLastTransaction($user, $transaction, 2);
                $message = [
                    "paymentMethod" => $data['paymentMethod'],
                    "digitable_line" => $slip['bank_slip']['digitable_line'],
                    "url" => $slip['bank_slip']['bank_slip_pdf_url'],
                    "message" => "Boleto gerado com sucesso. <br> 
                        Atenção, seu boleto pode demorar até 2 horas para ser registrado,<br>
                        se não conseguir realizar o pagamento aguarde alguns instantes ou tente fazer via PIX."
                ];
                $type = 1;
            } elseif($data['paymentMethod'] == 11) {
                $pix = IuguService::slip($user->getEmail(), $user->getCpf(), $user->getName(), $address, $transaction->getValor(), $transaction->getOriginTypeString(), 5, $keyIUGU['key']);
                if (!$pix['pix']['qrcode_text']) throw new \Exception("Erro ao gerar QR Code Pix");
                $transaction->setDataVencimento($date->add(new \DateInterval('P5D')))
                    ->setFormaPagamento(11)
                    ->setGatewayPagamento(1)
                    ->setUrl($pix['bank_slip']['bank_slip_pdf_url'])
                    ->setStatus('pending')
                    ->setCodigoBarras($pix['pix']['qrcode'])
                    ->setInvoiceId($pix['id']);
                $this->em->getRepository(Transaction::class)->save($transaction);
                if($transaction->getOrigemTransacao() == 2) $this->validLastTransaction($user, $transaction, 2);
                $message = [
                    "paymentMethod" => $data['paymentMethod'],
                    "qrcode_text" => $pix['pix']['qrcode_text'],
                    "qrcode" => $pix['pix']['qrcode'],
                    "message" => "QR Code para doação gerado com sucesso. <br> 
                        Procure em seu aplicativo de banco ou conta digital essa funcionalidade e escaneie o QR Code disponível 
                        ou copie o código utilizando a opção de “Pix Copia e Cola” para efetivar um pagamento.<br>
                        <b>O sistema pode levar até 24h para dar a baixa na contribuição.</b>"
                ];
                $type = 1;
            } else {
                throw new \Exception("Solicitação inválida");
            }
            /*-----Hubspot/Mailchimp-----*/
            $this->updateStatusRegister($user);
            $this->updatePaymentStatus($user, $type);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => $message
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),]
            )->withStatus(500);
        }
    }
}