<?php

namespace App\Controllers;

use App\Models\Entities\AccessLevel;
use App\Models\Entities\Directory;
use App\Models\Entities\DirectoryRegister;
use App\Models\Entities\Ombudsman;
use App\Models\Entities\OmbudsmanCategory;
use App\Models\Entities\OmbudsmanResponse;
use App\Models\Entities\OmbudsmanSubject;
use App\Helpers\Validator;
use App\Models\Entities\User;
use App\Services\Email;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

class OmbudsmanController extends Controller
{

    public function index(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $user->isAffiliated(true);
        $ombudsmans = $this->em->getRepository(Ombudsman::class)->findBy(['requester' => $user], ['id' => 'desc']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'ombudsman/index.phtml', 'title' => 'Ouvidoria',
            'section' => 'ombudsman', 'user' => $user, 'ombudsmans' => $ombudsmans]);
    }

    public function register(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $user->isAffiliated(true);
        $categories = $this->em->getRepository(OmbudsmanCategory::class)->findBy(['active' => 1], ['name' => 'asc']);
        $subjects = $this->em->getRepository(OmbudsmanSubject::class)->findBy(['active' => 1], ['name' => 'asc']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'ombudsman/register.phtml', 'title' => 'Ouvidoria Cadastro',
            'section' => 'ombudsman', 'user' => $user, 'categories' => $categories, 'subjects' => $subjects,]);
    }

    public function view(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $user->isAffiliated(true);
        $id = $request->getAttribute('route')->getArgument('id');
        $ombudsman = $this->em->getRepository(Ombudsman::class)->find($id);
        if ($user != $ombudsman->getRequester()) $this->redirect('', true);
        $ombudsmanResponses = $this->em->getRepository(OmbudsmanResponse::class)->findBy(['ombudsman' => $ombudsman], ['id' => 'ASC']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'ombudsman/view.phtml', 'title' => 'Ouvidoria',
            'section' => 'ombudsman', 'user' => $user, 'ombudsman' => $ombudsman, 'ombudsmanResponses' => $ombudsmanResponses]);
    }

    public function save(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            $data['anonymous'] ?? false;
            $fields = [
                'subject' => 'Assunto',
                'destiny' => 'Destino',
                'description' => 'Descrição',
            ];
            Validator::requireValidator($fields, $data);
            $ombudsman = new Ombudsman();
            $categoty = $this->em->getRepository(OmbudsmanCategory::class)->find($data['destiny']);
            $subject = $this->em->getRepository(OmbudsmanSubject::class)->find($data['subject']);
            $ombudsman->setRequester($user)
                ->setDescription($data['description'])
                ->setAnonymous((bool)$data['anonymous'])
                ->setOmbudsmanSubject($subject)
                ->setDestiny($categoty);
            $files = $request->getUploadedFiles();
            $folder = UPLOAD_FOLDER;
            $attachment = $files['attachment'];
            if ($attachment && $attachment->getClientFilename()) {
                $time = time() . $user->getId();
                $extension = explode('.', $attachment->getClientFilename());
                $extension = end($extension);
                $target = "{$folder}{$time}.{$extension}";
                $attachment->moveTo($target);
                $ombudsman->setAttachment($target);
            }
            $ombudsman = $this->em->getRepository(Ombudsman::class)->save($ombudsman);
            $msg = "Prezado(a), {$categoty->getResponsible()->getName()}<br><br>
                    Uma nova solicitação foi cadastrada, acesse o 
                    <a href='https://gestao.novo.org.br/ouvidoria/solicitacoes/{$ombudsman->getId()}' target='_blank'>sistema</a> para responder.<br><br>
                    Categoria: {$categoty->getName()} <br>
                    Assunto: {$subject->getName()}
                    ";
            Email::send($categoty->getResponsible()->getEmail(), $categoty->getResponsible()->getName(),
                'Nova Solicitação - Ouvidoria', $msg);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Solicitação cadastrada com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function saveReply(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            $fields = [
                'description' => 'Descrição',
            ];
            Validator::requireValidator($fields, $data);
            $ombudsman = $this->em->getRepository(Ombudsman::class)->findOneBy(['id' => $data['ombudsman'], 'requester' => $user]);
            if (!$ombudsman) throw new \Exception('Solicitação inválida');
            $ombudsman->setStatus(0);
            $ombudsman = $this->em->getRepository(Ombudsman::class)->save($ombudsman);
            $ombudsmanResponse = new OmbudsmanResponse();
            $ombudsmanResponse->setUser($user)
                ->setAnswer($data['description'])
                ->setOmbudsman($ombudsman);
            $this->em->getRepository(OmbudsmanResponse::class)->save($ombudsmanResponse);
            $msg = "Prezado(a), {$ombudsman->getDestiny()->getResponsible()->getName()}<br><br>
                    Uma solicitação foi atualizada, acesse o 
                    <a href='https://gestao.novo.org.br/ouvidoria/solicitacoes/{$ombudsman->getId()}' target='_blank'>sistema</a> para responder.<br><br>
                    
                    ";
            Email::send($ombudsman->getDestiny()->getResponsible()->getEmail(), $ombudsman->getDestiny()->getResponsible()->getName(),
                'Atualização de Solicitação - Ouvidoria', $msg);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Solicitação atualizada com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

}
