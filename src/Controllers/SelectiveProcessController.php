<?php

namespace App\Controllers;

use App\Helpers\Messages;
use App\Helpers\Session;
use App\Models\Entities\CandidatePost;
use App\Models\Entities\SelectiveProcess2022AnswerModule1;
use App\Models\Entities\SelectiveProcess2022Module1;
use App\Models\Entities\SelectiveProcess2022Module1Question;
use App\Models\Entities\SelectiveProcess2022Module1Video;
use PDO;
use App\Helpers\Validator;
use App\Helpers\Utils;
use App\Services\MaxiPago;
use App\Models\Entities\User;
use App\Models\Entities\Phone;
use App\Models\Entities\Gender;
use App\Models\Entities\Schooling;
use App\Models\Entities\CivilState;
use App\Models\Entities\State;
use App\Models\Entities\ReasonDisaffection;
use App\Models\Entities\City;
use App\Models\Entities\Country;
use App\Models\Entities\Address;
use App\Models\Entities\PersonSignature;
use App\Models\Entities\PersonCreditCard;
use App\Models\Entities\Communicated;
use App\Models\Entities\ChangeEmail;
use App\Models\Entities\HowdidKnow;
use App\Models\Entities\Transaction;
use App\Services\Email;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

class SelectiveProcessController extends Controller
{

    public function apiSelective2022Module1(Request $request, Response $response)
    {
        $header = apache_request_headers();
        if (!isset($header['Token']) || $header['Token'] != '%A79So#I8hvv') {
            $json['message'] = 'Token invalido';
            $json['status'] = 'error';
            echo json_encode($json);
            die();
        }
        $id = $request->getAttribute('route')->getArgument('id');
        $candidate = $this->em->getRepository(SelectiveProcess2022Module1::class)->find($id);
        $video = $this->em->getRepository(SelectiveProcess2022Module1Video::class)->findOneBy(['candidate' => $candidate->getCandidate()]);
        return $response->withJson([
            'status' => 'ok',
            'facebook' => $candidate->getFacebook(),
            'instagram' => $candidate->getInstagram(),
            'twitter' => $candidate->getTwitter(),
            'youtube' => $candidate->getYoutube(),
            'evaluationGovernmentState' => $candidate->getEvaluationGovernmentState(),
            'evaluationGovernmentFederal' => $candidate->getEvaluationGovernmentFederal(),
            'evaluationInstitutionAgents' => $candidate->getEvaluationInstitutionAgents(),
            'evaluationInstitution' => $candidate->getEvaluationInstitution(),
            'region' => $candidate->getRegion(),
            'motivations' => $candidate->getMotivations(),
            'lessons' => $candidate->getLessons(),
            'team' => $candidate->getTeam(),
            'flags' => $candidate->getFlags(),
            'visualIdentity' => $candidate->getVisualIdentity(true),
            'targetAudience' => $candidate->getTargetAudience(),
            'collection' => $candidate->getCollection(),
            'numberOfVotes' => $candidate->getNumberOfVotes(),
            'year' => $candidate->getYear(),
            'candidatePost' => $candidate->getCandidatePost() ? $candidate->getCandidatePost()->getName() : '',
            'professionalHistory' => $candidate->getProfessionalHistory(),
            'academicEducation' => $candidate->getAcademicEducation(),
            'exCandidate' => $candidate->isExCandidate(),
            'created' => $candidate->getCreated()->format('d/m/Y'),
            'video' => $video ? $video->getLink() : '',
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function apiSelective2022Module1Questions(Request $request, Response $response)
    {
        $header = apache_request_headers();
        if (!isset($header['Token']) || $header['Token'] != '%A79So#I8hvv') {
            $json['message'] = 'Token invalido';
            $json['status'] = 'error';
            echo json_encode($json);
            die();
        }
        $id = $request->getAttribute('route')->getArgument('id');
        $candidate = $this->em->getRepository(SelectiveProcess2022Module1::class)->find($id);
        $questions = $this->em->getRepository(SelectiveProcess2022AnswerModule1::class)->findBy(['user' => $candidate->getCandidate()]);
        $questionsArray = [];
        $analiseArray = [];
        foreach ($questions as $question) {
            $questionsArray[] = ['type' => $question->getQuestion()->getTypeString(),
                'question' => $question->getQuestion()->getText(),
                'optionMarked' => $question->getOptionMarked()];
            if (!array_key_exists($question->getQuestion()->getTypeString(), $analiseArray)) {
                $analiseArray[$question->getQuestion()->getTypeString()] = ['score' => 0, 'total' => 0];
            }
            $analiseArray[$question->getQuestion()->getTypeString()] = ['score' => $analiseArray[$question->getQuestion()->getTypeString()]['score'] + $question->getScore(),
                'total' => $analiseArray[$question->getQuestion()->getTypeString()]['total'] + 2];
        }
        return $response->withJson([
            'status' => 'ok',
            'analise' => $analiseArray,
            'questions' => $questionsArray,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function extraData(Request $request, Response $response)
    {
        $header = apache_request_headers();
        if (!isset($header['Token']) || $header['Token'] != '%A79So#I8hvv') {
            $json['message'] = 'Token invalido';
            $json['status'] = 'error';
            echo json_encode($json);
            die();
        }
        $id = $request->getAttribute('route')->getArgument('id');
        $selectiveProcess2022Module1 = $this->em->getRepository(SelectiveProcess2022Module1::class)->find($id);
        $candidate = $selectiveProcess2022Module1->getCandidate();
        $cellPhone = $this->em->getRepository(Phone::class)->findOneBy(['tbPessoaId' => $candidate->getId(), 'tbTipoTelefoneId' => 3], ['id' => 'desc']);
        $address = $this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $candidate->getId()]);
        $addressStr = "{$address->getCidade()}, {$address->getEstado()}";
        if (!$address->getResideExterior()) {
            $addressStr = "{$address->getCidade()},";
            if ($address->getTbCidadeId()) {
                $city = $this->em->getRepository(City::class)->find($address->getTbCidadeId());
                $addressStr = "{$city->getCidade()},";
            }
            if ($address->getStateId()) {
                $state = $this->em->getRepository(State::class)->find($address->getStateId());
                $addressStr .= " {$state->getEstado()}";
            } else {
                $addressStr .= " {$city->getEstado()}";
            }
        }

        return $response->withJson([
            'status' => 'ok',
            'isAffiliated' => $candidate->isAffiliated(),
            'affiliatedDate' => $candidate->affiliatedDate(),
            'phone' => $cellPhone->getTelefone(),
            'address' => $addressStr,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function ModuleOne2022Challenge(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $selectiveProcess2022Module1 = $this->em->getRepository(SelectiveProcess2022Module1::class)->findOneBy(['candidate' => $user], ['id' => 'DESC']);
        $questions = false;
        if ($selectiveProcess2022Module1) {
            $questions = $this->em->getRepository(SelectiveProcess2022Module1Question::class)->getQuizTypes($selectiveProcess2022Module1);
        }
        return $this->renderer->render($response, 'default.phtml', ['page' => 'selectiveProcess/challenge.phtml', 'user' => $user, 'section' => 'selectiveProcessTest',
            'questions' => $questions, 'subMenu' => 'selectiveProcess', 'selectiveProcess2022Module1' => $selectiveProcess2022Module1]);
    }

    public function ModuleOne2022General(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $selectiveProcess2022Module1 = $this->em->getRepository(SelectiveProcess2022Module1::class)->findOneBy(['candidate' => $user], ['id' => 'DESC']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'selectiveProcess/2022-geral.phtml', 'section' => 'selectiveProcessRegister',
            'user' => $user, 'title' => 'Processo Seletivo 2022',
            'selectiveProcess2022Module1' => $selectiveProcess2022Module1, 'subMenu' => 'selectiveProcess']);
    }

    public function ModuleOne2022Video(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $start = $this->em->getRepository(SelectiveProcess2022Module1::class)->findOneBy(['candidate' => $user], ['id' => 'DESC']);
        $selectiveProcess2022Module1 = $this->em->getRepository(SelectiveProcess2022Module1Video::class)->findOneBy(['candidate' => $user]);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'selectiveProcess/video.phtml', 'section' => 'selectiveProcessVideo',
            'user' => $user, 'title' => 'Processo Seletivo 2022', 'start' => $start,
            'selectiveProcess2022Module1' => $selectiveProcess2022Module1, 'subMenu' => 'selectiveProcess']);
    }

    public function ModuleOne2022ExCandidates(Request $request, Response $response)
    {
        if (!Session::get('seletivo2022')) $this->redirect();
        $user = $this->getLogged();
        $posts = $this->em->getRepository(CandidatePost::class)->findBy([], ['name' => 'ASC']);
        $selectiveProcess2022Module1 = $this->em->getRepository(SelectiveProcess2022Module1::class)->findOneBy(['candidate' => $user], ['id' => 'DESC']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'selectiveProcess/2022-ex-candidatos.phtml', 'section' => 'selectiveProcess',
            'user' => $user, 'title' => 'Processo Seletivo 2022', 'posts' => $posts, 'selectiveProcess2022Module1' => $selectiveProcess2022Module1]);
    }

    public function saveModuleOne2022ExCandidates(Request $request, Response $response)
    {
        try {
            $id = $request->getAttribute('route')->getArgument('id');
            $user = $this->em->getRepository(User::class)->find($id ?? 0);
            if (!$user) $user = $this->getLoggedApi();
            $data = (array)$request->getParams();
            $fields = [
                'desiredPost' => 'Cargo',
                'academicEducation' => 'Formação Acadêmica',
                'professionalHistory' => 'Histórico Profissional',
                'year' => 'Ano',
                'post' => 'Cargo',
                'votes' => 'Votos',
                'collection' => 'Arrecadação',
                'targetAudience' => 'Público-alvo',
                'flags' => 'Bandeiras da campanha',
                'team' => 'Equipe de campanha',
                'lessons' => 'Lições aprendidas de campanha',
                'motivations' => 'Motivações para 2022',
                'region' => '>Região/Locais de atuação 2022',
                'evaluationInstitution' => 'Avaliação da Instituição NOVO',
                'evaluationInstitutionAgents' => 'Avaliação da atuação dos mandatários do Novo',
                'evaluationGovernmentFederal' => 'Avaliação do Governo Federal atual',
                'evaluationGovernmentState' => 'Avaliação do Governo Estadual atual',
            ];
            Validator::requireValidator($fields, $data);
            if (empty(trim(($data['facebook']))) && empty(trim(($data['instagram']))) && empty(trim(($data['twitter'])))
                && empty(trim(($data['youtube'])))) {
                throw new \Exception('Informe ao menos uma rede social.');
            }
            $files = $request->getUploadedFiles();
            $folder = UPLOAD_FOLDER;
            $visualIdentity = $files['visualIdentity'];
            if ($visualIdentity && $visualIdentity->getClientFilename()) {
                $time = time();
                $extension = explode('.', $visualIdentity->getClientFilename());
                $extension = end($extension);
                $target = "{$folder}{$time}visualIdentity2022.{$extension}";
                $visualIdentity->moveTo($target);
            } else {
                throw new \Exception('O campo Identidade Visual é obrigátorio');
            }
            $selectiveProcess2022Module1 = new SelectiveProcess2022Module1();
            $selectiveProcess2022Module1->setCandidate($user)
                ->setVisualIdentity($target)
                ->setDesiredPost($data['desiredPost'])
                ->setAcademicEducation($data['academicEducation'])
                ->setProfessionalHistory($data['professionalHistory'])
                ->setYear($data['year'])
                ->setCandidatePost($this->em->getReference(CandidatePost::class, $data['post']))
                ->setNumberOfVotes($data['votes'])
                ->setFacebook($data['facebook'])
                ->setInstagram($data['instagram'])
                ->setTwitter($data['twitter'])
                ->setYoutube($data['youtube'])
                ->setCollection($data['collection'])
                ->setTargetAudience($data['targetAudience'])
                ->setFlags($data['flags'])
                ->setTeam($data['team'])
                ->setLessons($data['lessons'])
                ->setMotivations($data['motivations'])
                ->setRegion($data['region'])
                ->setEvaluationInstitution($data['evaluationInstitution'])
                ->setEvaluationInstitutionAgents($data['evaluationInstitutionAgents'])
                ->setEvaluationGovernmentFederal($data['evaluationGovernmentFederal'])
                ->setEvaluationGovernmentState($data['evaluationGovernmentState']);
            $this->em->getRepository(SelectiveProcess2022Module1::class)->save($selectiveProcess2022Module1);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Formulário enviado com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');

        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function saveModuleOne2022General(Request $request, Response $response)
    {
        try {
            $id = $request->getAttribute('route')->getArgument('id');
            $user = $this->em->getRepository(User::class)->find($id ?? 0);
            if (!$user) $user = $this->getLoggedApi();
            $data = (array)$request->getParams();
            $fields = [
                'desiredPost' => 'Cargo',
                'academicEducation' => 'Formação Acadêmica',
                'professionalHistory' => 'Histórico Profissional',
                'personalHistory' => 'Histórico Pessoal',
                'motivations' => 'Motivações para 2022',
                'region' => '>Região/Locais de atuação 2022',
                'evaluationGovernmentFederal' => 'Avaliação do Governo Federal atual',
                'evaluationGovernmentState' => 'Avaliação do Governo Estadual atual',
            ];
            Validator::requireValidator($fields, $data);
            if (empty(trim(($data['facebook']))) && empty(trim(($data['instagram']))) && empty(trim(($data['twitter'])))
                && empty(trim(($data['youtube'])))) {
                throw new \Exception('Informe ao menos uma rede social.');
            }
            $selectiveProcess2022Module1 = new SelectiveProcess2022Module1();
            $selectiveProcess2022Module1->setCandidate($user)
                ->setDesiredPost($data['desiredPost'])
                ->setAcademicEducation($data['academicEducation'])
                ->setProfessionalHistory($data['professionalHistory'])
                ->setPersonalHistory($data['personalHistory'])
                ->setFacebook($data['facebook'])
                ->setInstagram($data['instagram'])
                ->setTwitter($data['twitter'])
                ->setYoutube($data['youtube'])
                ->setMotivations($data['motivations'])
                ->setRegion($data['region'])
                ->setEvaluationGovernmentFederal($data['evaluationGovernmentFederal'])
                ->setEvaluationGovernmentState($data['evaluationGovernmentState'])
                ->setExCandidate(false);
            $this->em->getRepository(SelectiveProcess2022Module1::class)->save($selectiveProcess2022Module1);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Formulário enviado com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function saveModuleOne2022Video(Request $request, Response $response)
    {
        try {
            $user = $this->getLoggedApi();
            $data = (array)$request->getParams();
            $fields = [
                'link' => 'Link do Vídeo',
            ];
            Validator::requireValidator($fields, $data);
            $selectiveProcess2022Module1 = new SelectiveProcess2022Module1Video();
            $selectiveProcess2022Module1->setCandidate($user)
                ->setLink($data['link']);
            $this->em->getRepository(SelectiveProcess2022Module1Video::class)->save($selectiveProcess2022Module1);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Formulário enviado com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function saveModuleOne2022Test(Request $request, Response $response)
    {
        $id = $request->getAttribute('route')->getArgument('id');
        $user = $this->em->getRepository(User::class)->find($id ?? 0);
        if (!$user) throw new \Exception('Falha na autenticação.');
        $selectiveProcess2022Module1 = $this->em->getRepository(SelectiveProcess2022Module1::class)->findOneBy(['candidate' => $user], ['id' => 'desc']);
        $data = (array)$request->getParsedBody();
        $core = [
            'a' => ['a' => 2, 'b' => 1, 'c' => 0, 'd' => 0, 'e' => 0],
            'b' => ['a' => 1, 'b' => 2, 'c' => 0, 'd' => 0, 'e' => 0],
            'c' => ['a' => 0, 'b' => 0, 'c' => 2, 'd' => 0, 'e' => 0],
            'd' => ['a' => 0, 'b' => 0, 'c' => 0, 'd' => 2, 'e' => 1],
            'e' => ['a' => 0, 'b' => 0, 'c' => 0, 'd' => 1, 'e' => 2],
        ];
        try {
            foreach ($data as $question => $answer) {
                $question = explode('-', $question)[1];
                $correct = $this->em->getRepository(SelectiveProcess2022Module1Question::class)->find($question)->getAnswer();
                $score = $core[$correct][$answer] ?? 1;
                $selectiveProcess2022AnswerModule1 = new SelectiveProcess2022AnswerModule1();
                $selectiveProcess2022AnswerModule1->setUser($user)
                    ->setSelectiveProcess2022Module1($selectiveProcess2022Module1)
                    ->setOptionMarked($answer)
                    ->setScore($score)
                    ->setQuestion($this->em->getReference(SelectiveProcess2022Module1Question::class, $question));
                $this->em->getRepository(SelectiveProcess2022AnswerModule1::class)->save($selectiveProcess2022AnswerModule1);
            }
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Questionário salvo com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function export(Request $request, Response $response)
    {
        $pdo = new \PDO('mysql:host=bd.novo.org.br;dbname=seletivo2022', 'novo_master', 'ThhHNXBvv66HDi');
        $pdo->exec("set names utf8");
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        $stmt = $pdo->prepare("SELECT * FROM evaluations WHERE exCandidate = 0 AND status = 1");
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($result as $r) {
            $status = 1;
            $selectiveModule1 = $this->em->getRepository(SelectiveProcess2022Module1::class)->find((int)$r['externalReference']);
            if ($selectiveModule1) {
                $selectiveModule1Video = $this->em->getRepository(SelectiveProcess2022Module1Video::class)->findOneBy(
                    ['candidate' => $selectiveModule1->getCandidate()]);
                $questions = $this->em->getRepository(SelectiveProcess2022Module1Question::class)->getQuizTypes($selectiveModule1);
                if ($selectiveModule1Video && !$questions) $status = 2;
            }
            $stmt = $pdo->prepare('UPDATE evaluations SET status = :status
                                        WHERE id = :id');
            $stmt->execute([
                ':status' => $status,
                ':id' => $r['id'],
            ]);
        }

        $selectiveModule1 = $this->em->getRepository(SelectiveProcess2022Module1::class)->findBy([], ['id' => 'desc']);
        foreach ($selectiveModule1 as $teste) {
            $stmt = $pdo->prepare("SELECT * FROM evaluations where externalReference = :externalReference;");
            $stmt->execute([
                ':externalReference' => $teste->getId(),
            ]);
            if (!$stmt->fetch(\PDO::FETCH_ASSOC)) {
                $user = $teste->getCandidate();
                $stmt = $pdo->prepare('INSERT INTO candidates (name, publicName, email, cpf, birthDate, telephone) 
                                        VALUES(:name, :publicName, :email, :cpf, :birthDate, :telephone)');
                $stmt->execute([
                    ':name' => $user->getName(),
                    ':publicName' => $user->getName(),
                    ':email' => $user->getEmail(),
                    ':cpf' => $user->getCpf(true),
                    ':birthDate' => $user->getDataNascimento() ? $user->getDataNascimento()->format('Y-m-d') : '2021-03-15',
                    ':telephone' => '',
                ]);
                $candidate = $pdo->lastInsertId();
                $address = $this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()], ['id' => 'desc']);
                $state = $address->getStateId() ? $address->getStateId() : 1;
                $city = $address->getTbCidadeId() ? $address->getTbCidadeId() : 1;
                $stmt = $pdo->prepare('INSERT INTO evaluations (candidate, status, state, city, externalReference,exCandidate, desiredPost) 
                                        VALUES(:candidate, :status, :state, :city, :externalReference, :exCandidate, :desiredPost)');
                $stmt->execute([
                    ':candidate' => $candidate,
                    ':status' => $teste->isExCandidate() ? 2 : 1,
                    ':state' => $state,
                    ':city' => $city,
                    ':externalReference' => $teste->getId(),
                    ':desiredPost' => $teste->getDesiredPost(),
                    ':exCandidate' => $teste->isExCandidate(),
                ]);
            }

        }

    }

    public
    function auth(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParsedBody();
            $usersRepository = $this->em->getRepository(User::class);
            $user = $usersRepository->login($data['email'], $data['password']);
            if (!$user) {
                throw new \Exception('Login ou senha inválidos!');
            }
            $responseData = $this->em->getRepository(User::class)->apiSelectionProcess($data['email']);

            return $response->withJson([
                'status' => 'ok',
                'message' => $responseData,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }
}
