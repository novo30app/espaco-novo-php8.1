<?php

namespace App\Controllers;

use App\Helpers\Utils;
use App\Models\Entities\City;
use App\Models\Entities\State;
use App\Models\Entities\User;
use App\Models\Entities\Candidate;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class DamController extends Controller
{
    public function spending(Request $request, Response $response)
    {
        $this->redirect();
        $user = $this->getLogged();
        $cabinetsOff = array(56, 57, 58, 59, 60, 61, 62, 63);
        $agents = $this->em->getRepository(Candidate::class)->agents($cabinetsOff);
        $states = $this->em->getRepository(Candidate::class)->states();
        $cities = $this->em->getRepository(Candidate::class)->cities();
        $valueSaved = $this->em->getRepository(Candidate::class)->valueSaved();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'dam/spending.phtml', 'section' => 'taxmeter', 'user' => $user, 'title' => 'Economizometro ',
            'subMenu' => 'accountability', 'agents' => $agents, 'states' => $states, 'cities' => $cities, 'valueSaved' => $valueSaved[0]['valueSaved']]);
    }

    public function polls(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $cabinetsOff = array(56, 57, 58, 59, 60, 61, 62, 63);
        $agents = $this->em->getRepository(Candidate::class)->agents($cabinetsOff);
        $polls = $this->em->getRepository(Candidate::class)->polls();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'dam/polls.phtml', 'section' => 'polls', 'user' => $user, 'title' => 'Votações ',
            'subMenu' => 'accountability', 'agents' => $agents, 'polls' => $polls]);
    }

    public function presence(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $cabinetsOff = array(107, 112, 121);
        $agents = $this->em->getRepository(Candidate::class)->agents($cabinetsOff);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'dam/presence.phtml', 'section' => 'presence', 'user' => $user,'title' => 'Presenças ',
            'subMenu' => 'accountability', 'agents' => $agents]);
    }

    public function reports(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $agents = $this->em->getRepository(Candidate::class)->agents();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'dam/reports.phtml', 'section' => 'library', 'user' => $user,'title' => 'Relatórios ',
            'subMenu' => 'accountability', 'agents' => $agents]);
    }

    public function projects(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $cabinetsOff = array(107, 112, 121);
        $agents = $this->em->getRepository(Candidate::class)->agents();
        $tags = $this->em->getRepository(Candidate::class)->getTags($cabinetsOff);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'dam/projects.phtml', 'section' => 'projects', 'user' => $user,'title' => 'Repositório de projetos ',
            'subMenu' => 'accountability', 'agents' => $agents, 'tags' => $tags]);
    }

    public function getProjects(Request $request, Response $response)
    {
        $repository = $this->em->getRepository(Candidate::class)->listProject();
        $projects = [];
        foreach ($repository as $p) {
            $projects[] = ['id' => $p['id'], 'name' => $p['name'], 'ementa' => $p['ementa'], 'coverage' => $p['coverage'], 'date' => date('d/m/Y', strtotime($p['date'])),
                'nickname' => $p['nickname'], 'cabinet' => $p['cabinet'], 'link' => $p['link'], 'filePath' => $p['filePath']];
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $projects,
        ])->withHeader('Content-Type', 'application/json');
    }

    public function getProjectTags(Request $request, Response $response) {
        $repository = $this->em->getRepository(Candidate::class)->listTags();
        $tags = [];
        foreach ($repository as $t) {
            $tags[] = ['id' => $t['id'], 'name' => $t['name'], 'tagId' => $t['tagId'], 'project' => $t['project']];
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $tags,
        ])->withHeader('Content-Type', 'application/json');
    }

    public function getProjectAuthors(Request $request, Response $response) {
        $repository = $this->em->getRepository(Candidate::class)->listAuthors();
        $authors = [];
        foreach ($repository as $a) {
            $authors[] = ['id' => $a['id'], 'name' => $a['author'], 'project' => $a['project']];
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $authors,
        ])->withHeader('Content-Type', 'application/json');
    }
}