<?php

namespace App\Controllers;

use App\Helpers\Session;
use App\Models\Entities\User;
use App\Models\Entities\State;
use App\Models\Entities\Directory;
use App\Models\Entities\Transaction;
use App\Models\Entities\PaymentRequirements;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

class TransparencyPortalController extends Controller
{
    public function index(Request $request, Response $response)
    {
        $user = $this->getLogged([7, 8]);
        $verifyExemptionAffiliation = $this->em->getRepository(User::class)->verifyExemptionAffiliation($user->getId());
        $verifyCompliance = $this->em->getRepository(Transaction::class)->verifyCompliance($user->getId())['situation'];
        if(!$verifyExemptionAffiliation && $verifyCompliance != 'Adimplente') {
            Session::set('errorMsg', 'Você não poderá acessar este conteúdo até que regularize suas contribuições!');
            header("Location: {$this->baseUrl}{$url}");
            die();
        }
        $states = $this->em->getRepository(State::class)->findAll();
        $directories = $this->em->getRepository(Directory::class)->getDirectoriesTransparencyPortal();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'transparencyPortal/index.phtml', 'states' => $states,
            'section' => 'transparencyPortal', 'user' => $user, 'title' => 'Portal da Transparência', 'directories' => $directories]);
    }

    public function getList(Request $request, Response $response)
    {
        $index = $request->getQueryParam('index');
        $payDayBegin = $request->getQueryParam('payDayBegin');
        $payDayEnd = $request->getQueryParam('payDayEnd');
        $paymentValue = $request->getQueryParam('paymentValue');
        $origin = $request->getQueryParam('origin');
        $description = $request->getQueryParam('description');
        $directory = $request->getQueryParam('directory');
        $providerCnpj = $request->getQueryParam('providerCnpj');
        $providerName = $request->getQueryParam('providerName');
        $payments = $this->em->getRepository(PaymentRequirements::class)->listTransparencyPortal($payDayBegin, $payDayEnd, $paymentValue, $origin, $description, $directory, $providerCnpj, $providerName, 20, $index * 20);
        $total = $this->em->getRepository(PaymentRequirements::class)->listTransparencyPortalTotal($payDayBegin, $payDayEnd, $paymentValue, $origin, $description, $directory, $providerCnpj, $providerName)['total'];
        $partial = ($index * 20) + sizeof($payments);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status'  => 'ok',
            'message' => $payments,
            'total'   => $total,
            'partial' => $partial,
        ], 200)->withHeader('Content-type', 'application/json');
    }

    public function contracts(Request $request, Response $response)
    {
        $user = $this->getLogged([7, 8]);
        $verifyExemptionAffiliation = $this->em->getRepository(User::class)->verifyExemptionAffiliation($user->getId());
        $verifyCompliance = $this->em->getRepository(Transaction::class)->verifyCompliance($user->getId())['situation'];
        if(!$verifyExemptionAffiliation && $verifyCompliance != 'Adimplente') {
            Session::set('errorMsg', 'Você não poderá acessar este conteúdo até que regularize suas contribuições!');
            header("Location: {$this->baseUrl}{$url}");
            die();
        }
        $contract = $request->getAttribute('route')->getArgument('contract');
        $contractNumber = $this->em->getRepository(PaymentRequirements::class)->getContractNumber($contract);
        $contracts = $this->em->getRepository(PaymentRequirements::class)->getContracts($contract);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'transparencyPortal/contracts.phtml',
            'section' => 'transparencyPortal', 'user' => $user, 'title' => 'Contrato - ' . $contractNumber['number'], 'contracts' => $contracts]);
    }
}