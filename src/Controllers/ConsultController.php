<?php

namespace App\Controllers;

use App\Helpers\Validator;
use App\Models\Entities\Quiz1;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

class ConsultController extends Controller
{

    public function index(Request $request, Response $response)
    {
        $user = $this->getLogged();
        if (!$user->isAffiliated()) $this->redirect();
        $quiz = $this->em->getRepository(Quiz1::class)->findOneBy(['user' => $user]);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'consult/index.phtml',
            'section' => 'consult', 'user' => $user, 'title' => 'Pesquisa', 'quiz' => $quiz]);
    }


    public function save(Request $request, Response $response)
    {
        try {
            $user = $this->getLoggedApi();
            $data = (array)$request->getParams();
            $fields = [
                'question1' => 'Questão 1',
                'question2' => 'Questão 2',
                'question3' => 'Questão 3',
                'question4a' => 'Questão 4',
                'question4b' => 'Questão 4',
                'question4d' => 'Questão 4',
                'question4e' => 'Questão 4',
                'question4f' => 'Questão 4',
                'question5' => 'Questão 5',
                'question6' => 'Questão 6',
                'question7' => 'Questão 7',
                'question8' => 'Questão 8',
                'question9' => 'Questão 9',
                'question10' => 'Questão 10',
                'question11' => 'Questão 11',
                'question12' => 'Questão 12',
                'question13' => 'Questão 13',
                'question14a' => 'Questão 14',
                'question14b' => 'Questão 14',
            ];
            Validator::requireValidator($fields, $data);
            $quiz = new Quiz1();
            $quiz->setUser($user)
                ->setObs($data['question15'])
                ->setQuestion1($data['question1'])
                ->setQuestion2($data['question2'])
                ->setQuestion3($data['question3'])
                ->setQuestion4a($data['question4a'])
                ->setQuestion4b($data['question4b'])
                ->setQuestion4c($data['question4c'])
                ->setQuestion4d($data['question4d'])
                ->setQuestion4e($data['question4e'])
                ->setQuestion4f($data['question4f'])
                ->setQuestion5($data['question5'])
                ->setQuestion6($data['question6'])
                ->setQuestion7($data['question7'])
                ->setQuestion8($data['question8'])
                ->setQuestion9($data['question9'])
                ->setQuestion10($data['question10'])
                ->setQuestion11($data['question11'])
                ->setQuestion12($data['question12'])
                ->setQuestion13($data['question13'])
                ->setQuestion14a($data['question14a'])
                ->setQuestion14b($data['question14b']);
            $this->em->getRepository(Quiz1::class)->save($quiz);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Formulário enviado com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }


}
