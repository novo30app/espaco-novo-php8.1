<?php

namespace App\Controllers;
use App\Helpers\Validator;
use App\Helpers\Utils;
use App\Models\Entities\User;
use App\Models\Entities\Address;
use App\Models\Entities\Transaction;
use App\Services\IuguService;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class ChatBotController extends Controller
{
    public function verifyCpf(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $fields = ['cpf' => 'CPF'];
            Validator::requireValidator($fields, $data);
            Validator::validateCPF($data['cpf']);
            $cpf = Utils::formatCpf($data['cpf']);
            $register = $this->em->getRepository(User::class)->findOneBy(['cpf' => $cpf]);
            if(!$register) throw new \Exception("CPF não encontrado!");
            return $response->withJson([
                'status' => 'ok',
                'message' => $register->getFirstName(),
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function searchTransaction(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            $fields = ['cpf' => 'CPF'];
            Validator::requireValidator($fields, $data);
            Validator::validateCPF($data['cpf']);
            $cpf = Utils::formatCpf($data['cpf']);
            $user = $this->em->getRepository(User::class)->findOneBy(['cpf' => $cpf]);
            if(!$user) throw new \Exception("CPF não encontrado!");
            $transaction = $this->em->getRepository(Transaction::class)->lastTransactionChatbot($user);
            if(!$transaction){
                $message = "Não encontrei nenhuma contribuição em aberto<br>
                            O NOVO agradece sua colaboração!";
            } else {
                if($transaction->getGatewayPagamento() == 2){
                    $today = new \DateTime();
                    $address = $this->iuguAddress($this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()]));
                    if(!$address) throw new \Exception("Endereço não encontrado!");
                    $slip = IuguService::slip($user->getEmail(), $user->getCpf(), $user->getName(), $address, $transaction->getValor(), $transaction->getOriginTypeString());
                    $transaction->setDataVencimento($today->add(new \DateInterval('P5D')))
                                ->setFormaPagamento(2)
                                ->setGatewayPagamento(1)
                                ->setUrl($slip['bank_slip']['bank_slip_pdf_url'])
                                ->setStatus('pending')
                                ->setCodigoBarras($slip['bank_slip']['digitable_line'])
                                ->setInvoiceId($slip['id'])
                                ->setObs('chatbot');
                    $this->em->getRepository(Transaction::class)->save($transaction);
                }
                $message = "Encontrei uma contribuição de filiação em aberto, aqui em abaixo vou mandar os dados dela para pagamento<br>
                            Link do Boleto: {$transaction->getUrl()}<br>
                            Código de Barras: {$transaction->getCodigoBarras()}";
            }
            return $response->withJson([
                'status' => 'ok',
                'message' => $message,
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }
}