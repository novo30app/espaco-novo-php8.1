<?php

namespace App\Controllers;

use App\Helpers\Utils;
use App\Helpers\Validator;
use App\Models\Commons\Entities\BlingTokens;
use App\Models\Commons\Entities\LibertasCourse;
use App\Models\Commons\Entities\LibertasCourseBuy;
use App\Models\Commons\Entities\LibertasCourseCoupon;
use App\Models\Commons\Entities\UserIuguLibertas;
use App\Models\Entities\Address;
use App\Models\Entities\City;
use App\Models\Entities\CivilState;
use App\Models\Entities\CommunicationConsent;
use App\Models\Entities\Directory;
use App\Models\Entities\Gender;
use App\Models\Entities\Logs;
use App\Models\Entities\PersonIugu;
use App\Models\Entities\Schooling;
use App\Models\Entities\Transaction;
use App\Models\Entities\User;
use App\Services\Auth;
use App\Services\BlingLibertas;
use App\Services\Email;
use App\Services\EmailLibertas;
use App\Services\GoogleService;
use App\Services\Hubspot;
use App\Services\IuguLibertasService;
use App\Services\IuguService;
use App\Services\Libertas;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

class LibertasCoursesController extends Controller
{
    public function coursesIndex(Request $request, Response $response)
    {
        $user = $this->getLogged();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'libertas-courses/courses.phtml', 'section' => 'courses',
            'user' => $user, 'subMenu' => 'libertasCourses']);
    }

    public function coursesView(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $id = $request->getAttribute('id');
        $course = $this->em->getRepository(LibertasCourse::class)->find($id ?? 0);
        $buy = $this->em->getRepository(LibertasCourseBuy::class)->findOneBy(['user' => $user, 'course' => $course, 'status' => 1]);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'libertas-courses/buy.phtml', 'section' => 'courses',
            'user' => $user, 'course' => $course, 'subMenu' => 'libertasCourses', 'buy' => $buy]);
    }

    public function getCoupon(Request $request, Response $response)
    {
        $course = $request->getAttribute('course');
        $code = $request->getAttribute('code');
        $coupon = $this->em->getRepository(LibertasCourseCoupon::class)->findValidCoupon($course, $code)[0] ?? null;
        $discount = 0;
        if ($coupon) $discount = $coupon->getValue();
        return $response->withJson([
            'status' => 'ok',
            'discount' => $discount
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }


    public function buysIndex(Request $request, Response $response)
    {
//        IuguLibertasService::chargeBack($this->em->getRepository(LibertasCourseBuy::class)->find(5)->getInvoiceId());
        $user = $this->getLogged();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'libertas-courses/buys.phtml', 'section' => 'buys',
            'user' => $user, 'subMenu' => 'libertasCourses']);
    }

    public function coursesList(Request $request, Response $response)
    {
        $index = $request->getQueryParam('index');
        $filter = $request->getQueryParams();
        $filter['active'] = 1;
        $limit = $request->getQueryParam('limit');
        $docs = $this->em->getRepository(LibertasCourse::class)->list($filter, $limit, $index * $limit);
        $total = $this->em->getRepository(LibertasCourse::class)->listTotal($filter);
        $partial = ($index * $limit) + sizeof($docs);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $docs,
            'total' => (int)$total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function buysList(Request $request, Response $response)
    {
        $user = $this->getLoggedApi();
        $index = $request->getQueryParam('index');
        $filter = $request->getQueryParams();
        $filter['user'] = $user->getId();
        $limit = $request->getQueryParam('limit');
        $docs = $this->em->getRepository(LibertasCourseBuy::class)->list($filter, $limit, $index * $limit);
        $total = $this->em->getRepository(LibertasCourseBuy::class)->listTotal($filter);
        $partial = ($index * $limit) + sizeof($docs);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $docs,
            'total' => (int)$total,
            'partial' => $partial,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function buy(Request $request, Response $response, ?User $user = null)
    {
        try {
            $msg = "Compra realizada com sucesso.";
            if (!$user) $user = $this->getLoggedApi();
            else $msg = "Compra Concluída - Você receberá um e-mail para prosseguir com o acesso ao curso!";
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            $discount = 0;
            $course = $this->em->getRepository(LibertasCourse::class)->find($data['courseId']);
            $coupon = $this->em->getRepository(LibertasCourseCoupon::class)->findValidCoupon($data['courseId'], $data['coupon'])[0] ?? null;
            if ($coupon) $discount = $coupon->getValue();
            $total = $course->getPrice() - $discount;
            if ($total < 0) $total = 0;
            $url = '';
            $buy = new LibertasCourseBuy();
            $buy->setDueDate(new \DateTime())
                ->setPrice($total)
                ->setUser($user)
                ->setCourse($course)
                ->setDescription("{$course->getTitle()} - Parcela 1/1")
                ->setPaymentMethod($data['paymentMethod'])
                ->setCoupon($coupon);
            if ($total == 0) {
                $buy->setPayDay(new \DateTime())
                    ->setStatus(1);
                $login = $this->matricularMoodle($user, $course);
            } elseif ($data['paymentMethod'] == 1) { // cartao de credito
                $userIugu = $this->em->getRepository(UserIuguLibertas::class)->findOneBy(['user' => $user]);
                if (!$userIugu) {
                    $customerId = IuguLibertasService::createCliente($user->getName(), $user->getEmail());
                    $userIugu = new UserIuguLibertas();
                    $userIugu->setUser($user)
                        ->setCustomerId($customerId);
                    $userIugu = $this->em->getRepository(UserIuguLibertas::class)->save($userIugu);
                }
                $customerId = $userIugu->getCustomerId();
                if (!$customerId) throw new \Exception('Falha ao cadastrar usuário no no IUGU.');
                $transactionIugu = IuguLibertasService::chargeCredicCard($userIugu, $data['token'], $total, $course->getTitle(), 1);
                $buy->setPayDay(new \DateTime())
                    ->setStatus(1)
                    ->setInvoiceId($transactionIugu['invoice_id'])
                    ->setExternalReference($transactionIugu['token']);
                $login = $this->matricularMoodle($user, $course);
            } else { // boleto ou pix
                $slip = IuguLibertasService::slip($user->getEmail(), $user->getCpf(), $user->getName(), $total, $buy->getDescription());
                $slip['bank_slip']['barcode'] = str_replace('barcode', 'pdf', $slip['bank_slip']['barcode']);
                if (!isset($slip['id']) || $slip['id'] == null) throw new \Exception('Falha ao gerar cobrança no IUGU.');
                $url = $slip['bank_slip']['bank_slip_pdf_url'];
                $buy->setInvoiceId($slip['id'])
                    ->setUrl($url)
                    ->setBarCode($slip['bank_slip']['digitable_line'])
                    ->setQrCodeText($slip['pix']['qrcode_text'])
                    ->setQrCode($slip['pix']['qrcode']);
                $msg = "Cobrança gerada com sucesso. <br>
                        <img src=\"{$slip['pix']['qrcode']}\"
                         style=\"max-width: 300px\" class=\"img-fluid\"> <br>
                        <span class='btn btn-success' data-clipboard-text='{$slip['pix']['qrcode_text']}'>PIX Copia e Cola<i
                                class='fa fa-qrcode'></i></span>
                        <a class='btn btn-info' href='{$url}' target='_blank'>Visualizar a cobrança <i class='fa fa-file-pdf-o'></i></a>";
            }
            $this->em->getRepository(LibertasCourseBuy::class)->save($buy);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => $msg,
                'url' => $url,
                'loginUrl' => $login['link'] ?? '',
                'msgError' => $login['errorMessage'] ?? '',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }


    public function publicBuy(Request $request, Response $response)
    {

        try {
            $data = (array)$request->getParams();
            $date = new \DateTime();
            $fields = [
                'name' => 'Nome',
                'email' => 'E-mail',
                'emailConfirm' => 'Confirme o E-mail',
            ];
            $data['cpf'] = Utils::onlyNumbers($data['cpf']);
            Validator::validateCPF($data['cpf']);
            Validator::requireValidator($fields, $data);
            if ($data['email'] != $data['emailConfirm']) throw new \Exception('Os e-mails digitados são diferentes');
            $user = $this->em->getRepository(User::class)->findOneBy(['cpf' => Utils::formatCpf($data['cpf'])]);
            if (!$user) {
                $email = $this->em->getRepository(User::class)->findOneBy(['email' => $data['email']]);
                if ($email) throw new \Exception('E-mail já está vinculado a outro CPF');
                Validator::validatePassword2($data);
                $user = new User();
                $user->setFiliado(0)
                    ->setStatus(0)
                    ->setName($data['name'])
                    ->setCpf(Utils::formatCpf($data['cpf']))
                    ->setEmail($data['email'])
                    ->setTermoAceite0(1)
                    ->setTermoAceite1(1)
                    ->setTermoAceite2(1)
                    ->setTermoAceite3(1)
                    ->setDataCriacao($date)
                    ->setEstadoCivil($this->em->getReference(CivilState::class, 6))
                    ->setGenero($this->em->getReference(Gender::class, 3))
                    ->setEscolaridade($this->em->getReference(Schooling::class, 7))
                    ->setOpcaoReligiosa(11)
                    ->setIp(Utils::getAcessData()['ip'])
                    ->setUltimaAtualizacao($date)
                    ->setPassword(md5($data['password']));
                $this->em->getRepository(User::class)->save($user);
                Auth::charge($user->getName(), $user->getEmail(), $user->getPassword());
                $user = $this->em->getRepository(User::class)->save($user);
                $this->registerHistoric($user, 1);
                $this->confirmacaoCompraLibertas($user, 'Compra de curso - Ativação da Conta');
            } else {
                $msg = "<p>{$user->getName()}, agradecemos a compra de mais um curso do Instituto Libertas.</p>
                <p>Para acessar o curso comprado <a href='https://espaco-novo.novo.org.br/moodle/inicio' target='_blank'>Clique aqui!</a></p>
                <p>Caso tenha esquecido a sua senha, vá em <a href='https://espaco-novo.novo.org.br/recuperar-senha' target='_blank'>Recuperar Senha!</a></p>
                <p>Caso tenha alguma dúvida entre em contato conosco pelo e-mail: contato@institutolibertas.org.br</p>
                <p>Equipe Instituto Libertas</p>";
                EmailLibertas::send($user->getEmail(), $user->getName(), 'Compra de curso', $msg);
            }
            return $this->buy($request, $response, $user);
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }


    public function generateNfs()
    {
        $transactions = $this->em->getRepository(LibertasCourseBuy::class)->findBy(['status' => 1, 'nf' => 0]);
        $acessToken = $this->em->getRepository(BlingTokens::class)->findOneBy(['day' => new \DateTime()]);
        if (!$acessToken) {
            $refreshToken = $this->em->getRepository(BlingTokens::class)->findOneBy([], ['id' => 'DESC']);
            $refreshToken = BlingLibertas::updateAcessToken($refreshToken->getRefreshToken());
            $acessToken = new BlingTokens();
            $acessToken->setRefreshToken($refreshToken['refresh_token'])
                ->setAccessToken($refreshToken['access_token']);
            $this->em->persist($acessToken);
            $this->em->flush();
        }
        foreach ($transactions as $transaction) {
            $address = $this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $transaction->getUser()->getId()]);
            if (!$address) continue;
            $cidade = $address->getCidade();
            $uf = $address->getEstado();
            if ($address->getTbCidadeId()) {
                $city = $this->em->getRepository(City::class)->find($address->getTbCidadeId());
                if ($city) {
                    $cidade = $city->getCidade();
                    $uf = $city->getState()->getSigla();
                }
            }
            $nf = BlingLibertas::nf($acessToken->getAccessToken(), $transaction, $address->getEndereco() ?? 'Rua Lorem Ipsum', $uf, $cidade);
            if ($nf['error']) {
                Email::send('rodrigo@lifecode.dev', 'Rodrigo', "Erro de emissão de nota fiscal Bling - {$nf['error']['type']}", $nf['error']['description']);
            }
            if ($nf['data']['id']) {
                $transaction->setNf(true);
                $this->em->getRepository(LibertasCourseBuy::class)->save($transaction);
            }
        }

    }


}