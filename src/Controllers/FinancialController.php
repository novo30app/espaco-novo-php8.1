<?php

namespace App\Controllers;

use App\Models\Commons\Entities\LibertasCourseBuy;
use App\Models\Entities\City;
use App\Models\Entities\State;
use App\Services\Payload;
use App\Services\Pix;
use DateTime;
use App\Helpers\Session;
use Mpdf\QrCode\QrCode;
use Mpdf\QrCode\Output;
use App\Helpers\Validator;
use App\Helpers\Messages;
use App\Helpers\Utils;
use App\Models\Entities\Address;
use App\Models\Entities\PersonRede;
use App\Models\Entities\DirectoryGatway;
use App\Models\Entities\PersonIugu;
use App\Models\Entities\Transaction;
use App\Models\Entities\PersonCreditCard;
use App\Models\Entities\PersonSignature;
use App\Models\Entities\PersonSignatureInstallments;
use App\Models\Entities\Directory;
use App\Models\Entities\User;
use App\Models\Entities\Plan;
use App\Models\Entities\CivilState;
use App\Models\Entities\Gender;
use App\Models\Entities\Schooling;
use App\Services\GoogleService;
use App\Services\IuguService;
use App\Services\MaxiPago;
use App\Services\Hubspot;
use App\Services\Mailchimp;
use App\Services\Email;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

class FinancialController extends Controller
{

    public function checkPix()
    {
        $environment = $this->getConfigs();
        $header = apache_request_headers();
        if (!isset($header['token']) || $header['token'] != $environment['checkPix']) die('token invalido');
        $transactions = $this->em->getRepository(Transaction::class)->checkPix();
        foreach ($transactions as $transaction) {
            $pix = Pix::find($transaction);
            if ('CONCLUIDA' == $pix['status']) {
                $transaction->setDataPago(new DateTime())
                    ->setStatus('paid');
                $this->em->getRepository(Transaction::class)->save($transaction);
            }
        }
    }

    public function extract(Request $request, Response $response)
    {
        $user = $this->getLogged();
        if ($user->isAffiliated() && !$user->getUltimaAtualizacao()) $this->redirectExternal($this->baseUrl);
        $extract = $this->em->getRepository(Transaction::class)->findBy(['tbPessoaId' => $user], ['dataCriacao' => 'DESC']);
        $cards = $this->em->getRepository(PersonCreditCard::class)->findBy(['user' => $user, 'visivel' => 1, 'gatewayPagamento' => 1]);
        $reportIncome = $this->em->getRepository(Transaction::class)->getReportIncome($user);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/extract.phtml', 'section' => 'extract', 'extract' => $extract,
            'user' => $user, 'subMenu' => 'myFinancial', 'cards' => $cards, 'title' => 'Extrato', 'reportIncome' => $reportIncome]);
    }

    public function donation(Request $request, Response $response)
    {
        $user = $this->getLogged();
        if ($user->isAffiliated() && !$user->getUltimaAtualizacao()) $this->redirectExternal($this->baseUrl);
        $cards = $this->em->getRepository(PersonCreditCard::class)->findBy(['user' => $user, 'visivel' => 1, 'gatewayPagamento' => 1]);
        $directories = $this->em->getRepository(Directory::class)->findBy(['ativo' => 's'], ['nome' => 'ASC']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/donation.phtml', 'section' => 'donation',
            'user' => $user, 'subMenu' => 'myFinancial', 'cards' => $cards, 'title' => 'Doação', 'directories' => $directories]);
    }

    public function chargeTransaction(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            $date = new \DateTime();
            $fields = [
                'paymentMethod' => 'Forma de Pagamento',
                'transaction' => 'Transação',
            ];
            if ($data['paymentMethod'] == 1) $fields['card'] = 'Cartão de crédito';
            Validator::requireValidator($fields, $data);
            $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['id' => $data['transaction'], 'tbPessoaId' => $user->getId()]);
            if (!$transaction) throw new \Exception("Transação inválida!");
            $keyIUGU = $this->getIuguTokenDirectory($transaction->getTbDiretorioId());
            if ($data['paymentMethod'] == 1) { // cartao de credito
                $card = $this->em->getRepository(PersonCreditCard::class)->find($data['card']);
                if (!$card) throw new \Exception("Cartão inválido");
                if ($card->getGatewayPagamento() == 2) {//rede
                    $personRede = $this->em->getRepository(PersonRede::class)->findOneBy(['user' => $user->getId()], ['id' => 'DESC']);
                    $directoryGatway = null;
                    $transactionRede = MaxiPago::transaction($personRede, $card->getToken(), $transaction->getValor(), '', $directoryGatway, $user);
                    $invoiceId = $transactionRede['transactionID'];
                    $rederId = $transactionRede['orderId'];
                    $creditCardScheme = $transactionRede['creditCardScheme'];
                } else { // iugu
                    $rederId = $creditCardScheme = $invoiceId = '';
                    if ($transaction->getStatus() == 'pending' && $transaction->getGatewayPagamento() == 1) {
                        $charge = IuguService::chargeCreditCard($transaction->getInvoiceId(), $card->getToken(), 0, $keyIUGU['key']);
                        $invoiceId = $transaction->getInvoiceId();
                    } else {
                        $personIugu = $this->getCustomerIdIUGU($user);
                        $address = $this->iuguAddress($this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()]));
                        $charge = IuguService::charge($personIugu->getCustomerId(), $card->getToken(), $user->getEmail(), $user->getCpf(), $user->getName(), $address, $transaction->getValor(), $transaction->getOriginTypeString(), 0, $keyIUGU['key']);
                        $invoiceId = $charge['invoice_id'];
                    }
                }
                $transaction->setTokenCartaoCredito($card->getToken())
                    ->setFormaPagamento(1)
                    ->setGatewayPagamento($card->getGatewayPagamento())
                    ->setDataPago($date)
                    ->setStatus('paid')
                    ->setRedeId($rederId)
                    ->setUrl('')
                    ->setCodigoBarras('')
                    ->setInvoiceId($invoiceId)
                    ->setCreditCardFlag($creditCardScheme)
                    ->setNumeroRecibo($this->getReceiptDirectory($this->em->getReference(Directory::class, $transaction->getTbDiretorioId()), $transaction));
                $this->em->getRepository(Transaction::class)->save($transaction);
                $this->validLastTransaction($user, $transaction, 4);
                if ($user->getFiliado() == 1 || $user->getFiliado() == 3) {
                    $solicitation = $user->getFiliado() == 1 ? $user->getDataSolicitacaoFiliacao()->format('Y-m-d H:i:s') : $user->getDataSolicitacaoRefiliacao()->format('Y-m-d H:i:s');
                    if ($transaction->getOrigemTransacao() == 2 and $transaction->getDataCreation()->format('Y-m-d H:i:s') > $solicitation) {
                        $userChange = $this->em->getRepository(User::class)->findOneBy(['id' => $user->getId()]);
                        if ($userChange->getFiliado() == 1) {
                            $userChange->setFiliado(2);
                            //->setDataSolicitacaoFiliacao($date);
                            $type = 2;
                        } elseif ($userChange->getFiliado() == 3) {
                            $userChange->setFiliado(14);
                            //->setDataSolicitacaoRefiliacao($date);
                            $type = 5;
                        }
                        $this->em->getRepository(User::class)->save($userChange);
                        
                        /*-----Hubspot/Mailchimp-----*/
                        $this->updateStatusRegister($userChange);

                        $this->registerHistoric($userChange, $type);
                    }
                }
                $message = "Pagamento realizado com sucesso, o NOVO agradece sua contribuição!";
            } else { // boleto
                if ($transaction->getDataVencimento() >= new \Datetime()) {
                    $slip['bank_slip']['digitable_line'] = $transaction->getCodigoBarras();
                    $slip['bank_slip']['bank_slip_pdf_url'] = $transaction->getUrl();
                } else {
                    $address = $this->iuguAddress($this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()]));
                    $slip = IuguService::slip($user->getEmail(), $user->getCpf(), $user->getName(), $address, $transaction->getValor(), $transaction->getOriginTypeString(), 5, $keyIUGU['key']);
                    $transaction->setDataVencimento($date->add(new \DateInterval('P5D')))
                        ->setFormaPagamento(2)
                        ->setGatewayPagamento(1)
                        ->setUrl($slip['bank_slip']['bank_slip_pdf_url'])
                        ->setStatus('pending')
                        ->setCodigoBarras($slip['bank_slip']['digitable_line'])
                        ->setInvoiceId($slip['id']);
                    $this->em->getRepository(Transaction::class)->save($transaction);
                }
                $msg = Messages::chargeTransactionSplip($user, $slip);
                Email::send($user->getEmail(), $user->getName(), 'Partido NOVO - Pagamento de contribuição', $msg, null);
                $message = "Ótimo. Agora é só fazer o pagamento do boleto, utilizando o código abaixo:<br> 
                            Código de Barras: {$slip['bank_slip']['digitable_line']} <br> 
                            <a href='#'><span class='btn btn-primary' data-clipboard-text='{$slip['bank_slip']['digitable_line']}'>Copiar Código de Barras <i class='fa fa-barcode'></i></span></a>
                            <a class='btn btn-success' href='{$slip['bank_slip']['bank_slip_pdf_url']}' target='_blank'>Baixar o boleto <i class='fa fa-file-pdf-o'></i></a><br>
                            Atenção, seu boleto pode demorar até 2 horas para ser registrado, se não conseguir realizar o pagamento aguarde alguns instantes ou tente fazer via PIX.";
            }
            return $response->withJson([
                'paymentMethod' => $data['paymentMethod'],
                'status' => 'ok',
                'message' => $message
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function payDonation(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            $date = new \DateTime();
            $fields = [
                'directory' => 'Diretório',
                'paymentMethod' => 'Forma de Pagamento',
                'plan' => 'Plano de Pagamento',
                'value' => 'Valor',
            ];
            if ($data['paymentMethod'] == 1) $fields['card'] = 'Cartão de crédito';
            if ($data['value'] == 0) {
                $data['value2'] = str_replace('.', '', $data['value2']);
                $data['value2'] = str_replace(',', '.', $data['value2']);
                $data['value'] = $data['value2'];
            }
            Validator::requireValidator($fields, $data);
            if ($data['value'] < 11) throw new \Exception("Valor minimo para a doação é R$ 11,00");
            if (ENV == 'prod') GoogleService::recapchaEspacoNovo($data['g-recaptcha-response']);
            $directory = $this->em->getRepository(Directory::class)->find($data['directory']);
            $data['directory'] = $directory->getId();
            $data['flag'] ??= '';
            /* --------------- */
            if ($data['value'] > MAXVALUE && ENV == 'prod') {
                $directoryAccount = $this->em->getRepository(Directory::class)->getAccount($directory);
                throw new \Exception("Valor máximo para a doação via sistema é R$ " . number_format(MAXVALUE, 2, ',', '.') . ".<br> 
                                        Para doar um valor superior faça uma transferência bancaria:<br> 
                                        <b>Banco:</b> {$directoryAccount['bank']} <br>
                                        <b>Agência:</b> {$directoryAccount['agency']} <br>
                                        <b>Conta:</b> {$directoryAccount['account']} <br>
                                        <b>CNPJ:</b> {$directoryAccount['cnpj']} <br>");
            }
            $this->em->getRepository(Transaction::class)->validateDonation($user, $data['value'], $directory);
            /* ------- Pega chave IUGU -------- */
            $keyIUGU = null;
            $depositDirectory = 1;
            $iuguTokens = $this->getIuguTokenDirectory($directory->getId());
            if ($iuguTokens) {
                $keyIUGU = $iuguTokens['key'];
                $depositDirectory = $iuguTokens['deposit'];
            }
            /* --------------- */
            if ($data['paymentMethod'] == 1) {
                $subscription = null;
                $card = $this->em->getRepository(PersonCreditCard::class)->find($data['card']);
                $personIugu = $this->getCustomerIdIUGU($user);
                $address = $this->iuguAddress($this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()]));
                $charge = IuguService::charge($personIugu->getCustomerId(), $card->getToken(), $user->getEmail(), $user->getCpf(), $user->getName(),
                    $address, $data['value'], 'Doação - ' . $directory->getName(), 0, $keyIUGU);
                if ($data['plan'] == 1) $subscription = $this->createSubscription($user, $data, 1, $personIugu->getCustomerId(), $card->getToken(), 1)->getId();
                $transaction = $this->populateTransaction($user, $data['value'], $date, $date, $date, 1, 'paid', $personIugu->getCustomerId(), $data['plan'], 1, 1,
                    '', $card->getToken(), $charge['invoice_id'], '', $depositDirectory, $data['directory'], '', '', $subscription ?? '');
                $this->em->getRepository(Transaction::class)->save($transaction);
                return $response->withJson([
                    'status' => 'ok',
                    'message' => 'Doação realizada com sucesso',
                ], 201)
                    ->withHeader('Content-type', 'application/json');
            } elseif ($data['paymentMethod'] == 2) {
                $msg = $this->donationByBoleto($data, $user, $directory, $keyIUGU, $depositDirectory);
            } elseif ($data['paymentMethod'] == 3) {
                $msg = $this->donationByPix($data, $user, $directory, $keyIUGU, $depositDirectory);
            }
            return $response->withJson([
                'status' => 'ok',
                'message' => $msg,
            ], 201)
                ->withHeader('Content-type', 'application/json');

        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    private function donationByPix(array $data, User $user, Directory $directory, $keyIUGU, $directoryDeposit): string
    {
        $address = $this->iuguAddress($this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()]));
        $pix = IuguService::slip($user->getEmail(), $user->getCpf(), $user->getName(), $address, $data['value'], 'Doação - ' . $directory->getName(), 0, $keyIUGU);
        if (!$pix['pix']['qrcode_text']) throw new \Exception("Erro ao gerar QR Code Pix");
        if ($data['plan'] == 1) $subscription = $this->createSubscription($user, $data, 1, null, null, 1)->getId();
        $date = new \DateTime();
        $transaction = $this->populateTransaction($user, $data['value'], $date, $date, null, 11, 'pending', '', $data['plan'], 1, 1, '', '', $pix['id'], '',
            $directoryDeposit, $data['directory'], $pix['pix']['qrcode'], '', $subscription ?? '');
        $transaction = $this->em->getRepository(Transaction::class)->save($transaction);
        return "QR Code para doação gerado com sucesso. <br> 
                <span class='btn btn-primary' data-clipboard-text='{$pix['pix']['qrcode_text']}'>PIX Copia e Cola <i class='fa fa-qrcode'></i></span> <br>
                <img src=\"{$pix['pix']['qrcode']}\" style='width: 25%;'> <br>
                <b>O sistema pode levar até 24h para dar a baixa na doação.</b>";
    }

    private function donationByBoleto(array $data, User $user, Directory $directory, $keyIUGU, $directoryDeposit): string
    {
        if ($data['plan'] == 1) $subscription = $this->createSubscription($user, $data, 1, null, null, 1)->getId();
        $address = $this->iuguAddress($this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()]));
        $slip = IuguService::slip($user->getEmail(), $user->getCpf(), $user->getName(), $address, $data['value'], 'Doação - ' . $directory->getName(), 0, $keyIUGU);
        $date = new \DateTime();
        $transaction = $this->populateTransaction($user, $data['value'], $date, $date, null, 2, 'pending', '', $data['plan'], 1, 1, '', '', $slip['id'], '',
            $directoryDeposit, $data['directory'], $slip['bank_slip']['bank_slip_pdf_url'], $slip['bank_slip']['digitable_line'], $subscription ?? '');
        $this->em->getRepository(Transaction::class)->save($transaction);
        return "Boleto gerado com sucesso. <br> 
                                Código de Barras: {$slip['bank_slip']['digitable_line']} <br> 
                                <span class='btn btn-primary' data-clipboard-text='{$slip['bank_slip']['digitable_line']}'>Copiar Código de Barras <i class='fa fa-barcode'></i></span>
                                <a class='btn btn-success' href='{$slip['bank_slip']['bank_slip_pdf_url']}' target='_blank'>Baixar o boleto <i class='fa fa-file-pdf-o'></i></a><br>
                                Atenção, seu boleto pode demorar até 2 horas para ser registrado, se não conseguir realizar o pagamento aguarde alguns instantes ou tente fazer via PIX.";
    }

    public function cards(Request $request, Response $response)
    {
        $user = $this->getLogged();
        if ($user->isAffiliated() && !$user->getUltimaAtualizacao()) $this->redirectExternal($this->baseUrl);
        $name = explode(" ", strtolower($user->getName()));
        $name = str_replace(array(' ', 'à', 'á', 'â', 'ã', 'ä', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ù', 'Ú', 'Û', 'Ü', 'Ý'), array('_', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'N', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y'), $name[0]);

        // Verifica se há transações pendentes de pagamento por cartão de crédito
        $transactionPending = $this->getTransactionPending($user);
                
        return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/cards.phtml', 'section' => 'cards', 'user' => 
            $user, 'name' => $name, 'subMenu' => 'myFinancial', 'title' => 'Meus Cartões', 'transactionPending' => $transactionPending]);
    }

    public function qrcode(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('id');
            $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['id' => $id, 'tbPessoaId' => $user->getId(), 'status' => ['pending', 'expired']]);
            if (!$transaction) $this->redirect();
            $address = $this->iuguAddress($this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()]));
            $pix['pix']['qrcode_text'] = '1234';
            $pix = IuguService::slip($user->getEmail(), $user->getCpf(), $user->getName(), $address, $transaction->getValor(), $transaction->getOriginTypeString());
            if (!$pix['pix']['qrcode_text']) throw new \Exception("Erro ao gerar QR Code Pix");
            $transaction->setFormaPagamento(11)
                ->setDataVencimento(new \DateTime($pix['due_date']))
                ->setCodigoBarras($pix['digitable_line'])
                ->setGatewayPagamento(1)
                ->setQrCode($pix['pix']['qrcode_text'])
                ->setUrl($slip['bank_slip']['bank_slip_pdf_url'])
                ->setInvoiceId($pix['id'])
                ->setStatus('pending');
            $transaction = $this->em->getRepository(Transaction::class)->save($transaction);
            $message = "QR Code para pagamento gerado com sucesso. <br> 
                    <span class='btn btn-primary' onclick='copyQrCodeText()'>PIX Copia e Cola <i class='fa fa-qrcode'></i></span> <br>
                    <img src=\"{$pix['pix']['qrcode']}\" style='width: 100%; margin-top: 15px;'> <br>
                    <b>O sistema pode levar até 24h para dar a baixa na doação.</b>";
            return $response->withJson([
                'status' => 'ok',
                'message' => $message,
                'qrcodeText' => $pix['pix']['qrcode_text']
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function subscriptions(Request $request, Response $response)
    {
        $user = $this->getLogged();
        if ($user->isAffiliated() && !$user->getUltimaAtualizacao()) $this->redirectExternal($this->baseUrl);
        $cards = $this->em->getRepository(PersonCreditCard::class)->findBy(['user' => $user, 'visivel' => 1, 'gatewayPagamento' => 1]);
        $affiliation = $request->getAttribute('route')->getArgument('affiliation');
        $affiliation = $affiliation == 1;
        $subscriptionAffiliation = $this->em->getRepository(PersonSignature::class)->findOneBy(['tbPessoaId' => $user, 'origin' => 2, 'status' => 'created']);
        $directories = $this->em->getRepository(Directory::class)->findBy(['ativo' => 's'], ['nome' => 'ASC']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/subscriptions.phtml', 'section' => 'subscriptions',
            'user' => $user, 'subMenu' => 'myFinancial', 'title' => 'Assinaturas', 'cards' => $cards,
            'affiliation' => $affiliation, 'directories' => $directories, 'subscriptionAffiliation' => $subscriptionAffiliation]);
    }

    public function apiSubscriptions(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $id = $request->getAttribute('route')->getArgument('id');
        if ($id) {
            $subscription = $this->em->getRepository(PersonSignature::class)->findOneBy(['tbPessoaId' => $user, 'id' => $id]);
            if (!$subscription) {
                throw new \Exception('Assinatura não encontrada');
            }
            $subscriptionsArray = $this->em->getRepository(PersonSignature::class)->find($id)->toArray();
        } else {
            $subscriptionsArray = [];
            $subscriptions = $this->em->getRepository(PersonSignature::class)->findBy(['tbPessoaId' => $user]);
            foreach ($subscriptions as $subscription) {
                $subscriptionsArray[] = $subscription->toArray();
            }
        }

        $arr = [];
        foreach($subscriptionsArray as $subscriptionsArray) {
            $installments = $this->em->getRepository(PersonSignatureInstallments::class)->findBy(['signature' => $subscriptionsArray['id']]);
            $installmentsArray = [];
            foreach ($installments as $i) {
                $installmentsArray[] = $i->getDueDate()->format('d/m/Y') . "-";
            }
            $subscriptionsArray['installments'] = implode($installmentsArray);
            $arr[] = $subscriptionsArray;
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $arr,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function stopSubscription(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('id');
            $subscription = $this->em->getRepository(PersonSignature::class)->findOneBy(['tbPessoaId' => $user, 'id' => $id]);
            if (!$subscription) {
                throw new \Exception('Assinatura não encontrada');
            }
            if ($subscription->getOrigemTransacao() == 2) {
                throw new \Exception('Você não pode pausar a assinatura de filiação.');
            }
            $subscription->setStatus('paused');
            $this->em->getRepository(PersonSignature::class)->save($subscription);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Assinatura suspensa com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }

    }

    public function activateSubscription(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('id');
            $subscription = $this->em->getRepository(PersonSignature::class)->findOneBy(['tbPessoaId' => $user, 'id' => $id]);
            if (!$subscription) {
                throw new \Exception('Assinatura não encontrada');
            }
            if ($subscription->getOrigemTransacao() == 2) {
                throw new \Exception('Você não pode reativar a assinatura de filiação.');
            }
            $subscription->setStatus('created');
            $this->em->getRepository(PersonSignature::class)->save($subscription);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Assinatura ativada com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function saveSubscription(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            $directory = isset($data['directory']) ? $data['directory'] : 1;
            $fields = [
                'value' => 'Valor',
                'frequency' => 'Plano',
                'paymentMethod' => 'Forma de Pagamento',
                'day' => 'Dia de cobrança',
            ];
            Validator::requireValidator($fields, $data);
            $data['value'] = str_replace('.', '', $data['value']);
            $data['value'] = str_replace(',', '.', $data['value']);
            $data['value'] = str_replace('R$', '', $data['value']);
            $data['value'] = (float)trim($data['value']);
            $user = $this->getLogged();
            $subscription = $this->em->getRepository(PersonSignature::class)->findOneBy(['tbPessoaId' => $user, 'id' => $data['subscriptionId']]);
            if (!$subscription) throw new \Exception('Assinatura não encontrada');
            Validator::validateSubscription($data, $subscription->getOrigemTransacao());
            $today = new \DateTime();
            $dateSubscription = explode("-", date_format($subscription->getDataGeraFatura(), "Y-m-d"));
            if ($data['frequency'] == 12) {
                $nextCharge = new \Datetime ($dateSubscription[0] . '-' . $dateSubscription[1] . '-' . $data['day']);
            } else if ($data['frequency'] == 6) {
                $dateSubscription = explode("-", date_format($subscription->getDataGeraFatura(), "Y-m-d"));
                $nextCharge = new \Datetime ($dateSubscription[0] . '-' . $dateSubscription[1] . '-' . $data['day']);
            } else {
                if ($subscription->getDataGeraFatura()->format('m') == $today->format('m')) {
                    throw new \Exception('Você só pode realizar esta alteração após o pagamento da contribuição do mês vigente.');
                }
                $nextCharge = new \DateTime($dateSubscription[0] . '-' . $dateSubscription[1] . '-' . $data['day']);
                //if ($data['day'] <= $today->format('d')) {
                //    $nextCharge->add(new \DateInterval("P{$data['frequency']}M"));
                //}
            }
            $gateway = 1;
            if ($data['paymentMethod'] == 1) {
                $card = $this->em->getRepository(PersonCreditCard::class)->findOneBy(['id' => $data['card']]);
                if (!$card) throw new \Exception('Cartão não encontrado!');
                $gateway = $card->getGatewayPagamento();
            }
            if ($gateway == 1) {
                $customer = $this->getCustomerIdIUGU($user);
            } else {
                $customer = $this->em->getRepository(PersonRede::class)->findOneBy(['user' => $user], ['id' => 'DESC']);
            }
            $customerId = $customer->getCustomerId();
            $subscription->setTbPessoaId($user->getId())
                ->setValor($data['value'])
                ->setPaymentForm($data['paymentMethod'])
                ->setCustomerId($customerId)
                ->setGatewayPagamento($gateway)
                ->setGatewayStatus(1)
                ->setPeriodicidade($data['frequency'])
                ->setDiaCiclo($data['day'])
                ->setMesCiclo($subscription->getDataCriacao()->format('m'))
                ->setDataGeraFatura($nextCharge)
                ->setTbDiretorioOrigem($directory);
            if ($data['paymentMethod'] == 1) $subscription->setTokenCartaoCredito($card->getToken());
            $this->em->getRepository(PersonSignature::class)->save($subscription);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Assinatura cadastrada com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function apiCards(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $id = $request->getAttribute('route')->getArgument('id');
        if ($id) {
            $cardsArray = $this->em->getRepository(PersonCreditCard::class)->find($id)->toArray();
        } else {
            $cardsArray = [];
            $cards = $this->em->getRepository(PersonCreditCard::class)->findBy(['user' => $user, 'visivel' => 1, 'gatewayPagamento' => 1], ['id' => 'DESC']);
            foreach ($cards as $subscription) {
                $cardsArray[] = $subscription->toArray();
            }
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $cardsArray,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function activateCard(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('id');
            $card = $this->em->getRepository(PersonCreditCard::class)->findOneBy(['user' => $user, 'id' => $id]);
            if (!$card) {
                throw new \Exception('Cartão não encontrado');
            }
            $card->setStatus(1);
            $this->em->getRepository(PersonCreditCard::class)->save($card);
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Cartão habilitado com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function stopCard(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $user = $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('id');
            $card = $this->em->getRepository(PersonCreditCard::class)->findOneBy(['user' => $user, 'id' => $id]);
            $card->setStatus(0)->setPrincipal(0);
            $this->em->getRepository(PersonCreditCard::class)->save($card);
            $subscription = $this->em->getRepository(PersonSignature::class)->findOneBy(
                ['tbPessoaId' => $user->getId(), 'status' => 'created', 'tokenCartaoCredito' => $card->getToken(), 'periodicidade' => [1, 6, 12], 'formaPagamento' => 1]);
            if ($subscription) {
                $subscription->setGatewayPagamento(1)->setPaymentForm(2);
                $this->em->getRepository(PersonSignature::class)->save($subscription);
            }
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Cartão removido com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }

    }

    public function saveCard(Request $request, Response $response)
    {
        try {
            //$this->em->beginTransaction();
            $data = (array)$request->getParams();
            $fields = [
                'cvv' => 'Código de verificação',
                'expiringDate' => 'Válidade do cartão',
                'number' => 'Número do cartão',
                'name' => 'Nome como está no cartão',
                'international' => 'Cartão emitido no Brasil?'
            ];
            Validator::requireValidator($fields, $data);
            $data['flag'] ??= '';
            $user = $this->getLogged();
            $this->disabledCards($user);
            $card = $this->createCardApiIugu($user, $data['number'], $data['name'], $data['cvv'], $data['expiringDate'], $data['flag'], $data['international']);
            $subscriptions = $this->em->getRepository(PersonSignature::class)->findBy(['tbPessoaId' => $user->getId(), 'status' => 'created']);
            foreach ($subscriptions as $subscription) {
                $subscription->setPaymentForm(1)
                    ->setGatewayPagamento(1)
                    ->setCustomerId($card['customer'])
                    ->setTokenCartaoCredito($card['paymentToken']);
                if ($subscription->getOrigemTransacao() == 2 && $user->getExemption() == 1) {
                    $subscription->setPeriodicidade(1)
                        ->setValor(MINMONTHLY);
                }
                $this->em->getRepository(PersonSignature::class)->save($subscription);
            }
            $this->payLastOpened($user, $card['paymentToken']);
            $user->setIrpf(0)->setExemption(0);
            $this->em->getRepository(User::class)->save($user);
            
            /*-----Hubspot/Mailchimp-----*/
            $this->updateStatusRegister($user);

            //$this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Cartão cadastrado com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function receipt(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $id = $request->getAttribute('route')->getArgument('id');
        $transaction = $this->em->getRepository(Transaction::class)->find($id);
        if (!$transaction || $transaction->getTbPessoaId() != $user->getId()) {
            Session::set('errorMsg', 'Você não tem permissão para acessar esse conteúdo');
            $this->redirect();
        }
        $directory = $this->em->getRepository(Directory::class)->findOneBy(['id' => $transaction->getTbDiretorioId()]);
        $title = in_array($transaction->getOrigemTransacao(), [2, 15]) ?
            'RECIBO DE CONTRIBUIÇÂO DE FILIAÇÃO' : 'RECIBO DE DOAÇAO - VIA DOADOR';
        return $this->renderer->render($response, 'financial/receipts/receipt.phtml', ['section' => 'extract',
            'transaction' => $transaction, 'directory' => $directory, 'user' => $user, 'subMenu' => 'myFinancial',
            'title' => $title]);
    }

    public function anticipation(Request $request, Response $response)
    {
        $user = $this->getLogged();
        if (!$user->isAffiliated()) $this->redirectByPermissions();
        $lastTransaction = $this->em->getRepository(Transaction::class)->lastTransaction($user)[1];
        $cards = $this->em->getRepository(PersonCreditCard::class)->findBy(['user' => $user, 'visivel' => 1, 'gatewayPagamento' => 1]);
        $subscriptionAffiliation = $this->em->getRepository(PersonSignature::class)->findOneBy(['tbPessoaId' => $user, 'origin' => 2]);
        $lastTransactionAntecipate = $this->em->getRepository(Transaction::class)->findOneBy(['tbPessoaId' => $user, 'obs' => 'Filiacao antecipada', 'status' => 'pending']) ?: null;
        return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/anticipation.phtml', 'section' => 'anticipation', 'user' => $user, 'title' => 'Antecipação', 'lastTransaction' => $lastTransaction,
            'lastTransactionAntecipate' => $lastTransactionAntecipate, 'subMenu' => 'myFinancial', 'subscriptionAffiliation' => $subscriptionAffiliation, 'cards' => $cards]);
    }

    public function anticipationGenerate(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $user = $this->getLogged();
            $date = new \DateTime();
            $data = (array)$request->getParams();
            $fields = [
                'paymentMethod' => 'Forma de Pagamento',
                'parcel' => 'Parcelas',
            ];
            Validator::requireValidator($fields, $data);
            $subscription = $this->em->getRepository(PersonSignature::class)->findOneBy(['tbPessoaId' => $user, 'origin' => 2]);
            $value = $subscription->getValor() * $data['parcel'];
            if ($value > MAXVALUE) throw new \Exception("Valor máximo para antecipação diária é R$ " . number_format(MAXVALUE, 2, ',', '.') . ".");
            $address = $this->iuguAddress($this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()]));
            if ($data['paymentMethod'] == 1) {
                $card = $this->em->getRepository(PersonCreditCard::class)->find($data['card']);
                $url = $barCode = '';
                if ($card->getGatewayPagamento() == 2) {//rede
                    $personRede = $this->em->getRepository(PersonRede::class)->findOneBy(['user' => $user->getId()], ['id' => 'DESC']);
                    $directoryGatway = $this->em->getRepository(DirectoryGatway::class)->findOneBy(['tbDiretorioId' => 1, 'formaPagamento' => 1, 'gatwayPagamento' => 2]);
                    $gateway = 2;
                    $transactionRede = MaxiPago::transaction($personRede, $card->getToken(), $value, 'Filiacao - antecipada', $directoryGatway, $user);
                    $invoiceId = $transactionRede['transactionID'];
                    $rederId = $transactionRede['orderId'];
                    $status = 'paid';
                } else { // iugu
                    $personIugu = $this->getCustomerIdIUGU($user);
                    $charge = IuguService::charge($personIugu->getCustomerId(), $card->getToken(), $user->getEmail(), $user->getCpf(), $user->getName(), $address, $value, 'Filiação - Antecipação');
                    $gateway = 1;
                    $invoiceId = $charge['invoice_id'];
                    $rederId = '';
                    $status = 'paid';
                }
                $message = "Antecipação realizada com sucesso, o NOVO agradece sua contribuição!";
                $datePaid = $date;
                $subscription = $this->em->getRepository(PersonSignature::class)->findOneBy(['tbPessoaId' => $user, 'origin' => 2, 'status' => 'created']);
                $type = $subscription->getFormaPagamento() == 1 ? 4 : 1;
                $this->updatePaymentStatus($user, $type);
            } else {
                $slip = IuguService::slip($user->getEmail(), $user->getCpf(), $user->getName(), $address, $value, 'Filiação antecipada', 1);
                $message = "Boleto gerado com sucesso.<br> 
                            Código de Barras: {$slip['bank_slip']['digitable_line']}<br> 
                            <span class='btn btn-primary' data-clipboard-text='{$slip['bank_slip']['digitable_line']}'>Copiar Código de Barras <i class='fa fa-barcode'></i></span>
                            <a class='btn btn-success' href='{$slip['bank_slip']['bank_slip_pdf_url']}' target='_blank'>Baixar o boleto <i class='fa fa-file-pdf-o'></i></a><br>
                            Atenção, seu boleto pode demorar até 2 horas para ser registrado, se não conseguir realizar o pagamento aguarde alguns instantes ou tente fazer via PIX.";
                $status = 'pending';
                $url = $slip['bank_slip']['bank_slip_pdf_url'];
                $barCode = $slip['bank_slip']['digitable_line'];
                $gateway = 1;
                $invoiceId = $slip['id'];
                $datePaid = null;
            }
            $transaction = $this->populateTransaction($user, $value, $date, $date, $datePaid, $data['paymentMethod'], $status, '', 0, $gateway, 2, 'Filiacao antecipada', '', $invoiceId, '', 1, 1, $url, $barCode, $subscription->getId());
            $this->em->getRepository(Transaction::class)->save($transaction);
            $dateLastTransaction = date('Y-m', strtotime($this->em->getRepository(Transaction::class)->getDateLastTransaction($user->getId())[0]['data_criacao']));
            for ($i = 1; $i <= $data['parcel']; $i++) {
                $day = $subscription->getDiaCiclo();
                $dateTransaction = date("Y-m", strtotime($dateLastTransaction . "+" . $i . "month")) . '-' . $day;
                $explode = explode('-', $dateTransaction);
                $valid = checkdate($explode[1], $day, $explode[0]);
                while ($valid == false) {
                    $day--;
                    $dateTransaction = date("Y-m", strtotime($dateLastTransaction . "+" . $i . "month")) . '-' . $day;
                    $explode = explode('-', $dateTransaction);
                    $valid = checkdate($explode[1], $day, $explode[0]);
                }
                $transactionDate = new \Datetime($dateTransaction);
                $save = $this->populateTransaction($user, $subscription->getValor(), $transactionDate, $date, null, $data['paymentMethod'], 'accumulated', '', 1, $gateway, 2, 'Ref - ' . $transaction->getId(), '', '', '', 1, 1, '');
                $this->em->getRepository(Transaction::class)->save($save);
            }
            $nextCharge = new \DateTime(str_replace('/', '-', $data['nextCharge']));
            $subscription->setDataGeraFatura($nextCharge);
            $this->em->getRepository(PersonSignature::class)->save($subscription);
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => $message
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function payOff(Request $request, Response $response)
    {
        $user = $this->getLogged();
        if (!$user->isAffiliated()) $this->redirectByPermissions();
        if ($user->isAffiliated() && !$user->getUltimaAtualizacao()) $this->redirectExternal($this->baseUrl);
        $payoff = $this->em->getRepository(Transaction::class)->findBy(['tbPessoaId' => $user, 'status' => ['pending', 'expired'], 'periodicidade' => [1, 6, 12], 'origemTransacao' => 2], ['dataCriacao' => 'DESC']);
        $cards = $this->em->getRepository(PersonCreditCard::class)->findBy(['user' => $user, 'visivel' => 1, 'gatewayPagamento' => 1]);
        $count = count($this->em->getRepository(Transaction::class)->findBy(['tbPessoaId' => $user, 'status' => ['pending', 'expired'], 'origemTransacao' => 2], ['dataCriacao' => 'DESC']));
        $discharges = $this->em->getRepository(Transaction::class)->findBy(['tbPessoaId' => $user, 'status' => ['pending', 'expired'], 'periodicidade' => [0], 'origemTransacao' => 2], ['dataCriacao' => 'DESC']);
        return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/payoff.phtml', 'section' => 'payoff',
            'user' => $user, 'count' => $count, 'subMenu' => 'myFinancial', 'payoff' => $payoff, 'cards' => $cards, 'discharges' => $discharges, 'title' => 'Contribuições em aberto']);
    }


    public function payoffGenerate(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $user = $this->getLogged();
            $date = new \DateTime();
            $data = (array)$request->getParams();

            // Validação de ids
            if (!$data['ids']) throw new \Exception("Selecione ao menos uma transação!");

            // Validação de valor total
            $total = number_format($data['totalValue'], 2, '.', '');

            // Calcula o valor por parcela
            $valuePerParcel = $total / $data['parcel'];

            // Obtém o endereço do filiado
            $address = $this->iuguAddress($this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()]));
            
            // Validação de valor máximo diário
            if ($valuePerParcel > MAXVALUE) {
                throw new \Exception("Valor máximo para pagamento diário é R$ " . number_format(MAXVALUE, 2, ',', '.') . ".");
            }

            if($data['parcel'] <= 0) {
                throw new \Exception('Número de parcelas inválido');
            }

            for ($i = 1; $i <= $data['parcel']; $i++) {

                // Calcula a data de vencimento
                $dueDate = new \DateTime();
                if($i == 1) {
                    $dueDate = $date->add(new \DateInterval('P5D'));
                } else {
                    $dueDate = $dueDate->add(interval: new \DateInterval('P' . ($i - 1) . 'M'));
                }

                // calcula vencimento em dias
                $days = (new \DateTime())->diff($dueDate)->days;
                

                // Verifica se a forma de pagamento é válida
                if(!in_array($data['paymentMethod'], [1, 2, 11])) {
                    throw new \Exception('Forma de pagamento inválida');
                }

                // Gera o boleto
                if(in_array($data['paymentMethod'], [2, 11])) {

                    $charge = IuguService::slip($user->getEmail(), 
                        $user->getCpf(), 
                        $user->getName(), 
                        $address, 
                        $valuePerParcel, 
                        'Acerto de contribuições pendentes', 
                        $days
                    );

                    if($data['paymentMethod'] == 2) {
                        $message = "Boleto gerado com sucesso. <br>
                                    Código de Barras: {$charge['bank_slip']['digitable_line']} <br>
                                    <button class='btn btn-primary copy-button' type='button' 
                                            data-clipboard-text='{$charge['bank_slip']['digitable_line']}'>
                                        <i class='fa fa-copy'></i> Copiar Código de Barras
                                    </button>
                                    <a class='btn btn-success' 
                                        href='{$charge['bank_slip']['bank_slip_pdf_url']}' 
                                        target='_blank'>Baixar o boleto <i class='fa fa-file-pdf-o'></i></a>";
                    } else {
                        $message = "<div class='card'>
                                        <div class='card-header bg-success text-white'>
                                            <i class='fa fa-check-circle'></i> Pix gerado com sucesso
                                        </div>
                                        <div class='card-body'>
                                            <div class='mb-4'>
                                                <label class='form-label fw-bold mb-2'>Pix copia e cola:</label>
                                                <div class='alert alert-light border d-flex align-items-center'>
                                                    <span class='text-break flex-grow-1'>{$charge['pix']['qrcode_text']}</span>
                                                </div>
                                            </div>
                                            <div class='mb-4'>
                                                <button class='btn btn-primary copy-button' type='button' 
                                                        data-clipboard-text='{$charge['pix']['qrcode_text']}'>
                                                    <i class='fa fa-copy'></i> Copiar Pix copia e cola
                                                </button>
                                            </div>
                                            <div class='text-center'>
                                                <p class='fw-bold mb-2'>Ou escaneie o QR Code:</p>
                                                <div class='bg-light p-4 d-inline-block rounded'>
                                                    <img src='{$charge['pix']['qrcode']}' 
                                                        alt='QR Code Pix' 
                                                        class='img-fluid' 
                                                        style='max-width: 400px;'>
                                                </div>
                                            </div>
                                        </div>
                                    </div>";
                    }
    
                    $status = 'pending';
                    $url = $charge['bank_slip']['bank_slip_pdf_url'];
                    $barCode = $charge['bank_slip']['digitable_line'];
                    $qrcode = $charge['pix']['qrcode'];
                    $dueDate = $charge['due_date'];
                    $datePaid = null;

                // Pagamento com cartão de crédito
                } elseif($data['paymentMethod'] == 1) {

                    $message = "Pagamento realizado com sucesso.";
                    $card = $this->em->getRepository(PersonCreditCard::class)->find($data['card']);
                    $personIugu = $this->getCustomerIdIUGU($user);
                    $keyIUGU = $this->getIuguTokenDirectory(1);

                    $charge = IuguService::charge(
                        $personIugu->getCustomerId(), 
                        $card->getTokenCartaoCredito(), 
                        $user->getEmail(), 
                        $user->getCpf(), 
                        $user->getName(),
                        $address, 
                        $valuePerParcel, 
                        'Acerto de contribuições pendentes',
                        0,
                        $keyIUGU['key']
                    );
                        
                    $status = 'paid';
                    $url = $barCode = $qrcode = '';
                    $dueDate = $charge['due_date'];
                    $datePaid = $date;
                }

                // Cria a transação principal
                $transaction = $this->populateTransaction(
                    $user, 
                    $valuePerParcel, 
                    new \DateTime($dueDate), 
                    new \Datetime($dueDate), 
                    $datePaid, 
                    $data['paymentMethod'],
                    $status, 
                    '', 
                    0, 
                    1, 
                    2, 
                    'Parcela ' . $i . ' de ' . $data['parcel'], 
                    '', 
                    $charge['id'], 
                    '', 
                    1, 
                    1,
                    $url, 
                    $barCode, 
                    null, 
                    $qrcode);
                $this->em->getRepository(Transaction::class)->save($transaction);

                // Adiciona a transação ao array
                $arrayTransaction[] = $transaction->getId();

            }
            
             // Converte o array em uma string separada por vírgulas
             $idsString = implode(',', $arrayTransaction);

             // Substitui as transações acumuladas pela transação principal
             $idsArray = explode(',', $data['ids']);
             foreach ($idsArray as $id) {
                 $transactionAccumulated = $this->em->getRepository(Transaction::class)->findOneBy(['id' => $id]);
                 $transactionAccumulated
                     ->setStatus('accumulated')
                     ->setObs('Transação parcelada por ' . $user->getName() . ' (' . $idsString . ')')
                     ->setInvoiceId(null);
                 $this->em->getRepository(Transaction::class)->save($transactionAccumulated);
             };
            
            $this->em->commit();
            return $response->withJson([
                'paymentMethod' => $data['paymentMethod'],
                'status' => 'ok',
                'message' => $message
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function saveCardApi(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            $fields = [
                'cvv' => 'Código de verificação',
                'expiringDate' => 'Válidade do cartão',
                'cardNumber' => 'Número do cartão',
                'cardName' => 'Nome como está no cartão',
                'international' => 'Cartão emitido no Brasil?'
            ];
            Validator::requireValidator($fields, $data);
            $data['flag'] ??= '';
            $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['id' => base64_decode($data['transaction']), 'status' => 'pending']);
            if (!$transaction) throw new \Exception('Transação inválida!');
            $user = $this->em->getRepository(User::class)->findOneBy(['id' => $transaction->getTbPessoaId()]);
            if (!$user) throw new \Exception('Usuário inválido!');
            $cards = $this->em->getRepository(PersonCreditCard::class)->check($user)['total'];
            if ($cards >= 2) throw new \Exception('Você só pode cadastrar no máximo 2 cartões por dia!');
            GoogleService::recapchaNovo($data['g-recaptcha-response']);
            $this->checkCardName($user, $data['cardName']);
            $this->disabledCards($user);
            $card = $this->createCardApiIugu($user, $data['cardNumber'], $data['cardName'], $data['cvv'], $data['expiringDate'], $data['flag'], $data['international']);
            $subscriptions = $this->em->getRepository(PersonSignature::class)->findBy(['tbPessoaId' => $user->getId(), 'status' => 'created']);
            foreach ($subscriptions as $subscription) {
                $subscription->setPaymentForm(1)->setGatewayPagamento(1)->setCustomerId($card['customer'])->setTokenCartaoCredito($card['paymentToken']);
                if ($user->getExemption() == 1 && $subscription->getOrigemTransacao() == 2) $subscription->setPeriodicidade(1)->setValor(MINMONTHLY);
                $this->em->getRepository(PersonSignature::class)->save($subscription);
            }
            $user->setIrpf(0)->setExemption(0);
            $this->em->getRepository(User::class)->save($user);
            $this->payLastOpened($user, $card['paymentToken']);
            
            /*-----Hubspot/Mailchimp-----*/
            $this->updateStatusRegister($user);

            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => 'Cartão cadastrado com sucesso',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function generateSlip(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $date = new \DateTime();
            $data = (array)$request->getParams();
            //if (!isset($data['token']) || $data['token'] != "6LfJf9QZAAAAAArXzQDG-64_GJyGJyYggD2w4") die('Token Inválido!');
            if (!array_key_exists('transaction', $data)) die('Solicitação inválida!');
            $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['id' => base64_decode($data['transaction']), 'status' => ['pending', 'expired']]) ?: die($this->redirectExternal(BASEURL . '/financeiro/extrato'));
            $user = $this->em->getRepository(User::class)->findOneBy(['id' => $transaction->getTbPessoaId()]) ?: die($this->redirectExternal(BASEURL));
            if ($transaction->getInvoiceId() && $transaction->getDataVencimento()->format('Y-m-d') >= date('Y-m-d') && $transaction->getUrl()) {
                $url = str_replace(".pdf", "", $transaction->getUrl());
            } else {
                $address = $this->iuguAddress($this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()]));
                $slip = IuguService::slip($user->getEmail(), $user->getCpf(), $user->getName(), $address, $transaction->getValor(), $transaction->getOriginTypeString(), 5);
                $transaction->setDataVencimento($date)
                    ->setFormaPagamento(2)
                    ->setGatewayPagamento(1)
                    ->setUrl($slip['bank_slip']['bank_slip_pdf_url'])
                    ->setStatus('pending')
                    ->setCodigoBarras($slip['bank_slip']['digitable_line'])
                    ->setInvoiceId($slip['id'])
                    ->setObs('RD');
                $this->em->getRepository(Transaction::class)->save($transaction);
                $url = str_replace(".pdf", "", $slip['bank_slip']['bank_slip_pdf_url']);
            }
            $this->em->commit();
            $this->redirectExternal($url);
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function refreshCard(Request $request, Response $response)
    {
        try {
            $data = (array)$request->getParams();
            if ($data['transaction']) {
                $transaction = $this->em->getRepository(Transaction::class)->findOneBy(['id' => base64_decode($data['transaction']), 'status' => 'pending']);
                $user = $this->em->getRepository(User::class)->findOneBy(['id' => $transaction->getTbPessoaId()]);
            } else {
                $user = $this->getLogged();
            }
            return $response->withJson([
                'status' => 'ok',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function reportIncome(Request $request, Response $response, bool $print)
    {
        $user = $this->getLogged();
        $transactions = $this->em->getRepository(Transaction::class)->reportIncome($user->getId());
        if (!$transactions) {
            Session::set('errorMsg', 'Você não possui contribuições no ano de ' . date('Y') - 1 . ' para declaração!');
            $this->redirect();
        }
        foreach ($transactions as $transaction) {
            $groupTransactions[$transaction["cnpj"]][] = $transaction;
        }
        $transactionsTotal = $this->em->getRepository(Transaction::class)->reportIncomeTotal($user->getId());
        foreach ($transactionsTotal as $total) {
            $groupTotal[$total["cnpj"]][] = $total;
        }
        $group = array_map(null, $groupTransactions, $groupTotal);
        if ($print) {
            return $this->renderer->render($response, 'financial/receipts/reportIncome.phtml', ['section' => 'reportIncome',
                'user' => $user, 'subMenu' => 'myFinancial', 'group' => $group]);
        } else {
            return $this->renderer->render($response, 'default.phtml', ['page' => 'financial/reportIncome.phtml', 'section' => 'reportIncome',
                'user' => $user, 'subMenu' => 'myFinancial', 'group' => $group]);
        }
    }

    function tirarAcentos($string)
    {
        return preg_replace(array("/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/", "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/", "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/", "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/", "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/", "/(ñ)/", "/(Ñ)/"), explode(" ", "a A e E i I o O u U n N"), $string);
    }

    public function chatbotBillet(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $date = new \DateTime();
            $data = (array)$request->getParams();
            if (!array_key_exists('cpf', $data)) die('Solicitação inválida!');
            $user = $this->em->getRepository(User::class)->findOneBy(['cpf' => $data['cpf']]);
            if (!$user) throw new \Exception("Cadastro não encontrado");
            $transaction = $this->em->getRepository(Transaction::class)->lastTransactionChatbot($user);
            if (!$transaction) throw new \Exception("Contribuição não encontrada");
            if ($transaction->getFormaPagamento() == 1) {
                $address = $this->iuguAddress($this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()]));
                $slip = IuguService::slip($user->getEmail(), $user->getCpf(), $user->getName(), $address, $transaction->getValue(), 'Contribuição');
                $transaction->setGatewayPagamento(1)
                    ->setFormaPagamento(2)
                    ->setInvoiceId($slip['id'])
                    ->setUltimaAtualizacao($date)
                    ->setUrl($slip['bank_slip']['bank_slip_pdf_url'])
                    ->setCodigoBarras($slip['bank_slip']['digitable_line'])
                    ->setObs('Gerada pelo Chatbot');
                $this->em->getRepository(Transaction::class)->save($transaction);
            }
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'bank_slip_pdf_url' => $transaction->getUrl(),
                'digitable_line' => $transaction->getCodigoBarras(),
                'message' => '',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function updatePaymentData(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            $fields = [
                'cvv' => 'CVV',
                'expiringDate' => 'Válidade do cartão',
                'cardNumber' => 'Número do cartão',
                'cardName' => 'Nome como está no cartão',
                'international' => 'Cartão emitido no Brasil?'
            ];
            Validator::requireValidator($fields, $data);
            $data['flag'] ??= '';
            $this->disabledCards($user);
            $card = $this->createCardApiIugu($user, $data['cardNumber'], $data['cardName'], $data['cvv'], $data['expiringDate'], $data['flag'], $data['international']);
            $subscriptions = $this->em->getRepository(PersonSignature::class)->findBy(['tbPessoaId' => $user->getId(), 'status' => 'created']);
            foreach ($subscriptions as $subscription) {
                $subscription->setPaymentForm(1)
                    ->setGatewayPagamento(1)
                    ->setCustomerId($card['customer'])
                    ->setTokenCartaoCredito($card['paymentToken']);
                if ($user->getExemption() == 1) {
                    $subscription->setPeriodicidade(1)
                        ->setValor(MINMONTHLY);
                }
                $this->em->getRepository(PersonSignature::class)->save($subscription);
            }
            $user->setIrpf(0)->setExemption(0);
            $this->em->getRepository(User::class)->save($user);
            $this->payLastOpened($user, $card['paymentToken']);
            $user->setUltimaAtualizacao(new \Datetime());
            $this->em->getRepository(User::class)->save($user);

            /*-----Hubspot/Mailchimp-----*/
            $this->updateStatusRegister($user);

            return $response->withJson([
                'status' => 'ok',
                'message' => 'Dados registrados com sucesso, o NOVO agradece sua participação!',
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function refreshInvoiceLibertas(Request $request, Response $response)
    {
        $token = 'qj2n50OhznQwsdfdsfdsfze9uLKiG';

        /*-----Autenticação-----*/
        $header = $request->getHeaders();
        if (!isset($header['HTTP_AUTHORIZATION'][0]) || $header['HTTP_AUTHORIZATION'][0] != $token) die('token invalido');

        /*-----Inicia processamento-----*/
        $data = (array)$request->getParams();
        $status = $data['data']['status'];
        $paymentMethod = match ($data['data']['payment_method']) {
            'iugu_pix' => 3,
            'iugu_bank_slip' => 2,
            'iugu_credit_card' => 1,
            default => 0
        };

        /*-----Atualiza fatura-----*/
        $transaction = $this->em->getRepository(LibertasCourseBuy::class)->findOneBy(['invoiceId' => $data['data']['id']]);
        if (!$transaction) throw new \Exception("Error Processing Request");

        if ($data["event"] == "invoice.status_changed") {
            if ($transaction->getStatus() != $status && $status == 'paid') {
                $transaction->setStatus(1)
                    ->setPaymentMethod($paymentMethod)
                    ->setPayDay(new \DateTime());
                $this->em->getRepository(LibertasCourseBuy::class)->save($transaction);
                $this->matricularMoodle($transaction->getUser(), $transaction->getCourse());
            }
        }
    }

    public function paymentPlans(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $data = (array)$request->getParams();
            $fields = [
                'plan' => 'Período',
                'paymentPlan' => 'Plano',
                'value' => 'Valor',
                'name' => 'Nome completo',
                'email' => 'Melhor e-mail',
                'cpf' => 'CPF',
                'paymentMethod' => 'Forma Pagamento',
            ];

            if($data['paymentMethod'] == 1) [
                $fields = [
                    'cardNumber' => 'Número do cartão',
                    'cvv' => 'CVV',
                    'cardName' => 'Nome como está no cartão',
                    'expiringDate' => 'Validade',
                    'international' => 'Cartão emitido no Brasil'
                ]
            ];
            if ($data['value'] == 0) {
                $data['value2'] = str_replace('.', '', $data['value2']);
                $data['value2'] = str_replace(',', '.', $data['value2']);
                $data['value'] = $data['value2'];
            }
            Validator::requireValidator($fields, $data);
            GoogleService::recapchaNovo($data['g-recaptcha-response']);

            $data['flag'] ??= '';
            $data['directory'] = 1;
            $date = new \Datetime();

            $paymentPlan = $this->em->getRepository(Plan::class)->find($data['paymentPlan']);
            $user = $this->em->getRepository(User::class)->findOneBy(['cpf' => $data['cpf']]);

            if(!$user) {
                $user = new User();
                $user->setFiliado(0)
                    ->setName($data['name'])
                    ->setEmail($data['email'])
                    ->setCpf(Utils::formatCpf($data['cpf']))
                    ->setTermoAceite0(1)
                    ->setTermoAceite1(1)
                    ->setTermoAceite2(1)
                    ->setTermoAceite3(1)
                    ->setDataCriacao(new \Datetime)
                    ->setEstadoCivil($this->em->getReference(CivilState::class, 6))
                    ->setGenero($this->em->getReference(Gender::class, 3))
                    ->setEscolaridade($this->em->getReference(Schooling::class, 7))
                    ->setOpcaoReligiosa(11)
                    ->setIp(Utils::getAcessData()['ip'])
                    ->setUltimaAtualizacao(new \Datetime);
            };
            $user->setPaymentPlan($paymentPlan->getId())
                ->setDatePlan($date)
                ->setUltimaAtualizacao(new \Datetime);
            $this->em->getRepository(User::class)->save($user);

            $this->disabledCards($user);
            $subscriptions = $this->em->getRepository(PersonSignature::class)
                ->findBy(['tbPessoaId' => $user->getId(), 'origin' => [2, 3]]);
            foreach ($subscriptions as $s) {
                $s->setStatus('paused');
                $this->em->getRepository(PersonSignature::class)->save($s);
            }

            $personIugu = $this->getCustomerIdIUGU($user);
            $card['customer'] = $personIugu->getCustomerId();

            if($data['paymentMethod'] == 1) {
                $card = $this->createCardApiIugu(
                    $user, $data['cardNumber'], 
                    $data['cardName'], 
                    $data['cvv'], 
                    $data['expiringDate'], 
                    $data['flag'], 
                    $data['international']
                );
            };

            $subscription = new PersonSignature();
            $subscription->setStatus('created')
                ->setDataCriacao($date)
                ->setOrigemTransacao(3)
                ->setGatewayPagamento(1)
                ->setGatewayStatus(1)
                ->setCustomerId($card['customer'])
                ->setTbPessoaId($user->getId())
                ->setValor($data['value'])
                ->setPaymentForm($data['paymentMethod'])
                ->setPaymentPlan($paymentPlan->getId())
                ->setPeriodicidade($data['plan'])
                ->setDiaCiclo($date->format('d'))
                ->setMesCiclo(date('m'))
                ->setTbDiretorioOrigem((int)$data['directory']);
            if ($card) $subscription->setTokenCartaoCredito($card['paymentToken']);
            $this->em->getRepository(PersonSignature::class)->save($subscription);

            $address = $this->iuguAddressDonation();
            $keyIUGU = $this->getIuguTokenDirectory(1);

            if($data['installmentsValues']) {
                foreach($data['installmentsValues'] as $i) {
                    $arr = explode(";", $i);
                    $data['value'] = str_replace('.', '', $arr[0]);
                    $data['value'] = str_replace(',', '.', $data['value']);
                    $dueDate = str_replace("/", "-", $arr[1]);
                    $dueDate = new \Datetime(date('Y-m-d', strtotime($dueDate)));
                    if(strtotime(date('Y-m-d')) == strtotime($dueDate->format('Y-m-d'))) {
                        $dueDate->add(new \DateInterval("P{$data['plan']}M"));
                        $status = 'paid';
                        if($data['paymentMethod'] == 1) {
                            $charge = IuguService::charge($card['customer'], $card['paymentToken'], $user->getEmail(), $user->getCpf(), 
                                $user->getName(), $address, $data['value'], 'Planos de contribuição', 0, $keyIUGU['key']);
                            $transaction = $this->populateTransaction($user, $data['value'], $date, $dueDate, $date, 1, 'paid', $card['customer'], 
                                $data['plan'], 1, 8, '', $card['paymentToken'], $charge['invoice_id'], '', 1, 1, '', '', $subscription->getId());
                        } else {
                            $charge = IuguService::slip($user->getEmail(), $user->getCpf(), $user->getName(), $address, $data['value'], 
                                'Planos de contribuição', 5, $keyIUGU['key']);
                            $transaction = $this->populateTransaction($user, $data['value'], $date, $dueDate, null, 2, 'pending', '', 
                                $data['plan'], 1, 8, '', '', $charge['id'], '', 1, 1, $charge['bank_slip']['bank_slip_pdf_url'], $charge['bank_slip']['digitable_line'], $subscription->getId());
                        }
                        $this->em->getRepository(Transaction::class)->save($transaction);
                    }
                    $subscriptionInstallments = new PersonSignatureInstallments();
                    $subscriptionInstallments->setSignature($subscription)
                        ->setValue($data['value'])
                        ->setDueDate($dueDate)
                        ->setCreated($date);
                    $this->em->getRepository(PersonSignatureInstallments::class)->save($subscriptionInstallments);
                }
            } else {
                $dueDate = new \Datetime();
                if($data['paymentMethod'] == 1) {
                    $charge = IuguService::charge($card['customer'], $card['paymentToken'], $user->getEmail(), $user->getCpf(), 
                        $user->getName(), $address, $data['value'], 'Planos de contribuição', 0, $keyIUGU['key']);
                    $transaction = $this->populateTransaction($user, $data['value'], $dueDate, $dueDate, $date, 1, 'paid', $card['customer'], 
                        $data['plan'], 1, 8, '', $card['paymentToken'], $charge['invoice_id'], '', 1, 1, '', '', $subscription->getId());
                } else {
                    $charge = IuguService::slip($user->getEmail(), $user->getCpf(), $user->getName(), $address, $data['value'], 
                        'Planos de contribuição', 5, $keyIUGU['key']);
                    $transaction = $this->populateTransaction($user, $data['value'], $dueDate, $dueDate, null, 2, 'pending', '', 
                        $data['plan'], 1, 8, '', '', $charge['id'], '', 1, 1, '', '', $subscription->getId());
                }
                $this->em->getRepository(Transaction::class)->save($transaction);
                $dueDate->add(new \DateInterval("P{$data['plan']}M"));
                $subscription->setDataGeraFatura($dueDate);
                $this->em->getRepository(PersonSignature::class)->save($subscription);
            }

            $affiliationSubscription = $this->em->getRepository(PersonSignature::class)
                ->findOneBy(['tbPessoaId' => $user->getId(), 
                             'origin' => 2
                            ]);

            $hp = [];
            $hp['email'] = $user->getEmail();
            $hp['filiado'] = $user->getFiliadoString();
            $hp['status_de_filiado'] = $user->getFiliadoString();
            $hp['status_cadastro'] = $user->getStatusString();   
            $hp['isento'] = $user->getExemption() == 1 ? 'SIM' : 'NÃO';
            $hp['plano_atual'] = $paymentPlan->getNameStr();
            $hp['primeiro_plano'] = $paymentPlan->getNameStr();
            $hp['plano_cancelado'] = 'Não';
            $hp['inadimplente'] = 'Não';
            if($data['paymentMethod'] == 1) $hp['data_da_ultima_mensalidade_paga'] = $date->format('Y-m-d');
            $hp['periodo_de_assinatura'] = $subscription->getFrequency();
            $hp['valor_total_do_plano'] = $data['value'];
            $hp['valor_pago_filiacao'] = $affiliationSubscription ? $affiliationSubscription->getValor() : 0;
            Hubspot::updateContact($user, $hp);

            $body = [];
            $body['email_address'] = $user->getEmail();
            $body['merge_fields']['FILIADO'] = $user->getFiliadoString();
            $body['merge_fields']['MMERGE63'] = $user->getFiliadoString();
            $body['merge_fields']['MMERGE58'] = $user->getStatusString();
            $body['merge_fields']['PLANO'] = $paymentPlan->getNameStr();
            $body['merge_fields']['PLANOCANCE'] = 'Não';
            $body['merge_fields']['INADIMPLEN'] = 'Não';
            if($data['paymentMethod'] == 1) $body['merge_fields']['ULTIMAPAGA'] = $date->format('d/m/Y');
            $body['merge_fields']['PERIODO'] = $subscription->getFrequency();;
            $body['merge_fields']['VALORPLANO'] = Utils::formatMoney($data['value']);
            Mailchimp::updateContact($body);

            $message = "";
            if($data['paymentMethod'] == 2) {
                $message = "Boleto gerado com sucesso. <br> 
                    Código de Barras: <b>{$charge['bank_slip']['digitable_line']}</b> <br> 
                    <a href='#'><span class='btn btn-primary' data-clipboard-text='{$charge['bank_slip']['digitable_line']}'>Copiar Código de Barras</span></a>
                    <a class='btn btn-success' href='{$charge['bank_slip']['bank_slip_pdf_url']}' target='_blank'>Baixar o boleto</a><br>
                    Atenção, seu boleto pode demorar até 2 horas para ser registrado, 
                    se não conseguir realizar o pagamento aguarde alguns instantes ou tente fazer pelo PIX baixo.<br>
                    <a href='#'><span class='btn btn-primary' data-clipboard-text='{$charge['pix']['qrcode_text']}'>PIX Copia e Cola</span></a>
                    <img src=\"{$charge['pix']['qrcode']}\" style='width: 80%; margin-top: 15px;'> <br>
                    <b>O sistema pode levar até 24h para dar a baixa no seu pagamento.</b>";
            }


            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => $message,
                'track' => [Utils::formatMoney($data['value']), $paymentPlan->getNameStr(), $transaction->getId()],
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function generateTotalContributionIA(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();

            //Pega parâmetros da requisição
            $email = $request->getAttribute('route')->getArgument('email');
            $limit = $request->getAttribute('route')->getArgument('limit');
      
            //Valida email
            if(empty($email)) throw new \Exception("Email não informado!");
            if(!filter_var($email, FILTER_VALIDATE_EMAIL)) throw new \InvalidArgumentException("Email inválido");

            //Pega usuário pelo email
            $user = $this->em->getRepository(User::class)->findOneBy(['email' => $email]);
            if(!$user) throw new \Exception("Usuário não encontrado!");

            //Pega total de débitos do usuário
            $total = $this->em->getRepository(Transaction::class)->getTotalDebit($user->getId(), $limit);
            if($total['total'] == 0) throw new \Exception("Usuário não possui transações!");

            //Pega endereço do usuário
            $address = $this->iuguAddress($this->em->getRepository(Address::class)->findOneBy(['tbPessoaId' => $user->getId()]));
            $validadeCep = Validator::validateCep($address);
            if ($validadeCep == false) $address = $this->iuguAddressDonation();

            //Pega token do diretório
            $keyIUGU = $this->getIuguTokenDirectory(1);

            //Gera boleto total
            $slip = IuguService::slip($user->getEmail(), $user->getCpf(), $user->getName(), $address, $total['total'], 
                'Acerto de contribuição', 5, $keyIUGU['key']);
            
            //Gera transação total
            $date = new \Datetime();
            $transaction = $this->populateTransaction($user, $total['total'], $date, $date, null, 2, 'pending', '', 0, 
                1, 2, 'Transação de quitacao', '', $slip['id'], '', 1, 1, $slip['bank_slip']['bank_slip_pdf_url'], 
                $slip['bank_slip']['digitable_line'], 0, $slip['pix']['qrcode_text'], '');
            $this->em->getRepository(Transaction::class)->save($transaction);
            
            //Substitui transações abertas por uma transação total
            $openTransactions = explode(',', $total['ids']);
            foreach($openTransactions as $t) {
                $transactionAccumulated = $this->em->getRepository(Transaction::class)->findOneBy(['id' => $t]);
                if(!$transactionAccumulated) continue;
                $transactionAccumulated->setStatus('accumulated')
                    ->setObs('Transação substituída por ' . $transaction->getId())
                    ->setInvoiceId(null);
                $this->em->getRepository(Transaction::class)->save($transactionAccumulated);
            };

            //Retorna dados do boleto total
            $arr = [
                'codigo_de_barra_boleto_total' => $slip['bank_slip']['digitable_line'],
                'link_boleto_total' => $slip['bank_slip']['bank_slip_pdf_url'], 
                'pix_total' => $slip['pix']['qrcode_text'],
            ];

            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => $arr,
            ], 200)
                ->withHeader('Content-type', 'application/json');

        } catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }
}