<?php

namespace App\Controllers;

use App\Helpers\Messages;
use App\Helpers\Session;
use App\Models\Commons\Entities\Mission;
use App\Models\Commons\Entities\UserPoint;
use App\Models\Entities\User;
use App\Requests\MissionRequest;
use App\Services\Email;
use App\Services\Moodle;
use App\Services\Hubspot;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class MissionController extends Controller
{
    public function index(Request $request, Response $response)
    {
        $user = $this->getLogged();
        return $this->renderer->render($response, 'default.phtml', ['page' => 'mobilize/mission.phtml', 'subMenu' => 'mission',
            'user' => $user, 'title' => 'Título']);
    }

    public function redirectResearch(Request $request, Response $response)
    {
        $user = $this->getLogged();
        header("Location: https://novo.org.br/pesquisa/{$user->getTokenAd()}");
        die();
    }

    public function redirectResearch2(Request $request, Response $response)
    {
        $user = $this->getLogged();
        header("Location: https://novo.org.br/pesquisa-informativa/?hash={$user->getTokenAd()}");
        die();
    }

    public function list(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $filter = $request->getQueryParams();
            $index = $request->getQueryParam('index') ?: 0;
            $limit = $request->getQueryParam('limit') ?: 25;
            $filter['user'] = $user->getId();
            $result = $this->em->getRepository(Mission::class)->listUser($filter, $limit, $index);
            $total = $this->em->getRepository(Mission::class)->listTotalUser($filter);
            $partial = ($index * $limit) + sizeof($result);
            $partial = $partial <= $total ? $partial : $total;
            return $response->withJson([
                'status' => 'ok',
                'message' => $result,
                'total' => (int)$total,
                'partial' => (int)$partial,
            ], 200)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }

    public function indicateAchievement(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            $user = $this->getLogged();
            $data = (array)$request->getParams();
            $files = $request->getUploadedFiles();

            $requestValidate = new MissionRequest($data);
            $requestValidate->validate();

            $mission = $this->em->getRepository(Mission::class)->find($data['mission']);

            if ($data['mission'] == 4 && $data['check']) {
                $countMission = $this->em->getRepository(UserPoint::class)->totalByMission(4);
                if ($countMission > 15) throw new \Exception('Essa tarefa já chegou no seu limite de participantes.');
            }

            if ($data['mission'] == 4 && $data['check'] == null) $mission->setPoints(0);

            $result = $this->em->getRepository(UserPoint::class)->save($user, null, $mission, $files);

            $result['points'] = $this->em->getRepository(UserPoint::class)->pointsByUser($user)[0]['points'];

            $this->em->commit();

            return $this->responseJson($result, $response, 201);
        } catch (\Exception $e) {
            $this->em->rollback();
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function changeActive(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('id');

            $result = $this->em->getRepository(Mission::class)->changeActive($id);

            return $this->responseJson($result, $response);

        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function destroy(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('id');

            $result = $this->em->getRepository(Mission::class)->destroy($id);

            return $this->responseJson($result, $response);
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }

    public function show(Request $request, Response $response)
    {
        try {
            $user = $this->getLogged();
            $id = $request->getAttribute('route')->getArgument('id');
            $data = $this->em->getRepository(Mission::class)->find($id);
            $data->setDescription(nl2br($data->getDescription()));
            return $response->withJson([
                'status' => 'ok',
                'message' => $data->toArray(),
            ], 200)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson(['status' => 'error',
                'message' => $e->getMessage(),])->withStatus(500);
        }
    }


    public function libertasCourse(Request $request, Response $response)
    {
        $moodleUsers = Moodle::getUsersByCourse(18);
        $missionLibertas = $this->em->getRepository(Mission::class)->find(2);
        foreach ($moodleUsers as $moodleUser) {
            $user = $this->em->getRepository(User::class)->findOneBy(['email' => $moodleUser['email']]);
            if (!$user) continue;
            $userPoint = $this->em->getRepository(UserPoint::class)->findOneBy(['user' => $user, 'mission' => $missionLibertas]);
            if ($userPoint) continue;
            $userPoint = new UserPoint();
            $userPoint->setUser($user)
                ->setMission($missionLibertas)
                ->setPoints($missionLibertas->getPoints())
                ->setStatus(UserPoint::APPROVED_MISSION);
            $this->em->persist($userPoint);
        }
        $this->em->flush();
    }

    public function emails(Request $request, Response $response)
    {
        $userPoints = $this->em->getRepository(UserPoint::class)->findBy(['benefit' => null, 'emailSend' => false]);
        foreach ($userPoints as $userPoint) {
            $userPoint->setEmailSend(true);
            $this->em->persist($userPoint);
            $this->em->flush();
            $points = $this->em->getRepository(UserPoint::class)->pointsByUser($userPoint->getUser())[0]['points'];
            Email::send($userPoint->getUser()->getEmail(), $userPoint->getUser()->getName(), 'Missão cumprida com sucesso!', Messages::missionCompleted($userPoint, $points));


            /*-----Hubspot-----*/
            $hp = [];
            $hp['email'] = $userPoint->getUser()->getEmail();
            $hp[$userPoint->getMission()->getIdentifier()] = 'SIM';
            $hp['pontos_mobiliza__api_'] = $points;
            Hubspot::updateContact($userPoint->getUser(), $hp);

        }

    }


}
