<?php

namespace App\Controllers;

use App\Helpers\Utils;
use App\Models\Entities\City;
use App\Models\Entities\State;
use App\Models\Entities\User;
use App\Models\Entities\Candidate;
use App\Models\Entities\WhatsAppGroups;
use App\Models\Entities\LocalDocuments;
use App\Models\Entities\Transaction;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class ApiController extends Controller
{
    public function getCitiesByState(Request $request, Response $response)
    {
        $param = $request->getAttribute('route')->getArgument('state');
        $state = $this->em->getRepository(State::class)->find($param);
        if (!$state) {
            $state = $this->em->getRepository(State::class)->findOneBy(['sigla' => $param]);
        }
        $cities = $this->em->getRepository(City::class)->findBy(['state' => $state], ['cidade' => 'ASC']);
        $citiesArray = [];
        foreach ($cities as $city) {
            $citiesArray[] = ['id' => $city->getId(), 'name' => $city->getCidade()];
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $citiesArray,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function checkAffiliated(Request $request, Response $response)
    {
        $value = $request->getAttribute('route')->getArgument('value');
        $user = $this->em->getRepository(User::class)->checkAffiliated($value);
        if (!$user) throw new \Exception("Cadastro não encontrado!");
        $arr = [
            'nome' => $user['nome'],
            'cpf' => $user['cpf'],
            'titulo' => $user['titulo_eleitoral'],
            'filiacao' => $user['filiacao'],
            'cidade' => $user['cidade'],
            'estado' => $user['estado'],
            'uf' => $user['uf'],
        ];
        return $response->withJson([
            'status' => 'ok',
            'arr' => $arr,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function checkAcademiaDoAmanha(Request $request, Response $response)
    {
        $value = $request->getAttribute('route')->getArgument('value');
        $user = $this->em->getRepository(User::class)->checkAcademiaDoAmanha($value);
        if (!$user) throw new \Exception("Cadastro não encontrado!");
        return $response->withJson([
            'status' => 'ok',
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function getSpending(Request $request, Response $response)
    {
        $agent = $request->getQueryParam('agent');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $year = $request->getQueryParam('year');
        $values = $this->em->getRepository(Candidate::class)->spendings($agent, $state, $city, $year);
        $valueSaved = $this->em->getRepository(Candidate::class)->valueSaved($agent, $state, $city, $year);
        $valueSaved = !$valueSaved ? 0 : $valueSaved[0]['valueSaved'];
        if (!$values || !$agent && !$state && !$city && !$year) {
            $month = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
            $value1[] = $value2[] = $value3[] = $value4[] = $value5[] = $value6[] = 0;
        } else {
            foreach ($values as $value) {
                $month[] = $value['month'];
                $value1[] = $value['1'];
                $value2[] = $value['2'];
                $value3[] = $value['3'];
                $value4[] = $value['4'];
                $value5[] = $value['5'];
                $value6[] = $value['6'];
            }
        }
        return $response->withJson([
            'status' => 'ok',
            'month' => $month,
            'value1' => $value1,
            'value2' => $value2,
            'value3' => $value3,
            'value4' => $value4,
            'value5' => $value5,
            'value6' => $value6,
            'valueSaved' => $valueSaved,
            'year' => $year,
        ], 200)->withHeader('Content-type', 'application/json');
    }

    public function getSpendingTable(Request $request, Response $response)
    {
        $index = $request->getQueryParam('index');
        $agent = $request->getQueryParam('agent');
        $state = $request->getQueryParam('state');
        $city = $request->getQueryParam('city');
        $year = $request->getQueryParam('year');
        $spendings = $this->em->getRepository(Candidate::class)->listSpendings($agent, $state, $city, $year, 20, $index * 20);
        $total = $this->em->getRepository(Candidate::class)->listSpendingsTotal($agent, $state, $city, $year)[0]['total'];
        $partial = ($index * 20) + sizeof($spendings);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $spendings,
            'total' => $total,
            'partial' => $partial,
        ], 200)->withHeader('Content-type', 'application/json');
    }

    public function getPresences(Request $request, Response $response)
    {
        $agent = $request->getQueryParam('agent');
        $linkPresence = $this->em->getRepository(Candidate::class)->getPresencesLink($agent);
        $linkPresence = $linkPresence ? $linkPresence[0]['linkPresence'] : 0;
        $values = $this->em->getRepository(Candidate::class)->presences($agent);
        if (!$values) {
            $type = ['Atos Deliberativos', 'Comissões', 'Plenário'];
            $presences[] = $absences[] = 0;
        } else {
            foreach ($values as $value) {
                $type[] = $value['type'];
                $presences[] = $value['presences'];
                $absences[] = $value['absences'];
            }
        }
        return $response->withJson([
            'status' => 'ok',
            'type' => $type,
            'presences' => $presences,
            'absences' => $absences,
            'agent' => $agent,
            'linkPresence' => $linkPresence,
        ], 200)->withHeader('Content-type', 'application/json');
    }

    public function getPolls(Request $request, Response $response)
    {
        $agent = $request->getQueryParam('agent');
        $vote = $request->getQueryParam('vote');
        $index = $request->getQueryParam('index');
        $values = $this->em->getRepository(Candidate::class)->pollsGraphic($agent, $vote);
        $total = $this->em->getRepository(Candidate::class)->listPollsTotal($agent, $vote)[0]['total'];
        $polls = $this->em->getRepository(Candidate::class)->listPolls($agent, $vote, 20, $index * 20);
        $partial = ($index * 20) + sizeof($polls);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        if (!$values) {
            $cabinet = ['Contrário', 'Favorável', 'Abstenção'];
            $votes[] = 0;
            $rgb = [];
        } else {
            foreach ($values as $value) {
                $type[] = $value['type'];
                $votes[] = $value['votes'];
                $rgb = ['#f17021', '#1a2948', '#fdc908'];
            }
        }
        return $response->withJson([
            'status' => 'ok',
            'type' => $type,
            'votes' => $votes,
            'rgb' => $rgb,
            'message' => $polls,
            'total' => $total,
            'partial' => $partial,
        ], 200)->withHeader('Content-type', 'application/json');
    }

    public function getReports(Request $request, Response $response)
    {
        $agent = $request->getQueryParam('agent');
        $index = $request->getQueryParam('index');
        $reports = $this->em->getRepository(Candidate::class)->listReports($agent, 20, $index * 20);
        $total = $this->em->getRepository(Candidate::class)->listReportsTotal($agent)[0]['total'];
        $partial = ($index * 20) + sizeof($reports);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $reports,
            'total' => $total,
            'partial' => $partial,
        ], 200)->withHeader('Content-type', 'application/json');
    }

    public function getProjectsApi(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $index = $request->getQueryParam('index');
        $name = $request->getQueryParam('name');
        $agent = $request->getQueryParam('agent');
        $coverage = $request->getQueryParam('coverage');
        $nickname = $request->getQueryParam('nickname');
        $ementa = $request->getQueryParam('ementa');
        $tag = $request->getQueryParam('tag');
        $author = $request->getQueryParam('author');
        $date = $request->getQueryParam('date') ? date('Y-m-d', strtotime(str_replace('/', '-', $request->getQueryParam('date')))) : null;
        $projects = $this->em->getRepository(Candidate::class)->listProjects($name, $agent, $coverage, $nickname, $ementa, $date, $tag, $author, 20, $index * 20);
        $total = $this->em->getRepository(Candidate::class)->listProjectsTotal($name, $agent, $coverage, $nickname, $ementa, $date, $tag, $author)[0]['total'];
        $partial = ($index * 20) + sizeof($projects);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $projects,
            'total' => $total,
            'partial' => $partial,
        ], 200)->withHeader('Content-type', 'application/json');
    }


    public function getCitiesByAgents(Request $request, Response $response)
    {
        $param = $request->getAttribute('route')->getArgument('state');
        $cities = $this->em->getRepository(Candidate::class)->citiesByAgents($param);
        $citiesArray = [];
        foreach ($cities as $city) {
            $citiesArray[] = ['id' => $city['city'], 'name' => $city['city']];
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $citiesArray,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function getCandidatesByState(Request $request, Response $response)
    {
        $state = $request->getAttribute('route')->getArgument('state');
        $agents = $this->em->getRepository(Candidate::class)->agentsByState($state);
        $agentsArray = [];
        foreach ($agents as $agent) {
            $agentsArray[] = ['id' => $agent['id'], 'name' => $agent['name']];
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $agentsArray,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function getCandidatesByCity(Request $request, Response $response)
    {
        $city = $request->getAttribute('route')->getArgument('city');
        $agents = $this->em->getRepository(Candidate::class)->agentsByCity($city);
        $agentsArray = [];
        foreach ($agents as $agent) {
            $agentsArray[] = ['id' => $agent['id'], 'name' => $agent['name']];
        }
        return $response->withJson([
            'status' => 'ok',
            'message' => $agentsArray,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function whatsAppGroups(Request $request, Response $response)
    {
        $data = (array)$request->getParams();
        $group = $this->em->getRepository(WhatsAppGroups::class)->findOneBy(['uf' => $data['uf']]);
        return $response->withJson([
            'status' => 'ok',
            'message' => $group->getLink(),
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function affiliateConsultation(Request $request, Response $response)
    {
        $cpf = $request->getAttribute('route')->getArgument('cpf');
        $cpf = Utils::formatCpf($cpf);
        $user = $this->em->getRepository(User::class)->affiliateConsultation((string)$cpf)[0];
        if (!$user) throw new \Exception("Solicitação Inválida, verifique o CPF informado!");
        $array = [
            'email' => $user['email'],
            'filiado' => $user['filiado'],
            'name' => $user['name'],
            'birth' => $user['birth'],
            'phone' => $user['phone'],
            'address' => [
                'state' => $user['adressState'],
                'stateId' => (int)$user['adressStateId'],
                'city' => $user['adressCity'],
                'cityId' => (int)$user['adressCityId']
            ],
            'title' => [
                'state' => $user['titleState'],
                'stateId' => (int)$user['titleStateId'],
                'city' => $user['titleCity'],
                'cityId' => (int)$user['titleCityId']
            ]
        ];
        return $response->withJson([
            'status' => 'ok',
            'message' => $array,
        ], 200)
            ->withHeader('Content-type', 'application/json');
    }

    public function localDocuments(Request $request, Response $response)
    {
        $user = $this->getLogged();
        $index = $request->getQueryParam('index');
        $keyword = $request->getQueryParam('keyword');
        $projects = $this->em->getRepository(LocalDocuments::class)->list($user, $keyword, 20, $index * 20);
        $total = $this->em->getRepository(LocalDocuments::class)->listTotal($user, $keyword)['total'];
        $partial = ($index * 20) + sizeof($projects);
        $partial = $partial <= $total ? $partial : $total; // de algum jeito o scroll chamou a mais
        return $response->withJson([
            'status' => 'ok',
            'message' => $projects,
            'total' => $total,
            'partial' => $partial,
        ], 200)->withHeader('Content-type', 'application/json');
    }

    private string $token = 'ze9uLKiGqj2n50OhznQwze9uLKiG';

    private function validToken($request)
    {
        $header = $request->getHeaders();
        if (!isset($header['HTTP_TOKEN'][0]) || $header['HTTP_TOKEN'][0] != $this->token) die('token invalido');
    }

    public function getChargeCard(Request $request, Response $response)
    {
        $this->validToken($request);
        $records = $this->em->getRepository(Transaction::class)->reprocessChargeCardApi();
        $arr = [];
        foreach ($records as $record) {
            $arr[] = [
                'name' => $record['nome'],
                'e-mail' => $record['email'],
                'phone' => '55' . $record['phone'],
                'link' => 'https://novo.org.br/atualizacao/?transaction=' . base64_encode($record['id']),
            ];
        };
        return $response->withJson([
            'status' => 'ok',
            'message' => $arr
        ], 200)->withHeader('Content-type', 'application/json');
    }

    public function getChargeBillet(Request $request, Response $response)
    {
        $this->validToken($request);
        $records = $this->em->getRepository(Transaction::class)->reprocessChargeBilletApi();
        $arr = [];
        foreach ($records as $record) {
            $arr[] = [
                'name' => $record['nome'],
                'e-mail' => $record['email'],
                'phone' => '55' . $record['phone'],
                'link' => 'https://espaco-novo.novo.org.br/api/financeiro/gera-boleto/?transaction=' . base64_encode($record['id']) . '&token=6LfJf9QZAAAAAArXzQDG-64_GJyGJyYggD2w4',
            ];
        };
        return $response->withJson([
            'status' => 'ok',
            'message' => $arr
        ], 200)->withHeader('Content-type', 'application/json');
    }

    public function getChargeBilletDefeated(Request $request, Response $response)
    {
        $this->validToken($request);
        $records = $this->em->getRepository(Transaction::class)->reprocessChargeBilletApiDefeated();
        $arr = [];
        foreach ($records as $record) {
            $arr[] = [
                'name' => $record['nome'],
                'e-mail' => $record['email'],
                'phone' => '55' . $record['phone'],
                'link' => 'https://espaco-novo.novo.org.br/api/financeiro/gera-boleto/?transaction=' . base64_encode($record['id']) . '&token=6LfJf9QZAAAAAArXzQDG-64_GJyGJyYggD2w4',
            ];
        };
        return $response->withJson([
            'status' => 'ok',
            'message' => $arr
        ], 200)->withHeader('Content-type', 'application/json');
    }

    public function getData(Request $request, Response $response)
    {
        try {
            $this->em->beginTransaction();
            if ($_SERVER['SERVER_ADDR'] != '172.31.25.141') die();
            $data = (array)$request->getParams();
            $user = $this->em->getRepository(User::class)->getData(base64_decode($data['user']))[0];
            if (!$user) throw new \Exception("Solicitação Inválida");
            $this->em->commit();
            return $response->withJson([
                'status' => 'ok',
                'message' => $user
            ], 201)
                ->withHeader('Content-type', 'application/json');
        } catch (\Exception $e) {
            return $response->withJson([
                'status' => 'error',
                'message' => $e->getMessage(),
            ])->withStatus(500);
        }
    }
}