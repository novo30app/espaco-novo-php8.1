<?php

namespace App\Services; 

class BotConversa
{
    private static $key = '82945759-edaa-430c-939f-a215e83e09b5';

    public static function registerSubscriber(array $record): array
    {
        $params = array(
            "phone" => $record['phone'],
            "first_name" => $record['firstname'],
            "last_name" => $record['surname']
        );
    	$url = "https://backend.botconversa.com.br/api/v1/webhook/subscriber/";
        return self::exeCurl('POST', $url, $params);		
    }

    public static function getRegisterID(int $phone): array
    {
        $params = "";
    	$url = "https://backend.botconversa.com.br/api/v1/webhook/subscriber/get_by_phone/$phone/";
        return self::exeCurl('GET', $url, $params);		
    }

    public static function changeTag(int $user, int $tag, string $method): void
    {
        $params = [];
    	$url = "https://backend.botconversa.com.br/api/v1/webhook/subscriber/$user/tags/$tag/";
        self::exeCurl($method, $url, $params);		
    }

    public static function getTagsList(): array
    {
        $params = "";
    	$url = "https://backend.botconversa.com.br/api/v1/webhook/tags/";
        return self::exeCurl('GET', $url, $params);		
    }

    public static function setCustomField(int $user, array $customField): array
    {
        $params = [
            'value' => $customField[1]
        ];
    	$url = "https://backend.botconversa.com.br/api/v1/webhook/subscriber/$user/custom_fields/".$customField[0]."/";
        return self::exeCurl('POST', $url, $params);		
    }

    private static function exeCurl($method, $url, $params)
    {
        $data_json = json_encode($params);
        $key = "API-KEY: " . self::$key;
        $headers = array($key, 'Content-Type: application/json');
        $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response  = curl_exec($ch);
        $response = json_decode($response, true);
		curl_close($ch);
        return $response;
    }
}