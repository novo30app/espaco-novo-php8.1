<?php


namespace App\Services;

use App\Helpers\Utils;
use App\Models\Entities\PersonRede;
use App\Models\Entities\DirectoryGatway;
use App\Models\Entities\Transaction;
use App\Models\Entities\User;
use Filipegar\Maxipago\Gateway\Exceptions\TransactionException;
use Filipegar\Maxipago\Gateway\Operations\Registration\AddCardOnFile;
use Filipegar\Maxipago\Gateway\Operations\Registration\Entities\CardOnFile;
use Filipegar\Maxipago\Gateway\Operations\Transaction\Order\Entities\SaveOnFile;
use Filipegar\Maxipago\Gateway\Operations\Transaction\Order\ReturnSale;
use Filipegar\Maxipago\Gateway\Operations\Transaction\Order\Sale;
use Filipegar\Maxipago\Merchant;
use Filipegar\Maxipago\Gateway\Environment;
use Filipegar\Maxipago\Gateway\MaxipagoClient;
use Filipegar\Maxipago\Gateway\Operations\Registration\AddConsumer;
use Filipegar\Maxipago\Gateway\Operations\Registration\Entities\Consumer;


class MaxiPago
{

    private static $merchantId = '35257';
    private static $merchantKey = 'i6f8ic66g4si767714xj9fah';

    //sandbox
    private static $merchantIdTest = '5831';
    private static $merchantKeyTest = 'ps3vvrdblj2l1yolnwa2lkge';

    public static function createCustomer(User $user)
    {
        $environment = Environment::production();
        $merchant = new Merchant(self::$merchantId, self::$merchantKey);
        if (ENV != 'prod') {
//            $environment = Environment::sandbox();
//            $merchant = new Merchant(self::$merchantIdTest, self::$merchantKeyTest);
        }
        $maxipago = new MaxipagoClient($merchant, $environment);
        $consumer = new AddConsumer();
        $consumer->consumer()
            ->setFirstName($user->getFirstName())
            ->setLastName($user->getSurname())
            ->setEmail($user->getEmail())
            ->setCustomerIdExt(str_replace('-','',str_replace('.','',$user->getCpf())));
        $apiResult = $maxipago->registration($consumer);
        return $apiResult->getCustomerId();
    }

    public static function createCard(PersonRede $personRede, string $cardNumber, string $expirationDate, string $name)
    {
        $environment = Environment::production();
        $merchant = new Merchant(self::$merchantId, self::$merchantKey);
        if (ENV != 'prod') {
//            $environment = Environment::sandbox();
//            $merchant = new Merchant(self::$merchantIdTest, self::$merchantKeyTest);
        }
        if (strlen($expirationDate) == 5) {
            $expirationDate = explode('/', $expirationDate);
            $expirationDate[1] = "20{$expirationDate[1]}";
        }
        $expirationMonth = $expirationDate[0];
        $expirationYear = $expirationDate[1];
        $maxipago = new MaxipagoClient($merchant, $environment);
        $consumer = new AddCardOnFile();
        $consumer->cardOnFile($personRede->getCustomerId())
            ->setOnFilePermissions(CardOnFile::PERMISSION_ONGOING)
            ->setOnFileMaxChargeAmount(99999.99)
            ->setCreditCardNumber(Utils::onlyNumbers($cardNumber))
            ->setBillingName($name)
            ->setExpirationMonth($expirationMonth)
            ->setExpirationYear($expirationYear);
        $apiResult = $maxipago->registration($consumer);
        return $apiResult->getToken();
    }

    public static function transaction(PersonRede $personRede, string $cardToken, float $amount, string $description = '', ?DirectoryGatway $directoryGatway, User $user, int $cron = null)
    { 
        $description = trim($description) == '' ? 'Doação' : $description;
        $environment = Environment::production();
        $merchant = new Merchant(self::$merchantId, self::$merchantKey);
        $processorId = 1;

        if ($directoryGatway) {
            $merchant = new Merchant($directoryGatway->getConfigsGatway()->id, $directoryGatway->getConfigsGatway()->chave);
            $processorId = $directoryGatway->getConfigsGatway()->proc;
        }
        if (ENV != 'prod') {
            $amount = 1;
//            $environment = Environment::sandbox();
//            $merchant = new Merchant(self::$merchantIdTest, self::$merchantKeyTest);
        }

        $maxipago = new MaxipagoClient($merchant, $environment);
        $sale = (new Sale())->setProcessorID($processorId)->setReferenceNum($description)
            ->setFraudCheck(false)->setIpAddress("189.38.95.95")
            ->setCustomerIdExt($personRede->getUserId());
        $customer = $sale->billingCustomer()->setName($user->getName());
        $sale->setShipping($customer);
        $sale->transactionDetail()->payType()->onFile()
            ->setCustomerId($personRede->getCustomerId())->setToken($cardToken);
        $sale->payment()->setSoftDescriptor($description)->setChargeTotal($amount);

        $transactionResponse = $maxipago->transaction($sale);

        if ($transactionResponse->getResponseMessage() != 'CAPTURED' && !$cron){
            throw new \Exception('Falha ao efetuar pagamento: ' . $transactionResponse->getProcessorMessage());
        }
        return ['transactionID' => $transactionResponse->getTransactionID(), 'orderId' => $transactionResponse->getOrderId(), 
            'responseMessage' => $transactionResponse->getResponseMessage(), 'creditCardScheme' => $transactionResponse->getCreditCardScheme()]; 
    }

    public static function chargeback(Transaction $transaction)
    {
        $environment = Environment::production();
        $merchant = new Merchant(self::$merchantId, self::$merchantKey);
        $maxipago = new MaxipagoClient($merchant, $environment);
        $sale = (new ReturnSale())->setOrderID($transaction->getRedeId())
            ->setReferenceNum("Cancelado via sistema");
        $sale->payment()->setChargeTotal($transaction->getValor());
        try {
            $transactionResponse = $maxipago->transaction($sale);
            $responseCode = $transactionResponse->getResponseCode(); //estado da transação
            $responseString = $transactionResponse->getResponseMessage(); //CAPTURED
        } catch (TransactionException $e) {
            $error = $e->getMessage();
        }
    }

}
