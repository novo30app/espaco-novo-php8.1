<?php

namespace App\Services;

class Auth
{
    private static $key = '6Ez1s3hPn5pL0h1cRGOWVti8FXK3nWqafMHwMbczLdWUK2hR5P';
    private static $system = 'espaco-novo';

    public static function login(string $email, string $password)
    {
        $authorization = base64_encode(self::$key) . ':' . base64_encode(self::$system);
        $headers = ['Content-Type: application/json', 'Accept: application/json', "X-Auth: {$authorization}"];
        $data = [
            'password' => $password,
            'email' => $email,
        ];
        $ch = curl_init('https://auth.novo.org.br/login');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        if (!$result['status'] || $result['status'] != 'ok') throw new \Exception($result['message']);
    }

    public static function charge(string $name, string $email, string $password)
    {
        if (trim($password) == '') throw new \Exception('Senha em branco');
        if (trim($email) == '') throw new \Exception('E-mail em branco');
        if (trim($name) == '') throw new \Exception('Nome em branco');
        $authorization = base64_encode(self::$key) . ':' . base64_encode(self::$system);
        $headers = ['Content-Type: application/json', 'Accept: application/json', "X-Auth: {$authorization}"];
        $data = [
            'name' => $name,
            'email' => $email,
            'password' => $password,
        ];
        $ch = curl_init('https://auth.novo.org.br/carga-espaco-novo/');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        if (!$result['status'] || $result['status'] != 'ok') throw new \Exception('Falha ao sincronizar auth');
    }

}
