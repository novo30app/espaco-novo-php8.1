<?php


namespace App\Services;

use App\Helpers\Utils;
use App\Models\Commons\Entities\UserIuguLibertas;
use App\Models\Entities\MemberIugu;
use App\Models\Entities\MemberRede;

class IuguLibertasService
{
    private static $key = '49027b28fe31ac401cc39a996b344370';

    public static function slip(string $email, string $cpf, string $name,
                                float  $value, string $description = 'Doação', int $extraDays = 5)
    {
        $authorization = sprintf('Basic %s', base64_encode(self::$key . ':' . ''));
        $headers = ['Content-Type: application/json', 'Accept: application/json', "Authorization: {$authorization}"];
        $data = [
            'method' => 'bank_slip',
            'email' => $email,
            'bank_slip_extra_days' => $extraDays,
            'due_date' => (new \DateTime())->add(new \DateInterval("P{$extraDays}D"))->format('Y-m-d'),
            'order_id' => uniqid(),
            'payer' => [
                'cpf_cnpj' => Utils::onlyNumbers($cpf),
                'name' => $name,
                'email' => $email,
                'address' => [
                    'number' => '1',
                    'zip_code' => '01034902',
                ]
            ],
            'items' => [
                [
                    'description' => $description,
                    'quantity' => 1,
                    'price_cents' => intval(100 * $value)
                ],
            ],
        ];
        $ch = curl_init('https://api.iugu.com/v1/invoices');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        return $result;
    }

    public static function find(string $id)
    {
        $authorization = sprintf('Basic %s', base64_encode(self::$key . ':' . ''));
        $headers = ['Content-Type: application/json', "Authorization: {$authorization}"];
        $ch = curl_init('https://api.iugu.com/v1/invoices/' . $id);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        return $result;
    }

    public static function createCliente(string $name, string $email)
    {
        $authorization = sprintf('Basic %s', base64_encode(self::$key . ':' . ''));
        $headers = ['Content-Type: application/json', 'Accept: application/json', "Authorization: {$authorization}"];
        $data = [
            'name' => $name,
            'email' => $email,
        ];
        $ch = curl_init('https://api.iugu.com/v1/customers');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        return $result['id'];
    }

    public static function createCrediCard(string $customerId, string $token, string $number)
    {
        $authorization = sprintf('Basic %s', base64_encode(self::$key . ':' . ''));
        $headers = ['Content-Type: application/json', 'Accept: application/json', "Authorization: {$authorization}"];
        $data = [
            'description' => 'cartao ' . substr($number, -4),
            'token' => $token,
        ];
        $ch = curl_init("https://api.iugu.com/v1/customers/{$customerId}/payment_methods");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        return $result;
    }


    public static function chargeBack(string $invoiceId)
    {
        $authorization = sprintf('Basic %s', base64_encode(self::$key . ':' . ''));
        $headers = ['Content-Type: application/json', 'Accept: application/json', "Authorization: {$authorization}"];
        $ch = curl_init("https://api.iugu.com/v1/invoices/{$invoiceId}/refund");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        return $result;
    }

    public static function conciliation()
    {
        $headers = ['Content-Type: application/json', 'Accept: application/json'];
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => "https://api.iugu.com/v1/accounts/invoices?api_token=" . self::$key . "&year=" . date('Y') . "&month=" . date('m') . "&status=paid",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => $headers,
        ]);
        $result = curl_exec($curl);
        $result = json_decode($result, true);
        curl_close($curl);
        return $result;
    }

    public static function chargeCredicCard(UserIuguLibertas $userIugu, string $cardToken, float $value, string $description = 'Doação', int $plots = 1)
    {
        $authorization = sprintf('Basic %s', base64_encode(self::$key . ':' . ''));
        $headers = ['Content-Type: application/json', 'Accept: application/json', "Authorization: {$authorization}"];
        $data = [
            'token' => $cardToken,
            'customer_id' => $userIugu->getCustomerId(),
            'months' => $plots,
            'payer' => [
                'cpf_cnpj' => Utils::onlyNumbers($userIugu->getUser()->getCpf()),
                'name' => $userIugu->getUser()->getName(),
                'email' => $userIugu->getUser()->getEmail()
            ],
            'items' => [
                [
                    'description' => $description,
                    'quantity' => 1,
                    'price_cents' => intval(100 * $value)
                ],
            ],
        ];
        $ch = curl_init('https://api.iugu.com/v1/charge');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        if ($result['errors']) throw new \Exception($result['errors']);
        if ($result['status'] == 'unauthorized') throw new \Exception($result['message']);
        curl_close($ch);
        return $result;
    }


}