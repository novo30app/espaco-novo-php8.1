<?php

namespace App\Services;

class RdStation
{
    private static $token = "2ae428e7a41c7fb2611e729c5055990f";
    private static $client_id = "f1843330-c922-4c03-bcae-580e5876ff7e";
    private static $client_secret = "b43d987bfd02493b8a1fdbeb18bc7798";
    private static $host = "https://api.rd.services/";

    public static function conversion(array $data)
    {
        $data["token_rdstation"] = self::$token;
        $data["traffic_source"] = 1;
        $url = 'https://www.rdstation.com.br/api/1.3/conversions';
        $dataStr = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataStr);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json', 'Content-Length: ' . strlen($dataStr)]);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        $result = curl_exec($ch);
        curl_close($ch);
    }

    public static function getByEmail($credentialsRD, $email)
    {
        $url = self::$host . "platform/contacts/email:".$email;
        $header = array(
            'Authorization: Bearer ' . $credentialsRD['access_token'],
            'Content-Type: application/json'
        );
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_POSTFIELDS, "");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result, true);
    }

    public static function updateByUuid($credentialsRD, $uuid, $newEmail){
        $url = self::$host . "platform/contacts/uuid:".$uuid;
        $header = array(
            'Authorization: Bearer ' . $credentialsRD['access_token'],
            'Content-Type: application/json'
        );
        $data['email'] = $newEmail;
        $dataStr = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataStr);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        $result = curl_exec($ch);
        curl_close($ch);
    }

    public static function refresh_token($credentialsRD){
        $url = self::$host . 'auth/token';
        $data['client_id'] = self::$client_id;
        $data['client_secret'] = self::$client_secret;
        $data['refresh_token'] = $credentialsRD['refresh_token'];
        $dataStr = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataStr);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result, true);
    }

    public static function getContactsBySegmentations(string $accessToken, string $segmentation): array
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => "https://api.rd.services/platform/segmentations/{$segmentation}/contacts/?page=1&page_size=125",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => [
                "accept: application/json",
                "authorization: Bearer {$accessToken}"
            ],
        ]);
        $response = curl_exec($curl);
        $response = json_decode($response, true);
        return($response);
    }

    public static function insertTag(string $email, string $tag, $credentialsRD)
    {
        $url = "https://api.rd.services/platform/contacts/email:$email/tag";
        $header = array(
            'accept' => 'application/json',
            'Authorization: Bearer ' . $credentialsRD['access_token'],
            'Content-Type: application/json'
        );
        $dataStr = json_encode([
            'tags' => [
                $tag
            ],
            'traffic_source' => 1,
        ]);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataStr);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        curl_close($ch);
    }
}