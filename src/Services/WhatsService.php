<?php

namespace App\Services; 

class WhatsService
{
    public static function billetGenerated($firstName, $phone, $secure_url) // Boleto gerado
    {
		$url = "https://api.painel.zapfacil.com/api/webhooks/w1QnGhqCM3YwnpCTZbJtQCZT3MF53rww";
        $params = array(
                        "name" => $firstName,
                        "url" => $secure_url,
                        "phone" => '+55' . $phone
                    );
        self::exeCurl($url, $params);		
    }

    public static function billetValidityToday($firstName, $phone, $secure_url) // Boleto vence hoje
    {
		$url = "https://api.painel.zapfacil.com/api/webhooks/mlcA8h1h8wADzHR4cUmwx9isUowGGiqx";
        $params = array(
                        "name" => $firstName,
                        "url" => $secure_url,
                        "phone" => '+55' . $phone
                    );
        self::exeCurl($url, $params);		
    }

    public static function reminder($firstName, $record, $id) // lembrete boleto pessoas de 8 á 15 meses
    {
        $urlGenerateBillet = 'https://espaco-novo.novo.org.br/api/financeiro/gera-boleto/?transaction='.base64_encode($id);
        $url = "https://api.painel.zapfacil.com/api/webhooks/mlcA8h1h8wADzHR4cUmwx9isUowGGiqx";
        $params = array(
                        "name" => $firstName,
                        "phone" => '+55' . $record['phone'],
                        "url" => $urlGenerateBillet,
                        "value" => number_format($record['valor'], 2, ',', '.')
                    );
        self::exeCurl($url, $params);
    }

    public static function card_failure($firstName, $record, $id): void // Falha cartão
    {
        $id = str_replace('=','', base64_encode($id)) . '=';
        $urlNewCard = "https://novo.org.br/atualizacao-de-cartao-de-credito/?transaction=" . $id;
		$url = "https://api.painel.zapfacil.com/api/webhooks/TKU7ljezgP94c19u4jFEyLjOv5qQaIWM";
        $params = array(
                        "name" => $firstName,
                        "phone" => '+55' . $record['phone'],
                        "url" => $urlNewCard,
                        "value" => number_format($record['valor'], 2, ',', '.')
                    );
        self::exeCurl($url, $params);			
    }

    public static function contributionIncrease(array $record, string $firstName, string $message, string $linkEncode): void // Aumento contribuição
    {
        $date = date('Y-m-d');
        $weekday = date('w', strtotime($date));
        $url = "https://api.painel.zapfacil.com/api/webhooks/{$message}";
        $params = [
            "name" => $firstName,
            "phone" => '+55' . $record['phone'],
            "link" => $linkEncode,
            "day" => $weekday
        ];
        self::exeCurl($url, $params);
    }

    private static function exeCurl($url, $params)
    {
        $data_json = json_encode($params);
        $headers = array('Content-Type: application/json');
        $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response  = curl_exec($ch);
		curl_close($ch);
        echo $response;
    }
}