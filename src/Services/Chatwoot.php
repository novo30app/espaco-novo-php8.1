<?php

namespace App\Services;
use App\Models\Entities\User;
use App\Models\Entities\Phone;

class Chatwoot
{
    private static $apiKey = "27a48Xh6SN6euWaGhwSNoByJ";
    private static $url = "https://cogmocrm.novo.org.br/api/v1/";
    private static $accountId = 1;

    private static function exeCurl($method, $endpoint, $body = null)
    {
        if (empty($method) || empty($endpoint)) {
            throw new \Exception("Método ou endpoint inválido");
        }

        $data_json = json_encode($body);
        $headers = array(
            'api_access_token: ' . self::$apiKey,
            'Content-Type: application/json'
        );
        $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, self::$url . $endpoint);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response  = curl_exec($ch);

        if (curl_errno($ch)) {
            $error = curl_error($ch);
            curl_close($ch);
            throw new \Exception($error);
        }
        curl_close($ch);

        $response = json_decode($response, true);
        return $response;
    }

    public static function searchContactByEmail($email)
    {
        if (empty($email)) {
            throw new \Exception("Email é obrigatório");
        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \Exception("Email inválido");
        }

        $endpoint = "accounts/" . self::$accountId . "/contacts/search?q=" . ($email);
        $response = self::exeCurl('GET', $endpoint, null);

        // Verifica se há resposta e payload
        if (!isset($response['payload']) || empty($response['payload'])) {
            return null;
        }
        // Verifica se há o primeiro item e ID
        if (!isset($response['payload'][0]['id'])) {
            return null;
        }

        return $response['payload'][0]['id'];
    }

    public static function updateContactByEmail(User $user, Phone $phone)
    {
        if (empty($user)) {
            throw new \Exception("Usuário é obrigatório");
        }

        // Primeiro busca o ID do contato pelo email
        $contactId = self::searchContactByEmail($user->getEmail());
        if (!$contactId) {
            throw new \Exception("Contato não encontrado para o email: " . $user->getEmail());
        };

        // Dados fixos para atualização
        $data = [
            'name' => $user->getName(),
            'email' => $user->getEmail(),
            'custom_attributes' => [
                "status_de_filiao" => $user->getFiliadoString(),
                "plano_de_filiao" => $user->getPaymentPlanStr(),
                "linkdogesto" => "https://gestao.novo.org.br/filiados/{$user->getId()}"
            ]
        ];

        // Atualiza o contato usando o ID encontrado
        $endpoint = "accounts/" . self::$accountId . "/contacts/{$contactId}";
        return self::exeCurl('PATCH', $endpoint, $data);
    }
}