<?php

namespace App\Services;

class CEP
{
    const TOKEN = 'LkRuOVjuLbGffgFKVxTY';
    const URL_PROCESS = 'https://cep.novo.org.br/api/processos/';
    const URL_PROCESS_DETAILS = 'https://cep.novo.org.br/api/processo-detalhes/';
    const URL_DOCUMENT = 'https://cep.novo.org.br/api/documento-decisao/';

    public static function getProcessByUser(string $email) {
        $headers = ['Token: ' . self::TOKEN, 'Accept: application/json'];
        $ch = curl_init(self::URL_PROCESS . $email);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        return $result['message'];
    }

    public static function getProcessDetails(string $id, string $email) {
        $headers = ['Token: ' . self::TOKEN, 'Accept: application/json'];
        $ch = curl_init(self::URL_PROCESS_DETAILS . $id . '/' . $email);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        return $result;
    }

    public static function getDocument(string $hash) {
        $headers = ['Token: ' . self::TOKEN, 'Accept: application/json'];
        $ch = curl_init(self::URL_DOCUMENT . $hash);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        return $result;
    }
}