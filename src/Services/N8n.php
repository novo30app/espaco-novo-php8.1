<?php 

namespace App\Services;

class N8n 
{
    public static function billGenerated($firstName, $record, $secure_url) // Boleto gerado
    {
		$url = "https://n8n.novo.org.br/webhook/d56d0dbe-2124-49b1-a5de-f76089bfe8ce";
        $params = array(
                        "name" => $firstName,
                        "phone" => '+55' . $record['phone'],
                        "url" => $secure_url,
                        "value" => number_format($record['valor'], 2, ',', '.')
                    );
        self::exeCurl($url, $params);		
    }

    public static function expiredBillet($firstName, $record, $slip, $remainingDays) // Boleto vencido
    {
		$url = "https://n8n.novo.org.br/webhook/118d86e6-7ea5-429b-80ad-f19b1b292ff1";
        $params = array(
                        "name" => $firstName,
                        "phone" => '+55' . $record['phone'],
                        "url" => $slip,
                        "value" => number_format($record['valor'], 2, ',', '.'),
                        "remainingDays" => $remainingDays
                    );
        self::exeCurl($url, $params);		
    }

    public static function cardError($firstName, $record) // Erro cartão
    {
        $urlNewCard = "https://novo.org.br/atualiza/" . base64_encode($record['tb_pessoa_id']);
        $url = "https://n8n.novo.org.br/webhook/25b2c81c-0aed-45fd-a311-4bd3ea5105b8";
        $params = array(
                        "name" => $firstName,
                        "phone" => '+55' . $record['phone'],
                        "url" => $urlNewCard,
                        "value" => number_format($record['valor'], 2, ',', '.')
                    );
        self::exeCurl($url, $params);		
    }

    private static function exeCurl($url, $params)
    {
        die();
        $data_json = json_encode($params);
        $headers = array('Content-Type: application/json');
        $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response  = curl_exec($ch);
		curl_close($ch);
        echo $response;
    }
}