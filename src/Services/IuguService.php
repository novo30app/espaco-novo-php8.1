<?php

namespace App\Services;

use App\Helpers\Utils;

class IuguService
{

    private static $key = '9029F2B1B1FB39C18BC6E7E9F5874D36D4FAA5437DA1F00A69BDE3C32DFCB9A9';
    private static $account = '41ABC7E7818447848A0C521C0D0C71DF';

    /* PROD */
    //private static $key = '473900DCC93F1CF5329439FDA1EE86A494F5B694ADF7AD70F1A8224971FF9B59';
    //private static $account = 'A566DC8446684FEAB581A1CBEAF92F08';

    /* TEST */

    //private static $keyTest = '3E2FA0B6B0C50848959A1B2FF23FDD6345389C82D94360BB14E1F992F3ADB5A3';

    //private static $key = 'AE54672FA6DFE513E178719FC62BABF9724A456B214155E89A71AD91E76E0C73';
    //private static $keyTest = '19583F96A35021AE5ECF1E4D9372979AF34E785FFC2D2C7B444AF9AE584F1ED4';
    //private static $account = 'B9C64DF15D3E43FE972AFA2C2D9F7531';

    public static function createCliente(string $name, string $email)
    {
        $authorization = sprintf('Basic %s', base64_encode(self::$key . ':' . ''));
        $headers = ['Content-Type: application/json', 'Accept: application/json', "Authorization: {$authorization}"];
        $data = [
            'name' => $name,
            'email' => $email,
        ];
        $ch = curl_init('https://api.iugu.com/v1/customers');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        return $result['id'];
    }

    public static function slip(string $email, string $cpf, string $name, array $address,
                                float  $value, string $description = 'Doação', int $extraDays = 5, string $key = null)
    {
        if(!$key) $key = self::$key;
        $authorization = sprintf('Basic %s', base64_encode($key . ':' . ''));
        $headers = ['Content-Type: application/json', 'Accept: application/json', "Authorization: {$authorization}"];
        $data = [
            'method' => 'bank_slip',
            'email' => $email,
            'bank_slip_extra_days' => $extraDays,
            'due_date' => (new \DateTime())->add(new \DateInterval("P{$extraDays}D"))->format('Y-m-d'),
            'order_id' => uniqid(),
            'payer' => [
                'cpf_cnpj' => Utils::onlyNumbers($cpf),
                'name' => $name,
                'email' => $email,
                'address' => $address
            ],
            'items' => [
                [
                    'description' => $description,
                    'quantity' => 1,
                    'price_cents' => intval(round(100 * $value))
                ],
            ],
        ];
        $ch = curl_init('https://api.iugu.com/v1/invoices');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        return $result;
    }

    public static function find(string $id, string $key = null)
    {
        if(!$key) $key = self::$key;
        $headers = ['Content-Type: application/json', 'Accept: application/json'];
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => "https://api.iugu.com/v1/invoices/" . $id . "?api_token=" . $key,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => $headers,
        ]);
        $result = curl_exec($curl);
        $result = json_decode($result, true);
        curl_close($curl);
        return $result;
    }

    public static function charge(string $cusomerId, string $cardToken, string $email, string $cpf, string $name, array $address,
                                  float  $value, string $description = '', int $cron = null, string $key = null)
    {
        if(!$key) $key = self::$key;
        $description = trim($description) == '' ? 'Doação' : $description;
        $authorization = sprintf('Basic %s', base64_encode($key . ':' . ''));
        $headers = ['Content-Type: application/json', 'Accept: application/json', "Authorization: {$authorization}"];
        $data = [
            'payable_with' => ['credit_card'],
            'email' => $email,
            'due_date' => date('Y-m-d'),
            'order_id' => '',
            'payer' => [
                'cpf_cnpj' => Utils::onlyNumbers($cpf),
                'name' => $name,
                'email' => $email,
                'address' => $address
            ],
            'items' => [
                [
                    'description' => $description,
                    'quantity' => 1,
                    'price_cents' => intval(round(100 * $value))
                ],
            ],
        ];
        $ch = curl_init('https://api.iugu.com/v1/invoices');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        return self::chargeCreditCard($result['id'], $cardToken, $cron, $key);
    }

    public static function chargeCreditCard($id, $cardToken, $cron, string $key = null)
    {
        $authorization = sprintf('Basic %s', base64_encode($key . ':' . ''));
        $headers = ['Content-Type: application/json', 'Accept: application/json', "Authorization: {$authorization}"];
        $data = [
            'invoice_id' => $id,
            'customer_payment_method_id' => $cardToken,
        ];
        $ch = curl_init('https://api.iugu.com/v1/charge');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        curl_close($ch);
        if (!$cron && $result['errors']) throw new \Exception($result['errors']);
        if (!$cron && $result['status'] == 'unauthorized') throw new \Exception($result['message']);
        return $result;
    }

    public static function rescue(string $amount, string $key = null, string $account = null)
    {
        if(!$key) $key = self::$key;
        if(!$account) $account = self::$account;
        // token criado apenas para saque com autenticação RSA da conta mestre.
        if($account == "A566DC8446684FEAB581A1CBEAF92F08") $key = "E8B6228E5AB2E0432CB4F7B12800B63FE712EEC2FAEDEF632637CADED80162F0";
        $arr['amount'] = floatVal($amount);
        $datetime = new \DateTime(date('Y-m-d H:i:s'));
        // corpo completo do arquivo RSA configurado.
        $data = "POST|/v1/accounts/" . $account . "/request_withdraw\n{$key}|{$datetime->format(\DateTime::ATOM)}\n" . json_encode($arr); 
        // chave privada criada a partir do token acima.
        $private_key = file_get_contents(BASEURL . '/uploads/private.pem');
        // geração de assinatura.
        $assinatura = '';
        openssl_sign($data, $assinatura, $private_key, OPENSSL_ALGO_SHA256);
        $assinaturaBase64 = base64_encode($assinatura);
        // construção do header com assinatura criptografada para o saque.
        $headers = ['Content-Type: application/json', 
                    'Accept: application/json', 
                    "Signature:signature={$assinaturaBase64}",
                    "Request-Time:{$datetime->format(\DateTime::ATOM)}",
                    "Authorization:Bearer RThCNjIyOEU1QUIyRTA0MzJDQjRGN0IxMjgwMEI2M0ZFNzEyRUVDMkZBRURFRjYzMjYzN0NBREVEODAxNjJGMA=="
                ];
        // chamada curl para o saque.
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => "https://api.iugu.com/v1/accounts/" . $account . "/request_withdraw?api_token=" . $key,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($arr),
            CURLOPT_HTTPHEADER => $headers,
        ]);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            $result = json_decode($response, true);
            return $result;
        }
    }
    
    public static function queryAccount($key = null, $account = null)
    {
        if(!$key) $key = self::$key;
        if(!$account) $account = self::$account;
        $headers = ['Content-Type: application/json', 'Accept: application/json'];
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => "https://api.iugu.com/v1/accounts/" . $account . "?api_token=" . $key,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => $headers,
        ]);
        $result = curl_exec($curl);
        $result = json_decode($result, true);
        curl_close($curl);
        return $result;
    }

    public static function withdraw_conciliations(string $rescue, string $key = null)
    {
        if(!$key) $key = self::$key;
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => "https://api.iugu.com/v1/withdraw_requests/$rescue?api_token=" . $key,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => [
                "accept: application/json"
            ],
        ]);
        $response = curl_exec($curl);
        $response = json_decode($response, true);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }

    public static function createToken(string $cardNumber, string $cardName, string $cvv, string $expiringDate): string
    {
        $cardName = explode(" ", $cardName);
        $lastName = array_pop($cardName);
        $expiringDate = explode("/", $expiringDate);
        $data = [
            'data' => [
                'number' => $cardNumber,
                'verification_value' => $cvv,
                'first_name' => $cardName[0],
                'last_name' => $lastName,
                'month' => $expiringDate[0],
                'year' => $expiringDate[1]
            ],
            'account_id' => self::$account,
            'method' => 'credit_card',
            'test' => false
        ];
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => "https://api.iugu.com/v1/payment_token",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => [
                "accept: application/json",
                "content-type: application/json"
            ],
        ]);
        $response = curl_exec($curl);
        $response = json_decode($response, true);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response['id'];
        }
    }

    public static function createPaymentMethods(string $customerId, string $token): string
    {
        $data = [
            'description' => "Contribuição NOVO",
            'token' => $token,
            'set_as_default' => false
        ];
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => "https://api.iugu.com/v1/customers/$customerId/payment_methods?api_token=" . self::$key,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => [
                "accept: application/json",
                "content-type: application/json"
            ],
        ]);
        $response = curl_exec($curl);
        $response = json_decode($response, true);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response['id'];
        }
    }

    public static function reimbursement(string $id, string $key = null)
    {
        if(!$key) $key = self::$key;
        $headers = ['Content-Type: application/json', 'Accept: application/json'];
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => "https://api.iugu.com/v1/invoices/". $id ."/refund?api_token=" . $key,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => $headers,
        ]);
        $result = curl_exec($curl);
        $result = json_decode($result, true);
        curl_close($curl);
        return $result;
    }
}