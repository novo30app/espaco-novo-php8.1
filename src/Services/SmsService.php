<?php
namespace App\Services;

class SmsService
{
    public static function send_sms(string $number, string $msg)
    {
		$url = "https://sms-api-pointer.pontaltech.com.br/v1/multiple-concat-sms";
		$headers = 'Authorization: Basic cGFydGlkb25vdm9wcmVtaXVtOmVZTHZuUU1CVmc=';
		$msg = mb_convert_encoding($msg, "HTML-ENTITIES", "UTF-8");
		$array = array("to" => $number, "message" => $msg);
		$params = array(
			"urlCallback" => null,
			"flashSms" => true,
			"messages" => array($array)			
		);
		$data_json = json_encode($params);
		echo $headers . '<br>';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($headers));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response  = curl_exec($ch);
		curl_close($ch);
        return $response;
    }
}