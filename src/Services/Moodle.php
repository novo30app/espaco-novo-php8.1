<?php
/**
 * Created by PhpStorm.
 * User: rwerl
 * Date: 14/08/2019
 * Time: 22:45
 */

namespace App\Services;


use App\Helpers\Utils;

class Moodle
{

    const URL = 'https://cursos.institutolibertas.org.br/webservice/rest/server.php';
    const TOKEN = '8927438fec6efea3333e67253f0349e6';

    public static function createUser(string $email, string $name, string $password, string $city = '', array $customfields = []): array
    {
        $name = trim($name);
        $name = explode(' ', $name);
        $firstname = $name[0];
        $lastname = end($name);
        $data = [];
        $data['wstoken'] = self::TOKEN;
        $data['wsfunction'] = 'core_user_create_users';
        $data['moodlewsrestformat'] = 'json';
        $data = http_build_query($data);
        $data .= "&users[0][username]={$email}";
        $data .= "&users[0][email]={$email}";
        $data .= "&users[0][password]={$password}";
        $data .= "&users[0][firstname]={$firstname}";
        $data .= "&users[0][lastname]={$lastname}";
        if ($city != '') $data .= "&users[0][city]={$city}";

        $customfieldsIndex = 0;
        if (array_key_exists('filiado', $customfields)) {
            $data .= "&users[0][customfields][{$customfieldsIndex}][type]=filiado";
            $data .= "&users[0][customfields][{$customfieldsIndex}][value]={$customfields['filiado']}";
            $customfieldsIndex++;
        }
        if (array_key_exists('cpf', $customfields)) {
            $customfields['cpf'] = Utils::onlyNumbers($customfields['cpf']);
            $data .= "&users[0][customfields][{$customfieldsIndex}][type]=cpf";
            $data .= "&users[0][customfields][{$customfieldsIndex}][value]={$customfields['cpf']}";
            $customfieldsIndex++;
        }
        if (array_key_exists('estado', $customfields)) {
            $data .= "&users[0][customfields][{$customfieldsIndex}][type]=estado";
            $data .= "&users[0][customfields][{$customfieldsIndex}][value]={$customfields['estado']}";
            $customfieldsIndex++;
        }
        $data = str_replace(" ", "%20", $data); // to properly format the url
        $ch = curl_init(self::URL . "?{$data}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result, true);
        if ($result['exception']) {//erro no cadastro, vamos verificar se aquele email já está cadastrado
            $result = self::getUserByEmail($email);
        }
        if ($result[0]['id']) self::linkUserWithCompany($result[0]['id']);
        return $result ?? [];
    }

    public static function updateUser(int $id, string $email, string $name, string $city = '', array $customfields = []): array
    {
        $name = trim($name);
        $name = explode(' ', $name);
        $firstname = $name[0];
        $lastname = end($name);
        $data = [];
        $data['wstoken'] = self::TOKEN;
        $data['wsfunction'] = 'core_user_update_users';
        $data['moodlewsrestformat'] = 'json';
        $data = http_build_query($data);
        $data .= "&users[0][id]={$id}";
        $data .= "&users[0][username]={$email}";
        $data .= "&users[0][email]={$email}";
        $data .= "&users[0][firstname]={$firstname}";
        $data .= "&users[0][lastname]={$lastname}";
        if ($city != '') $data .= "&users[0][city]={$city}";
        $customfieldsIndex = 0;
        if (array_key_exists('filiado', $customfields)) {
            $data .= "&users[0][customfields][{$customfieldsIndex}][type]=filiado";
            $data .= "&users[0][customfields][{$customfieldsIndex}][value]={$customfields['filiado']}";
            $customfieldsIndex++;
        }
        if (array_key_exists('cpf', $customfields)) {
            $customfields['cpf'] = Utils::onlyNumbers($customfields['cpf']);
            $data .= "&users[0][customfields][{$customfieldsIndex}][type]=cpf";
            $data .= "&users[0][customfields][{$customfieldsIndex}][value]={$customfields['cpf']}";
            $customfieldsIndex++;
        }
        if (array_key_exists('estado', $customfields)) {
            $data .= "&users[0][customfields][{$customfieldsIndex}][type]=estado";
            $data .= "&users[0][customfields][{$customfieldsIndex}][value]={$customfields['estado']}";
            $customfieldsIndex++;
        }
        $data = str_replace(" ", "%20", $data); // to properly format the url
        $ch = curl_init(self::URL . "?{$data}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result, true);
        return $result ?? [];
    }

    public static function linkUserWithCompany(int $id, int $companyId = 1, int $departmentId = 1): void
    {
        $data = [];
        $data['wstoken'] = self::TOKEN;
        $data['wsfunction'] = 'block_iomad_company_admin_assign_users';
        $data['moodlewsrestformat'] = 'json';
        $data = http_build_query($data);
        $data .= "&users[0][userid]={$id}";
        $data .= "&users[0][managertype]=0";
        $data .= "&users[0][educator]=0";
        $data .= "&users[0][departmentid]={$departmentId}";
        $data .= "&users[0][companyid]={$companyId}";
        $ch = curl_init(self::URL . "?{$data}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
    }

    public static function login(string $email): array
    {
        $data = [];
        $data['wstoken'] = self::TOKEN;
        $data['wsfunction'] = 'auth_userkey_request_login_url';
        $data['moodlewsrestformat'] = 'json';
        $data = http_build_query($data);
        $data .= "&user[email]={$email}";
        $ch = curl_init(self::URL . "?{$data}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result, true);
        return $result ?? [];
    }

    public static function getUserByEmail(string $email): array
    {
        $data = [];
        $data['wstoken'] = self::TOKEN;
        $data['wsfunction'] = 'core_user_get_users_by_field';
        $data['moodlewsrestformat'] = 'json';
        $data['field'] = 'email';
        $data = http_build_query($data);
        $data .= "&values[0]={$email}";
        $ch = curl_init(self::URL . "?{$data}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result, true);
        return $result ?? [];
    }

    public static function getUsersByCourse(int $courseId): array
    {
        $data = [];
        $data['wstoken'] = self::TOKEN;
        $data['wsfunction'] = 'core_enrol_get_enrolled_users';
        $data['moodlewsrestformat'] = 'json';
        $data['courseid'] = $courseId;
        $data = http_build_query($data);
        $ch = curl_init(self::URL . "?{$data}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result, true);
        return $result ?? [];
    }



}
