<?php

namespace App\Services;

use PHPMailer\PHPMailer\PHPMailer;

class Email
{

    private static $HOST = 'smtp.office365.com';
    private static $LOGIN = 'noreply@novo.org.br';
    private static $PASS = 'LZdE!1}(RJ*K4pa#6c=1?';
    private static $PORT = 587;
    private static $SMTPSecure = 'tls';
    private static $SMTPAuth = true;
    private static $templateMail = BASEURL . '/assets/mail/template.html';

    public static function send(string $toEmail, string $toName, string $subject, string $message, string $replyTo = null, string $replyToName = null, $attachments = null, $copyTo = null)
    {
        if (ENV != 'prod') {
            $toEmail = 'dwerlich21@gmail.com';
            $copyTo = ['renan.almeida@novo.org.br', 'rodrigo@lifecode.dev', 'migueldolny@gmail.com'];
            $subject = "Teste - {$subject}";
        }
        $templateMail = file_get_contents(self::$templateMail);
        $message = str_replace('HTMLMessage', $message, $templateMail);

        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->CharSet = 'UTF-8';
        $mail->SMTPDebug = 0; //2 para modo debug
        $mail->Host = self::$HOST;
        $mail->Port = self::$PORT;
        $mail->SMTPSecure = self::$SMTPSecure;
        $mail->SMTPAuth = self::$SMTPAuth;
        $mail->Username = self::$LOGIN;
        $mail->Password = self::$PASS;
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        $mail->setFrom(self::$LOGIN, 'Espaço NOVO');
        $mail->Subject = $subject;
        $mail->Body = $message;
        $mail->IsHTML(true);
        if ($replyTo !== null) {
            $mail->addReplyTo(trim($replyTo), $replyToName);
        }
        $mail->addAddress(trim($toEmail), $toName);
        if ($copyTo) {
            foreach ($copyTo as $copy) {
                $mail->addCC(trim($copy));
            }
        }
        if ($attachments) {
            foreach ($attachments as $attachment) {
                $mail->addAttachment($attachment[0], $attachment[1]);
            }
        }
        if (!$mail->send()) {
            throw new \Exception($mail->ErrorInfo);
        }
    }


}