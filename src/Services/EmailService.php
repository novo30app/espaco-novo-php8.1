<?php
namespace App\Services;

class EmailService
{
    private static $templateCharge = BASEURL .'/assets/mail/templateCharge.html';
    private static $templateBankSlip = BASEURL .'/assets/mail/templateBankSlip.html';

    public static function send_email(string $email, string $msg, string $subject, $banner = null)
    {
        $user = 'partidonovopremium';
        $password = 'eYLvnQMBVg';
        $headers = array(
            'Content-Type: application/json',
            'Authorization: Basic '. base64_encode("$user:$password")
        );
		$url = "https://pointer-email-api.pontaltech.com.br/send";
        $templateMail = file_get_contents(self::$templateCharge);
        $msg = str_replace('HTMLMessage', $msg, $templateMail);
        $msg = str_replace('Banner', $banner, $msg);    
		$array = array("email" => $email);
		$params = array(
                    "to" => array($array),
                    "fromGroup" => "Partido Novo",
                    "mailBody" => $msg,
                    "subject" => $subject,
                    "sender" => "Partido NOVO",
                    "accountId" => 5230,
                    "tracking" => true,
                    "replaceVariable" => true         
                );
		$data_json = json_encode($params);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response  = curl_exec($ch);
		curl_close($ch);
        return $response;
    }

    public static function send_affiliation($name, $email, $filiado_id, $subject, $cpf, $value, $secure_url, $pix, $barCode)
    {
        $msg = null;
        $user = 'partidonovopremium';
        $password = 'eYLvnQMBVg';
        $headers = array(
            'Content-Type: application/json',
            'Authorization: Basic '. base64_encode("$user:$password")
        );
		$url = "https://pointer-email-api.pontaltech.com.br/send";
        $templateMail = file_get_contents(self::$templateBankSlip);
        $msg = str_replace('HTMLMessage', $msg, $templateMail);
        $msg = str_replace('name', $name, $msg);    
        $msg = str_replace('affiliatedId', $filiado_id, $msg);    
        $msg = str_replace('cpf', strrev(preg_replace('/\d/', '*',  strrev($cpf), 6)), $msg);    
        $msg = str_replace('value', $value, $msg);    
        $msg = str_replace('dueDate', date('d/m/Y'), $msg);    
        $msg = str_replace('urlSlip', $secure_url, $msg);    
        $msg = str_replace('qrCode', $pix, $msg); 
        $msg = str_replace('digitableLine', $barCode, $msg);       
		$array = array("email" => $email);
		$params = array(
                    "to" => array($array),
                    "fromGroup" => "Partido Novo",
                    "mailBody" => $msg,
                    "subject" => $subject,
                    "sender" => "Partido NOVO",
                    "accountId" => 5230,
                    "tracking" => true,
                    "replaceVariable" => true         
                );
		$data_json = json_encode($params);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response  = curl_exec($ch);
		curl_close($ch);
        return $response;
    }
}