<?php 

namespace App\Services;

class Vnda 
{
    /**
     * Documentação: https://developers.vnda.com.br/
     */
    
    private static $token = "Bearer DkvnozTafSyouXTUj38mPBzm";
    private static $host = "loja.novo.org.br";
    private static $api = "https://api.vnda.com.br/api/";

    public static function clients(string $name, int $gender, string $email, string $birth, string $password = null, string $tag = null, string $vndaId = null)
    {
        $request = 'v2/clients';
        $method = 'POST';
        /** Verifica se vai criar um novo cadastro ou se vai atualizar um existente */
        if($vndaId) {
            $request = "v2/clients/$vndaId";
            $method = 'PATCH';
        }
        $name = explode(" ", $name);
        $lastName = array_pop($name);
        switch ($gender) {
            case 1:
                $gender = 'M';
                break;
            case 2:
                $gender = 'F';
                break;
            default:
                $gender = "";
                break;
        }
        $data = [
            'email' => $email,
            'first_name' => $name[0],
            'last_name' => $lastName,
            'birthdate' => $birth,
            'gender' => $gender,
            'tags' => $tag,   
            'terms' => true
        ];
        if($password) array_push($data, ['password' => $password, 'password_confirmation' => $password]);   
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => self::$api.$request,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => [
                "X-Shop-Host: loja.novo.org.br",
                "accept: application/json",
                "authorization:" . self::$token,
                "content-type: application/json"
            ],
        ]);
        $response = curl_exec($curl);
        $response = json_decode($response, true);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }
}