<?php

namespace App\Services;

class Libertas
{
    private static $key = '6Ez1s3hPn5pL0h1cRGOWVti8FXK3nWqafMHwMbczLdWUK2hR5P';


    public static function moodleLogin(array $fields)
    {
        $authorization = self::$key;
        $headers = ['Content-Type: application/json', 'Accept: application/json', "X-Auth: {$authorization}"];
        $data = [
            'name' => $fields['name'],
            'email' => $fields['email'],
            'city' => $fields['city'],
            'cpf' => $fields['cpf'],
            'filiado' => $fields['filiado'],
            'estado' => $fields['estado'],
            'courseId' => $fields['courseId'] ?? '',
        ];
        $ch = curl_init('https://institutolibertas.org.br/api/moodle');
        if (ENV == 'local') $ch = curl_init('http://brasil-novo-php81.com/api/moodle');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result, true);
    }

}
