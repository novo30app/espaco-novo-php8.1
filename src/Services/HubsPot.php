<?php

namespace App\Services;
use App\Helpers\Utils;
use App\Models\Entities\User;

class Hubspot
{
    private static $token = "pat-na1-6e394bd2-1fad-432a-a180-313ef7b028b7";
    private static $url = "https://api.hubapi.com/crm/v3/objects/contacts/";

    private static function exeCurl($method, $url, $body)
    {
        $data_json = json_encode($body);
        $headers = array(
            'Authorization: Bearer ' . self::$token, 
            'Content-Type: application/json'
        );
        $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response  = curl_exec($ch);
        $response = json_decode($response, true);
        return $response;
    }

    // metodo para criar um cadastro no hubspot com variáveis básicas
    public static function createContact(User $user, $phone = null): void
    {
        $body['properties']['email'] = $user->getEmail();
        $body['properties']['firstname'] = $user->getFirstName();
        $body['properties']['lastname'] = $user->getSurname();
        $body['properties']['status_cadastro'] = $user->getStatusString();
        if($user->getCpf()) $body['properties']['cpf'] = Utils::onlyNumbers($user->getCpf()); 
        $body['properties']['filiado'] = $user->getFiliadoString();
        $body['properties']['status_de_filiado'] = $user->getFiliadoString();
        if($user->getTituloEleitoralUfId()) $body['properties']['estado'] = $user->getTituloEleitoralUfId()->getSigla();
        if($user->getTituloEleitoralMunicipioId()) $body['properties']['cidade_2'] = $user->getTituloEleitoralMunicipioId()->getCidade();
        if($user->getGenero()) $body['properties']['sexo_'] = $user->getGenero()->getGenero();
        if($phone) {
            $body['properties']['phone'] = $phone;
            $body['properties']['hs_whatsapp_phone_number'] = $phone;
        } 
        self::exeCurl('POST', self::$url, $body);
    }

    // metodo para consultar e retornar o id do cadastro dentro do hubspot
    public static function searchContact(string $email): array
    {
        $url = self::$url . "batch/read";
        $body['idProperty'] = "email";
        $body['inputs'][]['id'] = $email;
        $exeCurl = self::exeCurl('POST', $url, $body);
        return $exeCurl;
    }

    //metodo para atualizar o dados de um contato dentro do hubspot
    public static function updateContact(User $user, array $arr): void
    {
        $search = self::searchContact($user->getEmail());
        if(!$search['results'][0]['id']) {
            self::createContact($user);
            $search = self::searchContact($user->getEmail());
        }
        if($search['results'][0]['id']) {
            $hp['properties'] = $arr;
            $url = self::$url . $search['results'][0]['id'];
            self::exeCurl('PATCH', $url, $hp);
        }
    }

    // metodo para inserir contato dentro do fluxo de trabalho
    public static function insertIntoTheFlow(User $user, int $flow): void
    {
        $url = "https://api.hubapi.com/automation/v2/workflows/{$flow}/enrollments/contacts/{$user->getEmail()}";
        $body = [];
        self::exeCurl('POST', $url, $body);
    }

    public static function removeFromFlow(User $user, int $flow): void
    {
        $url = "https://api.hubapi.com/automation/v2/workflows/{$flow}/enrollments/contacts/{$user->getEmail()}";
        $body = [];
        self::exeCurl('DELETE', $url, $body);
    }

    public static function getListDetails(string $name, int $flow): array
    {
        $url = "https://api.hubapi.com/crm/v3/lists/object-type-id/0-1/name/{$name}?includeFilters=false";
        $body = [];
        $response = self::exeCurl('GET', $url, $body);
        return $response;
    }
}