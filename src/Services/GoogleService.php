<?php
namespace App\Services;

class GoogleService
{
    public static function recapchaNovo(String $recapcha)
    {
        $params = [
            "secret" => "6LeTW9EZAAAAAFE4ffIfaKvhz2qNk9KFpe5MGm9K",
            "response" => $recapcha,
            "remoteip" => $_SERVER["REMOTE_ADDR"]
        ];
        $cURL = curl_init('https://www.google.com/recaptcha/api/siteverify');
        curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cURL, CURLOPT_POST, true);
        curl_setopt($cURL, CURLOPT_POSTFIELDS, $params);
        $result = curl_exec($cURL);
        $arr = json_decode($result, true);
        curl_close($cURL);
        if (!$arr['success']) {
            throw new \Exception('Preencha corretamente o capcha');
        }
        return $result;
    }

    public static function recapchaEspacoNovo(String $recapcha)
    {
        $params = [
            "secret" => "6LeaZa4ZAAAAAF5ZfJYL4qRxubiOH4MLyKhMy36Q",
            "response" => $recapcha,
            "remoteip" => $_SERVER["REMOTE_ADDR"]
        ];
        $cURL = curl_init('https://www.google.com/recaptcha/api/siteverify');
        curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cURL, CURLOPT_POST, true);
        curl_setopt($cURL, CURLOPT_POSTFIELDS, $params);
        $result = curl_exec($cURL);
        $arr = json_decode($result, true);
        curl_close($cURL);
        if (!$arr['success']) {
            throw new \Exception('Preencha corretamente o capcha');
        }
        return $result;
    }

    public static function recapchaNovoDoacao(String $recapcha)
    {
        $params = [
            "secret" => "6LfmaiUnAAAAANid5ndcl73Hokp1skwpp2Gr5hNy",
            "response" => $recapcha,
            "remoteip" => $_SERVER["REMOTE_ADDR"]
        ];
        $cURL = curl_init('https://www.google.com/recaptcha/api/siteverify');
        curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cURL, CURLOPT_POST, true);
        curl_setopt($cURL, CURLOPT_POSTFIELDS, $params);
        $result = curl_exec($cURL);
        $arr = json_decode($result, true);
        curl_close($cURL);
        if (!$arr['success']) {
            throw new \Exception('Preencha corretamente o capcha');
        }
        return $result;
    }
}