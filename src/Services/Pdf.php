<?php

namespace App\Services;
use App\Models\Entities\City;
use App\Models\Entities\State;
use App\Models\Entities\IDontWantToPay;
use App\Helpers\Utils;
use Mpdf\Mpdf;

class Pdf
{
    public static function ticket($ticket)
    {   
        $template = BASEURL . '/assets/ticket/template.html';
        $mpdf = new mPDF();
        $mpdf->SetDisplayMode('fullpage');
        $template = file_get_contents($template);
        $template = str_replace('DATE', $ticket['begin'] . 'hs até ' . $ticket['end'] . 'hs', $template);    
        $template = str_replace('NAME_EVENT', $ticket['nome_evento'], $template);
        $template = str_replace('TICKET', $ticket['description'] . ' - ' . $ticket['value'], $template);    
        $template = str_replace('ADDRESS', $ticket['address'] , $template);    
        $template = str_replace('LOCATION', $ticket['city'] . '/' . $ticket['state'], $template);    
        $template = str_replace('CODE', $ticket['code'], $template);    
        $template = str_replace('NAME', $ticket['nome'], $template);    
        $template = str_replace('CPF_NUMBER', $ticket['cpf'], $template);    
        $mpdf->WriteHTML($template);
        $mpdf->Output("Ingresso " . $ticket['code'] . '.pdf','D');
        exit;
    }

    public static function declaration(IDontWantToPay $check, State $state, City $city)
    {   
        $template = 'https://espaco-novo.novo.org.br/assets/ticket/declaration.html';
        $mpdf = new mPDF();
        $mpdf->SetDisplayMode('fullpage');
        $template = file_get_contents($template);
        $template = str_replace('NOME', $check->getName(), $template);    
        $template = str_replace('EMAIL', $check->getEmail(), $template);    
        $template = str_replace('RGNUMBER', $check->getRG(), $template);    
        $template = str_replace('CPF', $check->getCPF(), $template);    
        $template = str_replace('COMPANYNAME', $check->getCompanyName(), $template);    
        $template = str_replace('COMPANYCNPJ', $check->getCompanyCNPJ(), $template);    
        $template = str_replace('CITY', $city->getCidade(), $template);    
        $template = str_replace('STATE', $state->getSigla(), $template);    
        setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');
        $date = strftime('%A, %d de %B de %Y', strtotime('today'));
        $template = str_replace('DATA', utf8_encode($date), $template);    
        $mpdf->WriteHTML($template);
        $mpdf->Output("Declaração.pdf",'D');
        exit;
    }
}