<?php

namespace App\Services; 

class Movidesk
{
    private static $key = 'e1e7546e-b537-478c-b98c-400ccfc1c016';

    public static function getTicket(string $id): array // Pega informações do Ticket
    {
        $url = "https://api.movidesk.com/public/v1/tickets?token=".self::$key."&id={$id}";
        $headers = array('Content-Type: application/json');
        $curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$response  = curl_exec($curl);
        $response = json_decode($response, true);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }

    public static function updateTicket(array $array): void // Envia CPF e Filiado ID para Ticket
    {
        $url = "https://api.movidesk.com/public/v1/tickets?token=".self::$key."&id={$array['id']}";
        $data_json = json_encode(['customFieldValues' => $array['customFieldValues']]);
        $headers = array('Content-Type: application/json');
        $curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$response  = curl_exec($curl);
        $response = json_decode($response, true);
        $err = curl_error($curl);
    }
}