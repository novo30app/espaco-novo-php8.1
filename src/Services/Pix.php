<?php


namespace App\Services;


use App\Helpers\Utils;
use App\Models\Entities\Transaction;
use App\Models\Entities\User;

class Pix
{

    private static function accessToken()
    {
        $token = PIX_CLIENT_ID . ':' . PIX_CLIENT_SECRET;
        $token = base64_encode($token);
        $ch = curl_init('https://oauth.bb.com.br/oauth/token');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials&scope=cob.write cob.read pix.read");
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/x-www-form-urlencoded',
            "Authorization: Basic {$token}"]);
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result);
        return $result->access_token;
    }

    public static function charge($value, User $user)
    {
        $params = [];
        $params['devedor']['cpf'] = Utils::onlyNumbers($user->getCpf());
        $params['devedor']['nome'] = $user->getName();
        $params['valor']['original'] = "123.45";
        $params['valor']['original'] = $value;
        $params['chave'] = "pix@novo.org.br";
        $params['solicitacaoPagador'] = "Doação";
        $ch = curl_init("https://api.bb.com.br/pix/v1/cob/?gw-dev-app-key=" . PIX_APPLICATION_KEY);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json',
            'Authorization: Bearer ' . self::accessToken()]);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        if ($err) {
            echo "cURL Error #:" . $err;
        }
        return json_decode($response, true);
    }


    //ATIVA: a cobrança está disponível, porém ainda não ocorreu pagamento;
    //CONCLUIDA: a cobrança encontra-se paga. Não se pode alterar e nem remover uma cobrança cujo status esteja “CONCLUÍDA”;
    //EM_PROCESSAMENTO: liquidação em processamento;
    //NAO_REALIZADO: indica que a devolução não pode ser realizada em função de algum erro durante a liquidação, como por exemplo, saldo insuficiente.;
    //DEVOLVIDO: cobrança com devolução realizada pelo Sistema de Pagamentos Instantâneos (SPI);
    //REMOVIDA_PELO_USUARIO_RECEBEDOR: foi solicitada a remoção da cobrança; a critério do usuário;
    //REMOVIDA_PELO_PSP: recebedor, por conta de algum critério, solicitou a remoção da cobrança.
    public static function find(Transaction $transaction)
    {
        $ch = curl_init("https://api.bb.com.br/pix/v1/cob/{$transaction->getInvoiceId()}?gw-dev-app-key=" . PIX_APPLICATION_KEY);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: Bearer ' . self::accessToken()]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        return json_decode($response, true);
    }


}