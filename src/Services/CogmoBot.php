<?php

namespace App\Services;

use App\Helpers\Utils;
use App\Models\Entities\CogmoSchedule;
use App\Models\Entities\User;

class CogmoBot
{
    private static $key = 'a1b2c3d4e5f6g7h8i9j0k1l2m3n4o5p6';

    public static function index(User $user, CogmoSchedule $schedule, string $totalDebit): array
    {
        $diasVencidos = (new \DateTime()) < $schedule->getTransaction()->getDataVencimento() ? "" : (new \DateTime())->diff($schedule->getTransaction()->getDataVencimento())->days . " dias";
        $urlPayment = "https://novo.org.br/faturas/" . base64_encode($schedule->getTransaction()->getId());

        // Headers
        $headers = [
            'Content-Type: application/json', 
            'Accept: application/json', 
            "API-SECRET-KEY: " . self::$key
        ];

        // Body
        $body = [
            "usuario_telefone" => Utils::onlyNumbers($schedule->getContactNumber()),
            "evolution_api_instancia"=> $schedule->getPhoneNumber()->getName(),
            "variables" => [
                [
                    "name" => "evolutionapi_instancia",
                    "value" => $schedule->getPhoneNumber()->getName()
                ],  
                [
                    "name" => "pushName",
                    "value" => $user->getName()
                ],
                [
                    "name" => "usuario_cpf",
                    "value" => $user->getCpf()
                ],
                [
                    "name" => "usuario_email",
                    "value" => $user->getEmail()
                ],
                [
                    "name" => "usuario_url_pagamento",
                    "value" => $schedule->getTransaction()->getUrl() ?: $urlPayment
                ],
                [
                    "name" => "usuario_forma_pagamento",
                    "value" => $schedule->getTransaction()->getPaymentTypeString()
                ],
                [
                    "name" => "usuario_tempo_inadimplencia",
                    "value" => $diasVencidos
                ],
                [
                    "name" => "usuario_vencimento_cartao",
                    "value" => ""
                ],
                [
                    "name" => "usuario_erro_cartao",
                    "value" => ""
                ],
                [
                    "name" => "usuario_codigo_boleto",
                    "value" => $schedule->getTransaction()->getCodigoBarras() ?: "",
                ],
                [
                    "name" => "usuario_outros",
                    "value" => $schedule->getMessage()
                ],
                [
                    "name" => "usuario_vencimento_boleto",
                    "value" => $schedule->getTransaction()->getDataVencimento()->format('d/m/Y')
                ],
                [
                    "name" => "valores_pendentes",
                    "value" => $totalDebit
                ],
                [
                    "name" => "usuario_status_cartao",
                    "value" => ""
                ],
                [
                    "name" => "usuario_valor",
                    "value" => $schedule->getTransaction()->getValue()
                ],
            ]
        ];
        // Curl
        $ch = curl_init("https://ia-api.novo.org.br/trigger");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        // Error
        $err = curl_error($ch);
        if($err) {
            return ['error' => $err];
        }
        // Result
        $result = json_decode($result, true);
        curl_close($ch);
        return $result;
    }
}