<?php

namespace App\Services;

class Jornada2024
{

    public static function isMilitary(string $cpf): bool
    {
        if (ENV === 'local') return true;
        $ch = curl_init("https://jornada2024.novo.org.br/api/militar/{$cpf}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result, true);
        return $result['status'];
    }

}
