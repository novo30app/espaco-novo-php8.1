<?php

namespace App\Services;


use App\Helpers\Utils;
use App\Models\Commons\Entities\LibertasCourseBuy;
use App\Models\Entities\Address;
use Prhost\Bling\Bling;

class BlingLibertas
{
    const CLIENT_ID = 'a67509f56d0d5dc041a3e82f2afd121b5c0c291e';
    const CLIENT_SECRET = '9ed145f26532f9c537ec9ad6a521bf3aba8e54be2366f3d3cc0fb4f4821f';


    public static function generateAcessToken()
    {
        //link para atualizar o code https://www.bling.com.br/cadastro.aplicativos.php#/form/28690
        //lhttps://www.bling.com.br/Api/v3/oauth/authorize?response_type=code&client_id=a67509f56d0d5dc041a3e82f2afd121b5c0c291e&state=b1673ca6677398c0180e69c58946056e
        $code = '';
        $token = self::CLIENT_ID . ':' . self::CLIENT_SECRET;
        $token = base64_encode($token);
        $ch = curl_init('https://bling.com.br/Api/v3/oauth/token');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=authorization_code&code=" . $code);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/x-www-form-urlencoded',
            "Authorization: Basic {$token}"]);
        $result = curl_exec($ch);
        curl_close($ch);
        die($result);
        $result = json_decode($result);
        return $result->access_token;
    }

    public static function updateAcessToken(string $refreshToken): array
    {
        $token = self::CLIENT_ID . ':' . self::CLIENT_SECRET;
        $token = base64_encode($token);
        $ch = curl_init('https://bling.com.br/Api/v3/oauth/token');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=refresh_token&refresh_token=" . $refreshToken);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/x-www-form-urlencoded',
            "Authorization: Basic {$token}"]);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result, true);
    }


    public static function nf(string $acessToken, LibertasCourseBuy $courseBuy, string $endereco, string $uf, string $cidade): array
    {
        $params = [];
        $params['contato']['nome'] = $courseBuy->getUser()->getName();
        $params['contato']['numeroDocumento'] = Utils::onlyNumbers($courseBuy->getUser()->getCpf());
        $params['contato']['email'] = $courseBuy->getUser()->getEmail();
        $params['contato']['endereco']['uf'] = $uf;
        $params['contato']['endereco']['municipio'] = $cidade;
        $params['contato']['endereco']['bairro'] = $endereco;
        $params['servicos'][0]['codigo'] = '08.02';
        $params['servicos'][0]['descricao'] = $courseBuy->getCourse()->getTitle();
        $params['servicos'][0]['valor'] = $courseBuy->getPrice();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.bling.com.br/Api/v3/nfse');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json',
            'Authorization: Bearer ' . $acessToken]);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        $response = curl_exec($ch);
        curl_close($ch);
        return json_decode($response, true);
    }


}