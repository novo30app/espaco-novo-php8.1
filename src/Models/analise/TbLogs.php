<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbLogs
 *
 * @ORM\Table(name="tb_logs")
 * @ORM\Entity
 */
class TbLogs
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="type", type="string", length=45, nullable=true)
     */
    private $type;

    /**
     * @var string|null
     *
     * @ORM\Column(name="communicated", type="text", length=65535, nullable=true)
     */
    private $content;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="inserted", type="datetime", nullable=true)
     */
    private $inserted;


}
