<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbEventosParticipantesBackup
 *
 * @ORM\Table(name="tb_eventos_participantes_backup")
 * @ORM\Entity
 */
class TbEventosParticipantesBackup
{
    /**
     * @var int
     *
     * @ORM\Column(name="id1", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id1;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     */
    private $id = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="tb_eventos_id", type="integer", nullable=false)
     */
    private $tbEventosId;

    /**
     * @var int
     *
     * @ORM\Column(name="tb_eventos_precos_id", type="integer", nullable=false)
     */
    private $tbEventosPrecosId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_eventos_transacao_id", type="integer", nullable=true)
     */
    private $tbEventosTransacaoId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nome", type="string", length=100, nullable=true)
     */
    private $nome;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_nascimento", type="date", nullable=true)
     */
    private $dataNascimento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cpf", type="string", length=25, nullable=true)
     */
    private $cpf;

    /**
     * @var int|null
     *
     * @ORM\Column(name="telefone_numero", type="integer", nullable=true)
     */
    private $telefoneNumero;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_estado_id", type="integer", nullable=true)
     */
    private $tbEstadoId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="estado", type="string", length=255, nullable=true)
     */
    private $estado;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_cidade_id", type="integer", nullable=true)
     */
    private $tbCidadeId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cidade", type="string", length=255, nullable=true)
     */
    private $cidade;

    /**
     * @var int|null
     *
     * @ORM\Column(name="cep", type="integer", nullable=true)
     */
    private $cep;

    /**
     * @var string|null
     *
     * @ORM\Column(name="endereco", type="string", length=150, nullable=true)
     */
    private $endereco;

    /**
     * @var int|null
     *
     * @ORM\Column(name="numero", type="integer", nullable=true)
     */
    private $numero;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bairro", type="string", length=45, nullable=true)
     */
    private $bairro;

    /**
     * @var string|null
     *
     * @ORM\Column(name="code", type="string", length=100, nullable=true)
     */
    private $code;

    /**
     * @var string|null
     *
     * @ORM\Column(name="valor", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $valor;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=0, nullable=false, options={"default"="pendente"})
     */
    private $status = 'pendente';

    /**
     * @var string|null
     *
     * @ORM\Column(name="checkin_hash", type="string", length=80, nullable=true)
     */
    private $checkinHash;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataCriacao = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_checkin", type="datetime", nullable=true)
     */
    private $dataCheckin;


}
