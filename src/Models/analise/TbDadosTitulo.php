<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbDadosTitulo
 *
 * @ORM\Table(name="tb_dados_titulo")
 * @ORM\Entity
 */
class TbDadosTitulo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id1", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="NOM_UF", type="string", length=255, nullable=true)
     */
    private $nomUf;

    /**
     * @var string|null
     *
     * @ORM\Column(name="NOM_LOCALIDADE", type="string", length=255, nullable=true)
     */
    private $nomLocalidade;

    /**
     * @var string|null
     *
     * @ORM\Column(name="NUM_ZONA", type="string", length=255, nullable=true)
     */
    private $numZona;

    /**
     * @var string|null
     *
     * @ORM\Column(name="NOM_PARTIDO", type="string", length=255, nullable=true)
     */
    private $nomPartido;

    /**
     * @var string|null
     *
     * @ORM\Column(name="NUM_INSCRICAO", type="string", length=255, nullable=true)
     */
    private $numInscricao;

    /**
     * @var string|null
     *
     * @ORM\Column(name="NOM_FILIADO", type="string", length=255, nullable=true)
     */
    private $nomFiliado;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DAT_FILIACAO", type="string", length=255, nullable=true)
     */
    private $datFiliacao;

    /**
     * @var string|null
     *
     * @ORM\Column(name="NUM_SECAO", type="string", length=255, nullable=true)
     */
    private $numSecao;

    /**
     * @var string|null
     *
     * @ORM\Column(name="SGL_UF", type="string", length=255, nullable=true)
     */
    private $sglUf;

    /**
     * @var string|null
     *
     * @ORM\Column(name="SGL_PARTIDO", type="string", length=255, nullable=true)
     */
    private $sglPartido;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DES_DOMINIO", type="string", length=255, nullable=true)
     */
    private $desDominio;


}
