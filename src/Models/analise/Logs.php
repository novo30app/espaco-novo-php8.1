<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Logs
 *
 * @ORM\Table(name="logs")
 * @ORM\Entity
 */
class Logs
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="method", type="string", length=150, nullable=true)
     */
    private $method;

    /**
     * @var string|null
     *
     * @ORM\Column(name="communicated", type="text", length=65535, nullable=true)
     */
    private $content;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="inserted", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $inserted = 'CURRENT_TIMESTAMP';


}
