<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbTransacoesAuxiliar
 *
 * @ORM\Table(name="tb_transacoes_auxiliar", indexes={@ORM\Index(name="TB_TRANSACAO_ID", columns={"tb_transacao_id"})})
 * @ORM\Entity
 */
class TbTransacoesAuxiliar
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_transacao_id", type="integer", nullable=true)
     */
    private $tbTransacaoId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nome_cliente", type="string", length=80, nullable=true)
     */
    private $nomeCliente;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cpf_cnpj", type="string", length=20, nullable=true)
     */
    private $cpfCnpj;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_pagamento", type="datetime", nullable=true)
     */
    private $dataPagamento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="valor_pago", type="decimal", precision=13, scale=4, nullable=true)
     */
    private $valorPago;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tipo", type="string", length=40, nullable=true)
     */
    private $tipo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="n_banco", type="string", length=20, nullable=true)
     */
    private $nBanco;

    /**
     * @var string|null
     *
     * @ORM\Column(name="n_agencia", type="string", length=20, nullable=true)
     */
    private $nAgencia;

    /**
     * @var string|null
     *
     * @ORM\Column(name="n_conta_corrente", type="string", length=20, nullable=true)
     */
    private $nContaCorrente;

    /**
     * @var string|null
     *
     * @ORM\Column(name="n_cheque", type="string", length=30, nullable=true)
     */
    private $nCheque;

    /**
     * @var string|null
     *
     * @ORM\Column(name="n_operacao", type="string", length=80, nullable=true)
     */
    private $nOperacao;

    /**
     * @var string|null
     *
     * @ORM\Column(name="origem", type="string", length=80, nullable=true)
     */
    private $origem;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codigo_operacao", type="string", length=100, nullable=true)
     */
    private $codigoOperacao;

    /**
     * @var string|null
     *
     * @ORM\Column(name="estimavel", type="string", length=200, nullable=true)
     */
    private $estimavel;

    /**
     * @var string|null
     *
     * @ORM\Column(name="n_recibo", type="string", length=40, nullable=true)
     */
    private $nRecibo;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_diretorio_id", type="integer", nullable=true, options={"default"="1"})
     */
    private $tbDiretorioId = '1';

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_diretorio_origem", type="integer", nullable=true)
     */
    private $tbDiretorioOrigem;


}
