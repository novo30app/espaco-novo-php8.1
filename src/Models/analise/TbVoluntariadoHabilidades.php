<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbVoluntariadoHabilidades
 *
 * @ORM\Table(name="tb_voluntariado_habilidades")
 * @ORM\Entity
 */
class TbVoluntariadoHabilidades
{
    /**
     * @var int
     *
     * @ORM\Column(name="id1", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id1;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_voluntariado_id", type="integer", nullable=true)
     */
    private $tbVoluntariadoId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_voluntariado_opcoes_id", type="integer", nullable=true)
     */
    private $tbVoluntariadoOpcoesId;


}
