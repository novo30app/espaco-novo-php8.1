<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbSuspensao
 *
 * @ORM\Table(name="tb_suspensao")
 * @ORM\Entity
 */
class TbSuspensao
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="tb_pessoa_id", type="integer", nullable=false)
     */
    private $tbPessoaId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_suspensao", type="datetime", nullable=true)
     */
    private $dataSuspensao;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_retira_suspensao", type="datetime", nullable=true)
     */
    private $dataRetiraSuspensao;


}
