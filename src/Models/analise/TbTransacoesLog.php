<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbTransacoesLog
 *
 * @ORM\Table(name="tb_transacoes_log", indexes={@ORM\Index(name="idx_tb_diretorio_nome", columns={"acao"})})
 * @ORM\Entity
 */
class TbTransacoesLog
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="tb_transacao_id", type="integer", nullable=false)
     */
    private $tbTransacaoId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_log", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataLog = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="acao", type="string", length=255, nullable=false)
     */
    private $acao;

    /**
     * @var string|null
     *
     * @ORM\Column(name="json", type="text", length=0, nullable=true)
     */
    private $json;


}
