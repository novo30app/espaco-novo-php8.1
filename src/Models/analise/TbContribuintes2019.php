<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbContribuintes2019
 *
 * @ORM\Table(name="tb_contribuintes_2019")
 * @ORM\Entity
 */
class TbContribuintes2019
{
    /**
     * @var int
     *
     * @ORM\Column(name="id1", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id1;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     */
    private $id = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="nome", type="string", length=100, nullable=true)
     */
    private $nome;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cpf", type="string", length=25, nullable=true)
     */
    private $cpf;

    /**
     * @var string|null
     *
     * @ORM\Column(name="titulo_eleitoral_municipio", type="string", length=150, nullable=true)
     */
    private $tituloEleitoralMunicipio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="titulo_eleitoral_uf", type="string", length=100, nullable=true)
     */
    private $tituloEleitoralUf;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cidade residência", type="string", length=255, nullable=true)
     */
    private $cidadeResidência;

    /**
     * @var string|null
     *
     * @ORM\Column(name="estado residência", type="string", length=255, nullable=true)
     */
    private $estadoResidência;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="filiado", type="boolean", nullable=true)
     */
    private $filiado = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="número de contribuições em 2019", type="bigint", nullable=false)
     */
    private $númeroDeContribuiçõesEm2019 = '0';


}
