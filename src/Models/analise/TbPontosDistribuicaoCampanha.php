<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbPontosDistribuicaoCampanha
 *
 * @ORM\Table(name="tb_pontos_distribuicao_campanha")
 * @ORM\Entity
 */
class TbPontosDistribuicaoCampanha
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=100, nullable=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="endereco", type="string", length=255, nullable=false)
     */
    private $endereco;

    /**
     * @var string
     *
     * @ORM\Column(name="bairro", type="string", length=100, nullable=false)
     */
    private $bairro;

    /**
     * @var string
     *
     * @ORM\Column(name="cidade", type="string", length=100, nullable=false)
     */
    private $cidade;

    /**
     * @var string
     *
     * @ORM\Column(name="uf", type="string", length=3, nullable=false)
     */
    private $uf;

    /**
     * @var string
     *
     * @ORM\Column(name="cep", type="string", length=10, nullable=false)
     */
    private $cep;

    /**
     * @var float
     *
     * @ORM\Column(name="lat", type="float", precision=10, scale=7, nullable=false)
     */
    private $lat;

    /**
     * @var float
     *
     * @ORM\Column(name="lng", type="float", precision=10, scale=7, nullable=false)
     */
    private $lng;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ddd", type="string", length=5, nullable=true)
     */
    private $ddd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telefone", type="string", length=30, nullable=true)
     */
    private $telefone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="exibe_endereco", type="string", length=1, nullable=true)
     */
    private $exibeEndereco;


}
