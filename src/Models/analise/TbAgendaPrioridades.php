<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbAgendaPrioridades
 *
 * @ORM\Table(name="tb_agenda_prioridades")
 * @ORM\Entity
 */
class TbAgendaPrioridades
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prioridade", type="string", length=100, nullable=true)
     */
    private $prioridade;


}
