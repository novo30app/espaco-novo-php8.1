<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbCandidatos
 *
 * @ORM\Table(name="tb_candidatos")
 * @ORM\Entity
 */
class TbCandidatos
{
    /**
     * @var int
     *
     * @ORM\Column(name="id1", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id1;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     */
    private $id = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="filiado_id", type="integer", nullable=true)
     */
    private $filiadoId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="token", type="string", length=60, nullable=true)
     */
    private $token;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="senha", type="string", length=60, nullable=true)
     */
    private $senha;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nome", type="string", length=100, nullable=true)
     */
    private $nome;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cpf", type="string", length=25, nullable=true)
     */
    private $cpf;

    /**
     * @var string|null
     *
     * @ORM\Column(name="rg", type="string", length=25, nullable=true)
     */
    private $rg;

    /**
     * @var string|null
     *
     * @ORM\Column(name="orgao_expedidor", type="string", length=10, nullable=true)
     */
    private $orgaoExpedidor;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_nascimento", type="date", nullable=true)
     */
    private $dataNascimento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="titulo_eleitoral", type="string", length=25, nullable=true)
     */
    private $tituloEleitoral;

    /**
     * @var string|null
     *
     * @ORM\Column(name="titulo_eleitoral_zona", type="string", length=4, nullable=true)
     */
    private $tituloEleitoralZona;

    /**
     * @var string|null
     *
     * @ORM\Column(name="titulo_eleitoral_secao", type="string", length=4, nullable=true)
     */
    private $tituloEleitoralSecao;

    /**
     * @var string|null
     *
     * @ORM\Column(name="titulo_eleitoral_pais", type="string", length=100, nullable=true)
     */
    private $tituloEleitoralPais;

    /**
     * @var int|null
     *
     * @ORM\Column(name="titulo_eleitoral_pais_id", type="integer", nullable=true)
     */
    private $tituloEleitoralPaisId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="titulo_eleitoral_uf", type="string", length=100, nullable=true)
     */
    private $tituloEleitoralUf;

    /**
     * @var int|null
     *
     * @ORM\Column(name="titulo_eleitoral_uf_id", type="integer", nullable=true)
     */
    private $tituloEleitoralUfId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="titulo_eleitoral_municipio", type="string", length=150, nullable=true)
     */
    private $tituloEleitoralMunicipio;

    /**
     * @var int|null
     *
     * @ORM\Column(name="titulo_eleitoral_municipio_id", type="integer", nullable=true)
     */
    private $tituloEleitoralMunicipioId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nome_mae", type="string", length=100, nullable=true)
     */
    private $nomeMae;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="estado_civil", type="boolean", nullable=true)
     */
    private $estadoCivil;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="genero", type="boolean", nullable=true)
     */
    private $genero;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="escolaridade", type="boolean", nullable=true)
     */
    private $escolaridade;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="opcao_religiosa", type="boolean", nullable=true)
     */
    private $opcaoReligiosa;

    /**
     * @var string|null
     *
     * @ORM\Column(name="profissao", type="string", length=100, nullable=true)
     */
    private $profissao;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="filiacao", type="boolean", nullable=true)
     */
    private $filiacao;

    /**
     * @var string|null
     *
     * @ORM\Column(name="filiacao_qual_partido", type="string", length=45, nullable=true)
     */
    private $filiacaoQualPartido;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="termo_aceite_1", type="boolean", nullable=true)
     */
    private $termoAceite1;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="termo_aceite_2", type="boolean", nullable=true)
     */
    private $termoAceite2;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="termo_aceite_3", type="boolean", nullable=true)
     */
    private $termoAceite3;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="status", type="boolean", nullable=true, options={"default"="1"})
     */
    private $status = '1';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataCriacao = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_solicitacao_filiacao", type="date", nullable=true)
     */
    private $dataSolicitacaoFiliacao;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_solicitacao_refiliacao", type="date", nullable=true)
     */
    private $dataSolicitacaoRefiliacao;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_filiacao", type="date", nullable=true)
     */
    private $dataFiliacao;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_desfiliacao", type="date", nullable=true)
     */
    private $dataDesfiliacao;

    /**
     * @var string|null
     *
     * @ORM\Column(name="motivo_desfiliacao", type="text", length=65535, nullable=true)
     */
    private $motivoDesfiliacao;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_refiliacao", type="date", nullable=true)
     */
    private $dataRefiliacao;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_cancelamento_pedido", type="date", nullable=true)
     */
    private $dataCancelamentoPedido;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_impugnacao", type="date", nullable=true)
     */
    private $dataImpugnacao;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_validade_ativo", type="date", nullable=true)
     */
    private $dataValidadeAtivo;

    /**
     * @var bool
     *
     * @ORM\Column(name="doador", type="boolean", nullable=false)
     */
    private $doador = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="filiado", type="boolean", nullable=true)
     */
    private $filiado = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="bloqueado", type="boolean", nullable=true)
     */
    private $bloqueado;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_motivo_id", type="integer", nullable=true)
     */
    private $tbMotivoId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="observacao", type="string", length=4000, nullable=true)
     */
    private $observacao;

    /**
     * @var string|null
     *
     * @ORM\Column(name="token_ad", type="string", length=255, nullable=true)
     */
    private $tokenAd;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="candidatura", type="boolean", nullable=true)
     */
    private $candidatura = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="email_confirmado", type="boolean", nullable=true)
     */
    private $emailConfirmado = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="A", type="string", length=80, nullable=true)
     */
    private $a;


}
