<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbPessoaReciboOffline
 *
 * @ORM\Table(name="tb_pessoa_recibo_offline")
 * @ORM\Entity
 */
class TbPessoaReciboOffline
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nome", type="string", length=255, nullable=true)
     */
    private $nome;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cpf", type="string", length=20, nullable=true)
     */
    private $cpf;

    /**
     * @var string|null
     *
     * @ORM\Column(name="endereco", type="string", length=400, nullable=true)
     */
    private $endereco;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telefone", type="string", length=45, nullable=true)
     */
    private $telefone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="banco", type="string", length=45, nullable=true)
     */
    private $banco;

    /**
     * @var string|null
     *
     * @ORM\Column(name="agencia", type="string", length=45, nullable=true)
     */
    private $agencia;

    /**
     * @var string|null
     *
     * @ORM\Column(name="contacorrente", type="string", length=45, nullable=true)
     */
    private $contacorrente;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cheque", type="string", length=45, nullable=true)
     */
    private $cheque;

    /**
     * @var string|null
     *
     * @ORM\Column(name="doc", type="string", length=45, nullable=true)
     */
    private $doc;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bem_estimavel", type="string", length=400, nullable=true)
     */
    private $bemEstimavel;

    /**
     * @var string|null
     *
     * @ORM\Column(name="valor", type="string", length=45, nullable=true)
     */
    private $valor;

    /**
     * @var string|null
     *
     * @ORM\Column(name="outra_forma_arrecadacao", type="string", length=400, nullable=true)
     */
    private $outraFormaArrecadacao;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nome_originario", type="string", length=255, nullable=true)
     */
    private $nomeOriginario;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nome_responsavel_emissao", type="string", length=255, nullable=true)
     */
    private $nomeResponsavelEmissao;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cpf_originario", type="string", length=20, nullable=true)
     */
    private $cpfOriginario;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cpf_responsavel_emissao", type="string", length=20, nullable=true)
     */
    private $cpfResponsavelEmissao;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_pago", type="date", nullable=true)
     */
    private $dataPago;

    /**
     * @var string|null
     *
     * @ORM\Column(name="numero", type="string", length=45, nullable=true)
     */
    private $numero;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataCriacao = 'CURRENT_TIMESTAMP';


}
