<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbWhitelist
 *
 * @ORM\Table(name="tb_whitelist")
 * @ORM\Entity
 */
class TbWhitelist
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ip", type="string", length=45, nullable=true)
     */
    private $ip;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="inserted", type="datetime", nullable=true)
     */
    private $inserted;


}
