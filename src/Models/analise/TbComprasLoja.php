<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbComprasLoja
 *
 * @ORM\Table(name="tb_compras_loja")
 * @ORM\Entity
 */
class TbComprasLoja
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="data", type="string", length=45, nullable=true)
     */
    private $data;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nome", type="string", length=100, nullable=true)
     */
    private $nome;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cpf", type="string", length=20, nullable=true)
     */
    private $cpf;

    /**
     * @var int|null
     *
     * @ORM\Column(name="nota_fiscal", type="integer", nullable=true)
     */
    private $notaFiscal;

    /**
     * @var string|null
     *
     * @ORM\Column(name="valor", type="string", length=20, nullable=true)
     */
    private $valor;

    /**
     * @var string|null
     *
     * @ORM\Column(name="status", type="string", length=50, nullable=true)
     */
    private $status;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="gateway", type="string", length=30, nullable=true)
     */
    private $gateway;

    /**
     * @var string|null
     *
     * @ORM\Column(name="invoice", type="string", length=100, nullable=true)
     */
    private $invoice;

    /**
     * @var string|null
     *
     * @ORM\Column(name="data_pagamento", type="string", length=45, nullable=true)
     */
    private $dataPagamento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="forma_pagamento", type="string", length=20, nullable=true)
     */
    private $formaPagamento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tarifa", type="string", length=20, nullable=true)
     */
    private $tarifa;

    /**
     * @var string|null
     *
     * @ORM\Column(name="valor_liquido", type="string", length=20, nullable=true)
     */
    private $valorLiquido;


}
