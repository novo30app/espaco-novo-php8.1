<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbPessoaIuguAssinaturaRenovacao
 *
 * @ORM\Table(name="tb_pessoa_iugu_assinatura_renovacao")
 * @ORM\Entity
 */
class TbPessoaIuguAssinaturaRenovacao
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="subscription_id", type="string", length=100, nullable=true)
     */
    private $subscriptionId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataCriacao = 'CURRENT_TIMESTAMP';


}
