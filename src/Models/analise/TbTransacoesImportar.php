<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbTransacoesImportar
 *
 * @ORM\Table(name="tb_transacoes_importar", indexes={@ORM\Index(name="idx_tb_transacoes_importar_invoice", columns={"invoice"})})
 * @ORM\Entity
 */
class TbTransacoesImportar
{
    /**
     * @var int
     *
     * @ORM\Column(name="id1", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="invoice", type="string", length=20, nullable=true)
     */
    private $invoice;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tse", type="string", length=60, nullable=true)
     */
    private $tse;

    /**
     * @var string|null
     *
     * @ORM\Column(name="id", type="string", length=20, nullable=true)
     */
    private $id;


}
