<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbPessoaRede
 *
 * @ORM\Table(name="tb_pessoa_rede", indexes={@ORM\Index(name="idx_pessoa", columns={"tb_pessoa_id"})})
 * @ORM\Entity
 */
class TbPessoaRede
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="customer_id", type="string", length=45, nullable=true)
     */
    private $customerId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataCriacao = 'CURRENT_TIMESTAMP';


}
