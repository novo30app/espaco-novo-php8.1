<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbDoacaoAvulsa
 *
 * @ORM\Table(name="tb_doacao_avulsa")
 * @ORM\Entity
 */
class TbDoacaoAvulsa
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="tb_pessoa_id", type="integer", nullable=false)
     */
    private $tbPessoaId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_transacao_id", type="integer", nullable=true)
     */
    private $tbTransacaoId;

    /**
     * @var int
     *
     * @ORM\Column(name="diretorio_id", type="integer", nullable=false)
     */
    private $diretorioId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="recorrencias", type="integer", nullable=true)
     */
    private $recorrencias;

    /**
     * @var string|null
     *
     * @ORM\Column(name="customer_id", type="string", length=100, nullable=true)
     */
    private $customerId;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="gateway_pagamento", type="boolean", nullable=true)
     */
    private $gatewayPagamento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="gateway_status", type="string", length=100, nullable=true)
     */
    private $gatewayStatus;

    /**
     * @var string|null
     *
     * @ORM\Column(name="valor_parcial", type="decimal", precision=13, scale=4, nullable=true)
     */
    private $valorParcial;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_total", type="decimal", precision=13, scale=4, nullable=false)
     */
    private $valorTotal;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="forma_pagamento", type="boolean", nullable=true)
     */
    private $formaPagamento;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="origem_transacao", type="boolean", nullable=true)
     */
    private $origemTransacao;

    /**
     * @var string|null
     *
     * @ORM\Column(name="status", type="string", length=10, nullable=true)
     */
    private $status;

    /**
     * @var string|null
     *
     * @ORM\Column(name="token_cartao_credito", type="string", length=255, nullable=true)
     */
    private $tokenCartaoCredito;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="iniciado_em", type="datetime", nullable=false)
     */
    private $iniciadoEm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="finalizado_em", type="datetime", nullable=false)
     */
    private $finalizadoEm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="criado_em", type="datetime", nullable=false)
     */
    private $criadoEm;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="alterado_em", type="datetime", nullable=true)
     */
    private $alteradoEm;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="removido_em", type="datetime", nullable=true)
     */
    private $removidoEm;

    /**
     * @var string|null
     *
     * @ORM\Column(name="campanha_ja", type="string", length=1, nullable=true, options={"fixed"=true})
     */
    private $campanhaJa;


}
