<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbEventosPrecos
 *
 * @ORM\Table(name="tb_eventos_precos", indexes={@ORM\Index(name="idx_tb_eventos_precos_descricao", columns={"descricao"}), @ORM\Index(name="fk_tb_preco_tb_eventos_idx", columns={"tb_eventos_id"})})
 * @ORM\Entity
 */
class TbEventosPrecos
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="string", length=255, nullable=false)
     */
    private $descricao;

    /**
     * @var string|null
     *
     * @ORM\Column(name="valor", type="decimal", precision=13, scale=4, nullable=true)
     */
    private $valor;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_inicio", type="datetime", nullable=true)
     */
    private $dataInicio;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_termino", type="datetime", nullable=true)
     */
    private $dataTermino;

    /**
     * @var int|null
     *
     * @ORM\Column(name="limite_inscritos", type="integer", nullable=true)
     */
    private $limiteInscritos;

    /**
     * @var int|null
     *
     * @ORM\Column(name="limite_total", type="integer", nullable=true)
     */
    private $limiteTotal;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="apenas_filiados", type="boolean", nullable=true)
     */
    private $apenasFiliados = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="status", type="string", length=0, nullable=true, options={"default"="ativo"})
     */
    private $status = 'ativo';

    /**
     * @var \TbEventos
     *
     * @ORM\ManyToOne(targetEntity="TbEventos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tb_eventos_id", referencedColumnName="id")
     * })
     */
    private $tbEventos;


}
