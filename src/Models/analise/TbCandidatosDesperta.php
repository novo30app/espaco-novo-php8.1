<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbCandidatosDesperta
 *
 * @ORM\Table(name="tb_candidatos_desperta")
 * @ORM\Entity
 */
class TbCandidatosDesperta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id1", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id1;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     */
    private $id = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="quem_indicou", type="string", length=120, nullable=true)
     */
    private $quemIndicou;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nome", type="string", length=120, nullable=true)
     */
    private $nome;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sobrenome", type="string", length=120, nullable=true)
     */
    private $sobrenome;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=200, nullable=true)
     */
    private $email;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="genero", type="boolean", nullable=true)
     */
    private $genero;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cidade", type="string", length=255, nullable=true)
     */
    private $cidade;

    /**
     * @var string|null
     *
     * @ORM\Column(name="estado", type="string", length=255, nullable=true)
     */
    private $estado;

    /**
     * @var string|null
     *
     * @ORM\Column(name="habilidades", type="text", length=65535, nullable=true)
     */
    private $habilidades;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=true)
     */
    private $dataCriacao;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_atualizacao", type="datetime", nullable=true)
     */
    private $dataAtualizacao;


}
