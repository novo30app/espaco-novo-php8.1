<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbBateMailing
 *
 * @ORM\Table(name="tb_bate_MAILING", indexes={@ORM\Index(name="idx_tb_bate_MAILING_cpf", columns={"cpf"})})
 * @ORM\Entity
 */
class TbBateMailing
{
    /**
     * @var int
     *
     * @ORM\Column(name="id1", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id1;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data", type="date", nullable=true)
     */
    private $data;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nome", type="string", length=30, nullable=true)
     */
    private $nome;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cpf", type="string", length=30, nullable=true)
     */
    private $cpf;

    /**
     * @var string|null
     *
     * @ORM\Column(name="valor_bruto", type="decimal", precision=13, scale=4, nullable=true)
     */
    private $valorBruto;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="numero_recibo", type="integer", nullable=true)
     */
    private $numeroRecibo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cod_transacao", type="string", length=20, nullable=true)
     */
    private $codTransacao;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sistema", type="string", length=10, nullable=true)
     */
    private $sistema;

    /**
     * @var string|null
     *
     * @ORM\Column(name="gateway", type="string", length=10, nullable=true)
     */
    private $gateway;

    /**
     * @var string|null
     *
     * @ORM\Column(name="origem", type="string", length=10, nullable=true)
     */
    private $origem;

    /**
     * @var string|null
     *
     * @ORM\Column(name="subir", type="string", length=2, nullable=true)
     */
    private $subir;


}
