<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbConteudoBackup
 *
 * @ORM\Table(name="tb_conteudo_backup")
 * @ORM\Entity
 */
class TbConteudoBackup
{
    /**
     * @var int
     *
     * @ORM\Column(name="id1", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id1;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     */
    private $id = '0';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data", type="date", nullable=true)
     */
    private $data;

    /**
     * @var string|null
     *
     * @ORM\Column(name="titulo", type="string", length=255, nullable=true)
     */
    private $titulo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="conteudo", type="text", length=65535, nullable=true)
     */
    private $conteudo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="arquivo", type="string", length=200, nullable=true)
     */
    private $arquivo;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="restrito", type="boolean", nullable=true)
     */
    private $restrito;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="exibir", type="boolean", nullable=true)
     */
    private $exibir;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="comite", type="boolean", nullable=true)
     */
    private $comite;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="comite_consultas", type="boolean", nullable=true)
     */
    private $comiteConsultas;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="resolucao", type="boolean", nullable=true)
     */
    private $resolucao;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="link", type="boolean", nullable=true)
     */
    private $link;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataCriacao = 'CURRENT_TIMESTAMP';

    /**
     * @var string|null
     *
     * @ORM\Column(name="diretorio", type="string", length=25, nullable=true)
     */
    private $diretorio;


}
