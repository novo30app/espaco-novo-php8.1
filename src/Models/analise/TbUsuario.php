<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbUsuario
 *
 * @ORM\Table(name="tb_usuario")
 * @ORM\Entity
 */
class TbUsuario
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nome", type="string", length=100, nullable=true)
     */
    private $nome;

    /**
     * @var string|null
     *
     * @ORM\Column(name="usuario", type="string", length=45, nullable=true)
     */
    private $usuario;

    /**
     * @var string|null
     *
     * @ORM\Column(name="senha", type="string", length=50, nullable=true)
     */
    private $senha;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataCriacao = 'CURRENT_TIMESTAMP';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="visivel", type="boolean", nullable=true, options={"default"="1"})
     */
    private $visivel = '1';


}
