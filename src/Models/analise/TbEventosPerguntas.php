<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbEventosPerguntas
 *
 * @ORM\Table(name="tb_eventos_perguntas")
 * @ORM\Entity
 */
class TbEventosPerguntas
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="tb_eventos_id", type="integer", nullable=false)
     */
    private $tbEventosId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tipo", type="string", length=0, nullable=true, options={"default"="texto"})
     */
    private $tipo = 'texto';

    /**
     * @var string|null
     *
     * @ORM\Column(name="descricao", type="string", length=255, nullable=true)
     */
    private $descricao;

    /**
     * @var string|null
     *
     * @ORM\Column(name="status", type="string", length=0, nullable=true, options={"default"="ativo"})
     */
    private $status = 'ativo';


}
