<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbAgendaLegislativaBancadaAdmin
 *
 * @ORM\Table(name="tb_agenda_legislativa_bancada_admin")
 * @ORM\Entity
 */
class TbAgendaLegislativaBancadaAdmin
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_criacao", type="date", nullable=true)
     */
    private $dataCriacao;

    /**
     * @var string|null
     *
     * @ORM\Column(name="diretorio", type="string", length=50, nullable=true)
     */
    private $diretorio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sugestao1_tema", type="string", length=50, nullable=true)
     */
    private $sugestao1Tema;

    /**
     * @var string|null
     *
     * @ORM\Column(name="diagnostico_sugestao1", type="string", length=1000, nullable=true)
     */
    private $diagnosticoSugestao1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="proposta_sugestao1", type="string", length=1000, nullable=true)
     */
    private $propostaSugestao1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sugestoes1", type="string", length=3, nullable=true)
     */
    private $sugestoes1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sugestao2_tema", type="string", length=50, nullable=true)
     */
    private $sugestao2Tema;

    /**
     * @var string|null
     *
     * @ORM\Column(name="diagnostico_sugestao2", type="string", length=1000, nullable=true)
     */
    private $diagnosticoSugestao2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="proposta_sugestao2", type="string", length=1000, nullable=true)
     */
    private $propostaSugestao2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sugestoes2", type="string", length=3, nullable=true)
     */
    private $sugestoes2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sugestao3_tema", type="string", length=50, nullable=true)
     */
    private $sugestao3Tema;

    /**
     * @var string|null
     *
     * @ORM\Column(name="diagnostico_sugestao3", type="string", length=1000, nullable=true)
     */
    private $diagnosticoSugestao3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="proposta_sugestao3", type="string", length=1000, nullable=true)
     */
    private $propostaSugestao3;


}
