<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbValoresDoacao
 *
 * @ORM\Table(name="tb_valores_doacao")
 * @ORM\Entity
 */
class TbValoresDoacao
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="valor", type="decimal", precision=15, scale=2, nullable=true)
     */
    private $valor;

    /**
     * @var string|null
     *
     * @ORM\Column(name="periodo", type="string", length=45, nullable=true)
     */
    private $periodo;


}
