<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TotvsLogs
 *
 * @ORM\Table(name="totvs_logs")
 * @ORM\Entity
 */
class TotvsLogs
{
    /**
     * @var int
     *
     * @ORM\Column(name="id1", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id1;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id_transacao", type="integer", nullable=true)
     */
    private $idTransacao;

    /**
     * @var string|null
     *
     * @ORM\Column(name="erro", type="string", length=300, nullable=true)
     */
    private $erro;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="inserted_in", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $insertedIn = 'CURRENT_TIMESTAMP';


}
