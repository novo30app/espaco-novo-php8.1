<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbValorProcessoSeletivo
 *
 * @ORM\Table(name="tb_valor_processo_seletivo")
 * @ORM\Entity
 */
class TbValorProcessoSeletivo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="valor", type="decimal", precision=15, scale=2, nullable=true)
     */
    private $valor;


}
