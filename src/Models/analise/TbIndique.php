<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbIndique
 *
 * @ORM\Table(name="tb_indique")
 * @ORM\Entity
 */
class TbIndique
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_pessoa_id_indicador", type="integer", nullable=true)
     */
    private $tbPessoaIdIndicador;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nome", type="string", length=100, nullable=true)
     */
    private $nome;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nome_indicador", type="string", length=100, nullable=true)
     */
    private $nomeIndicador;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email_indicador", type="string", length=100, nullable=true)
     */
    private $emailIndicador;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataCriacao = 'CURRENT_TIMESTAMP';


}
