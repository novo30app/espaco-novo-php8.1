<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbCaptcha
 *
 * @ORM\Table(name="tb_captcha", indexes={@ORM\Index(name="word", columns={"word"})})
 * @ORM\Entity
 */
class TbCaptcha
{
    /**
     * @var int
     *
     * @ORM\Column(name="captcha_id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $captchaId;

    /**
     * @var int
     *
     * @ORM\Column(name="captcha_time", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $captchaTime;

    /**
     * @var string
     *
     * @ORM\Column(name="ip_address", type="string", length=45, nullable=false)
     */
    private $ipAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="word", type="string", length=20, nullable=false)
     */
    private $word;


}
