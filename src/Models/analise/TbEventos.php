<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbEventos
 *
 * @ORM\Table(name="tb_eventos", indexes={@ORM\Index(name="idx_tb_eventos_data_inicio", columns={"data_inicio"}), @ORM\Index(name="idx_tb_eventos_diretorio_id", columns={"diretorio_id"}), @ORM\Index(name="idx_tb_eventos_estado_id", columns={"estado_id"}), @ORM\Index(name="idx_tb_eventos_status", columns={"status"}), @ORM\Index(name="idx_tb_eventos_nome", columns={"nome"}), @ORM\Index(name="idx_tb_eventos_cidade_id", columns={"cidade_id"})})
 * @ORM\Entity
 */
class TbEventos
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="diretorio_id", type="integer", nullable=true)
     */
    private $diretorioId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="nucleo_id", type="integer", nullable=true)
     */
    private $nucleoId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="usuario_responsavel_id", type="integer", nullable=true)
     */
    private $usuarioResponsavelId;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=100, nullable=false)
     */
    private $nome;

    /**
     * @var string|null
     *
     * @ORM\Column(name="status", type="string", length=0, nullable=true, options={"default"="pendente"})
     */
    private $status = 'pendente';

    /**
     * @var string|null
     *
     * @ORM\Column(name="tipo", type="string", length=0, nullable=true, options={"default"="pago"})
     */
    private $tipo = 'pago';

    /**
     * @var string|null
     *
     * @ORM\Column(name="endereco", type="string", length=255, nullable=true)
     */
    private $endereco;

    /**
     * @var string|null
     *
     * @ORM\Column(name="local", type="string", length=50, nullable=true)
     */
    private $local;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cep", type="string", length=100, nullable=true)
     */
    private $cep;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bairro", type="string", length=100, nullable=true)
     */
    private $bairro;

    /**
     * @var string|null
     *
     * @ORM\Column(name="complemento", type="string", length=100, nullable=true)
     */
    private $complemento;

    /**
     * @var int|null
     *
     * @ORM\Column(name="cidade_id", type="integer", nullable=true)
     */
    private $cidadeId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="estado_id", type="integer", nullable=true)
     */
    private $estadoId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_inicio", type="datetime", nullable=true)
     */
    private $dataInicio;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_termino", type="datetime", nullable=true)
     */
    private $dataTermino;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_evento", type="datetime", nullable=true)
     */
    private $dataEvento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="path_logo", type="string", length=255, nullable=true)
     */
    private $pathLogo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="path_banner", type="string", length=255, nullable=true)
     */
    private $pathBanner;

    /**
     * @var string|null
     *
     * @ORM\Column(name="link_video", type="string", length=255, nullable=true)
     */
    private $linkVideo;

    /**
     * @var int|null
     *
     * @ORM\Column(name="limite_inscritos", type="integer", nullable=true)
     */
    private $limiteInscritos;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descricao", type="text", length=0, nullable=true)
     */
    private $descricao;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="apenas_filiados", type="boolean", nullable=true)
     */
    private $apenasFiliados;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="oficial_novo", type="boolean", nullable=true)
     */
    private $oficialNovo = '0';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=true)
     */
    private $dataCriacao;

    /**
     * @var int|null
     *
     * @ORM\Column(name="removido", type="smallint", nullable=true)
     */
    private $removido = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="dias_boleto", type="integer", nullable=true)
     */
    private $diasBoleto;


}
