<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbObtem
 *
 * @ORM\Table(name="tb_obtem")
 * @ORM\Entity
 */
class TbObtem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="valores", type="string", length=1000, nullable=true)
     */
    private $valores;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataCriacao = 'CURRENT_TIMESTAMP';


}
