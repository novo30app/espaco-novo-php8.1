<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbObservacoesComentarios
 *
 * @ORM\Table(name="tb_observacoes_comentarios")
 * @ORM\Entity
 */
class TbObservacoesComentarios
{
    /**
     * @var int
     *
     * @ORM\Column(name="id1", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id1;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="observacao", type="string", length=50, nullable=true)
     */
    private $observacao;


}
