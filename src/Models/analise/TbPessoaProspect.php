<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbPessoaProspect
 *
 * @ORM\Table(name="tb_pessoa_prospect", uniqueConstraints={@ORM\UniqueConstraint(name="cpf", columns={"cpf"})})
 * @ORM\Entity
 */
class TbPessoaProspect
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cpf", type="string", length=25, nullable=true)
     */
    private $cpf;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_nascimento", type="date", nullable=true)
     */
    private $dataNascimento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="celular", type="string", length=30, nullable=true)
     */
    private $celular;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cep", type="string", length=10, nullable=true)
     */
    private $cep;

    /**
     * @var string|null
     *
     * @ORM\Column(name="endereco", type="string", length=150, nullable=true)
     */
    private $endereco;

    /**
     * @var string|null
     *
     * @ORM\Column(name="numero", type="string", length=6, nullable=true)
     */
    private $numero;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bairro", type="string", length=25, nullable=true)
     */
    private $bairro;

    /**
     * @var string|null
     *
     * @ORM\Column(name="complemento", type="string", length=200, nullable=true)
     */
    private $complemento;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_estado_id", type="integer", nullable=true)
     */
    private $tbEstadoId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_cidade_id", type="integer", nullable=true)
     */
    private $tbCidadeId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="diretorio", type="string", length=50, nullable=true)
     */
    private $diretorio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;


}
