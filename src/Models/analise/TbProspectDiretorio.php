<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbProspectDiretorio
 *
 * @ORM\Table(name="tb_prospect_diretorio")
 * @ORM\Entity
 */
class TbProspectDiretorio
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_diretorio_id", type="integer", nullable=true)
     */
    private $tbDiretorioId;


}
