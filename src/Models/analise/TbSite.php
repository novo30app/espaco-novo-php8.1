<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbSite
 *
 * @ORM\Table(name="tb_site")
 * @ORM\Entity
 */
class TbSite
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="range_inicio", type="bigint", nullable=true)
     */
    private $rangeInicio;

    /**
     * @var int|null
     *
     * @ORM\Column(name="range_fim", type="bigint", nullable=true)
     */
    private $rangeFim;

    /**
     * @var int|null
     *
     * @ORM\Column(name="valor_recibo_filiado", type="bigint", nullable=true)
     */
    private $valorReciboFiliado;

    /**
     * @var int|null
     *
     * @ORM\Column(name="max_tentativa_cartao", type="bigint", nullable=true)
     */
    private $maxTentativaCartao;


}
