<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbDesperta
 *
 * @ORM\Table(name="tb_desperta", indexes={@ORM\Index(name="idx_tb_pessoa_id", columns={"tb_pessoa_id"}), @ORM\Index(name="idx_email", columns={"email"})})
 * @ORM\Entity
 */
class TbDesperta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="quem_indicou", type="string", length=120, nullable=true)
     */
    private $quemIndicou;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nome", type="string", length=120, nullable=true)
     */
    private $nome;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sobrenome", type="string", length=120, nullable=true)
     */
    private $sobrenome;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=200, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telefone", type="string", length=30, nullable=true)
     */
    private $telefone;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="genero", type="boolean", nullable=true)
     */
    private $genero;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cidade", type="string", length=255, nullable=true)
     */
    private $cidade;

    /**
     * @var string|null
     *
     * @ORM\Column(name="estado", type="string", length=255, nullable=true)
     */
    private $estado;

    /**
     * @var string|null
     *
     * @ORM\Column(name="habilidades", type="text", length=65535, nullable=true)
     */
    private $habilidades;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=true)
     */
    private $dataCriacao;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_atualizacao", type="datetime", nullable=true)
     */
    private $dataAtualizacao;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="termo", type="boolean", nullable=true)
     */
    private $termo = '0';


}
