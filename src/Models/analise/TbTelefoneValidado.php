<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbTelefoneValidado
 *
 * @ORM\Table(name="tb_telefone_validado")
 * @ORM\Entity
 */
class TbTelefoneValidado
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="tb_pessoa_id", type="integer", nullable=false)
     */
    private $tbPessoaId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telefone", type="string", length=30, nullable=true)
     */
    private $telefone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codigo", type="string", length=10, nullable=true)
     */
    private $codigo;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_validado", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataValidado = 'CURRENT_TIMESTAMP';


}
