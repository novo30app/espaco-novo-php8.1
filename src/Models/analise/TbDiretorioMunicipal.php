<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbDiretorioMunicipal
 *
 * @ORM\Table(name="tb_diretorio_municipal")
 * @ORM\Entity
 */
class TbDiretorioMunicipal
{
    /**
     * @var int
     *
     * @ORM\Column(name="id1", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id1;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     */
    private $id = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=255, nullable=false)
     */
    private $nome;

    /**
     * @var string|null
     *
     * @ORM\Column(name="url_doacao", type="string", length=60, nullable=true)
     */
    private $urlDoacao;

    /**
     * @var string|null
     *
     * @ORM\Column(name="recibo_descricao", type="string", length=120, nullable=true)
     */
    private $reciboDescricao;

    /**
     * @var string|null
     *
     * @ORM\Column(name="recibo_mascara", type="string", length=24, nullable=true)
     */
    private $reciboMascara;

    /**
     * @var string|null
     *
     * @ORM\Column(name="recibo_emite", type="string", length=0, nullable=true)
     */
    private $reciboEmite;

    /**
     * @var string
     *
     * @ORM\Column(name="cnpj", type="string", length=20, nullable=false)
     */
    private $cnpj;

    /**
     * @var string
     *
     * @ORM\Column(name="telefone", type="string", length=20, nullable=false)
     */
    private $telefone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="endereco", type="string", length=255, nullable=false)
     */
    private $endereco;

    /**
     * @var int
     *
     * @ORM\Column(name="tb_cidade_id", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $tbCidadeId;

    /**
     * @var int
     *
     * @ORM\Column(name="tb_estado_id", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $tbEstadoId;

    /**
     * @var string
     *
     * @ORM\Column(name="ativo", type="string", length=0, nullable=false, options={"default"="S"})
     */
    private $ativo = 'S';

    /**
     * @var string
     *
     * @ORM\Column(name="principal", type="string", length=0, nullable=false, options={"default"="N"})
     */
    private $principal = 'N';

    /**
     * @var string
     *
     * @ORM\Column(name="estadual", type="string", length=0, nullable=false, options={"default"="N"})
     */
    private $estadual = 'N';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="criado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $criado = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="atualizado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $atualizado = 'CURRENT_TIMESTAMP';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="exibir", type="boolean", nullable=true)
     */
    private $exibir;


}
