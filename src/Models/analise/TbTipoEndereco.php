<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbTipoEndereco
 *
 * @ORM\Table(name="tb_tipo_endereco")
 * @ORM\Entity
 */
class TbTipoEndereco
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tipo_endereco", type="string", length=45, nullable=true)
     */
    private $tipoEndereco;


}
