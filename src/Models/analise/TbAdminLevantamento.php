<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbAdminLevantamento
 *
 * @ORM\Table(name="tb_admin_levantamento")
 * @ORM\Entity
 */
class TbAdminLevantamento
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="nivel_acesso", type="integer", nullable=true)
     */
    private $nivelAcesso;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="criado", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $criado = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="residentes_exterior", type="string", length=0, nullable=false, options={"default"="N"})
     */
    private $residentesExterior = 'N';

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nome", type="string", length=100, nullable=true)
     */
    private $nome;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cpf", type="string", length=25, nullable=true)
     */
    private $cpf;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="filiado", type="boolean", nullable=true)
     */
    private $filiado = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="cidade", type="string", length=255, nullable=true)
     */
    private $cidade;

    /**
     * @var string|null
     *
     * @ORM\Column(name="estado", type="string", length=255, nullable=true)
     */
    private $estado;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Consultar_Pessoas", type="string", length=5, nullable=true)
     */
    private $consultarPessoas;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Transacoes", type="string", length=5, nullable=true)
     */
    private $transacoes;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Recibos", type="string", length=5, nullable=true)
     */
    private $recibos;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Relatórios", type="string", length=5, nullable=true)
     */
    private $relatórios;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Manutenção", type="string", length=5, nullable=true)
     */
    private $manutenção;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Eventos", type="string", length=5, nullable=true)
     */
    private $eventos;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Configurações", type="string", length=5, nullable=true)
     */
    private $configurações;


}
