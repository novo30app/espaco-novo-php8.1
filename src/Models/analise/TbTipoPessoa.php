<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbTipoPessoa
 *
 * @ORM\Table(name="tb_tipo_pessoa")
 * @ORM\Entity
 */
class TbTipoPessoa
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tipo_filiado", type="string", length=45, nullable=true)
     */
    private $tipoFiliado;


}
