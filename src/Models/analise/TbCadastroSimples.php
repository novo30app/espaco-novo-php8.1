<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbCadastroSimples
 *
 * @ORM\Table(name="tb_cadastro_simples", uniqueConstraints={@ORM\UniqueConstraint(name="email_UNIQUE", columns={"email"})})
 * @ORM\Entity
 */
class TbCadastroSimples
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=80, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=100, nullable=false)
     */
    private $nome;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telefone", type="string", length=30, nullable=true)
     */
    private $telefone;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="estado_id", type="integer", nullable=true)
     */
    private $estadoId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="cidade_id", type="integer", nullable=true)
     */
    private $cidadeId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cep", type="string", length=10, nullable=true)
     */
    private $cep;

    /**
     * @var string|null
     *
     * @ORM\Column(name="evento", type="text", length=65535, nullable=true)
     */
    private $evento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=false)
     */
    private $dataCriacao;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_atualizacao", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataAtualizacao = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="integrado", type="datetime", nullable=true)
     */
    private $integrado;

    /**
     * @var string|null
     *
     * @ORM\Column(name="observacao", type="string", length=10, nullable=true)
     */
    private $observacao;

    /**
     * @var string|null
     *
     * @ORM\Column(name="data_nascimento", type="string", length=10, nullable=true)
     */
    private $dataNascimento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cpf", type="string", length=20, nullable=true)
     */
    private $cpf;

    /**
     * @var string|null
     *
     * @ORM\Column(name="endereco", type="string", length=100, nullable=true)
     */
    private $endereco;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bairro", type="string", length=50, nullable=true)
     */
    private $bairro;

    /**
     * @var string|null
     *
     * @ORM\Column(name="numero", type="string", length=10, nullable=true)
     */
    private $numero;

    /**
     * @var string|null
     *
     * @ORM\Column(name="complemento", type="string", length=20, nullable=true)
     */
    private $complemento;


}
