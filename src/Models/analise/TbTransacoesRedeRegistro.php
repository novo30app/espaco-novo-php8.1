<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbTransacoesRedeRegistro
 *
 * @ORM\Table(name="tb_transacoes_rede_registro")
 * @ORM\Entity
 */
class TbTransacoesRedeRegistro
{
    /**
     * @var int
     *
     * @ORM\Column(name="Id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="N_Pedido", type="string", length=50, nullable=true)
     */
    private $nPedido;


}
