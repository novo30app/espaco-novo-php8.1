<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbAvaliacaoInadimplencia
 *
 * @ORM\Table(name="tb_avaliacao_inadimplencia")
 * @ORM\Entity
 */
class TbAvaliacaoInadimplencia
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nome", type="string", length=250, nullable=true)
     */
    private $nome;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=120, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="documento", type="string", length=20, nullable=true)
     */
    private $documento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="item", type="string", length=250, nullable=true)
     */
    private $item;

    /**
     * @var float|null
     *
     * @ORM\Column(name="valor", type="float", precision=10, scale=0, nullable=true)
     */
    private $valor;

    /**
     * @var int|null
     *
     * @ORM\Column(name="dias_boleto", type="integer", nullable=true)
     */
    private $diasBoleto;

    /**
     * @var int
     *
     * @ORM\Column(name="dias_boleto_novo", type="integer", nullable=false)
     */
    private $diasBoletoNovo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="invoice_id", type="string", length=100, nullable=true)
     */
    private $invoiceId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_evento", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataEvento = 'CURRENT_TIMESTAMP';


}
