<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbEnderecoValidacao
 *
 * @ORM\Table(name="tb_endereco_validacao", indexes={@ORM\Index(name="idx_pessoa", columns={"tb_pessoa_id"})})
 * @ORM\Entity
 */
class TbEnderecoValidacao
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bairro", type="string", length=45, nullable=true)
     */
    private $bairro;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="cep_valido", type="boolean", nullable=true)
     */
    private $cepValido;


}
