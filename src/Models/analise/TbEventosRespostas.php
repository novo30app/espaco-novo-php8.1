<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbEventosRespostas
 *
 * @ORM\Table(name="tb_eventos_respostas")
 * @ORM\Entity
 */
class TbEventosRespostas
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_evento_id", type="integer", nullable=true)
     */
    private $tbEventoId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tb_eventos_perguntas_id", type="string", length=45, nullable=true)
     */
    private $tbEventosPerguntasId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="resposta", type="string", length=255, nullable=true)
     */
    private $resposta;


}
