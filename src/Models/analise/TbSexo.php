<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbSexo
 *
 * @ORM\Table(name="tb_sexo")
 * @ORM\Entity
 */
class TbSexo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sexo", type="string", length=45, nullable=true)
     */
    private $sexo;


}
