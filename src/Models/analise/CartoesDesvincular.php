<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CartoesDesvincular
 *
 * @ORM\Table(name="cartoes_desvincular")
 * @ORM\Entity
 */
class CartoesDesvincular
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="token_cartao_credito", type="string", length=50, nullable=true)
     */
    private $tokenCartaoCredito;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="gateway_pagamento", type="boolean", nullable=true)
     */
    private $gatewayPagamento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="legenda", type="string", length=45, nullable=true)
     */
    private $legenda;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bandeira", type="string", length=40, nullable=true)
     */
    private $bandeira;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="principal", type="boolean", nullable=true, options={"default"="1"})
     */
    private $principal = '1';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="visivel", type="boolean", nullable=true, options={"default"="1"})
     */
    private $visivel = '1';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataCriacao = 'CURRENT_TIMESTAMP';

    /**
     * @var string|null
     *
     * @ORM\Column(name="nome", type="string", length=255, nullable=true)
     */
    private $nome;


}
