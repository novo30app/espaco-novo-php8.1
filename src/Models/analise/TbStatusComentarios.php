<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbStatusComentarios
 *
 * @ORM\Table(name="tb_status_comentarios")
 * @ORM\Entity
 */
class TbStatusComentarios
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="status", type="string", length=50, nullable=true)
     */
    private $status;


}
