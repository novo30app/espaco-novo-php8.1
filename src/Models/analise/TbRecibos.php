<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbRecibos
 *
 * @ORM\Table(name="tb_recibos", indexes={@ORM\Index(name="idx_tb_recibos_invoiced", columns={"invoiced"})})
 * @ORM\Entity
 */
class TbRecibos
{
    /**
     * @var int
     *
     * @ORM\Column(name="id1", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nome", type="string", length=100, nullable=true)
     */
    private $nome;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cpf", type="string", length=25, nullable=true)
     */
    private $cpf;

    /**
     * @var string|null
     *
     * @ORM\Column(name="data_pago", type="string", length=25, nullable=true)
     */
    private $dataPago;

    /**
     * @var string|null
     *
     * @ORM\Column(name="valor", type="decimal", precision=13, scale=4, nullable=true)
     */
    private $valor;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="forma_pagamento", type="boolean", nullable=true)
     */
    private $formaPagamento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="invoiced", type="string", length=100, nullable=true)
     */
    private $invoiced;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="gateway", type="boolean", nullable=true)
     */
    private $gateway;

    /**
     * @var string|null
     *
     * @ORM\Column(name="recibo", type="string", length=40, nullable=true)
     */
    private $recibo;


}
