<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbAgendaLegislativaBancada
 *
 * @ORM\Table(name="tb_agenda_legislativa_bancada")
 * @ORM\Entity
 */
class TbAgendaLegislativaBancada
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="tb_pessoa_id", type="integer", nullable=false)
     */
    private $tbPessoaId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_criacao", type="date", nullable=false)
     */
    private $dataCriacao;

    /**
     * @var int|null
     *
     * @ORM\Column(name="resposta1", type="integer", nullable=true)
     */
    private $resposta1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="razao_baixa", type="string", length=300, nullable=true)
     */
    private $razaoBaixa;

    /**
     * @var string|null
     *
     * @ORM\Column(name="razao2_baixa", type="string", length=300, nullable=true)
     */
    private $razao2Baixa;

    /**
     * @var string|null
     *
     * @ORM\Column(name="razao_media", type="string", length=300, nullable=true)
     */
    private $razaoMedia;

    /**
     * @var string|null
     *
     * @ORM\Column(name="razao2_media", type="string", length=300, nullable=true)
     */
    private $razao2Media;

    /**
     * @var string|null
     *
     * @ORM\Column(name="razao_alta", type="string", length=300, nullable=true)
     */
    private $razaoAlta;

    /**
     * @var string|null
     *
     * @ORM\Column(name="razao2_alta", type="string", length=300, nullable=true)
     */
    private $razao2Alta;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prioridade1", type="string", length=300, nullable=true)
     */
    private $prioridade1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prioridade2", type="string", length=300, nullable=true)
     */
    private $prioridade2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prioridade3", type="string", length=300, nullable=true)
     */
    private $prioridade3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prioridade4", type="string", length=300, nullable=true)
     */
    private $prioridade4;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prioridade5", type="string", length=300, nullable=true)
     */
    private $prioridade5;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema1_1", type="string", length=10, nullable=true)
     */
    private $tema11;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema1_2", type="string", length=10, nullable=true)
     */
    private $tema12;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema1_3", type="string", length=10, nullable=true)
     */
    private $tema13;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema1_4", type="string", length=10, nullable=true)
     */
    private $tema14;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prioridade1_tema1", type="string", length=255, nullable=true)
     */
    private $prioridade1Tema1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema2_1", type="string", length=10, nullable=true)
     */
    private $tema21;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema2_2", type="string", length=10, nullable=true)
     */
    private $tema22;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema2_3", type="string", length=10, nullable=true)
     */
    private $tema23;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prioridade1_tema2", type="string", length=255, nullable=true)
     */
    private $prioridade1Tema2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema3_1", type="string", length=10, nullable=true)
     */
    private $tema31;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema3_2", type="string", length=10, nullable=true)
     */
    private $tema32;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema3_3", type="string", length=10, nullable=true)
     */
    private $tema33;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema3_4", type="string", length=10, nullable=true)
     */
    private $tema34;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prioridade1_tema3", type="string", length=255, nullable=true)
     */
    private $prioridade1Tema3;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema4_1", type="string", length=10, nullable=true)
     */
    private $tema41;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema4_2", type="string", length=10, nullable=true)
     */
    private $tema42;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema4_3", type="string", length=10, nullable=true)
     */
    private $tema43;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prioridade1_tema4", type="string", length=255, nullable=true)
     */
    private $prioridade1Tema4;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema5_1", type="string", length=10, nullable=true)
     */
    private $tema51;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema5_2", type="string", length=10, nullable=true)
     */
    private $tema52;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema5_3", type="string", length=10, nullable=true)
     */
    private $tema53;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema5_4", type="string", length=10, nullable=true)
     */
    private $tema54;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema5_5", type="string", length=10, nullable=true)
     */
    private $tema55;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prioridade1_tema5", type="string", length=255, nullable=true)
     */
    private $prioridade1Tema5;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema6_1", type="string", length=10, nullable=true)
     */
    private $tema61;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema6_2", type="string", length=10, nullable=true)
     */
    private $tema62;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema6_3", type="string", length=10, nullable=true)
     */
    private $tema63;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema6_4", type="string", length=10, nullable=true)
     */
    private $tema64;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prioridade1_tema6", type="string", length=255, nullable=true)
     */
    private $prioridade1Tema6;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema7_1", type="string", length=10, nullable=true)
     */
    private $tema71;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema7_2", type="string", length=10, nullable=true)
     */
    private $tema72;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema7_3", type="string", length=10, nullable=true)
     */
    private $tema73;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema7_4", type="string", length=10, nullable=true)
     */
    private $tema74;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema7_5", type="string", length=10, nullable=true)
     */
    private $tema75;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema7_6", type="string", length=10, nullable=true)
     */
    private $tema76;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prioridade1_tema7", type="string", length=255, nullable=true)
     */
    private $prioridade1Tema7;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema8_1", type="string", length=10, nullable=true)
     */
    private $tema81;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema8_2", type="string", length=10, nullable=true)
     */
    private $tema82;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema8_3", type="string", length=10, nullable=true)
     */
    private $tema83;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema8_4", type="string", length=10, nullable=true)
     */
    private $tema84;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prioridade1_tema8", type="string", length=255, nullable=true)
     */
    private $prioridade1Tema8;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema9_1", type="string", length=10, nullable=true)
     */
    private $tema91;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema9_2", type="string", length=10, nullable=true)
     */
    private $tema92;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema9_3", type="string", length=10, nullable=true)
     */
    private $tema93;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prioridade1_tema9", type="string", length=255, nullable=true)
     */
    private $prioridade1Tema9;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema10_1", type="string", length=10, nullable=true)
     */
    private $tema101;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema10_2", type="string", length=10, nullable=true)
     */
    private $tema102;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tema10_3", type="string", length=10, nullable=true)
     */
    private $tema103;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prioridade1_tema10", type="string", length=255, nullable=true)
     */
    private $prioridade1Tema10;


}
