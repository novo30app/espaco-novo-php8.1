<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbDoador
 *
 * @ORM\Table(name="tb_doador")
 * @ORM\Entity
 */
class TbDoador
{
    /**
     * @var int
     *
     * @ORM\Column(name="id1", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="id", type="string", length=255, nullable=true)
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="expires_at", type="string", length=255, nullable=true)
     */
    private $expiresAt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="created_at", type="string", length=255, nullable=true)
     */
    private $createdAt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="updated_at", type="string", length=255, nullable=true)
     */
    private $updatedAt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="plan_identifier", type="string", length=255, nullable=true)
     */
    private $planIdentifier;

    /**
     * @var string|null
     *
     * @ORM\Column(name="price_cents", type="string", length=255, nullable=true)
     */
    private $priceCents;

    /**
     * @var string|null
     *
     * @ORM\Column(name="customer_id", type="string", length=255, nullable=true)
     */
    private $customerId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="suspended", type="string", length=255, nullable=true)
     */
    private $suspended;

    /**
     * @var string|null
     *
     * @ORM\Column(name="interval", type="string", length=255, nullable=true)
     */
    private $interval;

    /**
     * @var string|null
     *
     * @ORM\Column(name="interval_type", type="string", length=255, nullable=true)
     */
    private $intervalType;

    /**
     * @var string|null
     *
     * @ORM\Column(name="suspended_at", type="string", length=255, nullable=true)
     */
    private $suspendedAt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="started_at", type="string", length=255, nullable=true)
     */
    private $startedAt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="credits", type="string", length=255, nullable=true)
     */
    private $credits;

    /**
     * @var string|null
     *
     * @ORM\Column(name="credits_min", type="string", length=255, nullable=true)
     */
    private $creditsMin;

    /**
     * @var string|null
     *
     * @ORM\Column(name="credits_cycle", type="string", length=255, nullable=true)
     */
    private $creditsCycle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="payable_with", type="string", length=255, nullable=true)
     */
    private $payableWith;

    /**
     * @var int|null
     *
     * @ORM\Column(name="imp", type="integer", nullable=true)
     */
    private $imp = '0';


}
