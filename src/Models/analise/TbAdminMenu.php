<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbAdminMenu
 *
 * @ORM\Table(name="tb_admin_menu")
 * @ORM\Entity
 */
class TbAdminMenu
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="controller", type="string", length=50, nullable=true)
     */
    private $controller;

    /**
     * @var string|null
     *
     * @ORM\Column(name="action", type="string", length=50, nullable=true)
     */
    private $action;

    /**
     * @var string|null
     *
     * @ORM\Column(name="label", type="string", length=80, nullable=true)
     */
    private $label;

    /**
     * @var string|null
     *
     * @ORM\Column(name="label_pai", type="string", length=50, nullable=true)
     */
    private $labelPai;

    /**
     * @var int|null
     *
     * @ORM\Column(name="posicao", type="integer", nullable=true)
     */
    private $posicao = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="config", type="text", length=65535, nullable=true)
     */
    private $config;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ativo", type="string", length=0, nullable=true, options={"default"="N"})
     */
    private $ativo = 'N';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="criado", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $criado = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="atualizado", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $atualizado = 'CURRENT_TIMESTAMP';


}
