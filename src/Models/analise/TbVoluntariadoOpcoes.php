<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbVoluntariadoOpcoes
 *
 * @ORM\Table(name="tb_voluntariado_opcoes")
 * @ORM\Entity
 */
class TbVoluntariadoOpcoes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descricao", type="string", length=200, nullable=true)
     */
    private $descricao = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="ativo", type="string", length=1, nullable=true, options={"fixed"=true})
     */
    private $ativo = '0';


}
