<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbCargoPublico
 *
 * @ORM\Table(name="tb_cargo_publico")
 * @ORM\Entity
 */
class TbCargoPublico
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cargo_publico", type="string", length=45, nullable=true)
     */
    private $cargoPublico;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="visivel", type="boolean", nullable=true)
     */
    private $visivel;


}
