<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbTipoTelefone
 *
 * @Table(name="tb_tipo_telefone")
 * @Entity
 */
class TbTipoTelefone
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @Column(name="tipo_telefone", type="string", length=20, nullable=true)
     */
    private $tipoTelefone;


}
