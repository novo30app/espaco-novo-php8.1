<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbPessoaAdmin
 *
 * @ORM\Table(name="tb_pessoa_admin")
 * @ORM\Entity
 */
class TbPessoaAdmin
{
    /**
     * @var int
     *
     * @ORM\Column(name="id1", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id1;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="nivel_acesso", type="integer", nullable=true)
     */
    private $nivelAcesso;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="criado", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $criado = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="atualizado", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $atualizado = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="residentes_exterior", type="string", length=0, nullable=false, options={"default"="N"})
     */
    private $residentesExterior = 'N';


}
