<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbDoadorantigo
 *
 * @ORM\Table(name="tb_doadorantigo")
 * @ORM\Entity
 */
class TbDoadorantigo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id1", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id1;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nome", type="string", length=100, nullable=true)
     */
    private $nome;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cpf", type="string", length=25, nullable=true)
     */
    private $cpf;

    /**
     * @var int|null
     *
     * @ORM\Column(name="uf_id", type="integer", nullable=true)
     */
    private $ufId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="municipio_id", type="integer", nullable=true)
     */
    private $municipioId;


}
