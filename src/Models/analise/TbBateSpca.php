<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbBateSpca
 *
 * @ORM\Table(name="tb_bate_SPCA", indexes={@ORM\Index(name="idx_tb_bate_SPCA_cpf_cnpj", columns={"cpf_cnpj"})})
 * @ORM\Entity
 */
class TbBateSpca
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data", type="date", nullable=true)
     */
    private $data;

    /**
     * @var int|null
     *
     * @ORM\Column(name="n_documento", type="integer", nullable=true)
     */
    private $nDocumento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descricao", type="string", length=30, nullable=true)
     */
    private $descricao;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nome", type="string", length=30, nullable=true)
     */
    private $nome;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cpf_cnpj", type="string", length=25, nullable=true)
     */
    private $cpfCnpj;

    /**
     * @var string|null
     *
     * @ORM\Column(name="valor", type="decimal", precision=13, scale=4, nullable=true)
     */
    private $valor;

    /**
     * @var string|null
     *
     * @ORM\Column(name="saldo", type="decimal", precision=13, scale=4, nullable=true)
     */
    private $saldo;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="deletar", type="boolean", nullable=true)
     */
    private $deletar;


}
