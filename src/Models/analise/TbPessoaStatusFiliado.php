<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbPessoaStatusFiliado
 *
 * @ORM\Table(name="tb_pessoa_status_filiado")
 * @ORM\Entity
 */
class TbPessoaStatusFiliado
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="statusId", type="string", length=255, nullable=true)
     */
    private $statusid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="status", type="string", length=45, nullable=true)
     */
    private $status;


}
