<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbAdminGrupo
 *
 * @ORM\Table(name="tb_admin_grupo")
 * @ORM\Entity
 */
class TbAdminGrupo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descricao", type="string", length=50, nullable=true)
     */
    private $descricao = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="sigla", type="string", length=10, nullable=true, options={"fixed"=true})
     */
    private $sigla = '0';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="criado", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $criado = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="atualizado", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $atualizado = 'CURRENT_TIMESTAMP';


}
