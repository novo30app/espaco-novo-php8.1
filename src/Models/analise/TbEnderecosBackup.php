<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbEnderecosBackup
 *
 * @ORM\Table(name="tb_enderecos_backup")
 * @ORM\Entity
 */
class TbEnderecosBackup
{
    /**
     * @var int
     *
     * @ORM\Column(name="id1", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id1;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     */
    private $id = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="tb_pessoa_id", type="integer", nullable=false)
     */
    private $tbPessoaId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_tipo_endereco_id", type="integer", nullable=true)
     */
    private $tbTipoEnderecoId;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="reside_exterior", type="boolean", nullable=true)
     */
    private $resideExterior;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_pais_id", type="integer", nullable=true)
     */
    private $tbPaisId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_estado_id", type="integer", nullable=true)
     */
    private $tbEstadoId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="estado", type="string", length=255, nullable=true)
     */
    private $estado;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_cidade_id", type="integer", nullable=true)
     */
    private $tbCidadeId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cidade", type="string", length=255, nullable=true)
     */
    private $cidade;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cep", type="string", length=10, nullable=true)
     */
    private $cep;

    /**
     * @var string
     *
     * @ORM\Column(name="cep_status", type="string", length=0, nullable=false)
     */
    private $cepStatus = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="endereco", type="string", length=150, nullable=true)
     */
    private $endereco;

    /**
     * @var string|null
     *
     * @ORM\Column(name="numero", type="string", length=6, nullable=true)
     */
    private $numero;

    /**
     * @var string|null
     *
     * @ORM\Column(name="complemento", type="string", length=200, nullable=true)
     */
    private $complemento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bairro", type="string", length=45, nullable=true)
     */
    private $bairro;


}
