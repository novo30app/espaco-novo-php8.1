<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbAdminAcesso
 *
 * @ORM\Table(name="tb_admin_acesso")
 * @ORM\Entity
 */
class TbAdminAcesso
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="item_id", type="integer", nullable=true)
     */
    private $itemId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tipo_relacao", type="string", length=0, nullable=true)
     */
    private $tipoRelacao;


}
