<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbDistribuicaoMaterial
 *
 * @ORM\Table(name="tb_distribuicao_material", indexes={@ORM\Index(name="idx_tb_distribuicao_material_envio", columns={"envio"})})
 * @ORM\Entity
 */
class TbDistribuicaoMaterial
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nome", type="string", length=120, nullable=true)
     */
    private $nome;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cep", type="string", length=10, nullable=true)
     */
    private $cep;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cpf", type="string", length=25, nullable=true)
     */
    private $cpf;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="estado", type="string", length=255, nullable=true)
     */
    private $estado;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cidade", type="string", length=255, nullable=true)
     */
    private $cidade;

    /**
     * @var string|null
     *
     * @ORM\Column(name="endereco", type="string", length=150, nullable=true)
     */
    private $endereco;

    /**
     * @var string|null
     *
     * @ORM\Column(name="numero", type="string", length=6, nullable=true)
     */
    private $numero;

    /**
     * @var string|null
     *
     * @ORM\Column(name="complemento", type="string", length=200, nullable=true)
     */
    private $complemento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bairro", type="string", length=45, nullable=true)
     */
    private $bairro;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=true)
     */
    private $dataCriacao;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ip", type="string", length=15, nullable=true)
     */
    private $ip;

    /**
     * @var string|null
     *
     * @ORM\Column(name="envio", type="string", length=1, nullable=true, options={"fixed"=true})
     */
    private $envio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="enviado", type="string", length=20, nullable=true)
     */
    private $enviado;

    /**
     * @var string|null
     *
     * @ORM\Column(name="declaracao", type="string", length=30, nullable=true)
     */
    private $declaracao;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Etiqueta", type="string", length=8, nullable=true)
     */
    private $etiqueta;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Documento", type="string", length=10, nullable=true)
     */
    private $documento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="custo_correio", type="string", length=1, nullable=true)
     */
    private $custoCorreio;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_emissao_etiqueta", type="datetime", nullable=true)
     */
    private $dataEmissaoEtiqueta;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_emissao_declaracao", type="datetime", nullable=true)
     */
    private $dataEmissaoDeclaracao;


}
