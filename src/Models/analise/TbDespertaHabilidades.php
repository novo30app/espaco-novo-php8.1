<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbDespertaHabilidades
 *
 * @ORM\Table(name="tb_desperta_habilidades", indexes={@ORM\Index(name="tb_desperta_id_tb_desperta_opcoes_id", columns={"tb_desperta_id", "tb_desperta_opcoes_id"})})
 * @ORM\Entity
 */
class TbDespertaHabilidades
{
    /**
     * @var int
     *
     * @ORM\Column(name="id1", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id1;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_desperta_id", type="integer", nullable=true)
     */
    private $tbDespertaId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_desperta_opcoes_id", type="integer", nullable=true)
     */
    private $tbDespertaOpcoesId;


}
