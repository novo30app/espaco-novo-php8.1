<?php


namespace App\Models\Entities;
use Doctrine\ORM\Mapping as ORM;

/**
 * TbTransacoes
 *
 * @Entity @Table(name="tb_transacoes", indexes={@Index(name="idx_id", columns={"id"}),@Index(name="idx_invoice_id", columns={"invoice_id"}), @Index(name="idx_tb_diretorio_id", columns={"tb_diretorio_id"}), @Index(name="idx_data_criacao", columns={"data_criacao"}), @Index(name="idx_tb_pessoa_id", columns={"tb_pessoa_id"}), @Index(name="idx_token_cartao_credito", columns={"token_cartao_credito"}), @Index(name="idx_tb_diretorio_origem", columns={"tb_diretorio_origem"}), @Index(name="idx_data_pago", columns={"data_pago"}), @Index(name="idx_id", columns={"id"}), @Index(name="idx_tb_assinatura_id", columns={"tb_assinatura_id"})})
 * @Entity(repositoryClass="App\Models\Repository\UsersRepository")
 */
class TbTransacoes
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var int|null
     *
     * @Column(name="tb_assinatura_id", type="integer", nullable=true)
     */
    private $tbAssinaturaId;

    /**
     * @Column(type="datetime", name="data_criacao")
     * @var string
     */
    private $dataCriacao;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_criacao_origem", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataCriacaoOrigem = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_gera_fatura", type="date", nullable=true)
     */
    private $dataGeraFatura;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_vencimento", type="date", nullable=true)
     */
    private $dataVencimento;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_vencimento_origem", type="datetime", nullable=true)
     */
    private $dataVencimentoOrigem;

    /**
     * @var string|null
     *
     * @Column(name="data_pago", type="string", length=45, nullable=true)
     */
    private $dataPago;

    /**
     * @Column(type="decimal", name="valor")
     * @var string
     */
    private $valor;

    /**
     * @var bool|null
     *
     * @Column(name="forma_pagamento", type="integer", nullable=true)
     */
    private $formaPagamento;

    /**
     * @var bool|null
     *
     * @Column(name="tipo_cartao", type="integer", nullable=true)
     */
    private $tipoCartao;

    /**
     * @var bool|null
     *
     * @Column(name="origem_transacao", type="integer", nullable=true)
     */
    private $origemTransacao;

    /**
     * @var int|null
     *
     * @Column(name="periodicidade", type="integer", nullable=true)
     */
    private $periodicidade;

    /**
     * @var bool|null
     *
     * @Column(name="gateway_pagamento", type="boolean", nullable=true)
     */
    private $gatewayPagamento;

    /**
     * @var string|null
     *
     * @Column(name="customer_id", type="string", length=45, nullable=true)
     */
    private $customerId;

    /**
     * @var string|null
     *
     * @Column(name="invoice_id", type="string", length=100, nullable=true)
     */
    private $invoiceId;

    /**
     * @var string|null
     *
     * @Column(name="status", type="string", length=45, nullable=true)
     */
    private $status;

    /**
     * @var string|null
     *
     * @Column(name="url", type="string", length=300, nullable=true)
     */
    private $url;

    /**
     * @var string|null
     *
     * @Column(name="numero_recibo", type="string", length=40, nullable=true)
     */
    private $numeroRecibo;

    /**
     * @var string|null
     *
     * @Column(name="token_cartao_credito", type="string", length=255, nullable=true)
     */
    private $tokenCartaoCredito;

    /**
     * @var int|null
     *
     * @Column(name="fase_cobranca", type="integer", nullable=true, options={"default"="1"})
     */
    private $faseCobranca = '1';

    /**
     * @var \DateTime|null
     *
     * @Column(name="ultima_atualizacao", type="datetime", nullable=true)
     */
    private $ultimaAtualizacao;

    /**
     * @var int|null
     *
     * @Column(name="atualizado_por", type="integer", nullable=true)
     */
    private $atualizadoPor;

    /**
     * @var int|null
     *
     * @Column(name="tb_diretorio_id", type="integer", nullable=true, options={"default"="1"})
     */
    private $tbDiretorioId = '1';

    /**
     * @var int|null
     *
     * @Column(name="tb_diretorio_origem", type="integer", nullable=true)
     */
    private $tbDiretorioOrigem;

    /**
     * @var string|null
     *
     * @Column(name="pendencia_titulo", type="string", length=0, nullable=true)
     */
    private $pendenciaTitulo;

    /**
     * @var bool|null
     *
     * @Column(name="sku", type="boolean", nullable=true)
     */
    private $sku;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDataCreation()
    {
        return $this->dataCriacao;
    }

    public function getOriginTypeString(): ?string
    {
        $origin = $this->origemTransacao;

        switch ($origin) {
            case 1: 
                return 'Doação';
                break;                
            case 2: 
                return 'Filiação';
                break;
            case 3: 
                return 'Doação Campanha';
                break;                
            case 4: 
                return 'Evento';
                break;
            case 5: 
                return 'Complemento de Filiação';
                break;          
        }
    }

    public function getPaymentTypeString(): ?string
    {
        $type = $this->formaPagamento;

        switch ($type) {
            case 1: 
                return 'Cartão de Crédito';
                break;                
            case 2: 
                return 'Boleto';
                break;
            case 3: 
                return 'TED';
                break;                
            case 4: 
                return 'DOC';
                break;
            case 5: 
                return 'Transferência Online';
                break;                
            case 6: 
                return 'Depósito Oline';
                break;
            case 7: 
                return 'Depósito';
                break;                
            case 8: 
                return 'Cheque';
                break; 
            case 9: 
                return 'Tranferência';
                break; 
        }
    }

    public function getValue()
    {
        $valor = $this->valor;  
        return "R$ " . number_format($valor,2,',','');   
    }

    public function getStatusTypeString(): ?string
    {
        switch ($this->status) {

            case 'canceled' :
                return 'Cancelada';
                break;
                
            case 'chargeback' :
                return 'Chargeback';
                break;

            case 'pending' :
                return 'Pendente';
                break;

            case 'expired' :
                return 'Expirado';
                break;

            case 'refunded' :
                return 'Reembolsada';
                break;

            case 'partially paid' :
                return 'Pago parcialmente';
                break;

            case 'paid' :
                return 'Pago';
                break;
        }

    }
    public function getReceipt(): ?string
    {
        return $this->numeroRecibo;  
    }

    public function getUrl(): ?string
    {
        return $this->url;  
    }
}
