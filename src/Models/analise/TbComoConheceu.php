<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbComoConheceu
 *
 * @ORM\Table(name="tb_como_conheceu", indexes={@ORM\Index(name="idx_pessoa_id", columns={"tb_pessoa_id"})})
 * @ORM\Entity
 */
class TbComoConheceu
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="type", type="string", length=100, nullable=true)
     */
    private $type;

    /**
     * @var string|null
     *
     * @ORM\Column(name="subtype", type="string", length=100, nullable=true)
     */
    private $subtype;

    /**
     * @var string|null
     *
     * @ORM\Column(name="estado", type="string", length=100, nullable=true)
     */
    private $estado;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cidade", type="string", length=100, nullable=true)
     */
    private $cidade;

    /**
     * @var string|null
     *
     * @ORM\Column(name="evento", type="string", length=100, nullable=true)
     */
    private $evento;

    /**
     * @var int|null
     *
     * @ORM\Column(name="filiado_id", type="integer", nullable=true)
     */
    private $filiadoId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="communicated", type="text", length=65535, nullable=true)
     */
    private $content;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;


}
