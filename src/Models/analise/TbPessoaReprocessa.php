<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbPessoaReprocessa
 *
 * @ORM\Table(name="tb_pessoa_reprocessa", indexes={@ORM\Index(name="idx_email", columns={"email"})})
 * @ORM\Entity
 */
class TbPessoaReprocessa
{
    /**
     * @var int
     *
     * @ORM\Column(name="id1", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id1;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;


}
