<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbAdminMenuItem
 *
 * @ORM\Table(name="tb_admin_menu_item")
 * @ORM\Entity
 */
class TbAdminMenuItem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_admin_menu_id", type="integer", nullable=true)
     */
    private $tbAdminMenuId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descricao", type="string", length=80, nullable=true)
     */
    private $descricao;

    /**
     * @var string|null
     *
     * @ORM\Column(name="campos", type="text", length=65535, nullable=true)
     */
    private $campos;


}
