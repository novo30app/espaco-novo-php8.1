<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Financeirotransacao
 *
 * @ORM\Table(name="FinanceiroTransacao")
 * @ORM\Entity
 */
class Financeirotransacao
{
    /**
     * @var string
     *
     * @ORM\Column(name="Id", type="string", length=128, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CodUltFatura", type="string", length=250, nullable=true)
     */
    private $codultfatura;

    /**
     * @var string|null
     *
     * @ORM\Column(name="UrlUltFatura", type="text", length=65535, nullable=true)
     */
    private $urlultfatura;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Plano", type="string", length=250, nullable=true)
     */
    private $plano;

    /**
     * @var string|null
     *
     * @ORM\Column(name="TipoFatura", type="string", length=250, nullable=true)
     */
    private $tipofatura;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Nome", type="string", length=250, nullable=true)
     */
    private $nome;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Email", type="string", length=250, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CPF", type="string", length=250, nullable=true)
     */
    private $cpf;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Cidade", type="string", length=250, nullable=true)
     */
    private $cidade;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Nucleo", type="text", length=65535, nullable=true)
     */
    private $nucleo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="StatusUltFatura", type="string", length=250, nullable=true)
     */
    private $statusultfatura;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="DataCriacaoUltFatura", type="datetime", nullable=true)
     */
    private $datacriacaoultfatura;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="VencimentoUltFatura", type="datetime", nullable=true)
     */
    private $vencimentoultfatura;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="DataPagUltFatura", type="datetime", nullable=true)
     */
    private $datapagultfatura;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ValorBrutoUltFatura", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $valorbrutoultfatura;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ValorTaxaUltFatura", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $valortaxaultfatura;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ValorLiqUltFatura", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $valorliqultfatura;

    /**
     * @var string|null
     *
     * @ORM\Column(name="CodAssinatura", type="string", length=250, nullable=true)
     */
    private $codassinatura;

    /**
     * @var string|null
     *
     * @ORM\Column(name="StatusAssinatura", type="string", length=250, nullable=true)
     */
    private $statusassinatura;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="VencAssinatura", type="datetime", nullable=true)
     */
    private $vencassinatura;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ValorTotalPago", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $valortotalpago;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="DataUltHistorico", type="datetime", nullable=true)
     */
    private $dataulthistorico;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DescUltHistorico", type="text", length=0, nullable=true)
     */
    private $desculthistorico;

    /**
     * @var string|null
     *
     * @ORM\Column(name="FormaPagamento", type="string", length=250, nullable=true)
     */
    private $formapagamento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="TituloEleitor", type="string", length=250, nullable=true)
     */
    private $tituloeleitor;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="DataCriacao", type="datetime", nullable=true)
     */
    private $datacriacao;


}
