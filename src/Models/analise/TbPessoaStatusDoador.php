<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbPessoaStatusDoador
 *
 * @ORM\Table(name="tb_pessoa_status_doador", indexes={@ORM\Index(name="idx_tb_pessoa_status_doador_status", columns={"status"}), @ORM\Index(name="idx_tb_pessoa_status_doador_statusId", columns={"statusId"})})
 * @ORM\Entity
 */
class TbPessoaStatusDoador
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="statusId", type="string", length=255, nullable=true)
     */
    private $statusid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="status", type="string", length=45, nullable=true)
     */
    private $status;


}
