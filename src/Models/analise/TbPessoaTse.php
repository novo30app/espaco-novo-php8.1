<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbPessoaTse
 *
 * @ORM\Table(name="tb_pessoa_tse", uniqueConstraints={@ORM\UniqueConstraint(name="idx_email", columns={"email"}), @ORM\UniqueConstraint(name="idx_cpf", columns={"cpf"})}, indexes={@ORM\Index(name="idx_pais", columns={"pais_tse"})})
 * @ORM\Entity
 */
class TbPessoaTse
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nome", type="string", length=100, nullable=true)
     */
    private $nome;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cpf", type="string", length=25, nullable=true)
     */
    private $cpf;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_nascimento", type="date", nullable=true)
     */
    private $dataNascimento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="titulo_eleitoral", type="string", length=25, nullable=true)
     */
    private $tituloEleitoral;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tit_tse", type="string", length=25, nullable=true)
     */
    private $titTse;

    /**
     * @var string|null
     *
     * @ORM\Column(name="titulo_eleitoral_zona", type="string", length=4, nullable=true)
     */
    private $tituloEleitoralZona;

    /**
     * @var string|null
     *
     * @ORM\Column(name="zona_tse", type="string", length=4, nullable=true)
     */
    private $zonaTse;

    /**
     * @var string|null
     *
     * @ORM\Column(name="titulo_eleitoral_secao", type="string", length=4, nullable=true)
     */
    private $tituloEleitoralSecao;

    /**
     * @var string|null
     *
     * @ORM\Column(name="secao_tse", type="string", length=4, nullable=true)
     */
    private $secaoTse;

    /**
     * @var string|null
     *
     * @ORM\Column(name="titulo_eleitoral_pais", type="string", length=100, nullable=true)
     */
    private $tituloEleitoralPais;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pais_tse", type="string", length=100, nullable=true)
     */
    private $paisTse;

    /**
     * @var int|null
     *
     * @ORM\Column(name="titulo_eleitoral_pais_id", type="integer", nullable=true)
     */
    private $tituloEleitoralPaisId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="pais_tse_id", type="integer", nullable=true)
     */
    private $paisTseId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="titulo_eleitoral_uf", type="string", length=100, nullable=true)
     */
    private $tituloEleitoralUf;

    /**
     * @var string|null
     *
     * @ORM\Column(name="uf_tse", type="string", length=100, nullable=true)
     */
    private $ufTse;

    /**
     * @var int|null
     *
     * @ORM\Column(name="titulo_eleitoral_uf_id", type="integer", nullable=true)
     */
    private $tituloEleitoralUfId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="uf_id_tse", type="integer", nullable=true)
     */
    private $ufIdTse;

    /**
     * @var string|null
     *
     * @ORM\Column(name="titulo_eleitoral_municipio", type="string", length=150, nullable=true)
     */
    private $tituloEleitoralMunicipio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cidade_tse", type="string", length=150, nullable=true)
     */
    private $cidadeTse;

    /**
     * @var int|null
     *
     * @ORM\Column(name="titulo_eleitoral_municipio_id", type="integer", nullable=true)
     */
    private $tituloEleitoralMunicipioId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="cidade_tse_id", type="integer", nullable=true)
     */
    private $cidadeTseId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nome_mae", type="string", length=100, nullable=true)
     */
    private $nomeMae;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_solicitacao_filiacao", type="date", nullable=true)
     */
    private $dataSolicitacaoFiliacao;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_solicitacao_refiliacao", type="date", nullable=true)
     */
    private $dataSolicitacaoRefiliacao;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_filiacao", type="date", nullable=true)
     */
    private $dataFiliacao;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_desfiliacao", type="date", nullable=true)
     */
    private $dataDesfiliacao;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_refiliacao", type="date", nullable=true)
     */
    private $dataRefiliacao;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="filiado", type="boolean", nullable=true)
     */
    private $filiado = '0';


}
