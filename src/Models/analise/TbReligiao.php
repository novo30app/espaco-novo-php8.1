<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbReligiao
 *
 * @ORM\Table(name="tb_religiao")
 * @ORM\Entity
 */
class TbReligiao
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="religiao", type="string", length=45, nullable=true)
     */
    private $religiao;


}
