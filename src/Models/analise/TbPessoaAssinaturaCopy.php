<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbPessoaAssinaturaCopy
 *
 * @ORM\Table(name="tb_pessoa_assinatura_copy")
 * @ORM\Entity
 */
class TbPessoaAssinaturaCopy
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $dataCriacao = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="customer_id", type="string", length=100, nullable=true)
     */
    private $customerId;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="gateway_pagamento", type="boolean", nullable=true)
     */
    private $gatewayPagamento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="gateway_status", type="string", length=100, nullable=true)
     */
    private $gatewayStatus;

    /**
     * @var string|null
     *
     * @ORM\Column(name="valor", type="decimal", precision=13, scale=2, nullable=true)
     */
    private $valor;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="forma_pagamento", type="boolean", nullable=true)
     */
    private $formaPagamento;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="origem_transacao", type="boolean", nullable=true)
     */
    private $origemTransacao;

    /**
     * @var string|null
     *
     * @ORM\Column(name="status", type="string", length=10, nullable=true, options={"default"="created"})
     */
    private $status = 'created';

    /**
     * @var string|null
     *
     * @ORM\Column(name="token_cartao_credito", type="string", length=255, nullable=true)
     */
    private $tokenCartaoCredito;

    /**
     * @var string|null
     *
     * @ORM\Column(name="periodicidade", type="string", length=20, nullable=true)
     */
    private $periodicidade;

    /**
     * @var int|null
     *
     * @ORM\Column(name="dia_ciclo", type="integer", nullable=true)
     */
    private $diaCiclo;

    /**
     * @var int|null
     *
     * @ORM\Column(name="mes_ciclo", type="integer", nullable=true)
     */
    private $mesCiclo;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_ultimo_status", type="date", nullable=true)
     */
    private $dataUltimoStatus;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_gera_fatura", type="date", nullable=true)
     */
    private $dataGeraFatura;

    /**
     * @var string|null
     *
     * @ORM\Column(name="subscription_id", type="string", length=100, nullable=true)
     */
    private $subscriptionId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="obs", type="string", length=255, nullable=true)
     */
    private $obs;


}
