<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbEventosTransacoes
 *
 * @ORM\Table(name="tb_eventos_transacoes", indexes={@ORM\Index(name="idx_tb_eventos_transacoes_tb_cidade_id", columns={"tb_cidade_id"}), @ORM\Index(name="idx_tb_eventos_transacoes_tb_eventos_id", columns={"tb_eventos_id"}), @ORM\Index(name="idx_tb_eventos_transacoes_tb_transacao_id", columns={"tb_transacao_id"}), @ORM\Index(name="idx_tb_eventos_transacoes_tb_estado_id", columns={"tb_estado_id"})})
 * @ORM\Entity
 */
class TbEventosTransacoes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="tb_eventos_id", type="integer", nullable=false)
     */
    private $tbEventosId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_transacao_id", type="integer", nullable=true)
     */
    private $tbTransacaoId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="valor", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $valor;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nome", type="string", length=100, nullable=true)
     */
    private $nome;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_nascimento", type="date", nullable=true)
     */
    private $dataNascimento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cpf", type="string", length=20, nullable=true)
     */
    private $cpf;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telefone_ddi", type="string", length=4, nullable=true)
     */
    private $telefoneDdi;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telefone_numero", type="string", length=25, nullable=true)
     */
    private $telefoneNumero;

    /**
     * @var string|null
     *
     * @ORM\Column(name="celular_ddi", type="string", length=4, nullable=true)
     */
    private $celularDdi;

    /**
     * @var string|null
     *
     * @ORM\Column(name="celular_numero", type="string", length=25, nullable=true)
     */
    private $celularNumero;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=80, nullable=true)
     */
    private $email;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_estado_id", type="integer", nullable=true)
     */
    private $tbEstadoId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="estado", type="string", length=255, nullable=true)
     */
    private $estado;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_cidade_id", type="integer", nullable=true)
     */
    private $tbCidadeId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cidade", type="string", length=255, nullable=true)
     */
    private $cidade;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cep", type="string", length=10, nullable=true)
     */
    private $cep;

    /**
     * @var string|null
     *
     * @ORM\Column(name="endereco", type="string", length=150, nullable=true)
     */
    private $endereco;

    /**
     * @var string|null
     *
     * @ORM\Column(name="numero", type="string", length=6, nullable=true)
     */
    private $numero;

    /**
     * @var string|null
     *
     * @ORM\Column(name="complemento", type="string", length=200, nullable=true)
     */
    private $complemento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bairro", type="string", length=45, nullable=true)
     */
    private $bairro;

    /**
     * @var string|null
     *
     * @ORM\Column(name="code", type="string", length=100, nullable=true)
     */
    private $code;

    /**
     * @var string|null
     *
     * @ORM\Column(name="status", type="string", length=0, nullable=true, options={"default"="pendente"})
     */
    private $status = 'pendente';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataCriacao = 'CURRENT_TIMESTAMP';


}
