<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TotvsTransactions
 *
 * @ORM\Table(name="totvs_transactions")
 * @ORM\Entity
 */
class TotvsTransactions
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tb_transacao_id", type="string", length=11, nullable=false)
     */
    private $tbTransacaoId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="inserted_in", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $insertedIn = 'CURRENT_TIMESTAMP';


}
