<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbAdminAcessoItem
 *
 * @ORM\Table(name="tb_admin_acesso_item")
 * @ORM\Entity
 */
class TbAdminAcessoItem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_admin_menu_item_id", type="integer", nullable=true)
     */
    private $tbAdminMenuItemId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="config", type="text", length=65535, nullable=true)
     */
    private $config;

    /**
     * @var string|null
     *
     * @ORM\Column(name="exportar", type="string", length=0, nullable=true, options={"default"="N"})
     */
    private $exportar = 'N';


}
