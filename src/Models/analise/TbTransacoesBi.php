<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbTransacoesBi
 *
 * @ORM\Table(name="tb_transacoes_bi", indexes={@ORM\Index(name="idx_pessoa_data", columns={"tb_pessoa_id", "data_criacao"}), @ORM\Index(name="idx_invoice", columns={"invoice_id"})})
 * @ORM\Entity
 */
class TbTransacoesBi
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_assinatura_id", type="integer", nullable=true)
     */
    private $tbAssinaturaId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataCriacao = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_gera_fatura", type="date", nullable=true)
     */
    private $dataGeraFatura;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_vencimento", type="date", nullable=true)
     */
    private $dataVencimento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="data_pago", type="string", length=45, nullable=true)
     */
    private $dataPago;

    /**
     * @var string|null
     *
     * @ORM\Column(name="valor", type="decimal", precision=13, scale=4, nullable=true)
     */
    private $valor;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="forma_pagamento", type="boolean", nullable=true)
     */
    private $formaPagamento;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="origem_transacao", type="boolean", nullable=true)
     */
    private $origemTransacao;

    /**
     * @var int|null
     *
     * @ORM\Column(name="periodicidade", type="integer", nullable=true)
     */
    private $periodicidade;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="gateway_pagamento", type="boolean", nullable=true)
     */
    private $gatewayPagamento;

    /**
     * @var string|null
     *
     * @ORM\Column(name="customer_id", type="string", length=45, nullable=true)
     */
    private $customerId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="invoice_id", type="string", length=100, nullable=true)
     */
    private $invoiceId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="status", type="string", length=45, nullable=true)
     */
    private $status;

    /**
     * @var string|null
     *
     * @ORM\Column(name="url", type="string", length=300, nullable=true)
     */
    private $url;

    /**
     * @var string|null
     *
     * @ORM\Column(name="numero_recibo", type="string", length=40, nullable=true)
     */
    private $numeroRecibo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="token_cartao_credito", type="string", length=255, nullable=true)
     */
    private $tokenCartaoCredito;


}
