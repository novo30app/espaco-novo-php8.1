<?php


namespace App\Models\Entities;
use Doctrine\ORM\Mapping as ORM;

/**
 * TbValoresFiliacao
 *
 * @Table(name="tb_valores_filiacao")
 * @Entity
 */
class TbValoresFiliacao
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @Column(name="valor", type="decimal", precision=15, scale=2, nullable=true)
     */
    private $valor;

    /**
     * @var string|null
     *
     * @Column(name="periodo", type="string", length=45, nullable=true)
     */
    private $periodo;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return TbValoresFiliacao
     */
    public function setId(int $id): TbValoresFiliacao
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getValor(): ?string
    {
        return $this->valor;
    }

    /**
     * @param string|null $valor
     * @return TbValoresFiliacao
     */
    public function setValor(?string $valor): TbValoresFiliacao
    {
        $this->valor = $valor;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPeriodo(): ?string
    {
        return $this->periodo;
    }

    /**
     * @param string|null $periodo
     * @return TbValoresFiliacao
     */
    public function setPeriodo(?string $periodo): TbValoresFiliacao
    {
        $this->periodo = $periodo;
        return $this;
    }



}
