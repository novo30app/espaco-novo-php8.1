<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbRecibosSubir
 *
 * @ORM\Table(name="tb_recibos_subir", indexes={@ORM\Index(name="idx_tb_recibos_subir_numero_recibo", columns={"numero_recibo"}), @ORM\Index(name="idx_tb_recibos_subir_invoice_id", columns={"invoice_id"})})
 * @ORM\Entity
 */
class TbRecibosSubir
{
    /**
     * @var int
     *
     * @ORM\Column(name="id1", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nome", type="string", length=100, nullable=true)
     */
    private $nome;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cpf", type="string", length=25, nullable=true)
     */
    private $cpf;

    /**
     * @var string|null
     *
     * @ORM\Column(name="origem", type="string", length=20, nullable=true)
     */
    private $origem;

    /**
     * @var string|null
     *
     * @ORM\Column(name="invoice_id", type="string", length=100, nullable=true)
     */
    private $invoiceId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="numero_recibo", type="string", length=40, nullable=true)
     */
    private $numeroRecibo;


}
