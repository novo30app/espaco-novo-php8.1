<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbEventosCarrinho
 *
 * @ORM\Table(name="tb_eventos_carrinho")
 * @ORM\Entity
 */
class TbEventosCarrinho
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_eventos_transacoes_id", type="integer", nullable=true)
     */
    private $tbEventosTransacoesId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_eventos_id", type="integer", nullable=true)
     */
    private $tbEventosId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_eventos_precos_id", type="integer", nullable=true)
     */
    private $tbEventosPrecosId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="quantidade", type="integer", nullable=true)
     */
    private $quantidade;

    /**
     * @var string|null
     *
     * @ORM\Column(name="valor_unitario", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $valorUnitario;

    /**
     * @var string|null
     *
     * @ORM\Column(name="session_id", type="string", length=150, nullable=true)
     */
    private $sessionId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="status", type="string", length=0, nullable=true, options={"default"="pendente"})
     */
    private $status = 'pendente';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataCriacao = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_atualizacao", type="datetime", nullable=true)
     */
    private $dataAtualizacao;


}
