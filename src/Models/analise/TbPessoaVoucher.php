<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbPessoaVoucher
 *
 * @ORM\Table(name="tb_pessoa_voucher")
 * @ORM\Entity
 */
class TbPessoaVoucher
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="code", type="string", length=20, nullable=true, options={"default"=" "})
     */
    private $code = ' ';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="visualizado", type="boolean", nullable=true)
     */
    private $visualizado = '0';


}
