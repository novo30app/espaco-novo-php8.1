<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * TbComentarios
 *
 * @ORM\Table(name="tb_comentarios", indexes={@ORM\Index(name="idx_tb_comentarios_comentario", columns={"comentario"}), @ORM\Index(name="idx_tb_comentarios_tb_pessoa_id", columns={"tb_pessoa_id"})})
 * @ORM\Entity
 */
class TbComentarios
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tb_admin_id", type="string", length=11, nullable=true)
     */
    private $tbAdminId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comentario", type="string", length=200, nullable=true)
     */
    private $comentario;

    /**
     * @var string|null
     *
     * @ORM\Column(name="status", type="string", length=50, nullable=true)
     */
    private $status;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataCriacao = 'CURRENT_TIMESTAMP';

    /**
     * @var string|null
     *
     * @ORM\Column(name="observacao", type="string", length=25, nullable=true)
     */
    private $observacao;


}
