<?php

namespace App\Models\Repository;

use App\Models\Entities\LgpdNews;
use Doctrine\ORM\EntityRepository;

class LgpdNewsRepository extends EntityRepository
{
    public function save(LgpdNews $entity): LgpdNews
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($user, $name = null, &$params): string
    {
        $where = '';
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND l.title LIKE :name";
        }
        if($user->getId() != 23){
            $where .= "AND l.status = 1";
        }
        return $where;
    }

    public function list($user, $name = null, $limit = null, $offset = null): array
    {
        $params = [];
        $where = $this->generateWhere($user, $name, $params);
        $limitSql = $this->generateLimit($limit, $offset);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT l.id, DATE_FORMAT(l.created_at, '%d/%m/%Y') AS created_at, l.title, l.file, l.status
                FROM lgpdNews l
                WHERE 1 = 1
                ORDER BY l.id DESC {$where} {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal($user, $name = null): array
    {
        $params = [];
        $where = $this->generateWhere($user, $name, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT DISTINCT(COUNT(l.id)) AS total
                FROM lgpdNews l
                WHERE 1 = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }
}