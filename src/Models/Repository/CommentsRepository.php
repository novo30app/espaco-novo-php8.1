<?php

namespace App\Models\Repository;

use App\Models\Entities\Comments;
use Doctrine\ORM\EntityRepository;

class CommentsRepository extends EntityRepository
{
    public function save(Comments $entity):Comments
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}