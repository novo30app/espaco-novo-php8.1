<?php

namespace App\Models\Repository;

use App\Models\Entities\CpfTmp;
use Doctrine\ORM\EntityRepository;

class CpfTmpRepository extends EntityRepository
{
    public function save(CpfTmp $entity):CpfTmp
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}