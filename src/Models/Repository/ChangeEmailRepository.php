<?php

namespace App\Models\Repository;

use App\Models\Entities\ChangeEmail;
use Doctrine\ORM\EntityRepository;

class ChangeEmailRepository extends EntityRepository
{
    public function save(ChangeEmail $entity):ChangeEmail
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}