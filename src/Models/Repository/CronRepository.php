<?php

namespace App\Models\Repository;

use App\Models\Entities\Cron;
use Doctrine\ORM\EntityRepository;

class CronRepository extends EntityRepository
{
    public function save(Cron $entity):Cron
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function check($task, $date)
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT * FROM cron WHERE tarefa = :task AND data LIKE :date";
        $rows = $pdo->prepare($sql)->execute([':task' => $task, ':date' => "%$date%"]);
        return $rows->fetchAssociative();        
    }
}