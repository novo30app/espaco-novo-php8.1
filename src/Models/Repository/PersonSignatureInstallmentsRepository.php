<?php

namespace App\Models\Repository;

use App\Models\Entities\PersonSignatureInstallments;
use Doctrine\ORM\EntityRepository;

class PersonSignatureInstallmentsRepository extends EntityRepository
{
    public function save(PersonSignatureInstallments $entity) {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function charge() 
    {
        $dueDate = date('Y-m-d');
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT p.id, p.signature, p.value, p.dueDate, tp.tb_pessoa_id AS user, tp.forma_pagamento AS paymentMethod, tp.plano_de_contribuicao AS paymentPlan,
                  tp.origem_transacao AS origin, tp.periodicidade AS plan, tp.token_cartao_credito AS cardToken, tp.data_gera_fatura
                FROM personSignatureInstallments p
                LEFT JOIN tb_pessoa_assinatura tp ON tp.id = p.signature
                WHERE tp.status = 'created' AND tp.origem_transacao = 3 AND tp.data_gera_fatura IS NULL AND DATE_FORMAT(p.dueDate, '%Y-%m-%d') <= '$dueDate'
                UNION ALL
                SELECT '' AS id, tp.id AS signature, tp.valor, tp.data_gera_fatura, tp.tb_pessoa_id AS user, tp.forma_pagamento AS paymentMethod,
                    tp.plano_de_contribuicao AS paymentPlan, tp.origem_transacao AS origin, tp.periodicidade AS plan, tp.token_cartao_credito AS cardToken, 
                    tp.data_gera_fatura
                FROM tb_pessoa_assinatura tp
                WHERE tp.status = 'created' AND tp.origem_transacao = 3 AND tp.data_gera_fatura IS NOT NULL AND DATE_FORMAT(tp.data_gera_fatura, '%Y-%m-%d') <= '$dueDate'";
        $rows = $pdo->prepare($sql)->execute();
        return $rows->fetchAllAssociative();
    }
}