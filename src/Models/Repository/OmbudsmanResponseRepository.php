<?php

namespace App\Models\Repository;

use App\Models\Entities\OmbudsmanResponse;
use Doctrine\ORM\EntityRepository;

class OmbudsmanResponseRepository extends EntityRepository
{
    public function save(OmbudsmanResponse $entity): OmbudsmanResponse
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}