<?php

namespace App\Models\Repository;

use App\Models\Entities\Quiz1;
use Doctrine\ORM\EntityRepository;

class Quiz1Repository extends EntityRepository
{
    public function save(Quiz1 $entity): Quiz1
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }


}