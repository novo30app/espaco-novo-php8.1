<?php

namespace App\Models\Repository;

use App\Models\Entities\EventsPrices;
use Doctrine\ORM\EntityRepository;

class EventsPricesRepository extends EntityRepository
{
    public function save(EventsPrices $entity):EventsPrices
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}