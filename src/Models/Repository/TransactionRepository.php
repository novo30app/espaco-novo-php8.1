<?php

namespace App\Models\Repository;

use App\Models\Entities\Directory;
use App\Models\Entities\Transaction;
use App\Models\Entities\User;
use Doctrine\ORM\EntityRepository;

class TransactionRepository extends EntityRepository
{
    public function save(Transaction $entity):Transaction
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function checkPix(): array
    {
        $today = new \DateTime();
        $today->sub(new \DateInterval('P1D'));
        return $this->getEntityManager()
                ->createQuery("SELECT t FROM App\Models\Entities\Transaction AS t                                  
                              WHERE t.dataCriacao >= :today AND t.formaPagamento = 11 AND t.status = 'pending'")
                ->setParameters([':today' => $today])
                ->getResult() ?? [];
    }

    public function getByUser(User $user): array
    {
        return $this->getEntityManager()
            ->createQuery("SELECT t FROM App\Models\Entities\Transaction AS t                                  
                              WHERE t.tbPessoaId = :user AND t.status <> 'canceled' ORDER BY t.dataCriacao DESC")
            ->setParameters([':user' => $user->getId()])
            ->getResult() ?? [];
    }

    public function validateDonation(User $user, float $value, Directory $directory)
    {
        if (ENV != 'prod') return true;
        $today = new \DateTime();
        $donationsDay = $this->getEntityManager()
                ->createQuery("SELECT t FROM App\Models\Entities\Transaction AS t
                              WHERE t.tbPessoaId = :user AND t.origemTransacao  = 1
                              AND t.dataCriacao BETWEEN :dateMin AND :dateMax AND t.origemTransacao = 1")
                ->setParameters([':user' => $user->getId(), ':dateMin' => $today->format('Y-m-d 00:00:00'), 'dateMax' => $today->format('Y-m-d 23:59:59')])
                ->getResult();
        if ($donationsDay){
            //throw new \Exception("Você só pode fazer uma doação por dia.");
        }
        $donationsDayValue = $this->getEntityManager()
            ->createQuery("SELECT SUM(t.valor) FROM App\Models\Entities\Transaction AS t                                  
                              WHERE t.tbPessoaId = :user 
                              AND t.dataPago BETWEEN :dateMin AND :dateMax 
                              AND t.status = 'paid' ")
            ->setParameters([':user' => $user->getId(), ':dateMin' => $today->format('Y-m-d 00:00:00'), 'dateMax' => $today->format('Y-m-d 23:59:59')])
            ->getResult();
        $totalDay = (float)$donationsDayValue[0][1];
        if ($totalDay + $value > 1064){
            $avaible = 1064 - $totalDay;
            $avaible = number_format($avaible,2,',','.');
            $totalDay = number_format($totalDay,2,',','.');
            throw new \Exception("Valor máximo para a doação via sistema é R$ 1064,00 por dia. Você já doou R$ {$totalDay}, 
                                          você pode doar ainda via sistema R$ {$avaible}. <br> 
                                          Para doar um valor superior faça uma transfêrencia bancaria:<br> 
                                          <b>Banco:</b> {$directory->getBank()} <br>
                                          <b>Agência:</b> {$directory->getAgency()} <br>
                                          <b>Conta:</b> {$directory->getAccount()} <br>
                                          <b>CNPJ:</b> {$directory->getCnpj()} <br>");
        }
    }

    public function getTransaction(User $user): ?Transaction
    {
        $today = new \DateTime();
        $dateFormat = $today->format('Y-m');
        $query = $this->getEntityManager()
                ->createQuery("SELECT t FROM App\Models\Entities\Transaction AS t                                  
                                  WHERE t.tbPessoaId = :user AND t.origemTransacao = 2 ORDER BY t.dataCriacao DESC")
                ->setParameters([':user' => $user->getId()])
                ->getResult();
            
        if (!$query) {
            return null;
        } else {
            return $query[0];
        }
    }

    public function lastTransaction(User $user): array
    {
        $query = $this->getEntityManager()
                        ->createQuery("SELECT max(t.id) FROM App\Models\Entities\Transaction AS t WHERE t.tbPessoaId = :user AND t.origemTransacao = :origem AND t.periodicidade != :periodicidade AND t.status = :status")
                        ->setParameters([':user' => $user->getId(), ':origem' => 2, ':periodicidade' => 0, ':status' => 'pending'])
                        ->getResult();
        return $query[0];
    }

    public function generateDonations(): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT ta.id AS assinaturaId, tp.id, tp.nome, tp.email, tp.cpf, ta.valor, ta.tb_diretorio_origem, ta.forma_pagamento, ta.gateway_pagamento, ta.data_gera_fatura, ta.dia_ciclo,
                    ta.customer_id , IF(ta.gateway_pagamento = 2, ta.token_cartao_credito, '') AS token_cartao_credito, 
                    DATE_FORMAT(ta.data_gera_fatura, '%Y-%m-%d') AS data_gera_fatura, td.nome AS diretorio
                FROM tb_pessoa_assinatura ta
                LEFT JOIN tb_pessoa tp ON tp.id = ta.tb_pessoa_id
                LEFT JOIN tb_diretorio td ON td.id = ta.tb_diretorio_origem
                WHERE ta.origem_transacao = 1 AND ta.status = 'created' AND ta.periodicidade = 1 AND date_format(ta.data_gera_fatura, '%d/%m/%Y') = date_format(curdate(), '%d/%m/%Y')";
        $rows = $pdo->prepare($sql)->execute();
        return $rows->fetchAllAssociative();
    }

    public function avoidDuplication($id = null, $plan = null, $data_gera_fatura = null): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(*) AS 'total' 
                FROM tb_transacoes tt
                WHERE tt.tb_pessoa_id = :id AND 
                      tt.origem_transacao IN (2,8) AND 
                      tt.periodicidade = :plan AND 
                      DATE_FORMAT(tt.data_criacao, '%m/%Y') = DATE_FORMAT(:dataFatura, '%m/%Y')";
        $rows = $pdo->prepare($sql)->execute([':id' => $id, ':plan' => $plan, ':dataFatura' => $data_gera_fatura]);
        return $rows->fetchAssociative();
    }

    public function chargeCard(): array
    {
        try {
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();

            $sql = "SELECT ta.id, tp.filiado_id, ta.tb_pessoa_id, ta.customer_id, ta.gateway_pagamento, ta.valor, ta.forma_pagamento, ta.origem_transacao, tp.pendencia_titulo, 
                        ta.periodicidade, ta.data_gera_fatura, ta.dia_ciclo, 1 AS tb_diretorio_origem, ta.token_cartao_credito, tp.email, tp.cpf, tp.nome, IFNULL(tp.suspensao, 0) AS suspensao,
                        IFNULL((SELECT REGEXP_REPLACE(telefone, '[^0-9]', '')
                                FROM tb_telefone tbtel 
                                WHERE ((tbtel.tb_pessoa_id = tp.id) 
                                    AND (tbtel.tb_tipo_telefone_id = 3) 
                                    AND (tbtel.contact != 0 || tbtel.contact IS NULL)) 
                                LIMIT 1), '') AS phone
                    FROM tb_pessoa_assinatura ta
                    JOIN tb_pessoa tp ON tp.id = ta.tb_pessoa_id
                    WHERE tp.filiado IN (7,8) 
                        AND ta.origem_transacao IN(2,3) 
                        AND ta.forma_pagamento = 1 
                        AND DATE(ta.data_gera_fatura) <= CURDATE() 
                        AND ta.status = 'created' 
                        AND (tp.bloqueado IS NULL OR tp.bloqueado = 0) 
                        AND ta.valor > 0
                    UNION ALL 
                    SELECT ta.id, tp.filiado_id, ta.tb_pessoa_id, ta.customer_id, ta.gateway_pagamento, ta.valor, ta.forma_pagamento, ta.origem_transacao, tp.pendencia_titulo, 
                        ta.periodicidade, ta.data_gera_fatura, ta.dia_ciclo, ta.tb_diretorio_origem, ta.token_cartao_credito, tp.email, tp.cpf, tp.nome, IFNULL(tp.suspensao, 0) AS suspensao,
                        IFNULL((SELECT REGEXP_REPLACE(telefone, '[^0-9]', '')
                                FROM tb_telefone tbtel  
                                WHERE ((tbtel.tb_pessoa_id = tp.id) 
                                    AND (tbtel.tb_tipo_telefone_id = 3) 
                                    AND (tbtel.contact != 0 || tbtel.contact IS NULL)) 
                                LIMIT 1), '') AS phone
                        FROM tb_pessoa_assinatura ta
                        JOIN tb_pessoa tp ON tp.id = ta.tb_pessoa_id
                        WHERE ta.origem_transacao = 1 
                            AND ta.forma_pagamento = 1 
                            AND DATE(ta.data_gera_fatura) <= CURDATE() 
                            AND ta.status = 'created' 
                            AND (tp.bloqueado IS NULL OR tp.bloqueado = 0)
                        ORDER BY gateway_pagamento DESC";
                    
            $rows = $pdo->prepare($sql)->execute();
            return $rows->fetchAllAssociative();

        } catch (\Exception $e) {
            throw new \Exception("Erro ao buscar cobranças: " . $e->getMessage());
        }
    }

    public function chargeBillet(): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT ta.id, tp.filiado_id, ta.tb_pessoa_id, ta.customer_id, ta.gateway_pagamento, ta.valor, 
                    REGEXP_REPLACE(FORMAT(ta.valor, 2), '\\.', ',') AS value, ta.forma_pagamento, ta.origem_transacao, tp.pendencia_titulo, 
                    ta.periodicidade, ta.data_gera_fatura, ta.dia_ciclo, 1 AS tb_diretorio_origem, ta.token_cartao_credito, tp.email, tp.cpf, 
                    tp.nome, IFNULL(tp.suspensao, 0) AS suspensao, tp.filiado,
                    IFNULL((SELECT REGEXP_REPLACE(telefone, '[^0-9]', '')
                            FROM tb_telefone tbtel
                            WHERE ((tbtel.tb_pessoa_id = tp.id) AND 
                                  (tbtel.tb_tipo_telefone_id = 3) AND 
                                  (tbtel.contact != 0 || tbtel.contact IS NULL)) LIMIT 1), '') AS phone
                    FROM tb_pessoa_assinatura ta
                    JOIN tb_pessoa tp ON tp.id = ta.tb_pessoa_id
                    WHERE tp.filiado IN (7,8) AND 
                          ta.origem_transacao IN (2,3) AND 
                          ta.forma_pagamento = 2 AND 
                          ta.data_gera_fatura <= FROM_DAYS((TO_DAYS(CURDATE()) + 5)) AND 
                          ta.status = 'created' AND (tp.bloqueado IS NULL OR tp.bloqueado = 0) 
                          AND ta.valor > 0
                UNION ALL 
                SELECT ta.id, tp.filiado_id, ta.tb_pessoa_id, ta.customer_id, ta.gateway_pagamento, ta.valor, 
                    REGEXP_REPLACE(FORMAT(ta.valor, 2), '\\.', ',') AS value, ta.forma_pagamento, ta.origem_transacao, tp.pendencia_titulo, 
                    ta.periodicidade, ta.data_gera_fatura, ta.dia_ciclo, IFNULL(ta.tb_diretorio_origem, 1) AS tb_diretorio_origem, 
                    ta.token_cartao_credito, tp.email, tp.cpf, tp.nome, IFNULL(tp.suspensao, 0) AS suspensao, tp.filiado,
                    IFNULL((SELECT REGEXP_REPLACE(telefone, '[^0-9]', '')
                            FROM tb_telefone tbtel  
                            WHERE ((tbtel.tb_pessoa_id = tp.id) AND 
                                  (tbtel.tb_tipo_telefone_id = 3) AND 
                                  (tbtel.contact != 0 || tbtel.contact IS NULL)) LIMIT 1), '') AS phone
                    FROM tb_pessoa_assinatura ta
                    JOIN tb_pessoa tp ON tp.id = ta.tb_pessoa_id
                    WHERE ta.origem_transacao = 1 AND 
                          ta.forma_pagamento = 2 AND 
                          ta.periodicidade = 1 AND 
                          ta.data_gera_fatura <= FROM_DAYS((TO_DAYS(CURDATE()) + 5)) AND 
                          ta.status = 'created' AND 
                          (tp.bloqueado IS NULL OR tp.bloqueado = 0)";
        $rows = $pdo->prepare($sql)->execute();
        return $rows->fetchAllAssociative();
    }

    public function active($id): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT * FROM tb_pessoa_assinatura ta
                WHERE ta.periodicidade = 1 AND ta.status = 'created' AND ta.tb_pessoa_id = :id
                    AND ta.tb_pessoa_id IN (
                        SELECT tbt.tb_pessoa_id FROM tb_transacoes tbt 
                            WHERE tbt.status = 'paid' AND tbt.data_pago BETWEEN (CURDATE() - INTERVAL 10 MONTH) AND CURDATE() ||
                                    tbt.obs = 'Filiação Isenta' AND tbt.data_criacao BETWEEN (CURDATE() - INTERVAL 10 MONTH) AND CURDATE()
                            GROUP BY tbt.tb_pessoa_id)";
        $rows = $pdo->prepare($sql)->execute([':id' => $id]);
        return $rows->fetchAllAssociative();
    }

    public function active2($id): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT * FROM tb_pessoa_assinatura ta
                WHERE ta.periodicidade = 1 AND ta.status = 'created' AND ta.tb_pessoa_id = :id
                    AND ta.tb_pessoa_id IN (
                        SELECT tbt.tb_pessoa_id FROM tb_transacoes tbt 
                            WHERE (tbt.status = 'paid' AND tbt.data_pago BETWEEN (CURDATE() - INTERVAL 15 MONTH) AND (CURDATE() - INTERVAL 8 MONTH))
                            GROUP BY tbt.tb_pessoa_id)";
        $rows = $pdo->prepare($sql)->execute([':id' => $id]);
        return $rows->fetchAllAssociative();
    }

    public function reprocessChargeCard(): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT  
                    tt.id, tp.filiado_id, tt.tb_pessoa_id, tp.cpf, tt.valor, tt.data_criacao, DATE_FORMAT(tt.data_criacao, '%d/%m/%Y') as data_criacao_format, tt.periodicidade, 
                    tt.forma_pagamento, tt.tb_assinatura_id, IFNULL(tt.tb_diretorio_origem, 1) AS  tb_diretorio_origem, ta.token_cartao_credito, tp.email, tp.nome, ta.gateway_pagamento, 
                    tt.origem_transacao, TO_DAYS(CURDATE()) - TO_DAYS(CAST(tt.data_criacao AS DATE)) AS days, tt.invoice_id,
                    IFNULL((SELECT REGEXP_REPLACE(telefone, '[^0-9]', '')
                            FROM tb_telefone tbtel  
                            WHERE ((tbtel.tb_pessoa_id = tp.id) AND 
                                  (tbtel.tb_tipo_telefone_id = 3) AND 
                                  (tbtel.contact != 0 || tbtel.contact IS NULL)) LIMIT 1), '') AS phone
                FROM tb_transacoes tt
                JOIN tb_pessoa tp ON tp.id = tt.tb_pessoa_id
                JOIN tb_pessoa_assinatura ta ON tt.tb_pessoa_id = ta.tb_pessoa_id
                LEFT JOIN tb_endereco en ON en.tb_pessoa_id = tp.id

                WHERE tp.filiado IN (7,8) AND 
                      ta.valor > 0  AND 
                      tt.status IN ('expired' , 'pending') AND 
                      tt.origem_transacao IN (2,3) AND 
                      tt.fase_cobranca < 4 AND 
                      NOT CAST(tt.ultima_atualizacao AS DATE) <=> CURDATE() AND
                      tt.forma_pagamento = 1 AND 
                      TO_DAYS(CURDATE()) - TO_DAYS(CAST(tt.data_criacao AS DATE)) IN (2, 4, 10) AND 
                      (tp.bloqueado IS NULL OR tp.bloqueado = 0) AND 
                      (tt.obs != 'não-reprocessa' || tt.obs IS NULL)
                GROUP BY tt.id ORDER BY tt.periodicidade DESC";

        $rows = $pdo->prepare($sql)->execute();
        return $rows->fetchAllAssociative();
    }

    public function reprocessChargeBillet(): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT 
                    tt.id, tp.filiado_id, tp.nome, tp.cpf, tp.id AS tb_pessoa_id, tt.origem_transacao, tt.periodicidade, tt.data_criacao, tt.url, tp.email, tt.data_vencimento, 
                    tt.valor, TO_DAYS(CURDATE()) - TO_DAYS(CAST(tt.data_criacao AS DATE)) AS days, tt.codigo_barras, tt.qrcode,
                    IFNULL((SELECT REGEXP_REPLACE(telefone, '[^0-9]', '')
                            FROM tb_telefone tbtel  
                            WHERE ((tbtel.tb_pessoa_id = tp.id) AND 
                                  (tbtel.tb_tipo_telefone_id = 3) AND 
                                  (tbtel.contact != 0 || tbtel.contact IS NULL)) LIMIT 1), '') AS phone,
                    TO_DAYS(CURDATE()) - TO_DAYS(CAST(tt.data_vencimento AS DATE)) AS days
                FROM tb_transacoes tt
                JOIN tb_pessoa tp ON tp.id = tt.tb_pessoa_id
                JOIN tb_pessoa_assinatura ta ON tt.tb_pessoa_id = ta.tb_pessoa_id
                LEFT JOIN tb_endereco en ON en.tb_pessoa_id = tp.id

                WHERE tp.filiado IN (7,8) AND 
                      tp.exemption = 0 AND 
                      tt.status IN ('expired' , 'pending') AND 
                      tt.origem_transacao IN (2, 3) AND 
                      NOT (CAST(tt.ultima_atualizacao AS DATE) <=> CURDATE()) AND
                      tt.forma_pagamento = 2 AND 
                      TO_DAYS(CURDATE()) - TO_DAYS(CAST(tt.data_vencimento AS DATE)) IN (1) AND 
                      (tp.bloqueado IS NULL OR tp.bloqueado = 0)
                GROUP BY tt.id ORDER BY tt.periodicidade DESC";

        $rows = $pdo->prepare($sql)->execute();
        return $rows->fetchAllAssociative();
    }

    public function sendZap(): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT tt.id, tp.id AS tb_pessoa_id, url,
                        IFNULL((SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(tbtel.telefone, '-', ''),'(',''),')',''),' ',''),'+','')
                            FROM tb_telefone tbtel  WHERE ((tbtel.tb_pessoa_id = tp.id) AND (tbtel.tb_tipo_telefone_id = 3) AND (tbtel.contact != 0 || tbtel.contact IS NULL)) LIMIT 1), '') AS phone,
                            TO_DAYS(CURDATE()) - TO_DAYS(CAST(tt.data_vencimento AS DATE)) AS days
                FROM tb_transacoes tt
                JOIN tb_pessoa tp ON tp.id = tt.tb_pessoa_id
                JOIN tb_pessoa_assinatura ta ON tt.tb_pessoa_id = ta.tb_pessoa_id
                WHERE tp.filiado IN (7,8) AND tp.exemption = 0 AND tt.status IN ('expired' , 'pending') AND tt.origem_transacao = 2 AND NOT (CAST(tt.ultima_atualizacao AS DATE) <=> CURDATE())
                    AND tt.forma_pagamento = 2 AND TO_DAYS(CURDATE()) - TO_DAYS(CAST(tt.data_vencimento AS DATE)) IN ((-1), 0) AND (tp.bloqueado IS NULL OR tp.bloqueado = 0)
                GROUP BY tt.id ORDER BY tt.periodicidade DESC
                LIMIT 15";
        $rows = $pdo->prepare($sql)->execute();
        return $rows->fetchAllAssociative();
    }

    public function totvsRevenue(): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT * FROM totvs_dados";
        $rows = $pdo->prepare($sql)->execute();
        return $rows->fetchAllAssociative();
    }

    public function registerTotvsTransactionError($id_transacao = null, $erro = null)
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "INSERT INTO totvs_logs (id_transacao, erro) VALUES ($id_transacao, $erro)";
        $sth = $pdo->prepare($sql);
        $sth->execute();
    }

    public function lastDirectoryReceipt($directory)
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT LPAD(SUBSTRING(numero_recibo, -6), 6, 0) AS receipt 
                FROM tb_transacoes 
                WHERE numero_recibo LIKE 'P%' AND status = 'paid' AND numero_recibo IS NOT NULL AND tb_diretorio_id = :directory
                ORDER BY id DESC
                LIMIT 1";
        $rows = $pdo->prepare($sql)->execute([':directory' => $directory]);
        return $rows->fetchAllAssociative();   
    }

    public function getTransactionsMonthlyByDirectory(Directory $directory): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT *
                FROM tb_transacoes tt
                WHERE tt.tb_diretorio_id = :directory AND DATE_FORMAT(tt.data_pago, '%Y-%m') = :date";
        $rows = $pdo->prepare($sql)->execute([':directory' => $directory->getId(), ':date' => date('Y-m')]);
        return $rows->fetchAllAssociative();   
    }
    
    public function reportIncome($id): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT DATE_FORMAT(`tbt`.`data_pago`, '%d/%m/%Y') AS data_pago, tbt.numero_recibo, tbd.cnpj AS cnpj, tbd.nome AS diretorio,
                    (CASE `tbt`.`origem_transacao`
                        WHEN 1 THEN 'Doação'
                        WHEN 2 THEN 'Filiação'
                        WHEN 3 THEN 'Doação Campanha'
                        WHEN 4 THEN 'Evento'
                        WHEN 5 THEN 'Complemento de Filiação'
                        WHEN 6 THEN 'Doação'
                        WHEN 15 THEN 'Filiação antecipada'
                        END) AS `origem_transacao`, 
                    REPLACE(ROUND(`tbt`.`valor`, 2), '.', ',') AS `valor`                
                FROM tb_transacoes tbt
                LEFT JOIN tb_diretorio tbd ON tbd.id = tbt.tb_diretorio_id
                WHERE tbt.tb_pessoa_id = :id AND tbt.status = :status AND YEAR(tbt.data_pago) = :year AND IF(tbt.gateway_pagamento = 3, tbt.tb_diretorio_id != :directoryId, 1 = 1)
                ORDER BY tbd.id ASC, tbt.data_pago ASC";
        $rows = $pdo->prepare($sql)->execute([':id' => $id, ':status' => 'paid', ':year' => date('Y') - 1, ':directoryId' => 27]);
        return $rows->fetchAllAssociative();
    }
    
    public function reportIncomeTotal($id): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT REPLACE(ROUND(SUM(`tbt`.`valor`), 2), '.', ',') AS `total`, cnpj, tbd.nome AS directory
                FROM tb_transacoes tbt
                LEFT JOIN tb_diretorio tbd ON tbd.id = tbt.tb_diretorio_id
                WHERE tbt.tb_pessoa_id = :id AND tbt.status = :status AND YEAR(tbt.data_pago) = :year AND IF(tbt.gateway_pagamento = 3, tbt.tb_diretorio_id != :directoryId, 1 = 1)
                GROUP BY cnpj
                ORDER BY tbd.id ASC, tbt.data_pago ASC";
        $rows = $pdo->prepare($sql)->execute([':id' => $id, ':status' => 'paid', ':year' => date('Y') - 1, 'directoryId' => 27]);
        return $rows->fetchAllAssociative();
    }

    public function getDateLastTransaction($id)
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT * FROM tb_transacoes WHERE tb_pessoa_id = :id AND origem_transacao = :origin AND periodicidade != :plan ORDER BY data_criacao DESC LIMIT 1";
        $rows = $pdo->prepare($sql)->execute([':id' => $id, ':origin' => 2, ':plan' => 0]);
        return $rows->fetchAllAssociative();
    }

    public function generateAffiliationExemption()
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT tp.id, tp.nome AS name, ta.id AS signature, ta.dia_ciclo, ta.periodicidade, ta.data_gera_fatura, tp.email, tp.cpf, tp.filiado_id
                FROM tb_pessoa_assinatura ta
                JOIN tb_pessoa tp ON tp.id = ta.tb_pessoa_id
                WHERE tp.filiado IN (7,8) AND ta.origem_transacao = 2  AND ta.data_gera_fatura <= CURDATE() AND ta.status = 'created' AND (tp.bloqueado IS NULL OR tp.bloqueado = 0) AND tp.exemption = 1 ";
        $rows = $pdo->prepare($sql)->execute();
        return $rows->fetchAllAssociative();
    }

    public function lastTransactionChatbot(User $user)
    {
        $query = $this->getEntityManager()
                        ->createQuery("SELECT t FROM App\Models\Entities\Transaction AS t WHERE t.tbPessoaId = :user AND t.origemTransacao = :origem AND t.status = :status ORDER BY t.id DESC")
                        ->setParameters([':user' => $user->getId(), ':origem' => 2, ':status' => 'pending'])
                        ->getResult();
        return $query[0];
    }

    public function monthlyPaymentsIugu()
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT * FROM tb_transacoes tbt
                WHERE LENGTH(invoice_id) > 9 AND tbt.gateway_pagamento = :gateway AND tbt.status = :status AND valor_s_taxas IS NULL AND
                DATE_FORMAT(data_pago, '%Y-%m-%d') = DATE_SUB(CURDATE(), INTERVAL 1 DAY)";
        $rows = $pdo->prepare($sql)->execute([':status' => 'paid', ':gateway' => 1]);
        return $rows->fetchAllAssociative();
    }

    public function iuguBilletWeeklyWithdraw()
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT * FROM tb_transacoes
                WHERE LENGTH(invoice_id) > :invoice AND gateway_pagamento = :gateway AND status = :status AND rescue IS NULL AND valor_s_taxas IS NOT NULL AND forma_pagamento != :paymentForm
                AND DATE_FORMAT(data_pago, '%Y-%m-%d') BETWEEN DATE_SUB(CURDATE(), INTERVAL 30 DAY) AND DATE_SUB(CURDATE(), INTERVAL 4 DAY) AND tb_diretorio_id = :directory
                UNION ALL 
                SELECT * FROM tb_transacoes
                WHERE LENGTH(invoice_id) > :invoice AND gateway_pagamento = :gateway AND status = :status AND rescue IS NULL AND valor_s_taxas IS NOT NULL AND forma_pagamento != :paymentForm AND
                DATE_FORMAT(data_pago, '%Y-%m-%d') BETWEEN DATE_SUB(CURDATE(), INTERVAL 30 DAY) AND DATE_SUB(CURDATE(), INTERVAL 3 DAY) AND tb_diretorio_id != :directory
                ORDER BY tb_diretorio_id ASC";
        $rows = $pdo->prepare($sql)->execute([':invoice' => 9, ':status' => 'paid', ':gateway' => 1, ':paymentForm' => 1, ':directory' => 1]);
        return $rows->fetchAllAssociative();
    }

    public function iuguCardDailyWithdraw()
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT * FROM tb_transacoes
                WHERE LENGTH(invoice_id) > 9 AND gateway_pagamento = :gateway AND status = :status AND rescue IS NULL AND valor_s_taxas IS NOT NULL AND forma_pagamento = :paymentForm
                AND data_liberacao <= CURDATE() AND tb_diretorio_id = :directory
                UNION ALL
                SELECT * FROM tb_transacoes
                WHERE LENGTH(invoice_id) > 9 AND gateway_pagamento = :gateway AND status = :status AND rescue IS NULL AND valor_s_taxas IS NOT NULL AND forma_pagamento = :paymentForm
                AND data_liberacao <= CURDATE() AND tb_diretorio_id != :directory
                ORDER BY tb_diretorio_id ASC";
        $rows = $pdo->prepare($sql)->execute([':status' => 'paid', ':gateway' => 1, ':paymentForm' => 1, ':directory' => 1]);
        return $rows->fetchAllAssociative();
    }

    public function getSigleTransactions(): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT  tt.id, tt.tb_pessoa_id, tt.valor, tt.token_cartao_credito, tt.origem_transacao
                FROM tb_transacoes tt
                JOIN tb_pessoa tp ON tp.id = tt.tb_pessoa_id
                JOIN tb_pessoa_assinatura ta ON tt.tb_pessoa_id = ta.tb_pessoa_id
                WHERE tp.filiado IN (7,8) AND tt.status IN ('expired' , 'pending') 
                        AND tt.origem_transacao = 2 AND tt.fase_cobranca < 4 
                        AND NOT CAST(tt.ultima_atualizacao AS DATE) <=> CURDATE()
                        AND tt.forma_pagamento = 1 
                        AND (tp.bloqueado IS NULL OR tp.bloqueado = 0) 
                        AND (tt.obs != 'não-reprocessa' OR tt.obs IS NULL)
                        AND tt.data_criacao LIKE '%2022-11-27%'
                        AND tt.id IN (0)
                GROUP BY tt.id ORDER BY tt.periodicidade DESC
                LIMIT 0";
        $rows = $pdo->prepare($sql)->execute();
        return $rows->fetchAllAssociative();
    }

    public function activeUpdatePersonalData(User $user): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT * FROM tb_transacoes tbt 
                LEFT JOIN tb_pessoa_assinatura tpa ON tpa.tb_pessoa_id = tbt.tb_pessoa_id
                WHERE tbt.tb_pessoa_id = :id AND tbt.status = 'paid' AND  tbt.origem_transacao = 2 AND
                IF(tpa.periodicidade = 1,
                    tbt.data_pago BETWEEN (CURDATE() - INTERVAL 2 MONTH) AND CURDATE(),
                    tbt.data_pago BETWEEN (CURDATE() - INTERVAL 13 MONTH) AND CURDATE())
                GROUP BY tbt.tb_pessoa_id";
        $rows = $pdo->prepare($sql)->execute([':id' => $user->getId()]);
        return $rows->fetchAllAssociative();
    }

    public function getLastOpenedTransaction(User $user)
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT * FROM tb_transacoes 
                WHERE tb_pessoa_id = :id AND origem_transacao = :origin AND status IN ('pending', 'expired')
                ORDER BY data_criacao DESC
                LIMIT 1";
        $rows = $pdo->prepare($sql)->execute([':id' => $user->getId(), ':origin' => 2]);
        return $rows->fetchAllAssociative()[0];
    }

    public function getTransactionsDirectories(): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT tt.id FROM tb_transacoes tt
                WHERE STR_TO_DATE(data_pago, '%Y-%m') = STR_TO_DATE(CURDATE(), '%Y-%m') AND gateway_pagamento = :gateway AND tb_diretorio_id != :directory 
                    AND (data_deposito IS NULL OR valor_s_taxas IS NULL)
                ORDER BY data_criacao ASC";
        $rows = $pdo->prepare($sql)->execute(['gateway' => 2, 'directory' => 1]);
        return $rows->fetchAllAssociative();
    }

    public function getReportIncome(User $user): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT * FROM tb_transacoes tt
                WHERE YEAR(tt.data_pago) = :year AND tt.status = :status AND tt.tb_pessoa_id = :user";
        $rows = $pdo->prepare($sql)->execute([':year' => date('Y') - 1, ':status' => 'paid', ':user' => $user->getId()]);
        return $rows->fetchAllAssociative();
    }

    public function verifyCompliance($id = null): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT 
                    IFNULL((SELECT IF(tbp.exemption = 1, 'Isento', IF(tbt.status = 'paid' , 'Adimplente', 'Inadimplente'))
                        FROM  tb_transacoes tbt
                        WHERE tbt.tb_pessoa_id = tbp.id AND tbt.status NOT IN ('accumulated' , 'canceled', 'reversed') AND IF(tbp.pendencia_titulo = 1, tbt.origem_transacao = 1, tbt.origem_transacao = 2)
                            AND if(tbt.forma_pagamento = 2 AND tbt.status != 'paid', tbt.data_criacao < DATE_SUB(CURDATE(), INTERVAL if(tbt.periodicidade in(1, 12), 5, 1) DAY), '1 = 1')
                        ORDER BY tbt.data_criacao DESC , tbt.id ASC LIMIT 1
                    ), 'Inadimplente') AS situation
                FROM tb_pessoa tbp
                LEFT JOIN tb_transacoes tbt ON tbt.tb_pessoa_id = tbp.id
                WHERE filiado IN (7,8) AND tbp.id = :id
                GROUP BY tbp.id";
        $rows = $pdo->prepare($sql)->execute([':id' => $id]);
        return $rows->fetchAssociative();
    }

    private string $sqlTest = "SELECT tt.id, tp.nome, tp.email,
                                    IFNULL((SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(tbtel.telefone, '-', ''),'(',''),')',''),' ',''),'+','')
                                        FROM tb_telefone tbtel  WHERE ((tbtel.tb_pessoa_id = tp.id) AND (tbtel.tb_tipo_telefone_id = 3) AND (tbtel.contact != 0 || tbtel.contact IS NULL)) LIMIT 1), '') AS phone
                                FROM tb_transacoes tt
                                JOIN tb_pessoa tp ON tp.id = tt.tb_pessoa_id
                                WHERE tt.id IN (44315184, 44315860, 44321594)";

    public function reprocessChargeCardApi(): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        /*
        $sql = "SELECT  tt.id, tp.nome, tp.email,
                    IFNULL((SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(tbtel.telefone, '-', ''),'(',''),')',''),' ',''),'+','')
                        FROM tb_telefone tbtel  WHERE ((tbtel.tb_pessoa_id = tp.id) AND (tbtel.tb_tipo_telefone_id = 3) AND (tbtel.contact != 0 || tbtel.contact IS NULL)) LIMIT 1), '') AS phone
                FROM tb_transacoes tt
                JOIN tb_pessoa tp ON tp.id = tt.tb_pessoa_id
                JOIN tb_pessoa_assinatura ta ON tt.tb_pessoa_id = ta.tb_pessoa_id
                WHERE tp.filiado IN (7,8) AND tt.status IN ('expired' , 'pending') AND tt.fase_cobranca < 4 AND tp.exemption = 0 AND (tp.bloqueado IS NULL OR tp.bloqueado = 0) AND (tt.obs != 'não-reprocessa' || tt.obs IS NULL)
                    AND tt.forma_pagamento = 1 AND tt.origem_transacao = 2 
                    AND MONTH(tt.data_criacao) = MONTH(CURDATE()) AND YEAR(tt.data_criacao) = YEAR(CURDATE())
                GROUP BY tt.id 
                ORDER BY tt.data_criacao DESC";
        */
        $sql = $this->sqlTest;
        $rows = $pdo->prepare($sql)->execute();
        return $rows->fetchAllAssociative();
    }

    public function reprocessChargeBilletApi(): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        /*
        $sql = "SELECT tt.id, tp.nome, tp.email,
                    IFNULL((SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(tbtel.telefone, '-', ''),'(',''),')',''),' ',''),'+','')
                        FROM tb_telefone tbtel  WHERE ((tbtel.tb_pessoa_id = tp.id) AND (tbtel.tb_tipo_telefone_id = 3) AND (tbtel.contact != 0 || tbtel.contact IS NULL)) LIMIT 1), '') AS phone
                FROM tb_transacoes tt
                JOIN tb_pessoa tp ON tp.id = tt.tb_pessoa_id
                JOIN tb_pessoa_assinatura ta ON tt.tb_pessoa_id = ta.tb_pessoa_id
                WHERE tp.filiado IN (7,8) AND tt.status IN ('expired' , 'pending') AND tt.fase_cobranca < 4  AND (tp.bloqueado IS NULL OR tp.bloqueado = 0) AND tp.exemption = 0
                    AND tt.origem_transacao = 2 AND tt.forma_pagamento = 2 AND ta.gateway_pagamento = 1
                    AND MONTH(tt.data_criacao) = MONTH(CURDATE()) AND YEAR(tt.data_criacao) = YEAR(CURDATE())
                    AND TO_DAYS(CURDATE()) - TO_DAYS(CAST(tt.data_criacao AS DATE)) < 1
                GROUP BY tt.id 
                ORDER BY tt.data_criacao DESC";
        */
        $sql = $this->sqlTest;
        $rows = $pdo->prepare($sql)->execute();
        return $rows->fetchAllAssociative();
    }

    public function reprocessChargeBilletApiDefeated(): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        /*
        $sql = "SELECT tt.id, tp.nome, tp.email, tt.data_criacao,
                    IFNULL((SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(tbtel.telefone, '-', ''),'(',''),')',''),' ',''),'+','')
                        FROM tb_telefone tbtel  WHERE ((tbtel.tb_pessoa_id = tp.id) AND (tbtel.tb_tipo_telefone_id = 3) AND (tbtel.contact != 0 || tbtel.contact IS NULL)) LIMIT 1), '') AS phone
                FROM tb_transacoes tt
                JOIN tb_pessoa tp ON tp.id = tt.tb_pessoa_id
                JOIN tb_pessoa_assinatura ta ON tt.tb_pessoa_id = ta.tb_pessoa_id
                WHERE tp.filiado IN (7,8) AND tt.status IN ('expired' , 'pending') AND tt.fase_cobranca < 4  AND (tp.bloqueado IS NULL OR tp.bloqueado = 0) AND tp.exemption = 0
                    AND tt.origem_transacao = 2 AND tt.forma_pagamento = 2 AND ta.gateway_pagamento = 1
                    AND MONTH(tt.data_criacao) = MONTH(CURDATE()) AND YEAR(tt.data_criacao) = YEAR(CURDATE())
                    AND TO_DAYS(CURDATE()) - TO_DAYS(CAST(tt.data_criacao AS DATE)) >= 1
                GROUP BY tt.id 
                ORDER BY tt.data_criacao DESC";
        */
        $sql = $this->sqlTest;
        $rows = $pdo->prepare($sql)->execute();
        return $rows->fetchAllAssociative();
    }

    public function validDonationDaily(string $ip): void
    {
        $today = new \DateTime();
        $donationsDay = $this->getEntityManager()
                ->createQuery("SELECT t FROM App\Models\Entities\User AS t
                              WHERE t.ip = :ip
                              AND t.dataCriacao BETWEEN :dateMin AND :dateMax")
                ->setParameters([':ip' => $ip, ':dateMin' => $today->format('Y-m-d 00:00:00'), 'dateMax' => $today->format('Y-m-d 23:59:59')])
                ->getResult();
        if ($donationsDay) throw new \Exception("Solicitação inválida!");
    }

    public function getTotalDebit(int $user, int $limit = null): array
    {
        try {
            if ($user <= 0) throw new \InvalidArgumentException("ID do usuário inválido");

            //Pega limite de transações
            if ($limit !== null) {
                $limit = "LIMIT $limit";
            }

            //Pega total de débitos do usuário
            $pdo = $this->getEntityManager()->getConnection();
            $sql = "SELECT 
                        COALESCE(SUM(valor), 0) AS total, 
                        GROUP_CONCAT(id ORDER BY data_criacao DESC) as ids
                    FROM (
                        SELECT valor, id, data_criacao
                        FROM tb_transacoes tbt
                        WHERE tbt.tb_pessoa_id = :user 
                            AND tbt.status IN ('expired', 'pending') 
                            AND tbt.origem_transacao IN (2, 8)
                        ORDER BY data_criacao DESC
                        $limit
                    ) AS sub";

            //Pega total de débitos do usuário
            $result = $pdo->executeQuery($sql, ['user' => $user])->fetch(\PDO::FETCH_ASSOC);
         
            //Retorna dados do total de débitos
            return [
                'total' => (float)($result['total'] ?? 0),
                'ids' => $result['ids'] ?? ''
            ];

        } catch (\PDOException $e) {
            throw new \Exception("Erro ao buscar total de débitos: " . $e->getMessage());
        }
    }

    public function groupMessageCogmo(): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT 
                    tt.id, 
                    tp.id AS tb_pessoa_id, 
                    tt.forma_pagamento,
                    tt.id AS transaction_id,
                    TO_DAYS(CURDATE()) - TO_DAYS(CAST(tt.data_vencimento AS DATE)) AS days,
                    IFNULL((SELECT CONCAT(55, REGEXP_REPLACE(telefone, '[^0-9]', ''))
                            FROM tb_telefone tbtel  
                            WHERE ((tbtel.tb_pessoa_id = tp.id) AND 
                                  (tbtel.tb_tipo_telefone_id = 3) AND 
                                  (tbtel.contact != 0 || tbtel.contact IS NULL)) LIMIT 1), '') AS phone

                FROM tb_transacoes tt

                LEFT JOIN tb_pessoa tp ON tp.id = tt.tb_pessoa_id
                LEFT JOIN tb_pessoa_assinatura ta ON tt.tb_pessoa_id = ta.tb_pessoa_id
                LEFT JOIN tb_endereco en ON en.tb_pessoa_id = tp.id

                WHERE tp.filiado IN (7,8) 
                      AND tp.exemption = 0 
                      AND tt.status IN ('expired' , 'pending') 
                      AND tt.origem_transacao IN (2, 3) 
                      AND NOT (CAST(tt.ultima_atualizacao AS DATE) <=> CURDATE()) 
                      AND tt.forma_pagamento = 1 
                      AND TO_DAYS(CURDATE()) - TO_DAYS(CAST(tt.data_vencimento AS DATE)) IN (0) 
                      AND (tp.bloqueado IS NULL OR tp.bloqueado = 0)
                      AND ta.forma_pagamento = 1
                      
                      AND tp.id IN (SELECT tb_pessoa_id 
                                        FROM tb_transacoes 
                                        WHERE status = 'paid' AND data_pago BETWEEN (CURDATE() - INTERVAL 6 MONTH) AND CURDATE()
                                        GROUP BY tb_pessoa_id)
                GROUP BY tt.id 
                ORDER BY RAND()
                LIMIT 500";
        $rows = $pdo->prepare($sql)->execute();
        return $rows->fetchAllAssociative();
    }
}