<?php

namespace App\Models\Repository;

use App\Models\Entities\Indicator;
use App\Models\Entities\User;

use Doctrine\ORM\EntityRepository;

class IndicatorRepository extends EntityRepository
{
    public function save(Indicator $entity):Indicator
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function check(User $user, int $status)
    {
        $status = $status == 1 ? '2, 14, 7, 8' : '0, 1, 3';
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT * FROM indicator i 
                LEFT JOIN tb_pessoa tp ON tp.id = i.user
                WHERE i.indicator = :user AND tp.filiado IN ($status) AND created_at BETWEEN '2023-12-12 00:00:01' AND '2023-12-31 23:59:59'
                GROUP BY i.user";
        $rows = $pdo->prepare($sql)->execute([':user' => $user->getId()]);
        return $rows->fetchAllAssociative();
    }
}