<?php

namespace App\Models\Repository;

use App\Models\Entities\UndersignedCounter;
use Doctrine\ORM\EntityRepository;

class UndersignedCounterRepository extends EntityRepository
{
    public function save(UndersignedCounter $entity):UndersignedCounter
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}