<?php

namespace App\Models\Repository;

use App\Models\Entities\RedeIuguTokens;
use Doctrine\ORM\EntityRepository;

class RedeIuguTokensRepository extends EntityRepository
{
    public function save(RedeIuguTokens $entity): RedeIuguTokens
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}