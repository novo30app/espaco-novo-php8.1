<?php

namespace App\Models\Repository;

use App\Models\Entities\SelectiveProcess2022Module1;
use App\Models\Entities\User;
use Doctrine\ORM\EntityRepository;
use App\Models\Entities\SelectiveProcess2022Module1Question;

class SelectiveProcess2022Module1QuestionRepository extends EntityRepository
{
    public function save(SelectiveProcess2022Module1Question $entity):SelectiveProcess2022Module1Question
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function getQuizTypes(SelectiveProcess2022Module1 $selectiveProcess2022Module1):array
    {
        $query = $this->getEntityManager()
            ->createQuery("SELECT DISTINCT(q.type) FROM App\Models\Entities\SelectiveProcess2022Module1Question q
                            WHERE q.type NOT IN (
                              SELECT DISTINCT(qp.type) FROM App\Models\Entities\SelectiveProcess2022Module1Question qp
                                  JOIN App\Models\Entities\SelectiveProcess2022AnswerModule1 a
                              WHERE a.selectiveProcess2022Module1 = :selectiveProcess2022Module1 AND a.question = q.id
                            )")
            ->setParameters([':selectiveProcess2022Module1' => $selectiveProcess2022Module1->getId()])
            ->setMaxResults(1);
        $types = $query->getResult();
        return !empty($types) ? $this->findBy(['type' => $types[0][1], 'status' => 1]) : [];
    }
}