<?php

namespace App\Models\Repository;

use App\Models\Entities\CogmoSchedule;
use Doctrine\ORM\EntityRepository;

class CogmoScheduleRepository extends EntityRepository
{
    public function save(CogmoSchedule $entity): CogmoSchedule
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function getPendingSchedule(): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT * 
                FROM cogmo_schedule 
                WHERE status = 'scheduled'
                AND DATE(scheduled_time) = CURDATE()
                AND TIME(scheduled_time) <= CURTIME()";
        $rows = $pdo->prepare($sql)->execute();
        return $rows->fetchAllAssociative();

    }
}