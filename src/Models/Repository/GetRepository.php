<?php

namespace App\Models\Repository;

use App\Models\Entities\Get;
use Doctrine\ORM\EntityRepository;

class GetRepository extends EntityRepository
{
    public function save(Get $entity):Get
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}