<?php

namespace App\Models\Repository;

use App\Models\Entities\EventsCoupons;
use Doctrine\ORM\EntityRepository;

class EventsCouponsRepository extends EntityRepository
{
    public function save(EventsCoupons $entity):EventsCoupons
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($name = null, $event = null, $status = null, &$params): string
    {
        $where = '';
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND ec.name LIKE :name";
        }
        if ($event) {
            $params[':event'] = "$event";
            $where .= " AND ec.event = :event";
        }
        if ($status) {
            $params[':status'] = "$status";
            $where .= " AND ec.status LIKE :status";
        }
        return $where;
    }

    public function list($name = null, $event = null, $status = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($name, $event, $status, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT ec.id, ec.name, ec.code, ua.name AS user, DATE_FORMAT(ec.created, '%d/%m/%Y %i:%m:%s') AS created, ev.nome AS event, ev.id AS eventId, ec.status AS statusId,
                (CASE ec.status
                    WHEN 1 THEN 'Ativo'
                    WHEN 2 THEN 'Cancelado'
                    WHEN 3 THEN 'Desativado'
                    ELSE ''
                END) AS status
                FROM eventsCoupons ec
                LEFT JOIN usersAdmin ua ON ua.id = ec.user
                LEFT JOIN tb_eventos ev ON ev.id = ec.event
                WHERE 1 = 1 {$where} GROUP BY ec.id {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal($name = null, $event = null, $status = null): array
    {
        $params = [];
        $where = $this->generateWhere($name, $event, $status, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(ec.id)) AS total 
                FROM eventsCoupons ec
                WHERE 1 = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }
}