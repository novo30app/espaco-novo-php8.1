<?php

namespace App\Models\Repository;

use App\Models\Entities\EventsTransactions;
use Doctrine\ORM\EntityRepository;

class EventsTransactionsRepository extends EntityRepository
{
    public function save(EventsTransactions $entity):EventsTransactions
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}