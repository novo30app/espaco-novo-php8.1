<?php

namespace App\Models\Repository; 

use App\Models\Entities\CommunicationConsent;
use Doctrine\ORM\EntityRepository;

class CommunicationConsentRepository extends EntityRepository
{
    public function save(CommunicationConsent $entity): CommunicationConsent
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}