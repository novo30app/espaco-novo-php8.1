<?php

namespace App\Models\Repository;

use App\Models\Entities\PersonRede;
use Doctrine\ORM\EntityRepository;

class PersonRedeRepository extends EntityRepository
{
    public function save(PersonRede $entity): PersonRede
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}