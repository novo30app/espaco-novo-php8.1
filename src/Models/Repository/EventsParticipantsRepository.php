<?php

namespace App\Models\Repository;

use App\Helpers\Utils;
use App\Models\Entities\EventsParticipants;
use Doctrine\ORM\EntityRepository;

class EventsParticipantsRepository extends EntityRepository
{
    public function save(EventsParticipants $entity):EventsParticipants
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function tickets($id, $ticket = null)
    {
        $params = [];
        $where = '';
        if($id){
            $params[':id'] = $id;
            $where .= " AND tep.tb_pessoa_id = :id";
        }
        if($ticket){
            $params[':ticket'] = $ticket;
            $where .= " AND tep.id = :ticket";
        }
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT tep.id, te.id AS eventId, tep.tb_pessoa_id AS user, tep.nome, tep.cpf, IFNULL(tbep.descricao, '') AS description, REPLACE(REPLACE(REPLACE(FORMAT(tep.valor, 2), '.', '@'), ',', '.'), '@', ',') AS value, DATE_FORMAT(tep.data_criacao, '%d/%m/%Y %H:%i:%s') AS date, tep.status, tep.code, 
                    te.nome AS nome_evento, te.status AS status_evento, te.data_inicio AS begin, te.data_termino AS end, CONCAT(te.endereco, ' ', te.local, ' ', te.bairro, ' ', te.complemento) AS address, ci.cidade AS city, es.estado AS state
                FROM tb_eventos_participantes tep
                LEFT JOIN tb_eventos te ON te.id = tep.tb_eventos_id
                LEFT JOIN tb_eventos_precos tbep ON tbep.id = tep.tb_eventos_precos_id
                LEFT JOIN tb_cidade ci ON ci.id = te.cidade_id
                LEFT JOIN tb_estado es ON es.id = te.estado_id
                LEFT JOIN tb_eventos_transacoes tet ON tet.id = tep.tb_eventos_transacao_id
                WHERE 1 = 1 {$where}
                ORDER BY tep.data_criacao DESC";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative(); 
    }
}