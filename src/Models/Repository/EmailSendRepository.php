<?php

namespace App\Models\Repository;

use App\Helpers\Utils;
use App\Models\Entities\EmailSend;
use Doctrine\ORM\EntityRepository;

class EmailSendRepository extends EntityRepository
{
    public function save(EmailSend $entity): EmailSend
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}