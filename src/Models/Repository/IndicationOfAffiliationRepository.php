<?php

namespace App\Models\Repository;

use App\Models\Entities\IndicationOfAffiliation;
use Doctrine\ORM\EntityRepository;

class IndicationOfAffiliationRepository extends EntityRepository
{
    public function save(IndicationOfAffiliation $entity):IndicationOfAffiliation
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}