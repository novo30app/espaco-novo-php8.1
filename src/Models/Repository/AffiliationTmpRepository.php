<?php

namespace App\Models\Repository;

use App\Models\Entities\AffiliationTmp;
use Doctrine\ORM\EntityRepository;

class AffiliationTmpRepository extends EntityRepository
{
    public function save(AffiliationTmp $entity):AffiliationTmp
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}