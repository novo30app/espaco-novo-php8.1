<?php

namespace App\Models\Repository;

use App\Models\Entities\EventsCart;
use Doctrine\ORM\EntityRepository;

class EventsCartRepository extends EntityRepository
{
    public function save(EventsCart $entity):EventsCart
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}