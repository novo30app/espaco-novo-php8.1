<?php

namespace App\Models\Repository;

use App\Models\Entities\ResearchNovoUnaffiliated;
use Doctrine\ORM\EntityRepository;

class ResearchNovoUnaffiliatedRepository extends EntityRepository
{
    public function save(ResearchNovoUnaffiliated $entity): ResearchNovoUnaffiliated
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function getList(): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT
                    DATE_FORMAT(ru.created, '%d/%m/%Y %H:%i:%s') AS 'Registro',
                    tp.nome AS 'Nome',
                    tp.cpf AS 'CPF',
                    tp.email AS 'E-mail',
                    (CASE tp.genero
                        WHEN 1 THEN 'Homem'
                        WHEN 2 THEN 'Mulher'
                        WHEN 3 THEN 'Não Quero Informar'
                        ELSE ''
                    END) AS 'Sexo',
                    CONCAT(ru.age, ' Anos') AS 'Idade',
                    es.sigla AS 'Estado',
                    ci.cidade AS 'Cidade',
                    (CASE ru.profession
                        WHEN 1 THEN 'Trabalhador doméstico'
                        WHEN 2 THEN 'Militar do exército, da marinha, da aeronáutica, da polícia militar ou do corpo de bombeiros militar'
                        WHEN 3 THEN 'Do setor privado'
                        WHEN 4 THEN 'Do setor público - funcionário estatutário'
                        WHEN 5 THEN 'Do setor público - empregado não estatutário'
                        WHEN 6 THEN 'De empresas estatais'
                        WHEN 7 THEN 'Empregador (com pelo menos um empregado)'
                        WHEN 8 THEN 'Conta própria (sem empregados)'
                        WHEN 9 THEN 'Trabalhador não remunerado em ajuda a algum morador do domicílio ou parente'
                    END) AS 'Profissão',
                    (CASE tp.escolaridade
						WHEN 1 THEN 'Ensino Superior'
                        WHEN 2 THEN 'Ensino Superior Incompleto'
                        WHEN 3 THEN 'Ensino Médio'
                        WHEN 4 THEN 'Ensino Médio Incompleto'
                        WHEN 5 THEN 'Ensino Fundamental'
                        WHEN 6 THEN 'Ensino Fundamental Incompleto'
                        WHEN 7 THEN 'Não quero informar'
					END) AS escolaridade,
                    (CASE ru.income
                        WHEN 1 THEN '00 a 500,00'
                        WHEN 2 THEN '501,00 a 1.000,00'
                        WHEN 3 THEN '1.001,00 a 2.000,00'
                        WHEN 4 THEN '2.001,00 a 3.000,00'
                        WHEN 5 THEN '3.001,00 a 5.000,00'
                        WHEN 6 THEN '5.001,00 a 10.000,00'
                        WHEN 7 THEN '10.001,00 a 20.000,00'
                        WHEN 8 THEN '20.001,00 a 100.000'
                        WHEN 9 THEN '100.001 ou mais'
                    END) AS 'Faixa de renda familiar mensal bruta',
                    IF(ru.contact = 1, 'Sim', 'Não') AS 'Newsletter',
                    REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(ra.question1, 
                        ',', ' '),
                        1, ' Me identifiquei com os princípios e valores do Partido;'), 
                        2, ' Gostaria de ter mais proximidade e acesso ao Partido e/ou Políticos do Partido;'),
                        3, ' Fui convidado por uma pessoa próxima, e me filiei mesmo sem conhecer muito sobre o Partido;'),
                        4, ' Considero que contribuir na política é importante, e vi no NOVO a melhor opção entre os demais partidos;'),
                        5, ' Entendi que a Filiação e a minha contribuição financeira mensal contribuem para a manutenção do Partido e para a construção de um NOVO Brasil;'),
                        6, ' Outros;') AS '1. Quais foram os principais motivos que o levaram a se filiar ao Partido NOVO?',
                    ra.textAreaOthersQuestion1 AS 'Descrição',
                    REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CONCAT(' ', ra.question2), 
                        ',', ' '),
                        ' 1 ', ' Estado mais simples, leve e eficiente;'), 
                        ';2', '; Renovação política do País;'),
                        3, ' Um Brasil com mais oportunidades e conectado ao mundo;'),
                        4, ' Fim de privilégios e mordomias com o dinheiro público;'),
                        5, ' Maior atenção à Educação em nosso país;'),
                        6, ' Maior defesa do liberalismo;'),
                        7, ' Sistema político verdadeiramente representativo;'),
                        8, ' Fortalecimento do combate à corrupção e à impunidade;'),
                        9, ' Priorização da Saúde em nosso país;'),
                        10, ' Garantia dos direitos iguais a todos os cidadãos;'),
                        11, ' País mais sustentável para as próximas gerações;'),
                        12, ' Outros;') AS '2. Quando você se filiou, o que queria que acontecesse (quais os posicionamentos do Partido com os quais mais se identificou)?',
                    ra.textAreaOthersQuestion2 AS 'Descrição2',
                    REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(ra.question3, 
                        ',', ' '),
                        1, ' Deixei de me identificar com os princípios e valores do Partido;'), 
                        2, ' Senti que não tinha proximidade e acesso ao Partido e/ou Políticos do Partido;'),
                        3, ' Como me filiei sem conhecer muito sobre o Partido, depois eu acabei concluindo que não fazia sentido continuar sendo filiado;'),
                        4, ' Me frustrei, e percebi que existem outras opções de Partido melhores do que o NOVO;'),
                        5, ' Não vi mais sentido em continuar sendo filiado e contribuir financeiramente para a manutenção do Partido;'),
                        6, ' Discordância do posicionamento do Partido;'),
                        7, ' Conflito interno com integrantes ou discordância da gestão partidária;'),
                        8, ' Entender que o Partido Novo não tem viabilidade política ou chance de crescimento;'),
                        9, ' Outros;') AS '3. E o que levou você a se desfiliar do Partido NOVO?',
                    ra.textAreaOthersQuestion3 AS 'Descrição3',
                    REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(ra.question4, 
                        ',', ' '),
                        1, ' Mudança de práticas de gestão;'), 
                        2, ' Mudança do posicionamento político do partido (pragmatismo saudável);'),
                        3, ' Ver possibilidades de crescimento do Partido;'),
                        4, ' Partido encampar pautas conservadoras;'),
                        5, ' Partido encampar pautas progressistas;'),
                        6, ' Ter maior participação e poder de decisão;'),
                        7, ' Outros;') AS '4. O que faria você retornar ao Partido NOVO (refiliar-se)?',
                    ra.textAreaOthersQuestion4 AS 'Descrição4',
                    ra.question5 AS '5. Você gostaria de deixar outras sugestões ou detalhar suas opiniões? Utilize o campo abaixo:',
                    IF(ra.question6 = 1, 'Sim', 'Não') AS '6. Você gostaria de receber acesso exclusivo a uma apresentação sobre o novo momento do Partido NOVO?'
                FROM researchNovoUnaffiliated ra
                LEFT JOIN researchNovoUser ru ON ra.researchNovoUser = ru.id
                LEFT JOIN tb_pessoa tp ON tp.id = ru.user
                LEFT JOIN tb_estado es ON tp.titulo_eleitoral_uf_id = es.id
                LEFT JOIN tb_cidade ci ON tp.titulo_eleitoral_municipio_id = ci.id
                GROUP BY ra.id
                ORDER BY ru.created DESC";
        $rows = $pdo->prepare($sql)->execute();
        return $rows->fetchAllAssociative();
    }
}