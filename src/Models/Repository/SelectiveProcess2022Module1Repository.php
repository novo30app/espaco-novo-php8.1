<?php

namespace App\Models\Repository;

use App\Models\Entities\SelectiveProcess2022Module1;
use Doctrine\ORM\EntityRepository;

class SelectiveProcess2022Module1Repository extends EntityRepository
{
    public function save(SelectiveProcess2022Module1 $entity):SelectiveProcess2022Module1
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}