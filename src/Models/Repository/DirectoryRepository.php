<?php

namespace App\Models\Repository;

use App\Models\Entities\Directory;
use Doctrine\ORM\EntityRepository;

class DirectoryRepository extends EntityRepository
{
    public function save(Directory $entity): Directory
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function getDirectoriesTransparencyPortal(): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT id, nome AS name 
                FROM tb_diretorio 
                WHERE nome NOT LIKE '%Núcleo%' AND ativo = 'S'
                ORDER BY nome ASC";
        $rows = $pdo->prepare($sql)->execute();
        return $rows->fetchAllAssociative();
    }

    public function getAccount(Directory $directory): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT CONCAT(b.name, ' (', b.cod, ')') AS bank, CONCAT(agency, '-', agencyDigit) AS agency, CONCAT(account, '-', accountDigit) AS account, dir.cnpj
                FROM directoryAccounts da
                LEFT JOIN banks b ON b.id = da.bank
                LEFT JOIN tb_diretorio dir ON dir.id = da.directory
                WHERE da.nickname = 3 AND da.directory = :directory
                LIMIT 1";
        $rows = $pdo->prepare($sql)->execute(['directory' => $directory->getId()]);
        return $rows->fetchAssociative();
    }
}