<?php

namespace App\Models\Repository;

use App\Models\Entities\PersonCreditCard;
use Doctrine\ORM\EntityRepository;

class PersonCreditCardRepository extends EntityRepository
{
    public function save(PersonCreditCard $entity): PersonCreditCard
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function check($user)
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(*) AS total 
                FROM tb_pessoa_cartao_credito 
                WHERE tb_pessoa_id = :user 
                    AND DATE_FORMAT(data_criacao, '%d/%m/%Y') = DATE_FORMAT(CURDATE(), '%d/%m/%Y')";
        $rows = $pdo->prepare($sql)->execute([':user' => $user->getId()]);
        return $rows->fetchAssociative();
    }

    public function validNewCard($tokenCreditCard)
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT *
                FROM tb_pessoa_cartao_credito
                WHERE DATE(data_criacao) > :createdAt 
                AND token_cartao_credito = :tokenCreditCard";
        $rows = $pdo->prepare($sql)->execute([':createdAt' => '2025-02-19', ':tokenCreditCard' => $tokenCreditCard]);
        return $rows->fetchAssociative();
    }
}