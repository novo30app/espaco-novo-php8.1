<?php

namespace App\Models\Repository;

use App\Models\Entities\Events;
use Doctrine\ORM\EntityRepository;

class EventsRepository extends EntityRepository
{
    public function save(Events $entity):Events
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function endsEvent(): array
    {
        try {
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT * FROM tb_eventos WHERE status = 'ativo' AND data_termino < CURDATE()
                    UNION ALL
                    SELECT * FROM tb_eventos WHERE especie IN (3, 5) AND categoria = 4 AND tipo = 'pago' AND status = 'ativo' and data_inicio <= CURDATE() + INTERVAL 7 DAY AND path_informative IS NULL";
            $rows = $pdo->prepare($sql)->execute();
            return $rows->fetchAllAssociative(); 
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function eventsList($data): array
    {
        try {
            $params = [];
            $where = '';
            if($data['uf']){
                $params[':uf'] = $data['uf'];
                $where .= " AND e.estado_id = :uf";
            }
            if($data['city']){
                $params[':city'] = $data['city'];
                $where .= " AND e.cidade_id = :city";
            }
            if($data['specie']){
                $params[':specie'] = $data['specie'];
                $where .= " AND e.especie = :specie";
            }
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT e.id, e.nome AS event, DATE_FORMAT(e.data_inicio, '%d/%m/%Y %H:%i:%s') AS date, d.nome AS directory, e.especie
                    FROM tb_eventos e
                    LEFT JOIN tb_diretorio d ON d.id = e.diretorio_id 
                    WHERE e.status = 'ativo' AND e.exibicao = 1 {$where}
                    ORDER BY FIELD(e.especie, '5', '4', '3', '2', '1', '6', '7'), e.data_inicio ASC";
            $rows = $pdo->prepare($sql)->execute($params);
            return $rows->fetchAllAssociative(); 
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function eventsListUf($data): array
    {
        try {
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT es.id, es.estado AS uf
                    FROM tb_eventos
                    LEFT JOIN tb_estado es ON es.id = tb_eventos.estado_id
                    WHERE status = 'ativo' AND exibicao = 1
                    GROUP BY estado_id
                    ORDER BY estado_id ASC";
            $rows = $pdo->prepare($sql)->execute();
            return $rows->fetchAllAssociative(); 
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function eventsListCity($data): array
    {
        try {
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT ci.id, ci.cidade AS city
                    FROM tb_eventos e
                    LEFT JOIN tb_cidade ci ON ci.id = e.cidade_id
                    WHERE status = 'ativo' AND exibicao = 1 AND estado_id = :uf
                    GROUP BY cidade_id
                    ORDER BY cidade_id ASC";
            $rows = $pdo->prepare($sql)->execute([':uf' => $data['uf']]);
            return $rows->fetchAllAssociative(); 
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function eventsListSpecie($data): array
    {
        try {
            $params = [];
            $where = '';
            if($data['uf']){
                $params[':uf'] = $data['uf'];
                $where .= " AND e.estado_id = :uf";
            }
            if($data['city']){
                $params[':city'] = $data['city'];
                $where .= " AND e.cidade_id = :city";
            }
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT especie AS id,
                        (CASE especie
                            WHEN 1 THEN 'Loja'
                            WHEN 2 THEN 'Mobilização'
                            WHEN 3 THEN 'Online'
                            WHEN 4 THEN 'Política da Mulher'
                            WHEN 5 THEN 'Tradicional'
                            WHEN 6 THEN 'Vaquinha'
                            WHEN 7 THEN 'Doação'
                            END
                        )AS specie
                    FROM tb_eventos e
                    WHERE e.status = 'ativo' AND e.exibicao = 1 {$where}
                    GROUP BY e.especie
                    ORDER BY e.especie ASC";
            $rows = $pdo->prepare($sql)->execute($params);
            return $rows->fetchAllAssociative(); 
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }
}