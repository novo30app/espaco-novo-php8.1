<?php

namespace App\Models\Repository;

use App\Models\Entities\DirectoryRegister;
use Doctrine\ORM\EntityRepository;

class DirectoryRegisterRepository extends EntityRepository
{
    public function getCities(): array
    {
        return $query = $this->getEntityManager()
            ->createQuery("SELECT d FROM App\Models\Entities\DirectoryRegister d
                                  JOIN App\Models\Entities\City c
                              WHERE d.level = 1 AND c.id = d.city
                              ORDER BY c.cidade ASC")
            ->getResult();
    }

    public function getStates(): array
    {
        return $query = $this->getEntityManager()
            ->createQuery("SELECT d FROM App\Models\Entities\DirectoryRegister d
                                  JOIN App\Models\Entities\State s
                              WHERE d.level = 2 AND s.id = d.state
                              ORDER BY s.estado ASC")
            ->getResult();
    }
}