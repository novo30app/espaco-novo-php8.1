<?php

namespace App\Models\Repository;

use App\Models\Entities\PersonIugu;
use App\Models\Entities\User;
use Doctrine\ORM\EntityRepository;

class PersonIuguRepository extends EntityRepository
{
    public function save(PersonIugu $entity): PersonIugu
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
    
    public function getCustomerId(User $user)
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT customer_id
                FROM tb_pessoa_iugu 
                WHERE tb_pessoa_id = :userId AND DATE(data_criacao) >= :date
                ORDER BY id DESC
                LIMIT 1";
        $rows = $pdo->prepare($sql)->execute([":userId" => $user->getId(), ":date" => '2025-02-25']);
        return $rows->fetchAssociative();
    }
}