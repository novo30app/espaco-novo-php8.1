<?php

namespace App\Models\Repository;

use App\Models\Entities\Address;
use Doctrine\ORM\EntityRepository;

class AddressRepository extends EntityRepository
{
    public function save(Address $entity): Address
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}