<?php

namespace App\Models\Repository;

use App\Models\Entities\EventsPress;
use Doctrine\ORM\EntityRepository;

class EventsPressRepository extends EntityRepository
{
    public function save(EventsPress $entity):EventsPress
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}