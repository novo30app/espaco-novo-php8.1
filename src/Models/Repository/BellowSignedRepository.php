<?php

namespace App\Models\Repository;

use App\Models\Entities\BellowSigned;
use Doctrine\ORM\EntityRepository;

class BellowSignedRepository extends EntityRepository
{
    public function save(BellowSigned $entity): BellowSigned
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function listTotalByTheme(string $theme): int
    {
        $params = [];
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(id) AS total
                FROM bellowSigned
                WHERE theme = :theme";
        $rows = $pdo->prepare($sql)->execute([':theme' => $theme]);
        return $rows->fetchAssociative()['total'];
    }

    public function getProtagonists(): array
    {
        $params = [];
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT name, email, phone, city, state
                FROM site2022.bellowSigned
                WHERE theme = :theme AND news = 1";
        $rows = $pdo->prepare($sql)->execute([':theme' => 'lp-mulheres']);
        return $rows->fetchAllAssociative();
    }

    public function exportProtagonists(): array
    {
        $params = [];
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT name, email, phone, cellPhone, ci.cidade, es.sigla
                FROM bellowSigned b
                LEFT JOIN tb_estado es ON es.id = b.state
                LEFT JOIN tb_cidade ci ON ci.id = b.city
                WHERE theme = :theme AND news = 1";
        $rows = $pdo->prepare($sql)->execute([':theme' => 'lp-mulheres']);
        return $rows->fetchAllAssociative();
    }
}