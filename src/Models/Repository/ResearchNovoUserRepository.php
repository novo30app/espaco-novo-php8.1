<?php

namespace App\Models\Repository;

use App\Models\Entities\ResearchNovoUser;
use Doctrine\ORM\EntityRepository;

class ResearchNovoUserRepository extends EntityRepository
{
    public function save(ResearchNovoUser $entity): ResearchNovoUser
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function verifyAnswer($user): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT *
                FROM researchNovoUser
                WHERE user = :user AND created >= :created";
        $rows = $pdo->prepare($sql)->execute([':user' => $user, 'created' => '2024-06-15']);
        return $rows->fetchAllAssociative();
    }
}