<?php

namespace App\Models\Repository;

use App\Helpers\Utils;
use App\Models\Entities\smsSend;
use Doctrine\ORM\EntityRepository;

class SmsSendRepository extends EntityRepository
{
    public function save(smsSend $entity): smsSend
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}