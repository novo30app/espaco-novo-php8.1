<?php

namespace App\Models\Repository;

use App\Models\Entities\WhatsAppGroups;
use Doctrine\ORM\EntityRepository;

class WhatsAppGroupsRepository extends EntityRepository
{
    public function save(WhatsAppGroups $entity):WhatsAppGroups
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}