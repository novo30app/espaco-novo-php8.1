<?php

namespace App\Models\Repository;

use App\Models\Entities\CheckLeadsByPhase;
use Doctrine\ORM\EntityRepository;

class CheckLeadsByPhaseRepository extends EntityRepository
{
    public function save(CheckLeadsByPhase $entity): CheckLeadsByPhase
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}
