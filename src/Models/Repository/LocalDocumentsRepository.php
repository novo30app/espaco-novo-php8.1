<?php

namespace App\Models\Repository;

use App\Models\Entities\User;
use App\Models\Entities\LocalDocuments;
use Doctrine\ORM\EntityRepository;

class LocalDocumentsRepository extends EntityRepository
{
	public function save(LocalDocuments $entity): LocalDocuments
	{
		$this->getEntityManager()->persist($entity);
		$this->getEntityManager()->flush();
		return $entity;
	}

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere($user, $keyWord = null, &$params): string
    {
        $where = '';
        if ($keyWord) {
            $params[':keyWord'] = "%$keyWord%";
            $where .= " AND ld.title LIKE :keyWord";
        }
        $params[':state'] = $user->getTituloEleitoralUfId()->getId();
        $where .= " AND ld.state = :state";
        $params[':active'] = "1";
        $where .= " AND ld.active = :active";
        return $where;
    }

    public function list(User $user, $keyword, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($user, $keyword, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT ld.id, DATE_FORMAT(ld.created, '%d/%m/%Y') AS created, ld.title, ld.description, ld.file
                FROM localDocuments ld
                LEFT JOIN tb_estado es ON es.id = ld.state
                LEFT JOIN tb_cidade ci ON ci.id = ld.city
                WHERE 1 = 1 {$where} GROUP BY ld.id {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal(User $user, $keyword): array
    {
        $params = [];
        $where = $this->generateWhere($user, $keyword, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(ld.id)) AS total 
                FROM localDocuments ld
                WHERE 1 = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }
}