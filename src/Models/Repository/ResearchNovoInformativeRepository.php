<?php

namespace App\Models\Repository;

use App\Models\Entities\ResearchNovoInformative;
use Doctrine\ORM\EntityRepository;

class ResearchNovoInformativeRepository extends EntityRepository
{
    public function save(ResearchNovoInformative $entity): ResearchNovoInformative
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function getList(): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT
                    tp.id,
                    tp. filiado_id AS 'FiliadoID',
                    tp.nome,
                    tp.email,
                    ge.genero,
                    DATE_FORMAT(created, '%d/%m/%Y %H:%i:%s') AS 'Data Resposta',
                    REPLACE(REPLACE(REPLACE(REPLACE(question1, ',', ' '),
                        1, ' Futura candidatura ao Legislativo.'), 
                        2, ' Futura candidatura ao Executivo.'),
                        3, ' Discussões ideológicas.'
                    ) AS '1. Qual destas opções representa seu maior interesse ao seu filiar ao NOVO?',
                    REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(question2, ',', ' '),
                        1, ' RenovaBR.'), 
                        2, ' RAPS.'),
                        3, ' Livres.'),
                        4, ' Politize.'),
                        5, ' Outro.'
                    ) AS '2. Você já participou ou participa de algum movimento de capacitação política?',
                    IFNULL(textAreaOthersQuestion2, '') AS 'Descrição',
                    question3 AS '3. Você tem alguma habilidade gostaria de colocar à disposição do NOVO, em um trabalho de voluntariado?'
                FROM researchNovoInformative
                LEFT JOIN tb_pessoa tp ON tp.id = researchNovoInformative.user
                LEFT JOIN tb_genero ge ON ge.id = tp.genero";
        $rows = $pdo->prepare($sql)->execute();
        return $rows->fetchAllAssociative();
    }
}