<?php

namespace App\Models\Repository;

use App\Models\Entities\UserAdmin;
use App\Models\Entities\Campaigns;
use Doctrine\ORM\EntityRepository;

class CampaignsRepository extends EntityRepository
{
    public function save(Campaigns $entity):Campaigns
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    private function generateWhere(UserAdmin $user, $name = null, $status = null, $state = null, $city = null, &$params): string
    {
        $where = '';
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND c.name LIKE :name";
        }
        if ($status) {
            $params[':status'] = "$status";
            $where .= " AND c.status LIKE :status";
        }
        /*
        if ($user->getLevel() == \App\Models\Entities\UserAdmin::LEVEL_NATIONAL) {
            if ($state) {
                $params[':state'] = "$state";
                $where .= " AND c.state = :state";
            }
            if ($city) {
                $params[':city'] = "$city";
                $where .= " AND c.city = :city";
            }
        } elseif ($user->getLevel() == \App\Models\Entities\UserAdmin::LEVEL_STATE) {
            $params[':state'] = $user->getState()->getId();
            $where .= " AND c.state = :state";
            if ($city) {
                $params[':city'] = "$city";
                $where .= " AND c.city = :city";
            }
        } else {
            $params[':state'] = $user->getState()->getId();
            $where .= " AND c.state = :state";
            $params[':city'] = $user->getCity()->getId();
            $where .= " AND c.city = :city";
        }
        */
        return $where;
    }

    public function list(UserAdmin $user, $name = null, $status = null,  $state = null, $city = null, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($user, $name, $status, $state, $city, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT c.id, c.name, DATE_FORMAT(c.created, '%d/%m/%Y') AS created, CONCAT(DATE_FORMAT(c.dateBegin, '%d/%m/%Y'), ' até ', DATE_FORMAT(c.dateFinal, '%d/%m/%Y')) AS period,
                    u.name AS user, c.description, CONCAT(ci.cidade, '/', es.sigla) AS location, IF(c.status = 1, 'Ativa', 'Inativa') AS statusString, c.status, c.hash,
                    DATE_FORMAT(c.dateBegin, '%Y-%m-%d') AS dateBegin, DATE_FORMAT(c.dateFinal, '%Y-%m-%d') AS dateFinal
                FROM campaigns c
                LEFT JOIN usersAdmin u ON u.id = c.user
                LEFT JOIN tb_estado es ON es.id = c.state
                LEFT JOIN tb_cidade ci ON ci.id = c.city
                WHERE 1 = 1 {$where} 
                GROUP BY c.id ORDER BY c.created ASC {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTotal(UserAdmin $user, $name = null, $status = null,  $state = null, $city = null): array
    {
        $params = [];
        $where = $this->generateWhere($user, $name, $status, $state, $city, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(c.id) AS total                  
                FROM campaigns c
                LEFT JOIN usersAdmin u ON u.id = c.user
                LEFT JOIN tb_estado es ON es.id = c.state
                LEFT JOIN tb_cidade ci ON ci.id = c.city
                WHERE 1 = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }
}