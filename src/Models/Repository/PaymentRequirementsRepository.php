<?php

namespace App\Models\Repository;

use App\Models\Entities\PaymentRequirements;
use App\Models\Entities\UserAdmin;
use Doctrine\ORM\EntityRepository;

class PaymentRequirementsRepository extends EntityRepository
{
    public function save(PaymentRequirements $entity): PaymentRequirements
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    public function listTransparencyPortal(string $payDayBegin, string $payDayEnd, string $paymentValue, string $origin, string $description, string $directory, string $providerCnpj, string $providerName, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($params, $payDayBegin, $payDayEnd, $paymentValue, $origin, $description, $directory, $providerCnpj, $providerName);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT DATE_FORMAT(p.payDay, '%d/%m/%Y') AS payDay, 
                    CONCAT('R$ ',REPLACE(REPLACE(REPLACE(FORMAT(p.paymentValue, 2), '.', '@'), ',', '.'), '@', ',')) as paymentValue,
                    p.description, dir.nome AS directory, pr.cpfCnpj AS providerCpfCnpj, pr.name AS providerName, 
                    IFNULL(IFNULL(con.id, attachmentContract), '') AS contract,  
                    IFNULL(p.attachmentNf, '') AS attachmentNf, IFNULL(p.attachmentBillet, '') AS attachmentBillet,
                    (CASE p.origin
                        WHEN 1 THEN 'Campanha Eleitoral'
                        WHEN 2 THEN 'Política da Mulher'
                        WHEN 3 THEN 'Outros Recursos'
                        WHEN 4 THEN 'Fundo Partidário'
                        ELSE ''
                    END) AS origin
                FROM paymentRequirements p
                LEFT JOIN providers pr ON pr.id = p.provider
                LEFT JOIN tb_diretorio dir ON dir.id = p.directory
                LEFT JOIN contract con ON con.id = p.contract
                WHERE p.requestStatus = 3 AND p.payDay BETWEEN '2023-04-01' AND '2024-11-30' AND dir.nome NOT LIKE '%Núcleo%' AND dir.ativo = 'S'  {$where}
                ORDER BY p.payDay DESC {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listTransparencyPortalTotal(string $payDayBegin, string $payDayEnd, string $paymentValue, string $origin, string $description, string $directory, string $providerCnpj, string $providerName): array
    {
        $params = [];
        $where = $this->generateWhere($params, $payDayBegin, $payDayEnd, $paymentValue, $origin, $description, $directory, $providerCnpj, $providerName);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(p.id)) AS total 
                FROM paymentRequirements p
                LEFT JOIN providers pr ON pr.id = p.provider
                LEFT JOIN tb_diretorio dir ON dir.id = p.directory
                LEFT JOIN contract con ON con.id = p.contract
                WHERE p.requestStatus = 3 AND p.payDay BETWEEN '2023-04-01' AND '2024-11-30' AND dir.nome NOT LIKE '%Núcleo%' AND dir.ativo = 'S' {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAssociative();
    }

    private function generateWhere(&$params, $payDayBegin = null, $payDayEnd = null, $paymentValue = null, $origin = null, $description = null, $directory = null, $providerCnpj = null, $providerName = null): string
    {
        $where = '';
        if ($payDayBegin) {
            $params[':payDayBegin'] = $payDayBegin;
            $where .= " AND p.payDay >= :payDayBegin";
        }
        if ($payDayEnd) {
            $params[':payDayEnd'] = $payDayEnd;
            $where .= " AND p.payDay <= :payDayEnd";
        }
        if ($paymentValue) {
            $data['paymentValue'] = str_replace('.', '', $paymentValue);
            $data['paymentValue'] = str_replace(',', '.', $data['paymentValue']);
            $data['paymentValue'] = str_replace('R$', '', $data['paymentValue']);
            $data['paymentValue'] = (float)trim($data['paymentValue']);
            $params[':paymentValue'] = $data['paymentValue'];
            $where .= " AND p.paymentValue = :paymentValue";
        }
        if ($origin) {
            $params[':origin'] = $origin;
            $where .= " AND p.origin = :origin";
        }   
        if ($directory) {
            $params[':directory'] = $directory;
            $where .= " AND p.directory = :directory";
        }        
        if ($description) {
            $params[':description'] = "%$description%";
            $where .= " AND p.description LIKE :description";
        }
        if ($providerCnpj) {
            $providerCnpj = preg_replace("/[^0-9]/", "", $providerCnpj);
            $params[':cpfCnpj'] = $providerCnpj;
            $where .= " AND pr.cpfCnpj = :cpfCnpj";
        }
        if ($providerName) {
            $params[':name'] = "%$providerName%";
            $where .= " AND pr.name LIKE :name";
        }
        return $where;
    }

    public function getContractNumber(string $contract): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT number FROM contract WHERE id = :contract";
        $rows = $pdo->prepare($sql)->execute([':contract' => $contract]);
        return $rows->fetchAssociative();
    }

    public function getContracts(string $contract): array
    {
        $params = [];
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT c.number, c.name, c.cpf, co.name as costCenter, REPLACE(REPLACE(REPLACE(FORMAT(IFNULL(c.value, c.monthlyValue), 2), '.', '@'), ',', '.'), '@', ',') as value, 
                    DATE_FORMAT(c.start, '%d/%m/%Y') AS start, DATE_FORMAT(c.end, '%d/%m/%Y') AS end, CONCAT('https://', REPLACE(c.doc, '/var/www/html/','')) AS doc,
                (CASE c.stage
                    WHEN 1 THEN 'Criado'
                    WHEN 2 THEN 'Colhendo Assinatura'
                    WHEN 3 THEN 'Assinado'
                    ELSE 'Desconhecido'
                END) AS status, 
                (CASE c.situation                
                    WHEN 1 THEN 'Vigente - Prazo Determinado'
                    WHEN 2 THEN 'Vigente - Prazo Indeterminado'
                    WHEN 3 THEN 'Expirado'
                    ELSE 'Desconhecido'
                END) AS situation
                FROM contract c
                LEFT JOIN costCenter co ON co.id = c.costCenter
                WHERE c.contract = $contract || c.id = $contract";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }
}