<?php

namespace App\Models\Repository;

use App\Models\Entities\Historic;
use Doctrine\ORM\EntityRepository;

class HistoricRepository extends EntityRepository
{
    public function save(Historic $entity): Historic
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}