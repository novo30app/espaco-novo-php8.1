<?php

namespace App\Models\Repository;

use App\Models\Entities\Candidade;
use Doctrine\ORM\EntityRepository;

class CandidateRepository extends EntityRepository
{
    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    public function agents($cabinets = null)
    {
        try {
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT * FROM dam.cabinets WHERE active = 1 ORDER BY name ASC";
            $rows = $pdo->prepare($sql)->execute();
            return $rows->fetchAllAssociative();        
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function states()
    {
        try {
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT distinct(state) FROM dam.cabinets WHERE active = 1 ORDER BY state ASC ";
            $rows = $pdo->prepare($sql)->execute();
            return $rows->fetchAllAssociative();        
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function cities()
    {
        try {
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT distinct(city) FROM dam.cabinets WHERE city IS NOT NULL AND active = 1 ORDER BY city ASC";
            $rows = $pdo->prepare($sql)->execute();
            return $rows->fetchAllAssociative();        
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function generateWhereSpendings($agent = null, $state = null, $city = null, $year = null, &$params): string
    {
        $where = '';
        if ($agent) {
            $params[':agent'] = "$agent";
            $where .= " AND cabinet = :agent";
        }
        if ($state) {
            $params[':state'] = "$state";
            $where .= " AND c.state = :state";
        }
        if ($city) {
            $params[':city'] = "$city";
            $where .= " AND c.city = :city";
        }
        if ($year) {
            $params[':year'] = "$year";
            $where .= " AND YEAR(dueDate) = :year";
        }
        return $where;
    }

    public function valueSaved($agent = null, $state = null, $city = null, $year = null): array   
    {
        try {
            $params = [];
            $where = $this->generateWhereSpendings($agent, $state, $city, $year, $params);
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql =  "SELECT IFNULL(REPLACE(REPLACE(REPLACE(FORMAT(SUM(valueSave), 2), '.', '@'), ',', '.'), '@', ','), '0,00') as valueSaved 
                     FROM dam.spendings
                     LEFT JOIN dam.cabinets c ON c.id = spendings.cabinet
                     WHERE c.active = 1 {$where}";
            $rows = $pdo->prepare($sql)->execute($params);
            return $rows->fetchAllAssociative();    
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function spendings($agent = null, $state = null, $city = null, $year = null): array
    {
        try {
            $params = [];
            $where = $this->generateWhereSpendings($agent, $state, $city, $year, $params);
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $months = array(1,2,3,4,5,6,7,8,9,10,11,12);
            $sql = null;
            foreach($months as $month) {
                $sql .= "SELECT 
                            (CASE $month
                                WHEN 1 THEN 'Janeiro'
                                WHEN 2 THEN 'Fevereiro'
                                WHEN 3 THEN 'Março'
                                WHEN 4 THEN 'Abril'
                                WHEN 5 THEN 'Maio'
                                WHEN 6 THEN 'Junho'
                                WHEN 7 THEN 'Julho'
                                WHEN 8 THEN 'Agosto'
                                WHEN 9 THEN 'Setembro'
                                WHEN 10 THEN 'Outubro'
                                WHEN 11 THEN 'Novembro'
                                WHEN 12 THEN 'Dezembro'
                            END) month,
                            IFNULL((SELECT SUM(valueSave) FROM dam.spendings LEFT JOIN dam.cabinets c ON c.id = spendings.cabinet WHERE c.active = 1 AND month(dueDate) = $month AND category = 1 {$where}), 0) as '1',
                            IFNULL((SELECT SUM(valueSave) FROM dam.spendings LEFT JOIN dam.cabinets c ON c.id = spendings.cabinet WHERE c.active = 1 AND month(dueDate) = $month AND category = 2 {$where}), 0) as '2',
                            IFNULL((SELECT SUM(valueSave) FROM dam.spendings LEFT JOIN dam.cabinets c ON c.id = spendings.cabinet WHERE c.active = 1 AND month(dueDate) = $month AND category = 3 {$where}), 0) as '3',
                            IFNULL((SELECT SUM(valueSave) FROM dam.spendings LEFT JOIN dam.cabinets c ON c.id = spendings.cabinet WHERE c.active = 1 AND month(dueDate) = $month AND category = 4 {$where}), 0) as '4',
                            IFNULL((SELECT SUM(valueSave) FROM dam.spendings LEFT JOIN dam.cabinets c ON c.id = spendings.cabinet WHERE c.active = 1 AND month(dueDate) = $month AND category = 5 {$where}), 0) as '5',
                            IFNULL((SELECT SUM(valueSave) FROM dam.spendings LEFT JOIN dam.cabinets c ON c.id = spendings.cabinet WHERE c.active = 1 AND month(dueDate) = $month AND category = 6 {$where}), 0) as '6'
                        FROM dam.spendings 
                        LEFT JOIN dam.cabinets c ON c.id = spendings.cabinet
                        WHERE c.active = 1 AND month(dueDate) = $month {$where} group by month(dueDate)";
                $sql .= $month != 12 ? 'UNION ALL ' : '';
            }
            $rows = $pdo->prepare($sql)->execute($params);
            return $rows->fetchAllAssociative();        
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function listSpendings($agent = null, $state = null, $city = null, $year = null, $limit = null, $offset = null)
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhereSpendings($agent, $state, $city, $year, $params);    
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT spendings.id, spendings.duedate, REPLACE(REPLACE(REPLACE(FORMAT(spendings.value, 2), '.', '@'), ',', '.'), '@', ',') AS 'value', IFNULL(REPLACE(REPLACE(REPLACE(FORMAT(spendings.valueSave, 2), '.', '@'), ',', '.'), '@', ','), '-') AS valueSave, IFNULL(spendings.justify, '-') AS justify, 
                    IFNULL(spendingCategories.name, '-') AS category, c.name AS 'cabinet', DATE_FORMAT(spendings.dueDate, '%d/%m/%Y') as dueDate, spendingTypes.name AS type 
                FROM dam.spendings
                LEFT JOIN dam.spendingTypes ON spendingTypes.id=spendings.type                
                LEFT JOIN dam.cabinets c ON c.id=spendings.cabinet       
                LEFT JOIN dam.spendingCategories ON spendingCategories.ID=spendings.category
                WHERE c.active = 1 {$where}
                GROUP BY spendings.id
                ORDER BY spendings.duedate DESC {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();    
    }

    public function listSpendingsTotal($agent = null, $state = null, $city = null, $year = null)
    {
        $params = [];
        $where = $this->generateWhereSpendings($agent, $state, $city, $year, $params);    
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(spendings.id)) AS total
                FROM dam.spendings
                LEFT JOIN dam.spendingTypes ON spendingTypes.id=spendings.type                
                LEFT JOIN dam.cabinets c ON c.id=spendings.cabinet       
                LEFT JOIN dam.spendingCategories ON spendingCategories.ID=spendings.category
                WHERE c.active = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();    
    }

    public function presences($agent = null): array
    {
        try {
            $params = [];
            $where = '';
            if ($agent) {
                $params[':agent'] = "$agent";
                $where .= " AND cabinet = :agent";
            }
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT 
                        type, 
                        (SELECT count(*) FROM dam.presences WHERE presence = 1 AND type = 'Atos Deliberativos' {$where}) as 'presences',
                        (SELECT count(*) FROM dam.presences WHERE presence = 0 AND type = 'Atos Deliberativos' {$where}) as 'absences'
                        FROM dam.presences s
                        WHERE type = 'Atos Deliberativos' {$where}
                        GROUP BY type
                    UNION ALL
                    SELECT 
                        type,
                        (SELECT count(*) FROM dam.presences WHERE presence = 1 AND type = 'Comissões' {$where}) as 'presences',
                        (SELECT count(*) FROM dam.presences WHERE presence = 0 AND type = 'Comissões'{$where}) as 'absences'
                        FROM dam.presences s
                        WHERE type = 'Comissões' {$where}
                        GROUP BY type
                    UNION ALL
                    SELECT 
                        type,
                        (SELECT count(*) FROM dam.presences WHERE presence = 1 AND type = 'Plenário' {$where}) as 'presences',
                        (SELECT count(*) FROM dam.presences WHERE presence = 0 AND type = 'Plenário' {$where}) as 'absences'
                        FROM dam.presences s
                        WHERE type = 'Plenário' {$where}
                        GROUP BY type";
            $rows = $pdo->prepare($sql)->execute($params);
            return $rows->fetchAllAssociative();        
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function getPresencesLink($agent = null): array
    {
        try {
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT IFNULL(linkPresence, 0) AS linkPresence FROM dam.cabinets WHERE active = 1 AND id = $agent";
            $rows = $pdo->prepare($sql)->execute();
            return $rows->fetchAllAssociative();        
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function polls($agent = null): array
    {
        try {
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT IFNULL(p.date, '-') AS 'date', c.name AS 'cabinet', p.projeto AS 'project', p.analise AS 'analysis', 
                            (CASE p.vote
                                WHEN 0 THEN 'Contrário'
                                WHEN 1 THEN 'Favorável'
                                WHEN 2 THEN 'Abstenção'
                            END) AS 'vote' 
                    FROM dam.polls p
                    LEFT JOIN dam.cabinets c ON c.id = p.cabinet
                    WHERE c.active = 1 AND p.cabinet IS NOT NULL";
            $rows = $pdo->prepare($sql)->execute();
            return $rows->fetchAllAssociative();        
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function generateWherePolls($agent = null, $vote = null, &$params): string
    {
        $where = '';
        if ($agent) {
            $params[':agent'] = "$agent";
            $where .= " AND cabinet = :agent";
        }
        if ($vote != '') {
            $params[':vote'] = $vote;
            $where .= " AND vote = :vote";
        }
        return $where;
    }

    public function pollsGraphic($agent = null, $vote = null): array
    {
        try {
            $params = [];
            $where = $this->generateWherePolls($agent, $vote, $params);
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT 'Contrário' AS 'type', COUNT(DISTINCT(polls.id)) AS 'votes' FROM dam.polls WHERE cabinet IS NOT NULL AND vote = 0 {$where}
                    UNION ALL
                    SELECT 'Favorável' AS 'type', COUNT(DISTINCT(polls.id)) AS 'votes' FROM dam.polls WHERE cabinet IS NOT NULL AND vote = 1 {$where}
                    UNION ALL
                    SELECT 'Abstenção' AS 'type', COUNT(DISTINCT(polls.id)) AS 'votes' FROM dam.polls WHERE cabinet IS NOT NULL AND vote = 2 {$where}";
            $rows = $pdo->prepare($sql)->execute($params);
            return $rows->fetchAllAssociative();        
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function listPolls($agent = null, $vote = null, $limit = null, $offset = null): array
    {
        try {
            $params = [];
            $where = $this->generateWherePolls($agent, $vote, $params);
            $limitSql = $this->generateLimit($limit, $offset);
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT IFNULL(DATE_FORMAT(polls.date, '%d/%m/%Y'), '-') AS date, cabinets.name AS cabinet, polls.projeto, polls.analise, polls.ementa, polls.link,
                        (CASE polls.vote 
                            WHEN 0 THEN 'Contrário'
                            WHEN 1 THEN 'Favorável'
                            WHEN 2 THEN 'Abstenção'
                        END) AS 'vote'
                    FROM dam.polls 
                    LEFT JOIN dam.cabinets ON cabinets.id = polls.cabinet
                    WHERE cabinets.active = 1 AND cabinet IS NOT NULL {$where} ORDER BY polls.date DESC {$limitSql}";
            $rows = $pdo->prepare($sql)->execute($params);
            return $rows->fetchAllAssociative();        
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function listPollsTotal($agent = null, $vote = null): array
    {
        $params = [];
        $where = $this->generateWherePolls($agent, $vote, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(polls.id)) AS 'total' 
                FROM dam.polls 
                LEFT JOIN dam.cabinets ON cabinets.id = polls.cabinet
                WHERE cabinets.active = 1 AND cabinet IS NOT NULL {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();    
    }

    public function generateWhereReports($agent = null, &$params): string
    {
        $where = '';
        if ($agent) {
            $params[':agent'] = "$agent";
            $where .= " AND r.cabinet = :agent";
        }
        return $where;
    }

    public function listReports($agent = null, $limit = null, $offset = null): array
    {
        try {
            $params = [];
            $where = $this->generateWhereReports($agent, $params);
            $limitSql = $this->generateLimit($limit, $offset);
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT r.id, c.name AS cabinet, r.title, r.link
                    FROM dam.reports r
                    LEFT JOIN dam.cabinets c ON c.id = r.cabinet 
                    WHERE c.active = 1 {$where} GROUP BY r.id ORDER BY r.created DESC {$limitSql}";
            $rows = $pdo->prepare($sql)->execute($params);
            return $rows->fetchAllAssociative();        
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function listReportsTotal($agent = null): array
    {
        $params = [];
        $where = $this->generateWhereReports($agent, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT COUNT(DISTINCT(r.id)) AS 'total' 
                FROM dam.reports r
                LEFT JOIN dam.cabinets c ON c.id = r.cabinet 
                WHERE c.active = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();    
    }

    public function generateWhereProject($name = null, $agent = null, $coverage = null, $nickname = null, $ementa = null, $date = null, $tag = null, $author = null, &$params): string
    {
        $where = '';
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND p.name LIKE :name";
        }
        if ($agent) {
            $params[':agent'] = "$agent";
            $where .= " AND p.cabinet = :agent";
        }
        if ($coverage) {
            $params[':coverage'] = "$coverage";
            $where .= " AND p.coverage = :coverage";
        }
        if ($nickname) {
            $params[':nickname'] = "%$nickname%";
            $where .= " AND p.nickname LIKE :nickname";
        }
        if ($ementa) {
            $params[':ementa'] = "%$ementa%";
            $where .= " AND p.ementa LIKE :ementa";
        }
        if ($date) {
            $params[':date'] = "$date";
            $where .= " AND p.date = :date";
        }
        if ($tag) {
            $params[':tag'] = "$tag";
            $where .= " AND pt.tag LIKE :tag";
        }
        if ($author) {
            $params[':author'] = "%$author%";
            $where .= " AND pa.author LIKE :author";
        }
        return $where;
    }

    public function listProjects($name = null, $agent = null, $coverage = null, $nickname = null, $ementa = null, $date = null, $tag = null, $author = null, $limit = null, $offset = null): array
    {
        try {
            $params = [];
            $where = $this->generateWhereProject($name, $agent, $coverage, $nickname, $ementa, $date, $tag, $author, $params);
            $limitSql = $this->generateLimit($limit, $offset);
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT p.id, p.name, c.name AS agent, p.coverage, p.nickname, p.ementa, DATE_FORMAT(p.date, '%d/%m/%Y') as date  
                    FROM dam.projects p
                    LEFT JOIN dam.cabinets c ON c.id = p.cabinet
                    LEFT JOIN dam.projectTags pt ON pt.project = p.id
                    LEFT JOIN dam.projectAuthors pa ON pa.project = p.id
                    WHERE c.active = 1 {$where}
                    GROUP BY p.id
                    ORDER BY p.date DESC {$limitSql}";
            $rows = $pdo->prepare($sql)->execute($params);
            return $rows->fetchAllAssociative();        
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function listProjectsTotal($name = null, $agent = null, $coverage = null, $nickname = null, $ementa = null, $date = null, $tag = null, $author = null): array
    {
        $params = [];
        $where = $this->generateWhereProject($name, $agent, $coverage, $nickname, $ementa, $date, $tag, $author, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT DISTINCT(COUNT(p.id)) AS total  
                FROM dam.projects p
                LEFT JOIN dam.cabinets c ON c.id = p.cabinet
                LEFT JOIN dam.projectTags pt ON pt.project = p.id
                LEFT JOIN dam.projectAuthors pa ON pa.project = p.id
                WHERE c.active = 1 {$where}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();    
    }

    public function listProject(): array
    {
        $params = [];
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT * FROM dam.projects";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();    
    }

    public function listTags(): array
    {
        $params = [];
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT t.id, t2.name, t2.id as tagId, t.project
                FROM dam.projectTags t
                LEFT JOIN dam.tags t2 ON t2.id = t.tag";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();    
    }

    public function listAuthors(): array
    {
        $params = [];
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT * FROM dam.projectAuthors";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();    
    }

    public function getTags(): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT * FROM dam.tags";
        $rows = $pdo->prepare($sql)->execute();
        return $rows->fetchAllAssociative();    
    }

    public function citiesByAgents($state = null)
    {
        try {
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT distinct(city) AS city FROM dam.cabinets WHERE city IS NOT NULL AND active = 1 AND state = :state ORDER BY city ASC";
            $rows = $pdo->prepare($sql)->execute([':state' => $state]);
            return $rows->fetchAllAssociative();        
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function agentsByState($state = null)
    {
        try {
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT id, name 
                    FROM dam.cabinets 
                    WHERE state IS NOT NULL AND active = 1 AND state = :state 
                    ORDER BY name ASC";
            $rows = $pdo->prepare($sql)->execute([':state' => $state]);
            return $rows->fetchAllAssociative();                    
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function agentsByCity($city = null)
    {
        try {
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT id, name 
                    FROM dam.cabinets 
                    WHERE state IS NOT NULL AND active = 1 AND city = :city 
                    ORDER BY name ASC";
            $rows = $pdo->prepare($sql)->execute([':city' => $city]);
            return $rows->fetchAllAssociative();        
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }
}