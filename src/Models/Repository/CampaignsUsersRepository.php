<?php

namespace App\Models\Repository;

use App\Models\Entities\CampaignsUsers;
use Doctrine\ORM\EntityRepository;

class CampaignsUsersRepository extends EntityRepository
{
    public function save(CampaignsUsers $entity):CampaignsUsers
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}