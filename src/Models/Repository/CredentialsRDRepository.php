<?php

namespace App\Models\Repository;

use App\Models\Entities\CredentialsRD;
use Doctrine\ORM\EntityRepository;

class CredentialsRDRepository extends EntityRepository
{
    public function save(CredentialsRD $entity):CredentialsRD
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function findCredential(): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT * FROM credentialsRD ORDER BY id DESC LIMIT 1";
        $rows = $pdo->prepare($sql)->execute();
        return $rows->fetchAssociative();        
    }
}