<?php

namespace App\Models\Repository;

use App\Helpers\Utils;
use App\Models\Entities\UserAdmin;
use Doctrine\ORM\EntityRepository;

class UserAdminRepository extends EntityRepository
{
    public function save(UserAdmin $entity): UserAdmin
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}