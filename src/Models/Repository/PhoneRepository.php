<?php

namespace App\Models\Repository;

use App\Models\Entities\Phone;
use Doctrine\ORM\EntityRepository;

class PhoneRepository extends EntityRepository
{
    public function save(Phone $entity): Phone
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function getPhoneByUser($user)
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(tbtel.telefone, '-', ''),'(',''),')',''),' ',''),'+','')
                FROM tb_telefone tbtel  
                WHERE tbtel.tb_pessoa_id = :user AND tbtel.tb_tipo_telefone_id = 3 LIMIT 1";
        $rows = $pdo->prepare($sql)->execute([':user' => $user->getId()]);
        return $rows->fetchAllAssociative();
    }

    public function getPhoneByEmail(string $email)
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(tbtel.telefone, '-', ''),'(',''),')',''),' ',''),'+','') AS phone
                FROM tb_telefone tbtel
                LEFT JOIN tb_pessoa tp ON tp.id = tbtel.tb_pessoa_id
                WHERE tp.email = :email AND tbtel.tb_tipo_telefone_id = 1 LIMIT 1";
        $rows = $pdo->prepare($sql)->execute([':email' => $email]);
        return $rows->fetchAssociative();
    }
}