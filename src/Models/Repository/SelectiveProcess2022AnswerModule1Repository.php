<?php

namespace App\Models\Repository;

use Doctrine\ORM\EntityRepository;
use App\Models\Entities\SelectiveProcess2022AnswerModule1;

class SelectiveProcess2022AnswerModule1Repository extends EntityRepository
{
    public function save(SelectiveProcess2022AnswerModule1 $entity):SelectiveProcess2022AnswerModule1
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}