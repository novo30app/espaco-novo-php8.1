<?php

namespace App\Models\Repository;

use App\Models\Entities\IndicatorFiles;
use Doctrine\ORM\EntityRepository;

class IndicatorFilesRepository extends EntityRepository
{
    public function save(IndicatorFiles $entity):IndicatorFiles
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}