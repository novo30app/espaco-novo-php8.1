<?php

namespace App\Models\Repository;

use App\Models\Entities\Directory;
use App\Models\Entities\Transaction;
use App\Models\Entities\User;
use App\Models\Entities\HowdidKnow;
use Doctrine\ORM\EntityRepository;

class HowdidKnowRepository extends EntityRepository
{
    public function save(HowdidKnow $entity):HowdidKnow
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}