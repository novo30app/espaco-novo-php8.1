<?php

namespace App\Models\Repository;

use App\Models\Entities\SelectiveProcess2022Module1Video;
use Doctrine\ORM\EntityRepository;

class SelectiveProcess2022Module1VideoRepository extends EntityRepository
{
    public function save(SelectiveProcess2022Module1Video $entity):SelectiveProcess2022Module1Video
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}