<?php

namespace App\Models\Repository;

use App\Models\Entities\HistoricalIncrease;
use Doctrine\ORM\EntityRepository;

class HistoricalIncreaseRepository extends EntityRepository
{
    public function save(HistoricalIncrease $entity): HistoricalIncrease
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}