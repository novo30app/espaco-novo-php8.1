<?php

namespace App\Models\Repository;

use App\Models\Entities\Ombudsman;
use Doctrine\ORM\EntityRepository;

class OmbudsmanRepository extends EntityRepository
{
    public function save(Ombudsman $entity):Ombudsman
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}