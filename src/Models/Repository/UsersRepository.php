<?php

namespace App\Models\Repository;

use App\Helpers\Utils;
use App\Models\Entities\User;
use Doctrine\ORM\EntityRepository;

class UsersRepository extends EntityRepository
{
    public function save(User $entity): User
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    private function generateLimit($limit = null, $offset = null): string
    {
        $limitSql = '';
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        return $limitSql;
    }

    public function delete(User $entity): User
    {
        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function login(string $emailCpf, string $password): User
    {
        $user = $this->getEntityManager()
            ->createQuery("SELECT u FROM App\Models\Entities\User AS u                                  
                              WHERE u.senha = :password AND (u.email = :email OR u.cpf = :cpf OR u.cpf = :cpf1)")
            ->setParameters([':password' => md5($password), ':email' => $emailCpf, ':cpf' => Utils::formatCpf($emailCpf), ':cpf1' => $emailCpf])
            ->getResult();
        if (!$user) {
            throw new \Exception('Usuário ou senha inválidos');
        }
        return $user[0];
    }

    public function loginAuth(string $emailCpf): User
    {
        $user = $this->getEntityManager()
            ->createQuery("SELECT u FROM App\Models\Entities\User AS u                                  
                              WHERE u.email = :email OR u.cpf = :cpf OR u.cpf = :cpf1")
            ->setParameters([':email' => $emailCpf, ':cpf' => Utils::formatCpf($emailCpf), ':cpf1' => $emailCpf])
            ->getResult();
        if (!$user) {
            throw new \Exception('Usuário ou senha inválidos');
        }
        return $user[0];
    }


    public function getByEmailOrCpf(string $emailCpf): User
    {
        $user = $this->getEntityManager()
            ->createQuery("SELECT u FROM App\Models\Entities\User AS u                                  
                              WHERE (u.email = :email OR u.cpf = :cpf)")
            ->setParameters([':email' => $emailCpf, ':cpf' => Utils::formatCpf($emailCpf)])
            ->getResult();
        if (!$user) {
            throw new \Exception('Usuário não encontrado');
        }
        return $user[0];
    }

    public function getListToImpeachment(User $user, $city, $uf, $name, $limit = null, $offset = null): array
    {
        $params = [];
        $params[':logged'] = $user->getId();
        $where = $limitSql = '';
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND u.nome LIKE :name";
        }
        if ($uf) {
            $params[':uf'] = $uf;
            $where .= " AND u.titulo_eleitoral_uf_id = :uf";
        }
        if ($city) {
            $params[':city'] = $city;
            $where .= " AND u.titulo_eleitoral_municipio_id = :city";
        }
        if ($limit) {
            $limit = (int)$limit;
            $offset = (int)$offset;
            $limitSql = " LIMIT {$limit} OFFSET {$offset}";
        }
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT u.id, u.nome as name, if(state.sigla is null, 'Exterior', state.sigla) as voterTitleUF, if(city.cidade is null, 'Exterior', city.cidade) as voterTitleCity, 
                    u.titulo_eleitoral_municipio as voterTitleCityString , u.titulo_eleitoral as voterTitleNumber
                FROM tb_pessoa AS u
                LEFT JOIN  tb_estado as state ON state.id = u.titulo_eleitoral_uf_id 
                LEFT JOIN  tb_cidade as city ON city.id = u.titulo_eleitoral_municipio_id 
                WHERE u.filiado in (2, 14) {$where}                 
                AND (SELECT COUNT(impeachment.id) FROM impeachment  WHERE impeachment.user = :logged AND impeachment.target = u.id) = 0
                GROUP BY u.id 
                UNION ALL
                SELECT u.id, u.nome as name, if(state.sigla is null, 'Exterior', state.sigla) as voterTitleUF, if(city.cidade is null, 'Exterior', city.cidade) as voterTitleCity, 
                    u.titulo_eleitoral_municipio as voterTitleCityString , u.titulo_eleitoral as voterTitleNumber
                FROM tb_pessoa AS u
                LEFT JOIN  tb_estado as state ON state.id = u.titulo_eleitoral_uf_id 
                LEFT JOIN  tb_cidade as city ON city.id = u.titulo_eleitoral_municipio_id 
                WHERE u.filiado in (4) AND IF(u.data_solicitacao_refiliacao IS NULL, u.data_solicitacao_filiacao, u.data_solicitacao_refiliacao) >= FROM_DAYS(TO_DAYS(CURDATE()) - 3) {$where}                 
                GROUP BY u.id 
                ORDER BY name ASC";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function recommendation(): array
    {
        try {
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT nome FROM tb_pessoa WHERE filiado IN (7,8)";
            $rows = $pdo->prepare($sql)->execute();
            return $rows->fetchAllAssociative();
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function checkAffiliated(string $value)
    {
        try {
            $email = strpos($value, "@");
            $value = $email ? $value : Utils::formatCpf($value);
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT nome, titulo_eleitoral, cpf, ci.cidade, es.estado, es.sigla AS uf,
                        DATE_FORMAT(if(filiado = 7, data_filiacao, data_refiliacao), '%d/%m/%Y') AS filiacao
                    FROM tb_pessoa
                    LEFT JOIN tb_cidade ci ON ci.id = tb_pessoa.titulo_eleitoral_municipio_id
                    LEFT JOIN tb_estado es ON es.id = tb_pessoa.titulo_eleitoral_uf_id
                    WHERE filiado IN(7,8) AND (cpf = :value || email = :value)";
            $rows = $pdo->prepare($sql)->execute([':value' => $value]);
            return $rows->fetchAssociative();
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function checkAcademiaDoAmanha(string $cpf)
    {
        $cpf = Utils::onlyNumbers($cpf);
        if ($cpf == '') return;
        $cpf = Utils::formatCpf($cpf);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT id FROM tb_pessoa WHERE plano_de_contribuicao IN(3,4) AND cpf = :cpf";
        $rows = $pdo->prepare($sql)->execute([':cpf' => $cpf]);
        return $rows->fetchAssociative();
    }

    public function apiSelectionProcess($email): array
    {
        try {
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT p.id, p.nome, p.cpf, p.filiado, p.data_nascimento, p.email, 
  						(SELECT telefone FROM tb_telefone WHERE tb_pessoa_id= p.id AND tb_tipo_telefone_id = 3 ORDER BY id DESC LIMIT 1) AS 'telefone',
                        p.titulo_eleitoral, p.titulo_eleitoral_zona, p.titulo_eleitoral_secao, p.titulo_eleitoral_municipio,
                        p.titulo_eleitoral_uf, p.nome_mae, p.filiado_id, p.data_filiacao, p.data_refiliacao, e.endereco, e.complemento,
                        e.bairro, e.numero, e.cep, e.cidade, e.estado
                    FROM tb_pessoa p 
                    LEFT JOIN tb_endereco e ON e.tb_pessoa_id = p.id
                    WHERE p.email = '$email' LIMIT 1";
            $rows = $pdo->prepare($sql)->execute();
            return $rows->fetchAllAssociative();
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function challengePeriod(): array
    {
        try {
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT tbp.id,tbp.nome, tbp.cpf, tbc.cidade, tbe.estado, 
                    IFNULL(
                        (SELECT nome FROM tb_diretorio tbd WHERE tbd.tb_cidade_id = tbp.titulo_eleitoral_municipio_id AND tbd.municipal = 'S' AND tbd.ativo = 'S' AND tbd.recebe_email = 'S'),
                        (SELECT nome FROM tb_diretorio tbd WHERE tbd.tb_estado_id = tbp.titulo_eleitoral_uf_id AND tbd.estadual = 'S' AND tbd.ativo = 'S' AND tbd.recebe_email = 'S')) AS diretorio,
                    IFNULL(
                        (SELECT email FROM tb_diretorio tbd WHERE tbd.tb_cidade_id = tbp.titulo_eleitoral_municipio_id AND tbd.municipal = 'S' AND tbd.ativo = 'S' AND tbd.recebe_email = 'S'),
                        (SELECT email FROM tb_diretorio tbd WHERE tbd.tb_estado_id = tbp.titulo_eleitoral_uf_id AND tbd.estadual = 'S' AND tbd.ativo = 'S' AND tbd.recebe_email = 'S')) AS email,
                    tbpsf.status, IF(tbp.exemption = 1, 'Sim', 'Não') AS isenta, CONCAT(TIMESTAMPDIFF(YEAR, tbp.data_nascimento,NOW()), ' anos') AS idade,
                    (SELECT CONCAT(
                        SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(tbtel.telefone), '(', ''), ')', ''), '-',''), ' ',  ''), 1, 2), 
                        SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(tbtel.telefone), '(', ''), ')', ''), '-', ''), ' ',  ''), 3, 5),  '',
                        SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(tbtel.telefone), '(', ''), ')', ''), '-', ''),' ', ''), 8))
                    FROM tb_telefone tbtel WHERE tbtel.tb_pessoa_id = tbp.id AND tb_tipo_telefone_id = 3 LIMIT 1) AS telefone, tbp.data_solicitacao_filiacao
                    FROM tb_pessoa tbp
                    LEFT JOIN tb_cidade tbc ON tbc.id = tbp.titulo_eleitoral_municipio_id
                    LEFT JOIN tb_estado tbe ON tbe.id = tbp.titulo_eleitoral_uf_id
                    LEFT JOIN tb_pessoa_status_filiado tbpsf ON tbpsf.id = tbp.filiado
                    WHERE tbp.filiado IN (2,14,4) AND if(tbp.filiado = 2, tbp.data_solicitacao_filiacao = DATE_SUB(CURDATE(),INTERVAL 1 DAY), '1 = 1')
                    GROUP BY tbp.id";
            $rows = $pdo->prepare($sql)->execute();
            return $rows->fetchAllAssociative();
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function refiliation(): array
    {
        try {
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT tp.id, tp.nome, tp.cpf, tbe.estado AS titulo_eleitoral_uf, tp.titulo_eleitoral, tbc.cidade AS titulo_eleitoral_municipio, 
                        tp.titulo_eleitoral_zona, tp.titulo_eleitoral_secao, DATE_FORMAT(tp.data_solicitacao_refiliacao, '%d/%m/%Y') as data_solicitacao_refiliacao, tpsf.status AS filiado,
                        IFNULL(
                            (SELECT nome FROM tb_diretorio tbd WHERE tbd.tb_cidade_id = tp.titulo_eleitoral_municipio_id AND tbd.municipal = 'S' AND tbd.ativo = 'S' AND tbd.recebe_email = 'S'),
                            (SELECT nome FROM tb_diretorio tbd WHERE tbd.tb_estado_id = tp.titulo_eleitoral_uf_id AND tbd.estadual = 'S' AND tbd.ativo = 'S' AND tbd.recebe_email = 'S')) AS diretorio,
                        IFNULL(
                            (SELECT email FROM tb_diretorio tbd WHERE tbd.tb_cidade_id = tp.titulo_eleitoral_municipio_id AND tbd.municipal = 'S' AND tbd.ativo = 'S' AND tbd.recebe_email = 'S'),
                            (SELECT email FROM tb_diretorio tbd WHERE tbd.tb_estado_id = tp.titulo_eleitoral_uf_id AND tbd.estadual = 'S' AND tbd.ativo = 'S' AND tbd.recebe_email = 'S')) AS email
                    FROM tb_pessoa tp
                    LEFT JOIN tb_cidade tbc ON tbc.id = tp.titulo_eleitoral_municipio_id
                    LEFT JOIN tb_estado tbe ON tbe.id = tp.titulo_eleitoral_uf_id
                    LEFT JOIN tb_pessoa_status_filiado tpsf ON tpsf.id = tp.filiado
                    WHERE tp.filiado = 14 
                        AND IF(tp.exemption = 1, tp.data_solicitacao_refiliacao, (SELECT DATE_FORMAT(data_pago, '%d/%m/%Y') FROM tb_transacoes tt WHERE tt.tb_pessoa_id = tp.id AND tt.origem_transacao = 2 AND tt.status = 'paid' ORDER BY tt.id DESC LIMIT 1)) <= FROM_DAYS((TO_DAYS(CURDATE()) - 3)) 
                        AND (tp.observacao != 'aguardando aprovacao refiliacao' || tp.observacao IS NULL)
                    ORDER BY tp.data_solicitacao_refiliacao";
            $rows = $pdo->prepare($sql)->execute();
            return $rows->fetchAllAssociative();
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function affiliation(): array
    {
        try {
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT tbt.id as transactionId, tbp.id, tbp.filiado_id, tbp.email, tbp.nome as name, tbp.cpf, tbc.cidade as city, tbe.estado as state,
                        tbp.titulo_eleitoral, tbp.titulo_eleitoral_zona, tbp.titulo_eleitoral_secao, tbp.exemption AS isencao, tbp.data_solicitacao_filiacao
                    FROM tb_transacoes tbt
                    LEFT JOIN tb_pessoa tbp ON tbp.id = tbt.tb_pessoa_id
                    LEFT JOIN tb_cidade tbc ON tbc.id = tbp.titulo_eleitoral_municipio_id
                    LEFT JOIN tb_estado tbe ON tbe.id = tbp.titulo_eleitoral_uf_id
                    WHERE tbt.origem_transacao = 2
                        AND tbt.status = 'paid'
                        AND tbp.filiado = 2
                        AND tbp.exemption = 0
                        AND DATE_FORMAT(tbt.data_pago, '%Y-%m-%d') <= FROM_DAYS(TO_DAYS(CURDATE()) - 3)
                        AND DATE_FORMAT(tbt.data_pago, '%Y-%m-%d') >= tbp.data_solicitacao_filiacao
                    GROUP BY tbt.tb_pessoa_id
                    ORDER BY tbt.data_pago";
            $rows = $pdo->prepare($sql)->execute();
            return $rows->fetchAllAssociative();
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function getLastAffiliatedIdFree(): array
    {
        try {
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT filiado_id + 1 AS id FROM tb_pessoa WHERE filiado_id is not null ORDER BY filiado_id DESC LIMIT 1";
            $rows = $pdo->prepare($sql)->execute();
            return $rows->fetchAllAssociative();
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function requestsNotice(): array
    {
        try {
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT SUBSTRING_INDEX(tp.nome, ' ', 1) AS firstName, tp.email, tp.filiado, tp.email,
                        (SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(tbtel.telefone, '-', ''),'(',''),')',''),' ',''),'+','')
                            FROM tb_telefone tbtel  WHERE ((tbtel.tb_pessoa_id = tp.id) AND (tbtel.tb_tipo_telefone_id = 3)) LIMIT 1) AS phone,
                        (SELECT id FROM tb_transacoes tt WHERE tt.tb_pessoa_id = tp.id  AND tt.data_criacao >= tp.data_solicitacao_filiacao AND tt.origem_transacao = 2 AND status IN ('pending','expired')  ORDER BY id DESC LIMIT 1) transaction
                    FROM tb_pessoa tp
                    WHERE tp.filiado = 1 AND (tp.data_solicitacao_filiacao = CURDATE() - INTERVAL 5 DAY || tp.data_solicitacao_filiacao = CURDATE() - INTERVAL 6 DAY)
                    UNION ALL 
                    SELECT SUBSTRING_INDEX(tp.nome, ' ', 1) AS firstName, tp.email, tp.filiado, tp.email,
                        (SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(tbtel.telefone, '-', ''),'(',''),')',''),' ',''),'+','')
                            FROM tb_telefone tbtel  WHERE ((tbtel.tb_pessoa_id = tp.id) AND (tbtel.tb_tipo_telefone_id = 3)) LIMIT 1) AS phone,
                        (SELECT id FROM tb_transacoes tt WHERE tt.tb_pessoa_id = tp.id  AND tt.data_criacao >= tp.data_solicitacao_refiliacao AND tt.origem_transacao = 2 AND status IN ('pending','expired')  ORDER BY id DESC LIMIT 1) transaction
                    FROM tb_pessoa tp
                    WHERE tp.filiado = 3 AND (tp.data_solicitacao_refiliacao = CURDATE() - INTERVAL 5 DAY || tp.data_solicitacao_filiacao = CURDATE() - INTERVAL 6 DAY)
                    UNION ALL
                    SELECT SUBSTRING_INDEX(tp.nome, ' ', 1) AS firstName, tp.email, tp.filiado, tp.email,
                        (SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(tbtel.telefone, '-', ''),'(',''),')',''),' ',''),'+','')
                            FROM tb_telefone tbtel  WHERE ((tbtel.tb_pessoa_id = tp.id) AND (tbtel.tb_tipo_telefone_id = 3)) LIMIT 1) AS phone,	'' AS transaction
                    FROM tb_pessoa tp
                    WHERE tp.filiado = 0 AND (tp.data_solicitacao_filiacao = CURDATE() - INTERVAL 3 DAY)";
            $rows = $pdo->prepare($sql)->execute();
            return $rows->fetchAllAssociative();
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function exemptionAffiliation(): array
    {
        try {
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT tp.id, tp.cpf, es.estado AS state, ci.cidade AS city, tp.nome AS name, tp.email, tp.filiado, tp.titulo_eleitoral_zona, tp.titulo_eleitoral_secao, tp.titulo_eleitoral, 
                        tp.data_solicitacao_filiacao
                        FROM tb_pessoa tp
                        LEFT JOIN tb_pessoa_assinatura ta ON ta.tb_pessoa_id = tp.id
                        LEFT JOIN tb_estado es ON es.id = tp.titulo_eleitoral_uf_id
                        LEFT JOIN tb_cidade ci ON ci.id = tp.titulo_eleitoral_municipio_id
                        WHERE ta.origem_transacao = 2 AND tp.filiado = 2 AND tp.exemption = 1 AND tp.data_solicitacao_filiacao <= FROM_DAYS(TO_DAYS(CURDATE()) - 3)
                    UNION ALL
                    SELECT tp.id, tp.cpf, es.estado  AS state, ci.cidade AS city, tp.nome AS name, tp.email, tp.filiado, tp.titulo_eleitoral_zona, tp.titulo_eleitoral_secao, tp.titulo_eleitoral,
                        tp.data_solicitacao_refiliacao
                        FROM tb_pessoa tp
                        LEFT JOIN tb_pessoa_assinatura ta ON ta.tb_pessoa_id = tp.id
                        LEFT JOIN tb_estado es ON es.id = tp.titulo_eleitoral_uf_id
                        LEFT JOIN tb_cidade ci ON ci.id = tp.titulo_eleitoral_municipio_id
                        WHERE ta.origem_transacao = 2 AND tp.filiado = 14 AND tp.exemption = 1 AND tp.data_solicitacao_refiliacao <= FROM_DAYS(TO_DAYS(CURDATE()) - 3)
                        AND (tp.observacao != 'aguardando aprovacao refiliacao' || tp.observacao IS NULL)";
            $rows = $pdo->prepare($sql)->execute();
            return $rows->fetchAllAssociative();
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function affiliateConsultation(string $cpf): array
    {
        try {
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT tp.nome AS name, tp.email, DATE_FORMAT(tp.data_nascimento, '%d/%m/%Y') AS birth, tp.filiado,
                    es.estado AS adressState, es.id AS adressStateId, ci.cidade AS adressCity, ci.id AS adressCityId,
                    es2.estado AS titleState, es2.id AS titleStateId, ci2.cidade AS titleCity, ci2.id AS titleCityId,
                    (SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(tbtel.telefone, '-', ''),'(',''),')',''),' ',''),'+','')
                        FROM tb_telefone tbtel  WHERE ((tbtel.tb_pessoa_id = tp.id) AND (tbtel.tb_tipo_telefone_id = 3)) LIMIT 1) AS phone
                    FROM tb_pessoa tp 
                    LEFT JOIN tb_endereco te ON te.tb_pessoa_id = tp.id
                    LEFT JOIN tb_estado es ON es.id = te.tb_estado_id
                    LEFT JOIN tb_cidade ci ON ci.id = te.tb_cidade_id
                    LEFT JOIN tb_estado es2 ON es2.id = tp.titulo_eleitoral_uf_id
                    LEFT JOIN tb_cidade ci2 ON ci2.id = tp.titulo_eleitoral_municipio_id
                    WHERE tp.cpf = :cpf";
            $rows = $pdo->prepare($sql)->execute([':cpf' => $cpf]);
            return $rows->fetchAllAssociative();
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function verifyExemptionAffiliation(string $id): array
    {
        try {
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT*FROM tb_pessoa WHERE filiado IN (7,8) AND irpf = 1 AND exemption = 1 AND id = :id";
            $rows = $pdo->prepare($sql)->execute([':id' => $id]);
            return $rows->fetchAllAssociative();
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function getRecordsBotConversa(int $state): array
    {
        try {
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT 
                        SUBSTRING_INDEX(tp.nome, ' ', 1) as firstname,
                        TRIM( SUBSTR(tp.nome, LOCATE(' ', tp.nome)) ) as surname,
                        ci.cidade as city,
                        IFNULL((SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CONCAT(55, tbtel.telefone), '-', ''),'(',''),')',''),' ',''),'+','')
                        FROM tb_telefone tbtel  WHERE ((tbtel.tb_pessoa_id = tp.id) AND (tbtel.tb_tipo_telefone_id = 3) AND (tbtel.contact != 0 || tbtel.contact IS NULL)) LIMIT 1), '') AS phone,
                        (CASE tp.filiado
                            WHEN 7 THEN 'Filiado'
                            WHEN 8 THEN 'Filiado'
                            WHEN 9 THEN 'Desfiliado'
                            WHEN 0 THEN 'Cadastrado'
                            WHEN 5 THEN 'Cadastrado'
                            ELSE 'Cadastrado'
                        END) AS 'status',
                        IFNULL((SELECT IF(tp.exemption = 1, 'Isento', IF(tbt.status = 'paid' , 'Adimplente', 'Inadimplente'))
                            FROM  tb_transacoes tbt
                            WHERE tbt.tb_pessoa_id = tp.id AND tbt.status NOT IN ('accumulated' , 'canceled', 'reversed') AND IF(tp.pendencia_titulo = 1, tbt.origem_transacao = 1, tbt.origem_transacao = 2)
                            AND if(tbt.forma_pagamento = 2 AND tbt.status != 'paid', tbt.data_criacao < DATE_SUB(CURDATE(), INTERVAL if(tbt.periodicidade in(1, 12), 5, 1) DAY), '1 = 1')
                            ORDER BY tbt.data_criacao DESC , tbt.id ASC LIMIT 1
                        ), 'Inadimplente') AS 'situation',
                        (CASE tp.genero
                            WHEN 1 THEN 'Masculino'
                            WHEN 2 THEN 'Feminino'
                            WHEN 3 THEN 'Não quero informar'
                            ELSE ''
                        END) AS gender,
                        (CASE 
                            WHEN ev.evaluationStatus >= 5 THEN 'Inscrito Jornada Completo'
                            WHEN ev.evaluationStatus < 5 THEN 'Inscrito Jornada Incompleto'
                            ELSE ''
                        END) AS 'jornada2024'
                    FROM tb_pessoa tp
                    LEFT JOIN tb_cidade ci ON ci.id = tp.titulo_eleitoral_municipio_id
                    LEFT JOIN jornada2024.candidate ca ON ca.cpf = REPLACE(REPLACE(tp.cpf, '-', ''), '.', '')
                    LEFT JOIN jornada2024.evaluation ev ON ev.candidate = ca.id
                    WHERE tp.titulo_eleitoral_uf_id = :state
                    AND DATE_FORMAT(tp.ultima_atualizacao, '%Y-%m-%d') = DATE_SUB(CURDATE(), INTERVAL 1 DAY)";
            $rows = $pdo->prepare($sql)->execute([':state' => $state]);
            return $rows->fetchAllAssociative();
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function getDataSearch($token): array
    {
        try {
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT tp.id, tp.token_ad, tp.nome, tp.cpf, tp.email, tp.genero, tp.genero, es.sigla AS state, tp.titulo_eleitoral_municipio_id, 
                        tp.escolaridade, tp.filiado, FLOOR(DATEDIFF(NOW(), tp.data_nascimento) / 365) AS idade,
                        IFNULL((SELECT IF(tp.exemption = 1, 'Isento', IF(tbt.status = 'paid' , 'Adimplente', 'Inadimplente'))
                            FROM  tb_transacoes tbt
                            WHERE tbt.tb_pessoa_id = tp.id 
                            AND tbt.status NOT IN ('accumulated' , 'canceled', 'reversed') 
                            AND IF(tp.pendencia_titulo = 1, tbt.origem_transacao = 1, tbt.origem_transacao = 2)
                            AND if(tbt.forma_pagamento = 2 AND tbt.status != 'paid', tbt.data_criacao < DATE_SUB(CURDATE(), INTERVAL if(tbt.periodicidade in(1, 12), 5, 1) DAY), '1 = 1')
                            ORDER BY tbt.data_criacao DESC , tbt.id ASC LIMIT 1
                        ), 'Inadimplente') AS situation
                    FROM tb_pessoa tp
                    LEFT JOIN tb_transacoes tbt ON tbt.tb_pessoa_id = tp.id
                    LEFT JOIN tb_estado es ON es.id = tp.titulo_eleitoral_uf_id
                    WHERE tp.filiado in (7,8,9) and tp.token_ad = :token
                    GROUP BY tp.id";
            $rows = $pdo->prepare($sql)->execute([':token' => $token]);
            return $rows->fetchAllAssociative();
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function listIndications(User $user, string $name, string $status, $limit = null, $offset = null): array
    {
        $params = [];
        $limitSql = $this->generateLimit($limit, $offset);
        $where = $this->generateWhere($user, $name, $status, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT tp.nome AS name, IF(tp.exemption = 1, 'Sim', 'Não') AS exemption, tpsf.status, es.estado AS state, ci.cidade AS city, tp.filiado AS affiliated,
                    (SELECT CONCAT(
                        SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(tbtel.telefone), '(', ''), ')', ''), '-',''), ' ',  ''), 1, 2), 
                        SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(tbtel.telefone), '(', ''), ')', ''), '-', ''), ' ',  ''), 3, 5),  '',
                        SUBSTR(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(tbtel.telefone), '(', ''), ')', ''), '-', ''),' ', ''), 8))
                    FROM tb_telefone tbtel WHERE tbtel.tb_pessoa_id = tp.id AND tb_tipo_telefone_id = 3 LIMIT 1) AS phone,
                    (CASE tp.filiado
                        WHEN 1 THEN DATE_FORMAT(tp.data_solicitacao_filiacao, '%d/%m/%Y')
                        WHEN 2 THEN DATE_FORMAT(tp.data_solicitacao_filiacao, '%d/%m/%Y')
                        WHEN 3 THEN DATE_FORMAT(tp.data_solicitacao_refiliacao, '%d/%m/%Y')
                        WHEN 14 THEN DATE_FORMAT(tp.data_solicitacao_refiliacao, '%d/%m/%Y')
                        WHEN 7 THEN DATE_FORMAT(tp.data_solicitacao_filiacao, '%d/%m/%Y')
                        WHEN 8 THEN DATE_FORMAT(tp.data_solicitacao_refiliacao, '%d/%m/%Y')
                        ELSE ''
                    END) AS date
                FROM indicator i 
                LEFT JOIN tb_pessoa tp ON tp.id = i.user
                LEFT JOIN tb_pessoa_status_filiado tpsf ON tpsf.id = tp.filiado
                LEFT JOIN tb_estado es ON es.id = tp.titulo_eleitoral_uf_id
                LEFT JOIN tb_cidade ci ON ci.id = tp.titulo_eleitoral_municipio_id
                WHERE i.indicator = {$user->getId()} AND i.created_at BETWEEN '2023-12-12 00:00:01' AND '2023-12-31 23:59:59' {$where} 
                GROUP BY i.user {$limitSql}";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    public function listIndicationsTotal(User $user, string $name, string $status): array
    {
        $params = [];
        $where = $this->generateWhere($user, $name, $status, $params);
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT * FROM indicator i 
                LEFT JOIN tb_pessoa tp ON tp.id = i.user
                LEFT JOIN tb_pessoa_status_filiado tpsf ON tpsf.id = tp.filiado
                LEFT JOIN tb_estado es ON es.id = tp.titulo_eleitoral_uf_id
                LEFT JOIN tb_cidade ci ON ci.id = tp.titulo_eleitoral_municipio_id
                WHERE i.indicator = {$user->getId()} AND i.created_at BETWEEN '2023-12-12 00:00:01' AND '2023-12-31 23:59:59' {$where} 
                GROUP BY i.user";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }

    private function generateWhere($user = null, $name = null, $status = null, &$params): string
    {
        $where = '';
        if ($name) {
            $params[':name'] = "%$name%";
            $where .= " AND tp.nome LIKE :name";
        }
        if ($status) {
            $status = match ($status) {
                '1' => '1, 3',
                '2' => '2, 14',
                '3' => '7, 8',
                default => '0'
            };
            $where .= " AND tp.filiado IN ($status)";
        }
        return $where;
    }

    public function getData(string $user): array
    {
        try {
            $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
            $sql = "SELECT tp.nome, tp.email, tp.cpf, DATE_FORMAT(tp.data_nascimento, '%d/%m/%Y') AS nascimento, tp.genero, tp.rg,
                        te.cep, te.endereco AS address, te.numero AS number, te.bairro AS district, te.complemento AS complement, es.sigla AS addressState, ci.cidade AS addressCity,
                        tp.titulo_eleitoral_pais_id, tp.titulo_eleitoral_uf_id AS titleState, tp.titulo_eleitoral_municipio_id AS titleCity, tp.titulo_eleitoral AS title,
                            tp.titulo_eleitoral_zona AS zone, tp.titulo_eleitoral_secao AS session, tp.nome_mae AS mother,
                        (SELECT CONCAT(tbtel.ddi,'+', REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(tbtel.telefone, '-', ''),'(',''),')',''),' ',''),'+',''))
                            FROM tb_telefone tbtel  WHERE ((tbtel.tb_pessoa_id = tp.id) AND (tbtel.tb_tipo_telefone_id = 3)) LIMIT 1) AS phone, tp.status
                    FROM tb_pessoa tp
                    LEFT JOIN tb_endereco te ON te.tb_pessoa_id = tp.id
                    LEFT JOIN tb_estado es ON es.id = te.tb_estado_id
                    LEFT JOIN tb_cidade ci ON ci.id = te.tb_cidade_id
                    WHERE tp.id = :user
                    GROUP BY tp.id";
            $rows = $pdo->prepare($sql)->execute([':user' => $user]);
            return $rows->fetchAllAssociative();
        } catch (\PDOException $e) {
            echo 'Error:' . $e->getMessage();
        }
    }

    public function getByMinId(int $minId): array
    {
        return $this->getEntityManager()
            ->createQuery("SELECT u FROM App\Models\Entities\User AS u                                  
                              WHERE u.id > :id")
            ->setMaxResults(500)
            ->setParameters([':id' => $minId])
            ->getResult();
    }

    public function getDataIA(string $email): ?array
    {
        try {
            if (empty($email)) throw new \InvalidArgumentException("Email do usuário inválido");

            $pdo = $this->getEntityManager()->getConnection();
            $sql = "SELECT 
                        tp.id,
                        tp.nome, 
                        tp.email,
                        tpsf2.status AS filiado,
                        DATE_FORMAT(tp.data_filiacao, '%d/%m/%Y') AS data_filiacao,
                        DATE_FORMAT(tp.data_desfiliacao, '%d/%m/%Y') AS data_desfiliacao,
                        es.estado AS state,
                        ci.cidade AS city,
                        (SELECT id FROM tb_transacoes WHERE tb_pessoa_id = tp.id ORDER BY id DESC LIMIT 1) AS transaction,
                        IF(tpsf.forma_pagamento = 2, 'Boleto', 'Cartão de Crédito') AS forma_pagamento,
                        tpsf.valor AS valor_assinatura,
                        DATE_FORMAT(tpsf.data_gera_fatura, '%d/%m/%Y') AS proxima_cobranca,
                        RIGHT(tpc.legenda, 4) AS card_brand,
                        tpc.expiration AS card_expiration,
                        (SELECT 
                            CONCAT(
                                COUNT(DISTINCT DATE_FORMAT(data_criacao, '%Y-%m')), 
                                ' meses'
                            ) as periodo_total
                            FROM tb_transacoes 
                            WHERE tb_pessoa_id = tp.id 
                                AND status IN ('pending', 'expired')
                                AND data_criacao <= CURRENT_DATE()
                        ) AS periodo_total_inadimplente

                    FROM tb_pessoa tp
                    LEFT JOIN tb_estado es ON es.id = tp.titulo_eleitoral_uf_id
                    LEFT JOIN tb_cidade ci ON ci.id = tp.titulo_eleitoral_municipio_id
                    LEFT JOIN tb_pessoa_assinatura tpsf ON tpsf.tb_pessoa_id = tp.id
                    LEFT JOIN tb_pessoa_cartao_credito tpc ON tpc.token_cartao_credito = tpsf.token_cartao_credito
                    LEFT JOIN tb_pessoa_status_filiado tpsf2 ON tpsf2.id = tp.filiado
                    WHERE tp.email = :email 
                        AND tpsf.status = :status 
                        AND tpsf.origem_transacao IN (:origin, :origin2)";

            $result = $pdo->executeQuery($sql, [
                'email' => $email, 
                'status' => 'created', 
                'origin' => '2',
                'origin2' => '3'
            ])->fetchAssociative();

            return $result ?: null;
            
        } catch (\PDOException $e) {
            throw new \Exception("Erro ao buscar dados do usuário: " . $e->getMessage());
        }
    }
}