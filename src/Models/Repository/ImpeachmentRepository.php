<?php

namespace App\Models\Repository;

use App\Models\Entities\Impeachment;
use Doctrine\ORM\EntityRepository;

class ImpeachmentRepository extends EntityRepository
{
    public function save(Impeachment $entity):Impeachment
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}