<?php

namespace App\Models\Repository;

use App\Models\Entities\ResearchNovoAffiliated;
use Doctrine\ORM\EntityRepository;

class ResearchNovoAffiliatedRepository extends EntityRepository
{
    public function save(ResearchNovoAffiliated $entity): ResearchNovoAffiliated
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function getList(): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT
                    DATE_FORMAT(ru.created, '%d/%m/%Y %H:%i:%s') AS 'Registro',
                    tp.nome AS 'Nome',
                    tp.cpf AS 'CPF',
                    tp.email AS 'E-mail',
                    (CASE tp.genero
                        WHEN 1 THEN 'Homem'
                        WHEN 2 THEN 'Mulher'
                        WHEN 3 THEN 'Não Quero Informar'
                        ELSE ''
                    END) AS 'Sexo',
                    CONCAT(ru.age, ' Anos') AS 'Idade',
                    es.sigla AS 'Estado',
                    ci.cidade AS 'Cidade',
                    (CASE ru.profession
                        WHEN 1 THEN 'Trabalhador doméstico'
                        WHEN 2 THEN 'Militar do exército, da marinha, da aeronáutica, da polícia militar ou do corpo de bombeiros militar'
                        WHEN 3 THEN 'Do setor privado'
                        WHEN 4 THEN 'Do setor público - funcionário estatutário'
                        WHEN 5 THEN 'Do setor público - empregado não estatutário'
                        WHEN 6 THEN 'De empresas estatais'
                        WHEN 7 THEN 'Empregador (com pelo menos um empregado)'
                        WHEN 8 THEN 'Conta própria (sem empregados)'
                        WHEN 9 THEN 'Trabalhador não remunerado em ajuda a algum morador do domicílio ou parente'
                        WHEN 10 THEN 'Aposentado(a)'
                    END) AS 'Profissão',
                    (CASE tp.escolaridade
                        WHEN 1 THEN 'Ensino Superior'
                        WHEN 2 THEN 'Ensino Superior Incompleto'
                        WHEN 3 THEN 'Ensino Médio'
                        WHEN 4 THEN 'Ensino Médio Incompleto'
                        WHEN 5 THEN 'Ensino Fundamental'
                        WHEN 6 THEN 'Ensino Fundamental Incompleto'
                        WHEN 7 THEN 'Não quero informar'
                    END) AS escolaridade,
                    (CASE ru.income
                        WHEN 1 THEN '00 a 500,00'
                        WHEN 2 THEN '501,00 a 1.000,00'
                        WHEN 3 THEN '1.001,00 a 2.000,00'
                        WHEN 4 THEN '2.001,00 a 3.000,00'
                        WHEN 5 THEN '3.001,00 a 5.000,00'
                        WHEN 6 THEN '5.001,00 a 10.000,00'
                        WHEN 7 THEN '10.001,00 a 20.000,00'
                        WHEN 8 THEN '20.001,00 a 100.000'
                        WHEN 9 THEN '100.001 ou mais'
                    END) AS 'Faixa de renda familiar mensal bruta',
                    IF(ru.contact = 1, 'Sim', 'Não') AS 'Newsletter',
                    REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CONCAT(' ', ra.question1), 
                        ',', ' '),
                        ' 1 ', ' Estado mais simples, leve e eficiente;'), 
                        ';2', '; Renovação política do País;'),
                        3, ' Um Brasil com mais oportunidades e conectado ao mundo;'),
                        4, ' Fim de privilégios e mordomias com o dinheiro público;'),
                        5, ' Maior atenção à Educação em nosso país;'),
                        6, ' Maior defesa do liberalismo;'),
                        7, ' Sistema político verdadeiramente representativo;'),
                        8, ' Fortalecimento do combate à corrupção e à impunidade;'),
                        9, ' Priorização da Saúde em nosso país;'),
                        10, ' Garantia dos direitos iguais a todos os cidadãos;'),
                        11, ' País mais sustentável para as próximas geraçõe;'),
                        12, ' Outros;') AS '1. Por que você quer ver o Partido NOVO crescer? (Selecione os 3 com os quais você mais se identifica)',
                    ra.textAreaOthersQuestion1 AS 'Descrição',
                    REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(ra.question2, 
                        ',', ' '),
                        '1 ', ' Mão de obra: me voluntariar para atender às demandas do Partido;'), 
                        2, ' Opinião: dar sugestões e ser ouvido pelo Partido e/ou Políticos do Partido;'),
                        3, ' Financeiro: contribuir de forma financeira e mensal;'),
                        4, ' Expansão: trazer novas pessoas ao Partido;'),
                        5, ' Gestão: me engajar mais com a gestão do Partido;'),
                        6, ' Liderança: me tornar alguém influente dentro do Partido;'),
                        7, ' Divulgação: divulgar as ações e projetos do Partido entre meus contatos;'),
                        8, ' Candidatura: um dia gostaria de ser eleito a um cargo político pelo Partido;'),
                        9, ' Dirigente: colaborar em campanha de um candidato do Partido;'),
                        10, ' Outros;') AS '2. Como você gostaria de ter maior participação nas atividades do Partido NOVO?',
                    ra.textAreaOthersQuestion2 AS 'Descrição2',
                    REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(ra.question3, 
                        ',', ' '),
                        1, ' Me identifiquei com os princípios e valores do Partido;'), 
                        2, ' Gostaria de ter mais proximidade e acesso ao Partido e/ou Políticos do Partido;'),
                        3, ' Fui convidado por uma pessoa próxima, e me filiei mesmo sem conhecer muito sobre o Partido;'),
                        4, ' Considero que contribuir na política é importante, e vi no NOVO a melhor opção entre os demais partidos;'),
                        5, ' Entendi que a Filiação e a minha contribuição financeira mensal contribuem para a manutenção do Partido e para a construção de um NOVO Brasil;'),
                        6, ' Desejo me tornar uma liderança dentro do Partido;'),
                        7, ' Desejo me candidatar a um cargo político;'),
                        8, ' Outros;') AS '3. O que levou você a se filiar ao Partido NOVO?',
                    ra.textAreaOthersQuestion3 AS 'Descrição3',
                    IFNULL(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(ra.question31, 
                        1, ' Gostaria de voltar a contribuir;'), 
                        2, ' Parei de contribuir pois estou passando por dificuldades financeiras;'),
                        3, ' Parei de contribuir pois não vejo mais benefícios;'),
                        4, ' Parei de contribuir pois não concordo mais com os posicionamentos;'),
                        5, ' Não sabia que eu ainda era Filiado. Achei que parando de contribuir eu deixaria de ser Filiado;'),
                        6, ' Não quero mais ser Filiado ao Partido;'),
                        7, ' Outros;'), '') AS '3.1 Percebemos que sua contribuição financeira mensal como Filiado encontra-se defasada ou atrasada. Poderia indicar o motivo que o levou a deixar de realizar essa contribuição?',
                    ra.textAreaOthersQuestion31 AS 'Descrição4',
                    IFNULL(REPLACE(REPLACE(REPLACE(ra.question32, 
                        1, ' Até R$ 20,00 por mês;'), 
                        2, ' Até R$ 30,00 por mês;'),
                        3, ' Outros;'), '') AS '3.2 Entendemos a situação. E, já que você gostaria de continuar contribuindo financeiramente, qual valor ficaria bom para você, atualmente?',
                    ra.textAreaOthersQuestion32 AS 'Descrição5',
                    ra.question4 AS '4. Como você entende que sua contribuição financeira mensal como Filiado contribui para a construção de um NOVO Brasil?',
                    ra.question5 AS '5. Em uma escala de 0 a 10, qual a probabilidade de você recomendar a filiação ao Partido NOVO a um amigo ou colega?', 
                    ra.question51Option1 AS '(9,10) 5.1 Qual a principal razão para você recomendar a filiação ao Partido?',
                    ra.question51Option2 AS '(9,10) 5.2 O que o Partido NOVO poderia fazer ainda melhor?',
                    ra.question52Option1 AS '(7,8) 5.1 Qual a principal razão para você recomendar a filiação ao Partido?',
                    ra.question52Option2 AS '(7,8) 5.2 Qual é a principal ação que o Partido NOVO poderia tomar para tornar você mais propenso a recomendar a filiação ao mesmo?',
                    ra.question53Option1 AS '(0-6) 5.1 Qual é a principal ação que o Partido NOVO poderia tomar para tornar você mais propenso a recomendar a filiação ao mesmo?',
                    ra.question53Option2 AS '(0-6) 5.2 Você tem algum outro comentário?',
                    IF(ra.question6 = 1, 'Sim', 'Não') AS '6. Você gostaria de receber acesso exclusivo a uma apresentação sobre o novo momento do Partido NOVO?'
                FROM researchNovoAffiliated ra
                LEFT JOIN researchNovoUser ru ON ra.researchNovoUser = ru.id
                LEFT JOIN tb_pessoa tp ON tp.id = ru.user
                LEFT JOIN tb_estado es ON tp.titulo_eleitoral_uf_id = es.id
                LEFT JOIN tb_cidade ci ON tp.titulo_eleitoral_municipio_id = ci.id
                GROUP BY ra.id
                ORDER BY ru.created DESC";
        $rows = $pdo->prepare($sql)->execute();
        return $rows->fetchAllAssociative();
    }
}