<?php

namespace App\Models\Repository;

use App\Models\Entities\IDontWantToPay;
use Doctrine\ORM\EntityRepository;

class IDontWantToPayRepository extends EntityRepository
{
    public function save(IDontWantToPay $entity): IDontWantToPay
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}