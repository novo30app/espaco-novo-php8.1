<?php

namespace App\Models\Repository;

use App\Models\Entities\BrandMessage;
use Doctrine\ORM\EntityRepository;

class BrandMessageRepository extends EntityRepository
{
    public function save(BrandMessage $entity):BrandMessage
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }
}