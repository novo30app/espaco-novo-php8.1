<?php

namespace App\Models\Repository;

use App\Models\Entities\Logs;
use Doctrine\ORM\EntityRepository;

class LogsRepository extends EntityRepository
{
    public function save(Logs $entity):Logs
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function publicDonationCheckValue(): array
    {
        $date = date('Y-m-d');
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT count(*) AS total
                FROM logs
                WHERE method = :method AND inserted LIKE :date";
        $rows = $pdo->prepare($sql)->execute(['method' => 'donation', ':date' => "%$date%"]);
        return $rows->fetchAssociative();
    }
}