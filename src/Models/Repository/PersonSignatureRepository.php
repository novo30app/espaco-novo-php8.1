<?php

namespace App\Models\Repository;

use App\Models\Entities\PersonSignature;
use Doctrine\ORM\EntityRepository;

class PersonSignatureRepository extends EntityRepository
{
    public function save(PersonSignature $entity): PersonSignature
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function contributionIncrease(): array
    {
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT a.id, a.tb_pessoa_id AS personId, p.email
                FROM tb_pessoa_assinatura a
                JOIN tb_pessoa p ON p.id = a.tb_pessoa_id
                WHERE p.filiado IN (7,8) 
                AND a.origem_transacao = 2 
                AND a.status = 'created' AND (p.bloqueado IS NULL OR p.bloqueado = 0)
                AND a.tb_pessoa_id NOT IN (SELECT user FROM historicalIncrease)
                AND p.exemption = 0
                LIMIT 5100";
        $rows = $pdo->prepare($sql)->execute();
        return $rows->fetchAllAssociative();
    }

    public function getRefreshCard(): array
    {
        $params = [];
        $pdo = $this->getEntityManager()->getConnection()->getWrappedConnection();
        $sql = "SELECT tp.id, tp.email FROM tb_pessoa tp
                LEFT JOIN tb_pessoa_assinatura ta ON ta.tb_pessoa_id = tp.id
                WHERE ta.origem_transacao = 1 AND ta.periodicidade = 1 AND ta.gateway_pagamento = 2 
                  AND ta.status = 'created' AND tp.observacao IS NULL AND (tp.bloqueado IS NULL OR tp.bloqueado = 0)
                GROUP BY tp.id
                ORDER BY tp.id ASC
                LIMIT 600";
        $rows = $pdo->prepare($sql)->execute($params);
        return $rows->fetchAllAssociative();
    }
}