<?php

namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\Mapping as ORM;

/**
 * TbConteudo
 *
 * @Table(name="tb_conteudo")
 * @Entity
 */
class Communicated
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data", type="date", nullable=true)
     */
    private $data;

    /**
     * @var string|null
     *
     * @Column(name="processo", type="string", length=15, nullable=true)
     */
    private $processo;

    /**
     * @var string|null
     *
     * @Column(name="titulo", type="string", length=260, nullable=true)
     */
    private $titulo;

    /**
     * @var string|null
     *
     * @Column(name="conteudo", type="text", length=65535, nullable=true)
     */
    private $conteudo;

    /**
     * @var string|null
     *
     * @Column(name="arquivo", type="string", length=200, nullable=true)
     */
    private $arquivo;

    /**
     * @var string|null
     *
     * @Column(name="link_comunicado", type="string", length=200, nullable=true)
     */
    private $linkComunicado;

    /**
     * @var bool|null
     *
     * @Column(name="restrito", type="boolean", nullable=true)
     */
    private $restrito;

    /**
     * @var bool|null
     *
     * @Column(name="exibir", type="boolean", nullable=true)
     */
    private $exibir;

    /**
     * @var bool|null
     *
     * @Column(name="comite", type="boolean", nullable=true)
     */
    private $comite;

    /**
     * @var bool|null
     *
     * @Column(name="comite_consultas", type="boolean", nullable=true)
     */
    private $comiteConsultas;

    /**
     * @var bool|null
     *
     * @Column(name="resolucao", type="boolean", nullable=true)
     */
    private $resolucao;

    /**
     * @var string|null
     *
     * @Column(name="tipo", type="string", length=200, nullable=true)
     */
    private $tipo;

    /**
     * @var bool|null
     *
     * @Column(name="link", type="boolean", nullable=true)
     */
    private $link;


    /**
     * @var string|null
     *
     * @Column(name="diretorio", type="string", length=25, nullable=true)
     */
    private $diretorio;

    /**
     * @var bool|null
     *
     * @Column(name="ativo", type="boolean", nullable=true)
     */
    private $ativo;

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->titulo;
    }

    public function getDate()
    {
        return $this->data;
    }

    public function getProcess()
    {
        return $this->process;
    }

    public function getLinkContent()
    {
        return $this->linkComunicado;
    }

    public function getArchive()
    {
        return $this->arquivo;
    }

    public function getLink()
    {
        return $this->link;
    }

    public function getType()
    {
        return $this->tipo;
    }
}
