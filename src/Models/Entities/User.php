<?php

namespace App\Models\Entities;

use App\Helpers\Session;
use App\Helpers\Utils;
use App\Models\Commons\Entities\UserPoint;

/**
 * TbPessoa
 * @Entity @Table(name="tb_pessoa", uniqueConstraints={@UniqueConstraint(name="idx_cpf", columns={"cpf"}), @UniqueConstraint(name="idx_email", columns={"email"})},  indexes={@Index(name="idx_filiado_id", columns={"filiado_id"}), @Index(name="idx_tb_pessoa_data_solicitacao_refiliacao", columns={"data_solicitacao_refiliacao"}), @Index(name="idx_tb_pessoa_doador", columns={"doador"}), @Index(name="idx_tb_pessoa_data_validade_ativo", columns={"data_validade_ativo"}), @Index(name="idx_ativo_ano", columns={"ativo_ano"}), @Index(name="idx_tb_pessoa_titulo_eleitoral", columns={"titulo_eleitoral"}), @Index(name="idx_tb_pessoa_titulo_eleitoral_municipio_id", columns={"titulo_eleitoral_municipio_id"}), @Index(name="idx_tb_pessoa_data_solicitacao_filiacao", columns={"data_solicitacao_filiacao"}), @Index(name="idx_bloqueado", columns={"bloqueado"}), @Index(name="idx_tb_pessoa_ip", columns={"ip"}), @Index(name="idx_tb_pessoa_data_filiacao", columns={"data_filiacao"}), @Index(name="idx_filiado", columns={"filiado"}), @Index(name="idx_cron_ativo_ano", columns={"cron_ativo_ano"}), @Index(name="idx_tb_pessoa_titulo_eleitoral_uf_id", columns={"titulo_eleitoral_uf_id"}), @Index(name="idx_data_criacao", columns={"data_criacao"})}))
 * @ORM @Entity(repositoryClass="App\Models\Repository\UsersRepository")
 */
class User
{

    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @Column(name="filiado_id", type="integer", nullable=true)
     */
    private $filiadoId;

    /**
     * @var int|null
     *
     * @Column(name="plano_de_contribuicao", type="integer", nullable=true)
     */
    private $paymentPlan = 0;

    /**
     * @var \DateTime|null
     * @Column(name="data_plano", type="datetime", nullable=true)
     */
    private $datePlan;

    /**
     * @var string|null
     *
     * @Column(name="token", type="string", length=60, nullable=true)
     */
    private $token;

    /**
     * @Column(name="token2", type="string", nullable=true)
     */
    private ?string $token2 = '';

    /**
     * @var string|null
     *
     * @Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @Column(name="senha", type="string", length=60, nullable=true)
     */
    private $senha;

    /**
     * @var string|null
     *
     * @Column(name="nome", type="string", length=100, nullable=true)
     */
    private $nome;

    /**
     * @var string|null
     *
     * @Column(name="cpf", type="string", length=25, nullable=true)
     */
    private $cpf;

    /**
     * @var string|null
     *
     * @Column(name="rg", type="string", length=25, nullable=true)
     */
    private $rg;

    /**
     * @var string|null
     *
     * @Column(name="orgao_expedidor", type="string", length=10, nullable=true)
     */
    private $orgaoExpedidor;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_nascimento", type="date", nullable=true)
     */
    private $dataNascimento;

    /**
     * @var string|null
     *
     * @Column(name="titulo_eleitoral", type="string", length=25, nullable=true)
     */
    private $tituloEleitoral;

    /**
     * @var string|null
     *
     * @Column(name="titulo_eleitoral_zona", type="string", length=4, nullable=true)
     */
    private $tituloEleitoralZona;

    /**
     * @var string|null
     *
     * @Column(name="titulo_eleitoral_secao", type="string", length=4, nullable=true)
     */
    private $tituloEleitoralSecao;

    /**
     * @var string|null
     *
     * @Column(name="titulo_eleitoral_pais", type="string", length=100, nullable=true)
     */
    private $tituloEleitoralPais;


    /**
     * @ManyToOne(targetEntity="Country")
     * @JoinColumn(name="titulo_eleitoral_pais_id", referencedColumnName="id", nullable=true)
     * @var Country
     */
    private $tituloEleitoralPaisId;

    /**
     * @var string|null
     *
     * @Column(name="titulo_eleitoral_uf", type="string", length=100, nullable=true)
     */
    private $tituloEleitoralUf;

    /**
     * @ManyToOne(targetEntity="State")
     * @JoinColumn(name="titulo_eleitoral_uf_id", referencedColumnName="id", nullable=true)
     * @var State
     */
    private $tituloEleitoralUfId;

    /**
     * @var string|null
     *
     * @Column(name="titulo_eleitoral_municipio", type="string", length=150, nullable=true)
     */
    private $tituloEleitoralMunicipio;

    /**
     * @ManyToOne(targetEntity="City")
     * @JoinColumn(name="titulo_eleitoral_municipio_id", referencedColumnName="id", nullable=true)
     * @var City
     */
    private $tituloEleitoralMunicipioId;

    /**
     * @var string|null
     *
     * @Column(name="nome_mae", type="string", length=100, nullable=true)
     */
    private $nomeMae;

    /**
     * @var CivilState|null
     *
     * @ManyToOne(targetEntity="CivilState")
     * @JoinColumns({
     *   @JoinColumn(name="estado_civil", referencedColumnName="id")
     * })
     */
    private $estadoCivil;

    /**
     * @var Gender
     *
     * @ManyToOne(targetEntity="Gender")
     * @JoinColumns({
     *   @JoinColumn(name="genero", referencedColumnName="id")
     * })
     */
    private $genero;

    /**
     * @var Schooling
     *
     * @ManyToOne(targetEntity="Schooling")
     * @JoinColumns({
     *   @JoinColumn(name="escolaridade", referencedColumnName="id")
     * })
     */
    private $escolaridade;

    /**
     * @var bool|null
     *
     * @Column(name="opcao_religiosa", type="boolean", nullable=true)
     */
    private $opcaoReligiosa;

    /**
     * @var string|null
     *
     * @Column(name="profissao", type="string", length=100, nullable=true)
     */
    private $profissao;

    /**
     * @var int|null
     *
     * @Column(name="filiacao", type="integer", nullable=true)
     */
    private $filiacao;

    /**
     * @var string|null
     *
     * @Column(name="filiacao_qual_partido", type="string", length=45, nullable=true)
     */
    private $filiacaoQualPartido;

    /**
     *
     * @Column(name="termo_aceite_0", type="integer", nullable=true)
     */
    private $termoAceite0;

    /**
     * @var bool|null
     *
     * @Column(name="termo_aceite_1", type="boolean", nullable=true)
     */
    private $termoAceite1;

    /**
     * @var bool|null
     *
     * @Column(name="termo_aceite_2", type="boolean", nullable=true)
     */
    private $termoAceite2;

    /**
     * @var bool|null
     *
     * @Column(name="termo_aceite_3", type="boolean", nullable=true)
     */
    private $termoAceite3;

    /**
     * @var int|null
     *
     * @Column(name="status", type="integer", nullable=true, options={"default"="1"})
     */
    private $status;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_criacao", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataCriacao;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_solicitacao_filiacao", type="date", nullable=true)
     */
    private $dataSolicitacaoFiliacao;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_solicitacao_refiliacao", type="date", nullable=true)
     */
    private $dataSolicitacaoRefiliacao;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_filiacao", type="date", nullable=true)
     */
    private $dataFiliacao;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_desfiliacao", type="date", nullable=true)
     */
    private $dataDesfiliacao;

    /**
     * @var string|null
     *
     * @Column(name="motivo_desfiliacao", type="text", length=65535, nullable=true)
     */
    private $motivoDesfiliacao;

    /**
     * @var string|null
     *
     * @Column(name="reclassificacao_desfiliacao", type="text", length=65535, nullable=true)
     */
    private $reclassificacaoDesfiliacao;

    /**
     * @var string|null
     *
     * @Column(name="descricao_desfiliacao", type="text", length=65535, nullable=true)
     */
    private $descricaoDesfiliacao;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_refiliacao", type="date", nullable=true)
     */
    private $dataRefiliacao;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_cancelamento_pedido", type="date", nullable=true)
     */
    private $dataCancelamentoPedido;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_impugnacao", type="date", nullable=true)
     */
    private $dataImpugnacao;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_validade_ativo", type="date", nullable=true)
     */
    private $dataValidadeAtivo;

    /**
     * @var bool|null
     *
     * @Column(name="ativo_ano", type="boolean", nullable=true)
     */
    private $ativoAno;

    /**
     * @var \DateTime|null
     *
     * @Column(name="cron_ativo_ano", type="datetime", nullable=true)
     */
    private $cronAtivoAno;

    /**
     * @var bool
     *
     * @Column(name="doador", type="boolean", nullable=false)
     */
    private $doador = '0';

    /**
     * @var int|null
     *
     * @Column(name="filiado", type="integer", nullable=true)
     */
    private $filiado = '0';

    /**
     * @var bool|null
     *
     * @Column(name="bloqueado", type="boolean", nullable=true)
     */
    private $bloqueado;

    /**
     * @var int|null
     *
     * @Column(name="tb_motivo_id", type="integer", nullable=true)
     */
    private $tbMotivoId;

    /**
     * @var string|null
     *
     * @Column(name="observacao", type="string", length=4000, nullable=true)
     */
    private $observacao;

    /**
     * @var string|null
     *
     * @Column(name="token_ad", type="string", length=255, nullable=true)
     */
    private $tokenAd;

    /**
     * @var bool|null
     *
     * @Column(name="candidatura", type="boolean", nullable=true)
     */
    private $candidatura = '0';

    /**
     * @var bool|null
     *
     * @Column(name="email_confirmado", type="boolean", nullable=true)
     */
    private $emailConfirmado = '0';

    /**
     * @var bool|null
     *
     * @Column(name="pendencia_titulo", type="boolean", nullable=true)
     */
    private $pendenciaTitulo = '0';

    /**
     * @var string|null
     *
     * @Column(name="ip", type="string", length=100, nullable=true)
     */
    private $ip;

    /**
     * @var bool|null
     *
     * @Column(name="suspensao", type="boolean", nullable=true, options={"default"="0"})
     */
    private $suspensao = '0';

    /**
     * @var bool|null
     *
     * @Column(name="membro_diretorio", type="boolean", nullable=true, options={"default"="0"})
     */
    private $membro_diretorio = '0';

    /**
     * @var bool
     *
     * @Column(name="permissionario", type="boolean", nullable=false)
     */
    private $permissionario = '0';

    /**
     * @var bool
     *
     * @Column(name="irpf", type="boolean", nullable=false)
     */
    private $irpf = '0';

    /**
     * @var bool
     *
     * @Column(name="exemption", type="boolean", nullable=false)
     */
    private $exemption = '0';

    /**
     * @var \DateTime|null
     *
     * @Column(name="ultima_atualizacao", type="datetime", nullable=true)
     */
    private $ultimaAtualizacao;

    /**
     * @var int
     *
     * @Column(name="indicator", type="integer", nullable=true)
     */
    private $indicator;

    /**
     * @var int
     *
     * @Column(name="vndaId", type="integer", nullable=true)
     */
    private $vndaId;

    /**
     * @var int
     *
     * @Column(name="monthlyBill", type="boolean", nullable=false)
     */
    private $monthlyBill = '0';

    public function __construct()
    {
        $this->tokenAd = Utils::generateToken(22) . md5(date('Y-m-d H:i:s'));
    }

    public function toArray(): array
    {
        $array = [];
        $array['id'] = $this->getId();
        $array['name'] = $this->getName();
        $array['voterTitleUF'] = $this->getTituloEleitoralUf();
        $array['voterTitleCity'] = $this->getTituloEleitoralMunicipio();
        $array['voterTitleNumber'] = $this->getTituloEleitoral();
        return $array;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getToken2(bool $encoded = true): string
    {
        if ($encoded) return base64_encode($this->id) . ':' . base64_encode($this->token2);
        return $this->token2;
    }

    public function setToken2(string $token2): User
    {
        $this->token2 = $token2;
        return $this;
    }


    public function getFiliadoString()
    {
        switch ($this->getFiliado()) {
            case 0:
                return 'Não';
            case 1:
                return 'Pendente de Pgto Filiação';
            case 2:
                return 'Período Impugnação Filiação';
            case 3:
                return 'Pendente de Pgto Refiliação';
            case 4:
                return 'Impugnação Solicitada';
            case 5:
                return 'Solicitação Cancelada';
            case 6:
                return 'Impugnado';
            case 7:
                return 'Aprovado';
            case 8:
                return 'Aprovado - Refiliação';
            case 9:
                return 'Desfiliado';
            case 10:
                return 'Desfiliado - Falecimento';
            case 11:
                return 'Desfiliado - Expulsão';
            case 12:
                return 'Desfiliado - Falta de Pagamento';
            case 13:
                return 'Filiação Suspensa por Pendência Titulo';
            case 14:
                return 'Período de Impugnação Refiliação';
            default:
                return 'Desconhecido';
        }
    }

    public function getFiliadoId(): ?int
    {
        return $this->filiadoId;
    }

    public function getFirstName(): string
    {
        return explode(' ', $this->getName())[0];
    }

    public function getSurname(): string
    {
        $name = explode(' ', $this->getName());
        return end($name);
    }

    public function setFiliadoId(?int $filiadoId): User
    {
        $this->filiadoId = $filiadoId;
        return $this;
    }

    public function getPaymentPlan(): ?int
    {
        return $this->paymentPlan;
    }

    public function getPaymentPlanStr(): string
    {
        switch ($this->name) {
            case 2:
                return 'Plano laranja';
                break;
            case 3:
                return 'Plano ouro';
                break;
            case 4:
                return 'Plano diamante';
                break;
            default:
                return 'Plano zero';
                break;
        }
    }

    public function setPaymentPlan(?int $paymentPlan): User
    {
        $this->paymentPlan = $paymentPlan;
        return $this;
    }


    public function getDatePlan(): ?\DateTime
    {
        return $this->datePlan;
    }

    public function setDatePlan(?\DateTime $datePlan): User
    {
        $this->datePlan = $datePlan;
        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): User
    {
        $this->token = $token;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getEmailCustom(): ?string
    {
        $email = substr_replace($this->email, '*****', 1, strpos($this->email, '@') - 2);
        return $email;
    }

    public function setEmail(?string $email): User
    {
        $this->email = strtolower($email);
        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->senha;
    }

    public function setPassword(?string $senha): User
    {
        $this->senha = $senha;
        return $this;
    }

    public function getName(): ?string
    {
        return rtrim(Utils::firstUpper($this->nome));
    }

    public function setName(?string $nome): User
    {
        $this->nome = $nome;
        return $this;
    }

    public function getCpf(): ?string
    {
        return $this->cpf;
    }

    public function setCpf(?string $cpf): User
    {
        $this->cpf = $cpf;
        return $this;
    }

    public function getRg(): ?string
    {
        return $this->rg;
    }

    public function setRg(?string $rg): User
    {
        $this->rg = $rg;
        return $this;
    }

    public function getOrgaoExpedidor(): ?string
    {
        return $this->orgaoExpedidor;
    }

    public function setOrgaoExpedidor(?string $orgaoExpedidor): User
    {
        $this->orgaoExpedidor = $orgaoExpedidor;
        return $this;
    }

    public function getDataNascimento(): ?\DateTime
    {
        return $this->dataNascimento;
    }

    public function setDataNascimento(?\DateTime $dataNascimento): User
    {
        $this->dataNascimento = $dataNascimento;
        return $this;
    }

    public function getTituloEleitoral(): ?string
    {
        return $this->tituloEleitoral;
    }

    public function setTituloEleitoral(?string $tituloEleitoral): User
    {
        $this->tituloEleitoral = $tituloEleitoral;
        return $this;
    }

    public function getTituloEleitoralZona(): ?string
    {
        return $this->tituloEleitoralZona;
    }

    public function setTituloEleitoralZona(?string $tituloEleitoralZona): User
    {
        $this->tituloEleitoralZona = $tituloEleitoralZona;
        return $this;
    }

    public function getTituloEleitoralSecao(): ?string
    {
        return $this->tituloEleitoralSecao;
    }

    public function setTituloEleitoralSecao(?string $tituloEleitoralSecao): User
    {
        $this->tituloEleitoralSecao = $tituloEleitoralSecao;
        return $this;
    }

    public function getTituloEleitoralPais(): ?string
    {
        return $this->tituloEleitoralPais;
    }

    public function setTituloEleitoralPais(?string $tituloEleitoralPais): User
    {
        $this->tituloEleitoralPais = $tituloEleitoralPais;
        return $this;
    }

    public function getTituloEleitoralPaisId(): ?Country
    {
        return $this->tituloEleitoralPaisId;
    }

    public function setTituloEleitoralPaisId(?Country $tituloEleitoralPaisId): User
    {
        $this->tituloEleitoralPaisId = $tituloEleitoralPaisId;
        return $this;
    }

    public function getTituloEleitoralUf(): ?string
    {
        return $this->tituloEleitoralUf;
    }

    public function setTituloEleitoralUf(?string $tituloEleitoralUf): User
    {
        $this->tituloEleitoralUf = $tituloEleitoralUf;
        return $this;
    }

    public function getTituloEleitoralUfId(): ?State
    {
        return $this->tituloEleitoralUfId;
    }

    public function setTituloEleitoralUfId(?State $tituloEleitoralUfId): User
    {
        $this->tituloEleitoralUfId = $tituloEleitoralUfId;
        return $this;
    }

    public function getTituloEleitoralMunicipio(): ?string
    {
        return $this->tituloEleitoralMunicipio;
    }

    public function setTituloEleitoralMunicipio(?string $tituloEleitoralMunicipio): User
    {
        $this->tituloEleitoralMunicipio = $tituloEleitoralMunicipio;
        return $this;
    }

    public function getTituloEleitoralMunicipioId(): ?City
    {
        return $this->tituloEleitoralMunicipioId;
    }

    public function setTituloEleitoralMunicipioId(?City $tituloEleitoralMunicipioId): User
    {
        $this->tituloEleitoralMunicipioId = $tituloEleitoralMunicipioId;
        return $this;
    }

    public function getNomeMae(): ?string
    {
        return $this->nomeMae;
    }

    public function setNomeMae(?string $nomeMae): User
    {
        $this->nomeMae = $nomeMae;
        return $this;
    }

    public function getEstadoCivil(): CivilState
    {
        return $this->estadoCivil;
    }

    public function setEstadoCivil(CivilState $estadoCivil): User
    {
        $this->estadoCivil = $estadoCivil;
        return $this;
    }

    public function getGenero(): Gender
    {
        return $this->genero;
    }

    public function setGenero(Gender $genero): User
    {
        $this->genero = $genero;
        return $this;
    }

    public function getEscolaridade(): Schooling
    {
        return $this->escolaridade;
    }

    public function setEscolaridade(Schooling $escolaridade): User
    {
        $this->escolaridade = $escolaridade;
        return $this;
    }

    public function getOpcaoReligiosa(): ?bool
    {
        return $this->opcaoReligiosa;
    }

    public function setOpcaoReligiosa(?bool $opcaoReligiosa): User
    {
        $this->opcaoReligiosa = $opcaoReligiosa;
        return $this;
    }

    public function getProfissao(): ?string
    {
        return $this->profissao;
    }

    public function setProfissao(?string $profissao): User
    {
        $this->profissao = $profissao;
        return $this;
    }

    public function getFiliacao(): int
    {
        return $this->filiacao;
    }

    public function setFiliacao(int $filiacao): User
    {
        $this->filiacao = $filiacao;
        return $this;
    }

    public function getFiliacaoQualPartido(): ?string
    {
        return $this->filiacaoQualPartido;
    }

    public function setFiliacaoQualPartido(?string $filiacaoQualPartido): User
    {
        $this->filiacaoQualPartido = $filiacaoQualPartido;
        return $this;
    }

    public function getTermoAceite0(): ?int
    {
        return $this->termoAceite0;
    }

    public function setTermoAceite0(?int $termoAceite0): User
    {
        $this->termoAceite0 = $termoAceite0;
        return $this;
    }

    public function getTermoAceite1(): ?bool
    {
        return $this->termoAceite1;
    }

    public function setTermoAceite1(?bool $termoAceite1): User
    {
        $this->termoAceite1 = $termoAceite1;
        return $this;
    }

    public function getTermoAceite2(): ?bool
    {
        return $this->termoAceite2;
    }

    public function setTermoAceite2(?bool $termoAceite2): User
    {
        $this->termoAceite2 = $termoAceite2;
        return $this;
    }

    public function getTermoAceite3(): ?bool
    {
        return $this->termoAceite3;
    }

    public function setTermoAceite3(?bool $termoAceite3): User
    {
        $this->termoAceite3 = $termoAceite3;
        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function getStatusString(): string
    {
        $status = match ($this->status) {
            1 => 'Etapa 1 - Dados Básicos',
            2 => 'Etapa 2 - CPF',
            3 => 'Etapa 3 - Endereço',
            4 => 'Etapa 4 - Titulo Eleitor',
            5 => 'Etapa 5 - Termos',
            6 => 'Etapa 6 - IRPF',
            7 => 'Etapa 7 - Espaço NOVO',
            8 => 'Etapa 8 - Pagamento - Erro',
            9 => 'Etapa 9 - Pagamento - Sucesso',
            11 => 'Período de Impugnação',
            12 => 'Filiação Sucesso',
            default => 'Etapa 0 - Sem Status'
        };
        return $status;
    }

    public function setStatus(?int $status): User
    {
        $this->status = $status;
        return $this;
    }

    public function getDataCriacao(): ?\DateTime
    {
        return $this->dataCriacao;
    }

    public function setDataCriacao(?\DateTime $dataCriacao): User
    {
        $this->dataCriacao = $dataCriacao;
        return $this;
    }

    public function getDataSolicitacaoFiliacao(): ?\DateTime
    {
        return $this->dataSolicitacaoFiliacao;
    }

    public function setDataSolicitacaoFiliacao(?\DateTime $dataSolicitacaoFiliacao): User
    {
        $this->dataSolicitacaoFiliacao = $dataSolicitacaoFiliacao;
        return $this;
    }

    public function getDataSolicitacaoRefiliacao(): ?\DateTime
    {
        return $this->dataSolicitacaoRefiliacao;
    }

    public function setDataSolicitacaoRefiliacao(?\DateTime $dataSolicitacaoRefiliacao): User
    {
        $this->dataSolicitacaoRefiliacao = $dataSolicitacaoRefiliacao;
        return $this;
    }

    public function getDataFiliacao(): ?\DateTime
    {
        return $this->dataFiliacao;
    }

    public function setDataFiliacao(?\DateTime $dataFiliacao): User
    {
        $this->dataFiliacao = $dataFiliacao;
        return $this;
    }

    public function getDataDesfiliacao(): ?\DateTime
    {
        return $this->dataDesfiliacao;
    }

    public function setDataDesfiliacao(?\DateTime $dataDesfiliacao): User
    {
        $this->dataDesfiliacao = $dataDesfiliacao;
        return $this;
    }

    public function getMotivoDesfiliacao(): ?string
    {
        return $this->motivoDesfiliacao;
    }

    public function setMotivoDesfiliacao(?string $motivoDesfiliacao): User
    {
        $this->motivoDesfiliacao = $motivoDesfiliacao;
        return $this;
    }

    public function getReclassificacaoDesfiliacao(): ?string
    {
        return $this->reclassificacaoDesfiliacao;
    }

    public function setReclassificacaoDesfiliacao(?string $reclassificacaoDesfiliacao): User
    {
        $this->reclassificacaoDesfiliacao = $reclassificacaoDesfiliacao;
        return $this;
    }

    public function getDescricaoDesfiliacao(): ?string
    {
        return $this->descricaoDesfiliacao;
    }

    public function setDescricaoDesfiliacao(?string $descricaoDesfiliacao): User
    {
        $this->descricaoDesfiliacao = $descricaoDesfiliacao;
        return $this;
    }

    public function getDataRefiliacao(): ?\DateTime
    {
        return $this->dataRefiliacao;
    }

    public function setDataRefiliacao(?\DateTime $dataRefiliacao): User
    {
        $this->dataRefiliacao = $dataRefiliacao;
        return $this;
    }

    public function getDataCancelamentoPedido(): ?\DateTime
    {
        return $this->dataCancelamentoPedido;
    }

    public function setDataCancelamentoPedido(?\DateTime $dataCancelamentoPedido): User
    {
        $this->dataCancelamentoPedido = $dataCancelamentoPedido;
        return $this;
    }

    public function getDataImpugnacao(): ?\DateTime
    {
        return $this->dataImpugnacao;
    }

    public function setDataImpugnacao(?\DateTime $dataImpugnacao): User
    {
        $this->dataImpugnacao = $dataImpugnacao;
        return $this;
    }

    public function getDataValidadeAtivo(): ?\DateTime
    {
        return $this->dataValidadeAtivo;
    }

    public function setDataValidadeAtivo(?\DateTime $dataValidadeAtivo): User
    {
        $this->dataValidadeAtivo = $dataValidadeAtivo;
        return $this;
    }

    public function getAtivoAno(): ?bool
    {
        return $this->ativoAno;
    }

    public function setAtivoAno(?bool $ativoAno): User
    {
        $this->ativoAno = $ativoAno;
        return $this;
    }

    public function getCronAtivoAno(): ?\DateTime
    {
        return $this->cronAtivoAno;
    }

    public function setCronAtivoAno(?\DateTime $cronAtivoAno): User
    {
        $this->cronAtivoAno = $cronAtivoAno;
        return $this;
    }

    public function isDoador(): bool
    {
        return $this->doador;
    }

    public function setDoador(bool $doador): User
    {
        $this->doador = $doador;
        return $this;
    }

    public function getFiliado(): ?int
    {
        return $this->filiado;
    }

    public function setFiliado(?int $filiado): User
    {
        $this->filiado = $filiado;
        return $this;
    }

    public function getBloqueado(): ?bool
    {
        return $this->bloqueado;
    }

    public function setBloqueado(?bool $bloqueado): User
    {
        $this->bloqueado = $bloqueado;
        return $this;
    }

    public function getTbMotivoId(): ?int
    {
        return $this->tbMotivoId;
    }

    public function setTbMotivoId(?int $tbMotivoId): User
    {
        $this->tbMotivoId = $tbMotivoId;
        return $this;
    }

    public function getObservacao(): ?string
    {
        return $this->observacao;
    }

    public function setObservacao(?string $observacao): User
    {
        $this->observacao = $observacao;
        return $this;
    }

    public function getTokenAd(): ?string
    {
        return $this->tokenAd;
    }

    public function setTokenAd(?string $tokenAd): User
    {
        $this->tokenAd = $tokenAd;
        return $this;
    }

    public function getCandidatura(): ?bool
    {
        return $this->candidatura;
    }

    public function setCandidatura(?bool $candidatura): User
    {
        $this->candidatura = $candidatura;
        return $this;
    }

    public function getEmailConfirmado(): ?bool
    {
        return $this->emailConfirmado;
    }

    public function setEmailConfirmado(?bool $emailConfirmado): User
    {
        $this->emailConfirmado = $emailConfirmado;
        return $this;
    }

    public function getPendenciaTitulo(): ?bool
    {
        return (bool)$this->pendenciaTitulo;
    }

    public function setPendenciaTitulo(?bool $pendenciaTitulo): User
    {
        $this->pendenciaTitulo = $pendenciaTitulo;
        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(?string $ip): User
    {
        $this->ip = $ip;
        return $this;
    }

    public function getSuspensao(): ?bool
    {
        return $this->suspensao;
    }

    public function setSuspensao(?bool $suspensao): User
    {
        $this->suspensao = $suspensao;
        return $this;
    }

    public function affiliatedDateDays(): int
    {
        $today = new \DateTime();
        switch ($this->getFiliado()) {
            case 7:
                return $this->dataFiliacao ? $today->diff($this->dataFiliacao)->days : 0;
            case 8:
                return $this->dataRefiliacao ? $today->diff($this->dataRefiliacao)->days : 0;
            default:
                return 0;
        }
    }

    public function affiliatedDate(): string
    {
        switch ($this->getFiliado()) {
            case 7:
                return $this->dataFiliacao ? $this->dataFiliacao->format('d/m/Y') : '';
            case 8:
                return $this->dataRefiliacao ? $this->dataRefiliacao->format('d/m/Y') : '';
            default:
                return '';
        }
    }

    public function isAffiliated(bool $redirect = false): bool
    {
        $isAffiliated = in_array($this->getFiliado(), [7, 8]);

        if ($redirect && !$isAffiliated) {
            Session::set('errorMsg', 'Você não tem permissão para acessar esse conteúdo');
            header('Location: ' . BASEURL);
            die();
        }
        return $isAffiliated;
    }

    public function liberarMobiliza(bool $redirect = false): bool
    {
        $isAffiliated = in_array($this->getFiliado(), [7, 8]);
        $isPlan = in_array($this->getPaymentPlan(), [2, 3, 4]);//laranja, ouro ou diamante
        if ($redirect && (!$isAffiliated && !$isPlan)) {
            Session::set('errorMsg', 'Você não tem permissão para acessar esse conteúdo');
            header('Location: ' . BASEURL);
            die();
        }
        return $isAffiliated || $isPlan;
    }

    public function getMembroDiretorio(): ?bool
    {
        return $this->membro_diretorio;
    }

    public function setMembroDiretorio(?bool $membro_diretorio): User
    {
        $this->membro_diretorio = $membro_diretorio;
        return $this;
    }

    public function getPermissionario(): bool
    {
        return $this->permissionario;
    }

    public function setPermissionario(bool $permissionario): User
    {
        $this->permissionario = $permissionario;
        return $this;
    }

    public function getIrpf(): ?bool
    {
        return $this->irpf;
    }

    public function setIrpf(?bool $irpf): User
    {
        $this->irpf = $irpf;
        return $this;
    }

    public function getExemption(): ?bool
    {
        return $this->exemption;
    }

    public function setExemption(?bool $exemption): User
    {
        $this->exemption = $exemption;
        return $this;
    }

    public function getUltimaAtualizacao(): ?\Datetime
    {
        return $this->ultimaAtualizacao;
    }

    public function setUltimaAtualizacao(?\Datetime $ultimaAtualizacao): User
    {
        $this->ultimaAtualizacao = $ultimaAtualizacao;
        return $this;
    }

    public function getIndicator(): ?int
    {
        return $this->indicator;
    }

    public function setIndicator(int $indicator): User
    {
        $this->indicator = $indicator;
        return $this;
    }

    public function getVndaId(): ?int
    {
        return $this->vndaId;
    }

    public function setVndaId(int $vndaId): User
    {
        $this->vndaId = $vndaId;
        return $this;
    }

    public function getMonthlyBill(): int
    {
        return $this->monthlyBill;
    }

    public function setMonthlyBill(bool $monthlyBill): User
    {
        $this->monthlyBill = $monthlyBill;
        return $this;
    }

    public function getPoints(): UserPoint
    {
        return $this->points;
    }
}
