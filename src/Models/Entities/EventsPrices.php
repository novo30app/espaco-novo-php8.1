<?php

namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\ORM\Mapping as ORM;

/**
 * EventsPrices
 *
 * @Entity @Table(name="tb_eventos_precos")
 * @ORM @Entity(repositoryClass="App\Models\Repository\EventsPricesRepository")
 */
class EventsPrices
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @Column(name="tb_eventos_id", type="integer", nullable=false)
     */
    private $tbEventosId;

    /**
     * @var string|null
     *
     * @Column(name="descricao", type="string", length=255, nullable=true)
     */
    private $descricao;

    /**
     * @Column(type="decimal", name="valor", precision=13, scale=4, nullable=true)
     * @var float
     */
    private $valor;

    /**
     * @Column(type="datetime", name="data_inicio", nullable=true)
     * @var string
     */
    private $dataInicio;

    /**
     * @Column(type="datetime", name="data_termino", nullable=true)
     * @var string
     */
    private $dataTermino;

    /**
     * @var int|null
     *
     * @Column(name="limite_inscritos", type="integer", nullable=true)
     */
    private $limiteInscritos;

    /**
     * @var int|null
     *
     * @Column(name="limite_total", type="integer", nullable=true)
     */
    private $limiteTotal;

    /**
     * @var int|null
     *
     * @Column(name="apenas_filiados", type="integer", nullable=true)
     */
    private $apenasFiliados;

    /**
     * @var string|null
     *
     * @Column(name="status", type="string", length=25, nullable=true)
     */
    private $status;

    public function getId(): int
    {
        return $this->id;
    }

    public function getTbEventsId(): int
    {
        return $this->tbEventosId;
    }

    public function setTbEventsId(int $tbEventosId): EventsPrices
    {
        $this->tbEventosId = $tbEventosId;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->descricao;
    }

    public function setDescription(?string $descricao): EventsPrices
    {
        $this->descricao = $descricao;
        return $this;
    }

    public function getValue(): float
    {
        return $this->valor;
    }

    public function setValue(float $valor): EventsPrices
    {
        $this->valor = $valor;
        return $this;
    }

    public function getDatBegin(): ?\DateTime
    {
        return $this->dataInicio;
    }

    public function setDateBegin(?\DateTime $dataInicio): EventsPrices
    {
        $this->dataInicio = $dataInicio;
        return $this;
    }

    public function getDateFinal(): ?\DateTime
    {
        return $this->dataTermino;
    }

    public function setDateFinal(?\DateTime $dataTermino): EventsPrices
    {
        $this->dataTermino = $dataTermino;
        return $this;
    }

    public function getLimitParticipants(): ?int
    {
        return $this->limiteInscritos;
    }

    public function setLimitParticipants(?int $limiteInscritos): EventsPrices
    {
        $this->limiteInscritos = $limiteInscritos;
        return $this;
    }

    public function getLimitTotal(): ?int
    {
        return $this->limiteTotal;
    }

    public function setLimitTotal(?int $limiteTotal): EventsPrices
    {
        $this->limiteTotal = $limiteTotal;
        return $this;
    }

    public function getOnlyAffiliated(): ?int
    {
        return $this->apenasFiliados;
    }

    public function setOnlyAffiliated(?int $apenasFiliados): EventsPrices
    {
        $this->apenasFiliados = $apenasFiliados;
        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): EventsPrices
    {
        $this->status = $status;
        return $this;
    }
}