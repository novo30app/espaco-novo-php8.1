<?php

namespace App\Models\Entities;

/**
 * @Entity(repositoryClass="App\Models\Repository\HistoricRepository")
 * @Table(name="historic")
 * @ORM 
 */

 class Historic
 {
   /**
    * @Id
    * @GeneratedValue 
    * @Column(type="integer")
    */
    private $id;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user", referencedColumnName="id")
     */
    private User $user;

    /**
     * @Column(type="integer")
     */
    private $type;

    /**
     * @Column(type="datetime")
     */
    private \DateTime $date;

    public function getId(): int
    {
        return $this->id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): Historic
    {
        $this->user = $user;
        return $this;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function getTypeString(): string
    {
        switch ($this->type) {
            case 1:
                $type = "Cria Cadastro";
                break;
            case 2:
                $type = "Solicita Filiação";
                break;
            case 3:
                $type = "Data Filiação";
                break;
            case 4:
                $type = "Solicita Desfiliação";
                break;
            case 5:
                $type = "Solicita Refiliação";
                break;
            case 6:
                $type = "Data Refiliação";
                break;
            case 7: 
                $type = "Impugnação";
                break;
            case 8:
                $type = "Suspensão";
                break;
            case 9:
                $type = "Cancelamento de pedido de filiação/refiliação";
                break;
            default:
                $type = '';
                break;
        }
        return $type;
    }

    public function setType(int $type): Historic
    {
        $this->type = $type;
        return $this;
    }

    public function getDate(): \DateTime
    {
        return $this->date;
    }

    public function setDate(\DateTime $date): Historic
    {
        $this->date = $date;
        return $this;
    }
 }