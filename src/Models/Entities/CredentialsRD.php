<?php

namespace App\Models\Entities;
use Doctrine\ORM\EntityRepository;
use Doctrine\Mapping as ORM;

/**
 * @Entity @Table(name="credentialsRD")
 * @ORM @Entity(repositoryClass="App\Models\Repository\CredentialsRDRepository")
 */
class CredentialsRD
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private int $id;

    /**
     * @Column(type="string")
     */
    private string $access_token;

    /**
     * @Column(type="string")
     */
    private string $refresh_token;

    /**
     * @var \DateTime|null
     *
     * @Column(name="date", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $date;

    public function getId(): int
    {
        return $this->id;
    }

    public function getAccessToken(): string
    {
        return $this->access_token;
    }

    public function setAccessToken($access_token): CredentialsRD
    {
        $this->access_token = $access_token;
        return $this;
    }

    public function getRefreshToken(): string
    {
        return $this->refresh_token;
    }

    public function setRefreshToken($refresh_token): CredentialsRD
    {
        $this->refresh_token = $refresh_token;
        return $this;
    }

    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    public function setDate(?\DateTime $date): CredentialsRD
    {
        $this->date = $date;
        return $this;
    }
}