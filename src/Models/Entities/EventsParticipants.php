<?php

namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\ORM\Mapping as ORM;

/**
 * EventsParticipants
 *
 * @Entity @Table(name="tb_eventos_participantes")
 * @ORM @Entity(repositoryClass="App\Models\Repository\EventsParticipantsRepository")
 */
class EventsParticipants
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @Column(name="tb_eventos_id", type="integer", nullable=true)
     */
    private $tbEventosId;

    /**
     * @var int|null
     *
     * @Column(name="tb_eventos_precos_id", type="integer", nullable=true)
     */
    private $tbEventosPrecosId;

    /**
     * @var int|null
     *
     * @Column(name="tb_eventos_transacao_id", type="integer", nullable=true)
     */
    private $tbEventosTransacaoId;

    /**
     * @var int|null
     *
     * @Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var string|null
     *
     * @Column(name="nome", type="string", nullable=true)
     */
    private $nome;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_nascimento", type="date", nullable=true)
     */
    private $dataNascimento;

    /**
     * @var string|null
     *
     * @Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @Column(name="cpf", type="string", length=25, nullable=true)
     */
    private $cpf;

    /**
     * @var int
     *
     * @Column(name="telefone_numero", type="string", nullable=false)
     */
    private $telefoneNumero;

    /**
     * @var int|null
     *
     * @Column(name="tb_estado_id", type="integer", nullable=true)
     */
    private $tbEstadoId;

    /**
     * @var string|null
     *
     * @Column(name="estado", type="string", length=255, nullable=true)
     */
    private $estado;

    /**
     * @var int|null
     *
     * @Column(name="tb_cidade_id", type="integer", nullable=true)
     */
    private $tbCidadeId;

    /**
     * @var string|null
     *
     * @Column(name="cidade", type="string", length=255, nullable=true)
     */
    private $cidade;

    /**
     * @var int|null
     *
     * @Column(name="cep", type="integer", nullable=true)
     */
    private $cep;

    /**
     * @var string|null
     *
     * @Column(name="endereco", type="string", length=150, nullable=true)
     */
    private $endereco;

    /**
     * @var string|null
     *
     * @Column(name="numero", type="string", length=6, nullable=true)
     */
    private $numero;

    /**
     * @var string|null
     *
     * @Column(name="bairro", type="string", length=45, nullable=true)
     */
    private $bairro;

    /**
     * @var string|null
     *
     * @Column(name="code", type="string", length=45, nullable=true)
     */
    private $code;

    /**
     * @Column(type="decimal", name="valor", precision=13, scale=4, nullable=true)
     * @var float
     */
    private $valor;

    /**
     * @var string|null
     *
     * @Column(name="status", type="string", length=45, nullable=true)
     */
    private $status;

    /**
     * @var string|null
     *
     * @Column(name="checkin_hash", type="string", length=45, nullable=true)
     */
    private $checkinHash;

    /**
     * @Column(type="datetime", name="data_criacao", nullable=true)
     * @var string
     */
    private $dataCriacao;

    /**
     * @Column(type="datetime", name="data_checkin", nullable=true)
     * @var string
     */
    private $dataCheckin;

    /**
     * @var string|null
     *
     * @Column(name="recommendation", type="string", length=155, nullable=true)
     */
    private $recommendation;

    public function getId(): int
    {
        return $this->id;
    }

    public function getTbEventsId(): int
    {
        return $this->tbEventosId;
    }

    public function setTbEventsId(int $tbEventosId): EventsParticipants
    {
        $this->tbEventosId = $tbEventosId;
        return $this;
    }

    public function getTbEventsPricesId(): int
    {
        return $this->tbEventosPrecosId;
    }

    public function setTbEventsPricesId(int $tbEventosPrecosId): EventsParticipants
    {
        $this->tbEventosPrecosId = $tbEventosPrecosId;
        return $this;
    }

    public function getTbEventsTransactionId(): int
    {
        return $this->tbEventosTransacaoId;
    }

    public function setTbEventsTransactionId(int $tbEventosTransacaoId): EventsParticipants
    {
        $this->tbEventosTransacaoId = $tbEventosTransacaoId;
        return $this;
    }

    public function getTbPessoaId(): int
    {
        return $this->tbPessoaId;
    }

    public function setTbPessoaId(int $tbPessoaId): EventsParticipants
    {
        $this->tbPessoaId = $tbPessoaId;
        return $this;
    }

    public function getName(): ?string
    {
        return  rtrim(Utils::firstUpper($this->nome));
    }

    public function setName(?string $nome): EventsParticipants
    {
        $this->nome = $nome;
        return $this;
    }

    public function getDateBirth(): ?\DateTime
    {
        return $this->dataNascimento;
    }

    public function setDateBirth(?\DateTime $dataNascimento): EventsParticipants
    {
        $this->dataNascimento = $dataNascimento;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): EventsParticipants
    {
        $this->email = $email;
        return $this;
    }

    public function getCpf(): ?string
    {
        return $this->cpf;
    }

    public function setCpf(?string $cpf): EventsParticipants
    {
        $this->cpf = $cpf;
        return $this;
    }

    public function getPhone(bool $onlyNumbers = false): ?string
    {
        if ($onlyNumbers){
            return Utils::onlyNumbers($this->telefoneNumero);
        }
        return $this->telefoneNumero;
    }

    public function setPhone(?string $telefoneNumero): EventsParticipants
    {
        $this->telefoneNumero = $telefoneNumero;
        return $this;
    }

    public function getStateId(): ?int
    {
        return $this->tbEstadoId;
    }

    public function setStateId(?int $tbEstadoId): EventsParticipants
    {
        $this->tbEstadoId = $tbEstadoId;
        return $this;
    }

    public function getState(): ?string
    {
        return $this->estado;
    }

    public function setState(?string $estado): EventsParticipants
    {
        $this->estado = $estado;
        return $this;
    }

    public function getCityId(): ?int
    {
        return $this->tbCidadeId;
    }

    public function setCityId(?int $tbCidadeId): EventsParticipants
    {
        $this->tbCidadeId = $tbCidadeId;
        return $this;
    }

    public function getCity(): ?string
    {
        return $this->cidade;
    }

    public function setCity(?string $cidade): EventsParticipants
    {
        $this->cidade = $cidade;
        return $this;
    }

    public function getCep(): ?string
    {
        return $this->cep;
    }

    public function setCep(?string $cep): EventsParticipants
    {
        $this->cep = $cep;
        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->endereco;
    }

    public function setAddress(?string $endereco): EventsParticipants
    {
        $this->endereco = $endereco;
        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->numero;
    }

    public function setNumber(?string $numero): EventsParticipants
    {
        $this->numero = $numero;
        return $this;
    }

    public function getDistrict(): ?string
    {
        return $this->bairro;
    }

    public function setDistrict(?string $bairro): EventsParticipants
    {
        $this->bairro = $bairro;
        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): EventsParticipants
    {
        $this->code = $code;
        return $this;
    }

    public function getValue(): float
    {
        return $this->valor;
    }

    public function setValue(float $valor): EventsParticipants
    {
        $this->valor = $valor;
        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): EventsParticipants
    {
        $this->status = $status;
        return $this;
    }

    public function getCheckinHash(): ?string
    {
        return $this->checkinHash;
    }

    public function setCheckinHash(?string $checkinHash): EventsParticipants
    {
        $this->checkinHash = $checkinHash;
        return $this;
    }

    public function getDateCreated(): ?\DateTime
    {
        return $this->dataCriacao;
    }

    public function setDateCreated(?\DateTime $dataCriacao): EventsParticipants
    {
        $this->dataCriacao = $dataCriacao;
        return $this;
    }

    public function getDataCheckin(): ?\DateTime
    {
        return $this->dataCheckin;
    }

    public function setDataCheckin(?\DateTime $dataCheckin): EventsParticipants
    {
        $this->dataCheckin = $dataCheckin;
        return $this;
    }

    public function getRecommendation(): ?string
    {
        return $this->recommendation;
    }

    public function setRecommendation(?string $recommendation): EventsParticipants
    {
        $this->recommendation = $recommendation;
        return $this;
    }
}