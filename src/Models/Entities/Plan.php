<?php

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="plans")
 */
class Plan
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="string")
     */
    private string $name = '';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getNameStr(): string
    {
        switch ($this->id) {
            case 2:
                return 'Plano laranja';
                break;
            case 3:
                return 'Plano ouro';
                break;
            case 4:
                return 'Plano diamante';
                break;        
            default:
                return 'Plano zero';
                break;
        }
    }

    public function setName(string $name): Plan
    {
        $this->name = $name;
        return $this;
    }
}