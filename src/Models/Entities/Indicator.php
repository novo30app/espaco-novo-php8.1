<?php

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="indicator")
 * @ORM @Entity(repositoryClass="App\Models\Repository\IndicatorRepository")
 */
class Indicator
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user", referencedColumnName="id", nullable=true)
     */
    private User $user;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="indicator", referencedColumnName="id", nullable=true)
     */
    private User $indicator;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $origin;

    /**
     * @ManyToOne(targetEntity="Events")
     * @JoinColumn(name="eventId", referencedColumnName="id", nullable=true)
     */
    private Events $eventId;

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreated_at(): \DateTime
    {
        return $this->created_at;
    }

    public function setCreated_at(\DateTime $created_at): Indicator
    {
        $this->created_at = $created_at;
        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): Indicator
    {
        $this->user = $user;
        return $this;
    }

    public function getIndicator(): User
    {
        return $this->indicator;
    }

    public function setIndicator(User $indicator): Indicator
    {
        $this->indicator = $indicator;
        return $this;
    }

    public function getOrigin(): int
    {
        return $this->origin;
    }

    public function getOriginString(): string
    {
        switch ($this->origin) {
            case 1:
                $origin = 'Tela de Doação';
                break;
            case 2:
                $origin = 'Tela de Filiação';
                break;
            case 4: 
                $origin = 'Tela de eventos';
                break;
            default:
                $origin = '';
                break;
        }
        return $origin;
    }

    public function setOrigin(int $origin): Indicator
    {
        $this->origin = $origin;
        return $this;
    }

    public function getEventId(): Events
    {
        return $this->eventId;
    }

    public function setEventId(?Events $eventId): Indicator
    {
        $this->eventId = $eventId;
        return $this;
    }
}