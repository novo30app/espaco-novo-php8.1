<?php

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="checkLeadsByPhase")
 * @ORM @Entity(repositoryClass="App\Models\Repository\CheckLeadsByPhaseRepository")
 */
class CheckLeadsByPhase
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @Column(type="string")
     * @var string
     */
    private $email;

    /**
     * @Column(type="string")
     * @var string
     */
    private $phase;

    /**
     * @Column(type="boolean")
     * @var bool
     */
    private $valid;

    public function getId(): int
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): CheckLeadsByPhase
    {
        $this->email = $email;
        return $this;
    }

    public function getPhase(): string
    {
        return $this->phase;
    }

    public function setPhase(string $phase): CheckLeadsByPhase
    {
        $this->phase = $phase;
        return $this;
    }


    public function getValid(): bool
    {
        return $this->valid;
    }

    public function setValid(bool $valid): CheckLeadsByPhase
    {
        $this->valid = $valid;
        return $this;
    }
}