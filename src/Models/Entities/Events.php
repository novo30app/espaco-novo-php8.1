<?php

namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\Mapping as ORM;

/**
 * TbEventos
 *
 * @Table(name="tb_eventos", indexes={@Index(name="idx_tb_eventos_data_inicio", columns={"data_inicio"}), @Index(name="idx_tb_eventos_diretorio_id", columns={"diretorio_id"}), @Index(name="idx_tb_eventos_estado_id", columns={"estado_id"}), @Index(name="idx_tb_eventos_status", columns={"status"}), @Index(name="idx_tb_eventos_nome", columns={"nome"}), @Index(name="idx_tb_eventos_cidade_id", columns={"cidade_id"})})
 * @ORM @Entity(repositoryClass="App\Models\Repository\EventsRepository")
 */
class Events
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @Column(name="diretorio_id", type="integer", nullable=true)
     */
    private $diretorioId;

    /**
     * @var int|null
     *
     * @Column(name="nucleo_id", type="integer", nullable=true)
     */
    private $nucleoId;

    /**
     * @var int|null
     *
     * @Column(name="usuario_responsavel_id", type="integer", nullable=true)
     */
    private $usuarioResponsavelId;

    /**
     * @var string
     *
     * @Column(name="nome", type="string", length=100, nullable=false)
     */
    private $nome;

    /**
     * @var string|null
     *
     * @Column(name="status", type="string", length=0, nullable=true, options={"default"="pendente"})
     */
    private $status = 'pendente';

    /**
     * @var string|null
     *
     * @Column(name="tipo", type="string", length=0, nullable=true, options={"default"="pago"})
     */
    private $tipo = 'pago';

    /**
     * @var string|null
     *
     * @Column(name="endereco", type="string", length=255, nullable=true)
     */
    private $endereco;

    /**
     * @var string|null
     *
     * @Column(name="local", type="string", length=50, nullable=true)
     */
    private $local;

    /**
     * @var string|null
     *
     * @Column(name="cep", type="string", length=100, nullable=true)
     */
    private $cep;

    /**
     * @var string|null
     *
     * @Column(name="bairro", type="string", length=100, nullable=true)
     */
    private $bairro;

    /**
     * @var string|null
     *
     * @Column(name="complemento", type="string", length=100, nullable=true)
     */
    private $complemento;

    /**
     * @var int|null
     *
     * @Column(name="cidade_id", type="integer", nullable=true)
     */
    private $cidadeId;

    /**
     * @var int|null
     *
     * @Column(name="estado_id", type="integer", nullable=true)
     */
    private $estadoId;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_inicio", type="datetime", nullable=true)
     */
    private $dataInicio;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_termino", type="datetime", nullable=true)
     */
    private $dataTermino;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_evento", type="datetime", nullable=true)
     */
    private $dataEvento;

    /**
     * @var string|null
     *
     * @Column(name="path_logo", type="string", length=255, nullable=true)
     */
    private $pathLogo;

    /**
     * @var string|null
     *
     * @Column(name="path_banner", type="string", length=255, nullable=true)
     */
    private $pathBanner;

    /**
     * @var string|null
     *
     * @Column(name="link_video", type="string", length=255, nullable=true)
     */
    private $linkVideo;

    /**
     * @var string|null
     *
     * @Column(name="link_online", type="string", length=255, nullable=true)
     */
    private $linkOnline = '';

    /**
     * @var int|null
     *
     * @Column(name="limite_inscritos", type="integer", nullable=true)
     */
    private $limiteInscritos;

    /**
     * @var string|null
     *
     * @Column(name="descricao", type="text", length=0, nullable=true)
     */
    private $descricao;

    /**
     * @var bool|null
     *
     * @Column(name="apenas_filiados", type="boolean", nullable=true)
     */
    private $apenasFiliados;

    /**
     * @var string|null
     *
     * @Column(name="especie", type="text", nullable=true)
     */
    private $especie;

    /**
     * @var string|null
     *
     * @Column(name="categoria", type="text", nullable=true)
     */
    private $categoria;

    /**
     * @var string|null
     *
     * @Column(name="exibicao", type="text", nullable=true)
     */
    private $exibicao;

    /**
     * @var bool|null
     *
     * @Column(name="oficial_novo", type="boolean", nullable=true)
     */
    private $oficialNovo = '0';

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_criacao", type="datetime", nullable=true)
     */
    private $dataCriacao;

    /**
     * @var int|null
     *
     * @Column(name="removido", type="smallint", nullable=true)
     */
    private $removido = '0';

    /**
     * @var int|null
     *
     * @Column(name="dias_boleto", type="integer", nullable=true)
     */
    private $diasBoleto;

    /**
     * @var bool|null
     *
     * @Column(name="vaquinha", type="boolean", nullable=true)
     */
    private $vaquinha = '0';

    /**
     * @var bool|null
     *
     * @Column(name="presencial", type="boolean", nullable=true)
     */
    private $presencial = '0';

    public function getId(): int
    {
        return $this->id;
    }

    public function getDiretorioId(): int
    {
        return $this->diretorioId;
    }

    public function setDiretorioId(int $diretorioId): Events
    {
        $this->diretorioId = $diretorioId;
        return $this;
    }

    public function getNucleoId(): int
    {
        return $this->nucleoId;
    }

    public function setNucleoId(int $nucleoId): Events
    {
        $this->nucleoId = $nucleoId;
        return $this;
    }

    public function getUsuarioResponsavelId(): int
    {
        return $this->usuarioResponsavelId;
    }

    public function setUsuarioResponsavelId(int $usuarioResponsavelId): Events
    {
        $this->usuarioResponsavelId = $usuarioResponsavelId;
        return $this;
    }

    public function getName(): string
    {
        return $this->nome;
    }

    public function setName(string $nome): Events
    {
        $this->nome = $nome;
        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): Events
    {
        $this->status = $status;
        return $this;
    }

    public function getType(): string
    {
        return $this->tipo;
    }

    public function setType(string $tipo): Events
    {
        $this->tipo = $tipo;
        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->endereco;
    }

    public function setAddress(string $endereco): Events
    {
        $this->endereco = $endereco;
        return $this;
    }

    public function getLocal(): ?string
    {
        return $this->local;
    }

    public function setLocal(string $local): Events
    {
        $this->local = $local;
        return $this;
    }

    public function getCep(): ?string
    {
        return $this->cep;
    }

    public function setCep(string $cep): Events
    {
        $this->cep = $cep;
        return $this;
    }

    public function getDistrict(): ?string
    {
        return $this->bairro;
    }

    public function setDistrict(string $bairro): Events
    {
        $this->bairro = $bairro;
        return $this;
    }

    public function getComplement(): ?string
    {
        return $this->complemento;
    }

    public function setComplement(string $complemento): Events
    {
        $this->baicomplementorro = $complemento;
        return $this;
    }

    public function getCityId(): int
    {
        return $this->cidadeId;
    }

    public function setCityId(int $cidadeId): Events
    {
        $this->cidadeId = $cidadeId;
        return $this;
    }

    public function getStateId(): int
    {
        return $this->estadoId;
    }

    public function setStateId(int $estadoId): Events
    {
        $this->estadoId = $estadoId;
        return $this;
    }

    public function getDateBegin(): ?\DateTime
    {
        return $this->dataInicio;
    }

    public function setDateBegin(?\DateTime $dataInicio): Events
    {
        $this->dataInicio = $dataInicio;
        return $this;
    }

    public function getDateFinal(): ?\DateTime
    {
        return $this->dataTermino;
    }

    public function setDateFinal(?\DateTime $dataTermino): Events
    {
        $this->dataTermino = $dataTermino;
        return $this;
    }

    public function getDateEvent(): ?\DateTime
    {
        return $this->dataEvento;
    }

    public function setDateEvent(?\DateTime $dataEvento): Events
    {
        $this->dataEvento = $dataEvento;
        return $this;
    }

    public function getPathLogo(): ?string
    {
        return $this->pathLogo;
    }

    public function setPathLogo(string $pathLogo): Events
    {
        $this->pathLogo = $pathLogo;
        return $this;
    }

    public function getPathBanner(): ?string
    {
        return $this->pathBanner;
    }

    public function setPathBanner(string $pathBanner): Events
    {
        $this->pathBanner = $pathBanner;
        return $this;
    }

    public function getLinkVideo(): string
    {
        return $this->linkVideo;
    }

    public function setLinkVideo(string $linkVideo): Events
    {
        $this->linkVideo = $linkVideo;
        return $this;
    }

    public function getLinkOnline(): string
    {
        return $this->linkOnline ?? '' ;
    }

    public function setLinkOnline(string $linkOnline): Events
    {
        $this->linkOnline = $linkOnline;
        return $this;
    }

    public function getLimitSubscribers(): int
    {
        return $this->limiteInscritos;
    }

    public function SetLimitSubscribers(int $limiteInscritos): Events
    {
        $this->limiteInscritos = $limiteInscritos;
        return $this;
    }

    public function getDescription(): string
    {
        return $this->descricao;
    }

    public function setDescription(string $descricao): Events
    {
        $this->descricao = $descricao;
        return $this;
    }

    public function getAffiliatedOnly(): int
    {
        return $this->apenasFiliados;
    }

    public function setAffiliatedOnly(int $apenasFiliados): Events
    {
        $this->apenasFiliados = $apenasFiliados;
        return $this;
    }
    
    public function getSpecie(): string
    {
        return $this->especie;
    }

    public function setSpecie(string $especie): Events
    {
        $this->especie = $especie;
        return $this;
    }

    public function getCategory(): string
    {
        return $this->categoria;
    }

    public function getCategoryString(): ?string
    {
        switch ($this->categoria) {
            case 1:
                $categoria = 'Doação';
                break;
            default:
                $categoria = 'Evento';
                break;
        }
        return $categoria;
    }

    public function setCategory(string $categoria): Events
    {
        $this->categoria = $categoria;
        return $this;
    }

    public function getExhibition(): string
    {
        return $this->exibicao;
    }

    public function setExhibition(string $exibicao): Events
    {
        $this->exibicao = $exibicao;
        return $this;
    }

    public function getOficial(): int
    {
        return $this->oficialNovo;
    }

    public function setOficial(int $oficialNovo): Events
    {
        $this->oficialNovo = $oficialNovo;
        return $this;
    }

    public function getDateCreate(): ?\DateTime
    {
        return $this->dataCriacao;
    }

    public function setDateCreate(?\DateTime $dataCriacao): Events
    {
        $this->dataCriacao = $dataCriacao;
        return $this;
    }

    public function getRemoved(): int
    {
        return $this->removido;
    }

    public function setRemoved(int $removido): Events
    {
        $this->removido = $removido;
        return $this;
    }

    public function getBillsDay(): int
    {
        return $this->diasBoleto;
    }

    public function setBillsDay(int $diasBoleto): Events
    {
        $this->diasBoleto = $diasBoleto;
        return $this;
    }

    public function getCrowdfunding(): int
    {
        return $this->vaquinha;
    }

    public function setCrowdfunding(int $vaquinha): Events
    {
        $this->vaquinha = $vaquinha;
        return $this;
    }

    public function getPresential(): int
    {
        return $this->presencial;
    }

    public function setPresential(int $presencial): Events
    {
        $this->presencial = $presencial;
        return $this;
    }
}