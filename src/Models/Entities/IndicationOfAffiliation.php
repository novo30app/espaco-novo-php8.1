<?php

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="indicationOfAffiliation")
 * @ORM @Entity(repositoryClass="App\Models\Repository\IndicationOfAffiliationRepository")
 */
class IndicationOfAffiliation
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $status;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user", referencedColumnName="id", nullable=true)
     */
    private User $user;

    /**
     * @Column(type="string")
     * @var string
     */
    private $hash;

    /**
     * @Column(type="string")
     * @var string
     */
    private $membershipForm;

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="userAdmin", referencedColumnName="id", nullable=true)
     */
    private UserAdmin $userAdmin;

    /**
     * @Column(type="boolean")
     * @var bool
     */
    private $used;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): IndicationOfAffiliation
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function getStatusString(): string
    {
        $status = match($this->status) {
            1 => 'Aguardando aprovação de filiação',
            2 => 'Aguardando aprovação do filiado',
            3 => 'Cancelado',
            4 => 'Período de impugnação',
            5 => 'Filiado',
            6 => 'Aguardando Pagamento',
            default => 'Desconhecido'
        };
        return $status;
    }

    public function setStatus(int $status): IndicationOfAffiliation
    {
        $this->status = $status;
        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): IndicationOfAffiliation
    {
        $this->user = $user;
        return $this;
    }

    public function getHash(): ?string
    {
        return $this->hash;
    }

    public function setHash(?string $hash): IndicationOfAffiliation
    {
        $this->hash = $hash;
        return $this;
    }

    public function getMembershipForm(): ?string
    {
        return $this->membershipForm;
    }

    public function setMembershipForm(?string $hash): IndicationOfAffiliation
    {
        $this->membershipForm = $membershipForm;
        return $this;
    }

    public function getUserAdmin(): UserAdmin
    {
        return $this->userAdmin;
    }

    public function setUserAdmin(UserAdmin $userAdmin): IndicationOfAffiliation
    {
        $this->userAdmin = $userAdmin;
        return $this;
    }

    public function getUsed(): bool
    {
        return $this->used;
    }

    public function setUsed(bool $used): IndicationOfAffiliation
    {
        $this->used = $used;
        return $this;
    }
}