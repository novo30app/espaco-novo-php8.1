<?php


namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\Mapping as ORM;

/**
 * @Entity @Table(name="candidatesStatus")
 * @ORM @Entity(repositoryClass="App\Models\Repository\CandidateStatusRepository")
 */
class CandidateStatus
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private int $id;

    /**
     * @Column(type="string")
     */
    private string $name;


    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName($name): CandidateStatus
    {
        $this->name = $name;
        return $this;
    }


}