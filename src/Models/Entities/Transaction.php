<?php


namespace App\Models\Entities;

use App\Helpers\Utils;
use Doctrine\ORM\Mapping as ORM;

/**
 * Transactions
 *
 * @Entity @Table(name="tb_transacoes", indexes={@Index(name="idx_id", columns={"id"}),@Index(name="idx_invoice_id", columns={"invoice_id"}), @Index(name="idx_tb_diretorio_id", columns={"tb_diretorio_id"}), @Index(name="idx_data_criacao", columns={"data_criacao"}), @Index(name="idx_tb_pessoa_id", columns={"tb_pessoa_id"}), @Index(name="idx_token_cartao_credito", columns={"token_cartao_credito"}), @Index(name="idx_tb_diretorio_origem", columns={"tb_diretorio_origem"}), @Index(name="idx_data_pago", columns={"data_pago"}), @Index(name="idx_id", columns={"id"}), @Index(name="idx_tb_assinatura_id", columns={"tb_assinatura_id"})})
 * @ORM @Entity(repositoryClass="App\Models\Repository\TransactionRepository")
 */
class Transaction
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var bool|null
     *
     * @Column(name="obs", type="string", nullable=true)
     */
    private $obs;

    /**
     * @var int|null
     *
     * @Column(name="tb_assinatura_id", type="integer", nullable=true)
     */
    private $tbAssinaturaId;

    /**
     * @Column(type="datetime", name="data_criacao", nullable=true)
     * @var string
     */
    private $dataCriacao;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_criacao_origem", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataCriacaoOrigem;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_gera_fatura", type="date", nullable=true)
     */
    private $dataGeraFatura;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_vencimento", type="date", nullable=true)
     */
    private $dataVencimento;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_vencimento_origem", type="date", nullable=true)
     */
    private $dataVencimentoOrigem;

    /**
     * @var \DateTime|null
     *
     * @Column(type="string", name="data_pago", nullable=true)
     */
    private $dataPago;

    /**
     * @var \DateTime|null
     *
     * @Column(type="datetime", name="data_deposito", nullable=true)
     */
    private $dataDeposito;

    /**
     * @var \DateTime|null
     *
     * @Column(type="datetime", name="data_cancelamento", nullable=true)
     */
    private $dataCancelamento;

    /**
     * @var \DateTime|null
     *
     * @Column(type="datetime", name="data_liberacao", nullable=true)
     */
    private $dataLiberacao;

    /**
     * @Column(type="decimal", name="valor", precision=13, scale=4, nullable=true)
     * @var float
     */
    private $valor;

    /**
     * @Column(type="decimal", name="valor_s_taxas", precision=13, scale=4, nullable=true)
     *
     * @var float
     */
    private $valorSemTaxas;

    /**
     * @var int|null
     *
     * @Column(name="forma_pagamento", type="integer", nullable=true)
     */
    private $formaPagamento;

    /**
     * @var bool|null
     *
     * @Column(name="tipo_cartao", type="integer", nullable=true)
     */
    private $tipoCartao;

    /**
     * @var string|null
     *
     * @Column(name="creditCardFlag", type="string", length=25, nullable=true)
     */
    private $creditCardFlag;

    /**
     * @var bool|null
     *
     * @Column(name="origem_transacao", type="integer", nullable=true)
     */
    private $origemTransacao;

    /**
     * @var int|null
     *
     * @Column(name="periodicidade", type="integer", nullable=true)
     */
    private $periodicidade;

    /**
     * @var bool|null
     *
     * @Column(name="gateway_pagamento", type="integer", nullable=true)
     */
    private $gatewayPagamento;

    /**
     * @var string|null
     *
     * @Column(name="customer_id", type="string", length=45, nullable=true)
     */
    private $customerId;

    /**
     * @var string|null
     *
     * @Column(name="invoice_id", type="string", length=100, nullable=true)
     */
    private $invoiceId;

    /**
     * @var string|null
     *
     * @Column(name="nsu", type="string", length=25, nullable=true)
     */
    private $nsu;

    /**
     * @var string|null
     *
     * @Column(name="status", type="string", length=45, nullable=true)
     */
    private $status;

    /**
     * @var string|null
     *
     * @Column(name="url", type="string", length=300, nullable=true)
     */
    private $url;

    /**
     * @var string|null
     *
     * @Column(name="codigo_barras", type="string", length=300, nullable=true)
     */
    private $codigoBarras;


    /**
     * @var string|null
     *
     * @Column(name="numero_recibo", type="string", length=40, nullable=true)
     */
    private $numeroRecibo;

    /**
     * @var string|null
     *
     * @Column(name="token_cartao_credito", type="string", length=255, nullable=true)
     */
    private $tokenCartaoCredito;

    /**
     * @var int|null
     *
     * @Column(name="fase_cobranca", type="integer", nullable=true, options={"default"="1"})
     */
    private $faseCobranca = '1';

    /**
     * @var \DateTime|null
     *
     * @Column(name="ultima_atualizacao", type="datetime", nullable=true)
     */
    private $ultimaAtualizacao;

    /**
     * @var int|null
     *
     * @Column(name="atualizado_por", type="integer", nullable=true)
     */
    private $atualizadoPor;

    /**
     * @var int|null
     *
     * @Column(name="tb_diretorio_id", type="integer", nullable=true, options={"default"="1"})
     */
    private $tbDiretorioId = '1';

    /**
     * @var int|null
     *
     * @Column(name="tb_diretorio_origem", type="integer", nullable=true)
     */
    private $tbDiretorioOrigem;

    /**
     * @var string|null
     *
     * @Column(name="pendencia_titulo", type="string", length=1, nullable=true)
     */
    private $pendenciaTitulo;

    /**
     * @var bool|null
     *
     * @Column(name="sku", type="boolean", nullable=true)
     */
    private $sku;

    /**
     * @var string|null
     *
     * @Column(name="rede_id", type="string", nullable=true)
     */
    private $redeId;

    /**
     * @var string|null
     *
     * @Column(name="qrcode", type="string", nullable=true)
     */
    private $qrCode;

    /**
     * @var string|null
     *
     * @Column(name="rescue", type="string", nullable=true)
     */
    private $rescue;

    public function toArray(): array
    {
        $array['id'] = $this->getId();
        $array['value'] = $this->getValue();
        $array['status'] = $this->getStatus();
        return $array;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRedeId(): ?string
    {
        return $this->redeId;
    }


    public function setRedeId(?string $id): Transaction
    {
        $this->redeId = $id;
        return $this;
    }


    public function getDataCreation()
    {
        return $this->dataCriacao;
    }

    public function getDataPaid()
    {
        return $this->dataPago;
    }

    public function getOriginTypeString(): ?string
    {
        $origin = $this->origemTransacao;

        switch ($origin) {
            case 1:
                return 'Doação';
            case 2:
                return 'Filiação';
            case 3:
                return 'Doação Campanha';
            case 4:
                return 'Evento';
            case 5:
                return 'Complemento de Filiação';
            case 6:
                return 'Doação Maquininha';
            case 7:
                return 'Processo Seletivo';
            case 8:
                return 'Plano de contribuição';
            case 15:
                return 'Filiação antecipada';
            default:
                return 'Desconhecido';
        }
    }

    public function getPaymentTypeString(): ?string
    {
        switch ($this->formaPagamento) {
            case 1:
                return 'Cartão de Crédito';
            case 2:
                return 'Boleto';
            case 3:
                return 'TED';
            case 4:
                return 'DOC';
            case 5:
                return 'Transferência Online';
            case 6:
                return 'Depósito Oline';
            case 7:
                return 'Depósito';
            case 8:
                return 'Cheque';
            case 9:
                return 'Tranferência';
            case 10:
                return 'Cartão de Débito';
            case 11:
                return 'PIX';
            default:
                return '-';
        }
    }

    public function getValue($formated = true)
    {
        if (!$formated) return $this->valor;
        $valor = $this->valor;
        return number_format($valor, 2, ',', '.');
    }

    public function getDirectoryOrigin()
    {
        return $this->tbDiretorioOrigem;
    }

    public function getStatusTypeString(): ?string
    {
        switch ($this->status) {

            case 'canceled' :
                return 'Cancelada';
            case 'chargeback' :
                return 'Chargeback';
            case 'pending' :
                return 'Pendente';
            case 'expired' :
                return 'Expirado';
            case 'refunded' :
                return 'Reembolsada';
            case 'reversed' :
                return 'Estornado';
            case 'partially paid' :
                return 'Pago parcialmente';
            case 'accumulated' :
                return 'Acumulado';
            case 'paid' :
                return 'Pago (' . $this->getDataPago()->format('d/m/Y H:i:s') . ')';
            case 'exemption' :
                return 'Isento';
            default:
                return 'Desconhecido';
        }

    }

    public function getReceipt(): ?string
    {
        return $this->numeroRecibo;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): Transaction
    {
        $this->url = $url;
        return $this;
    }

    public function getCodigoBarras(): ?string
    {
        return $this->codigoBarras;
    }

    public function setCodigoBarras(?string $codigoBarras): Transaction
    {
        $this->codigoBarras = $codigoBarras;
        return $this;
    }


    public function getValueDescription()
    {
        return ucfirst(ltrim(Utils::describeValue($this->valor)));
    }

    public function getTbPessoaId(): ?int
    {
        return $this->tbPessoaId;
    }

    public function setTbPessoaId(?int $tbPessoaId): Transaction
    {
        $this->tbPessoaId = $tbPessoaId;
        return $this;
    }

    public function getTbAssinaturaId(): ?int
    {
        return $this->tbAssinaturaId;
    }

    public function setTbAssinaturaId(?int $tbAssinaturaId): Transaction
    {
        $this->tbAssinaturaId = $tbAssinaturaId;
        return $this;
    }

    public function getDataCriacao(): string
    {
        return $this->dataCriacao;
    }

    public function setDataCriacao(\DateTime $dataCriacao): Transaction
    {
        $this->dataCriacao = $dataCriacao;
        return $this;
    }

    public function getDataCriacaoOrigem(): ?\DateTime
    {
        return $this->dataCriacaoOrigem;
    }

    public function setDataCriacaoOrigem(?\DateTime $dataCriacaoOrigem): Transaction
    {
        $this->dataCriacaoOrigem = $dataCriacaoOrigem;
        return $this;
    }

    public function getDataGeraFatura(): ?\DateTime
    {
        return $this->dataGeraFatura;
    }

    public function setDataGeraFatura(?\DateTime $dataGeraFatura): Transaction
    {
        $this->dataGeraFatura = $dataGeraFatura;
        return $this;
    }

    public function getDataVencimento(): ?\DateTime
    {
        return $this->dataVencimento;
    }

    public function setDataVencimento(?\DateTime $dataVencimento): Transaction
    {
        $this->dataVencimento = $dataVencimento;
        return $this;
    }

    public function getDataVencimentoOrigem(): ?\DateTime
    {
        return $this->dataVencimentoOrigem;
    }

    public function setDataVencimentoOrigem(?\DateTime $dataVencimentoOrigem): Transaction
    {
        $this->dataVencimentoOrigem = $dataVencimentoOrigem;
        return $this;
    }

    public function getDataPago(): ?\DateTime
    {
        return new \DateTime($this->dataPago);
    }

    public function setDataPago(?\DateTime $dataPago): Transaction
    {
        $this->dataPago = $dataPago ? $dataPago->format('Y-m-d H:i:s') : null;
        return $this;
    }

    public function getValor(): float
    {
        return $this->valor;
    }

    public function getWithoutFeesIugu(): float
    {
        $value = $this->valor;
        $valueWithoutFees = match($this->formaPagamento) {
            1 => $value - (0.30 + ($value * (2.59 / 100))),
            11 =>  $value - ($value * (0.99 / 100)),
            2 =>  $value - 1.91,
        };
        return number_format($valueWithoutFees, 2, '.', ' ');
    }

    public function setValor(float $valor): Transaction
    {
        $this->valor = $valor;
        return $this;
    }

    public function getFormaPagamento(): ?int
    {
        return $this->formaPagamento;
    }

    public function setFormaPagamento(?int $formaPagamento): Transaction
    {
        $this->formaPagamento = $formaPagamento;
        return $this;
    }

    public function getTipoCartao(): ?bool
    {
        return $this->tipoCartao;
    }

    public function setTipoCartao(?bool $tipoCartao): Transaction
    {
        $this->tipoCartao = $tipoCartao;
        return $this;
    }

    public function getCreditCardFlag(): string
    {
        return $this->creditCardFlag;
    }

    public function setCreditCardFlag(string $creditCardFlag): Transaction
    {
        $this->creditCardFlag = $creditCardFlag;
        return $this;
    }

    public function getOrigemTransacao(): ?int
    {
        return $this->origemTransacao;
    }

    public function setOrigemTransacao(?int $origemTransacao): Transaction
    {
        $this->origemTransacao = $origemTransacao;
        return $this;
    }

    public function getPeriodicidade(): ?int
    {
        return $this->periodicidade;
    }

    public function setPeriodicidade(?int $periodicidade): Transaction
    {
        $this->periodicidade = $periodicidade;
        return $this;
    }

    public function getGatewayPagamento(): ?int
    {
        return $this->gatewayPagamento;
    }

    public function setGatewayPagamento(?int $gatewayPagamento): Transaction
    {
        $this->gatewayPagamento = $gatewayPagamento;
        return $this;
    }

    public function getCustomerId(): ?string
    {
        return $this->customerId;
    }

    public function setCustomerId(?string $customerId): Transaction
    {
        $this->customerId = $customerId;
        return $this;
    }

    public function getInvoiceId(): ?string
    {
        return $this->invoiceId;
    }

    public function setInvoiceId(?string $invoiceId): Transaction
    {
        $this->invoiceId = $invoiceId;
        return $this;
    }

    public function getNsu(): string
    {
        return $this->nsu;
    }

    public function setNsu(string $nsu): Transaction
    {
        $this->nsu = $nsu;
        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): Transaction
    {
        $this->status = $status;
        return $this;
    }

    public function getNumeroRecibo(): ?string
    {
        return $this->numeroRecibo;
    }

    public function setNumeroRecibo(?string $numeroRecibo): Transaction
    {
        $this->numeroRecibo = $numeroRecibo;
        return $this;
    }

    public function getTokenCartaoCredito(): ?string
    {
        return $this->tokenCartaoCredito;
    }

    public function setTokenCartaoCredito(?string $tokenCartaoCredito): Transaction
    {
        $this->tokenCartaoCredito = $tokenCartaoCredito;
        return $this;
    }

    public function getFaseCobranca(): ?int
    {
        return $this->faseCobranca;
    }

    public function setFaseCobranca(?int $faseCobranca): Transaction
    {
        $this->faseCobranca = $faseCobranca;
        return $this;
    }

    public function getUltimaAtualizacao(): ?\DateTime
    {
        return $this->ultimaAtualizacao;
    }

    public function setUltimaAtualizacao(?\DateTime $ultimaAtualizacao): Transaction
    {
        $this->ultimaAtualizacao = $ultimaAtualizacao;
        return $this;
    }

    public function getAtualizadoPor(): ?int
    {
        return $this->atualizadoPor;
    }

    public function setAtualizadoPor(?int $atualizadoPor): Transaction
    {
        $this->atualizadoPor = $atualizadoPor;
        return $this;
    }

    public function getTbDiretorioId(): ?int
    {
        return $this->tbDiretorioId;
    }

    public function setTbDiretorioId(?int $tbDiretorioId): Transaction
    {
        $this->tbDiretorioId = $tbDiretorioId;
        return $this;
    }

    public function getTbDiretorioOrigem(): ?int
    {
        return $this->tbDiretorioOrigem;
    }

    public function setTbDiretorioOrigem(?int $tbDiretorioOrigem): Transaction
    {
        $this->tbDiretorioOrigem = $tbDiretorioOrigem;
        return $this;
    }

    public function getPendenciaTitulo(): ?string
    {
        return $this->pendenciaTitulo;
    }

    public function setPendenciaTitulo(?string $pendenciaTitulo): Transaction
    {
        $this->pendenciaTitulo = $pendenciaTitulo;
        return $this;
    }

    public function getSku(): ?bool
    {
        return $this->sku;
    }

    public function setSku(?bool $sku): Transaction
    {
        $this->sku = $sku;
        return $this;
    }

    public function getObs(): ?string
    {
        return $this->obs;
    }

    public function setObs(?string $obs): Transaction
    {
        $this->obs = $obs;
        return $this;
    }

    public function getQrCode(): ?string
    {
        return $this->qrCode;
    }

    public function setQrCode(?string $qrCode): Transaction
    {
        $this->qrCode = $qrCode;
        return $this;
    }

    public function getDataDeposito(): ?\DateTime
    {
        return $this->dataDeposito;
    }

    public function setDataDeposito(?\DateTime $dataDeposito): Transaction
    {
        $this->dataDeposito = $dataDeposito;
        return $this;
    }

    public function getValorSemTaxas(): ?float
    {
        return $this->valorSemTaxas;
    }

    public function setValorSemTaxas(?float $valorSemTaxas): Transaction
    {
        $this->valorSemTaxas = $valorSemTaxas;
        return $this;
    }

    public function getRescue(): string
    {
        return $this->rescue;
    }

    public function setRescue(string $rescue): Transaction
    {
        $this->rescue = $rescue;
        return $this;
    }

    public function getDataCancelamento(): \DateTime
    {
        return $this->dataCancelamento;
    }

    public function setDataCancelamento(\DateTime $dataCancelamento): Transaction
    {
        $this->dataCancelamento = $dataCancelamento;
        return $this;
    }

    public function getValueSimple($formated = true)
    {
        $valor = $this->valor;
        return $valor;
    }

    public function getDataLiberacao(): ?\DateTime
    {
        return new \DateTime($this->dataLiberacao);
    }

    public function setDataLiberacao(?\DateTime $dataLiberacao): Transaction
    {
        $this->dataLiberacao = $dataLiberacao;
        return $this;
    }
}
