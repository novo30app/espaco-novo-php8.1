<?php

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="historicalIncrease")
 * @ORM @Entity(repositoryClass="App\Models\Repository\HistoricalIncreaseRepository")
 */
class HistoricalIncrease
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $user;

    /**
     * @Column(type="string")
     * @var string
     */
    private $phase;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    private $dateEvent;

    /**
     * @Column(type="float")
     * @var float
     */
    private $currentValue;

    /**
     * @Column(type="float")
     * @var float
     */
    private $newValue;

    /**
     * @Column(type="string")
     * @var string
     */
    private $obs;

    public function getId()
    {
        return $this->id;
    }

    public function getUser(): int
    {
        return $this->user;
    }

    public function setUser(int $user): HistoricalIncrease
    {
        $this->user = $user;
        return $this;
    }

    public function getPhase(): string
    {
        return $this->phase;
    }

    public function setPhase(string $phase): HistoricalIncrease
    {
        $this->phase = $phase;
        return $this;
    }

    public function getDateEvent(): \DateTime
    {
        return $this->dateEvent;
    }

    public function setDateEvent(\DateTime $dateEvent): HistoricalIncrease
    {
        $this->dateEvent = $dateEvent;
        return $this;
    }

    public function getCurrentValue(): float
    {
        return $this->currentValue;
    }

    public function setCurrentValue(?float $currentValue): HistoricalIncrease
    {
        $this->currentValue = $currentValue;
        return $this;
    }

    public function getNewValue(): float
    {
        return $this->newValue;
    }

    public function setNewValue(?float $newValue): HistoricalIncrease
    {
        $this->newValue = $newValue;
        return $this;
    }

    public function getObs(): ?string
    {
        return $this->obs;
    }

    public function setObs(?string $obs): HistoricalIncrease
    {
        $this->obs = $obs;
        return $this;
    }
}