<?php

namespace App\Models\Entities;

/**
 * @Entity @Table(name="researchNovoAffiliated")
 * @ORM @Entity(repositoryClass="App\Models\Repository\ResearchNovoAffiliatedRepository")
 */
class ResearchNovoAffiliated
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ManyToOne(targetEntity="ResearchNovoUser")
     * @JoinColumn(name="researchNovoUser", referencedColumnName="id")
     */
    private ResearchNovoUser $researchNovoUser;

    /**
     * @Column(type="text")
     */
    private string $question1;

    /**
     * @Column(type="text")
     */
    private string $textAreaOthersQuestion1;

    /**
     * @Column(type="text")
     */
    private string $question2;

    /**
     * @Column(type="text")
     */
    private string $textAreaOthersQuestion2;

    /**
     * @Column(type="text")
     */
    private string $question3;

    /**
     * @Column(type="text")
     */
    private string $question31;

    /**
     * @Column(type="text")
     */
    private string $question32;

    /**
     * @Column(type="text")
     */
    private string $textAreaOthersQuestion3;

    /**
     * @Column(type="text")
     */
    private string $textAreaOthersQuestion31;

    /**
     * @Column(type="text")
     */
    private string $textAreaOthersQuestion32;

    /**
     * @Column(type="text")
     */
    private string $question4;

    /**
     * @Column(type="text")
     */
    private string $question5;

    /**
     * @Column(type="text")
     */
    private string $question51Option1;

    /**
     * @Column(type="text")
     */
    private string $question51Option2;

    /**
     * @Column(type="text")
     */
    private string $question52Option1;

    /**
     * @Column(type="text")
     */
    private string $question52Option2;

    /**
     * @Column(type="text")
     */
    private string $question53Option1;

    /**
     * @Column(type="text")
     */
    private string $question53Option2;

    /**
     * @Column(type="text")
     */
    private string $question6;

    public function getId()
    {
        return $this->id;
    }

    public function getResearchNovoUser(): ResearchNovoUser
    {
        return $this->researchNovoUser;
    }

    public function setResearchNovoUser(ResearchNovoUser $researchNovoUser): ResearchNovoAffiliated
    {
        $this->researchNovoUser = $researchNovoUser;
        return $this;
    }

    public function getQuestion1(): string
    {
        return $this->question1;
    }

    public function setQuestion1(string $question1): ResearchNovoAffiliated
    {
        $this->question1 = $question1;
        return $this;
    }

    public function getTextAreaOthersQuestion1(): string
    {
        return $this->textAreaOthersQuestion1;
    }

    public function setTextAreaOthersQuestion1(string $textAreaOthersQuestion1): ResearchNovoAffiliated
    {
        $this->textAreaOthersQuestion1 = $textAreaOthersQuestion1;
        return $this;
    }

    public function getQuestion2(): string
    {
        return $this->question2;
    }

    public function setQuestion2(string $question2): ResearchNovoAffiliated
    {
        $this->question2 = $question2;
        return $this;
    }

    public function getTextAreaOthersQuestion2(): string
    {
        return $this->textAreaOthersQuestion2;
    }

    public function setTextAreaOthersQuestion2(string $textAreaOthersQuestion2): ResearchNovoAffiliated
    {
        $this->textAreaOthersQuestion2 = $textAreaOthersQuestion2;
        return $this;
    }

    public function getQuestion3(): string
    {
        return $this->question3;
    }

    public function setQuestion3(string $question3): ResearchNovoAffiliated
    {
        $this->question3 = $question3;
        return $this;
    }

    public function getQuestion31(): string
    {
        return $this->question31;
    }

    public function setQuestion31(string $question31): ResearchNovoAffiliated
    {
        $this->question31 = $question31;
        return $this;
    }

    public function getQuestion32(): string
    {
        return $this->question32;
    }

    public function setQuestion32(string $question32): ResearchNovoAffiliated
    {
        $this->question32 = $question32;
        return $this;
    }

    public function getTextAreaOthersQuestion3(): string
    {
        return $this->textAreaOthersQuestion3;
    }

    public function setTextAreaOthersQuestion3(string $textAreaOthersQuestion3): ResearchNovoAffiliated
    {
        $this->textAreaOthersQuestion3 = $textAreaOthersQuestion3;
        return $this;
    }

    public function getTextAreaOthersQuestion31(): string
    {
        return $this->textAreaOthersQuestion31;
    }

    public function setTextAreaOthersQuestion31(string $textAreaOthersQuestion31): ResearchNovoAffiliated
    {
        $this->textAreaOthersQuestion31 = $textAreaOthersQuestion31;
        return $this;
    }

    public function getTextAreaOthersQuestion32(): string
    {
        return $this->textAreaOthersQuestion32;
    }

    public function setTextAreaOthersQuestion32(string $textAreaOthersQuestion32): ResearchNovoAffiliated
    {
        $this->textAreaOthersQuestion32 = $textAreaOthersQuestion32;
        return $this;
    }

    public function getQuestion4(): string
    {
        return $this->question4;
    }

    public function setQuestion4(string $question4): ResearchNovoAffiliated
    {
        $this->question4 = $question4;
        return $this;
    }

    public function getQuestion5(): string
    {
        return $this->question5;
    }

    public function setQuestion5(string $question5): ResearchNovoAffiliated
    {
        $this->question5 = $question5;
        return $this;
    }

    public function getQuestion51Option1(): string
    {
        return $this->question51Option1;
    }

    public function setQuestion51Option1(string $question51Option1): ResearchNovoAffiliated
    {
        $this->question51Option1 = $question51Option1;
        return $this;
    }

    public function getQuestion51Option2(): string
    {
        return $this->question51Option2;
    }

    public function setQuestion51Option2(string $question51Option2): ResearchNovoAffiliated
    {
        $this->question51Option2 = $question51Option2;
        return $this;
    }

    public function getQuestion52Option1(): string
    {
        return $this->question52Option1;
    }

    public function setQuestion52Option1(string $question52Option1): ResearchNovoAffiliated
    {
        $this->question52Option1 = $question52Option1;
        return $this;
    }

    public function getQuestion52Option2(): string
    {
        return $this->question52Option2;
    }

    public function setQuestion52Option2(string $question52Option2): ResearchNovoAffiliated
    {
        $this->question52Option2 = $question52Option2;
        return $this;
    }

    public function getQuestion53Option1(): string
    {
        return $this->question53Option1;
    }

    public function setQuestion53Option1(string $question53Option1): ResearchNovoAffiliated
    {
        $this->question53Option1 = $question53Option1;
        return $this;
    }

    public function getQuestion53Option2(): string
    {
        return $this->question53Option2;
    }

    public function setQuestion53Option2(string $question53Option2): ResearchNovoAffiliated
    {
        $this->question53Option2 = $question53Option2;
        return $this;
    }

    public function getQuestion6(): string
    {
        return $this->question6;
    }

    public function setQuestion6(string $question6): ResearchNovoAffiliated
    {
        $this->question6 = $question6;
        return $this;
    }
}