<?php

namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\Mapping as ORM;

/**
 * TbConteudo
 *
 * @Table(name="communicationConsent")
 * @ORM @Entity(repositoryClass="App\Models\Repository\CommunicationConsentRepository")
 */
class CommunicationConsent
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer|null
     * @Column(name="user", type="integer")
     */
    private $user;

    /**
     * @var integer|null
     * @Column(name="type", type="integer")
     */
    private $type;

    /**
     * @var bool|null
     * @Column(name="valid", type="boolean")
     */
    private $valid;

    /**
     * @var \DateTime|null
     * @Column(name="created_at", type="datetime")
     */
    private $created_at;

    public function getId(): int
    {
        return $this->id;
    }

    public function getUser(): int
    {
        return $this->user;
    }

    public function setUser(int $user): CommunicationConsent
    {
        $this->user = $user;
        return $this;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function getTypeString()
    {
        switch ($this->type) {
            case 1:
                $type = 'Cadastro';
                break;
            case 2:
                $type = "Filiação";
                break;
            case 3:
                $type = "Doação";
                break;
            case 4:
                $type = "Evento";
                break;
        }
        return $type;
    }

    public function setType(int $type): CommunicationConsent
    {
        $this->type = $type;
        return $this;
    }

    public function getValid(): bool
    {
        return $this->valid;
    }

    public function setValid(bool $valid): CommunicationConsent
    {
        $this->valid = $valid;
        return $this;
    }

    public function getCreated_at(): \DateTime
    {
        return $this->created_at;
    }

    public function setCreated_at(\DateTime $created_at): CommunicationConsent
    {
        $this->created_at = $created_at;
        return $this;
    }
}