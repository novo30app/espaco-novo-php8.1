<?php

namespace App\Models\Entities;

/**
 * @Entity @Table(name="researchNovoUser")
 * @ORM @Entity(repositoryClass="App\Models\Repository\ResearchNovoUserRepository")
 */
class ResearchNovoUser
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private \DateTime $created;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user", referencedColumnName="id")
     */
    private User $user;

    /**
     * @Column(type="integer")
     */
    private int $age;

    /**
     * @Column(type="integer")
     */
    private int $profession;

    /**
     * @Column(type="integer")
     */
    private int $income;

    /**
     * @Column(type="boolean")
     */
    private bool $contact;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    
    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): ResearchNovoUser
    {
        $this->user = $user;
        return $this;
    }

    public function getAge(): int
    {
        return $this->age;
    }

    public function setAge(int $age): ResearchNovoUser
    {
        $this->age = $age;
        return $this;
    }

    public function getProfession(): int
    {
        return $this->profession;
    }

    public function setProfession(int $profession): ResearchNovoUser
    {
        $this->profession = $profession;
        return $this;
    }

    public function getIncome(): int
    {
        return $this->income;
    }

    public function setIncome(int $income): ResearchNovoUser
    {
        $this->income = $income;
        return $this;
    }

    public function getContact(): bool
    {
        return $this->contact;
    }

    public function setContact(bool $contact): ResearchNovoUser
    {
        $this->contact = $contact;
        return $this;
    }
}