<?php


namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;
/**
 * @Entity @Table(name="directoryRegister")
 * @ORM @Entity(repositoryClass="App\Models\Repository\DirectoryRegisterRepository")
 */
class DirectoryRegister
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ManyToOne(targetEntity="AccessLevel")
     * @JoinColumn(name="level", referencedColumnName="id")
     */
    private AccessLevel $level;

    /**
     * @ManyToOne(targetEntity="City")
     * @JoinColumn(name="city", referencedColumnName="id", nullable=true)
     */
    private ?City $city = null;

    /**
     * @ManyToOne(targetEntity="State")
     * @JoinColumn(name="state", referencedColumnName="id")
     */
    private State $state;

    /**
     * @Column(type="boolean")
     */
    private bool $active = true;

    /**
     * @Column(type="string", unique=true)
     */
    private string $email = '';

    public function getId(): string
    {
        return $this->id;
    }

    public function getLevel(): AccessLevel
    {
        return $this->level;
    }


    public function setLevel(AccessLevel $level): DirectoryRegister
    {
        $this->level = $level;
        return $this;
    }


    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): DirectoryRegister
    {
        $this->city = $city;
        return $this;
    }

    public function getState(): State
    {
        return $this->state;
    }

    public function setState(State $state): DirectoryRegister
    {
        $this->state = $state;
        return $this;
    }

    public function getActive(): bool
    {
        return $this->active;
    }

    public function setActive($active): DirectoryRegister
    {
        $this->active = $active;
        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail($email): DirectoryRegister
    {
        $this->email = $email;
        return $this;
    }

}