<?php


namespace App\Models\Entities;
use Doctrine\ORM\Mapping as ORM;

/**
 * TbPais
 *
 * @Table(name="tb_pais")
 * @Entity
 */
class Country
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="nome", type="string", length=250, nullable=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @Column(name="iso", type="string", length=250, nullable=false)
     */
    private $iso;

    /**
     * @var string
     *
     * @Column(name="iso3", type="string", length=250, nullable=false)
     */
    private $iso3;

    /**
     * @var string
     *
     * @Column(name="code", type="string", length=250, nullable=false)
     */
    private $code;

    /**
     * @var \DateTime
     *
     * @Column(name="datacriacao", type="datetime", nullable=false)
     */
    private $datacriacao;

    /**
     * @var bool
     *
     * @Column(name="deletado", type="boolean", nullable=false)
     */
    private $deletado;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): Country
    {
        $this->id = $id;
        return $this;
    }

    public function getNome(): string
    {
        return $this->nome;
    }

    public function setNome(string $nome): Country
    {
        $this->nome = $nome;
        return $this;
    }

    public function getIso(): string
    {
        return $this->iso;
    }

    public function setIso(string $iso): Country
    {
        $this->iso = $iso;
        return $this;
    }

    public function getIso3(): string
    {
        return $this->iso3;
    }

    public function setIso3(string $iso3): Country
    {
        $this->iso3 = $iso3;
        return $this;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): Country
    {
        $this->code = $code;
        return $this;
    }

    public function getDatacriacao(): \DateTime
    {
        return $this->datacriacao;
    }

    public function setDatacriacao(\DateTime $datacriacao): Country
    {
        $this->datacriacao = $datacriacao;
        return $this;
    }

    public function isDeletado(): bool
    {
        return $this->deletado;
    }

    public function setDeletado(bool $deletado): Country
    {
        $this->deletado = $deletado;
        return $this;
    }



}
