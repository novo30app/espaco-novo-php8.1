<?php


namespace App\Models\Entities;

use App\Helpers\Utils;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;
/**
 * @Entity @Table(name="candidates", indexes={@Index(name="candidates_cpf", columns={"cpf"})})
 * @ORM @Entity(repositoryClass="App\Models\Repository\CandidateRepository")
 */
class Candidate
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private int $id;

    /**
     * @Column(type="integer")
     */
    private string $year;

    /**
     * @Column(type="integer")
     */
    private string $numberOfVotes;

    /**
     * @ManyToOne(targetEntity="CandidatePost")
     * @JoinColumn(name="candidatePost", referencedColumnName="id")
     */
    private CandidatePost $candidatePost;

    /**
     * @Column(type="string")
     */
    private string $name;

    /**
     * @ManyToOne(targetEntity="CandidateStatus")
     * @JoinColumn(name="candidateStatus", referencedColumnName="id")
     */
    private CandidateStatus $candidateStatus;

    /**
     * @Column(type="string")
     */
    private string $email;

    /**
     * @Column(type="string", length=11, options={"fixed"=true})
     */
    private string $cpf;

    /**
     * @ManyToOne(targetEntity="City")
     * @JoinColumn(name="city", referencedColumnName="id", nullable=true)
     */
    private ?City $city = null;

    /**
     * @ManyToOne(targetEntity="State")
     * @JoinColumn(name="state", referencedColumnName="id", nullable=true)
     */
    private ?State $state = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getYear(): string
    {
        return $this->year;
    }

    public function setYear(string $year): Candidate
    {
        $this->year = $year;
        return $this;
    }

    public function getNumberOfVotes(): string
    {
        return $this->numberOfVotes;
    }

    public function setNumberOfVotes(string $numberOfVotes): Candidate
    {
        $this->numberOfVotes = $numberOfVotes;
        return $this;
    }

    public function getCandidatePost(): CandidatePost
    {
        return $this->candidatePost;
    }

    public function setCandidatePost(CandidatePost $candidatePost): Candidate
    {
        $this->candidatePost = $candidatePost;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Candidate
    {
        $this->name = $name;
        return $this;
    }

    public function getCandidateStatus(): CandidateStatus
    {
        return $this->candidateStatus;
    }

    public function setCandidateStatus(CandidateStatus $candidateStatus): Candidate
    {
        $this->candidateStatus = $candidateStatus;
        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): Candidate
    {
        $this->email = $email;
        return $this;
    }

    public function getCpf(): string
    {
        return $this->cpf;
    }

    public function setCpf(string $cpf): Candidate
    {
        $this->cpf = Utils::onlyNumbers($cpf);
        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): Candidate
    {
        $this->city = $city;
        return $this;
    }

    public function getState(): ?State
    {
        return $this->state;
    }

    public function setState(?State $state): Candidate
    {
        $this->state = $state;
        return $this;
    }
    
}