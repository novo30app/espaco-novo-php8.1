<?php


namespace App\Models\Entities;
use Doctrine\ORM\Mapping as ORM;

/**
 * Schooling
 *
 * @Table(name="tb_escolaridade")
 * @Entity
 */
class Schooling
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @Column(name="escolaridade", type="string", length=45, nullable=true)
     */
    private $escolaridade;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_criacao", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataCriacao = 'CURRENT_TIMESTAMP';

    /**
     * @var bool|null
     *
     * @Column(name="visivel", type="boolean", nullable=true, options={"default"="1"})
     */
    private $visivel = '1';

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getEscolaridade(): ?string
    {
        return $this->escolaridade;
    }

    /**
     * @param string|null $escolaridade
     * @return Schooling
     */
    public function setEscolaridade(?string $escolaridade): Schooling
    {
        $this->escolaridade = $escolaridade;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDataCriacao(): ?\DateTime
    {
        return $this->dataCriacao;
    }

    /**
     * @param \DateTime|null $dataCriacao
     * @return Schooling
     */
    public function setDataCriacao(?\DateTime $dataCriacao): Schooling
    {
        $this->dataCriacao = $dataCriacao;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getVisivel(): ?bool
    {
        return $this->visivel;
    }

    /**
     * @param bool|null $visivel
     * @return Schooling
     */
    public function setVisivel(?bool $visivel): Schooling
    {
        $this->visivel = $visivel;
        return $this;
    }





}
