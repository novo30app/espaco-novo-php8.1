<?php

namespace App\Models\Entities;
use Doctrine\ORM\Mapping as ORM;

/**
 * TbDiretorioGatway
 *
 * @Table(name="tb_diretorio_gatway")
 * @Entity
 */
class DirectoryGatway
{
    /**
     * @var int
     *
     * @Column(name="id1", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id1;

    /**
     * @var int
     *
     * @Column(name="tb_diretorio_id", type="integer", nullable=false)
     */
    private $tbDiretorioId;

    /**
     * @var int
     *
     * @Column(name="forma_pagamento", type="integer", nullable=false)
     */
    private $formaPagamento;

    /**
     * @var int
     *
     * @Column(name="origem_transacao", type="integer", nullable=false)
     */
    private $origemTransacao;

    /**
     * @var int
     *
     * @Column(name="gatway_pagamento", type="integer", nullable=false)
     */
    private $gatwayPagamento;

    /**
     * @var int
     * 
     * @Column(name="PDV", type="integer")
     */
    private $PDV;

    /**
     * @var string
     *
     * @Column(name="configs_gatway", type="text", length=65535, nullable=false)
     */
    private $configsGatway;

    /**
     * @var \DateTime
     *
     * @Column(name="criado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $criado;

    public function getId(){
        return $this->tbDiretorioId;
    }

    public function getConfigsGatway()
    {
        return json_decode($this->configsGatway);
    }
    
    public function getPDV(): ?int
    {
        return $this->PDV;
    }

    public function setPDV(int $PDV): DirectoryGatway
    {
        $this->PDV = $PDV;
        return $this;
    }
}
