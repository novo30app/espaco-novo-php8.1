<?php

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="indicatorFiles")
 * @ORM @Entity(repositoryClass="App\Models\Repository\IndicatorFilesRepository")
 */
class IndicatorFiles
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user", referencedColumnName="id", nullable=true)
     */
    private User $user;

    /**
     * @Column(type="string")
     * @var string
     */
    private $file;

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreated_at(): \DateTime
    {
        return $this->created_at;
    }

    public function setCreated_at(\DateTime $created_at): IndicatorFiles
    {
        $this->created_at = $created_at;
        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): IndicatorFiles
    {
        $this->user = $user;
        return $this;
    }

    public function getFile(): string
    {
        return $this->file;
    }

    public function setFile(string $file): IndicatorFiles
    {
        $this->file = $file;
        return $this;
    }
}