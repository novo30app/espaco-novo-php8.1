<?php
/**
 * Created by PhpStorm.
 * User: rwerl
 * Date: 16/04/2019
 * Time: 22:52
 */

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="cpfTmp")
 * @ORM @Entity(repositoryClass="App\Models\Repository\CpfTmpRepository")
 */
class CpfTmp
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     * @var int
     */
    private ?int $id = null;

    /**
     * @Column(type="string")
     * @var string
     */
    private string $cpf;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    private ?\DateTime $created;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCpf(): string
    {
        return $this->cpf;
    }

    public function setCpf(string $cpf): CpfTmp
    {
        $this->cpf = $cpf;
        return $this;
    }

}