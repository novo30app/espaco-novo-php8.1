<?php


namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\Mapping as ORM;

/**
 * @Entity @Table(name="accessLevel")
 */
class AccessLevel
{

    const LEVEL_CITY = 1;
    const LEVEL_STATE = 2;
    const LEVEL_NATIONAL = 3;


    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private int $id;

    /**
     * @Column(type="string")
     */
    private string $name;


    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName($name): AccessLevel
    {
        $this->name = $name;
        return $this;
    }


}