<?php

namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\Mapping as ORM;

/**
 * TbComentarios
 *
 * @Table(name="tb_comentarios")
 * @ORM @Entity(repositoryClass="App\Models\Repository\CommentsRepository")
 */

class Comments
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="tb_admin_id", referencedColumnName="id", nullable=true)
     */
    private $tbAdminId;

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="userAdmin", referencedColumnName="id", nullable=true)
     */
    private $userAdmin;

    /**
     * @Column(name="comentario", type="text")
     */
    private string $comentario;

    /**
     * @ManyToOne(targetEntity="CommentStatus")
     * @JoinColumn(name="status", referencedColumnName="id", nullable=true)
     */
    private $status;
        
    /**
     * @var \DateTime|null
     *
     * @Column(name="data_criacao", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataCriacao;

    /**
     * @Column(name="observacao", type="text")
     */
    private string $observacao;

    public function getId(): int
    {
        return $this->id;
    }
    
    public function getPessoaId(): ?int
    {
        return $this->tbPessoaId;
    }

    public function setPessoaId(?int $tbPessoaId): Comments
    {
        $this->tbPessoaId = $tbPessoaId;
        return $this;
    }

    public function getAdminId(): UserAdmin
    {
        return $this->tbAdminId;
    }

    public function setAdminId(UserAdmin $tbAdminId): Comments
    {
        $this->tbAdminId = $tbAdminId;
        return $this;
    }

    public function getUserAdmin(): UserAdmin
    {
        return $this->userAdmin;
    }

    public function setUserAdmin(UserAdmin $userAdmin): Comments
    {
        $this->userAdmin = $userAdmin;
        return $this;
    }

    public function getComentario(): string
    {
        return $this->comentario;
    }

    public function setComentario(?string $comentario): Comments
    {
        $this->comentario = $comentario;
        return $this;
    }

    public function getStatus(): CommentStatus
    {
        return $this->status;
    }

    public function setStatus(CommentStatus $status): Comments
    {
        $this->status = $status;
        return $this;
    }

    public function getDataCriacao(): ?\DateTime
    {
        return $this->dataCriacao;
    }

    public function setDataCriacao(?\DateTime $dataCriacao): Comments
    {
        $this->dataCriacao = $dataCriacao;
        return $this;
    }
}

?>