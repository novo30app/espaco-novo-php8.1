<?php


namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="ombudsmanResponse")
 * @ORM @Entity(repositoryClass="App\Models\Repository\OmbudsmanResponseRepository")
 */
class OmbudsmanResponse
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="datetime")
     */
    private \DateTime $created;

    /**
     * @Column(type="text")
     */
    private string $answer;

    /**
     * @ManyToOne(targetEntity="Ombudsman")
     * @JoinColumn(name="ombudsman", referencedColumnName="id")
     */
    private Ombudsman $ombudsman;

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="userAdmin", referencedColumnName="id", nullable=true)
     */
    private ?UserAdmin $userAdmin = null;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user", referencedColumnName="id", nullable=true)
     */
    private ?User $user = null;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function getAnswer(): string
    {
        return $this->answer;
    }

    public function setAnswer(string $answer): OmbudsmanResponse
    {
        $this->answer = $answer;
        return $this;
    }

    public function getUserAdmin():?UserAdmin
    {
        return $this->userAdmin;
    }

    public function setUserAdmin(?UserAdmin $userAdmin): OmbudsmanResponse
    {
        $this->userAdmin = $userAdmin;
        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): OmbudsmanResponse
    {
        $this->user = $user;
        return $this;
    }

    public function getOmbudsman(): Ombudsman
    {
        return $this->ombudsman;
    }

    public function setOmbudsman(Ombudsman $ombudsman): OmbudsmanResponse
    {
        $this->ombudsman = $ombudsman;
        return $this;
    }

}