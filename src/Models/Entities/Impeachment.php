<?php
/**
 * Created by PhpStorm.
 * User: rwerl
 * Date: 16/04/2019
 * Time: 22:52
 */

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="impeachment")
 * @ORM @Entity(repositoryClass="App\Models\Repository\ImpeachmentRepository")
 */
class Impeachment
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="datetime")
     */
    private \DateTime $created;


    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user", referencedColumnName="id")
     */
    private User $user;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="target", referencedColumnName="id")
     */
    private User $target;

    /**
     * @ManyToOne(targetEntity="AccessLog")
     * @JoinColumn(name="accessLog", referencedColumnName="id")
     */
    private AccessLog $accessLog;

    /**
     * @Column(type="text")
     */
    private string $msg;


    public function __construct()
    {
        $this->created = new \DateTime();
    }


    public function getId(): int
    {
        return $this->id;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): Impeachment
    {
        $this->user = $user;
        return $this;
    }

    public function getTarget(): User
    {
        return $this->target;
    }

    public function setTarget(User $target): Impeachment
    {
        $this->target = $target;
        return $this;
    }

    public function getMsg(): string
    {
        return $this->msg;
    }

    public function setMsg(string $msg): Impeachment
    {
        $this->msg = $msg;
        return $this;
    }

    public function getAccessLog(): AccessLog
    {
        return $this->accessLog;
    }

    public function setAccessLog(AccessLog $accessLog): Impeachment
    {
        $this->accessLog = $accessLog;
        return $this;
    }


}