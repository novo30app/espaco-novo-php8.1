<?php


namespace App\Models\Entities;
use Doctrine\ORM\Mapping as ORM;

/**
 * Gender
 *
 * @Table(name="tb_genero")
 * @Entity
 */
class Gender
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @Column(name="genero", type="string", length=45, nullable=true)
     */
    private $genero;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Gender
     */
    public function setId(int $id): Gender
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGenero(): ?string
    {
        return $this->genero;
    }

    /**
     * @param string|null $genero
     * @return Gender
     */
    public function setGenero(?string $genero): Gender
    {
        $this->genero = $genero;
        return $this;
    }




}
