<?php


namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\ORM\Mapping as ORM;

/**
 * EmailSend
 *
 * @Entity @Table(name="tb_envio_email")
 * @ORM @Entity(repositoryClass="App\Models\Repository\EmailSendRepository")
 */
class EmailSend
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var int|null
     *
     * @Column(name="tarefa", type="integer", nullable=true)
     */
    private $tarefa;

    /**
     * @var bool|null
     *
     * @Column(name="tipo", type="string", nullable=true)
     */
    private $tipo;

    /**
     * @var \DateTime|null
     *
     * @Column(type="datetime", name="data", nullable=true)
     */
    private $data;

    /**
     * @var bool|null
     *
     * @Column(name="mensagem", type="string", nullable=true)
     */
    private $mensagem;

    /**
     * @var bool|null
     *
     * @Column(name="email", type="string", nullable=true)
     */
    private $email;

    /**
     * @var bool|null
     *
     * @Column(name="obs", type="string", nullable=true)
     */
    private $obs;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTbPessoaId(): ?int
    {
        return $this->tbPessoaId;
    }

    public function setTbPessoaId(?int $tbPessoaId): EmailSend
    {
        $this->tbPessoaId = $tbPessoaId;
        return $this;
    }

    public function getTask(): ?int
    {
        return $this->tarefa;
    }

    public function setTask(?int $tarefa): EmailSend
    {
        $this->tarefa = $tarefa;
        return $this;
    }

    public function getType(): ?string
    {
        return $this->tipo;
    }

    public function setType(?string $tipo): EmailSend
    {
        $this->tipo = $tipo;
        return $this;
    }

    public function getDate(): ?\DateTime
    {
        return new \DateTime($this->data);
    }

    public function setDate(?\DateTime $data): EmailSend
    {
        $this->data = $data;
        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->mensagem;
    }

    public function setMessage(?string $mensagem): EmailSend
    {
        $this->mensagem = $mensagem;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): EmailSend
    {
        $this->email = $email;
        return $this;
    }

    public function getObs(): ?string
    {
        return $this->obs;
    }

    public function setObs(?string $obs): EmailSend
    {
        $this->obs = $obs;
        return $this;
    }
}