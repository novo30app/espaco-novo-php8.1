<?php


namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\ORM\Mapping as ORM;

/**
 * Phone
 *
 * @Table(name="tb_telefone", indexes={@Index(name="idx_pessoa_tipo", columns={"tb_pessoa_id", "tb_tipo_telefone_id"}), @Index(name="idx_pessoa", columns={"tb_pessoa_id"})})
 * @ORM @Entity(repositoryClass="App\Models\Repository\PhoneRepository")
 */
class Phone
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @Column(name="tb_pessoa_id", type="integer", nullable=false)
     */
    private $tbPessoaId;

    /**
     * @var int
     *
     * @Column(name="tb_tipo_telefone_id", type="integer", nullable=false)
     */
    private $tbTipoTelefoneId;

    /**
     * @var string
     *
     * @Column(name="pais", type="string", length=5, nullable=false, options={"default"="br","fixed"=true})
     */
    private $pais = 'br';

    /**
     * @var string
     *
     * @Column(name="ddi", type="string", length=5, nullable=false, options={"default"="55","fixed"=true})
     */
    private $ddi = '55';

    /**
     * @var string|null
     *
     * @Column(name="telefone", type="string", length=30, nullable=true)
     */
    private $telefone;

    /**
     * @Column(name="contact", type="boolean", options={"default"="1","fixed"=true})
     */
    private $contact = '1';

    public function getId(): int
    {
        return $this->id;
    }

    public function getTbPessoaId(): int
    {
        return $this->tbPessoaId;
    }

    public function setTbPessoaId(int $tbPessoaId): Phone
    {
        $this->tbPessoaId = $tbPessoaId;
        return $this;
    }

    public function getTbTipoTelefoneId(): int
    {
        return $this->tbTipoTelefoneId;
    }

    public function setTbTipoTelefoneId(int $tbTipoTelefoneId): Phone
    {
        $this->tbTipoTelefoneId = $tbTipoTelefoneId;
        return $this;
    }

    public function getPais(): string
    {
        return $this->pais;
    }

    public function setPais(string $pais): Phone
    {
        $this->pais = $pais;
        return $this;
    }

    public function getDdi(): string
    {
        return $this->ddi;
    }

    public function setDdi(string $ddi): Phone
    {
        $this->ddi = $ddi;
        return $this;
    }

    public function getTelefone(bool $onlyNumbers = false): ?string
    {
        if ($onlyNumbers){
            return Utils::onlyNumbers($this->telefone);
        }
        return $this->telefone;
    }

    public function setTelefone(?string $telefone): Phone
    {
        $this->telefone = $telefone;
        return $this;
    }

    public function getContact(): ?bool
    {
        return $this->contact;
    }

    public function setContact(bool $contact): Phone
    {
        $this->contact = $contact;
        return $this;
    }
}
