<?php


namespace App\Models\Entities;
use Doctrine\ORM\Mapping as ORM;

/**
 * Address
 *
 * @Table(name="tb_endereco", indexes={@Index(name="idx_tb_endereco_cidade", columns={"cidade"}), @Index(name="idx_tb_endereco_tb_estado_id", columns={"tb_estado_id"}), @Index(name="idx_tb_endereco_cidade_estado", columns={"cidade", "estado"}), @Index(name="idx_tb_cidade_id", columns={"tb_cidade_id"}), @Index(name="idx_pessoa", columns={"tb_pessoa_id"})})
 * @ORM @Entity(repositoryClass="App\Models\Repository\AddressRepository")
 */
class Address
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @Column(name="tb_pessoa_id", type="integer", nullable=false)
     */
    private $tbPessoaId;

    /**
     * @var int|null
     *
     * @Column(name="tb_tipo_endereco_id", type="integer", nullable=true)
     */
    private $tbTipoEnderecoId;

    /**
     * @var bool|null
     *
     * @Column(name="reside_exterior", type="boolean", nullable=true)
     */
    private $resideExterior;

    /**
     * @var int|null
     *
     * @Column(name="tb_pais_id", type="integer", nullable=true)
     */
    private $tbPaisId;

    /**
     * @var int|null
     *
     * @Column(name="tb_estado_id", type="integer", nullable=true)
     */
    private $tbEstadoId;

    /**
     * @var string|null
     *
     * @Column(name="estado", type="string", length=255, nullable=true)
     */
    private $estado;

    /**
     * @var int|null
     *
     * @Column(name="tb_cidade_id", type="integer", nullable=true)
     */
    private $tbCidadeId;

    /**
     * @var string|null
     *
     * @Column(name="cidade", type="string", length=255, nullable=true)
     */
    private $cidade;

    /**
     * @var string|null
     *
     * @Column(name="cep", type="string", length=10, nullable=true)
     */
    private $cep;

    /**
     * @var string
     *
     * @Column(name="cep_status", type="string", length=0, nullable=false)
     */
    private $cepStatus = '0';

    /**
     * @var string|null
     *
     * @Column(name="endereco", type="string", length=150, nullable=true)
     */
    private $endereco;

    /**
     * @var string|null
     *
     * @Column(name="numero", type="string", length=6, nullable=true)
     */
    private $numero;

    /**
     * @var string|null
     *
     * @Column(name="complemento", type="string", length=200, nullable=true)
     */
    private $complemento;

    /**
     * @var string|null
     *
     * @Column(name="bairro", type="string", length=45, nullable=true)
     */
    private $bairro;

    public function getId(): int
    {
        return $this->id;
    }

    public function getTbPessoaId(): int
    {
        return $this->tbPessoaId;
    }

    public function setTbPessoaId(int $tbPessoaId): Address
    {
        $this->tbPessoaId = $tbPessoaId;
        return $this;
    }

    public function getTbTipoEnderecoId(): ?int
    {
        return $this->tbTipoEnderecoId;
    }

    public function setTbTipoEnderecoId(?int $tbTipoEnderecoId): Address
    {
        $this->tbTipoEnderecoId = $tbTipoEnderecoId;
        return $this;
    }

    public function getResideExterior(): ?bool
    {
        return $this->resideExterior;
    }

    public function setResideExterior(?bool $resideExterior): Address
    {
        $this->resideExterior = $resideExterior;
        return $this;
    }

    public function getTbPaisId(): ?int
    {
        return $this->tbPaisId;
    }

    public function setCountryId(?int $tbPaisId): Address
    {
        $this->tbPaisId = $tbPaisId;
        return $this;
    }

    public function getStateId(): ?int
    {
        return $this->tbEstadoId;
    }

    public function setStateId(?int $tbEstadoId): Address
    {
        $this->tbEstadoId = $tbEstadoId;
        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(?string $estado): Address
    {
        $this->estado = $estado;
        return $this;
    }

    public function getTbCidadeId(): ?int
    {
        return $this->tbCidadeId;
    }

    public function setCityId(?int $tbCidadeId): Address
    {
        $this->tbCidadeId = $tbCidadeId;
        return $this;
    }

    public function getCidade(): ?string
    {
        return $this->cidade;
    }

    public function setCidade(?string $cidade): Address
    {
        $this->cidade = $cidade;
        return $this;
    }

    public function getCep(): ?string
    {
        return $this->cep;
    }

    public function setCep(?string $cep): Address
    {
        $this->cep = $cep;
        return $this;
    }

    public function getCepStatus(): string
    {
        return $this->cepStatus;
    }

    public function setCepStatus(string $cepStatus): Address
    {
        $this->cepStatus = $cepStatus;
        return $this;
    }

     public function getEndereco(): ?string
    {
        return $this->endereco;
    }

    public function setEndereco(?string $endereco): Address
    {
        $this->endereco = $endereco;
        return $this;
    }

    public function getNumero(): ?string
    {
        return $this->numero;
    }

    public function setNumero(?string $numero): Address
    {
        $this->numero = $numero;
        return $this;
    }

    public function getComplemento(): ?string
    {
        return $this->complemento;
    }

    public function setComplemento(?string $complemento): Address
    {
        $this->complemento = $complemento;
        return $this;
    }

    public function getBairro(): ?string
    {
        return $this->bairro;
    }

    public function setBairro(?string $bairro): Address
    {
        $this->bairro = $bairro;
        return $this;
    }

}
