<?php
/**
 * Created by PhpStorm.
 * User: rwerl
 * Date: 16/04/2019
 * Time: 22:52
 */

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="selectiveProcess2022Module1")
 * @ORM @Entity(repositoryClass="App\Models\Repository\SelectiveProcess2022Module1Repository")
 */
class SelectiveProcess2022Module1
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="datetime")
     */
    private \DateTime $created;

    /**
     * @OneToOne(targetEntity="User")
     * @JoinColumn(name="candidate", referencedColumnName="id")
     */
    private User $candidate;

    /**
     * @ManyToOne(targetEntity="CandidatePost")
     * @JoinColumn(name="candidatePost", referencedColumnName="id", nullable=true)
     */
    private ?CandidatePost $candidatePost = null;

    /**
     * @Column(type="text")
     */
    private string $academicEducation = '';

    /**
     * @Column(type="text")
     */
    private string $professionalHistory = '';

    /**
     * @Column(type="text")
     */
    private string $personalHistory = '';

    /**
     * @Column(type="integer")
     */
    private int $year = 0;

    /**
     * @Column(type="integer", options={"unsigned":true, "default":0})
     */
    private int $status = 0;

    /**
     * @Column(type="integer")
     */
    private int $numberOfVotes = 0;

    /**
     * @Column(type="float")
     */
    private float $collection = 0;

    /**
     * @Column(type="string")
     */
    private string $targetAudience = '';

    /**
     * @Column(type="string")
     */
    private string $visualIdentity = '';

    /**
     * @Column(type="text")
     */
    private string $flags = '';

    /**
     * @Column(type="text")
     */
    private string $team = '';

    /**
     * @Column(type="text")
     */
    private string $lessons = '';

    /**
     * @Column(type="text")
     */
    private string $motivations = '';

    /**
     * @Column(type="text")
     */
    private string $region = '';

    /**
     * @Column(type="text")
     */
    private string $evaluationInstitution = '';

    /**
     * @Column(type="text")
     */
    private string $evaluationInstitutionAgents = '';

    /**
     * @Column(type="text")
     */
    private string $evaluationGovernmentFederal = '';

    /**
     * @Column(type="text")
     */
    private string $evaluationGovernmentState = '';

    /**
     * @Column(type="string")
     */
    private string $facebook = '';

    /**
     * @Column(type="string")
     */
    private string $instagram = '';

    /**
     * @Column(type="string")
     */
    private string $twitter = '';

    /**
     * @Column(type="string")
     */
    private string $youtube = '';

    /**
     * @Column(type="string")
     */
    private string $desiredPost = '';

    /**
     * @Column(type="boolean")
     */
    private bool $exCandidate = true;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function getDesiredPost(): string
    {
        return $this->desiredPost;
    }

    public function setDesiredPost(string $desiredPost): SelectiveProcess2022Module1
    {
        $this->desiredPost = $desiredPost;
        return $this;
    }

    public function getCandidate(): User
    {
        return $this->candidate;
    }

    public function setCandidate(User $candidate): SelectiveProcess2022Module1
    {
        $this->candidate = $candidate;
        return $this;
    }

    public function getCandidatePost(): ?CandidatePost
    {
        return $this->candidatePost;
    }

    public function setCandidatePost(?CandidatePost $candidatePost): SelectiveProcess2022Module1
    {
        $this->candidatePost = $candidatePost;
        return $this;
    }

    public function getAcademicEducation(): string
    {
        return $this->academicEducation;
    }

    public function setAcademicEducation(string $academicEducation): SelectiveProcess2022Module1
    {
        $this->academicEducation = $academicEducation;
        return $this;
    }

    public function getProfessionalHistory(): string
    {
        return $this->professionalHistory;
    }

    public function setProfessionalHistory(string $professionalHistory): SelectiveProcess2022Module1
    {
        $this->professionalHistory = $professionalHistory;
        return $this;
    }

    public function getYear(): int
    {
        return $this->year;
    }

    public function setYear(int $year): SelectiveProcess2022Module1
    {
        $this->year = $year;
        return $this;
    }

    public function getNumberOfVotes(): int
    {
        return $this->numberOfVotes;
    }

    public function setNumberOfVotes(int $numberOfVotes): SelectiveProcess2022Module1
    {
        $this->numberOfVotes = $numberOfVotes;
        return $this;
    }

    public function getCollection()
    {
        return $this->collection;
    }

    public function setCollection($collection)
    {
        $this->collection = $collection;
        return $this;
    }

    public function getTargetAudience(): string
    {
        return $this->targetAudience;
    }

    public function setTargetAudience(string $targetAudience): SelectiveProcess2022Module1
    {
        $this->targetAudience = $targetAudience;
        return $this;
    }

    public function getVisualIdentity(): string
    {
        return $this->visualIdentity;
    }

    public function setVisualIdentity(string $visualIdentity): SelectiveProcess2022Module1
    {
        $this->visualIdentity = $visualIdentity;
        return $this;
    }

    public function getFlags(): string
    {
        return $this->flags;
    }

    public function setFlags(string $flags): SelectiveProcess2022Module1
    {
        $this->flags = $flags;
        return $this;
    }

    public function getTeam(): string
    {
        return $this->team;
    }

    public function setTeam(string $team): SelectiveProcess2022Module1
    {
        $this->team = $team;
        return $this;
    }

    public function getLessons(): string
    {
        return $this->lessons;
    }

    public function setLessons(string $lessons): SelectiveProcess2022Module1
    {
        $this->lessons = $lessons;
        return $this;
    }

    public function getMotivations(): string
    {
        return $this->motivations;
    }

    public function setMotivations(string $motivations): SelectiveProcess2022Module1
    {
        $this->motivations = $motivations;
        return $this;
    }

    public function getRegion(): string
    {
        return $this->region;
    }

    public function setRegion(string $region): SelectiveProcess2022Module1
    {
        $this->region = $region;
        return $this;
    }

    public function getEvaluationInstitution(): string
    {
        return $this->evaluationInstitution;
    }

    public function setEvaluationInstitution(string $evaluationInstitution): SelectiveProcess2022Module1
    {
        $this->evaluationInstitution = $evaluationInstitution;
        return $this;
    }

    public function getEvaluationInstitutionAgents(): string
    {
        return $this->evaluationInstitutionAgents;
    }

    public function setEvaluationInstitutionAgents(string $evaluationInstitutionAgents): SelectiveProcess2022Module1
    {
        $this->evaluationInstitutionAgents = $evaluationInstitutionAgents;
        return $this;
    }

    public function getEvaluationGovernmentFederal(): string
    {
        return $this->evaluationGovernmentFederal;
    }

    public function setEvaluationGovernmentFederal(string $evaluationGovernmentFederal): SelectiveProcess2022Module1
    {
        $this->evaluationGovernmentFederal = $evaluationGovernmentFederal;
        return $this;
    }

    public function getEvaluationGovernmentState(): string
    {
        return $this->evaluationGovernmentState;
    }

    public function setEvaluationGovernmentState(string $evaluationGovernmentState): SelectiveProcess2022Module1
    {
        $this->evaluationGovernmentState = $evaluationGovernmentState;
        return $this;
    }

    public function getFacebook(): string
    {
        return $this->facebook;
    }

    public function setFacebook(string $facebook): SelectiveProcess2022Module1
    {
        $this->facebook = $facebook;
        return $this;
    }

    public function getInstagram(): string
    {
        return $this->instagram;
    }

    public function setInstagram(string $instagram): SelectiveProcess2022Module1
    {
        $this->instagram = $instagram;
        return $this;
    }

    public function getTwitter(): string
    {
        return $this->twitter;
    }

    public function setTwitter(string $twitter): SelectiveProcess2022Module1
    {
        $this->twitter = $twitter;
        return $this;
    }

    public function getYoutube(): string
    {
        return $this->youtube;
    }

    public function setYoutube(string $youtube): SelectiveProcess2022Module1
    {
        $this->youtube = $youtube;
        return $this;
    }

    public function getPersonalHistory(): string
    {
        return $this->personalHistory;
    }

    public function setPersonalHistory(string $personalHistory): SelectiveProcess2022Module1
    {
        $this->personalHistory = $personalHistory;
        return $this;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setStatus(int $status): SelectiveProcess2022Module1
    {
        $this->status = $status;
        return $this;
    }

    public function isExCandidate(): bool
    {
        return $this->exCandidate;
    }

    public function setExCandidate(bool $exCandidate): SelectiveProcess2022Module1
    {
        $this->exCandidate = $exCandidate;
        return $this;
    }

}