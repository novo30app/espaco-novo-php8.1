<?php


namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\ORM\Mapping as ORM;

/**
 * BellowSigned
 *
 * @Entity @Table(name="bellowSigned")
 * @ORM @Entity(repositoryClass="App\Models\Repository\BellowSignedRepository")
 */
class BellowSigned
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private \DateTime $created;

    /**
     * @Column(type="string")
     */
    private string $name = '';

    /**
     * @Column(type="string", unique=true)
     */
    private string $email = '';

    /**
     *
     * @Column(type="string", nullable=true)
     */
    private $phone;

    /**
     *
     * @Column(type="string", nullable=true)
     */
    private $cellPhone;

    /**
     * @ManyToOne(targetEntity="City")
     * @JoinColumn(name="city", referencedColumnName="id", nullable=true)
     */
    private ?City $city = null;

    /**
     * @ManyToOne(targetEntity="State")
     * @JoinColumn(name="state", referencedColumnName="id", nullable=true)
     */
    private ?State $state = null;

    /**
     * @Column(type="boolean")
     */
    private bool $news = true;

    /**
     * @Column(type="string")
     */
    private string $theme;

    public function getId()
    {
        return $this->id;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function setCreated(\DateTime $created): BellowSigned
    {
        $this->created = $created;
        return $this;
    }

    public function setName(string $name): BellowSigned
    {
        $this->name = $name;
        return $this;
    }

    public function setEmail(string $email): BellowSigned
    {
        $this->email = $email;
        return $this;
    }

    public function setPhone(string $phone): BellowSigned
    {
        $this->phone = $phone;
        return $this;
    }

    public function setCellPhone(string $cellPhone): BellowSigned
    {
        $this->cellPhone = $cellPhone;
        return $this;
    }

    public function setCity(?City $city): BellowSigned
    {
        $this->city = $city;
        return $this;
    }

    public function setState(?State $state): BellowSigned
    {
        $this->state = $state;
        return $this;
    }

    public function setNews(bool $news): BellowSigned
    {
        $this->news = $news;
        return $this;
    }

    public function setTheme(string $theme): BellowSigned
    {
        $this->theme = $theme;
        return $this;
    }
}