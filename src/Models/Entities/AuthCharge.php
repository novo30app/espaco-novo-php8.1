<?php
/**
 * Created by PhpStorm.
 * User: rwerl
 * Date: 16/04/2019
 * Time: 22:52
 */

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="authCharge")
 */
class AuthCharge
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    private $created;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $lastId = 0;


    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function getLastId(): int
    {
        return $this->lastId;
    }

    public function setLastId(int $lastId): AuthCharge
    {
        $this->lastId = $lastId;
        return $this;
    }


}