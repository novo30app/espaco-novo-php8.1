<?php
/**
 * Created by PhpStorm.
 * User: rwerl
 * Date: 16/04/2019
 * Time: 22:52
 */

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="selectiveProcess2022Module1Questions")
 * @ORM @Entity(repositoryClass="App\Models\Repository\SelectiveProcess2022Module1QuestionRepository")
 */
class SelectiveProcess2022Module1Question
{

    const TYPE_ECONOMY = 1;
    const TYPE_POLITIC = 2;
    const TYPE_SOCIETY = 3;
    const TYPE_STATE = 4;
    const TYPE_JUSTICE = 5;

    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="boolean", options={"default":1})
     */
    private bool $status = true;

    /**
     * @Column(type="integer")
     */
    private int $type = 1;

    /**
     * @Column(type="text")
     */
    private string $text = '';

    /**
     * @Column(type="string", length=1, options={"fixed" = true})
     */
    private string $answer = 'a';

    public function getId(): int
    {
        return $this->id;
    }

    public function isStatus(): bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): SelectiveProcess2022Module1Question
    {
        $this->status = $status;
        return $this;
    }

    public function getTypeString(): string
    {
        switch ((int)$this->type) {
            case self::TYPE_ECONOMY:
                return 'Economia';
            case self::TYPE_POLITIC:
                return 'Política';
            case self::TYPE_SOCIETY:
                return 'Sociedade';
            case self::TYPE_STATE:
                return 'Estado';
            case self::TYPE_JUSTICE:
                return 'Justiça';
            default:
                return 'Desconhecido';
        }
    }


    public function getType(): int
    {
        return $this->type;
    }

    public function setType(int $type): SelectiveProcess2022Module1Question
    {
        $this->type = $type;
        return $this;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): SelectiveProcess2022Module1Question
    {
        $this->text = $text;
        return $this;
    }

    public function getAnswer(): string
    {
        return $this->answer;
    }

    public function setAnswer(string $answer): SelectiveProcess2022Module1Question
    {
        $this->answer = $answer;
        return $this;
    }

}