<?php

namespace App\Models\Entities;

use App\Helpers\Utils;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;
/**
 * @Entity @Table(name="cron")
 * @ORM @Entity(repositoryClass="App\Models\Repository\CronRepository")
 */
class Cron
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     * @Column(name="tarefa", type="integer", nullable=true)
     */
    private $tarefa;

    /**
     * @var \DateTime|null
     * @Column(name="data", type="datetime", nullable=true)
     */
    private $data;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTarefa(): ?int
    {
        return $this->tarefa;
    }

    public function setTarefa(?int $tarefa): Cron
    {
        $this->tarefa = $tarefa;
        return $this;
    }

    public function getData(): ?\DateTime
    {
        return $this->data;
    }

    public function setData(?\DateTime $data): Cron
    {
        $this->data = $data;
        return $this;
    }
}