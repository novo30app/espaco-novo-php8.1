<?php

namespace App\Models\Entities;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="localDocuments")
 * @ORM @Entity(repositoryClass="App\Models\Repository\LocalDocumentsRepository")
 */
class LocalDocuments
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private int $id;

    /**
     * @Column(type="datetime")
     */
    private \Datetime $created;

    /**
     * @Column(type="integer")
     */
    private int $author;

    /**
     * @Column(type="string")
     */
    private string $title;

    /**
     * @Column(type="string")
     */
    private string $description;

    /**
     * @Column(type="integer")
     */
    private int $state;

    /**
     * @Column(type="integer")
     */
    private int $city;

    /**
     * @Column(type="string")
     */
    private string $file;

    /**
     * @Column(type="boolean")
     */
    private bool $active;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreated(): \Datetime
    {
        return $this->created;
    }

    public function setCreated(\Datetime $created): LocalDocuments
    {
        $this->created = $created;
        return $this;
    }

    public function getAuthor(): int
    {
        return $this->author;
    }

    public function setAuthor(int $author): LocalDocuments
    {
        $this->author = $author;
        return $this;
    }
    
    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): LocalDocuments
    {
        $this->title = $title;
        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): LocalDocuments
    {
        $this->description = $description;
        return $this;
    }

    public function getState(): int
    {
        return $this->state;
    }

    public function setState(int $state): LocalDocuments
    {
        $this->state = $state;
        return $this;
    }

    public function getCity(): int
    {
        return $this->city;
    }

    public function setCity(int $city): LocalDocuments
    {
        $this->city = $city;
        return $this;
    }

    public function getFile(): string
    {
        return $this->file;
    }

    public function setFile(string $file): LocalDocuments
    {
        $this->file = $file;
        return $this;
    }

    public function getActive(): string
    {
        return $this->active;
    }

    public function setActive(string $active): LocalDocuments
    {
        $this->active = $active;
        return $this;
    }
}