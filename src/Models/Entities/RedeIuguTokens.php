<?php

namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\Mapping as ORM;

/**
 * @Entity @Table(name="redeIuguTokens")
 * @ORM @Entity(repositoryClass="App\Models\Repository\RedeIuguTokensRepository")
 */
class RedeIuguTokens
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var bool|null
     * @Column(name="used", type="boolean", length=255, nullable=true)
     */
    private $used;

    /**
     * @var string|null
     * @Column(name="rede", type="string", length=255, nullable=true)
     */
    private $rede;

    /**
     * @var string|null
     * @Column(name="iugu", type="string", length=255, nullable=true)
     */
    private $iugu;

    /**
     * @var string|null
     * @Column(name="user", type="integer", nullable=true)
     */
    private $user;

    /**
     * @var string|null
     * @Column(name="refresh", type="datetime", nullable=true)
     */
    private $refresh;

    public function getId(): int
    {
        return $this->id;
    }

    public function getUsed(): bool
    {
        return $this->used;
    }

    public function setUsed(bool $used): RedeIuguTokens
    {
        $this->used = $used;
        return $this;
    }

    public function getRede(): string
    {
        return $this->rede;
    }

    public function setRede(string $rede): RedeIuguTokens
    {
        $this->rede = $rede;
        return $this;
    }

    public function getIugu(): string
    {
        return $this->iugu;
    }

    public function setIugu(string $iugu): RedeIuguTokens
    {
        $this->iugu = $iugu;
        return $this;
    }

    public function getUser(): int
    {
        return $this->user;
    }

    public function setUser(int $user): RedeIuguTokens
    {
        $this->user = $user;
        return $this;
    }

    public function getRefresh(): string
    {
        return $this->refresh;
    }

    public function setRefresh(\Datetime $refresh): RedeIuguTokens
    {
        $this->refresh = $refresh;
        return $this;
    }
}
