<?php


namespace App\Models\Entities;

use App\Helpers\Utils;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="paymentRequirements")
 * @ORM @Entity(repositoryClass="App\Models\Repository\PaymentRequirementsRepository")
 */
class PaymentRequirements
{

    const REQUEST_STATUS_REQUESTED = 0;
    const REQUEST_STATUS_PENDENCY = 1;
    const REQUEST_STATUS_SCHEDULED = 2;
    const REQUEST_STATUS_PAID = 3;
    const REQUEST_WAITING_APPROVAL = 4;
	
	const APPROVED_USER = 1; // carimbo do usuário
	const APPROVED_MANAGER = 2; // carimbo do gerente responsável pela área
	const APPROVED_FINANCIAL = 3; // carimbo do financeiro
	const APPROVED_TREASURY = 4; // carimbo da tesouraria
	const APPROVED_ACCOUNTING = 5; // carimbo da contabilidade


    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="datetime")
     */
    private \DateTime $created;

    /**
     * @ManyToOne(targetEntity="UserAdmin")
     * @JoinColumn(name="user", referencedColumnName="id")
     */
    private UserAdmin $user;

    /**
     * @Column(type="integer")
     */
    private int $origin;

    /**
     * @Column(type="integer")
     */
    private int $type;

    /**
     * @Column(type="text")
     */
    private string $description;

    /**
     * @Column(type="string")
     */
    private string $beneficiary = '';

    /**
     * @Column(type="string", nullable=true)
     */
    private ?string $beneficiaryFile;

    /**
     * @Column(type="string")
     */
    private string $reason = '';

    /**
     * @Column(type="string")
     */
    private string $accommodationHotel = '';

    /**
     * @Column(type="date", nullable=true)
     */
    private ?\DateTime $accommodationStart = null;

    /**
     * @Column(type="date", nullable=true)
     */
    private ?\DateTime $accommodationEnd = null;

    /**
     * @Column(type="string")
     */
    private string $taxiRoute = '';

    /**
     * @Column(type="string")
     */
    private string $campaignMaterial = '';

    /**
     * @Column(type="string")
     */
    private string $fuelPlate = '';


    /**
     * @Column(type="integer")
     */
    private int $activeType = 0;

    /**
     * @Column(type="string")
     */
    private string $laborOccupation = '';

    /**
     * @Column(type="integer")
     */
    private int $accountConsumer = 0;

    /**
     * @Column(type="float")
     */
    private float $nfValue = 0;

    /**
     * @Column(type="string")
     */
    private string $nfNumber = '';

    /**
     * @Column(type="string")
     */
    private string $attachmentNf = '';

    /**
     * @Column(type="string")
     */
    private string $attachmentContract = '';

    /**
     * @Column(type="string")
     */
    private string $attachmentBillet = '';

    /**
     * @Column(type="string", nullable=true)
     */
    private ?string $barCode = '';

    /**
     * @Column(type="integer")
     */
    private int $paymentMethod = 0;

    /**
     * @Column(type="integer")
     */
    private int $paymentType = 0;

    /**
     * @Column(type="integer")
     */
    private int $installmentsNumber = 0;

    /**
     * @Column(type="float")
     */
    private float $paymentValue = 0;

    /**
     * @Column(type="date")
     */
    private ?\DateTime $paymentDueDate = null;

    /**
     * @Column(type="string")
     */
    private string $paymentDoc = '';

    /**
     * @Column(type="string")
     */
    private string $paymentName = '';

    /**
     * @Column(type="string")
     */
    private string $paymentBank = '';

    /**
     * @ManyToOne(targetEntity="Events")
     * @JoinColumn(name="event", referencedColumnName="id", nullable=true)
     */
    private ?Events $event = null;

    /**
     * @Column(type="string")
     */
    private string $provider;

    /**
     * @Column(type="string")
     */
    private string $paymentBankAgency = '';

    /**
     * @Column(type="string")
     */
    private string $paymentBankAccount = '';

    /**
     * @Column(type="string")
     */
    private string $paymentBankAccountType = '';

    /**
     * @Column(type="integer", options = {"default": 0})
     */
    private int $requestStatus = 0;

    /**
     * @Column(type="text")
     */
    private string $pendency = '';

    /**
     * @Column(type="date", nullable=true)
     */
    private ?\DateTime $payDay = null;

    /**
     * @Column(type="date", nullable=true)
     */
    private ?\DateTime $scheduledDate = null;

    /**
     * @Column(type="string")
     */
    private string $attachmentPayment = '';

    /**
     * @Column(type="string")
     */
    private string $bond = '';

    /**
     * @Column(type="string")
     */
    private string $cpf = '';

    /**
     * @Column(type="float")
     */
    private float $value = 0;

    /**
     * @Column(type="boolean", nullable=true, options = {"default": false})
     */
    private bool $locationDirectory = false;

    /**
     * @ManyToOne(targetEntity="City")
     * @JoinColumn(name="city", referencedColumnName="id", nullable=true)
     */
    private ?City $city = null;

    /**
     * @ManyToOne(targetEntity="State")
     * @JoinColumn(name="state", referencedColumnName="id", nullable=true)
     */
    private ?State $state = null;

    /**
     * @ManyToOne(targetEntity="Directory")
     * @JoinColumn(name="directory", referencedColumnName="id", nullable=true)
     */
    private ?Directory $directory = null;

    /**
     * @Column(type="string", nullable=true)
     */
    private ?string $thrustFile;

    /**
     * @ManyToOne(targetEntity="Directory")
     * @JoinColumn(name="directoryTransfer", referencedColumnName="id", nullable=true)
     */
    private ?Directory $directoryTransfer;
	
    /**
     * @Column(type="string")
     */
	private string $travels;
	
    /**
     * @Column(type="string")
     */
	private ?Contract $contract = null;
	
    /**
     * @Column(type="string")
     */
	private string $costCenter = '';
	
	/**
	 * @Column(type="integer", nullable=true, options = {"default": 1})
	 */
	private int $approved = 1;

    public function __construct()
    {
        $this->created = new \DateTime();
	    $this->travels = new ArrayCollection();
    }

    public function getRequestStatusString(): string
    {
        switch ($this->requestStatus) {
            case 0:
                return 'Pagamento Solicitado';
            case 1:
                return 'Pendência';
            case 2:
                return 'Pagamento Agendado';
            case 3:
                return 'Pagamento Efetuado';
            case 4:
                return 'Aguardando Aprovação';
            case 5:
                return 'Cancelado';
        }
    }

    public function isLocationDirectory(): bool
    {
        return $this->locationDirectory;
    }

    public function setLocationDirectory(bool $locationDirectory): PaymentRequirements
    {
        $this->locationDirectory = $locationDirectory;
        return $this;
    }

    public function getRequestStatus(): int
    {
        return $this->requestStatus;
    }

    public function setRequestStatus($requestStatus)
    {
        $this->requestStatus = $requestStatus;
        return $this;
    }

    public function getEvent(): ?Events
    {
        return $this->event;
    }

    public function setEvent(?Events $event): PaymentRequirements
    {
        $this->event = $event;
        return $this;
    }

    public function getPendency(): string
    {
        return $this->pendency;
    }

    public function setPendency($pendency)
    {
        $this->pendency = $pendency;
        return $this;
    }

    public function getPayDay(): ?\DateTime
    {
        return $this->payDay;
    }

    public function setPayDay(?\DateTime $payDay): PaymentRequirements
    {
        $this->payDay = $payDay;
        return $this;
    }

    public function getScheduledDate(): ?\DateTime
    {
        return $this->scheduledDate;
    }

    public function setScheduledDate(?\DateTime $scheduledDate): PaymentRequirements
    {
        $this->scheduledDate = $scheduledDate;
        return $this;
    }

    public function getAccountConsumerStr(): string
    {
        switch ($this->accountConsumer) {
            case 1:
                return 'Luz';
            case 2:
                return 'Água';
            case 3:
                return 'Telefone';
            case 4:
                return 'Condomínio';
            default:
                return 'Desconhecido';
        }
    }

    public function getOriginStr(): string
    {
        switch ($this->origin) {
            case 1:
                return 'Campanha Eleitoral';
            case 2:
                return 'Política da Mulher';
            case 3:
                return 'Outros recursos';
            default:
                return 'Desconhecido';
        }
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function setType($type): PaymentRequirements
    {
        $this->type = $type;
        return $this;
    }

    public function getTypeStr(): string
    {
        switch ($this->type) {
            case 1:
                return 'Hospedagem/Viagens';
            case 2:
                return 'Taxi';
            case 3:
                return 'Material Impresso para Campanha';
            case 4:
                return 'CombustÃ­veis';
            case 5:
                return 'Evento';
            case 6:
                return 'Compra de Ativos';
            case 7:
                return 'MÃ£o de Obra/PrestaÃ§Ã£o de ServiÃ§os';
            case 8:
                return 'Despesa de EscritÃ³rio';
            case 9:
                return 'Contas de Consumo';
            case 10:
                return 'Gastos com AlimentaÃ§Ã£o';
            case 11:
                return 'Outros';
            case 13:
                return 'Gastos com Impulsionamentos';
            case 14:
                return 'Serviços Jurídicos';
            case 15:
                return 'Serviços Contábeis';
            default:
                return 'Desconhecido';
        }
    }

    public function getActiveTypeStr(): string
    {
        switch ($this->activeType) {
            case 1:
                return 'Impressora';
            case 2:
                return 'Computador';
            case 3:
                return 'MÃ¡quinas';
            default:
                return 'Desconhecido';
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBeneficiary(): string
    {
        return $this->beneficiary;
    }

    public function setBeneficiary($beneficiary): PaymentRequirements
    {
        $this->beneficiary = $beneficiary;
        return $this;
    }

    public function getBeneficiaryFile(): ?string
    {
        $beneficiaryFile = $this->beneficiaryFile;
        if ($beneficiaryFile) $beneficiaryFile = Utils::formatAttachment($this->beneficiaryFile);
        return $beneficiaryFile;
    }

    public function setBeneficiaryFile(string $beneficiaryFile): PaymentRequirements
    {
        $this->beneficiaryFile = $beneficiaryFile;
        return $this;
    }

    public function getReason(): string
    {
        return $this->reason;
    }

    public function setReason($reason): PaymentRequirements
    {
        $this->reason = $reason;
        return $this;
    }


    public function getBond(): string
    {
        return $this->bond;
    }

    public function setBond($bond): PaymentRequirements
    {
        $this->bond = $bond;
        return $this;
    }

    public function getCpf(): string
    {
        return $this->cpf;
    }

    public function setCpf($cpf): PaymentRequirements
    {
        $this->cpf = $cpf;
        return $this;
    }

    public function getValue(): float
    {
        return $this->value;
    }

    public function setValue($value): PaymentRequirements
    {
        $this->value = $value;
        return $this;
    }


    public function getAttachmentBillet(): string
    {
        return Utils::formatAttachment($this->attachmentBillet);
    }

    public function setAttachmentBillet(string $attachmentBillet): PaymentRequirements
    {
        $this->attachmentBillet = $attachmentBillet;
        return $this;
    }

    public function getBarCode(): ?string
    {
        return $this->barCode;
    }

    public function setBarCode(?string $barCode): PaymentRequirements
    {
        $this->barCode = $barCode;
        return $this;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function getUser(): UserAdmin
    {
        return $this->user;
    }

    public function setUser(UserAdmin $user): PaymentRequirements
    {
        $this->user = $user;
        return $this;
    }

    public function getOrigin(): int
    {
        return $this->origin;
    }

    public function setOrigin(int $origin): PaymentRequirements
    {
        $this->origin = $origin;
        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): PaymentRequirements
    {
        $this->description = $description;
        return $this;
    }

    public function getAccommodationHotel(): string
    {
        return $this->accommodationHotel;
    }

    public function setAccommodationHotel(string $accommodationHotel): PaymentRequirements
    {
        $this->accommodationHotel = $accommodationHotel;
        return $this;
    }

    public function getAccommodationStart(): ?\DateTime
    {
        return $this->accommodationStart;
    }

    public function setAccommodationStart(?\DateTime $accommodationStart): PaymentRequirements
    {
        $this->accommodationStart = $accommodationStart;
        return $this;
    }

    public function getAccommodationEnd(): ?\DateTime
    {
        return $this->accommodationEnd;
    }

    public function setAccommodationEnd(?\DateTime $accommodationEnd): PaymentRequirements
    {
        $this->accommodationEnd = $accommodationEnd;
        return $this;
    }

    public function getTaxiRoute(): string
    {
        return $this->taxiRoute;
    }

    public function setTaxiRoute(string $taxiRoute): PaymentRequirements
    {
        $this->taxiRoute = $taxiRoute;
        return $this;
    }

    public function getCampaignMaterial(): string
    {
        $campaignMaterial = $this->campaignMaterial;
        if ($campaignMaterial) $campaignMaterial = Utils::formatAttachment($this->campaignMaterial);
        return $campaignMaterial;
    }

    public function setCampaignMaterial(string $campaignMaterial): PaymentRequirements
    {
        $this->campaignMaterial = $campaignMaterial;
        return $this;
    }

    public function getAttachmentPayment(): string
    {
        return $this->attachmentPayment;
    }

    public function setAttachmentPayment(string $attachmentPayment): PaymentRequirements
    {
        $this->attachmentPayment = $attachmentPayment;
        return $this;
    }

    public function getFuelPlate(): string
    {
        return $this->fuelPlate;
    }

    public function setFuelPlate(string $fuelPlate): PaymentRequirements
    {
        $this->fuelPlate = $fuelPlate;
        return $this;
    }

    public function getEventDate(): ?\DateTime
    {
        return $this->eventDate;
    }

    public function setEventDate(?\DateTime $eventDate): PaymentRequirements
    {
        $this->eventDate = $eventDate;
        return $this;
    }

    public function getActiveType(): int
    {
        return $this->activeType;
    }

    public function setActiveType(int $activeType): PaymentRequirements
    {
        $this->activeType = $activeType;
        return $this;
    }

    public function getLaborOccupation(): string
    {
        return $this->laborOccupation;
    }

    public function setLaborOccupation(string $laborOccupation): PaymentRequirements
    {
        $this->laborOccupation = $laborOccupation;
        return $this;
    }

    public function getAccountConsumer(): int
    {
        return $this->accountConsumer;
    }

    public function setAccountConsumer(int $accountConsumer): PaymentRequirements
    {
        $this->accountConsumer = $accountConsumer;
        return $this;
    }

    public function getProvider(): string
    {
        return $this->provider;
    }

    public function setProvider(PaymentRequirements $provider): PaymentRequirements
    {
        $this->provider = $provider;
        return $this;
    }

    public function getNfValue(bool $money = false)
    {
        if ($money) return Utils::formatMoney($this->nfValue);
        return $this->nfValue;
    }

    public function setNfValue($nfValue)
    {
        $this->nfValue = $nfValue;
        return $this;
    }

    public function getNfNumber(): string
    {
        return $this->nfNumber;
    }

    public function setNfNumber(string $nfNumber): PaymentRequirements
    {
        $this->nfNumber = $nfNumber;
        return $this;
    }

    public function getAttachmentNf(): string
    {
        return Utils::formatAttachment($this->attachmentNf);
    }

    public function setAttachmentNf(string $attachmentNf): PaymentRequirements
    {
        $this->attachmentNf = $attachmentNf;
        return $this;
    }

    public function getAttachmentContract(): string
    {
        return Utils::formatAttachment($this->attachmentContract);
    }

    public function setAttachmentContract(string $attachmentContract): PaymentRequirements
    {
        $this->attachmentContract = $attachmentContract;
        return $this;
    }

    public function getPaymentMethod(): int
    {
        return $this->paymentMethod;
    }

    public function getPaymentType(): int
    {
        return $this->paymentType;
    }

    public function setPaymentType($paymentType): PaymentRequirements
    {
        $this->paymentType = $paymentType;
        return $this;
    }

    public function getInstallmentsNumber(): int
    {
        return $this->installmentsNumber;
    }

    public function setInstallmentsNumber($installmentsNumber): PaymentRequirements
    {
        $this->installmentsNumber = $installmentsNumber;
        return $this;
    }

    public function getPaymentTypeString(): string
    {
        switch ($this->paymentType) {
            case 1:
                return 'Ã€ vista';
            case 2:
                return 'Parcelado';
            default:
                return 'Desconhecido';
        }
    }

    public function setPaymentMethod(int $paymentMethod): PaymentRequirements
    {
        $this->paymentMethod = $paymentMethod;
        return $this;
    }

    public function getPaymentValue(bool $money = false)
    {
        if ($money) return Utils::formatMoney($this->paymentValue);
        return $this->paymentValue;
    }

    public function setPaymentValue($paymentValue)
    {
        $this->paymentValue = $paymentValue;
        return $this;
    }

    public function getPaymentDueDate(): ?\DateTime
    {
        return $this->paymentDueDate;
    }

    public function setPaymentDueDate(?\DateTime $paymentDueDate): PaymentRequirements
    {
        $this->paymentDueDate = $paymentDueDate;
        return $this;
    }

    public function getPaymentDoc(): string
    {
        return $this->paymentDoc;
    }

    public function setPaymentDoc(string $paymentDoc): PaymentRequirements
    {
        $this->paymentDoc = $paymentDoc;
        return $this;
    }

    public function getPaymentName(): string
    {
        return $this->paymentName;
    }

    public function setPaymentName(string $paymentName): PaymentRequirements
    {
        $this->paymentName = $paymentName;
        return $this;
    }

    public function getPaymentBank(): string
    {
        return $this->paymentBank;
    }

    public function setPaymentBank(string $paymentBank): PaymentRequirements
    {
        $this->paymentBank = $paymentBank;
        return $this;
    }

    public function getPaymentBankAgency(): string
    {
        return $this->paymentBankAgency;
    }

    public function setPaymentBankAgency(string $paymentBankAgency): PaymentRequirements
    {
        $this->paymentBankAgency = $paymentBankAgency;
        return $this;
    }

    public function getPaymentBankAccount(): string
    {
        return $this->paymentBankAccount;
    }

    public function setPaymentBankAccount(string $paymentBankAccount): PaymentRequirements
    {
        $this->paymentBankAccount = $paymentBankAccount;
        return $this;
    }

    public function getPaymentBankAccountType(): string
    {
        return $this->paymentBankAccountType;
    }

    public function setPaymentBankAccountType(string $paymentBankAccountType): PaymentRequirements
    {
        $this->paymentBankAccountType = $paymentBankAccountType;
        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): PaymentRequirements
    {
        $this->city = $city;
        return $this;
    }

    public function getState(): ?State
    {
        return $this->state;
    }

    public function setState(?State $state): PaymentRequirements
    {
        $this->state = $state;
        return $this;
    }

    public function getDirectory(): ?Directory
    {
        return $this->directory;
    }

    public function setDirectory(?Directory $directory): PaymentRequirements
    {
        $this->directory = $directory;
        return $this;
    }

    public function getThrustFile(): ?string
    {
        $thrustFile = $this->thrustFile;
        if ($thrustFile) $thrustFile = Utils::formatAttachment($this->thrustFile);
        return $thrustFile;
    }

    public function setThrustFile(string $thrustFile): PaymentRequirements
    {
        $this->thrustFile = $thrustFile;
        return $this;
    }

    public function getDirectoryTransfer(): ?Directory
    {
        return $this->directoryTransfer;
    }

    public function setDirectoryTransfer(?Directory $directoryTransfer): PaymentRequirements
    {
        $this->directoryTransfer = $directoryTransfer;
        return $this;
    }

	public function getTravels(): Collection
	{
		return $this->travels;
	}

	public function getContract(): string
	{
		return $this->contract;
	}

	public function setContract(string $contract): PaymentRequirements
	{
		$this->contract = $contract;
		return $this;
	}

	public function getCostCenter(): string
	{
		return $this->costCenter;
	}

	public function setCostCenter(string $costCenter): PaymentRequirements
	{
		$this->costCenter = $costCenter;
		return $this;
	}

	public function getApproved(): int
	{
		return $this->approved;
	}

	public function setApproved(int $approved): PaymentRequirements
	{
		$this->approved = $approved;
		return $this;
	}
}