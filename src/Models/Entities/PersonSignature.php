<?php

namespace App\Models\Entities;
use Doctrine\ORM\Mapping as ORM;

/**
 * PersonSignature
 *
 * @Entity @Table(name="tb_pessoa_assinatura", indexes={
 * @Index(name="idx_id", columns={"id"}),@Index(name="idx_tb_pessoa_id", columns={"tb_pessoa_id"})})
 * @Entity(repositoryClass="App\Models\Repository\PersonSignatureRepository")
 */
class PersonSignature
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var \DateTime
     *
     * @Column(name="data_criacao", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     * @var string
     */
    private $dataCriacao;

    /**
     * @var string|null
     *
     * @Column(name="customer_id", type="string", length=100, nullable=true)
     */
    private $customerId;

    /**
     * @var integer|null
     *
     * @Column(name="gateway_pagamento", type="integer", nullable=true)
     */
    private $gatewayPagamento;

    /**
     * @var string|null
     *
     * @Column(name="gateway_status", type="string", length=100, nullable=true)
     */
    private $gatewayStatus;

    /**
     * @Column(name="valor", type="decimal", precision=13, scale=4, nullable=true)
     * @var string|null
     */
    private $valor;

    /**
     * @var int|null
     *
     * @Column(name="forma_pagamento", type="integer", nullable=true)
     */
    private $formaPagamento;

    /**
     * @var int|null
     *
     * @Column(name="plano_de_contribuicao", type="integer", nullable=true)
     */
    private $paymentPlan = 0;

    /**
     * @var int|null
     *
     * @Column(name="origem_transacao", type="integer", nullable=true)
     */
    private $origin;

    /**
     * @var string|null
     *
     * @Column(name="status", type="string", length=10, nullable=true)
     */
    private $status;

    /**
     * @var string|null
     *
     * @Column(name="token_cartao_credito", type="string", length=255, nullable=true)
     */
    private $tokenCartaoCredito;

    /**
     * @var string|null
     *
     * @Column(name="periodicidade", type="string", length=20, nullable=true)
     */
    private $periodicidade;

    /**
     * @var int|null
     *
     * @Column(name="dia_ciclo", type="integer", nullable=true)
     */
    private $diaCiclo;

    /**
     * @var int|null
     *
     * @Column(name="mes_ciclo", type="integer", nullable=true)
     */
    private $mesCiclo;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_ultimo_status", type="datetime", nullable=true)
     */
    private $dataUltimoStatus;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_gera_fatura", type="datetime", nullable=true)
     */
    private $dataGeraFatura;

    /**
     * @var string|null
     *
     * @Column(name="subscription_id", type="string", length=100, nullable=true)
     */
    private $subscriptionId;

    /**
     * @var string|null
     *
     * @Column(name="obs", type="string", length=255, nullable=true)
     */
    private $obs;

    /**
     * @var int|null
     *
     * @Column(name="tb_diretorio_origem", type="integer", nullable=true)
     */
    private $tbDiretorioOrigem;

    public function toArray():array {
        $array['id'] = $this->getId();
        $array['originStr'] = $this->getOrigemTransacaoStr();
        $array['origin'] = $this->getOrigemTransacao();
        $array['day'] = $this->getDay();
        $array['statusStr'] = $this->getStatusStr();
        $array['status'] = $this->getStatus();
        $array['frequencyStr'] = $this->getFrequency();
        $array['frequency'] = $this->getPeriodicidade();
        $array['value'] = $this->getValue();
        $array['paymentTypeString'] = $this->getPaymentTypeString();
        $array['paymentType'] = $this->getFormaPagamento();
        $array['nextCharge'] = $this->getDataGeraFatura()->format('d/m/Y');
        $array['directory'] = $this->getTbDiretorioOrigem();
        return $array;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getStatusStr()
    {
        $status = $this->status;

        switch ($status) {
            case 'paused':
                return 'Suspensa';
            case 'created':
                return 'Ativa';
        }
    }

    public function getDay()
    {
        return $this->diaCiclo;
    }

    public function getFrequency()
    {
        $periodicidade = $this->periodicidade;

        switch ($periodicidade) {
            case 1:
                return 'Mensal';
                break;
            case 6:
                return 'Semestral';
                break;
            case 12:
                return 'Anual';
                break;
            case 24:
                return 'Bienal';
                break;
            default:
                return 'Desconhecido';
                break;
        }
    }


    public function getValue($money = true)
    {
        $valor = $this->valor;
        if ($money){
            return "R$ " . number_format($valor, 2, ',', '');
        }
        return number_format($valor, 2, ',', '');
    }

    public function getPaymentTypeString(): ?string
    {
        $type = $this->formaPagamento;

        switch ($type) {
            case 1:
                return 'Cartão de Crédito';
            case 2:
                return 'Boleto';
            default:
                return '-';
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return PersonSignature
     */
    public function setId(int $id): PersonSignature
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getTbPessoaId(): ?int
    {
        return $this->tbPessoaId;
    }

    /**
     * @param int|null $tbPessoaId
     * @return PersonSignature
     */
    public function setTbPessoaId(?int $tbPessoaId): PersonSignature
    {
        $this->tbPessoaId = $tbPessoaId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDataCriacao()
    {
        return $this->dataCriacao;
    }

    /**
     * @param mixed $dataCriacao
     * @return PersonSignature
     */
    public function setDataCriacao($dataCriacao)
    {
        $this->dataCriacao = $dataCriacao;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCustomerId(): ?string
    {
        return $this->customerId;
    }

    /**
     * @param string|null $customerId
     * @return PersonSignature
     */
    public function setCustomerId(?string $customerId): PersonSignature
    {
        $this->customerId = $customerId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getGatewayPagamento(): ?int
    {
        return $this->gatewayPagamento;
    }

    /**
     * @param int|null $gatewayPagamento
     * @return PersonSignature
     */
    public function setGatewayPagamento(?int $gatewayPagamento): PersonSignature
    {
        $this->gatewayPagamento = $gatewayPagamento;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGatewayStatus(): ?string
    {
        return $this->gatewayStatus;
    }

    /**
     * @param string|null $gatewayStatus
     * @return PersonSignature
     */
    public function setGatewayStatus(?string $gatewayStatus): PersonSignature
    {
        $this->gatewayStatus = $gatewayStatus;
        return $this;
    }

    /**
     * @return string
     */
    public function getValor(): string
    {
        return $this->valor;
    }

    /**
     * @param string $valor
     * @return PersonSignature
     */
    public function setValor(string $valor): PersonSignature
    {
        $this->valor = $valor;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getFormaPagamento(): ?int
    {
        return $this->formaPagamento;
    }

    /**
     * @param int|null $formaPagamento
     * @return PersonSignature
     */
    public function setPaymentForm(?int $formaPagamento): PersonSignature
    {
        $this->formaPagamento = $formaPagamento;
        return $this;
    }

    public function getPaymentPlan(): ?int
    {
        return $this->paymentPlan;
    }

    public function setPaymentPlan(?int $paymentPlan): PersonSignature
    {
        $this->paymentPlan = $paymentPlan;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getOrigemTransacao(): int
    {
        return $this->origin;
    }

    public function getOrigemTransacaoStr(): ?string
    {
        $origin = $this->origin;
    
        switch ($origin) {
            case 1:
                return 'Doação';
            case 2:
                return 'Filiação';
            case 3:
                $paymentPlan = match ($this->paymentPlan) {
                    2 => 'Plano laranja',
                    3 => 'Plano ouro',
                    4 => 'Plano diamante',
                    default => 'Plano zero'
                };
                return 'Plano de contribuição - ' . $paymentPlan;
            default:
                return '-';
        }
    }

    /**
     * @param int|null $origemTransacao
     * @return PersonSignature
     */
    public function setOrigemTransacao(?int $origemTransacao): PersonSignature
    {
        $this->origin = $origemTransacao;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTokenCartaoCredito(): ?string
    {
        return $this->tokenCartaoCredito;
    }

    /**
     * @param string|null $tokenCartaoCredito
     * @return PersonSignature
     */
    public function setTokenCartaoCredito(?string $tokenCartaoCredito): PersonSignature
    {
        $this->tokenCartaoCredito = $tokenCartaoCredito;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPeriodicidade(): ?string
    {
        return $this->periodicidade;
    }

    /**
     * @param string|null $periodicidade
     * @return PersonSignature
     */
    public function setPeriodicidade(?string $periodicidade): PersonSignature
    {
        $this->periodicidade = $periodicidade;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getDiaCiclo(): ?int
    {
        return $this->diaCiclo;
    }

    /**
     * @param int|null $diaCiclo
     * @return PersonSignature
     */
    public function setDiaCiclo(?int $diaCiclo): PersonSignature
    {
        $this->diaCiclo = $diaCiclo;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getMesCiclo(): ?int
    {
        return $this->mesCiclo;
    }

    /**
     * @param int|null $mesCiclo
     * @return PersonSignature
     */
    public function setMesCiclo(?int $mesCiclo): PersonSignature
    {
        $this->mesCiclo = $mesCiclo;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDataUltimoStatus(): ?\DateTime
    {
        return $this->dataUltimoStatus;
    }

    /**
     * @param \DateTime|null $dataUltimoStatus
     * @return PersonSignature
     */
    public function setDataUltimoStatus(?\DateTime $dataUltimoStatus): PersonSignature
    {
        $this->dataUltimoStatus = $dataUltimoStatus;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDataGeraFatura(): ?\DateTime
    {
        if (!$this->dataGeraFatura){
            $this->dataGeraFatura = new \DateTime(date('Y-m-') . $this->diaCiclo);
            if ($this->dataGeraFatura <  new \DateTime()){
                $this->dataGeraFatura->add(new \DateInterval('P1M'));
            }
        }
        return $this->dataGeraFatura;
    }

    /**
     * @param \DateTime|null $dataGeraFatura
     * @return PersonSignature
     */
    public function setDataGeraFatura(?\DateTime $dataGeraFatura): PersonSignature
    {
        $this->dataGeraFatura = $dataGeraFatura;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubscriptionId(): ?string
    {
        return $this->subscriptionId;
    }

    /**
     * @param string|null $subscriptionId
     * @return PersonSignature
     */
    public function setSubscriptionId(?string $subscriptionId): PersonSignature
    {
        $this->subscriptionId = $subscriptionId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getObs(): ?string
    {
        return $this->obs;
    }

    /**
     * @param string|null $obs
     * @return PersonSignature
     */
    public function setObs(?string $obs): PersonSignature
    {
        $this->obs = $obs;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getTbDiretorioOrigem(): ?int
    {
        return $this->tbDiretorioOrigem;
    }

    /**
     * @param int|null $tbDiretorioOrigem
     * @return PersonSignature
     */
    public function setTbDiretorioOrigem(?int $tbDiretorioOrigem): PersonSignature
    {
        $this->tbDiretorioOrigem = $tbDiretorioOrigem;
        return $this;
    }

    /**
     * @param string|null $status
     * @return PersonSignature
     */
    public function setStatus(?string $status): PersonSignature
    {
        $this->status = $status;
        return $this;
    }
}
