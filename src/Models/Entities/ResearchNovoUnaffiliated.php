<?php

namespace App\Models\Entities;

/**
 * @Entity @Table(name="researchNovoUnaffiliated")
 * @ORM @Entity(repositoryClass="App\Models\Repository\ResearchNovoUnaffiliatedRepository")
 */
class ResearchNovoUnaffiliated
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ManyToOne(targetEntity="ResearchNovoUser")
     * @JoinColumn(name="researchNovoUser", referencedColumnName="id")
     */
    private ResearchNovoUser $researchNovoUser;

    /**
     * @Column(type="string")
     */
    private string $question1;

    /**
     * @Column(type="string")
     */
    private string $textAreaOthersQuestion1;

    /**
     * @Column(type="string")
     */
    private string $question2;

    /**
     * @Column(type="string")
     */
    private string $textAreaOthersQuestion2;

    /**
     * @Column(type="string")
     */
    private string $question3;

    /**
     * @Column(type="string")
     */
    private string $textAreaOthersQuestion3;

    /**
     * @Column(type="string")
     */
    private string $question4;

    /**
     * @Column(type="string")
     */
    private string $textAreaOthersQuestion4;

    /**
     * @Column(type="string")
     */
    private string $question5;

    /**
     * @Column(type="string")
     */
    private string $question6;

    public function getId()
    {
        return $this->id;
    }

    public function getResearchNovoUserUser(): ResearchNovoUser
    {
        return $this->researchNovoUser;
    }

    public function setResearchNovoUserUser(ResearchNovoUser $researchNovoUser): ResearchNovoUnaffiliated
    {
        $this->researchNovoUser = $researchNovoUser;
        return $this;
    }

    public function getQuestion1(): string
    {
        return $this->question1;
    }

    public function setQuestion1(string $question1): ResearchNovoUnaffiliated
    {
        $this->question1 = $question1;
        return $this;
    }

    public function getTextAreaOthersQuestion1(): string
    {
        return $this->textAreaOthersQuestion1;
    }

    public function setTextAreaOthersQuestion1(string $textAreaOthersQuestion1): ResearchNovoUnaffiliated
    {
        $this->textAreaOthersQuestion1 = $textAreaOthersQuestion1;
        return $this;
    }

    public function getQuestion2(): string
    {
        return $this->question2;
    }

    public function setQuestion2(string $question2): ResearchNovoUnaffiliated
    {
        $this->question2 = $question2;
        return $this;
    }

    public function getTextAreaOthersQuestion2(): string
    {
        return $this->textAreaOthersQuestion2;
    }

    public function setTextAreaOthersQuestion2(string $textAreaOthersQuestion2): ResearchNovoUnaffiliated
    {
        $this->textAreaOthersQuestion2 = $textAreaOthersQuestion2;
        return $this;
    }

    public function getQuestion3(): string
    {
        return $this->question3;
    }

    public function setQuestion3(string $question3): ResearchNovoUnaffiliated
    {
        $this->question3 = $question3;
        return $this;
    }

    public function getTextAreaOthersQuestion3(): string
    {
        return $this->textAreaOthersQuestion3;
    }

    public function setTextAreaOthersQuestion3(string $textAreaOthersQuestion3): ResearchNovoUnaffiliated
    {
        $this->textAreaOthersQuestion3 = $textAreaOthersQuestion3;
        return $this;
    }

    public function getQuestion4(): string
    {
        return $this->question4;
    }

    public function setQuestion4(string $question4): ResearchNovoUnaffiliated
    {
        $this->question4 = $question4;
        return $this;
    }

    public function getTextAreaOthersQuestion4(): string
    {
        return $this->textAreaOthersQuestion4;
    }

    public function setTextAreaOthersQuestion4(string $textAreaOthersQuestion4): ResearchNovoUnaffiliated
    {
        $this->textAreaOthersQuestion4 = $textAreaOthersQuestion4;
        return $this;
    }

    public function getQuestion5(): string
    {
        return $this->question5;
    }

    public function setQuestion5(string $question5): ResearchNovoUnaffiliated
    {
        $this->question5 = $question5;
        return $this;
    }

    public function getQuestion6(): string
    {
        return $this->question6;
    }

    public function setQuestion6(string $question6): ResearchNovoUnaffiliated
    {
        $this->question6 = $question6;
        return $this;
    }
}