<?php
namespace App\Models\Entities;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity
 * @Table(name="whatsAppGroups")
 * @ORM @Entity(repositoryClass="App\Models\Repository\WhatsAppGroupsRepository")
 */

 class WhatsAppGroups
 {
    /**
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @Column(name="UF", type="string", nullable=false)
     */
    private $uf;

    /**
     * @Column(name="link", type="string", nullable=false)
     */
    private $link;

    public function getId(): int
    {
        return $this->id;
    }

    public function getUf(): string
    {
        return $this->uf;
    }

    public function getLink(): string
    {
        return $this->link;
    }
 }