<?php

namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\ORM\Mapping as ORM;

/**
 * EventsCart
 *
 * @Entity @Table(name="tb_eventos_carrinho")
 * @ORM @Entity(repositoryClass="App\Models\Repository\EventsCartRepository")
 */
class EventsCart
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var int|null
     *
     * @Column(name="tb_eventos_transacao_id", type="integer", nullable=true)
     */
    private $tbEventosTransacaoId;

    /**
     * @var int|null
     *
     * @Column(name="tb_eventos_id", type="integer", nullable=true)
     */
    private $tbEventosId;

    /**
     * @var int|null
     *
     * @Column(name="tb_eventos_precos_id", type="integer", nullable=true)
     */
    private $tbEventosPrecosId;

    /**
     * @var int|null
     *
     * @Column(name="quantidade", type="integer", nullable=true)
     */
    private $quantidade;

    /**
     * @Column(type="decimal", name="valor_unitario", precision=13, scale=4, nullable=true)
     * @var float
     */
    private $valor;

    /**
     * @var string|null
     *
     * @Column(name="status", type="string", length=45, nullable=true)
     */
    private $status;

    /**
     * @Column(type="datetime", name="data_criacao", nullable=true)
     * @var string
     */
    private $dataCriacao;
    
    /**
     * @Column(type="datetime", name="data_atualizacao", nullable=true)
     * @var string
     */
    private $dataAtualizacao;

    public function getId(): int
    {
        return $this->id;
    }

    public function getTbPessoaId(): int
    {
        return $this->tbPessoaId;
    }

    public function setTbPessoaId(int $tbPessoaId): EventsCart
    {
        $this->tbPessoaId = $tbPessoaId;
        return $this;
    }

    public function getTbEventsTransactionId(): int
    {
        return $this->tbEventosTransacaoId;
    }

    public function setTbEventsTransactionId(int $tbEventosTransacaoId): EventsCart
    {
        $this->tbEventosTransacaoId = $tbEventosTransacaoId;
        return $this;
    }

    public function getTbEventsId(): int
    {
        return $this->tbEventosId;
    }

    public function setTbEventsId(int $tbEventosId): EventsCart
    {
        $this->tbEventosId = $tbEventosId;
        return $this;
    }

    public function getTbEventsPricesId(): int
    {
        return $this->tbEventosPrecosId;
    }

    public function setTbEventsPricesId(int $tbEventosPrecosId): EventsCart
    {
        $this->tbEventosPrecosId = $tbEventosPrecosId;
        return $this;
    }

    public function getQuantity(): int
    {
        return $this->quantidade;
    }

    public function setQuantity(int $quantidade): EventsCart
    {
        $this->quantidade = $quantidade;
        return $this;
    }

    public function getValue(): float
    {
        return $this->valor;
    }

    public function setValue(float $valor): EventsCart
    {
        $this->valor = $valor;
        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): EventsCart
    {
        $this->status = $status;
        return $this;
    }

    public function getDateCreated(): ?\DateTime
    {
        return $this->dataCriacao;
    }

    public function setDateCreated(?\DateTime $dataCriacao): EventsCart
    {
        $this->dataCriacao = $dataCriacao;
        return $this;
    }

    public function getDateRefresh(): ?\DateTime
    {
        return $this->dataAtualizacao;
    }

    public function setDateRefresh(?\DateTime $dataAtualizacao): EventsCart
    {
        $this->dataAtualizacao = $dataAtualizacao;
        return $this;
    }
}