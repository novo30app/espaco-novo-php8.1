<?php

namespace App\Models\Entities;
use Doctrine\Mapping as ORM;

/**
 * 
 * @Entity @Table(name="cogmo_schedule")
 * @ORM @Entity(repositoryClass="App\Models\Repository\CogmoScheduleRepository")
 */
class CogmoSchedule
{
    /**
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @Column(type="integer")
     */
    private int $id;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="contact_id", referencedColumnName="id")
     */
    private User $contactId;

    /**
     * @Column(name="contact_number", type="string", length=20)
     */
    private string $contactNumber;

    /**
     * @ManyToOne(targetEntity="Transaction")
     * @JoinColumn(name="transaction", referencedColumnName="id")
     */
    private Transaction $transactionId;

    /**
     * @ManyToOne(targetEntity="CogmoPhones")
     * @JoinColumn(name="phone_number", referencedColumnName="id")
     */
    private CogmoPhones $phoneNumber;
    
    /**
     * @Column(name="message", type="string", length=255)
     */
    private string $message;

    /**
     * @Column(name="scheduled_time", type="datetime")
     */
    private \DateTime $scheduledTime;

    /**
     * @Column(name="status", type="string", length=20)
     */
    private string $status = 'pending';

    /**
     * @Column(name="created_at", type="datetime")
     */
    private \DateTime $createdAt;

    /**
     * @Column(name="updated_at", type="datetime")
     */
    private \DateTime $updatedAt;

    public function getId(): int 
    { 
        return $this->id; 
    }

    public function getContactId(): User 
    { 
        return $this->contactId; 
    }

    public function setContact(User $contactId): CogmoSchedule
    {
        $this->contactId = $contactId;
        return $this;
    }

    public function getContactNumber(): string  
    { 
        return $this->contactNumber; 
    }

    public function setContactNumber(string $contactNumber): CogmoSchedule
    {
        $this->contactNumber = $contactNumber;
        return $this;
    }

    public function getTransaction(): Transaction
    {
        return $this->transactionId;
    }

    public function setTransaction(Transaction $transactionId): CogmoSchedule
    {
        $this->transactionId = $transactionId;
        return $this;
    }

    public function getPhoneNumber(): CogmoPhones 
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(CogmoPhones $phoneNumber): CogmoSchedule
    {
        $this->phoneNumber = $phoneNumber;
        return $this;
    }

    public function getMessage(): string 
    {
        return $this->message;
    }

    public function setMessage(string $message): CogmoSchedule
    {
        $this->message = $message;
        return $this;
    }

    public function getScheduledTime(): \DateTime 
    {
        return $this->scheduledTime;
    }

    public function setScheduledTime(\DateTime $scheduledTime): CogmoSchedule
    {
        $this->scheduledTime = $scheduledTime;
        return $this;
    }

    public function getStatus(): string 
    {
        return $this->status;
    }

    public function setStatus(string $status): CogmoSchedule

    { 
        $this->status = $status; 
        return $this; 
    }

    public function getCreatedAt(): \DateTime 
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): CogmoSchedule
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTime $updatedAt): CogmoSchedule
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}