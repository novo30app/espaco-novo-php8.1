<?php

namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\ORM\Mapping as ORM;

/**
 * HowdidKnow
 *
 * @Entity @Table(name="tb_como_conheceu", indexes={
 * @Index(name="idx_pessoa_id", columns={"tb_pessoa_id"})})
 * @Entity(repositoryClass="App\Models\Repository\HowdidKnowRepository")
 */
class HowdidKnow 
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @Column(name="tb_pessoa_id", type="integer", nullable=true)
     */
    private $tbPessoaId;

    /**
     * @var string|null
     *
     * @Column(name="type", type="string", length=100, nullable=true)
     */
    private $type;

    /**
     * @var string|null
     *
     * @Column(name="subtype", type="string", length=100, nullable=true)
     */
    private $subtype;

    /**
     * @var string|null
     *
     * @Column(name="estado", type="string", length=100, nullable=true)
     */
    private $estado;

    /**
     * @var string|null
     *
     * @Column(name="cidade", type="string", length=100, nullable=true)
     */
    private $cidade;

    /**
     * @var string|null
     *
     * @Column(name="evento", type="string", length=100, nullable=true)
     */
    private $evento;

    /**
     * @var int|null
     *
     * @Column(name="filiado_id", type="integer", nullable=true)
     */
    private $filiadoId;

    /**
     * @var string|null
     *
     * @Column(name="content", type="text", length=65535, nullable=true)
     */
    private $content;

    /**
     * @var \DateTime
     *
     * @Column(name="created_at", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdAt;

    public function setTbPessoaId(?int $tbPessoaId): HowdidKnow
    {
        $this->tbPessoaId = $tbPessoaId;
        return $this;
    }

    public function setFiliadoId(?int $filiadoId): HowdidKnow
    {
        $this->filiadoId = $filiadoId;
        return $this;
    }

    public function setType(?int $type): HowdidKnow
    {
        $this->type = $type;
        return $this;
    }

    public function setSubType(?string $subtype): HowdidKnow
    {
        $this->subtype = $subtype;
        return $this;
    }

    public function setUf(?string $estado): HowdidKnow
    {
        $this->estado = $estado;
        return $this;
    }

    public function setCity(?string $cidade): HowdidKnow
    {
        $this->cidade = $cidade;
        return $this;
    }

    public function setEvent(?string $evento): HowdidKnow
    {
        $this->evento = $evento;
        return $this;
    }

    public function setContent(?string $content): HowdidKnow
    {
        $this->content = $content;
        return $this;
    }

    public function setDataCriacao(\DateTime $createdAt): HowdidKnow
    {
        $this->createdAt = $createdAt;
        return $this;
    }
}
