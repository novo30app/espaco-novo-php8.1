<?php


namespace App\Models\Entities;
use Doctrine\ORM\Mapping as ORM;

/**
 * Logs
 *
 * @Entity @Table(name="logs")
 * @ORM @Entity(repositoryClass="App\Models\Repository\LogsRepository")
 */
class Logs
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @Column(type="string")
     */
    private string $method = '';

    /**
     * @Column(type="string")
     */
    private string $content = '';

    /**
     * @Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private \DateTime $inserted;

    public function getId()
    {
        return $this->id;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function setMethod(string $method): Logs
    {
        $this->method = $method;
        return $this;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): Logs
    {
        $this->content = $content;
        return $this;
    }

    public function getInserted(): \DateTime
    {
        return $this->inserted;
    }

    public function setInserted(\DateTime $inserted): Logs
    {
        $this->inserted = $inserted;
        return $this;
    }
}