<?php

namespace App\Models\Entities;

/**
 * @Entity @Table(name="researchNovoInformative")
 * @ORM @Entity(repositoryClass="App\Models\Repository\ResearchNovoInformativeRepository")
 */
class ResearchNovoInformative
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private \DateTime $created;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user", referencedColumnName="id")
     */
    private User $user;

    /**
     * @Column(type="text")
     */
    private string $question1;

    /**
     * @Column(type="text")
     */
    private string $question2;

    /**
     * @Column(type="text")
     */
    private string $textAreaOthersQuestion2;

    /**
     * @Column(type="text")
     */
    private string $question3;

    public function getId()
    {
        return $this->id;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function setCreated(\DateTime $created): ResearchNovoInformative
    {
        $this->created = $created;
        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): ResearchNovoInformative
    {
        $this->user = $user;
        return $this;
    }

    public function getQuestion1(): string
    {
        return $this->question1;
    }

    public function setQuestion1(string $question1): ResearchNovoInformative
    {
        $this->question1 = $question1;
        return $this;
    }

    public function getQuestion2(): string
    {
        return $this->question2;
    }

    public function setQuestion2(string $question2): ResearchNovoInformative
    {
        $this->question2 = $question2;
        return $this;
    }

    public function getTextAreaOthersQuestion2(): string
    {
        return $this->textAreaOthersQuestion2;
    }

    public function setTextAreaOthersQuestion2(string $textAreaOthersQuestion2): ResearchNovoInformative
    {
        $this->textAreaOthersQuestion2 = $textAreaOthersQuestion2;
        return $this;
    }

    public function getQuestion3(): string
    {
        return $this->question3;
    }

    public function setQuestion3(string $question3): ResearchNovoInformative
    {
        $this->question3 = $question3;
        return $this;
    }
}