<?php

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\Mapping as ORM;

/**
 * @Entity @Table(name="undersignedCounter")
 * @ORM @Entity(repositoryClass="App\Models\Repository\UndersignedCounterRepository")
 */
class UndersignedCounter
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private int $id;

    /**
     * @Column(type="integer")
     */
    private int $count;


    public function getId(): int
    {
        return $this->id;
    }

    public function getCount(): int
    {
        return $this->count;
    }

    public function setCount(int $count): UndersignedCounter
    {
        $this->count = $count;
        return $this;
    }
}