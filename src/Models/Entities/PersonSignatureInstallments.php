<?php

namespace App\Models\Entities;

use App\Helpers\Utils;
use Doctrine\Mapping as ORM; 

/**
 * @Table(name="personSignatureInstallments")
 * @Entity(repositoryClass="App\Models\Repository\PersonSignatureInstallmentsRepository")
 */
class PersonSignatureInstallments
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="PersonSignature")
     * @JoinColumn(name="signature", referencedColumnName="id", nullable=true)
     * @var PersonSignature
     */
    private $signature;

    /**
     * @Column(name="value", type="decimal", precision=13, scale=4, nullable=true)
     * @var string|null
     */
    private $value;

    /**
     * @var \DateTime|null
     * @Column(name="dueDate", type="datetime", nullable=true)
     */
    private $dueDate;

    /**
     * @Column(name="created", type="datetime")
     */
    private \Datetime $created;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSignature(): PersonSignature
    {
        return $this->signature;
    }

    public function setSignature(PersonSignature $signature): PersonSignatureInstallments
    {
        $this->signature = $signature;
        return $this;
    }

    public function getValue($money = true)
    {
        $value = $this->value;
        if ($money){
            return "R$" . number_format($value, 2, ',', '');
        }
        return number_format($value, 2, ',', '');
    }

    public function setValue(string $value): PersonSignatureInstallments
    {
        $this->value = $value;
        return $this;
    }

    public function getDueDate(): ?\DateTime
    {
        return $this->dueDate;
    }

    public function setDueDate(?\DateTime $dueDate): PersonSignatureInstallments
    {
        $this->dueDate = $dueDate;
        return $this;
    }

    public function getCreated(): \Datetime
    {
        return $this->created;
    }

    public function setCreated(\Datetime $created): PersonSignatureInstallments
    {
        $this->created = $created;
        return $this;
    }
}