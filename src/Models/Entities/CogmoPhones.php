<?php

namespace App\Models\Entities;
use Doctrine\Mapping as ORM;

/**
 * @Entity @Table(name="cogmo_phones")
 * @ORM @Entity(repositoryClass="App\Models\Repository\CogmoPhonesRepository")
 */
class CogmoPhones
{
    /**
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @Column(type="integer")
     */
    private int $id;

    /**
     * @Column(type="string", length=20, unique=true)
     */
    private string $phone;

    /**
     * @Column(type="string", length=100, nullable=true)
     */
    private ?string $name;

    /**
     * @Column(name="status", type="string", length=20)
     */
    private string $status = 'active';

    /**
     * @Column(name="endpoint", type="string", length=255)
     */
    private string $endpoint;

    /**
     * @Column(name="created_at", type="datetime")
     */
    private \DateTime $created_at;

    /**
     * @Column(name="updated_at", type="datetime")
     */
    private \DateTime $updated_at;

    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = new \DateTime();
    }

    // Getters
    public function getId(): int { 
        return $this->id; 
    }

    public function getPhone(): string { 
        return $this->phone; 
    }

    public function getName(): ?string { 
        return $this->name; 
    }

    public function getEndpoint(): string { 
        return $this->endpoint; 
    }

    public function getStatus(): string { 
        return $this->status; 
    }

    public function getCreatedAt(): \DateTime { 
        return $this->created_at; 
    }

    public function getUpdatedAt(): \DateTime { 
        return $this->updated_at; 
    }

    // Setters
    public function setPhone(string $phone): string { 
        $this->phone = $phone; 
        return $this; 
    }

    public function setName(?string $name): ?string { 
        $this->name = $name; 
        return $this; 
    }

    public function setEndpoint(string $endpoint): string { 
        $this->endpoint = $endpoint; 
        return $this; 
    }

    public function setStatus(string $status): string { 
        $this->status = $status; 
        return $this; 
    }

    public function setUpdatedAt(): \DateTime { 
        $this->updated_at = new \DateTime(); 
        return $this; 
    }
}