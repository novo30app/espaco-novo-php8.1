<?php
/**
 * Created by PhpStorm.
 * User: rwerl
 * Date: 16/04/2019
 * Time: 22:52
 */

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="selectiveProcess2022Module1Video")
 * @ORM @Entity(repositoryClass="App\Models\Repository\SelectiveProcess2022Module1VideoRepository")
 */
class SelectiveProcess2022Module1Video
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="datetime")
     */
    private \DateTime $created;

    /**
     * @OneToOne(targetEntity="User")
     * @JoinColumn(name="candidate", referencedColumnName="id")
     */
    private User $candidate;

    /**
     * @Column(type="string")
     */
    private string $link = '';


    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function getCandidate(): User
    {
        return $this->candidate;
    }

    public function setCandidate(User $candidate): SelectiveProcess2022Module1Video
    {
        $this->candidate = $candidate;
        return $this;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function setLink(string $link): SelectiveProcess2022Module1Video
    {
        $this->link = $link;
        return $this;
    }

}