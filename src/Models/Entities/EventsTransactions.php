<?php

namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\ORM\Mapping as ORM;

/**
 * EventsTransactions
 *
 * @Entity @Table(name="tb_eventos_transacoes")
 * @ORM @Entity(repositoryClass="App\Models\Repository\EventsTransactionsRepository")
 */
class EventsTransactions
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @Column(name="tb_eventos_id", type="integer", nullable=false)
     */
    private $tbEventosId;

    /**
     * @var int|null
     *
     * @Column(name="tb_pessoa_id", type="integer", nullable=false)
     */
    private $tbPessoaId;

    /**
     * @var int|null
     *
     * @Column(name="tb_transacao_id", type="integer", nullable=false)
     */
    private $tbTransacaoId;

    /**
     * @Column(type="decimal", name="valor", precision=13, scale=4, nullable=true)
     * @var float
     */
    private $valor;

    /**
     * @var string|null
     *
     * @Column(name="nome", type="string", length=100, nullable=true)
     */
    private $nome;

    /**
     * @var \DateTime|null
     *
     * @Column(name="data_nascimento", type="date", nullable=true)
     */
    private $dataNascimento;

    /**
     * @var string|null
     *
     * @Column(name="cpf", type="string", length=25, nullable=true)
     */
    private $cpf;

    /**
     * @var string
     *
     * @Column(name="telefone_ddi", type="string", length=4, nullable=false, options={"default"="55","fixed"=true})
     */
    private $telefoneDdi = '55';

    /**
     * @var string|null
     *
     * @Column(name="telefone_numero", type="string", length=25, nullable=true)
     */
    private $telefoneNumero;

    /**
     * @var string
     *
     * @Column(name="celular_ddi", type="string", length=4, nullable=false, options={"default"="55","fixed"=true})
     */
    private $celularDdi = '55';

    /**
     * @var string|null
     *
     * @Column(name="celular_numero", type="string", length=25, nullable=true)
     */
    private $celularNumero;

    /**
     * @var string|null
     *
     * @Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @var int|null
     *
     * @Column(name="tb_estado_id", type="integer", nullable=true)
     */
    private $tbEstadoId;

    /**
     * @var string|null
     *
     * @Column(name="estado", type="string", length=255, nullable=true)
     */
    private $estado;

    /**
     * @var int|null
     *
     * @Column(name="tb_cidade_id", type="integer", nullable=true)
     */
    private $tbCidadeId;

    /**
     * @var string|null
     *
     * @Column(name="cidade", type="string", length=255, nullable=true)
     */
    private $cidade;

    /**
     * @var string|null
     *
     * @Column(name="cep", type="string", length=10, nullable=true)
     */
    private $cep;

    /**
     * @var string|null
     *
     * @Column(name="endereco", type="string", length=150, nullable=true)
     */
    private $endereco;

    /**
     * @var string|null
     *
     * @Column(name="numero", type="string", length=6, nullable=true)
     */
    private $numero;

    /**
     * @var string|null
     *
     * @Column(name="complemento", type="string", length=200, nullable=true)
     */
    private $complemento;

    /**
     * @var string|null
     *
     * @Column(name="bairro", type="string", length=45, nullable=true)
     */
    private $bairro;

    /**
     * @var string|null
     *
     * @Column(name="code", type="string", length=100, nullable=true)
     */
    private $code;

    /**
     * @ManyToOne(targetEntity="EventsCoupons")
     * @JoinColumn(name="coupon", referencedColumnName="id", nullable=true)
     * @var EventsCoupons
     */
    private $coupon;

    /**
     * @var string|null
     *
     * @Column(name="status", type="string", length=45, nullable=true)
     */
    private $status;

    /**
     * @Column(type="datetime", name="data_criacao", nullable=true)
     * @var string
     */
    private $dataCriacao;

    public function getId(): int
    {
        return $this->id;
    }

    public function getTbEventosId(): int
    {
        return $this->tbEventosId;
    }

    public function setTbEventosId(int $tbEventosId): EventsTransactions
    {
        $this->tbEventosId = $tbEventosId;
        return $this;
    }

    public function getTbPessoaId(): int
    {
        return $this->tbPessoaId;
    }

    public function setTbPessoaId(int $tbPessoaId): EventsTransactions
    {
        $this->tbPessoaId = $tbPessoaId;
        return $this;
    }

    public function getTbTransacaoId(): int
    {
        return $this->tbTransacaoId;
    }

    public function setTbTransacaoId(int $tbTransacaoId): EventsTransactions
    {
        $this->tbTransacaoId = $tbTransacaoId;
        return $this;
    }

    public function getValor(): float
    {
        return $this->valor;
    }

    public function setValor(float $valor): EventsTransactions
    {
        $this->valor = $valor;
        return $this;
    }

    public function getName(): ?string
    {
        return  rtrim(Utils::firstUpper($this->nome));
    }

    public function setName(?string $nome): EventsTransactions
    {
        $this->nome = $nome;
        return $this;
    }

    public function getDataNascimento(): ?\DateTime
    {
        return $this->dataNascimento;
    }

    public function setDataNascimento(?\DateTime $dataNascimento): EventsTransactions
    {
        $this->dataNascimento = $dataNascimento;
        return $this;
    }

    public function getCpf(): ?string
    {
        return $this->cpf;
    }

    public function setCpf(?string $cpf): EventsTransactions
    {
        $this->cpf = $cpf;
        return $this;
    }

    public function getTelefoneDdi(): string
    {
        return $this->telefoneDdi;
    }

    public function setTelefoneDdi(string $telefoneDdi): EventsTransactions
    {
        $this->telefoneDdi = $telefoneDdi;
        return $this;
    }

    public function getTelefoneNumero(bool $onlyNumbers = false): ?string
    {
        if ($onlyNumbers){
            return Utils::onlyNumbers($this->telefoneNumero);
        }
        return $this->telefoneNumero;
    }

    public function setTelefoneNumero(?string $telefoneNumero): EventsTransactions
    {
        $this->telefoneNumero = $telefoneNumero;
        return $this;
    }

    public function getCelularDdi(): string
    {
        return $this->celularDdi;
    }

    public function setCelularDdi(string $celularDdi): EventsTransactions
    {
        $this->celularDdi = $celularDdi;
        return $this;
    }

    public function getCelularNumero(bool $onlyNumbers = false): ?string
    {
        if ($onlyNumbers){
            return Utils::onlyNumbers($this->celularNumero);
        }
        return $this->celularNumero;
    }

    public function setCelularNumero(?string $celularNumero): EventsTransactions
    {
        $this->celularNumero = $celularNumero;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): EventsTransactions
    {
        $this->email = $email;
        return $this;
    }

    public function getStateId(): ?int
    {
        return $this->tbEstadoId;
    }

    public function setStateId(?int $tbEstadoId): EventsTransactions
    {
        $this->tbEstadoId = $tbEstadoId;
        return $this;
    }

    public function getState(): ?string
    {
        return $this->estado;
    }

    public function setState(?string $estado): EventsTransactions
    {
        $this->estado = $estado;
        return $this;
    }

    public function getCityId(): ?int
    {
        return $this->tbCidadeId;
    }

    public function setCityId(?int $tbCidadeId): EventsTransactions
    {
        $this->tbCidadeId = $tbCidadeId;
        return $this;
    }

    public function getCidade(): ?string
    {
        return $this->cidade;
    }

    public function setCidade(?string $cidade): EventsTransactions
    {
        $this->cidade = $cidade;
        return $this;
    }

    public function getCep(): ?string
    {
        return $this->cep;
    }

    public function setCep(?string $cep): EventsTransactions
    {
        $this->cep = $cep;
        return $this;
    }

    public function getEndereco(): ?string
    {
        return $this->endereco;
    }

    public function setEndereco(?string $endereco): EventsTransactions
    {
        $this->endereco = $endereco;
        return $this;
    }

    public function getNumero(): ?string
    {
        return $this->numero;
    }

    public function setNumero(?string $numero): EventsTransactions
    {
        $this->numero = $numero;
        return $this;
    }

    public function getComplemento(): ?string
    {
        return $this->complemento;
    }

    public function setComplemento(?string $complemento): EventsTransactions
    {
        $this->complemento = $complemento;
        return $this;
    }

    public function getBairro(): ?string
    {
        return $this->bairro;
    }

    public function setBairro(?string $bairro): EventsTransactions
    {
        $this->bairro = $bairro;
        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): EventsTransactions
    {
        $this->code = $code;
        return $this;
    }

    public function getCoupon(): EventsCoupons
    {
        return $this->coupon;
    }

    public function setCoupon(EventsCoupons $coupon): EventsTransactions
    {
        $this->coupon = $coupon;
        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): EventsTransactions
    {
        $this->status = $status;
        return $this;
    }

    public function getDataCriacao(): ?\DateTime
    {
        return $this->dataCriacao;
    }

    public function setDataCriacao(?\DateTime $dataCriacao): EventsTransactions
    {
        $this->dataCriacao = $dataCriacao;
        return $this;
    }
}