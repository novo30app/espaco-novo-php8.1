<?php


namespace App\Models\Entities;

use App\Helpers\Session;
use App\Helpers\Utils;
use Doctrine\ORM\Mapping as ORM;

/**
 * ProcessCep
 * 
 * @Entity @Table(name="processCep")
 * @ORM @Entity(repositoryClass="App\Models\Repository\ProcessCepRepository")
 */
class ProcessCep
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @Column(name="userAdmin", type="integer", nullable=true)
     */
    private $userAdmin;

    /**
     * @var int|null
     *
     * @Column(name="personId", type="integer", nullable=true)
     */
    private $personId;

    /**
     * @var bool|null
     *
     * @Column(name="reason", type="string", nullable=true)
     */
    private $reason;

    /**
     * @var bool|null
     *
     * @Column(name="description", type="string", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime|null
     *
     * @Column(name="dateBegin", type="datetime", nullable=true)
     */
    private $dateBegin;

    /**
     * @var \DateTime|null
     *
     * @Column(name="dateFinal", type="datetime", nullable=true)
     */
    private $dateFinal;

    /**
     * @var bool|null
     *
     * @Column(name="file", type="string", nullable=true)
     */
    private $file;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserAdmin(): ?int
    {
        return $this->userAdmin;
    }

    public function setUserAdmin(?int $userAdmin): ProcessCep
    {
        $this->userAdmin = $userAdmin;
        return $this;
    }

    public function getPersonId(): ?int
    {
        return $this->personId;
    }

    public function setPersonId(?int $personId): ProcessCep
    {
        $this->personId = $personId;
        return $this;
    }

    public function getReason(): ?string
    {
        return $this->reason;
    }

    public function setReason(?string $reason): ProcessCep
    {
        $this->reason = $reason;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): ProcessCep
    {
        $this->description = $description;
        return $this;
    }

    public function getDateBegin(): ?\DateTime
    {
        return $this->dateBegin;
    }

    public function setDateBegin(\DateTime $dateBegin): ProcessCep
    {
        $this->dateBegin = $dateBegin;
        return $this;
    }

    public function getDateFinal(): ?\DateTime
    {
        return $this->dateFinal;
    }

    public function setDateFinal(\DateTime $dateFinal): ProcessCep
    {
        $this->dateFinal = $dateFinal;
        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): ProcessCep
    {
        $this->file = $file;
        return $this;
    }
}