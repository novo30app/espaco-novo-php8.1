<?php

namespace App\Models\Entities;
use Doctrine\ORM\EntityRepository;
use Doctrine\Mapping as ORM;

/**
 * @Entity @Table(name="brandMessage")
 * @ORM @Entity(repositoryClass="App\Models\Repository\BrandMessageRepository")
 */
class BrandMessage
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private int $id;

    /**
     * @Column(type="integer")
     */
    private int $transaction;

    /**
     * @Column(type="string")
     */
    private string $brandCode;

    /**
     * @Column(type="string")
     */
    private string $brandMessage;

    /**
     * @Column(type="boolean")
     */
    private int $type;

    /**
     * @Column(type="string")
     */
    private string $flag;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    private $created;

    public function getId(): int
    {
        return $this->id;
    }

    public function getTransaction(): int
    {
        return $this->transaction;
    }

    public function setTransaction(?int $transaction): BrandMessage
    {
        $this->transaction = $transaction;
        return $this;
    }

    public function getBrandCode(): string
    {
        return $this->brandCode;
    }

    public function setBrandCode(?string $brandCode): BrandMessage
    {
        $this->brandCode = $brandCode;
        return $this;
    }

    public function getbrandMessage(): string
    {
        return $this->brandMessage;
    }

    public function setbrandMessage(?string $brandMessage): BrandMessage
    {
        $this->brandMessage = $brandMessage;
        return $this;
    }

    public function getType(): ?bool
    {
        return $this->type;
    }

    public function setType(?bool $type): BrandMessage
    {
        $this->type = $type;
        return $this;
    }

    public function getFlag(): string
    {
        return $this->flag;
    }

    public function setFlag(?string $flag): BrandMessage
    {
        $this->flag = $flag;
        return $this;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function setCreated(\DateTime $created): BrandMessage
    {
        $this->created = $created;
        return $this;
    }
}