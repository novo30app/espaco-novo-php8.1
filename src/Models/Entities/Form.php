<?php


namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;
/**
 * @Entity @Table(name="form")
 * @ORM @Entity(repositoryClass="App\Models\Repository\FormRepository")
 */
class Form
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ManyToOne(targetEntity="State")
     * @JoinColumn(name="state", referencedColumnName="id")
     */
    private State $state;

    /**
     * @ManyToOne(targetEntity="City")
     * @JoinColumn(name="city", referencedColumnName="id")
     */
    private City $city;

    /**
     * @Column(type="string")
     */
    private string $email;

    /**
     * @Column(type="integer")
     */
    private int $cityParticipation;

    /**
     * @Column(type="string")
     */
    private string $volunteer;

    /**
     * @Column(type="integer")
     */
    private int $electedCandidate;

    /**
     * @Column(type="text")
     */
    private string $strategy;

    /**
     * @Column(type="integer")
     */
    private int $votes;

    /**
     * @Column(type="text")
     */
    private string $events;

    /**
     * @Column(type="string")
     */
    private string $disclosureEvents;

    /**
     * @Column(type="integer")
     */
    private int $followersNow;

    /**
     * @Column(type="integer")
     */
    private int $followers2022;

    /**
     * @Column(type="string")
     */
    private string $socialMedia;

    /**
     * @Column(type="text")
     */
    private string $socialMediaObjective;

    /**
     * @Column(type="text")
     */
    private string $otherDisclosures;

    /**
     * @Column(type="string")
     */
    private string $numberOfAffiliates;

    /**
     * @Column(type="text")
     */
    private string $fundraisingActions;

    /**
     * @Column(type="string")
     */
    private string $fundraisingValue;

    /**
     * @Column(type="text")
     */
    private string $theme;

    /**
     * @Column(type="text")
     */
    private string $danger;

    /**
     * @Column(type="text")
     */
    private string $divisionOfTasks;

    /**
     * @Column(type="string")
     */
    private string $reference;

    /**
     * @Column(type="string")
     */
    private string $newIdea;

    public function getNewIdea(): string
    {
        return $this->newIdea;
    }

    public function setNewIdea(string $newIdea): Form
    {
        $this->newIdea = $newIdea;
        return $this;
    }

    public function getReference(): string
    {
        return $this->reference;
    }

    public function setReference(string $reference): Form
    {
        $this->reference = $reference;
        return $this;
    }

    public function getDivisionOfTasks(): string
    {
        return $this->divisionOfTasks;
    }

    public function setDivisionOfTasks(string $divisionOfTasks): Form
    {
        $this->divisionOfTasks = $divisionOfTasks;
        return $this;
    }


    public function getDanger(): string
    {
        return $this->danger;
    }

    public function setDanger(string $danger): Form
    {
        $this->danger = $danger;
        return $this;
    }

    public function getTheme(): string
    {
        return $this->theme;
    }

    public function setTheme(string $theme): Form
    {
        $this->theme = $theme;
        return $this;
    }

    public function getFundraisingValue(): string
    {
        return $this->fundraisingValue;
    }

    public function setFundraisingValue(string $fundraisingValue): Form
    {
        $this->fundraisingValue = $fundraisingValue;
        return $this;
    }

    public function getFundraisingActions(): string
    {
        return $this->fundraisingActions;
    }

    public function setFundraisingActions(string $fundraisingActions): Form
    {
        $this->fundraisingActions = $fundraisingActions;
        return $this;
    }

    public function getNumberOfAffiliates(): string
    {
        return $this->numberOfAffiliates;
    }

    public function setNumberOfAffiliates(string $numberOfAffiliates): Form
    {
        $this->numberOfAffiliates = $numberOfAffiliates;
        return $this;
    }

    public function getOtherDisclosures(): string
    {
        return $this->otherDisclosures;
    }

    public function setOtherDisclosures(string $otherDisclosures): Form
    {
        $this->otherDisclosures = $otherDisclosures;
        return $this;
    }

    public function getSocialMediaObjective(): string
    {
        return $this->socialMediaObjective;
    }

    public function setSocialMediaObjective(string $socialMediaObjective): Form
    {
        $this->socialMediaObjective = $socialMediaObjective;
        return $this;
    }

    public function getSocialMedia(): string
    {
        return $this->socialMedia;
    }

    public function setSocialMedia(string $socialMedia): Form
    {
        $this->socialMedia = $socialMedia;
        return $this;
    }


    public function getFollowers2022(): int
    {
        return $this->followers2022;
    }


    public function setFollowers2022(int $followers2022): Form
    {
        $this->followers2022 = $followers2022;
        return $this;
    }


    public function getFollowersNow():int
    {
        return $this->followersNow;
    }

    public function setFollowersNow(int $followersNow): Form
    {
        $this->followersNow = $followersNow;
        return $this;
    }

    public function getDisclosureEvents(): string
    {
        return $this->disclosureEvents;
    }

    public function setDisclosureEvents(string $disclosureEvents): Form
    {
        $this->disclosureEvents = $disclosureEvents;
        return $this;
    }

    public function getEvents(): string
    {
        return $this->events;
    }

    public function setEvents(string $events): Form
    {
        $this->events = $events;
        return $this;
    }

    public function getVotes(): int
    {
        return $this->votes;
    }

    public function setVotes(int $votes): Form
    {
        $this->votes = $votes;
        return $this;
    }

    public function getStrategy(): string
    {
        return $this->strategy;
    }

    public function setStrategy(string $strategy): Form
    {
        $this->strategy = $strategy;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getState(): State
    {
        return $this->state;
    }

    public function setState(State $state): Form
    {
        $this->state = $state;
        return  $this;
    }

    public function getCity(): City
    {
        return $this->city;
    }

    public function setCity(City $city): Form
    {
        $this->city = $city;
        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): Form
    {
        $this->email = $email;
        return $this;
    }

    public function getCityParticipation(): int
    {
        return $this->cityParticipation;
    }

    public function setCityParticipation(int $cityParticipation): Form
    {
        $this->cityParticipation = $cityParticipation;
        return $this;
    }

    public function getVolunteer(): string
    {
        return $this->volunteer;
    }

    public function setVolunteer(string $volunteer): Form
    {
        $this->volunteer = $volunteer;
        return $this;
    }

    public function getElectedCandidate(): int
    {
        return $this->electedCandidate;
    }

    public function setElectedCandidate(int $electedCandidate): Form
    {
        $this->electedCandidate = $electedCandidate;
        return $this;
    }



}