<?php
/**
 * Created by PhpStorm.
 * User: rwerl
 * Date: 30/07/2019
 * Time: 16:29
 */

namespace App\Models\Entities;

/**
 * @Entity @Table(name="selectiveProcess2022AnswerModule1")
 * @ORM @Entity(repositoryClass="App\Models\Repository\SelectiveProcess2022AnswerModule1Repository")
 */
class SelectiveProcess2022AnswerModule1
{

    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private \DateTime $created;

    /**
     * @ManyToOne(targetEntity="SelectiveProcess2022Module1Question")
     * @JoinColumn(name="question", referencedColumnName="id")
     */
    private SelectiveProcess2022Module1Question $question;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user", referencedColumnName="id")
     */
    private User $user;

    /**
     * @ManyToOne(targetEntity="SelectiveProcess2022Module1")
     * @JoinColumn(name="selectiveProcess2022Module1", referencedColumnName="id")
     */
    private SelectiveProcess2022Module1 $selectiveProcess2022Module1;

    /**
     * @Column(type="integer")
     */
    private int $score = 0;

    /**
     * @Column(type="string",length=1, options={"fixed" = true})
     */
    private string $optionMarked = 'a';

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function getSelectiveProcess2022Module1(): SelectiveProcess2022Module1
    {
        return $this->selectiveProcess2022Module1;
    }

    public function setSelectiveProcess2022Module1(SelectiveProcess2022Module1 $selectiveProcess2022Module1): SelectiveProcess2022AnswerModule1
    {
        $this->selectiveProcess2022Module1 = $selectiveProcess2022Module1;
        return $this;
    }

    public function getQuestion(): SelectiveProcess2022Module1Question
    {
        return $this->question;
    }

    public function setQuestion(SelectiveProcess2022Module1Question $question): SelectiveProcess2022AnswerModule1
    {
        $this->question = $question;
        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): SelectiveProcess2022AnswerModule1
    {
        $this->user = $user;
        return $this;
    }

    public function getScore(): int
    {
        return $this->score;
    }

    public function setScore(int $score): SelectiveProcess2022AnswerModule1
    {
        $this->score = $score;
        return $this;
    }

    public function getOptionMarked(): string
    {
        return $this->optionMarked;
    }

    public function setOptionMarked(string $optionMarked): SelectiveProcess2022AnswerModule1
    {
        $this->optionMarked = $optionMarked;
        return $this;
    }

}