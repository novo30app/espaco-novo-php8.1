<?php
namespace App\Models\Entities;
use Doctrine\ORM\Mapping as ORM;

/**
 * EventsCart
 *
 * @Entity @Table(name="tb_obtem")
 * @ORM @Entity(repositoryClass="App\Models\Repository\GetRepository")
 */
class Get
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @Column(name="valores", type="string", length=1000, nullable=true)
     */
    private $valores;
    
    /**
     * @Column(type="datetime", name="data_criacao", nullable=true)
     * @var string
     */
    private $dataCriacao;

    public function getId(): int
    {
        return $this->id;
    }

    public function getValue(): ?string
    {
        return  rtrim(Utils::firstUpper($this->valores));
    }

    public function setValue(?string $valores): Get
    {
        $this->valores = $valores;
        return $this;
    }

    public function getDateCreated(): ?\DateTime
    {
        return $this->dataCriacao;
    }

    public function setDateCreated(?\DateTime $dataCriacao): Get
    {
        $this->dataCriacao = $dataCriacao;
        return $this;
    }
}