<?php

namespace App\Models\Entities;
use App\Helpers\Utils;
use Doctrine\ORM\Mapping as ORM;

/**
 * IDontWantToPay
 * @Entity @Table(name="iDontWantToPay")
 * @Entity(repositoryClass="App\Models\Repository\IDontWantToPayRepository")
 */
class IDontWantToPay 
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @Column(type="datetime")
     */
    private \DateTime $created;

    /**
     * @var int|null
     * @Column(name="form", type="integer", nullable=false)
     */
    private $form;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user", referencedColumnName="id")
     * @var User
     */
    private $user;

    /**
     * @var string|null
     *
     * @Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string|null
     *
     * @Column(name="email", type="string", length=100, nullable=false)
     */
    private $email;

    /**
     * @var string|null
     *
     * @Column(name="cpf", type="string", length=100, nullable=false)
     */
    private $cpf;

    /**
     * @var string|null
     *
     * @Column(name="rg", type="string", length=100, nullable=true)
     */
    private $rg;

    /**
     * @var string|null
     *
     * @Column(name="companyName", type="string", length=100, nullable=true)
     */
    private $companyName;

    /**
     * @var string|null
     *
     * @Column(name="companyCNPJ", type="string", length=100, nullable=true)
     */
    private $companyCNPJ;

    /**
     * @ManyToOne(targetEntity="State")
     * @JoinColumn(name="state", referencedColumnName="id")
     * @var State
     */
    private $state;

    /**
     * @ManyToOne(targetEntity="City")
     * @JoinColumn(name="city", referencedColumnName="id")
     * @var City
     */
    private $city;

    /**
     * @var int|null
     * @Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var string|null
     * @Column(name="phone", type="string", nullable=true)
     */
    private $phone;

    /**
     * @var string|null
     * @Column(name="syndicateName", type="string", nullable=true)
     */
    private $syndicateName;

    /**
     * @ManyToOne(targetEntity="State")
     * @JoinColumn(name="syndicateState", referencedColumnName="id")
     * @var State
     */
    private $syndicateState;

    /**
     * @ManyToOne(targetEntity="City")
     * @JoinColumn(name="syndicateCity", referencedColumnName="id")
     * @var City
     */
    private $syndicateCity;

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function setCreated(\DateTime $created): IDontWantToPay
    {
        $this->created = $created;
        return $this;
    }

    public function getForm(): int
    {
        return $this->form;
    }

    public function setForm(int $form): IDontWantToPay
    {
        $this->form = $form;
        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): IDontWantToPay
    {
        $this->user = $user;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): IDontWantToPay
    {
        $this->name = $name;
        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): IDontWantToPay
    {
        $this->email = $email;
        return $this;
    }

    public function getCPF(): string
    {
        return $this->cpf;
    }

    public function setCPF(string $cpf): IDontWantToPay
    {
        $this->cpf = $cpf;
        return $this;
    }

    public function getRG(): string
    {
        return $this->rg;
    }

    public function setRG(string $rg): IDontWantToPay
    {
        $this->rg = $rg;
        return $this;
    }

    public function getCompanyName(): string
    {
        return $this->companyName;
    }

    public function setCompanyName(string $companyName): IDontWantToPay
    {
        $this->companyName = $companyName;
        return $this;
    }

    public function getCompanyCNPJ(): string
    {
        return $this->companyCNPJ;
    }

    public function setCompanyCNPJ(string $companyCNPJ): IDontWantToPay
    {
        $this->companyCNPJ = $companyCNPJ;
        return $this;
    }

    public function getState(): State
    {
        return $this->state;
    }

    public function setState(State $state): IDontWantToPay
    {
        $this->state = $state;
        return $this;
    }

    public function getCity(): City
    {
        return $this->city;
    }

    public function setCity(City $city): IDontWantToPay
    {
        $this->city = $city;
        return $this;
    }

    public function getDate(): \Datetime
    {
        return $this->date;
    }

    public function setDate(\Datetime $date): IDontWantToPay
    {
        $this->date = $date;
        return $this;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): IDontWantToPay
    {
        $this->phone = $phone;
        return $this;
    }

    public function getSyndicateName(): string
    {
        return $this->syndicateName;
    }

    public function setSyndicateName(string $syndicateName): IDontWantToPay
    {
        $this->syndicateName = $syndicateName;
        return $this;
    }

    public function getSyndicateState(): State
    {
        return $this->syndicateState;
    }

    public function setSyndicateState(State $syndicateState): IDontWantToPay
    {
        $this->syndicateState = $syndicateState;
        return $this;
    }

    public function getSyndicateCity(): City
    {
        return $this->syndicateCity;
    }

    public function setSyndicateCity(City $syndicateCity): IDontWantToPay
    {
        $this->syndicateCity = $syndicateCity;
        return $this;
    }
}
