<?php

namespace App\Models\Entities;
use Doctrine\ORM\EntityRepository;
use Doctrine\Mapping as ORM;

/**
 * @Entity @Table(name="vaccineVoucher")
 * @ORM @Entity(repositoryClass="App\Models\Repository\vaccineVoucherRepository")
 */
class vaccineVoucher
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private int $id;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user", referencedColumnName="id")
     */
    private User $user;

    /**
     * @ManyToOne(targetEntity="Events")
     * @JoinColumn(name="event", referencedColumnName="id")
     */
    private Events $event;

    /**
     * @var string|null
     *
     * @Column(name="file", type="string", length=200, nullable=true)
     */
    private $file;

    /**
     * @Column(type="datetime", name="date", nullable=true)
     * @var string
     */
    private $date;

    /**
     * @var bool|null
     *
     * @Column(name="validation", type="boolean", nullable=true)
     */
    private $validation;

    public function getId(): int
    {
        return $this->id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): vaccineVoucher
    {
        $this->user = $user;
        return $this;
    }

    public function getEvent(): Events
    {
        return $this->event;
    }

    public function setEvent(Events $event): vaccineVoucher
    {
        $this->event = $event;
        return $this;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function setFile(?string $file): vaccineVoucher
    {
        $this->file = $file;
        return $this;
    }

    public function getDate(): string
    {
        return $this->date;
    }

    public function setDate(\DateTime $date): vaccineVoucher
    {
        $this->date = $date;
        return $this;
    }

    public function getValidation(): ?bool
    {
        return $this->validation;
    }

    public function setValidation(?bool $validation): vaccineVoucher
    {
        $this->validation = $validation;
        return $this;
    }
}