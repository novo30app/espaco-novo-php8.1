<?php
/**
 * Created by PhpStorm.
 * User: rwerl
 * Date: 16/04/2019
 * Time: 22:52
 */

namespace App\Models\Entities;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="usersAdmin")
 * @ORM @Entity(repositoryClass="App\Models\Repository\UserAdminRepository")
 */
class UserAdmin
{

    const TYPE_ASSISTANT = 1;
    const TYPE_LEADER = 2;
    const TYPE_ADMIN = 3;
    const LEVEL_CITY = 1;
    const LEVEL_STATE = 2;
    const LEVEL_NATIONAL = 3;

    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="integer")
     */
    private int $level = 1;

    /**
     * @Column(type="integer")
     */
    private int $type = 1;

    /**
     * @ManyToOne(targetEntity="City")
     * @JoinColumn(name="city", referencedColumnName="id", nullable=true)
     */
    private ?City $city = null;

    /**
     * @ManyToOne(targetEntity="State")
     * @JoinColumn(name="state", referencedColumnName="id", nullable=true)
     */
    private ?State $state = null;

    /**
     * @Column(type="boolean")
     */
    private bool $active = true;

    /**
     * @Column(type="string")
     */
    private string $name = '';

    /**
     * @Column(type="string")
     */
    private string $password = '';

    /**
     * @Column(type="string", unique=true)
     */
    private string $email = '';


    public function havePermissionBillsToPay()
    {
        return ($this->level == 3);
    }

    public function havePermissionbillsToReceive()
    {
        return ($this->level == 3);
    }

    public function havePermission(int $level, int $type)
    {
        return ($this->level >= $level && $this->type >= $type);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getDirectory():string{

        if (!$this->city && !$this->state) return "DN";
        if (!$this->city && $this->state) return "DE - {$this->getState()->getSigla()}";
        return "DM - {$this->getCity()->getCidade()}/{$this->getState()->getSigla()}";
    }

    public function getLevel(): int
    {
        return $this->level;
    }

    public function setLevel(int $level): UserAdmin
    {
        $this->level = $level;
        return $this;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function setType(int $type): UserAdmin
    {
        $this->type = $type;
        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): UserAdmin
    {
        $this->city = $city;
        return $this;
    }

    public function getState(): ?State
    {
        return $this->state;
    }

    public function setState(?State $state): UserAdmin
    {
        $this->state = $state;
        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): UserAdmin
    {
        $this->active = $active;
        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): UserAdmin
    {
        $this->password = $password;
        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): UserAdmin
    {
        $this->email = $email;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): UserAdmin
    {
        $this->name = $name;
        return $this;
    }

    public function isAdminNational(){
        return $this->level == self::LEVEL_NATIONAL && $this->type == self::TYPE_ADMIN;
    }

}