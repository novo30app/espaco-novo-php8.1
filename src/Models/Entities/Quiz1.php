<?php
/**
 * Created by PhpStorm.
 * User: rwerl
 * Date: 30/07/2019
 * Time: 16:29
 */

namespace App\Models\Entities;

/**
 * @Entity @Table(name="quiz1")
 * @ORM @Entity(repositoryClass="App\Models\Repository\Quiz1Repository")
 */
class Quiz1
{

    /**
     * @Id @GeneratedValue @Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private \DateTime $created;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user", referencedColumnName="id")
     */
    private User $user;

    /**
     * @Column(type="text")
     */
    private string $obs = '';

    /**
     * @Column(type="string",length=1, options={"fixed" = true})
     */
    private string $question1 = '5';

    /**
     * @Column(type="string",length=1, options={"fixed" = true})
     */
    private string $question2 = '5';

    /**
     * @Column(type="string",length=1, options={"fixed" = true})
     */
    private string $question3 = '5';

    /**
     * @Column(type="string",length=1, options={"fixed" = true})
     */
    private string $question4a = '5';

    /**
     * @Column(type="string",length=1, options={"fixed" = true})
     */
    private string $question4b = '5';

    /**
     * @Column(type="string",length=1, options={"fixed" = true})
     */
    private string $question4c = '';

    /**
     * @Column(type="string",length=1, options={"fixed" = true})
     */
    private string $question4d = '5';

    /**
     * @Column(type="string",length=1, options={"fixed" = true})
     */
    private string $question4e = '5';

    /**
     * @Column(type="string",length=1, options={"fixed" = true})
     */
    private string $question4f = '5';

    /**
     * @Column(type="string",length=1, options={"fixed" = true})
     */
    private string $question5 = '5';

    /**
     * @Column(type="string",length=1, options={"fixed" = true})
     */
    private string $question6 = '5';

    /**
     * @Column(type="string",length=1, options={"fixed" = true})
     */
    private string $question7 = '5';

    /**
     * @Column(type="string",length=1, options={"fixed" = true})
     */
    private string $question8 = '5';

    /**
     * @Column(type="string",length=1, options={"fixed" = true})
     */
    private string $question9 = '5';

    /**
     * @Column(type="string",length=1, options={"fixed" = true})
     */
    private string $question10 = '5';

    /**
     * @Column(type="string",length=1, options={"fixed" = true})
     */
    private string $question11 = '5';

    /**
     * @Column(type="string",length=1, options={"fixed" = true})
     */
    private string $question12 = '5';

    /**
     * @Column(type="string",length=1, options={"fixed" = true})
     */
    private string $question13 = '5';

    /**
     * @Column(type="string",length=1, options={"fixed" = true})
     */
    private string $question14a = '5';

    /**
     * @Column(type="string",length=1, options={"fixed" = true})
     */
    private string $question14b = '5';


    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): Quiz1
    {
        $this->user = $user;
        return $this;
    }

    public function getObs(): string
    {
        return $this->obs;
    }

    public function setObs(string $obs): Quiz1
    {
        $this->obs = $obs;
        return $this;
    }

    public function getQuestion1(): string
    {
        return $this->question1;
    }

    public function setQuestion1(string $question1): Quiz1
    {
        $this->question1 = $question1;
        return $this;
    }

    public function getQuestion2(): string
    {
        return $this->question2;
    }

    public function setQuestion2(string $question2): Quiz1
    {
        $this->question2 = $question2;
        return $this;
    }

    public function getQuestion3(): string
    {
        return $this->question3;
    }

    public function setQuestion3(string $question3): Quiz1
    {
        $this->question3 = $question3;
        return $this;
    }

    public function getQuestion4a(): string
    {
        return $this->question4a;
    }

    public function setQuestion4a(string $question4a): Quiz1
    {
        $this->question4a = $question4a;
        return $this;
    }

    public function getQuestion4b(): string
    {
        return $this->question4b;
    }

    public function setQuestion4b(string $question4b): Quiz1
    {
        $this->question4b = $question4b;
        return $this;
    }

    public function getQuestion4c(): string
    {
        return $this->question4c;
    }

    public function setQuestion4c(string $question4c): Quiz1
    {
        $this->question4c = $question4c;
        return $this;
    }

    public function getQuestion4d(): string
    {
        return $this->question4d;
    }

    public function setQuestion4d(string $question4d): Quiz1
    {
        $this->question4d = $question4d;
        return $this;
    }

    public function getQuestion4e(): string
    {
        return $this->question4e;
    }

    public function setQuestion4e(string $question4e): Quiz1
    {
        $this->question4e = $question4e;
        return $this;
    }

    public function getQuestion4f(): string
    {
        return $this->question4f;
    }

    public function setQuestion4f(string $question4f): Quiz1
    {
        $this->question4f = $question4f;
        return $this;
    }

    public function getQuestion5(): string
    {
        return $this->question5;
    }

    public function setQuestion5(string $question5): Quiz1
    {
        $this->question5 = $question5;
        return $this;
    }

    public function getQuestion6(): string
    {
        return $this->question6;
    }

    public function setQuestion6(string $question6): Quiz1
    {
        $this->question6 = $question6;
        return $this;
    }

    public function getQuestion7(): string
    {
        return $this->question7;
    }

    public function setQuestion7(string $question7): Quiz1
    {
        $this->question7 = $question7;
        return $this;
    }

    public function getQuestion8(): string
    {
        return $this->question8;
    }

    public function setQuestion8(string $question8): Quiz1
    {
        $this->question8 = $question8;
        return $this;
    }

    public function getQuestion9(): string
    {
        return $this->question9;
    }

    public function setQuestion9(string $question9): Quiz1
    {
        $this->question9 = $question9;
        return $this;
    }

    public function getQuestion10(): string
    {
        return $this->question10;
    }

    public function setQuestion10(string $question10): Quiz1
    {
        $this->question10 = $question10;
        return $this;
    }

    public function getQuestion11(): string
    {
        return $this->question11;
    }

    public function setQuestion11(string $question11): Quiz1
    {
        $this->question11 = $question11;
        return $this;
    }

    public function getQuestion12(): string
    {
        return $this->question12;
    }

    public function setQuestion12(string $question12): Quiz1
    {
        $this->question12 = $question12;
        return $this;
    }

    public function getQuestion13(): string
    {
        return $this->question13;
    }

    public function setQuestion13(string $question13): Quiz1
    {
        $this->question13 = $question13;
        return $this;
    }

    public function getQuestion14a(): string
    {
        return $this->question14a;
    }

    public function setQuestion14a(string $question14a): Quiz1
    {
        $this->question14a = $question14a;
        return $this;
    }

    public function getQuestion14b(): string
    {
        return $this->question14b;
    }

    public function setQuestion14b(string $question14b): Quiz1
    {
        $this->question14b = $question14b;
        return $this;
    }

}