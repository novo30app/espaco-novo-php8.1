<?php

namespace App\Models\Entities;

use App\Helpers\Utils;
use Doctrine\Mapping as ORM;

/**
 * @Entity @Table(name="tb_diretorio")
 * @ORM @Entity(repositoryClass="App\Models\Repository\DirectoryRepository")
 */

class Directory
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="nome", type="string", length=255, nullable=false)
     */
    private $nome;

    /**
     * @var string|null
     *
     * @Column(name="url_doacao", type="string", length=60, nullable=true)
     */
    private $urlDoacao;

    /**
     * @var string|null
     *
     * @Column(name="recibo_descricao", type="string", length=120, nullable=true)
     */
    private $reciboDescricao;

    /**
     * @var string|null
     *
     * @Column(name="recibo_mascara", type="string", length=24, nullable=true)
     */
    private $reciboMascara;

    /**
     * @var string|null
     *
     * @Column(name="recibo_emite", type="string", length=0, nullable=true)
     */
    private $reciboEmite;

    /**
     * @var string|null
     *
     * @Column(name="recebe_email", type="string", length=0, nullable=true)
     */
    private $recebeEmail;

    /**
     * @var string|null
     *
     * @Column(name="cnpj", type="string", length=20, nullable=true)
     */
    private $cnpj;

    /**
     * @var string
     *
     * @Column(name="telefone", type="string", length=20, nullable=false)
     */
    private $telefone;

    /**
     * @var string|null
     *
     * @Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @Column(name="endereco", type="string", length=255, nullable=false)
     */
    private $endereco;

    /**
     * @var int
     *
     * @Column(name="tb_cidade_id", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $tbCidadeId;

    /**
     * @var int
     *
     * @Column(name="tb_estado_id", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $tbEstadoId;

    /**
     * @var string
     *
     * @Column(name="ativo", type="string", length=0, nullable=false, options={"default"="S"})
     */
    private $ativo = 'S';

    /**
     * @var string
     *
     * @Column(name="principal", type="string", length=0, nullable=false, options={"default"="N"})
     */
    private $principal = 'N';

    /**
     * @var string
     *
     * @Column(name="estadual", type="string", length=0, nullable=false, options={"default"="N"})
     */
    private $estadual = 'N';

    /**
     * @var string
     *
     * @Column(name="municipal", type="string", length=0, nullable=false, options={"default"="N"})
     */
    private $municipal = 'N';

    /**
     * @var \DateTime
     *
     * @Column(name="criado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $criado = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @Column(name="atualizado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $atualizado = 'CURRENT_TIMESTAMP';

    /**
     * @var bool|null
     *
     * @Column(name="exibir", type="boolean", nullable=true)
     */
    private $exibir;

    /**
     * @var string|null
     *
     * @Column(name="ec", type="string", length=30, nullable=true)
     */
    private $ec;

    /**
     * @var string|null
     *
     * @Column(name="agencia", type="string", length=6, nullable=true)
     */
    private $agencia;

    /**
     * @var string|null
     *
     * @Column(name="conta_corrente", type="string", length=8, nullable=true)
     */
    private $contaCorrente;

    /**
     * @var string|null
     *
     * @Column(name="banco", type="string", length=11, nullable=true)
     */
    private $banco;

    /**
     * @var string|null
     *
     * @Column(name="n_totvs", type="string", length=6, nullable=true)
     */
    private $nTotvs;

    /**
     * @var string|null
     *
     * @Column(name="conta_contabil_or", type="string", length=50, nullable=true)
     */
    private $contaContabilOr;

    /**
     * @var string|null
     *
     * @Column(name="conta_contabil_ca", type="string", length=50, nullable=true)
     */
    private $contaContabilCa;

    /**
     * @var string|null
     *
     * @Column(name="inicio_vigencia", type="string", length=15, nullable=true)
     */
    private $inicioVigencia;

    /**
     * @var string|null
     *
     * @Column(name="termino_vigencia", type="string", length=15, nullable=true)
     */
    private $terminoVigencia;

    /**
     * @var string|null
     *
     * @Column(name="nome_presidente", type="string", length=100, nullable=true)
     */
    private $nomePresidente;

    /**
     * @var string|null
     *
     * @Column(name="cpf_presidente", type="string", length=25, nullable=true)
     */
    private $cpfPresidente;

    /**
     * @var string|null
     *
     * @Column(name="profissao_presidente", type="string", length=70, nullable=true)
     */
    private $profissaoPresidente;

    /**
     * @var string|null
     *
     * @Column(name="estadocivil_presidente", type="string", length=20, nullable=true)
     */
    private $estadocivilPresidente;

    /**
     * @var int|null
     *
     * @Column(name="celular_presidente", type="integer", nullable=true)
     */
    private $celularPresidente;

    /**
     * @var string|null
     *
     * @Column(name="endereco_presidente", type="string", length=70, nullable=true)
     */
    private $enderecoPresidente;

    /**
     * @var int|null
     *
     * @Column(name="numero_presidente", type="integer", nullable=true)
     */
    private $numeroPresidente;

    /**
     * @var string|null
     *
     * @Column(name="bairro_presidente", type="string", length=70, nullable=true)
     */
    private $bairroPresidente;

    /**
     * @var string|null
     *
     * @Column(name="nome_vicepresidente", type="string", length=100, nullable=true)
     */
    private $nomeVicepresidente;

    /**
     * @var string|null
     *
     * @Column(name="cpf_vicepresidente", type="string", length=25, nullable=true)
     */
    private $cpfVicepresidente;

    /**
     * @var string|null
     *
     * @Column(name="profissao_vicepresidente", type="string", length=70, nullable=true)
     */
    private $profissaoVicepresidente;

    /**
     * @var string|null
     *
     * @Column(name="estadocivil_vicepresidente", type="string", length=20, nullable=true)
     */
    private $estadocivilVicepresidente;

    /**
     * @var int|null
     *
     * @Column(name="celular_vicepresidente", type="integer", nullable=true)
     */
    private $celularVicepresidente;

    /**
     * @var string|null
     *
     * @Column(name="endereco_vicepresidente", type="string", length=70, nullable=true)
     */
    private $enderecoVicepresidente;

    /**
     * @var int|null
     *
     * @Column(name="numero_vicepresidente", type="integer", nullable=true)
     */
    private $numeroVicepresidente;

    /**
     * @var string|null
     *
     * @Column(name="bairro_vicepresidente", type="string", length=70, nullable=true)
     */
    private $bairroVicepresidente;

    /**
     * @var string|null
     *
     * @Column(name="nome_secretariofinancas", type="string", length=100, nullable=true)
     */
    private $nomeSecretariofinancas;

    /**
     * @var string|null
     *
     * @Column(name="cpf_secretariofinancas", type="string", length=25, nullable=true)
     */
    private $cpfSecretariofinancas;

    /**
     * @var string|null
     *
     * @Column(name="profissao_secretariofinancas", type="string", length=70, nullable=true)
     */
    private $profissaoSecretariofinancas;

    /**
     * @var string|null
     *
     * @Column(name="estadocivil_secretariofinancas", type="string", length=20, nullable=true)
     */
    private $estadocivilSecretariofinancas;

    /**
     * @var int|null
     *
     * @Column(name="celular_secretariofinancas", type="integer", nullable=true)
     */
    private $celularSecretariofinancas;

    /**
     * @var string|null
     *
     * @Column(name="endereco_secretariofinancas", type="string", length=70, nullable=true)
     */
    private $enderecoSecretariofinancas;

    /**
     * @var int|null
     *
     * @Column(name="numero_secretariofinancas", type="integer", nullable=true)
     */
    private $numeroSecretariofinancas;

    /**
     * @var string|null
     *
     * @Column(name="bairro_secretariofinancas", type="string", length=70, nullable=true)
     */
    private $bairroSecretariofinancas;

    /**
     * @var string|null
     *
     * @Column(name="nome_secretarioadministrativo", type="string", length=100, nullable=true)
     */
    private $nomeSecretarioadministrativo;

    /**
     * @var string|null
     *
     * @Column(name="cpf_secretarioadministrativo", type="string", length=25, nullable=true)
     */
    private $cpfSecretarioadministrativo;

    /**
     * @var string|null
     *
     * @Column(name="profissao_secretarioadministrativo", type="string", length=70, nullable=true)
     */
    private $profissaoSecretarioadministrativo;

    /**
     * @var string|null
     *
     * @Column(name="estadocivil_secretarioadministrativo", type="string", length=20, nullable=true)
     */
    private $estadocivilSecretarioadministrativo;

    /**
     * @var int|null
     *
     * @Column(name="celular_secretarioadministrativo", type="integer", nullable=true)
     */
    private $celularSecretarioadministrativo;

    /**
     * @var string|null
     *
     * @Column(name="endereco_secretarioadministrativo", type="string", length=70, nullable=true)
     */
    private $enderecoSecretarioadministrativo;

    /**
     * @var int|null
     *
     * @Column(name="numero_secretarioadministrativo", type="integer", nullable=true)
     */
    private $numeroSecretarioadministrativo;

    /**
     * @var string|null
     *
     * @Column(name="bairro_secretarioadministrativo", type="string", length=70, nullable=true)
     */
    private $bairroSecretarioadministrativo;

    /**
     * @var string|null
     *
     * @Column(name="nome_secretarioassuntos", type="string", length=100, nullable=true)
     */
    private $nomeSecretarioassuntos;

    /**
     * @var string|null
     *
     * @Column(name="cpf_secretarioassuntos", type="string", length=25, nullable=true)
     */
    private $cpfSecretarioassuntos;

    /**
     * @var string|null
     *
     * @Column(name="profissao_secretarioassuntos", type="string", length=70, nullable=true)
     */
    private $profissaoSecretarioassuntos;

    /**
     * @var string|null
     *
     * @Column(name="estadocivil_secretarioassuntos", type="string", length=20, nullable=true)
     */
    private $estadocivilSecretarioassuntos;

    /**
     * @var int|null
     *
     * @Column(name="celular_secretarioassuntos", type="integer", nullable=true)
     */
    private $celularSecretarioassuntos;

    /**
     * @var string|null
     *
     * @Column(name="endereco_secretarioassuntos", type="string", length=70, nullable=true)
     */
    private $enderecoSecretarioassuntos;

    /**
     * @var int|null
     *
     * @Column(name="numero_secretarioassuntos", type="integer", nullable=true)
     */
    private $numeroSecretarioassuntos;

    /**
     * @var string|null
     *
     * @Column(name="bairro_secretarioassuntos", type="string", length=70, nullable=true)
     */
    private $bairroSecretarioassuntos;

    public function getId()
    {
        return $this->id;
    }

    public function getCnpj()
    {
        return $this->cnpj;
    }

    public function getName()
    {
        return $this->nome;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getStateId(): int
    {
        return $this->tbEstadoId ?? 0;
    }

    public function getCityId(): int
    {
        return $this->tbCidadeId ?? 0;
    }

    public function isState(): bool
    {
        return strtoupper($this->estadual) == 'S';
    }

    public function isCity(): bool
    {
        return strtoupper($this->municipal) == 'S';
    }

    public function getBank()
    {
        return $this->banco;
    }

    public function getAgency()
    {
        return $this->agencia;
    }

    public function getAccount()
    {
        return $this->contaCorrente;
    }
    
    public function getMaskReceipt()
    {
        return $this->reciboMascara;
    }

    public function getReciboEmite()
    {
        return $this->reciboEmite;
    }
}
