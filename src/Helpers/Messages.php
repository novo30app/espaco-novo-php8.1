<?php

namespace App\Helpers;

use App\Models\Commons\Entities\Benefit;
use App\Models\Commons\Entities\UserPoint;
use App\Models\Entities\Address;
use App\Models\Entities\User;
use App\Models\Entities\Directory;
use App\Services\Email;

class Messages
{
    public static function emailMembershipAwaitingPayment($name, $slip)
    {
        return "Olá, {$name}. <br><br>    
                Este é um aviso automático para lembrá-lo de efetuar o pagamento da sua contribuição no boleto bancário. <br><br>    
                <a href='{$slip['bank_slip']['bank_slip_pdf_url']}' class='btn btn-primary text-white text-center' target='_blank'>Acessar Boleto</a> <br><br>   
                <b>Código de barras:</b> {$slip['bank_slip']['digitable_line']} <br><br> 
                Fique atento ao prazo de vencimento! Se precisar gerar outro boleto ou mudar a forma de pagamento, 
                acesse o menu Financeiro dentro do <a href='https://espaco-novo.novo.org.br/login'>Espaço NOVO</a>.;
                Se já efetuou o pagamento, desconsidere este aviso. <br><br>    
                Equipe NOVO. <br><br>      
                Caso tenha alguma dúvida, entre em contato com nosso setor de filiação através do e-mail filiacao@novo.org.br ou 
                pelo <a href='https://novo.org.br/acompanhe/fale-conosco/' target='_blank'>Fale Conosco</a>.";
    }

    public static function affiliationPaymentConfirmed($user)
    {
        $days = $user->getFiliado() == 14 ? '10 dias' : '5 dias';
        $payment = $user->getExemption() == 0 ? 'acompanhada do seu pagamento' : '';
        return "Olá, {$user->getName()}. <br><br>    
                Você declarou que não ocupa cargo eletivo nos poderes executivo e legislativo (vereador, deputado, senador), 
                bem como não se encontra na condição de suplente. <br><br>
                Por cumprir os requisitos, Recebemos sua solicitação de filiação {$payment}. <br><br>
                Desde já agradecemos o seu apoio e participação no projeto do NOVO! <br><br>    
                Pedimos que aguarde até {$days} úteis para análise do seu pedido de filiação, quando então enviaremos a confirmação e seu número de filiação. <br><br>    
                Enquanto isso, convidamos a conhecer mais sobre o NOVO: <br><br>    
                Em nosso site, conheça nossos <a href='https://novo.org.br/partido/valores/' target='_blank'>valores</a> e diferenciais, 
                nossos <a href='novo.org.br/partido/desafios' target='_blank'>desafios</a> e nossos <a href='novo.org.br/partido/diretórios' target='_blank'>diretórios</a> 
                e líderes no Brasil, acesse também a nossa <a href='https://novo.org.br/participe/eventos/' target='_blank'>agenda</a> e confira onde serão nossos próximos eventos pelo Brasil. <br><br>    
                Não deixe de nos acompanhar nas nossas redes sociais. É através delas que o NOVO mais se comunica. Curta, comente e compartilhe. Participe! <br><br>  
                Facebook: <a href='https://facebook.com/novo30'>https://facebook.com/novo30</a><br>
                Instagram: <a href='https://instagram.com/partidonovo3'>https://instagram.com/partidonovo30</a><br>
                Twitter: <a href='https://twitter.com/partidonovo30'>https://twitter.com/partidonovo30</a><br>
                YouTube: <a href='https://youtube.com/partidonovo30'>https://youtube.com/partidonovo30</a><br><br>
                Parabéns por decidir ser mais um agente de mudanças. A renovação do Brasil depende do nosso protagonismo e você será parte fundamental dessa transformação. <br><br>
                Até breve!<br>
                NOVO, fazendo o certo para mudar o Brasil. <br><br>
                Caso tenha alguma dúvida, entre em contato com nosso setor de filiação através do e-mail filiacao@novo.org.br ou 
                pelo <a href='https://novo.org.br/acompanhe/fale-conosco/' target='_blank'>Fale Conosco</a>.<br><br>
                Equipe NOVO!";
    }

    public static function disaffectionEmail(User $user, string $reason)
    {
        $cityElectoral = $user->getTituloEleitoralMunicipioId() ? $user->getTituloEleitoralMunicipioId()->getCidade() : $user->getTituloEleitoralMunicipio();
        $ufElectoral = $user->getTituloEleitoralUfId() ? $user->getTituloEleitoralUfId()->getEstado() : $user->getTituloEleitoralUf();
        $today = date('d/m/Y');
        return "DATA DA DESFILIAÇÃO: {$user->getDataDesfiliacao()->format('d/m/Y')} <br>
                NOME DO FILIADO: {$user->getName()} <br>
                EMAIL DO FILIADO:  {$user->getEmail()}  <br>
                DETALHE DO MOTIVO: {$reason} <br><br>
                DADOS ELEITORAIS: <br>
                TÍTULO DE ELEITOR: {$user->getTituloEleitoral()} <br>
                ZONA: {$user->getTituloEleitoralZona()}<br>
                SEÇÃO:  {$user->getTituloEleitoralSecao()}<br>
                MUNICIPIO ELEITORAL: {$cityElectoral}<br>
                UF ELEITORAL: {$ufElectoral}<br>
            ";
    }

    public static function membershipNotice($value2)
    {
        $msg = "Bom dia,<br>
                Seguem abaixo pessoas do seu municipio em período de impugnação:<br><br>";
        foreach ($value2 as $k) {
            $msg .= "Nome: " . $k['nome'] . "<br>
                     Cpf: " . $k['cpf'] . "<br>
                     Idade: " . $k['idade'] . "<br>
                     Telefone: " . $k['telefone'] . "<br>
                     Cidade: " . $k['cidade'] . "<br>
                     Estado: " . $k['estado'] . "<br>
                     Status: " . $k['status'] . "<br>
                     Filiação Isenta: " . $k['isenta'] . "<br><br>";
        }
        return $msg .= "Qualquer dúvida contatar filiacao@novo.org.br!<br>Equipe NOVO!";
    }

    public static function refiliation($value2)
    {
        $msg = "Olá, <br>
                Favor acessar o sistema de gestão e analisar os pedidos de refiliação abaixo: <br><br>";
        foreach ($value2 as $k) {
            $msg .= "Nome: " . $k['nome'] . "<br>
                         Cpf: " . $k['cpf'] . "<br>
                         Municipio eleitoral: " . $k['titulo_eleitoral_municipio'] . "<br>
                         UF eleitoral: " . $k['titulo_eleitoral_uf'] . "<br>
                         Titulo de Eleitor: " . $k['titulo_eleitoral'] . "<br>
                         Zona: " . $k['titulo_eleitoral_zona'] . "<br>
                         Seção: " . $k['titulo_eleitoral_secao'] . "<br>
                         Data de solicitação de refiliação: " . $k['data_solicitacao_refiliacao'] . "<br><br>";
        }
        $msg .= "Para visualizar todos os pedidos pendentes de aprovação <a href='https://gestao.novo.org.br/consulta/refiliacao'>Clique Aqui!</a><br><br>
                    Qualquer dúvida contatar ti@novo.org.br!<br>
                    Equipe NOVO!";
        return $msg;
    }

    public static function affiliationAlert(User $user): string
    {
        $msg = "Olá,<br>
                Uma nova indicação de filiação foi realizada para {$user->getName()},<br>
                Confira as informações <a href='https://gestao.novo.org.br/filiados/{$user->getId()}/'>Clicando aqui!</a><br><br>
                Atenção: este é um e-mail automático; por favor não responda.<br><br>
                Equipe NOVO!";
        return $msg;
    }

    public static function affiliation(string $name, string $lastAffiliatedIdFree, int $term): string
    {
        $msg = "Olá {$name}!<br><br>";
        if ($term == 1) {
            $msg .= "Por declarar que não ocupa cargo eletivo nos poderes executivo e legislativo (vereador, deputado, senador), bem como não se encontra na condição de suplente.<br><br>";
        }
        $msg .= "O seu pedido de filiação foi confirmado.<br>
                Seja bem-vindo ao NOVO!<br><br>
                Parabéns por decidir ser mais um agente de mudanças. A renovação do Brasil depende do nosso protagonismo e você será parte fundamental dessa transformação,<br><br>
                Seu número de filiado é: <b><u>" . $lastAffiliatedIdFree . "</u></b>.<br><br>
                A partir de agora, passa a ter acesso ao Espaço NOVO, um espaço onde todo filiado pode ter conhecimento sobre nosso estatuto, os documentos legais e verificar nossas prestações de contas.<br><br>
                Reiteramos o nosso convite para frequentar os nossos encontros, reuniões e apresentações. Acesse a nossa agenda e confira onde serão os próximos eventos pelo Brasil: <a href='https://novo.org.br/participe/eventos/'>Agenda de eventos!</a>,<br><br>
                Não deixe de acompanhar as nossas redes sociais. É através delas que o NOVO mais se comunica. Curta, comente e compartilhe.<br><br>
                Facebook: <a href='https://facebook.com/novo30'>https://facebook.com/novo30</a><br>
                Instagram: <a href='https://instagram.com/partidonovo3'>https://instagram.com/partidonovo30</a><br>
                Twitter: <a href='https://twitter.com/partidonovo30'>https://twitter.com/partidonovo30</a><br>
                YouTube: <a href='https://youtube.com/partidonovo30'>https://youtube.com/partidonovo30</a><br><br>
                NOVO, fazendo o certo para mudar o Brasil!<br>
                Atenção: este é um e-mail automático; por favor não responda.";
        return $msg;
    }

    public static function affiliationFiliation($record, $date)
    {
        return "Favor inserir este usuário no Filiaweb: <br><br>
                Nome: " . $record['name'] . "<br>
                Email: " . $record['email'] . "<br> 
                Municipio eleitoral: " . $record['city'] . "<br>
                UF eleitoral: " . $record['state'] . "<br>
                Titulo de Eleitor: " . $record['titulo_eleitoral'] . "<br>
                Zona: " . $record['titulo_eleitoral_zona'] . "<br>
                Seção: " . $record['titulo_eleitoral_secao'] . "<br>
                Data da filiação: " . $date . "<br><br>
                NOVO, fazendo o certo para mudar o Brasil!<br>
                Atenção: este é um e-mail automático; por favor não responda.";
    }

    public static function affiliationCep($record, $cep)
    {
        return "Atenção,<br>
                Há uma nova filiação que se encontrava com um processo em aberto para análise da CEP, <br><br>
                Nome: " . $record['name'] . "<br>
                Email: " . $record['email'] . "<br> 
                Municipio eleitoral: " . $record['city'] . "<br>
                UF eleitoral: " . $record['state'] . "<br>
                Titulo de Eleitor: " . $record['titulo_eleitoral'] . "<br>
                Zona: " . $record['titulo_eleitoral_zona'] . "<br>
                Seção: " . $record['titulo_eleitoral_secao'] . "<br>
                Data da filiação: " . date('d/m/Y') . "<br>
                Descrição do processo: " . $cep->getDescription() . "<br><br>
                Equipe NOVO!";
    }

    public static function refreshInvoice($firstName, $event, $type)
    {
        return "Parabéns " . $firstName . ",<br><br>
                Sua " . $type . " no evento " . $event->getName() . " foi confirmada com sucesso.<br>
                Para ter acesso aos seus ingressos ou transferir para outra pessoa <a href='https://espaco-novo.novo.org.br/eventos/meus-eventos'>Clique aqui!</a><br><br>
                Fique atento ao dia e horário do evento, aguardamos ansiosamente sua presença<br>
                Atenção: este é um e-mail automático, por favor não responda!<br><br>
                Equipe NOVO";
    }

    public static function notificationTerm0Filiation($user)
    {
        return "Atenção,<br>
                Uma nova solicitação de filiação foi realizada e o titular indicou nos termos a seguinte opção:<br><br>
                - Possuo cargo de primeiro escalão de relevância política e/ou pública.<br><br>
                Seguem dados:<br>
                - Nome: {$user->getName()}<br>
                - E-mail: {$user->getEmail()}<br>
                - CPF: {$user->getCpf()}<br><br>
                Equipe NOVO!";
    }

    public static function notificationTerm0Dam($user)
    {
        return "Atenção,<br>
                Uma nova solicitação de filiação foi realizada e o titular indicou nos termos a seguinte opção:<br><br>
                - Possuo mandato legislativo.<br><br>
                Seguem dados:<br>
                - Nome: {$user->getName()}<br>
                - E-mail: {$user->getEmail()}<br>
                - CPF: {$user->getCpf()}<br><br>
                Equipe NOVO!";
    }

    public static function notificationTerm0Affiliation()
    {
        return "Como definido na Resolução 19/2019<br>
                Para prosseguir com a sua filiação é necessário ter sido aprovado na avaliação de migração de mandatários. 
                Se o seu processo não tenha sido iniciado ou finalizado por favor entre em contato com o Departamento de Apoio ao Mandatário pelo email <a href='mailto:dam@novo.org.br'>dam@novo.org.br</a>";
    }

    public static function eventsPaymentCard($user, $type, $event)
    {
        return "Olá " . $user->getName() . ",<br><br>
                Sua " . $type . " no Evento - " . $event->getName() . " foi confirmada com sucesso.<br><br>
                Seus ingressos já estão disponíveis em seu Espaço NOVO, para visualizar ou transferir para outra pessoa <a href='https://espaco-novo.novo.org.br/eventos/meus-eventos'>Clique aqui!</a>.<br><br>
                Convide seus amigos para participar também<br>
                E veja mais eventos do NOVO <a href='http://novo.org.br/participe/eventos/'>Clicando aqui!</a><br><br>
                Aguardamos ansiosamente sua presença<br>
                Equipe NOVO!";
    }

    public static function eventsPaymentSlip($user, $type, $event, $slip, $ticket)
    {
        return "Olá " . $user->getName() . ",<br>
                Sua " . $type . " no Evento - " . $event->getName() . " foi solicitada com sucesso.<br>
                No link abaixo você poderá visualizar seu boleto para pagamento,<br><br>
                Link Boleto: " . $slip['bank_slip']['bank_slip_pdf_url'] . ".<br><br>
                Lembramos que você terá 3 dias úteis para pagamento do boleto e após a validação do débito sua " . $type . " será confirmada.<br><br>" . $ticket .
            "Fique atento ao prazo de vencimento!<br>
                Se já efetuou o pagamento, desconsidere este aviso.<br>
                Equipe NOVO!";
    }

    public static function eventsPaymentPix($user, $type, $event, $ticket, $pix)
    {
        return "Olá " . $user->getName() . ",<br>
                Sua " . $type . " no Evento - " . $event->getName() . " foi solicitada com sucesso.<br>"
            . $ticket .
            "Abaixo você poderá visualizar seu Qrcode para pagamento,<br><br>
                <img src=\"{$pix['pix']['qrcode']}\" style='width: 40%;'><br><br>
                Lembramos que sua " . $type . " só será confirmada após o pagameto.<br><br>
                Se já efetuou o pagamento, desconsidere este aviso.<br>
                Equipe NOVO!";
    }

    public static function eventsPaymentMessage($paymentMethod, $type, $iugu = null)
    {
        switch ($paymentMethod) {
            case 1:
                return "Pagamento realizado com sucesso!<br> 
                        Parabéns, sua " . $type . " foi confirmada,<br>
                        Um e-mail foi enviado para sua caixa de entrada, o NOVO agradece sua participação!";
                break;
            case 2:
                return "Boleto gerado com sucesso. <br>
                        Sua " . $type . " está quase lá, realize o pagamento do boleto abaixo para confirmar sua participação.
                        Código de Barras: {$iugu['bank_slip']['digitable_line']} <br> 
                        <span style='margin: 4%;' class='btn btn-primary' data-clipboard-text='{$iugu['bank_slip']['digitable_line']}'>Copiar Código de Barras <i class='fa fa-barcode'></i></span>
                        <a class='btn btn-success' href='{$iugu['bank_slip']['bank_slip_pdf_url']}' target='_blank'>Baixar o boleto <i class='fa fa-file-pdf-o'></i></a>";
                break;
            case 5:
                return "QR Code gerado com sucesso!<br>
                        <img src=\"{$iugu['pix']['qrcode']}\" style='width: 50%;'> <br>
                        <span class='btn btn-primary' data-clipboard-text='{$iugu['pix']['qrcode_text']}'>PIX Copia e Cola<i class='fa fa-qrcode'></i></span> <br>
                        <b>O sistema pode levar até 24h para dar a baixa na inscrição.</b>";
                break;
            default:
                return "Pagamento realizado com sucesso!<br> 
                        Parabéns, sua Inscrição foi confirmada,<br>
                        Um e-mail foi enviado para sua caixa de entrada, o NOVO agradece sua participação!";
                break;
        }
    }

    public static function subscriptionOnline($data, $firstName, $event)
    {
        $linkEvent = '';
        if ($event->getLinkOnline()) $linkEvent = "Segue link abaixo, para acessar no horário do evento:<br> Link: " . $event->getLinkOnline() . "<br><br>";
        if ($event->getId() == 6414) $linkEvent = "<b>Fique de olho aqui na sua Caixa de Entrada. No dia do evento, vamos enviar um e-mail para você com o link da sala no Zoom.</b><br><br>";
        return "Parabéns " . $firstName . ",<br><br>
                Sua inscrição no evento " . $event->getName() . " no dia  " . $event->getDateBegin()->format('d/m/Y H:i:s') . " foi confirmada com sucesso.<br><br>"
            . $linkEvent .
            "Convide seus amigos para participar deste evento também,<br>
                E veja mais eventos do NOVO em https://novo.org.br/participe/eventos<br>
                Equipe NOVO!";
    }

    public static function subscriptionOnlineEvent6311($firstName)
    {
        return "Parabéns {$firstName}, Você se inscreveu no Encontro Online das Protagonistas pelo Brasil!<br><br> 
                Você acabou de dar um grande passo para assumir o protagonismo na política de sua cidade.<br>
                Agora, engaje outras filiadas do NOVO a participarem do Encontro Online e mãos à obra!<br><br>
                Entre para o grupo do WhatsApp do Encontro Online para ficar por dentro de tudo:<br>
                https://chat.whatsapp.com/JharERhmy9z8UHK6A0iEbL<br><br>
                Este é o link de acesso para a reunião:<br>
                https://streamyard.com/watch/URpnVYB6uBzE<br><br>
                Fique tranquila! Enviaremos o acesso novamente no dia do evento.<br>
                Esperamos por você!<br>
                Mulheres pelo NOVO!";
    }

    public static function subscriptionLogin($data, $user, $event)
    {
        return "Parabéns " . $user->getFirstName() . ",<br><br>
                Sua inscrição no evento " . $event->getName() . " no dia  " . $event->getDateBegin()->format('d/m/Y H:i:s') . " foi confirmada com sucesso.<br><br>
                Seus ingressos já estão disponíveis em seu Espaço NOVO, para visualizar ou transferir para outra pessoa <a href='https://espaco-novo.novo.org.br/eventos/meus-eventos'>Clique aqui</a>.<br><br>
                Convide seus amigos para participar deste evento também,<br>
                E veja mais eventos do NOVO em https://novo.org.br/participe/eventos<br>
                Equipe NOVO!";
    }

    public static function chargeTransactionSplip($user, $slip)
    {
        return "Olá " . $user->getName() . ",<br><br>
                Você solicitou um boleto para pagamento de sua contribuição, abaixo seguem Código de barras e o boleto para pagamento:<br><br>
                Código de Barras: " . $slip['bank_slip']['digitable_line'] . "<br>
                Boleto: <a href='{$slip['bank_slip']['bank_slip_pdf_url']}'>" . $slip['bank_slip']['bank_slip_pdf_url'] . "</a><br><br>
                Equipe NOVO!";
    }

    public static function exemptionAffiliation()
    {
        return "Parabéns!<br>
            Você concluiu a primeira etapa para sua Filiação ao NOVO!<br>
            Enviamos para seu e-mail a confirmação e os detalhes mais importantes, para concluir o processo.<br>
            E agora, você já pode acessar seu ambiente exclusivo, no Espaço NOVO. Aperte no botão abaixo:";
    }

    public static function rescue($rescue)
    {
        $date = date('d/m/Y H:i:s');
        return "Bom dia<br>
                Resgate semanal do IUGU solicitado com sucesso!<br>
                Abaixo seguem informações<br><br>
                ID Saque: {$rescue['id']}<br>
                Valor Saque: {$rescue['amount']}<br>
                Data Saque: {$date}<br><br>
                Para fazer o download das transações <a href='https://gestao.novo.org.br/api/financeiro/transacoes-resgate/{$rescue['id']}'>Clique aqui!</a><br><br>
                Para mais detalhes sobre o saque ou conciliação <a href='https://gestao.novo.org.br/financeiro/conciliacao-iugu/'>Clique aqui!</a><br>
                Dúvidas entrar em contato com ti@novo.org.br<br><br>
                Equipe NOVO!";
    }

    public static function rescueError($amount)
    {
        $date = date('d/m/Y H:i:s');
        $amount = number_format($amount, 2, ',', '.');
        return "Bom dia<br>
                Houve um erro ao solicitar o resgate semanal do IUGU!<br>
                Abaixo seguem informações<br><br>
                Valor Saque: {$amount}<br>
                Data Saque: {$date}<br><br>
                Entrar em contato com ti@novo.org.br<br><br>
                Equipe NOVO!";
    }

    public static function rescueErrorQueryAccount($queryAccount, $amount)
    {
        $date = date('d/m/Y H:i:s');
        $value = number_format($amount, 2, ',', '.');
        return "Bom dia<br>
                Houve um erro ao solicitar o resgate semanal do IUGU!<br><br>
                O valor solicitado é superior ao disponível em conta,<br><br>
                Valor Disponível: {$queryAccount['balance_available_for_withdraw']}<br> 
                Valor Solicitado: {$value}<br>
                Data Saque: {$date}<br><br>
                Solicitar o saque manualmente e informar valor sacado para ti@novo.org.br.<br><br>
                Equipe NOVO!";
    }

    public static function increaseContributionSave(string $name, string $value, string $period): string
    {
        return "Olá {$name}, recebemos com muita alegria o seu pedido de aumento de contribuição.<br><br>
                A sua nova contribuição será no valor de R$ {$value} na periocidade {$period}.<br><br>
                Este é um momento delicado do nosso país, mas temos uma oportunidade única de mostrar que temos o melhor projeto para melhorar a vida do cidadão brasileiro.<br><br>
                Ao aumentar o valor da sua contribuição, você ajuda o NOVO a alcançar ainda mais pessoas e a chegar a todas as cidades possíveis.<br><br>
                Todos juntos, conseguiremos dar passos cada vez maiores para transformar a realidade do Brasil!";
    }

    public static function messageReprocessChargeCard(array $record): array
    {
        $date = date('d/m/Y', strtotime($record['data_criacao']));
        switch ($record['days']) {
            case 2:
                $step = 2;
                $msg = "O pagamento da sua contribuição mensal com vencimento programado para " . $date . " foi recusado.<br/>
                                    Por favor, verifique se os dados do cartão estão atualizados.<br/>
                                    É só clicar aqui e inserir um cartão válido:";
                $msg2 = "Lembre-se: a sua contribuição é essencial para o crescimento do Partido NOVO e para que possamos realizar a nossa missão de transformar o Brasil.";
                break;
            case 4:
                $step = 3;
                $msg = "Ainda não conseguimos processar o pagamento da sua contribuição mensal com vencimento programado para  " . $date . " .<br/>
                                    Por favor, verifique se os dados do cartão estão atualizados.<br/>
                                    É só clicar aqui e inserir um cartão válido:";
                $msg2 = "O Partido NOVO não existira sem a contribuição dos nossos filiados! Ajude-nos a fazer a transformação que o Brasil precisa.";
                break;
            case 10:
                $step = 4;
                $msg = "Ainda não recebemos a confirmação de pagamento da sua contribuição mensal com vencimento programado para  " . $date . " .<br/>
                                    Por favor, verifique com a operadora do seu cartão de crédito ou clique aqui para inserir um cartão válido";
                $msg2 = "O Partido NOVO precisa da sua contribuição e da sua participação!";
                break;
            default:
                $step = 0;
                $msg = $msg2 = '';
        }
        return [$msg, $msg2, $step];
    }

    public static function protagonists(): string
    {
        return "Olá, segue abaixo o link para download da planilha de exportação de mulheres protagonistas<br/>
                <a href='http://espaco-novo.novo.org.br/api/protagonistas/exporta/'>Clique aqui para baixar</a>.<br/>
                Dúvidas entrar em contato com <a href='mailto:ti@novo.org.br'>ti@novo.org.br.</a><br/>
                Equipe NOVO!";
    }

    public static function requestBenefit(string $name, Benefit $benefiti): string
    {
        $msg = "<p>Olá, {$name}!</p>";
        $msg .= "<p>Sua solicitação foi recebida com sucesso e o benefício {$benefiti->getName()} está disponível para resgate imediato.</p>";
        $msg .= "<p><b>Faça isso para Resgatar o Benefício {$benefiti->getName()}:</b></p>";
        $msg .= "<p>1º - Vá até a Loja do NOVO durante o Encontro Nacional 2024 <br>
                    2º - Informe seu CPF aos atendentes para que possam localizar seu benefício</p>";
        $msg .= "<p>E pronto!</p>";
        $msg .= "<p>É simples e rápido.</p>";
        $msg .= "<p>Não perca a oportunidade de aproveitar essa vantagem exclusiva. </p>";
        $msg .= "<p>Sua participação e dedicação ao nosso movimento são valiosas, e queremos garantir que você receba todos os benefícios disponíveis!</p>";
        $msg .= "<p>A gente respeita o Brasil.</p>";
        $msg .= "<p>Equipe NOVO</p>";
        return $msg;
    }

    public static function missionCompleted(UserPoint $userPoint, int $points): string
    {
        $msg = "<p>Olá, {$userPoint->getUser()->getName()}!</p>";
        $msg .= "<p>Parabéns por ter cumprido a Missão {$userPoint->getMission()->getTitle()}! Seu empenho e dedicação são fundamentais para o sucesso do nosso movimento.</p>";
        $msg .= "<p>- Missão Cumprida: {$userPoint->getMission()->getTitle()} <br>
                    - Pontuação Recebida: {$userPoint->getPoints()} <br>
                    - Pontuação Total Acumulada: {$points}</p>";
        $msg .= "<p>Você é indispensável para o NOVO. E queremos que você aproveite ao máximo os benefícios disponíveis!</p>";
        $msg .= "<p><a href='https://espaco-novo.novo.org.br/novo-mobiliza/beneficios' target='_blank'>Veja quais são os benefícios</a></p>";
        $msg .= "<p>Mas não pare por aí! </p>";
        $msg .= "<p>Há muitas outras missões esperando por você. Continue contribuindo e acumulando pontos para alcançar ainda mais vantagens.</p>";
        $msg .= "<p><a href='https://espaco-novo.novo.org.br/novo-mobiliza/' target='_blank'>Veja quais são as missões</a></p>";
        $msg .= "<p>Mais uma vez, parabéns por sua conquista e por ser parte ativa do nosso movimento.</p>";
        $msg .= "<p>Contamos com você para continuar fazendo a diferença no nosso país!</p>";
        $msg .= "<p>A gente respeita o Brasil.</p>";
        $msg .= "<p>Equipe NOVO</p>";
        return $msg;
    }

    public static function requestBenefitParam(User $user, Benefit $benefiti): void
    {
        $firstName = explode(' ', $user->getName())[0];
        $title = $benefiti->getTitle();
        $title = str_replace('[Nome Benefício]', $benefiti->getName(), $title);
        $msg = nl2br($benefiti->getDescription());
        $msg = str_replace('[Nome Benefício]', $benefiti->getName(), $msg);
        $msg = str_replace('[Primeiro Nome]', $firstName, $msg);
        Email::send($user->getEmail(), $user->getName(), $title, $msg);
    }

    public static function receiptReserved(array $receipts, Directory $directory): string
    {
        $msg = "Recibos reservados para este mês para o {$directory->getName()}.<br><br>";
        foreach($receipts as $r) {
            $msg .= "Recibo: <b>" . $r . "</b><br>";
        }
        $msg .= "<br>Equipe NOVO!";
        return $msg;
    }
}