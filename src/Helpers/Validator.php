<?php

namespace App\Helpers;

class Validator
{

    public static function validDate(string $date)
    {
        $dateArr = explode('/', $date);
        if (!checkdate($dateArr[1], $dateArr[0], $dateArr[2])) {
            throw new \Exception("Data inválida: {$date}");
        }
        return true;
    }

    public static function validateRangeNumber($number, $field, $min = PHP_FLOAT_MIN, $max = PHP_FLOAT_MAX)
    {
        if ($number < $min) {
            throw new \Exception("Valor minimo para o campo {$field} é {$min}");
        }
        if ($number > $max) {
            throw new \Exception("Valor máximo para o campo {$field} é {$max}");
        }
    }

    public static function validateSubscription($data, $type)
    {
        $data['card'] ??= 0;
        $min = 10;
        if ($type == 2) {
            switch ($data['frequency']) {
                case 1:
                    $min = MINMONTHLY;
                    break;
                case 6:
                    $min = MINSEMESTER;
                    break;
                case 12:
                    $min = MINYEARLY;
                    break;
                default:
                    $min = MINMONTHLY;
                    break;
            }
        }
        if ($data['value'] < $min) {
            $min = number_format($min, 2, ',', '');
            throw new \Exception("Valor minimo para a filiação é R$ {$min}");
        }

        if ($data['value'] > 1064) {
            throw new \Exception('Valor máximo para a filiação é R$ 1064,00');
        }
        if ($data['day'] <= 0 || $data['day'] > 30) {
            throw new \Exception('Informe um dia entre 1 e 30');
        }
        if ($data['paymentMethod'] == 1 && !$data['card']) {
            throw new \Exception('Selecione um cartão de crédito');
        }
        if ($data['day'] <= 0 || $data['day'] > 30) {
            throw new \Exception('Informe um dia entre 1 e 30');
        }
    }

    public static function validateEmail($data)
    {
        if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            throw new \Exception('Email inválido.');
        }
        //if ($data['email2'] != $data['email']) {
        //    throw new \Exception('Os emails informados são diferentes.');
        //}
    }

    public static function validatePassword($data)
    {
        //if ($data['password2'] != $data['password']) {
        //    throw new \Exception('As senhas digitadas são diferentes');
        //}
        if (strlen($data['password']) < 8) {
            throw new \Exception('A senha deve ter pelo menos 8 caracteres');
        }

    }

    public static function requireValidator($fields, $data)
    {
        foreach ($fields as $key => $value) {
            if (!array_key_exists($key, $data) || (is_string($data[$key]) && trim($data[$key]) === '') || $data[$key] === null) {
                throw new \Exception('O campo ' . $value . ' é obrigátorio');
            }
        }
    }

    public static function validateCPF(string $cpf)
    {
        if (empty($cpf)) {
            throw new \Exception('CPF inválido');
        }
        $cpf = preg_replace("/[^0-9]/", "", $cpf);
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

        if (strlen($cpf) != 11) {
            throw new \Exception('CPF inválido');
        } else if ($cpf == '00000000000' ||
            $cpf == '11111111111' ||
            $cpf == '22222222222' ||
            $cpf == '33333333333' ||
            $cpf == '44444444444' ||
            $cpf == '55555555555' ||
            $cpf == '66666666666' ||
            $cpf == '77777777777' ||
            $cpf == '88888888888' ||
            $cpf == '99999999999') {
            throw new \Exception('CPF inválido');
        } else {
            for ($t = 9; $t < 11; $t++) {
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf[$c] * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf[$c] != $d) {
                    throw new \Exception('CPF inválido');
                }
            }
        }
    }

    public static function validateCep(array $address): bool
    {
        $result = false;
        if ($address['outside'] == 0) {
            $url = "https://viacep.com.br/ws/{$address['zip_code']}/json/";
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = json_decode(curl_exec($ch), true);
            $result = $result['erro'] ? false : true;
            curl_close($ch);
        }
        return $result;
    }

    public static function validatePassword2($data)
    {
        if ($data['password2'] != $data['password']) throw new \Exception('As senhas digitadas são diferentes');
        if (strlen($data['password']) < 8) throw new \Exception('A senha deve ter pelo menos 8 caracteres');
    }

}