<?php
/**
 * Created by PhpStorm.
 * User: werlich
 * Date: 13/01/20
 * Time: 17:50
 */

namespace App\Helpers;


class Utils
{

    public static function formatMoney(float $value, bool $symbol = true): string
    {
        if (!$symbol) return number_format($value, 2, ',', '.');
        return "R$ " . number_format($value, 2, ',', '.');
    }

    public static function firstUpper($word):string {
        $word = strtolower($word);
        $words = explode(' ', $word);
        $result = '';
        foreach ($words as $word){
            $result .= ucfirst($word) . ' ';
        }
        return $result;
    }

    public static function getAcessData(): array
    {
        $u_agent = $_SERVER['HTTP_USER_AGENT'];
        $bname = 'Unknown';
        $platform = 'Unknown';
        $version = "";
        //First get the platform?
        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'Linux';
        } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'Mac';
        } elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'windows';
        }
        // Next get the name of the useragent yes seperately and for good reason
        if (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
            $bname = 'Internet Explorer';
            $ub = "MSIE";
        } elseif (preg_match('/Firefox/i', $u_agent)) {
            $bname = 'Mozilla Firefox';
            $ub = "Firefox";
        } elseif (preg_match('/OPR/i', $u_agent)) {
            $bname = 'Opera';
            $ub = "Opera";
        } elseif (preg_match('/Chrome/i', $u_agent) && !preg_match('/Edge/i', $u_agent)) {
            $bname = 'Google Chrome';
            $ub = "Chrome";
        } elseif (preg_match('/Safari/i', $u_agent) && !preg_match('/Edge/i', $u_agent)) {
            $bname = 'Apple Safari';
            $ub = "Safari";
        } elseif (preg_match('/Netscape/i', $u_agent)) {
            $bname = 'Netscape';
            $ub = "Netscape";
        } elseif (preg_match('/Edge/i', $u_agent)) {
            $bname = 'Edge';
            $ub = "Edge";
        } elseif (preg_match('/Trident/i', $u_agent)) {
            $bname = 'Internet Explorer';
            $ub = "MSIE";
        }
        // finally get the correct version number
        $known = array('Version', $ub, 'other');
        $pattern = '#(?<browser>' . join('|', $known) .
            ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
            // we have no matching number just continue
        }
        // see how many we have
        $i = count($matches['browser']);
        if ($i != 1) {
            //we will have two since we are not using 'other' argument yet
            //see if version is before or after the name
            if (strripos($u_agent, "Version") < strripos($u_agent, $ub)) {
                $version = $matches['version'][0];
            } else {
                $version = $matches['version'][1];
            }
        } else {
            $version = $matches['version'][0];
        }

        // check if we have a number
        if ($version == null || $version == "") {
            $version = "?";
        }

        return [
            'userAgent' => $u_agent,
            'name' => $bname,
            'version' => $version,
            'platform' => $platform,
            'pattern' => $pattern,
            'ip' => $_SERVER['HTTP_X_FORWARDED_FOR']
        ];
    }

    public static function generateToken(int $length = 50)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString . time();
    }

    public static function formatCpf(string $cpf): string
    {
        return preg_replace("/(\d{3})(\d{3})(\d{3})(\d{2})/", "\$1.\$2.\$3-\$4", $cpf);
    }

    public static function onlyNumbers(string $str){
        return preg_replace("/[^0-9]/", "", $str);
    }

    public static function describeValue($value)
    {    
        $singular = array("centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
        $plural = array("centavos", "reais", "mil", "milhões", "bilhões", "trilhões","quatrilhões");
    
        $c = array("", "cem", "duzentos", "trezentos", "quatrocentos","quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos");
        $d = array("", "dez", "vinte", "trinta", "quarenta", "cinquenta","sessenta", "setenta", "oitenta", "noventa");
        $d10 = array("dez", "onze", "doze", "treze", "quatorze", "quinze","dezesseis", "dezesete", "dezoito", "dezenove");
        $u = array("", "um", "dois", "três", "quatro", "cinco", "seis","sete", "oito", "nove");
    
        $z=0;
        $rt = null;
    
        $value = number_format($value, 2, ".", ".");
        $integer = explode(".", $value);
        for($i=0;$i<count($integer);$i++)
            for($ii=strlen($integer[$i]);$ii<3;$ii++)
                $integer[$i] = "0".$integer[$i];
    
        // $fim identifica onde que deve se dar junção de centenas por "e" ou por "," ;)
        $end = count($integer) - ($integer[count($integer)-1] > 0 ? 1 : 2);
        for ($i=0;$i<count($integer);$i++) {
            $value = $integer[$i];
            $rc = (($value > 100) && ($value < 200)) ? "cento" : $c[$value[0]];
            $rd = ($value[1] < 2) ? "" : $d[$value[1]];
            $ru = ($value > 0) ? (($value[1] == 1) ? $d10[$value[2]] : $u[$value[2]]) : "";

            $r = $rc.(($rc && ($rd || $ru)) ? " e " : "").$rd.(($rd && $ru) ? " e " : "").$ru;
            $t = count($integer)-1-$i;
            $r .= $r ? " ".($value > 1 ? $plural[$t] : $singular[$t]) : "";
            if ($value == "000")$z++; elseif ($z > 0) $z--;
            if (($t==1) && ($z>0) && ($integer[0] > 0)) $r .= (($z>1) ? " de " : "").$plural[$t]; 
            if ($r) $rt = $rt . ((($i > 0) && ($i <= $end) && ($integer[0] > 0) && ($z < 1)) ? ( ($i < $end) ? ", " : " e ") : " ") . $r;
        }    
        return($rt ? $rt : "zero");
    }
}