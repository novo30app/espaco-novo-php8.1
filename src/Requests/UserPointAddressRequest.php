<?php

namespace App\Requests;

class UserPointAddressRequest extends FormRequest
{
    public function __construct($data)
    {
        $this->data = $data;
        $this->rules = $this->rules();
        $this->attributes = $this->attributes();
    }

    public function rules(): array
    {
        return [
            'zipCode' => ['required'],
            'uf' => ['required'],
            'city' => ['required'],
            'neighborhood' => ['required'],
            'address' => ['required'],
            'number' => ['required'],
        ];
    }

    public function attributes(): array
    {
        return [
            'zipCode' => 'CEP',
            'uf' => 'Estado',
            'city' => 'Cidade',
            'neighborhood' => 'Bairro',
            'address' => 'Endereço',
            'number' => 'Número',
        ];
    }
}

