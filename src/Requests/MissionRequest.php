<?php

namespace App\Requests;

class MissionRequest extends FormRequest
{
    public function __construct($data)
    {
        $this->data = $data;
        $this->rules = $this->rules();
        $this->attributes = $this->attributes();
    }

    public function rules(): array
    {
        return [
            'doc' => ['required'],
        ];
    }

    public function attributes(): array
    {
        return [
            'doc' => 'Documento',
        ];
    }
}
