<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;


$app->group('/api/sistema', function () use ($app) {

    $app->get('/cidades/{state}/', fn(Request $request, Response $response) => $this->ApiController->getCitiesByState($request, $response));

    $app->options('/cidades/{state}/', function (Request $request, Response $response) {
        return;
    });

    $app->get('/cidades-mandatarios/{state}/', fn(Request $request, Response $response) => $this->ApiController->getCitiesByAgents($request, $response));

    $app->options('/candidatos-mandatarios/{state}/', function (Request $request, Response $response) {
        return;
    });

    $app->get('/candidatos/{state}/', fn(Request $request, Response $response) => $this->ApiController->getCandidatesByState($request, $response));

    $app->options('/candidatos/{state}/', function (Request $request, Response $response) {
        return;
    });

    $app->get('/candidatos-por-cidade/{city}/', fn(Request $request, Response $response) => $this->ApiController->getCandidatesByCity($request, $response));

    $app->options('/candidatos-por-cidade/{city}/', function (Request $request, Response $response) {
        return;
    });
});



