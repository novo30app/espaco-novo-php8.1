<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;


$app->group('/api/financeiro', function () use ($app) {

    $app->get('/assinaturas/[{id}/]', fn(Request $request, Response $response) => $this->FinancialController->apiSubscriptions($request, $response));

    $app->delete('/assinaturas/{id}/', fn(Request $request, Response $response) => $this->FinancialController->stopSubscription($request, $response));

    $app->put('/assinaturas/ativar/{id}/', fn(Request $request, Response $response) => $this->FinancialController->activateSubscription($request, $response));

    $app->put('/assinaturas/', fn(Request $request, Response $response) => $this->FinancialController->saveSubscription($request, $response));

    $app->post('/assinaturas/', fn(Request $request, Response $response) => $this->FinancialController->saveSubscription($request, $response));

    $app->get('/meus-cartoes/[{id}/]', fn(Request $request, Response $response) => $this->FinancialController->apiCards($request, $response));

    $app->put('/meus-cartoes/ativar/{id}/', fn(Request $request, Response $response) => $this->FinancialController->activateCard($request, $response));

    $app->put('/altera-cartao/', fn(Request $request, Response $response) => $this->FinancialController->refreshCard($request, $response));

    $app->delete('/meus-cartoes/{id}/', fn(Request $request, Response $response) => $this->FinancialController->stopCard($request, $response));

    $app->post('/meus-cartoes/', fn(Request $request, Response $response) => $this->FinancialController->saveCard($request, $response));

    $app->post('/cadastra-cartao/', fn(Request $request, Response $response) => $this->FinancialController->saveCardApi($request, $response));

    $app->get('/gera-boleto/', fn(Request $request, Response $response) => $this->FinancialController->generateSlip($request, $response));

    $app->post('/doacao/', fn(Request $request, Response $response) => $this->FinancialController->payDonation($request, $response));

    $app->post('/pagamento/', fn(Request $request, Response $response) => $this->FinancialController->chargeTransaction($request, $response));

    $app->post('/anticipation/', fn(Request $request, Response $response) => $this->FinancialController->anticipationGenerate($request, $response));
    
    $app->post('/payoff/', fn(Request $request, Response $response) => $this->FinancialController->payoffGenerate($request, $response));

    $app->post('/contribuicoes-abertas/', fn(Request $request, Response $response) => $this->FinancialController->payoffGenerate($request, $response));

    $app->post('/atualiza-fatura-libertas/', fn(Request $request, Response $response) => $this->FinancialController->refreshInvoiceLibertas($request, $response));

    $app->get('/faturas/{id}/', fn(Request $request, Response $response) => $this->PublicController->invoiceSite($request, $response));

    $app->post('/pagamento-fatura/', fn(Request $request, Response $response) => $this->PublicController->saveInvoiceSite($request, $response));

    $app->post('/pagamento-planos/', fn(Request $request, Response $response) => $this->FinancialController->paymentPlans($request, $response));

});