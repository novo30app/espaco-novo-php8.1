<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;



$app->group('/api/usuario', function () use ($app) {

    $app->put('/alterar-senha/', fn(Request $request, Response $response) => $this->UserController->savePassword($request, $response));

    $app->put('/dados-pessoais/', fn(Request $request, Response $response) => $this->UserController->savePersonalData($request, $response));

    $app->put('/desfiliacao/', fn(Request $request, Response $response) => $this->UserController->saveDisaffection($request, $response));

    $app->post('/recuperar-senha/', fn(Request $request, Response $response) => $this->LoginController->saveRecover($request, $response));

    $app->put('/recuperar-senha/', fn(Request $request, Response $response) => $this->LoginController->savePassword($request, $response));

    $app->put('/alterar-email/', fn(Request $request, Response $response) => $this->UserController->saveEmail($request, $response));
    
    $app->post('/knowledge/', fn(Request $request, Response $response) => $this->UserController->saveKnowledge($request, $response));

    $app->post('/cancelar-filiacao/', fn(Request $request, Response $response) => $this->UserController->SaveAffiliationCancel($request, $response));

    $app->put('/solicita-contato/', fn(Request $request, Response $response) => $this->UserController->requestContact($request, $response));

});
