<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;


$app->group('/api/dam', function () use ($app) {

    $app->get('/gastos/', fn(Request $request, Response $response) => $this->ApiController->getSpending($request, $response));

    $app->get('/tabela-gastos/', fn(Request $request, Response $response) => $this->ApiController->getSpendingTable($request, $response));

    $app->get('/presencas/', fn(Request $request, Response $response) => $this->ApiController->getPresences($request, $response));

    $app->get('/votacoes/', fn(Request $request, Response $response) => $this->ApiController->getPolls($request, $response));

    $app->get('/relatorios/', fn(Request $request, Response $response) => $this->ApiController->getReports($request, $response));

    $app->get('/projetos/', fn(Request $request, Response $response) => $this->ApiController->getProjectsApi($request, $response));

});