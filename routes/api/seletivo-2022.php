<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/api/seletivo-2022', function () use ($app) {
    $app->get('/modulo-1/questoes/{id}/', fn(Request $request, Response $response) => $this->SelectiveProcessController->apiSelective2022Module1Questions($request, $response));
    $app->get('/extra/{id}/', fn(Request $request, Response $response) => $this->SelectiveProcessController->extraData($request, $response));
    $app->get('/modulo-1/{id}/', fn(Request $request, Response $response) => $this->SelectiveProcessController->apiSelective2022Module1($request, $response));
    $app->post('/integracao/', fn(Request $request, Response $response) => $this->SelectiveProcessController->auth($request, $response));
});