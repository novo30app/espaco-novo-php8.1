<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;


$app->group('/api/cadastros', function () use ($app) {

    $app->post('/landing-page/', fn(Request $request, Response $response) => $this->RegisterController->saveRegister($request, $response));

    $app->post('/abaixo-assinado/', fn(Request $request, Response $response) => $this->RegisterController->undersigned($request, $response));

    $app->post('/cadastro-jornada2024/', fn(Request $request, Response $response) => $this->RegisterController->journey2024($request, $response));

});