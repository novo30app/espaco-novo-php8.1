<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;


$app->group('/api/solicitacoes', function () use ($app) {

    $app->post('/impugnacao/', fn(Request $request, Response $response) => $this->RequestsController->impeachment($request, $response));

    $app->get('/filiacao/', fn(Request $request, Response $response) => $this->RequestsController->membershipList($request, $response));

});



