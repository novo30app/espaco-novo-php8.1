<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/api/filiacao', function () use ($app) {

    $app->options('/[{variavel}/]', function (Request $request, Response $response) {
        return;
    });

    $app->get('/{cpf}/', fn(Request $request, Response $response) => $this->AffiliationController->validateCpf($request, $response));
    
    $app->post('/dados/', fn(Request $request, Response $response) => $this->AffiliationController->dados($request, $response));

    $app->post('/pagamento/', fn(Request $request, Response $response) => $this->AffiliationController->charge($request, $response));

    $app->post('/parcial/', fn(Request $request, Response $response) => $this->AffiliationController->partial($request, $response));

    $app->post('/', fn(Request $request, Response $response) => $this->AffiliationController->register($request, $response));

    $app->post('/configura-senha/', fn(Request $request, Response $response) => $this->AffiliationController->registerPassword($request, $response));

    $app->post('/inicio-filiacao/', fn(Request $request, Response $response) => $this->AffiliationController->beginFiliation($request, $response));

    $app->post('/primeiro-registro-filiacao/', fn(Request $request, Response $response) => $this->AffiliationController->firstRegister($request, $response));

    $app->post('/atualiza-status/', fn(Request $request, Response $response) => $this->UserController->updateStatus($request, $response));

});

$app->group('/api', function () use ($app) {

    $app->options('/doacao/', function (Request $request, Response $response) {
        return;
    });

    $app->post('/doacao/', fn(Request $request, Response $response) => $this->AffiliationController->publicDonation($request, $response));

    $app->options('/verifica-filiado/', function (Request $request, Response $response) {
        return;
    });

    $app->get('/verifica-filiado/{value}/', fn(Request $request, Response $response) => $this->ApiController->checkAffiliated($request, $response));
    
    $app->get('/academia-do-amanha/{value}/', fn(Request $request, Response $response) => $this->ApiController->checkAcademiaDoAmanha($request, $response));

    $app->options('/cadastro/', function (Request $request, Response $response) {
        return;
    });

    $app->post('/cadastro/', fn(Request $request, Response $response) => $this->AffiliationController->publicRegister($request, $response));

    $app->options('/cadastra-cartao/', function (Request $request, Response $response) {
        return;
    });

    $app->post('/cadastra-cartao/', fn(Request $request, Response $response) => $this->FinancialController->saveCardApi($request, $response));

    $app->options('/whatsapp-group/', function (Request $request, Response $response) {
        return;
    });

    $app->get('/whatsapp-group/', fn(Request $request, Response $response) => $this->ApiController->whatsAppGroups($request, $response));

    $app->options('/chatbot-boleto/', function (Request $request, Response $response) {
        return;
    });

    $app->get('/chatbot-boleto/', fn(Request $request, Response $response) => $this->FinancialController->chatbotBillet($request, $response));

    $app->options('/consulta-filiado/', function (Request $request, Response $response) {
        return;
    });

    $app->get('/consulta-filiado/{cpf}/', fn(Request $request, Response $response) => $this->ApiController->affiliateConsultation($request, $response));

    $app->options('/valores-filiacao/', function (Request $request, Response $response) {
        return;
    });

    $app->get('/valores-filiacao/', fn(Request $request, Response $response) => $this->PublicController->filiationValues($request, $response));

    $app->options('/pega-dados/', function (Request $request, Response $response) {
        return;
    });

    $app->post('/pega-dados/', fn(Request $request, Response $response) => $this->ApiController->getData($request, $response));

});

$app->group('/api/agente-de-ia', function () use ($app) {

    $app->options('/[{variavel}/]', function (Request $request, Response $response) {
        return;
    });

    $app->get('/pega-dados/{value}/', fn(Request $request, Response $response) => $this->UserController->getDataIA($request, $response));

    $app->post('/gera-boleto-total/{email}/', fn(Request $request, Response $response) => $this->FinancialController->generateTotalContributionIA($request, $response));

    $app->post('/gera-boleto-total/{email}/{limit}/', fn(Request $request, Response $response) => $this->FinancialController->generateTotalContributionIA($request, $response));

});

$app->group('/api/atualizacao-iugu', function () use ($app) {

    $app->options('/[{variavel}/]', function (Request $request, Response $response) {
        return;
    });

    $app->get('/atualizacao-iugu-get/', fn(Request $request, Response $response) => $this->PublicController->getUserRefreshCard($request, $response));

    $app->post('/atualizacao-iugu-post/', fn(Request $request, Response $response) => $this->PublicController->saveRefreshCard($request, $response));

});

$app->group('/api/aumenta-contribuicao', function () use ($app) {

    $app->options('/[{variavel}/]', function (Request $request, Response $response) {
        return;
    });

    $app->get('/identifica/', fn(Request $request, Response $response) => $this->PublicController->increaseContribution($request, $response));

    $app->post('/efetiva/', fn(Request $request, Response $response) => $this->PublicController->increaseContributionSave($request, $response));

});

$app->group('/api/eventos', function () use ($app) {

    $app->options('/[{variavel}/]', function (Request $request, Response $response) {
        return;
    }); 

    $app->post('/', fn(Request $request, Response $response) => $this->EventsController->subscriptionOnline($request, $response));

    $app->post('/evento-login/', fn(Request $request, Response $response) => $this->EventsController->eventsLogin($request, $response));

    $app->post('/login/', fn(Request $request, Response $response) => $this->EventsController->subscriptionLogin($request, $response));

    $app->post('/pagamento/', fn(Request $request, Response $response) => $this->EventsController->eventsPayment($request, $response));

    $app->post('/comprovante-vacina/', fn(Request $request, Response $response) => $this->EventsController->vaccineVoucher($request, $response));

    $app->get('/listagem/', fn(Request $request, Response $response) => $this->EventsController->events($request, $response));
    
    $app->get('/detalhes/', fn(Request $request, Response $response) => $this->EventsController->eventsDetails($request, $response));

    $app->get('/ingressos/', fn(Request $request, Response $response) => $this->EventsController->eventsTickets($request, $response));

    $app->get('/uf/', fn(Request $request, Response $response) => $this->EventsController->eventsByUf($request, $response));
    
    $app->get('/cidade/', fn(Request $request, Response $response) => $this->EventsController->eventsByCity($request, $response));

    $app->get('/especie/', fn(Request $request, Response $response) => $this->EventsController->eventsBySpecie($request, $response));

    $app->post('/recomendacao/', fn(Request $request, Response $response) => $this->EventsController->recommendation($request, $response));

    $app->get('/cartoes/', fn(Request $request, Response $response) => $this->EventsController->cards($request, $response));

    $app->post('/valida-cpf/', fn(Request $request, Response $response) => $this->EventsController->validateCpf($request, $response));

    $app->post('/imprensa/', fn(Request $request, Response $response) => $this->EventsController->subscriptionPress($request, $response));

});

$app->group('/api/chatbot', function () use ($app) {

    $app->get('/consulta-cpf/', fn(Request $request, Response $response) => $this->ChatBotController->verifyCpf($request, $response));

    $app->get('/busca-contribuicao/', fn(Request $request, Response $response) => $this->ChatBotController->searchTransaction($request, $response));

});

$app->group('/api/movidesk', function () use ($app) {

    $app->options('/[{variavel}/]', function (Request $request, Response $response) {
        return;
    });

    $app->post('/pega-dados/', fn(Request $request, Response $response) => $this->PublicController->getDataMovidesk($request, $response));

});

$app->group('/api/pesquisa', function () use ($app) {

    $app->options('/[{variavel}/]', function (Request $request, Response $response) {
        return;
    });

    $app->get('/pega-dados/', fn(Request $request, Response $response) => $this->PublicController->getDataSearch($request, $response));

    $app->post('/', fn(Request $request, Response $response) => $this->PublicController->saveSearch($request, $response));

    $app->get('/exporta-filiados/{token}/', fn(Request $request, Response $response) => $this->PublicController->csvAffiliatedSearch($request, $response));

    $app->get('/exporta-desfiliados/{token}/', fn(Request $request, Response $response) => $this->PublicController->csvUnaffiliatedSearch($request, $response));

    $app->post('/informativa/', fn(Request $request, Response $response) => $this->PublicController->saveSearchInformative($request, $response));

    $app->get('/exporta-pesquisa-informativa/{token}/', fn(Request $request, Response $response) => $this->PublicController->csvInformativeSearch($request, $response));

});

$app->group('/api/cobranca', function () use ($app) {

    $app->options('/[{variavel}/]', function (Request $request, Response $response) {
        return;
    });

    $app->get('/cartao-cobranca/', fn(Request $request, Response $response) => $this->ApiController->getChargeCard($request, $response));

    $app->get('/boleto-cobranca-lembrete/', fn(Request $request, Response $response) => $this->ApiController->getChargeBillet($request, $response));

    $app->get('/boleto-cobranca-vencido/', fn(Request $request, Response $response) => $this->ApiController->getChargeBilletDefeated($request, $response));

});

$app->group('/api/nao-quero-pagar', function () use ($app) {

    $app->options('/[{variavel}/]', function (Request $request, Response $response) {
        return;
    });

    $app->post('/', fn(Request $request, Response $response) => $this->RegisterController->IDontWantToPay($request, $response));

    $app->get('/gera-declaracao/{cpf}/', fn(Request $request, Response $response) => $this->RegisterController->generateDeclarationPDF($request, $response));

});