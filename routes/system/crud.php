<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->get('/crud/{model}/{url}/[{folder}/]', fn(Request $request, Response $response) => $this->CrudController->index($request, $response));
