<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/cursos-libertas', function () use ($app) {
    $app->get('/', fn(Request $request, Response $response) => $this->LibertasCoursesController->coursesIndex($request, $response));
    $app->get('/json/api/', fn(Request $request, Response $response) => $this->LibertasCoursesController->coursesList($request, $response));
    $app->get('/curso/[{id}/]', fn(Request $request, Response $response) => $this->LibertasCoursesController->coursesView($request, $response));
    $app->post('/compra/', fn(Request $request, Response $response) => $this->LibertasCoursesController->buy($request, $response));
    $app->post('/compra/site/', fn(Request $request, Response $response) => $this->LibertasCoursesController->publicBuy($request, $response));
    $app->get('/minhas-compras/', fn(Request $request, Response $response) => $this->LibertasCoursesController->buysIndex($request, $response));
    $app->get('/minhas-compras/json/api/', fn(Request $request, Response $response) => $this->LibertasCoursesController->buysList($request, $response));
    $app->get('/notas-fiscais/', fn(Request $request, Response $response) => $this->LibertasCoursesController->generateNfs());
    $app->get('/cupom/{course}/{code}/', fn(Request $request, Response $response) => $this->LibertasCoursesController->getCoupon($request, $response));
});