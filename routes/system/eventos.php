<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/eventos', function () use ($app) {

    $app->get('/meus-eventos/', fn(Request $request, Response $response) => $this->EventsController->myEvents($request, $response));

    $app->get('/ingresso/{id}/', fn(Request $request, Response $response) => $this->EventsController->myTickets($request, $response));

    $app->post('/edita-ingresso/', fn(Request $request, Response $response) => $this->EventsController->editTicket($request, $response));

});