<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/consulta', function () use ($app) {
    $app->get('/', fn(Request $request, Response $response) => $this->ConsultController->index($request, $response));
    $app->post('/', fn(Request $request, Response $response) => $this->ConsultController->save($request, $response));
});