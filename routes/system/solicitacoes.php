<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/solicitacoes', function () use ($app) {

    $app->get('/filiacao/', fn(Request $request, Response $response) => $this->RequestsController->membership($request, $response));

});