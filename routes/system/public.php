<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->get('/usuario/alterar-email/{token}/', fn(Request $request, Response $response) => $this->UserController->changeEmail($request, $response));
$app->get('/filiacao/', fn(Request $request, Response $response) => $this->AffiliationController->index($request, $response));
$app->get('/filiacao/pagamento/{cpf}/', fn(Request $request, Response $response) => $this->AffiliationController->payment($request, $response));
$app->get('/filiacao/{cpf}/', fn(Request $request, Response $response) => $this->AffiliationController->filication($request, $response));
$app->get('/planejamento-estrategico-municipal-2022/', fn(Request $request, Response $response) => $this->PublicController->municipalStrategicPlanning2022($request, $response));
$app->post('/planejamento-estrategico-municipal-2022/cadastro/', fn(Request $request, Response $response) => $this->PublicController->saveMunicipalStrategicPlanning2022($request, $response));
$app->post('/totvs/', fn(Request $request, Response $response) => $this->PublicController->totvsRevenue($request, $response));
$app->post('/totvs-erro/', fn(Request $request, Response $response) => $this->PublicController->totvsTransactionError($request, $response));
$app->get('/economizometro/{size}/', fn(Request $request, Response $response) => $this->PublicController->economizometer($request, $response));
$app->get('/confirmacao-de-filiacao/{hash}/', fn(Request $request, Response $response) => $this->AffiliationController->confirmAffiliation($request, $response));