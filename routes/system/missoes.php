<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/missoes', function () use ($app) {
    $app->get('/', fn(Request $request, Response $response) => $this->MissionController->index($request, $response));
    $app->get('/pesquisa/', fn(Request $request, Response $response) => $this->MissionController->redirectResearch($request, $response));
    $app->get('/pesquisa2/', fn(Request $request, Response $response) => $this->MissionController->redirectResearch2($request, $response));
    $app->post('/indicar-realizacao/', fn(Request $request, Response $response) => $this->MissionController->indicateAchievement($request, $response));
    $app->get('/json/api/', fn(Request $request, Response $response) => $this->MissionController->list($request, $response));
    $app->get('/curso-libertas/', fn(Request $request, Response $response) => $this->MissionController->libertasCourse($request, $response));
    $app->get('/emails/', fn(Request $request, Response $response) => $this->MissionController->emails($request, $response));
    $app->get('/{id}/', fn(Request $request, Response $response) => $this->MissionController->show($request, $response));
    $app->put('/novo-status/{id}/', fn(Request $request, Response $response) => $this->MissionController->changeActive($request, $response));
    $app->delete('/{id}/', fn(Request $request, Response $response) => $this->MissionController->destroy($request, $response));
});
