<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/dam', function () use ($app) {

    $app->get('/gastos/', fn(Request $request, Response $response) => $this->DamController->spending($request, $response));

    $app->get('/votacoes/', fn(Request $request, Response $response) => $this->DamController->polls($request, $response));

    $app->get('/presencas/', fn(Request $request, Response $response) => $this->DamController->presence($request, $response));

    $app->get('/relatorios/', fn(Request $request, Response $response) => $this->DamController->reports($request, $response));

    $app->get('/projetos/', fn(Request $request, Response $response) => $this->DamController->projects($request, $response));

    $app->get('/mostra-projetos/', fn(Request $request, Response $response) => $this->DamController->getProjects($request, $response));

    $app->get('/mostra-tags/', fn(Request $request, Response $response) => $this->DamController->getProjectTags($request, $response));

    $app->get('/mostra-autores/', fn(Request $request, Response $response) => $this->DamController->getProjectAuthors($request, $response));

});
