<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;


$app->get('/', fn(Request $request, Response $response) => $this->UserController->index($request, $response));

$app->group('/usuario', function () use ($app) {

    $app->get('/', fn(Request $request, Response $response) => $this->UserController->personalData($request, $response));

    $app->get('/filiacao/', fn(Request $request, Response $response) => $this->UserController->newAffiliation($request, $response));

    $app->get('/filiacao/pagamento/', fn(Request $request, Response $response) => $this->UserController->affiliationPayment($request, $response));

    $app->get('/desfiliacao/', fn(Request $request, Response $response) => $this->UserController->disaffection($request, $response));

    $app->get('/cancelar-solicitacao/', fn(Request $request, Response $response) => $this->UserController->affiliationCancel($request, $response));

    $app->put('/atualiza-dados/', fn(Request $request, Response $response) => $this->UserController->updatePersonalData($request, $response));

    $app->post('/finaliza-atualizacao/', fn(Request $request, Response $response) => $this->UserController->updateFinish($request, $response));

    $app->get('/indicacao/', fn(Request $request, Response $response) => $this->UserController->recommendation($request, $response));

    $app->get('/indicacoes/', fn(Request $request, Response $response) => $this->UserController->listRecommendation($request, $response));

    $app->post('/indicacoes-arquivo/', fn(Request $request, Response $response) => $this->UserController->fileRecommendation($request, $response));

    $app->get('/carta-desfiliacao/', fn(Request $request, Response $response) => $this->UserController->disaffiliationLetter($request, $response));

});
