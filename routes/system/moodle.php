<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/moodle', function () use ($app) {

    $app->get('/[{short}/]', fn(Request $request, Response $response) => $this->MoodleController->index($request, $response));

});