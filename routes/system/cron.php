<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/cron', function () use ($app) {

    $app->get('/carga-auth/', fn(Request $request, Response $response) => $this->CronController->chargeAuth());
    $app->get('/baixa-pix/', fn(Request $request, Response $response) => $this->FinancialController->checkPix());

    $app->get('/sinaliza-filiacoes/', fn(Request $request, Response $response) => $this->CronController->membershipNotice($request, $response));

    $app->get('/carrinho-abandonado/', fn(Request $request, Response $response) => $this->CronController->requestsNotice($request, $response));

    $app->get('/filiacoes/', fn(Request $request, Response $response) => $this->CronController->affiliation($request, $response));

    $app->get('/refiliacoes/', fn(Request $request, Response $response) => $this->CronController->refiliation($request, $response));

    $app->get('/finaliza-evento/', fn(Request $request, Response $response) => $this->CronController->endsEvent($request, $response));

    $app->get('/cobranca-cartao/', fn(Request $request, Response $response) => $this->CronController->chargeCard($request, $response));

    $app->get('/cobranca-boleto/', fn(Request $request, Response $response) => $this->CronController->chargeBillet($request, $response));

    $app->get('/reprocessa-cobranca-cartao/', fn(Request $request, Response $response) => $this->CronController->reprocessChargeCard($request, $response));

    $app->get('/reprocessa-cobranca-boleto/', fn(Request $request, Response $response) => $this->CronController->reprocessChargeBillet($request, $response));

    $app->post('/atualiza-fatura/', fn(Request $request, Response $response) => $this->CronController->refreshInvoice($request, $response));

    $app->get('/aprova-filiacao-isenta/', fn(Request $request, Response $response) => $this->CronController->exemptionAffiliation($request, $response));

    $app->get('/renova-filiacao-isenta/', fn(Request $request, Response $response) => $this->CronController->chargeExemption($request, $response));

    $app->get('/resgate-iugu/', fn(Request $request, Response $response) => $this->CronController->IuguRescue($request, $response));

    $app->get('/verifica-taxas/', fn(Request $request, Response $response) => $this->CronController->verifyValueWithoutFeesTransactions($request, $response));

    $app->get('/aumenta-contribuicao/', fn(Request $request, Response $response) => $this->CronController->contributionIncrease($request, $response));

    $app->get('/checa-segmentacao-aumento/', fn(Request $request, Response $response) => $this->CronController->checkSegmentation($request, $response));

    $app->get('/atualizacao-iugu/', fn(Request $request, Response $response) => $this->CronController->refreshCard($request, $response));

    $app->get('/reembolso/', fn(Request $request, Response $response) => $this->CronController->reimbursementIUGU($request, $response));

    $app->get('/botconversa/', fn(Request $request, Response $response) => $this->CronController->botconversa($request, $response));

    $app->get('/envia-whatsapp/', fn(Request $request, Response $response) => $this->CronController->sendWhatsZap($request, $response));

    $app->get('/cobranca-planos/', fn(Request $request, Response $response) => $this->CronController->chargeInstallmentsPlans($request, $response));

    $app->get('/agendamento-cogmo/', fn(Request $request, Response $response) => $this->CronController->scheduleMessageCogmo($request, $response));

    $app->get('/disparo-cogmo/', fn(Request $request, Response $response) => $this->CronController->sendMessageCogmo($request, $response));
});