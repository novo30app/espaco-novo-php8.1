<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/ouvidoria', function () use ($app) {

    $app->get('/', fn(Request $request, Response $response) => $this->OmbudsmanController->index($request, $response));
    $app->post('/', fn(Request $request, Response $response) => $this->OmbudsmanController->save($request, $response));
    $app->post('/replica/', fn(Request $request, Response $response) => $this->OmbudsmanController->saveReply($request, $response));
    $app->get('/cadastro/', fn(Request $request, Response $response) => $this->OmbudsmanController->register($request, $response));
    $app->get('/{id}/', fn(Request $request, Response $response) => $this->OmbudsmanController->view($request, $response));
});