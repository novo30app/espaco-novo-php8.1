<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/novo-mobiliza', function () use ($app) {

    $app->get('/', fn(Request $request, Response $response) => $this->NovoMobilizeController->index($request, $response));

    $app->get('/missoes/', fn(Request $request, Response $response) => $this->NovoMobilizeController->missions($request, $response));

    $app->get('/pontos/', fn(Request $request, Response $response) => $this->NovoMobilizeController->getPoints($request, $response));

    $app->get('/beneficios/', fn(Request $request, Response $response) => $this->NovoMobilizeController->benefits($request, $response));

    $app->post('/beneficios/solicitar/', fn(Request $request, Response $response) => $this->NovoMobilizeController->requestBenefit($request, $response));

});
