<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/processo-seletivo/2022', function () use ($app) {
    $app->get('/', fn(Request $request, Response $response) => $this->SelectiveProcessController->ModuleOne2022ExCandidates($request, $response));
    $app->get('/cadastro/', fn(Request $request, Response $response) => $this->SelectiveProcessController->ModuleOne2022General($request, $response));
    $app->get('/exportar/', fn(Request $request, Response $response) => $this->SelectiveProcessController->export($request, $response));
    $app->get('/apresentacao/', fn(Request $request, Response $response) => $this->SelectiveProcessController->ModuleOne2022Video($request, $response));
    $app->get('/teste-de-alinhamento/', fn(Request $request, Response $response) => $this->SelectiveProcessController->ModuleOne2022Challenge($request, $response));
    $app->post('/cadastro/[{id}/]', fn(Request $request, Response $response) => $this->SelectiveProcessController->saveModuleOne2022General($request, $response));
    $app->post('/apresentacao/', fn(Request $request, Response $response) => $this->SelectiveProcessController->saveModuleOne2022Video($request, $response));
    $app->post('/teste-de-alinhamento/{id}/', fn(Request $request, Response $response) => $this->SelectiveProcessController->saveModuleOne2022Test($request, $response));
    $app->post('/[{id}/]', fn(Request $request, Response $response) => $this->SelectiveProcessController->saveModuleOne2022ExCandidates($request, $response));
});