<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/portal-da-transparencia', function () use ($app) {

    $app->get('/', fn(Request $request, Response $response) => $this->TransparencyPortalController->index($request, $response));

    $app->get('/listagem/', fn(Request $request, Response $response) => $this->TransparencyPortalController->getList($request, $response));

    $app->get('/contratos/{contract}/', fn(Request $request, Response $response) => $this->TransparencyPortalController->contracts($request, $response));

});