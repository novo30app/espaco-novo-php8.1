<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->get('/cadastro/', fn(Request $request, Response $response) => $this->UserController->register($request, $response));

$app->get('/contador/', fn(Request $request, Response $response) => $this->UserController->counter($request, $response));

$app->get('/abaixo-assinado/', fn(Request $request, Response $response) => $this->UserController->undersigned($request, $response));

$app->get('/abaixo-assinado-contador/', fn(Request $request, Response $response) => $this->UserController->undersignedCounter($request, $response));


