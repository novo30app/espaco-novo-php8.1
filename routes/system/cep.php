<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/cep', function () use ($app) {

    $app->get('/denuncias/', fn(Request $request, Response $response) => $this->CepController->complaints($request, $response));

    $app->get('/visualiza/{id}/', fn(Request $request, Response $response) => $this->CepController->view($request, $response));

    $app->get('/nova-denuncia/', fn(Request $request, Response $response) => $this->CepController->register($request, $response));

    $app->get('/documento/{hash}/', fn(Request $request, Response $response) => $this->CepController->document($request, $response));

});