<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/financeiro', function () use ($app) {

    $app->get('/extrato/', fn(Request $request, Response $response) => $this->FinancialController->extract($request, $response));

    $app->get('/meus-cartoes/', fn(Request $request, Response $response) => $this->FinancialController->cards($request, $response));

    $app->get('/meus-cartoes', fn(Request $request, Response $response) => $this->FinancialController->cards($request, $response));

    $app->get('/assinaturas/[{affiliation}/]', fn(Request $request, Response $response) => $this->FinancialController->subscriptions($request, $response));

    $app->get('/recibos/{id}/', fn(Request $request, Response $response) => $this->FinancialController->receipt($request, $response));

    $app->get('/doacao/', fn(Request $request, Response $response) => $this->FinancialController->donation($request, $response));

    $app->post('/doacao/qrcode/{id}/', fn(Request $request, Response $response) => $this->FinancialController->qrcode($request, $response));

    $app->get('/anticipation/', fn(Request $request, Response $response) => $this->FinancialController->anticipation($request, $response));

    $app->get('/payoff/', fn(Request $request, Response $response) => $this->FinancialController->payOff($request, $response));

    $app->get('/contribuicoes-abertas/', fn(Request $request, Response $response) => $this->FinancialController->payOff($request, $response));

    $app->get('/informe-rendimentos/', fn(Request $request, Response $response) => $this->FinancialController->reportIncome($request, $response, false));

    $app->get('/imprime-informe-rendimentos/', fn(Request $request, Response $response) => $this->FinancialController->reportIncome($request, $response, true));

    $app->post('/atualiza-cartao/', fn(Request $request, Response $response) => $this->FinancialController->updatePaymentData($request, $response));

});