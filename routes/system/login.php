<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->get('/instituto-libertas/login/', fn(Request $request, Response $response) => $this->LoginController->login($request, $response, true));
$app->post('/instituto-libertas/login/', fn(Request $request, Response $response) => $this->LoginController->autentication($request, $response, true));
$app->get('/login/', fn(Request $request, Response $response) => $this->LoginController->login($request, $response));
$app->post('/login/', fn(Request $request, Response $response) => $this->LoginController->autentication($request, $response));
$app->get('/instituto-libertas/recuperar-senha/', fn(Request $request, Response $response) => $this->LoginController->recover($request, $response, true));
$app->get('/recuperar-senha/', fn(Request $request, Response $response) => $this->LoginController->recover($request, $response));
$app->get('/recuperar-senha/{id}/', fn(Request $request, Response $response) => $this->LoginController->changePassword($request, $response));
$app->get('/logout/', fn(Request $request, Response $response) => $this->LoginController->logout($request, $response));