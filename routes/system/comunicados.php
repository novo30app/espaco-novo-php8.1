<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app->group('/comunicados', function () use ($app) {

    $app->get('/membros-cep/', fn(Request $request, Response $response) => $this->CommunicatedController->membersCep($request, $response));

    $app->get('/consultas-cep/', fn(Request $request, Response $response) => $this->CommunicatedController->consultationsCep($request, $response));

    $app->get('/decisoes-cep/', fn(Request $request, Response $response) => $this->CommunicatedController->decisionsCep($request, $response));

    $app->get('/perguntas-cep/', fn(Request $request, Response $response) => $this->CommunicatedController->questionsCep($request, $response));

    $app->get('/fluxograma/', fn(Request $request, Response $response) => $this->CommunicatedController->flowchart($request, $response));

    $app->get('/estatuto/', fn(Request $request, Response $response) => $this->CommunicatedController->statute($request, $response));

    $app->get('/conduta/', fn(Request $request, Response $response) => $this->CommunicatedController->conduct($request, $response));

    $app->get('/documentos-locais/', fn(Request $request, Response $response) => $this->CommunicatedController->localDocuments($request, $response));

    $app->get('/comunicados-listagem/', fn(Request $request, Response $response) => $this->ApiController->localDocuments($request, $response));

    $app->get('/visualizar/{file}/', fn(Request $request, Response $response) => $this->CommunicatedController->view($request, $response));

    $app->get('/[{type}/]', fn(Request $request, Response $response) => $this->CommunicatedController->index($request, $response));

});