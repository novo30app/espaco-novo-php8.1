<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");

require 'bootstrap.php';
setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo');
session_start();
ignore_user_abort(1); // run script in background
set_time_limit(0); // run script forever
ini_set("memory_limit", -1); // remove o limite de memória

function dd($arg){
    die(var_dump($arg));
}

foreach (glob(__DIR__ . '/routes/system/*.php') as $filename) {
    require $filename;
}
foreach (glob(__DIR__ . '/routes/api/*.php') as $filename) {
    require $filename;
}

//liberar cors
$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->run();

